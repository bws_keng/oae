<?php

class masterVillageController extends Controller
{
	public $layout='admin';
	public function actionIndex()
	{
		$model = new masterVillage;
		$criteria = new CDbCriteria;
        $criteria->compare('active', 1);
		$criteria->order = 'village_name ASC';
		$data = masterVillage::model()->findAll($criteria);

		$this->render('index', array(
			'data' => $data,
			'model' => $model,
		));
	}
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}
	public function actionEdit($id)
	{
		$model=$this->loadModel($id);

		if(isset($_POST['masterVillage']))
		{
		$model->attributes = $_POST['masterVillage'];
		if($model->validate()){
			if($model->save()){
				$this->redirect(array('view','id' => $model->village_id));
			}
		}
	}
	$this->render('update',array(
		'model' => $model,
	));
	}

					public function actionCreate() 
				{
				$model=new masterVillage();
				
				if(isset($_POST['masterVillage']))
					{
				$model->attributes = $_POST['masterVillage'];
				$model->active = 1;
				if($model->validate()){
					if($model->save()){
					$this->redirect(array('view', 'id' => $model->village_id));
					}
				}
				}
				$this->render('create', Array(
				'model' => $model
				));
				}
			

	public function actionDelete($id)
	{
		$model = $this->loadModel($id);
		$model->active = 0;

		if ($model->save()) {
			$this->redirect(array('index'));
		}
	}
	public function loadModel($id){
		$model = masterVillage::model()->findByPk($id);
		return $model;
	}


	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}