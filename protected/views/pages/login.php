
<!DOCTYPE html>
<html lang="en">

<head>
  <meta name="description" content="Cuba admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
  <meta name="keywords" content="admin template, Cuba admin template, dashboard template, flat admin template, responsive admin template, web app">
  <meta name="author" content="pixelstrap">
  <title>เข้าสู่ระบบ</title>
  <?php include 'theme/assets/include/inc-head.php'; ?>
</head>

<body>
  <div class="tap-top"><i data-feather="chevrons-up"></i></div>

  <div class="page-wrapper">
    <div class="row">
      <div class="bg-login col-lg-6" style="max-height:100vh;padding:0;">
        <div class="overlay"></div>
        <script>
          document.getElementById("myVideo1").autoplay = true;
          document.getElementById("myVideo1").load();
          document.getElementById("myVideo1").controls = false;
        </script>

        <video id="myVideo1" style="width:100%">
          <source src="theme/assets/video/login-bg-new.mp4" type="video/mp4">
          Your browser does not support HTML video.
        </video>


      </div>
      <div class="login-main col-sm-12 col-lg-6"
        <div class="auth-innerright">
          <div class="authentication-box">
            <div class="mt-4">

              <form class="theme-form">
                <div class="text-center mb-3">
                  <img src="theme/assets/images/logo/logo-login.png">
                </div>
                <h4 class="text-center">ลงชื่อเข้าใช้งานระบบ</h4>

                <div class="form-group">
                  <label class="col-form-label pt-0">ชื่อผู้ใช้งาน</label>
                  <input class="form-control" placeholder="ชื่อผู้ใช้งาน" type="text" required="">
                </div>

                <div class="form-group">
                  <label class="col-form-label">รหัสผ่าน</label>
                  <input class="form-control" placeholder="รหัสผ่าน" type="password" required="">
                </div>

                <div class="checkbox p-0 checkbox-solid-primary">
                  <input id="checkbox1" type="checkbox" checked="">
                  <label for="checkbox1">จดจำฉัน</label>
                </div>

                <div class="form-group row mt-3 mb-0">
                  <button class="btn btn-primary btn-block btn-lg" type="submit">เข้าสู่ระบบ</button>
                </div>
                <br>
                <div class="text-center mb-3">
                <a href="#">ลืมรหัสผ่าน</a>
                </div>
              </form>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <?php include 'theme/assets/include/inc-script.php'; ?>

</body>

</html>