<div class="row">
  <div class="table-responsive">
    <table class="table table-borderless table-modalmain" id="">
      <tbody>
        <tr>
          <td class="text-right">ปีเพาะปลูก :<font color="red">*</font></td>
          <td><span class="text-main">
            <select class="form-control digits" id="year-data" name="year">
              <option selected disabled>เลือกปี พ.ศ.</option>
              <?php
              foreach ($Survey as $key => $value) {
                $year = $value->year + 543;
               ?>
               <option value="<?= $value->id;  ?>" ><?= $year;  ?>/63</option>
               <?php
             }
             ?>
           </select></span></td>
         </tr>
         <tr>
          <td class="text-right">พวกที่ :<font color="red">*</font></td>
          <td><span class="text-main"><select class="form-control digits" id="group-data" name="group">
            <option selected disabled>เลือกพวกที่</option>
            <?php
            foreach ($Those as $key => $value) { ?>
             <option value="<?= $value->those_id;  ?>"><?= $value->those_code;  ?> -> <?= $value->those_name;  ?></option>
             <?php
           }
           ?>
         </select></span></td>
       </tr>
       <tr>
        <td class="text-right">จังหวัด :<font color="red">*</font></td>
        <td><span class="text-main"><?php
        $criteria = new CDbCriteria();
        $criteria->compare('active',1);
        $criteria->order = "province_name_th ASC";
        $MtProvince = MtProvince::model()->findAll($criteria);
        ?>
        <select class="form-control digits" id="province-data" name="province">
          <option selected disabled>เลือกจังหวัด</option>
          <?php
          if (!empty($MtProvince)) {                                                    
            foreach ($MtProvince as $key => $value) { ?>
                <option value="<?= $value->id;  ?>" data-id="<?= $value->id; ?>"><?= $value->province_code;  ?> -> <?= $value->province_name_th;  ?></option>
          <?php
            }
          }
          ?>
       </select></td>
     </tr>
   </tbody>
 </table>
</div>
</div>
<hr>
<div class="row">
  <div class="table-responsive">
    <table class="table table-borderless table-modalmain" id="">
      <tbody>
        <tr>
          <td class="text-right">จำนวนหมู่ทั้งหมด :</td>
          <td><span class="text-main"><input class="form-control" type="text" name="total" id="total-data" model-id="0"></span></td>
        </tr>
        <tr>
          <td class="text-right">จำนวนหมู่ที่สุ่ม :</td>
          <td><span class="text-main"><input class="form-control" type="text" name="Random" id="random-data"></span></td>
        </tr>
        <tr>
          <td class="text-right">จำนวนหมู่ที่สำรวจ :</td>
          <td><span class="text-main"><input class="form-control" type="text" name="survey" id="survey-data" disabled="true"></span></td>
        </tr>
      </tbody>
    </table>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function() {
    $("#random-data").keyup(function(){
      $("#survey-data").val($(this).val());
    });
  });

  $("#group-data").change(function() {
        var id = $(this).val();
        if (id != "") {
            $.ajax({
                type: 'POST',
                url: "<?= $this->createUrl('/Processing/checkGroup'); ?>", 
                data: {
                    id_group: id,
                },
                success: function(data) {
                 $('#province-data').empty();
                 $('#province-data').append(data);

                }
            });
        }
    });

  $("#province-data").select2({
       placeholder: "เลือกจังหวัด",
       allowClear: true,
       width: '100%',
       dropdownParent: $("#exampleModal")
  });
  
  $("#group-data").select2( {
     placeholder: "เลือกพวกที่",
     allowClear: true,
     width: '100%',
    dropdownParent: $("#exampleModal")
     });
</script>
