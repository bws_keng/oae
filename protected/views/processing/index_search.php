<link rel="stylesheet" href="http://cdn.datatables.net/1.10.2/css/jquery.dataTables.min.css"></style>
<script type="text/javascript" src="http://cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js"></script>
<div class="col-lg-12 set-height set-padding">
	<div id="section2">	
		<div class="card table-card">
			<div class="table-responsive">
				<table class="table table-striped table-main" id="myTable">
					<thead>
						<tr>
							<th colspan="7" class="text-left">ตารางแสดงข้อมูลระดับที่ 1</th>
						</tr>
						<tr>

							<th>รหัสจังหวัด</th>
							<th>จังหวัด</th>
							<th>จำนวนหมู่ทั้งหมด</th>
							<th>จำนวนหมู่สุ่ม</th>
							<th>จำนวนหมู่สำรวจ</th>
							<th>จัดการ</th>
						</tr>
					</thead>
					<tbody><?php
					if ($model) {
					foreach ($model as $key => $value) {
						$criteria = new CDbCriteria();
						$criteria->compare('id',$value->province);
						$ProvinceQuery = Province::model()->find($criteria);         
						?>
						<tr>

							<td><a href="javascript:void(0)" class="processing-class" data-id="<?php echo $value->id; ?>" data-province="<?php echo $value->province; ?>"><?php echo $ProvinceQuery->province_code; ?></a></td>
							<td><?php echo $ProvinceQuery->province_name_th; ?></td>
							<td><?php echo $value->total;?></td>
							<td><?php echo $value->random;?></td>
							<td><?php echo $value->survey;?></td>
							<td>
								<a href="javascript:void(0)" class="edit-processing" data-id="<?php echo $value->id; ?>"><img src="<?php echo Yii::app()->baseUrl;?>/theme/assets/images/icon/edit.jpg"></a>
								<a href="javascript:void(0)" class="delete-processing" data-id="<?php echo $value->id; ?>"><img src="<?php echo Yii::app()->baseUrl;?>/theme/assets/images/icon/delete.jpg"></a>
							</td>  
						</tr>
						<?php
					}
				}else{
					?>
					<tr>
							<td colspan="7" align="text-center">ไม่พบข้อมูล</td>  
					</tr>
				<?php } ?>
				</tbody>
			</table>
		</div>
	</div>
</div>

</div>

<script type="text/javascript">

$(document).ready(function(){
    $('#myTable').dataTable({
    	responsive: true,
        "pageLength": 10,
        "searching": false,
	    "info": false,
	    "lengthChange": false

    });

});

	$(".processing-class").click(function(){
		var id = $(this).attr("data-id");

		if (id != "") {
			$.ajax({
				url: "<?= $this->createUrl('/Processing/SubProcessing'); ?>", 
				type: "POST",
				data:  {
					id_data:id,
				},
				success: function(data){

					$('#SubProcessing').html(data);
				}                           
			});
		}
	});

	$(".edit-processing").click(function(){
		var id = $(this).attr("data-id");

		if (id != "") {
			$.ajax({
				url: "<?= $this->createUrl('/Processing/updateProcessing'); ?>", 
				type: "POST",
				data:  {
					id_Processing:id,
            },
            success: function(data){

              $('#exampleModal').modal('show');
              $('#exampleModal .modal-body').html(data);
          }                           
      });
		}
	});

	$(".delete-processing").click(function(){
		var id = $(this).attr("data-id");

		if (id != "") {
			$.ajax({
				url: "<?= $this->createUrl('/Processing/DeleteProcessing'); ?>", 
				type: "POST",
				data:  {
					id_Processing:id,
            },
            success: function(data){
     
            if(data == 1){
            	SearchData();
            }else{
            	alert("DELETE DATA ERROR");
            }
          }                           
      });
		}
	});
</script>