<?php
/**
 * คลาสสำหรับการสร้างหน้าสำหรับผู้ดูแลระบบ ใช้ชั่วคราวกรณีที่ต้องการจัดการหน้าแทนการใช้ php แบบเดิม
 */
class AdminController extends Controller
{

    public $layout = 'admin';

    public function init()
    {
      if(!Yii::app()->user->id ){
        $this->redirect(Yii::app()->baseurl.'/user/login?admin=1');
      }else{
        $user = User::model()->findByPk(Yii::app()->user->id);
        if($user->superuser != 1){
        $this->redirect(Yii::app()->baseurl);

        }
      }
    }

    public function actionIndex()
	{
		$this->render('admin');
    }

    /**
     * ฟังค์ชั่นสำหรับแสดงหน้าเว็บไซต์ตามไฟล์ที่อยู่ใน `/views/admin/`
     * โดยนำค่าจาก GET ที่มี key ว่า `page` มาแสดง
     *  */ 
    // public function actionPage()
    // {
    //     $this->render($_GET['page']);
    // }

    public function actionSurveyQuestion()
    {
        $this->render('survey/question', Array(
            "title"  => FormModel::getTitle($_GET['id'])[0],
            "survey" => FormModel::questionList($_GET['id']),
            'years' => QuestionnaireModel::getYears(),
        ));
    }

    public function actionSearchSurveyQuestion()
    {
         $question = isset($_GET['question']) ? $_GET['question'] : null;
         //$year = isset($_GET['year']) ? $_GET['year'] : null;
        $this->renderPartial('survey/question/search', Array(
           // "title"  => FormModel::getTitle($_GET['id'])[0],
            "survey" => FormModel::questionListSearch($_GET['id'],$question),
            //'years' => QuestionnaireModel::getYears(),
        ));
    }

    public function actionSurveyQuestionForm()
    {
        $this->render('survey/question/create', Array(
            "title"  => FormModel::getTitle($_GET['id'])[0],
            'years' => QuestionnaireModel::getYears(),
            'masterlist' => GenericDataModel::list()
        ));
    }
    public function actionSurveyQuestionInsert()
    {
        FormModel::questionCreate($_POST);
        $this->render('survey/question', Array(
            "title"  => FormModel::getTitle($_GET['id'])[0],
            "survey" => FormModel::questionList($_GET['id']),
            'years' => QuestionnaireModel::getYears()
        ));
    }
    public function actionSurveyQuestionDelete()
    {
        FormModel::questionDelete($_POST['key']);
        $this->redirect(Yii::app()->baseurl.'/admin/survey/'.$_GET['id'].'/question');
    }
    public function actionSurveyQuestionEdit()
    {
        $this->render('survey/question/edit', Array(
            "title"  => FormModel::getTitle($_GET['id'])[0],
            'years' => QuestionnaireModel::getYears(),
            'masterlist' => GenericDataModel::list(),
            'data' => FormModel::getQuestion($_GET['question_id'], $_GET['id'])
        ));
    }
    
    public function actionSurveyQuestionUpdate()
    {
        FormModel::questionUpdate($_GET['question_id'], $_GET['id'], $_POST);
        $this->redirect(Yii::app()->baseurl.'/admin/survey/'.$_GET['id'].'/question');
    }

    public function actionsurveyRate()
    {
        $this->render('survey/rate', Array(
            "title"  => FormModel::getTitle($_GET['id'])[0],
            "survey" => FormModel::questionListRate($_GET['id']),
            'years' => QuestionnaireModel::getYears(),
        ));
    }

    public function actionSurveyRateForm()
    {
        $this->render('survey/rate/create', Array(
            "title"  => FormModel::getTitle($_GET['id'])[0],
            'years' => QuestionnaireModel::getYears(),
            'masterlist' => GenericDataModel::list()
        ));
    }

    public function actionSurveyRateInsert()
    {
        if (isset($_POST)) {
            $QuestionGroup = QuestionGroup::model()->findByPk((int)$_POST['question_group']);
            if ($QuestionGroup) {
                $QuestionGroup->description = $_POST['description'];
                $QuestionGroup->save();
            }

            $criteria = new CDbCriteria();
            $criteria->compare('question_group_id',$_POST['question_group']);
            $criteria->compare('question',$_POST['question']);
            $Question_model = Question::model()->find($criteria);

            if (!$Question_model) {

                $Question = new Question;
                $Question->question_group_id = $_POST['question_group'];
                $Question->question = $_POST['question'];
                $Question->type = 'text';
                $Question->sortorder = 1;

                if ($Question->save()) {

                    foreach($_POST as $key => $value){ 

                        $exp_key = explode('-', $key);
                        if($exp_key[0] == 'question_detail'){
                            $model = new QuestionSub;
                            $model->question_id = $Question->question_id;
                            $model->question_detail = $value;
                            if ($model->save()) {
                                foreach($_POST as $keyrange => $valuerange){ 
                                    $exp_keyrange = explode('-', $keyrange);
                                    if ($exp_keyrange[0] == 'range' && $exp_keyrange[1] == $exp_key[1]) {
                                        $QuestionSub = QuestionSub::model()->findByPk($model->id);
                                        $QuestionSub->range = $valuerange;
                                        $QuestionSub->save();
                                    }
                                }
                            }    
                        }  
                    }
                } 
            }
        }
        $this->render('survey/rate', Array(
            "title"  => FormModel::getTitle($_GET['id'])[0],
            "survey" => FormModel::questionListRate($_GET['id']),
            'years' => QuestionnaireModel::getYears(),
        ));
    }

    public function actionSurveyRateEdit()
    {
        if (isset($_GET['id']) && isset($_GET['question_id'])) {

            $model = Question::model()->findByPk((int)$_GET['question_id']);

            if ($model) {
                $criteria = new CDbCriteria();
                $criteria->compare('question_id',$model->question_id);
                $QuestionSub = QuestionSub::model()->findAll($criteria);
            }
        
            $this->render('survey/rate/edit', Array(
                "title"  => FormModel::getTitle($_GET['id'])[0],
                'years' => QuestionnaireModel::getYears(),
                'masterlist' => GenericDataModel::list(),
                'data' => FormModel::getQuestion($_GET['question_id'], $_GET['id']),
                'model' => $model,
                'QuestionSub'=> $QuestionSub
            ));
        }
    }

    public function actionSurveyRateUpdate()
    {
        if (isset($_POST)) {
  
            $QuestionGroup = QuestionGroup::model()->findByPk((int)$_POST['question_group']);
            if ($QuestionGroup) {
                $QuestionGroup->description = $_POST['description'];
                $QuestionGroup->save();
            }
            $Question_model = Question::model()->findByPk((int)$_GET['question_id']);

            if (!$Question_model) {
                $Question = new Question;
                $Question->question_group_id = $_POST['question_group'];
                $Question->question = $_POST['question'];
                $Question->type = 'text';
                $Question->sortorder = 1;
                if ($Question->save()) {

                    foreach($_POST as $key => $value){ 

                        $exp_key = explode('-', $key);
                        if($exp_key[0] == 'question_detail'){
                            $model = new QuestionSub;
                            $model->question_id = $Question->question_id;
                            $model->question_detail = $value;
                            if ($model->save()) {
                                foreach($_POST as $keyrange => $valuerange){ 
                                    $exp_keyrange = explode('-', $keyrange);
                                    if ($exp_keyrange[0] == 'range' && $exp_keyrange[1] == $exp_key[1]) {
                                        $QuestionSub = QuestionSub::model()->findByPk($model->id);
                                        $QuestionSub->range = $valuerange;
                                        $QuestionSub->save();
                                    }
                                }
                            }    
                        }  
                    }
                }  
            }else{
                $Question_model->question = $_POST['question'];
                if ($Question_model->save()) {

                            $Array_ID_QuestionSub = [];
                            foreach($_POST as $key => $value){ 
                                
                                $exp_key = explode('-', $key);
                                
                                if($exp_key[0] == 'question_detail'){
                                    $model = QuestionSub::model()->findByPk((int)$exp_key[1]);
                                    if ($model) {
                                        $model->question_id = $Question_model->question_id;
                                        $model->question_detail = $value;
                                        if ($model->save()) {
                                            foreach($_POST as $keyrange => $valuerange){ 
                                                $exp_keyrange = explode('-', $keyrange);
                                                if ($exp_keyrange[0] == 'range' && $exp_keyrange[1] == $exp_key[1]) {
                                                    $QuestionSub = QuestionSub::model()->findByPk($model->id);
                                                    $QuestionSub->range = $valuerange;
                                                    $QuestionSub->save();
                                                }
                                            }
                                        }  
                                        $Array_ID_QuestionSub[] = $model->id;
                                    }else{
                                        $model = new QuestionSub;
                                        $model->question_id = $Question_model->question_id;
                                        $model->question_detail = $value;
                                        if ($model->save()) {
                                            foreach($_POST as $keyrange => $valuerange){ 
                                                $exp_keyrange = explode('-', $keyrange);
                                                if ($exp_keyrange[0] == 'range' && $exp_keyrange[1] == $exp_key[1]) {
                                                    $QuestionSub = QuestionSub::model()->findByPk($model->id);
                                                    $QuestionSub->range = $valuerange;
                                                    $QuestionSub->save();
                                                }
                                            }
                                        }    
                                        $Array_ID_QuestionSub[] = $model->id;
                                    }
                                }  
                            }
                     
                            if (isset($Array_ID_QuestionSub)) {
                              $criteria = new CDbCriteria;
                              $criteria->compare('question_id',$Question_model->question_id);
                              $criteria->addNotIncondition('id',$Array_ID_QuestionSub);
                              QuestionSub::model()->deleteAll($criteria);
                            }
                }
            }        
            
        }

        $this->render('survey/rate', Array(
            "title"  => FormModel::getTitle($_GET['id'])[0],
            "survey" => FormModel::questionListRate($_GET['id']),
            'years' => QuestionnaireModel::getYears(),
        ));
    }

    public function actionSurveyRateDelete()
    {
        if (isset($_GET['question_id'])) {

            $Question = Question::model()->deleteAll('question_id = :question_id ', array(':question_id' => $_GET['question_id']));

            $QuestionSubDelete = QuestionSub::model()->deleteAll('question_id = :question_id', array(':question_id' => $_GET['question_id']));
        }
        $this->render('survey/rate', Array(
            "title"  => FormModel::getTitle($_GET['id'])[0],
            "survey" => FormModel::questionList($_GET['id']),
            'years' => QuestionnaireModel::getYears(),
        ));
    }

    public function actionSortRate()
    {
        if (isset($_POST["post_order"])) {
            $post_order = $_POST["post_order"];
            if(!empty($post_order)){
                foreach ($post_order as $key => $value) {
                    if($value != ""){
                        $Question = Question::model()->findByPk($value);
                        $Question->sortorder = $key+1;
                        $Question->save();
                    }
                }
                echo(1);
            }
        }
    }


    public function actionSurveyAnswer()
    {
        $this->render('survey/answer', Array(
            "title"  => FormModel::getTitle($_GET['id'])[0],
            "answer" => QuestionnaireModel::answerList($_GET['id']),
            'years' => QuestionnaireModel::getYears(),
        ));
    }

    public function actionSearchSurveyAnswer()
    {
        $question = isset($_GET['question']) ? $_GET['question'] : null;
        $year = isset($_GET['year']) ? $_GET['year'] : null;

        $this->renderPartial('survey/answer/search', Array(
            // "title"  => FormModel::getTitle($_GET['id'])[0],
            "answer" => QuestionnaireModel::answerListSearch($_GET['id'],$question,$year),
            // 'years' => QuestionnaireModel::getYears(),
        ));
    }

    public function actionSurveyAnswerForm()
    {
        $this->render('survey/answer/create', Array(
            "title"  => FormModel::getTitle($_GET['id'])[0],
            "survey" => FormModel::questionList($_GET['id']),
            "questions" => FormModel::questionList($_GET['id']),
            'years' => QuestionnaireModel::getYears(),
            'masterlist' => GenericDataModel::list()
        ));
    }
    public function actionSurveyAnswerInsert()
    {
        QuestionnaireModel::answerInsert($_POST['question'],$_POST['title'],$_POST['description']);
        // exit();
        $this->render('survey/answer', Array(
            "title"  => FormModel::getTitle($_GET['id'])[0],
            "answer" => QuestionnaireModel::answerList($_GET['id']),
            'years' => QuestionnaireModel::getYears(),
        ));
    }
    public function actionSurveyAnswerDelete()
    {
        QuestionnaireModel::answerDelete($_POST['key']);
        $this->redirect(Yii::app()->baseurl.'/admin/survey/'.$_GET['id'].'/answer');
    }
    public function actionSurveyAnswerEdit()
    {
        $this->render('survey/answer/edit', Array(
            "title"  => FormModel::getTitle($_GET['id'])[0],
            "survey" => FormModel::questionList($_GET['id']),
            "questions" => FormModel::questionList($_GET['id']),
            'years' => QuestionnaireModel::getYears(),
            'masterlist' => GenericDataModel::list(),
            'data' => FormModel::getAnswer($_GET['answer_id'], $filter="id")
        ));
    }
    public function actionSurveyAnswerUpdate()
    {
        // print_r(Array('question'=>$_POST['question'], 'title'=>$_POST['title'], 'descriptions'=>json_encode($_POST['description'])));
        QuestionnaireModel::answerUpdate($_GET['answer_id'], $_POST['question'], $_POST['title'], $_POST['description']);
        $this->redirect(Yii::app()->baseurl.'/admin/survey/'.$_GET['id'].'/answer');
    }

    public function actionSurveyGroup()
    {
        $this->render('survey/group', Array(
            "title"  => FormModel::getTitle($_GET['id'])[0],
            "survey" => QuestionnaireModel::surveyListWithGroup($_GET['id']),
            'years' => QuestionnaireModel::getYears(),
        ));
    }

    public function actionSurveyGroupSearch()
    {
        $search_year = isset($_GET['search_year']) ? $_GET['search_year'] : null;
        if (isset($_GET['id'])) {
          
            $this->renderPartial('survey/group/search', Array(
                //"title"  => FormModel::getTitle($id)[0],
                "survey" => QuestionnaireModel::surveyListWithGroupSearch($_GET['id'],$search_year),
                //'years' => QuestionnaireModel::getYears(),
            ));
        }
    }

    public function actionSurveyGroupForm()
    {
        $this->render('survey/group/create', Array(
            "title"  => FormModel::getTitle($_GET['id'])[0],
            "survey" => QuestionnaireModel::surveyListWithoutGroup($_GET['id']),
            "questions" => FormModel::questionList($_GET['id']),
            'years' => QuestionnaireModel::getYears(),
            'masterlist' => GenericDataModel::list()
        ));
    }
    
    public function actionSurveyGroupInsert()
    {
        $data = $_POST;
        $survey_id = $data['survey'];
        unset($data['survey']);
        QuestionnaireModel::surveyQuestionGroupMap($survey_id, $data);
        $this->render('survey/group', Array(
            "title"  => FormModel::getTitle($_GET['id'])[0],
            'years' => QuestionnaireModel::getYears(),
            "survey" => QuestionnaireModel::surveyListWithGroup($_GET['id'])
        ));
    }

    public function actionSurveyGroupDelete()
    {
        QuestionnaireModel::surveyQuestionGroupUnmap($_GET['id'], $_POST['key']);
        $this->redirect(Yii::app()->baseurl.'/admin/survey/'.$_GET['id'].'/group');
    }

    public function actionsurveyGroupEdit()
    {
        $this->render('survey/group/edit', Array(
            "title"  => FormModel::getTitle($_GET['id'])[0],
            "survey" => QuestionnaireModel::surveyListWithoutGroup($_GET['id']),
            "questions" => FormModel::questionList($_GET['id']),
            'years' => QuestionnaireModel::getYears(),
            'masterlist' => GenericDataModel::list(),
            'data' => QuestionnaireModel::getSurveyGroup($_GET['survey_id'], $_GET['id'])
        ));
    }
    public function actionSurveyGroupUpdate()
    {
        $data = $_POST;
        $survey_id = $data['survey'];
        unset($data['survey']);
        // QuestionnaireModel::surveyQuestionGroupUnmap($_GET['id'], $survey_id);
        QuestionnaireModel::surveyQuestionGroupMap($survey_id, $data);
        print_r($data);
        $this->redirect(Yii::app()->baseurl.'/admin/survey/'.$_GET['id'].'/group');
    }


    public function actionQuestionaire()
    {
        $this->render('questioniare/question', Array(
            "survey" => QuestionnaireModel::getQuestion()
        ));
    }

       public function actionSearchQuestion()
    {  
        $catgory_question = isset($_GET['catgory_question']) ? $_GET['catgory_question'] : null;
        $question = isset($_GET['question']) ? $_GET['question'] : null;

       $data = QuestionnaireModel::getSearchQuestion($catgory_question,$question);
        //var_dump($data);exit();
        $this->renderPartial('questioniare/search', Array(
                "survey" => $data,
        ));
    }


    public function actionQuestionaireForm()
    {
        $this->render('questioniare/question/create', Array(
            'years' => QuestionnaireModel::getYears(),
            'masterlist' => GenericDataModel::list(),
            'lastid' => QuestionnaireModel::questionLastId()
        ));
    }

     public function actionQuestionaireExcel()
    {
        $model = new User('import');

        $criteria = new CDbCriteria;
        $QuestionnaireQuestion = QuestionnaireQuestion::model()->findAll($criteria);

        $this->render('questioniare/question/excel', Array(
            'model' => $model,
            'QuestionnaireQuestion' => $QuestionnaireQuestion,
        ));
    }

    public function actionQuestionairetemplateexcel()
    {
        $model = new User('import');

        $this->layout = false;
        $criteria = new CDbCriteria;
        $QuestionnaireQuestion = QuestionnaireQuestion::model()->findAll($criteria);

        $criteria=new CDbCriteria;
        $criteria->order = "id DESC";
        $survey = Survey::model()->find($criteria);

        $criteria = new CDbCriteria;
        $criteria->compare('survey_id',$survey->id);
        $criteria->order = "t.questionnaire_id ASC";
        $Questionnaire = Questionnaire::model()->find($criteria);


        $this->render('questioniare/question/excel-template', Array(
            'model' => $model,
            'QuestionnaireQuestion'=>$QuestionnaireQuestion,
            'survey'=>$survey,
            'Questionnaire'=>$Questionnaire,
        ));
    }

    public function actionQuestionaireExport()
    {
        $model = new User('import');
        $criteria=new CDbCriteria;
        $criteria->compare('active',1);
        $criteria->order = "province_code ASC";
        $MtProvince = MtProvince::model()->findAll($criteria);

        $criteria=new CDbCriteria;
        $criteria->order = "id DESC";
        $survey = Survey::model()->findAll($criteria);

        $criteria = new CDbCriteria;
        $QuestionnaireQuestion = QuestionnaireQuestion::model()->findAll($criteria);
        $this->render('questioniare/question/export', Array(
            'model' => $model,
            'MtProvince' => $MtProvince,
            'survey' => $survey,
            'QuestionnaireQuestion' => $QuestionnaireQuestion,
        ));
    }

    public function actionQuestionaireExportexcel()
    {
        $criteria=new CDbCriteria;
        if ($_POST['export']["Province"] != "all") {
            $criteria->compare('province',$_POST['export']["Province"]);
        }
        if ($_POST['export']["survey"] != "all") {
            $criteria->compare('survey_id',$_POST['export']["survey"]);
        }
        $criteria->order = "t.survey_id ASC , t.province ASC , t.group ASC";
        $Questionnaire = Questionnaire::model()->findAll($criteria);

        $criteria = new CDbCriteria;
        $QuestionnaireQuestion = QuestionnaireQuestion::model()->findAll($criteria);

        $criteria=new CDbCriteria;
        $criteria->compare('id',$_POST['export']["survey"]);
        $criteria->order = "id DESC";
        $survey = Survey::model()->find($criteria);

        $this->layout = false;
        $this->render('questioniare/question/exportexcel', Array(
            'Questionnaire' => $Questionnaire,
            'QuestionnaireQuestion' => $QuestionnaireQuestion,
            'survey' => $survey,
        ));
    }

     public function actionQuestionaireInsertExcel()
     {
        $model = new User('import');
        $HisImportArr = array();
        $HisImportErrorArr = array();
        $HisImportAttrErrorArr = array();
        $HisImportErrorMessageArr = array();
        $HisImportUserPassArr = array();
        $data = array();
        $model->excel_file = CUploadedFile::getInstance($model,'excel_file');
        $phpExcelPath = Yii::getPathOfAlias('ext.phpexcel.Classes');
        include($phpExcelPath . DIRECTORY_SEPARATOR . 'PHPExcel.php');

        $model->excel_file = CUploadedFile::getInstance($model,'excel_file');

        $webroot = Yii::app()->basePath."/..";

        $filename = $webroot.'/uploads/' . $model->excel_file->name;
        $model->excel_file->saveAs($filename);

        $sheet_array = Yii::app()->yexcel->readActiveSheet($filename);
        $inputFileName = $filename;
        $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objReader->setReadDataOnly(true);
        $objPHPExcel = $objReader->load($inputFileName);
        $objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
        $highestRow = $objWorksheet->getHighestRow();
        $highestColumn = $objWorksheet->getHighestColumn();

        $headingsArray = $objWorksheet->rangeToArray('A1:'.$highestColumn.'1',null, true, true, true);
        $headingsArray = $headingsArray[1];

        $r = -1;
        $namedDataArray = array();
        for ($row = 2; $row <= $highestRow; ++$row) {
            $dataRow = $objWorksheet->rangeToArray('A'.$row.':'.$highestColumn.$row,null, true, true, true);
            if ((isset($dataRow[$row]['A'])) && ($dataRow[$row]['A'] > '')) {
                ++$r;
                foreach($headingsArray as $columnKey => $columnHeading) {
                    $namedDataArray[$r][$columnHeading] = $dataRow[$row][$columnKey];
                }
            }
        }

        $criteria = new CDbCriteria;
        $criteria->order = "question_id ASC";
        $QuestionnaireQuestion = QuestionnaireQuestion::model()->findAll($criteria);


        $index = 0;
        foreach($namedDataArray as $key => $result){
            $criteria=new CDbCriteria;
            $criteria->order = "id DESC";
            $survey = Survey::model()->find($criteria);

            $criteria=new CDbCriteria;
            $criteria->compare('survey_id',$survey->id);
            $criteria->compare('province',$result["จังหวัด"]);
            $criteria->compare('district',$result["อำเภอ"]);
            $criteria->compare('subdistrict',$result["ตำบล"]);
            $criteria->compare('village',$result["หมู่ที่"]);
            $criteria->compare('t.group',$result["พวกที่"]);
            $criteria->compare('sample',$result["ผู้ขึ้นทะเบียนเกษตรกร(ตัวอย่างที่)"]);
            $queryCheck = Questionnaire::model()->find($criteria);
            $result["เจ้าหน้าที่สำรวจ(วัน/เดือน/ปี)"] = date("Y-m-d", strtotime($result["เจ้าหน้าที่สำรวจ(วัน/เดือน/ปี)"]));

            $result["เจ้าหน้าที่สำรวจ(รหัส)"] = str_replace("'","",$result["เจ้าหน้าที่สำรวจ(รหัส)"]);

            $result["จังหวัด"] = str_replace("'","",$result["จังหวัด"]);
            $result["อำเภอ"] = str_replace("'","",$result["อำเภอ"]);
            $result["ตำบล"] = str_replace("'","",$result["ตำบล"]);
            $result["หมู่ที่"] = str_replace("'","",$result["หมู่ที่"]);

            $result["ผู้ขึ้นทะเบียนเกษตรกร(เลขบัตรประจำตัวประชาชน)"] = str_replace("'","",$result["ผู้ขึ้นทะเบียนเกษตรกร(เลขบัตรประจำตัวประชาชน)"]);
            $result["ชื่อผู้ให้ข้อมูล(เลขบัตรประจำตัวประชาชน)"] = str_replace("'","",$result["ชื่อผู้ให้ข้อมูล(เลขบัตรประจำตัวประชาชน)"]);
            $result["ชื่อผู้ให้ข้อมูล(เบอร์ติดต่อ)"] = str_replace("'","",$result["ชื่อผู้ให้ข้อมูล(เบอร์ติดต่อ)"]);

            if (empty($queryCheck)) {
                $queryCheck = new Questionnaire();
                $queryCheck->staff_name = $result["เจ้าหน้าที่สำรวจ(ชื่อ)"];
                $queryCheck->staff_lname = $result["เจ้าหน้าที่สำรวจ(นามสกุล)"];
                $queryCheck->staff_id = $result["เจ้าหน้าที่สำรวจ(รหัส)"];
                $queryCheck->survey_time = $result["เจ้าหน้าที่สำรวจ(วัน/เดือน/ปี)"];
                $queryCheck->office_id = $result["สศท."];
                $queryCheck->province = $result["จังหวัด"];
                $queryCheck->district = $result["อำเภอ"];
                $queryCheck->subdistrict = $result["ตำบล"];
                $queryCheck->village = $result["หมู่ที่"];
                $queryCheck->addressNumber = $result["บ้านเลขที่"];
                $queryCheck->group = $result["พวกที่"];
                $queryCheck->farmer_name = $result["ผู้ขึ้นทะเบียนเกษตรกร(ชื่อ)"];
                $queryCheck->farmer_lastname = $result["ผู้ขึ้นทะเบียนเกษตรกร(นามสกุล)"];
                $queryCheck->sample = $result["ผู้ขึ้นทะเบียนเกษตรกร(ตัวอย่างที่)"];
                $queryCheck->farmer_id = $result["ผู้ขึ้นทะเบียนเกษตรกร(เลขบัตรประจำตัวประชาชน)"];
                $queryCheck->farmer_registered = $result["ผู้ขึ้นทะเบียนเกษตรกร(ผู้ขึ้นทะเบียนเกษตร)"];
                $queryCheck->informant_name = $result["ชื่อผู้ให้ข้อมูล(ชื่อ)"];
                $queryCheck->informant_lastname = $result["ชื่อผู้ให้ข้อมูล(นามสกุล)"];
                $queryCheck->informant_relation = $result["ชื่อผู้ให้ข้อมูล(ความสัมพันธ์กับหัวหน้าครัวเรือน)"];
                $queryCheck->informant_id = $result["ชื่อผู้ให้ข้อมูล(เลขบัตรประจำตัวประชาชน)"];
                $queryCheck->informant_tel = $result["ชื่อผู้ให้ข้อมูล(เบอร์ติดต่อ)"];
                $queryCheck->inIrrigatedArea = $result["ครัวเรือนเกษตรมีที่ดินทำการเกษตรอย่างน้อย 1 แปลง อยู่ในเขตชลประทาน"];

                $queryCheck->survey_id = $survey->id;
                $queryCheck->save();
            }else{
                $queryCheck->staff_name = $result["เจ้าหน้าที่สำรวจ(ชื่อ)"];
                $queryCheck->staff_lname = $result["เจ้าหน้าที่สำรวจ(นามสกุล)"];
                $queryCheck->staff_id = $result["เจ้าหน้าที่สำรวจ(รหัส)"];
                $queryCheck->survey_time = $result["เจ้าหน้าที่สำรวจ(วัน/เดือน/ปี)"];
                $queryCheck->office_id = $result["สศท."];
                $queryCheck->addressNumber = $result["บ้านเลขที่"];
                $queryCheck->farmer_name = $result["ผู้ขึ้นทะเบียนเกษตรกร(ชื่อ)"];
                $queryCheck->farmer_lastname = $result["ผู้ขึ้นทะเบียนเกษตรกร(นามสกุล)"];
                $queryCheck->farmer_id = $result["ผู้ขึ้นทะเบียนเกษตรกร(เลขบัตรประจำตัวประชาชน)"];
                $queryCheck->farmer_registered = $result["ผู้ขึ้นทะเบียนเกษตรกร(ผู้ขึ้นทะเบียนเกษตร)"];
                $queryCheck->informant_name = $result["ชื่อผู้ให้ข้อมูล(ชื่อ)"];
                $queryCheck->informant_lastname = $result["ชื่อผู้ให้ข้อมูล(นามสกุล)"];
                $queryCheck->informant_relation = $result["ชื่อผู้ให้ข้อมูล(ความสัมพันธ์กับหัวหน้าครัวเรือน)"];
                $queryCheck->informant_id = $result["ชื่อผู้ให้ข้อมูล(เลขบัตรประจำตัวประชาชน)"];
                $queryCheck->informant_tel = $result["ชื่อผู้ให้ข้อมูล(เบอร์ติดต่อ)"];
                $queryCheck->inIrrigatedArea = $result["ครัวเรือนเกษตรมีที่ดินทำการเกษตรอย่างน้อย 1 แปลง อยู่ในเขตชลประทาน"];

                $queryCheck->survey_id = $survey->id;
                $queryCheck->save();
            }

            // Clear คำตอบเก่า
            // $criteria=new CDbCriteria;
            // $criteria->compare('questionnaire_id',$queryCheck->questionnaire_id);
            // $criteria->addIncondition('question_id',$question_id);
            // $SurveyValue = SurveyValue::model()->findAll($criteria);
            // $survey_group = array();
            // foreach ($SurveyValue as $key => $value) {
            //     $value->delete();
            //     if (!in_array($value->survey_group, $survey_group)) {
            //         $survey_group[] = $value->survey_group;
            //     }
            // }
            // $criteria=new CDbCriteria;
            // $criteria->addIncondition('survey_group',$survey_group);
            // $SurveyGroup = SurveyGroup::model()->findAll($criteria);
            // foreach ($SurveyGroup as $key => $value) {
            //     $value->delete();
            // }
            // Clear คำตอบเก่า

            // เพิ่มคำตอบ *คำถามแบบสอบถาม
            foreach ($QuestionnaireQuestion as $key => $value) {
                if (isset($result["QuestionnaireQuestion-".$value->question_id])) {
                    $criteria=new CDbCriteria;
                    $criteria->compare('question_id',$queryCheck->questionnaire_id);
                    $criteria->compare('questionnaire_question_id',$value->question_id);
                    $QuestionnaireQuestionValue = QuestionnaireQuestionValue::model()->find($criteria);
                    if (!empty($QuestionnaireQuestionValue)) {
                        $QuestionnaireQuestionValue->value = $result["QuestionnaireQuestion-".$value->question_id];
                        $QuestionnaireQuestionValue->save();
                    }else{
                        $QuestionnaireQuestionValue = new QuestionnaireQuestionValue();
                        $QuestionnaireQuestionValue->question_id = $queryCheck->questionnaire_id;
                        $QuestionnaireQuestionValue->questionnaire_question_id = $value->question_id;
                        $QuestionnaireQuestionValue->value = $result["QuestionnaireQuestion-".$value->question_id];
                        $QuestionnaireQuestionValue->save();
                    }
                }
            }
            // เพิ่มคำตอบ *คำถามแบบสอบถาม


            // เพิ่มคำตอบใหม่ *คำถามหมวด
            // $newGroup = new SurveyGroup();
            // $newGroup->timestamp = date("Y-m-d h:i:s");
            // $newGroup->save();


            // foreach ($Question as $key => $value) {
            //     if (isset($result["Question-".$value->question_id])) {
            //         $modelValue = new SurveyValue();
            //         $modelValue->questionnaire_id = $queryCheck->questionnaire_id;
            //         $modelValue->question_id = $value->question_id;
            //         $modelValue->value = $result["Question-".$value->question_id];
            //         $modelValue->survey_group = $newGroup->survey_group;
            //         $modelValue->save();
            //     }
            // }
            // เพิ่มคำตอบใหม่ *คำถามหมวด


        }
        $this->redirect(Yii::app()->baseurl.'/admin/questionaire/excel/?success=true');
        exit();
    }

     public function actionSurveyExcel()
    {
        $model = new User('import');
        $this->render('questioniare/survey/excel', Array(
            'model' => $model
        ));
    }

      public function actionSurveyExport()
    {
         $this->layout = FALSE;
        $items = FormModel::generate(1);
        $model = new User('import');
        $this->render('questioniare/survey/export', Array(
            'model' => $model,
            'items' => $items

        ));
    }
   public function actionSurvey()
    {   
        $model=new User('import');
        $HisImportArr = array();
        $HisImportErrorArr = array();
        $HisImportAttrErrorArr = array();
        $HisImportErrorMessageArr = array();
        $HisImportUserPassArr = array();
        $data = array();
        // if(isset($_FILES['excel_import_student']))
        $model->excel_file = CUploadedFile::getInstance($model,'excel_file');
        
            $phpExcelPath = Yii::getPathOfAlias('ext.phpexcel.Classes');
            include($phpExcelPath . DIRECTORY_SEPARATOR . 'PHPExcel.php');

            $model->excel_file = CUploadedFile::getInstance($model,'excel_file');
            // $model->excel_file =  $_FILES['excel_import_student'];
           
            //if ($model->excel_file && $model->validate()) {
                // $webroot = YiiBase::getPathOfAlias('webroot');
                $webroot = Yii::app()->basePath."/..";
                // $filename = $webroot.'/uploads/' . $model->excel_file->name . '.' . $model->excel_file->extensionName;
                $filename = $webroot.'/uploads/' . $model->excel_file->name;
                $model->excel_file->saveAs($filename);

                $sheet_array = Yii::app()->yexcel->readActiveSheet($filename);
                $inputFileName = $filename;
                $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
                $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                $objReader->setReadDataOnly(true);
                $objPHPExcel = $objReader->load($inputFileName);
                $objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
                $highestRow = $objWorksheet->getHighestRow();
                $highestColumn = $objWorksheet->getHighestColumn();
                

                for ($row = 1; $row <= $highestRow; ++$row) {

                   $dataRow = $objWorksheet->rangeToArray('A'.$row.':'.$highestColumn.$row,null, true, true, true);
                   $data_row = $dataRow[$row];

                   if(!empty($data_row["A"])){

                    $ex = explode(';',$data_row["A"]);
                 
                    $result=array_flip($ex);
                    // var_dump($ex);

                    $chk = [];
                    foreach ($ex as $key => $value) {
                        if($key == 0){
                            $text = $value;
                        }else{
                            $text = substr($value, 1, -1);
                        }
                        $chk[] = $text;
                    }

            if($chk[0]=='Socio'){
                $question_key = $chk[0];
                $page = $chk[1];
                $staff_id = $chk[2];
                $survey_time = $chk[3];
                $office = sprintf("%01d",$chk[4]);
                $province = sprintf("%03d",$chk[5]);
                $district = sprintf("%02d",$chk[9]);
                // var_dump($district);exit();
                $subdistrict = sprintf("%02d",$chk[7]);
                $village = sprintf("%02d",$chk[8]);
                $group = intval($chk[4]);

                $sample = intval($chk[10]);
                $farmer_id = $chk[11];
                $informant_id = $chk[12];
                $inIrrigatedArea = $chk[13];
                $question1 = $chk[14];
                $question2 = $chk[15];
                $question3 = $chk[16];
                $question4 = $chk[17];
                $question5 = $chk[18];
                $question6 = $chk[19];
                $question7 = $chk[20];
                $question8 = $chk[21];
                $farmer_registered = $chk[22];

                if($survey_time){
                    $chkdate = explode(".",$survey_time);
                    if(count($chkdate)>1){
                        $newdate = (intval($chkdate[2])-543).'-'.$chkdate[1].'-'.$chkdate[0];
                    }
                }else{
                    $newdate = date("Y-m-d");
                }
            $pro = profile::model()->findByAttributes(array('staff_id' => $staff_id));

                $firstname= $pro['firstname'];
                $lastname =$pro['lastname'];

               $sql = 'SELECT
                        questionnaire.*
                    FROM
                        "'.schema_name.'".questionnaire
                    WHERE
                        questionnaire.province = '."'".$province."'".' AND
                        questionnaire.district = '."'".$district."'".' AND
                        questionnaire.subdistrict = '."'".$subdistrict."'".' AND
                        questionnaire.village = '."'".$village."'".' AND
                        questionnaire.group = '."'".intval($group)."'".' AND
                        questionnaire.sample = '."'".intval($sample)."'".' LIMIT 1';

                    $query = FormModel::query($sql);
          
      

            $data = array(
                'id'=>$query[0]['questionnaire_id'],
                'question_key'=>$question_key,
                'page'=>$page,
                'staff_id'=>$staff_id,
                'survey_time'=>$newdate,
                'office'=>$office,
                'province'=>$province,
                'district'=>$district,
                'subdistrict'=>$subdistrict,
                'village'=>$village,
                'addressNumber'=>00,
                'farmer_name'=>$firstname,
                'farmer_lname'=>$lastname,
                'farmer_id'=>null,
                'informant_name'=>null,
                'informant_lname'=>null,
                'informant_relation'=>null,
                'informant_tel'=>null,
                'staff_name'=>null,
                'staff_lname'=>null,
                'group'=>$group,
                'sample'=>$sample,
                'farmer_id'=>$farmer_id,
                'informant_id'=>$informant_id,
                'inIrrigatedArea'=>$inIrrigatedArea,
                'question-1'=>$question1,
                'question-2'=>$question2,
                'question-3'=>$question3,
                'question-4'=>$question4,
                'question-5'=>$question5,
                'question-6'=>$question6,
                'question-7'=>$question7,
                'question-8'=>$question8,
                'farmer_registered'=>$farmer_registered
            );

        }
        
      


        if(count($query) == 0){
            $questionnaire = QuestionnaireModel::create($data);
        }else{
            // $questionnaire = $query;
             QuestionnaireModel::update($data);

            // var_dump(count($questionnaire));exit();
            
        }
                    $one=0;
                    $twoone=0;
                    $twotwo=0;
                    $three=0;
                    $four=0;
                    $five=0;
                    $six=0;
                    $seven=0;
                    $eight=0;
                    $nine=0;
                    $ten=0;
                    $eleven=0;
                    $eleven=0;

                    foreach ($chk as $keychk => $chkkkk) {

                        if($chkkkk == 'M01'){
                           $one =  $keychk;
                        break;
                        }
                    }

                    foreach ($chk as $keychk => $chkkkk) {
                        if($chkkkk == 'M021'){
                           $twoone =  $keychk;
                        break;
                        }
                    }

                    foreach ($chk as $keychk => $chkkkk) {
                        if($chkkkk == 'M022'){
                           $twotwo =  $keychk;
                        break;
                        }
                    }

                    foreach ($chk as $keychk => $chkkkk) {
                        if($chkkkk == 'M03'){
                           $three =  $keychk;
                        break;
                        }
                    }

                    foreach ($chk as $keychk => $chkkkk) {
                        if($chkkkk == 'M04'){
                           $four =  $keychk;
                        break;
                        }
                    }

                    foreach ($chk as $keychk => $chkkkk) {
                        if($chkkkk == 'M05'){
                           $five =  $keychk;
                        break;
                        }
                    }

                    foreach ($chk as $keychk => $chkkkk) {
                        if($chkkkk == 'M06'){
                           $six =  $keychk;
                        break;
                        }
                    }

                    foreach ($chk as $keychk => $chkkkk) {
                        if($chkkkk == 'M07'){
                           $seven =  $keychk;
                        break;
                        }
                    }

                    foreach ($chk as $keychk => $chkkkk) {
                        if($chkkkk == 'M08'){
                           $eight =  $keychk;
                        break;
                        }
                    }

                    foreach ($chk as $keychk => $chkkkk) {
                        if($chkkkk == 'M09'){
                           $nine =  $keychk;
                        break;
                        }
                    }

                    foreach ($chk as $keychk => $chkkkk) {
                        if($chkkkk == 'M10'){
                           $ten =  $keychk;
                        break;
                        }
                    }

                    foreach ($chk as $keychk => $chkkkk) {
                        if($chkkkk == 'M11'){
                           $eleven =  $keychk;
                        break;
                        }
                    }

                    $ch01 = [];
                    $ch021 = [];
                    $ch022 = [];
                    $ch03 = [];
                    $ch04 = [];
                    $ch05 = [];
                    $ch06 = [];
                    $ch07 = [];
                    $ch08 = [];
                    $ch09 = [];
                    $ch10 = [];
                    $ch11 = [];



                    foreach ($chk as $keysave => $chkkkksave) {
                        if(!$chkkkksave || $chkkkksave == '.'){
                            $chkkkksave = null;
                        }
                        
                    if($keysave >= $one+1 && $keysave < $twoone){
                        $ch01[] = $chkkkksave;
                    }

                    if($keysave >= $twoone+1 && $keysave < $twotwo){
                        if($chkkkksave != 'M021'){
                        $ch021[] = $chkkkksave;
                        }
                    }

                    if($keysave >= $twotwo+1 && $keysave < $three){
                        if($chkkkksave != 'M022'){
                        $ch022[] = $chkkkksave;
                        }
                    }


                    if($keysave >= $three+1 && $keysave < $four){
                        if($chkkkksave != 'M03'){
                        $ch03[] = $chkkkksave;
                        }
                    }

                    if($keysave >= $four+1 && $keysave < $five){
                        if($chkkkksave != 'M04'){
                        $ch04[] = $chkkkksave;
                        }
                    }

                    if($keysave >= $five+1 && $keysave < $six){
                        if($chkkkksave != 'M05'){
                        $ch05[] = $chkkkksave;
                        }
                    }

                    if($keysave >= $six+1 && $keysave < $seven){
                        if($chkkkksave != 'M06'){
                        $ch06[] = $chkkkksave;
                        }
                    }

                    if($keysave >= $seven+1 && $keysave < $eight){
                        if($chkkkksave != 'M07'){
                        $ch07[] = $chkkkksave;
                        }
                    }

                    if($keysave >= $eight+1 && $keysave < $nine){
                        if($chkkkksave != 'M08'){
                        $ch08[] = $chkkkksave;
                        }
                    }

                    if($keysave >= $nine+1 && $keysave < $ten){
                        if($chkkkksave != 'M09'){
                        $ch09[] = $chkkkksave;
                        }
                    }

                    if($keysave >= $ten+1 && $keysave < $eleven){
                         if($chkkkksave != 'M10'){
                        $ch10[] = $chkkkksave;
                        }
                    }

                    if($keysave >= $eleven+1 ){
                        if($chkkkksave != 'M11'){
                        $ch11[] = $chkkkksave;
                        }
                    }

                }


                $insert_1 = FormModel::generate(1);
                $insert_arr1 = [];
                $insert_21 = FormModel::generate(2);
                $insert_arr21 = [];
                $insert_22 = FormModel::generate(3);
                $insert_arr22 = [];
                $insert_3 = FormModel::generate(4);
                $insert_arr3 = [];
                $insert_4 = FormModel::generate(5);
                $insert_arr4 = [];
                $insert_5 = FormModel::generate(6);
                $insert_arr5 = [];
                $insert_6 = FormModel::generate(7);
                $insert_arr6 = [];
                $insert_7 = FormModel::generate(8);
                $insert_arr7 = [];
                $insert_8 = FormModel::generate(9);
                $insert_arr8 = [];
                $insert_9 = FormModel::generate(10);
                $insert_arr9 = [];
                $insert_10 = FormModel::generate(11);
                $insert_arr10 = [];
                $insert_11 = FormModel::generate(12);
                $insert_arr11 = [];


                foreach ($insert_1 as $key1 => $item1) {
                        if($item1['type'] != 'note' && $item1['type'] != 'header' && $item1['type'] != 'line'){
                            $insert_arr1[] = $item1['question_id'];
                        }
                }

                foreach ($insert_21 as $key21 => $item21) {
                        if($item21['type'] != 'note' && $item21['type'] != 'header' && $item21['type'] != 'line'){
                            $insert_arr21[] = $item21['question_id'];
                        }
                }

                foreach ($insert_22 as $key22 => $item22) {
                        if($item22['type'] != 'note' && $item22['type'] != 'header' && $item22['type'] != 'line'){
                            $insert_arr22[] = $item22['question_id'];
                        }
                }

                foreach ($insert_3 as $key3 => $item3) {
                        if($item3['type'] != 'note' && $item3['type'] != 'header' && $item3['type'] != 'line'){
                            $insert_arr3[] = $item3['question_id'];
                        }
                }

                foreach ($insert_4 as $key4 => $item4) {
                        if($item4['type'] != 'note' && $item4['type'] != 'header' && $item4['type'] != 'line'){
                            $insert_arr4[] = $item4['question_id'];
                        }
                }

                foreach ($insert_5 as $key5 => $item5) {
                        if($item5['type'] != 'note' && $item5['type'] != 'header' && $item5['type'] != 'line'){
                            $insert_arr5[] = $item5['question_id'];
                        }
                }

                foreach ($insert_6 as $key6 => $item6) {
                        if($item6['type'] != 'note' && $item6['type'] != 'header' && $item6['type'] != 'line'){
                            $insert_arr6[] = $item6['question_id'];
                        }
                }

                foreach ($insert_7 as $key7 => $item7) {
                        if($item7['type'] != 'note' && $item7['type'] != 'header' && $item7['type'] != 'line'){
                            $insert_arr7[] = $item7['question_id'];
                        }
                }

                foreach ($insert_8 as $key8 => $item8) {
                        if($item8['type'] != 'note' && $item8['type'] != 'header' && $item8['type'] != 'line'){
                            $insert_arr8[] = $item8['question_id'];
                        }
                }

                foreach ($insert_9 as $key9 => $item9) {
                        if($item9['type'] != 'note' && $item9['type'] != 'header' && $item9['type'] != 'line'){
                            $insert_arr9[] = $item9['question_id'];
                        }
                }

                foreach ($insert_10 as $key10 => $item10) {
                        if($item10['type'] != 'note' && $item10['type'] != 'header' && $item10['type'] != 'line'){
                            $insert_arr10[] = $item10['question_id'];
                        }
                }

                foreach ($insert_11 as $key11 => $item11) {
                        if($item11['type'] != 'note' && $item11['type'] != 'header' && $item11['type'] != 'line'){
                            $insert_arr11[] = $item11['question_id'];
                        }
                }

                if($ch01 != null){
                    $ch01_new = [];
                     foreach ($ch01 as $key01 => $val01) {
                      if($key01 >= 6){
                        $ch01_new[] = $val01;
                      }
                   }

                   $tmp1 = Array();
                   $survey_group1 = FormModel::query('INSERT INTO "'.schema_name.'"."survey_group"("timestamp") VALUES (NOW()) RETURNING *')[0]['survey_group'];


                    foreach($insert_arr1 as $key_new1 => $value1){
                    array_push($tmp1, FormModel::query('INSERT INTO "'.schema_name.'"."survey_value"("questionnaire_id", "question_id", "value", "survey_group") 
                        VALUES ('.$questionnaire[0]["questionnaire_id"].', '.$value1.', \''.$ch01_new[$key_new1].'\', '.$survey_group1.') RETURNING *')[0]);
                    }

                }

                 if($ch021 != null){
                    $ch021_new = [];
                     foreach ($ch021 as $key021 => $val021) {
                      if($key021 >= 6){
                        $ch021_new[] = $val021;
                      }
                   }

                   $tmp21 = Array();
                   $survey_group21 = FormModel::query('INSERT INTO "'.schema_name.'"."survey_group"("timestamp") VALUES (NOW()) RETURNING *')[0]['survey_group'];


                    foreach($insert_arr21 as $key_new21 => $value21){
                    array_push($tmp21, FormModel::query('INSERT INTO "'.schema_name.'"."survey_value"("questionnaire_id", "question_id", "value", "survey_group") 
                        VALUES ('.$questionnaire[0]["questionnaire_id"].', '.$value21.', \''.$ch021_new[$key_new21].'\', '.$survey_group21.') RETURNING *')[0]);
                    }

                }

                 if($ch022 != null){
                    $ch022_new = [];
                     foreach ($ch022 as $key022 => $val022) {
                      if($key022 >= 6){
                        $ch022_new[] = $val022;
                      }
                   }

                   $tmp22 = Array();
                   $survey_group22 = FormModel::query('INSERT INTO "'.schema_name.'"."survey_group"("timestamp") VALUES (NOW()) RETURNING *')[0]['survey_group'];


                    foreach($insert_arr22 as $key_new22 => $value22){
                    array_push($tmp22, FormModel::query('INSERT INTO "'.schema_name.'"."survey_value"("questionnaire_id", "question_id", "value", "survey_group") 
                        VALUES ('.$questionnaire[0]["questionnaire_id"].', '.$value22.', \''.$ch022_new[$key_new22].'\', '.$survey_group22.') RETURNING *')[0]);
                    }

                }

                if($ch03 != null){
                    $ch03_new = [];
                     foreach ($ch03 as $key03 => $val03) {
                      if($key03 >= 6){
                        $ch03_new[] = $val03;
                      }
                   }

                   $tmp3 = Array();
                   $survey_group3 = FormModel::query('INSERT INTO "'.schema_name.'"."survey_group"("timestamp") VALUES (NOW()) RETURNING *')[0]['survey_group'];


                    foreach($insert_arr3 as $key_new3 => $value3){
                    array_push($tmp3, FormModel::query('INSERT INTO "'.schema_name.'"."survey_value"("questionnaire_id", "question_id", "value", "survey_group") 
                        VALUES ('.$questionnaire[0]["questionnaire_id"].', '.$value3.', \''.$ch03_new[$key_new3].'\', '.$survey_group3.') RETURNING *')[0]);
                    }

                }

                if($ch04 != null){
                    $ch04_new = [];
                     foreach ($ch04 as $key04 => $val04) {
                      if($key04 >= 6){
                        $ch04_new[] = $val04;
                      }
                   }

                   $tmp4 = Array();
                   $survey_group4 = FormModel::query('INSERT INTO "'.schema_name.'"."survey_group"("timestamp") VALUES (NOW()) RETURNING *')[0]['survey_group'];


                    foreach($insert_arr4 as $key_new4 => $value4){
                    array_push($tmp4, FormModel::query('INSERT INTO "'.schema_name.'"."survey_value"("questionnaire_id", "question_id", "value", "survey_group") 
                        VALUES ('.$questionnaire[0]["questionnaire_id"].', '.$value4.', \''.$ch04_new[$key_new4].'\', '.$survey_group4.') RETURNING *')[0]);
                    }

                }

                if($ch05 != null){
                    $ch05_new = [];
                     foreach ($ch05 as $key05 => $val05) {
                      if($key05 >= 6){
                        $ch05_new[] = $val05;
                      }
                   }

                   $tmp5 = Array();
                   $survey_group5 = FormModel::query('INSERT INTO "'.schema_name.'"."survey_group"("timestamp") VALUES (NOW()) RETURNING *')[0]['survey_group'];


                    foreach($insert_arr5 as $key_new5 => $value5){
                    array_push($tmp5, FormModel::query('INSERT INTO "'.schema_name.'"."survey_value"("questionnaire_id", "question_id", "value", "survey_group") 
                        VALUES ('.$questionnaire[0]["questionnaire_id"].', '.$value5.', \''.$ch05_new[$key_new5].'\', '.$survey_group5.') RETURNING *')[0]);
                    }

                }

                if($ch06 != null){
                    $ch06_new = [];
                     foreach ($ch06 as $key06 => $val06) {
                      if($key06 >= 6){
                        $ch06_new[] = $val06;
                      }
                   }

                   $tmp6 = Array();
                   $survey_group6 = FormModel::query('INSERT INTO "'.schema_name.'"."survey_group"("timestamp") VALUES (NOW()) RETURNING *')[0]['survey_group'];


                    foreach($insert_arr6 as $key_new6 => $value6){
                    array_push($tmp6, FormModel::query('INSERT INTO "'.schema_name.'"."survey_value"("questionnaire_id", "question_id", "value", "survey_group") 
                        VALUES ('.$questionnaire[0]["questionnaire_id"].', '.$value6.', \''.$ch06_new[$key_new6].'\', '.$survey_group6.') RETURNING *')[0]);
                    }

                }

                if($ch07 != null){
                    $ch07_new = [];
                     foreach ($ch07 as $key07 => $val07) {
                      if($key07 >= 7){
                        $ch07_new[] = $val07;
                      }
                   }

                   $tmp7 = Array();
                   $survey_group7 = FormModel::query('INSERT INTO "'.schema_name.'"."survey_group"("timestamp") VALUES (NOW()) RETURNING *')[0]['survey_group'];


                    foreach($insert_arr7 as $key_new7 => $value7){
                    array_push($tmp7, FormModel::query('INSERT INTO "'.schema_name.'"."survey_value"("questionnaire_id", "question_id", "value", "survey_group") 
                        VALUES ('.$questionnaire[0]["questionnaire_id"].', '.$value7.', \''.$ch07_new[$key_new7].'\', '.$survey_group7.') RETURNING *')[0]);
                    }

                }


                if($ch08 != null){
                    $ch08_new = [];
                     foreach ($ch08 as $key08 => $val08) {
                      if($key08 >= 8){
                        $ch08_new[] = $val08;
                      }
                   }

                   $tmp8 = Array();
                   $survey_group8 = FormModel::query('INSERT INTO "'.schema_name.'"."survey_group"("timestamp") VALUES (NOW()) RETURNING *')[0]['survey_group'];


                    foreach($insert_arr8 as $key_new8 => $value8){
                    array_push($tmp8, FormModel::query('INSERT INTO "'.schema_name.'"."survey_value"("questionnaire_id", "question_id", "value", "survey_group") 
                        VALUES ('.$questionnaire[0]["questionnaire_id"].', '.$value8.', \''.$ch08_new[$key_new8].'\', '.$survey_group8.') RETURNING *')[0]);
                    }

                }

                if($ch09 != null){
                    $ch09_new = [];
                     foreach ($ch09 as $key09 => $val09) {
                      if($key09 >= 9){
                        $ch09_new[] = $val09;
                      }
                   }

                   $tmp9 = Array();
                   $survey_group9 = FormModel::query('INSERT INTO "'.schema_name.'"."survey_group"("timestamp") VALUES (NOW()) RETURNING *')[0]['survey_group'];


                    foreach($insert_arr9 as $key_new9 => $value9){
                    array_push($tmp9, FormModel::query('INSERT INTO "'.schema_name.'"."survey_value"("questionnaire_id", "question_id", "value", "survey_group") 
                        VALUES ('.$questionnaire[0]["questionnaire_id"].', '.$value9.', \''.$ch09_new[$key_new9].'\', '.$survey_group9.') RETURNING *')[0]);
                    }

                }

                if($ch10 != null){
                    $ch10_new = [];
                     foreach ($ch10 as $key10 => $val10) {
                      if($key10 >= 10){
                        $ch10_new[] = $val10;
                      }
                   }

                   $tmp10 = Array();
                   $survey_group10 = FormModel::query('INSERT INTO "'.schema_name.'"."survey_group"("timestamp") VALUES (NOW()) RETURNING *')[0]['survey_group'];


                    foreach($insert_arr10 as $key_new10 => $value10){
                    array_push($tmp10, FormModel::query('INSERT INTO "'.schema_name.'"."survey_value"("questionnaire_id", "question_id", "value", "survey_group") 
                        VALUES ('.$questionnaire[0]["questionnaire_id"].', '.$value10.', \''.$ch10_new[$key_new10].'\', '.$survey_group10.') RETURNING *')[0]);
                    }

                }

                if($ch11 != null){
                    $ch11_new = [];
                     foreach ($ch11 as $key11 => $val11) {
                      if($key11 >= 11){
                        $ch11_new[] = $val11;
                      }
                   }

                   $tmp11 = Array();
                   $survey_group11 = FormModel::query('INSERT INTO "'.schema_name.'"."survey_group"("timestamp") VALUES (NOW()) RETURNING *')[0]['survey_group'];


                    foreach($insert_arr11 as $key_new11 => $value11){
                    array_push($tmp11, FormModel::query('INSERT INTO "'.schema_name.'"."survey_value"("questionnaire_id", "question_id", "value", "survey_group") 
                        VALUES ('.$questionnaire[0]["questionnaire_id"].', '.$value11.', \''.$ch11_new[$key_new11].'\', '.$survey_group11.') RETURNING *')[0]);
                    }

                }
                }
            }

            $this->render('questioniare/survey/excel', array(
                'model'=>$model,

            ));
    }

     public function actionSurveyInsertExcel($id)
    {
        $model=new User('import');
        $HisImportArr = array();
        $HisImportErrorArr = array();
        $HisImportAttrErrorArr = array();
        $HisImportErrorMessageArr = array();
        $HisImportUserPassArr = array();
        $data = array();
        // if(isset($_FILES['excel_import_student']))
        $model->excel_file = CUploadedFile::getInstance($model,'excel_file');
        
            $phpExcelPath = Yii::getPathOfAlias('ext.phpexcel.Classes');
            include($phpExcelPath . DIRECTORY_SEPARATOR . 'PHPExcel.php');

            $model->excel_file = CUploadedFile::getInstance($model,'excel_file');
            // $model->excel_file =  $_FILES['excel_import_student'];
           
            //if ($model->excel_file && $model->validate()) {
                // $webroot = YiiBase::getPathOfAlias('webroot');
                $webroot = Yii::app()->basePath."/..";
                // $filename = $webroot.'/uploads/' . $model->excel_file->name . '.' . $model->excel_file->extensionName;
                $filename = $webroot.'/uploads/' . $model->excel_file->name;
                $model->excel_file->saveAs($filename);

                $sheet_array = Yii::app()->yexcel->readActiveSheet($filename);
                $inputFileName = $filename;
                $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
                $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                $objReader->setReadDataOnly(true);
                $objPHPExcel = $objReader->load($inputFileName);
                $objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
                $highestRow = $objWorksheet->getHighestRow();
                $highestColumn = $objWorksheet->getHighestColumn();
                

                for ($row = 1; $row <= $highestRow; ++$row) {

                   $dataRow = $objWorksheet->rangeToArray('A'.$row.':'.$highestColumn.$row,null, true, true, true);
                   $data_row = $dataRow[$row];

                   if(!empty($data_row["A"])){

                    $ex = explode(';',$data_row["A"]);
                    $one = [];
                    $result=array_flip($ex);
                    // var_dump($ex);

                    $chk = [];
                    foreach ($ex as $key => $value) {
                        if($key == 0){
                            $text = $value;
                        }else{
                            $text = substr($value, 1, -1);
                        }
                        $chk[] = $text;
                    }

                    
                $question_key = $chk[0];
                $page = $chk[1];
                $staff_id = $chk[2];
                $survey_time = $chk[3];
                $office = $chk[4];

                $province = sprintf("%03d",$chk[5]);
                $district = sprintf("%02d",$chk[6]);
                $subdistrict = sprintf("%02d",$chk[7]);
                $village = sprintf("%02d",$chk[8]);

                $group = intval($chk[9]);
                $sample = intval($chk[10]);
                $farmer_id = $chk[11];
                $informant_id = $chk[12];
                $inIrrigatedArea = $chk[13];
                $question1 = $chk[14];
                $question2 = $chk[15];
                $question3 = $chk[16];
                $question4 = $chk[17];
                $question5 = $chk[18];
                $question6 = $chk[19];
                $question7 = $chk[20];
                $question8 = $chk[21];
                $farmer_registered = $chk[22];

                if($survey_time){
                    $chkdate = explode(".",$survey_time);
                    $newdate = (intval($chkdate[2])-543).'-'.$chkdate[1].'-'.$chkdate[0];
                }else{
                    $newdate = date("Y-m-d");
                }
                

            $data = array(
                'question_key'=>$question_key,
                'page'=>$page,
                'staff_id'=>$staff_id,
                'survey_time'=>$newdate,
                'office'=>$office,
                'province'=>$province,
                'district'=>$district,
                'subdistrict'=>$subdistrict,
                'village'=>$village,
                'addressNumber'=>00,
                'farmer_name'=>null,
                'farmer_lname'=>null,
                'farmer_id'=>null,
                'informant_name'=>null,
                'informant_lname'=>null,
                'informant_relation'=>null,
                'informant_tel'=>null,
                'staff_name'=>null,
                'staff_lname'=>null,
                'group'=>$group,
                'sample'=>$sample,
                'farmer_id'=>$farmer_id,
                'informant_id'=>$informant_id,
                'inIrrigatedArea'=>$inIrrigatedArea,
                'question-1'=>$question1,
                'question-2'=>$question2,
                'question-3'=>$question3,
                'question-4'=>$question4,
                'question-5'=>$question5,
                'question-6'=>$question6,
                'question-7'=>$question7,
                'question-8'=>$question8,
                'farmer_registered'=>$farmer_registered
            );


       $sql = 'SELECT
            questionnaire.*
        FROM
            "'.schema_name.'".questionnaire
        WHERE
            questionnaire.province = '."'".$province."'".' AND
            questionnaire.district = '."'".$district."'".' AND
            questionnaire.subdistrict = '."'".$subdistrict."'".' AND
            questionnaire.village = '."'".$village."'".' AND
            questionnaire.group = '."'".intval($group)."'".' AND
            questionnaire.sample = '."'".intval($sample)."'".' LIMIT 1';

        $query = FormModel::query($sql);
            
        if(count($query) <= 0){
            $questionnaire = QuestionnaireModel::create($data);
        }else{
            $questionnaire = $query;
        }



                    foreach ($chk as $keychk => $chkkkk) {

                        if($chkkkk == 'M01'){
                           $one =  $keychk;
                        break;
                        }
                    }

                    foreach ($chk as $keychk => $chkkkk) {
                        if($chkkkk == 'M021'){
                           $twoone =  $keychk;
                        break;
                        }
                    }

                    foreach ($chk as $keychk => $chkkkk) {
                        if($chkkkk == 'M022'){
                           $twotwo =  $keychk;
                        break;
                        }
                    }

                    foreach ($chk as $keychk => $chkkkk) {
                        if($chkkkk == 'M03'){
                           $three =  $keychk;
                        break;
                        }
                    }

                    foreach ($chk as $keychk => $chkkkk) {
                        if($chkkkk == 'M04'){
                           $four =  $keychk;
                        break;
                        }
                    }

                    foreach ($chk as $keychk => $chkkkk) {
                        if($chkkkk == 'M05'){
                           $five =  $keychk;
                        break;
                        }
                    }

                    foreach ($chk as $keychk => $chkkkk) {
                        if($chkkkk == 'M06'){
                           $six =  $keychk;
                        break;
                        }
                    }

                    foreach ($chk as $keychk => $chkkkk) {
                        if($chkkkk == 'M07'){
                           $seven =  $keychk;
                        break;
                        }
                    }

                    foreach ($chk as $keychk => $chkkkk) {
                        if($chkkkk == 'M08'){
                           $eight =  $keychk;
                        break;
                        }
                    }

                    foreach ($chk as $keychk => $chkkkk) {
                        if($chkkkk == 'M09'){
                           $nine =  $keychk;
                        break;
                        }
                    }

                    foreach ($chk as $keychk => $chkkkk) {
                        if($chkkkk == 'M10'){
                           $ten =  $keychk;
                        break;
                        }
                    }

                    foreach ($chk as $keychk => $chkkkk) {
                        if($chkkkk == 'M11'){
                           $eleven =  $keychk;
                        break;
                        }
                    }

                    $ch01 = [];
                    $ch021 = [];
                    $ch022 = [];
                    $ch03 = [];
                    $ch04 = [];
                    $ch05 = [];
                    $ch06 = [];
                    $ch07 = [];
                    $ch08 = [];
                    $ch09 = [];
                    $ch10 = [];
                    $ch11 = [];



                    foreach ($chk as $keysave => $chkkkksave) {
                        if(!$chkkkksave || $chkkkksave == '.'){
                            $chkkkksave = null;
                        }
                        
                    if($keysave >= $one+1 && $keysave < $twoone){
                        $ch01[] = $chkkkksave;
                    }

                    if($keysave >= $twoone+1 && $keysave < $twotwo){
                        if($chkkkksave != 'M021'){
                        $ch021[] = $chkkkksave;
                        }
                    }

                    if($keysave >= $twotwo+1 && $keysave < $three){
                        if($chkkkksave != 'M022'){
                        $ch022[] = $chkkkksave;
                        }
                    }


                    if($keysave >= $three+1 && $keysave < $four){
                        if($chkkkksave != 'M03'){
                        $ch03[] = $chkkkksave;
                        }
                    }

                    if($keysave >= $four+1 && $keysave < $five){
                        if($chkkkksave != 'M04'){
                        $ch04[] = $chkkkksave;
                        }
                    }

                    if($keysave >= $five+1 && $keysave < $six){
                        if($chkkkksave != 'M05'){
                        $ch05[] = $chkkkksave;
                        }
                    }

                    if($keysave >= $six+1 && $keysave < $seven){
                        if($chkkkksave != 'M06'){
                        $ch06[] = $chkkkksave;
                        }
                    }

                    if($keysave >= $seven+1 && $keysave < $eight){
                        if($chkkkksave != 'M07'){
                        $ch07[] = $chkkkksave;
                        }
                    }

                    if($keysave >= $eight+1 && $keysave < $nine){
                        if($chkkkksave != 'M08'){
                        $ch08[] = $chkkkksave;
                        }
                    }

                    if($keysave >= $nine+1 && $keysave < $ten){
                        if($chkkkksave != 'M09'){
                        $ch09[] = $chkkkksave;
                        }
                    }

                    if($keysave >= $ten+1 && $keysave < $eleven){
                         if($chkkkksave != 'M10'){
                        $ch10[] = $chkkkksave;
                        }
                    }

                    if($keysave >= $eleven+1 ){
                        if($chkkkksave != 'M11'){
                        $ch11[] = $chkkkksave;
                        }
                    }

                }


                $insert_1 = FormModel::generate(1);
                $insert_arr1 = [];
                $insert_21 = FormModel::generate(2);
                $insert_arr21 = [];
                $insert_22 = FormModel::generate(3);
                $insert_arr22 = [];
                $insert_3 = FormModel::generate(4);
                $insert_arr3 = [];
                $insert_4 = FormModel::generate(5);
                $insert_arr4 = [];
                $insert_5 = FormModel::generate(6);
                $insert_arr5 = [];
                $insert_6 = FormModel::generate(7);
                $insert_arr6 = [];
                $insert_7 = FormModel::generate(8);
                $insert_arr7 = [];
                $insert_8 = FormModel::generate(9);
                $insert_arr8 = [];
                $insert_9 = FormModel::generate(10);
                $insert_arr9 = [];
                $insert_10 = FormModel::generate(11);
                $insert_arr10 = [];
                $insert_11 = FormModel::generate(12);
                $insert_arr11 = [];


                foreach ($insert_1 as $key1 => $item1) {
                        if($item1['type'] != 'note' && $item1['type'] != 'header' && $item1['type'] != 'line'){
                            $insert_arr1[] = $item1['question_id'];
                        }
                }

                foreach ($insert_21 as $key21 => $item21) {
                        if($item21['type'] != 'note' && $item21['type'] != 'header' && $item21['type'] != 'line'){
                            $insert_arr21[] = $item21['question_id'];
                        }
                }

                foreach ($insert_22 as $key22 => $item22) {
                        if($item22['type'] != 'note' && $item22['type'] != 'header' && $item22['type'] != 'line'){
                            $insert_arr22[] = $item22['question_id'];
                        }
                }

                foreach ($insert_3 as $key3 => $item3) {
                        if($item3['type'] != 'note' && $item3['type'] != 'header' && $item3['type'] != 'line'){
                            $insert_arr3[] = $item3['question_id'];
                        }
                }

                foreach ($insert_4 as $key4 => $item4) {
                        if($item4['type'] != 'note' && $item4['type'] != 'header' && $item4['type'] != 'line'){
                            $insert_arr4[] = $item4['question_id'];
                        }
                }

                foreach ($insert_5 as $key5 => $item5) {
                        if($item5['type'] != 'note' && $item5['type'] != 'header' && $item5['type'] != 'line'){
                            $insert_arr5[] = $item5['question_id'];
                        }
                }

                foreach ($insert_6 as $key6 => $item6) {
                        if($item6['type'] != 'note' && $item6['type'] != 'header' && $item6['type'] != 'line'){
                            $insert_arr6[] = $item6['question_id'];
                        }
                }

                foreach ($insert_7 as $key7 => $item7) {
                        if($item7['type'] != 'note' && $item7['type'] != 'header' && $item7['type'] != 'line'){
                            $insert_arr7[] = $item7['question_id'];
                        }
                }

                foreach ($insert_8 as $key8 => $item8) {
                        if($item8['type'] != 'note' && $item8['type'] != 'header' && $item8['type'] != 'line'){
                            $insert_arr8[] = $item8['question_id'];
                        }
                }

                foreach ($insert_9 as $key9 => $item9) {
                        if($item9['type'] != 'note' && $item9['type'] != 'header' && $item9['type'] != 'line'){
                            $insert_arr9[] = $item9['question_id'];
                        }
                }

                foreach ($insert_10 as $key10 => $item10) {
                        if($item10['type'] != 'note' && $item10['type'] != 'header' && $item10['type'] != 'line'){
                            $insert_arr10[] = $item10['question_id'];
                        }
                }

                foreach ($insert_11 as $key11 => $item11) {
                        if($item11['type'] != 'note' && $item11['type'] != 'header' && $item11['type'] != 'line'){
                            $insert_arr11[] = $item11['question_id'];
                        }
                }

                if($ch01[0] != null){
                    $ch01_new = [];
                     foreach ($ch01 as $key01 => $val01) {
                      if($key01 >= 6){
                        $ch01_new[] = $val01;
                      }
                   }

                   $tmp1 = Array();
                   $survey_group1 = FormModel::query('INSERT INTO "'.schema_name.'"."survey_group"("timestamp") VALUES (NOW()) RETURNING *')[0]['survey_group'];


                    foreach($insert_arr1 as $key_new1 => $value1){
                    array_push($tmp1, FormModel::query('INSERT INTO "'.schema_name.'"."survey_value"("questionnaire_id", "question_id", "value", "survey_group") 
                        VALUES ('.$questionnaire[0]["questionnaire_id"].', '.$value1.', \''.$ch01_new[$key_new1].'\', '.$survey_group1.') RETURNING *')[0]);
                    }

                }

                 if($ch021[0] != null){
                    $ch021_new = [];
                     foreach ($ch021 as $key021 => $val021) {
                      if($key021 >= 6){
                        $ch021_new[] = $val021;
                      }
                   }

                   $tmp21 = Array();
                   $survey_group21 = FormModel::query('INSERT INTO "'.schema_name.'"."survey_group"("timestamp") VALUES (NOW()) RETURNING *')[0]['survey_group'];


                    foreach($insert_arr21 as $key_new21 => $value21){
                    array_push($tmp21, FormModel::query('INSERT INTO "'.schema_name.'"."survey_value"("questionnaire_id", "question_id", "value", "survey_group") 
                        VALUES ('.$questionnaire[0]["questionnaire_id"].', '.$value21.', \''.$ch021_new[$key_new21].'\', '.$survey_group21.') RETURNING *')[0]);
                    }

                }

                 if($ch022[0] != null){
                    $ch022_new = [];
                     foreach ($ch022 as $key022 => $val022) {
                      if($key022 >= 6){
                        $ch022_new[] = $val022;
                      }
                   }

                   $tmp22 = Array();
                   $survey_group22 = FormModel::query('INSERT INTO "'.schema_name.'"."survey_group"("timestamp") VALUES (NOW()) RETURNING *')[0]['survey_group'];


                    foreach($insert_arr22 as $key_new22 => $value22){
                    array_push($tmp22, FormModel::query('INSERT INTO "'.schema_name.'"."survey_value"("questionnaire_id", "question_id", "value", "survey_group") 
                        VALUES ('.$questionnaire[0]["questionnaire_id"].', '.$value22.', \''.$ch022_new[$key_new22].'\', '.$survey_group22.') RETURNING *')[0]);
                    }

                }

                if($ch03[0] != null){
                    $ch03_new = [];
                     foreach ($ch03 as $key03 => $val03) {
                      if($key03 >= 6){
                        $ch03_new[] = $val03;
                      }
                   }

                   $tmp3 = Array();
                   $survey_group3 = FormModel::query('INSERT INTO "'.schema_name.'"."survey_group"("timestamp") VALUES (NOW()) RETURNING *')[0]['survey_group'];


                    foreach($insert_arr3 as $key_new3 => $value3){
                    array_push($tmp3, FormModel::query('INSERT INTO "'.schema_name.'"."survey_value"("questionnaire_id", "question_id", "value", "survey_group") 
                        VALUES ('.$questionnaire[0]["questionnaire_id"].', '.$value3.', \''.$ch03_new[$key_new3].'\', '.$survey_group3.') RETURNING *')[0]);
                    }

                }

                if($ch04[0] != null){
                    $ch04_new = [];
                     foreach ($ch04 as $key04 => $val04) {
                      if($key04 >= 6){
                        $ch04_new[] = $val04;
                      }
                   }

                   $tmp4 = Array();
                   $survey_group4 = FormModel::query('INSERT INTO "'.schema_name.'"."survey_group"("timestamp") VALUES (NOW()) RETURNING *')[0]['survey_group'];


                    foreach($insert_arr4 as $key_new4 => $value4){
                    array_push($tmp4, FormModel::query('INSERT INTO "'.schema_name.'"."survey_value"("questionnaire_id", "question_id", "value", "survey_group") 
                        VALUES ('.$questionnaire[0]["questionnaire_id"].', '.$value4.', \''.$ch04_new[$key_new4].'\', '.$survey_group4.') RETURNING *')[0]);
                    }

                }

                if($ch05[0] != null){
                    $ch05_new = [];
                     foreach ($ch05 as $key05 => $val05) {
                      if($key05 >= 6){
                        $ch05_new[] = $val05;
                      }
                   }

                   $tmp5 = Array();
                   $survey_group5 = FormModel::query('INSERT INTO "'.schema_name.'"."survey_group"("timestamp") VALUES (NOW()) RETURNING *')[0]['survey_group'];


                    foreach($insert_arr5 as $key_new5 => $value5){
                    array_push($tmp5, FormModel::query('INSERT INTO "'.schema_name.'"."survey_value"("questionnaire_id", "question_id", "value", "survey_group") 
                        VALUES ('.$questionnaire[0]["questionnaire_id"].', '.$value5.', \''.$ch05_new[$key_new5].'\', '.$survey_group5.') RETURNING *')[0]);
                    }

                }

                if($ch06[0] != null){
                    $ch06_new = [];
                     foreach ($ch06 as $key06 => $val06) {
                      if($key06 >= 6){
                        $ch06_new[] = $val06;
                      }
                   }

                   $tmp6 = Array();
                   $survey_group6 = FormModel::query('INSERT INTO "'.schema_name.'"."survey_group"("timestamp") VALUES (NOW()) RETURNING *')[0]['survey_group'];


                    foreach($insert_arr6 as $key_new6 => $value6){
                    array_push($tmp6, FormModel::query('INSERT INTO "'.schema_name.'"."survey_value"("questionnaire_id", "question_id", "value", "survey_group") 
                        VALUES ('.$questionnaire[0]["questionnaire_id"].', '.$value6.', \''.$ch06_new[$key_new6].'\', '.$survey_group6.') RETURNING *')[0]);
                    }

                }

                if($ch07[0] != null){
                    $ch07_new = [];
                     foreach ($ch07 as $key07 => $val07) {
                      if($key07 >= 7){
                        $ch07_new[] = $val07;
                      }
                   }

                   $tmp7 = Array();
                   $survey_group7 = FormModel::query('INSERT INTO "'.schema_name.'"."survey_group"("timestamp") VALUES (NOW()) RETURNING *')[0]['survey_group'];


                    foreach($insert_arr7 as $key_new7 => $value7){
                    array_push($tmp7, FormModel::query('INSERT INTO "'.schema_name.'"."survey_value"("questionnaire_id", "question_id", "value", "survey_group") 
                        VALUES ('.$questionnaire[0]["questionnaire_id"].', '.$value7.', \''.$ch07_new[$key_new7].'\', '.$survey_group7.') RETURNING *')[0]);
                    }

                }


                if($ch08[0] != null){
                    $ch08_new = [];
                     foreach ($ch08 as $key08 => $val08) {
                      if($key08 >= 8){
                        $ch08_new[] = $val08;
                      }
                   }

                   $tmp8 = Array();
                   $survey_group8 = FormModel::query('INSERT INTO "'.schema_name.'"."survey_group"("timestamp") VALUES (NOW()) RETURNING *')[0]['survey_group'];


                    foreach($insert_arr8 as $key_new8 => $value8){
                    array_push($tmp8, FormModel::query('INSERT INTO "'.schema_name.'"."survey_value"("questionnaire_id", "question_id", "value", "survey_group") 
                        VALUES ('.$questionnaire[0]["questionnaire_id"].', '.$value8.', \''.$ch08_new[$key_new8].'\', '.$survey_group8.') RETURNING *')[0]);
                    }

                }

                if($ch09[0] != null){
                    $ch09_new = [];
                     foreach ($ch09 as $key09 => $val09) {
                      if($key09 >= 9){
                        $ch09_new[] = $val09;
                      }
                   }

                   $tmp9 = Array();
                   $survey_group9 = FormModel::query('INSERT INTO "'.schema_name.'"."survey_group"("timestamp") VALUES (NOW()) RETURNING *')[0]['survey_group'];


                    foreach($insert_arr9 as $key_new9 => $value9){
                    array_push($tmp9, FormModel::query('INSERT INTO "'.schema_name.'"."survey_value"("questionnaire_id", "question_id", "value", "survey_group") 
                        VALUES ('.$questionnaire[0]["questionnaire_id"].', '.$value9.', \''.$ch09_new[$key_new9].'\', '.$survey_group9.') RETURNING *')[0]);
                    }

                }

                if($ch10[0] != null){
                    $ch10_new = [];
                     foreach ($ch10 as $key10 => $val10) {
                      if($key10 >= 10){
                        $ch10_new[] = $val10;
                      }
                   }

                   $tmp10 = Array();
                   $survey_group10 = FormModel::query('INSERT INTO "'.schema_name.'"."survey_group"("timestamp") VALUES (NOW()) RETURNING *')[0]['survey_group'];


                    foreach($insert_arr10 as $key_new10 => $value10){
                    array_push($tmp10, FormModel::query('INSERT INTO "'.schema_name.'"."survey_value"("questionnaire_id", "question_id", "value", "survey_group") 
                        VALUES ('.$questionnaire[0]["questionnaire_id"].', '.$value10.', \''.$ch10_new[$key_new10].'\', '.$survey_group10.') RETURNING *')[0]);
                    }

                }

                if($ch11[0] != null){
                    $ch11_new = [];
                     foreach ($ch11 as $key11 => $val11) {
                      if($key11 >= 11){
                        $ch11_new[] = $val11;
                      }
                   }

                   $tmp11 = Array();
                   $survey_group11 = FormModel::query('INSERT INTO "'.schema_name.'"."survey_group"("timestamp") VALUES (NOW()) RETURNING *')[0]['survey_group'];


                    foreach($insert_arr11 as $key_new11 => $value11){
                    array_push($tmp11, FormModel::query('INSERT INTO "'.schema_name.'"."survey_value"("questionnaire_id", "question_id", "value", "survey_group") 
                        VALUES ('.$questionnaire[0]["questionnaire_id"].', '.$value11.', \''.$ch11_new[$key_new11].'\', '.$survey_group11.') RETURNING *')[0]);
                    }

                }
                }
            }

            $this->render('questioniare/survey/excel', array(
                'model'=>$model,

            ));
    }

    public function actionQuestionaireInsert()
    {
        QuestionnaireModel::questionCreate(Array(
            'question_id' => $_POST['question_id'],
            'title' => $_POST['title'],
            'subtitle' => $_POST['subtitle']
        ));
        $this->render('questioniare/question', Array(
            "survey" => QuestionnaireModel::getQuestion()
        ));
    }

    public function actionQuestionaireDelete()
    {
        QuestionnaireModel::questionDelete($_POST['key']);
        $this->redirect(Yii::app()->baseurl.'/admin/questionaire/question');
    }

    public function actionQuestionaireEdit()
    {
        $this->render('questioniare/question/edit', Array(
            'years' => QuestionnaireModel::getYears(),
            'masterlist' => GenericDataModel::list(),
            'lastid' => QuestionnaireModel::questionLastId(),
            'data' => QuestionnaireModel::getQuestion($_GET['id'])
        ));
    }

    public function actionQuestionaireUpdate()
    {
        QuestionnaireModel::questionUpdate($_POST['question_id'], $_POST);
        $this->redirect(Yii::app()->baseurl.'/admin/questionaire/question');
    }

    public function actionQuestionaireSurvey()
    {
        $this->render('questioniare/survey', Array(
            'years' => QuestionnaireModel::getYears(),
            "survey" => QuestionnaireModel::surveyList()
        ));
    }

    public function actionSearchQuestionaire()
    {
        $year_start = isset($_GET['year_start']) ? $_GET['year_start'] : null;
        $year_end = isset($_GET['year_end']) ? $_GET['year_end'] : null;
        $date_start = isset($_GET['date_start']) ? $_GET['date_start'] : null;
        $date_end = isset($_GET['date_end']) ? $_GET['date_end'] : null;

        $this->renderPartial('questioniare/searchQuestionaire', Array(
            'years' => QuestionnaireModel::getYears(),
            "survey" => QuestionnaireModel::surveySearchList($year_start,$year_end,$date_start,$date_end)
        ));
    }

    public function actionQuestionaireSurveyForm()
    {
        $this->render('questioniare/survey/create', Array(
            'years' => QuestionnaireModel::getYears(),
            "surveylist" => QuestionnaireModel::surveyList(),
            "survey" => QuestionnaireModel::getQuestion(),
            'masterlist' => GenericDataModel::list(),
            'lastid' => QuestionnaireModel::questionLastId()
        ));
    }

    public function actionQuestionaireSurveyInsert()
    {
        QuestionnaireModel::surveyInsert($_POST);
        $this->redirect(Yii::app()->baseurl . '/admin/questionaire/survey');
    }

    public function actionQuestionaireSurveyDelete()
    {
        QuestionnaireModel::surveyDelete($_POST['key']);
        $this->redirect(Yii::app()->baseurl . '/admin/questionaire/survey');
    }

    public function actionQuestionaireSurveyEdit()
    {
        $this->render('questioniare/survey/edit', Array(
            'years' => QuestionnaireModel::getYears(),
            "survey" => QuestionnaireModel::getQuestion(),
            'masterlist' => GenericDataModel::list(),
            'lastid' => QuestionnaireModel::questionLastId(),
            'data' => QuestionnaireModel::getSurvey($_GET['id'])
        ));
    }

    public function actionQuestionaireSurveyUpdate()
    {
        QuestionnaireModel::surveyUpdate($_GET['id'], $_POST);
        $this->redirect(Yii::app()->baseurl . '/admin/questionaire/survey');
    }

    public function actionQuestionaireSurveyFormManagement()
    {
        $this->render('questioniare/survey/form', Array(
            'years' => QuestionnaireModel::getYears(),
            "survey" => QuestionnaireModel::getQuestion(),
            'masterlist' => GenericDataModel::list(),
            'lastid' => QuestionnaireModel::questionLastId(),
            'data' => QuestionnaireModel::form()
        ));
    }

    public function actionQuestionaireSurveyFormAction()
    {
        QuestionnaireModel::orderMap($_GET['id'], $_POST['form']);
        $this->redirect(Yii::app()->baseurl . '/admin/questionaire/survey');
    }

    public function actionsurveyGroupManagement()
    {
        $this->render('survey/group/form', Array(
            "title"  => FormModel::getTitle($_GET['id'])[0],
            "survey" => QuestionnaireModel::surveyListWithoutGroup($_GET['id']),
            "data" => FormModel::generate($_GET['id'],$_GET["survey_id"]),
            'years' => QuestionnaireModel::getYears(),
            'masterlist' => GenericDataModel::list()
        ));
    }

    public function actionSurveyGroupAction()
    {
        FormModel::orderMap($_GET['survey_id'], $_POST['form'], $_POST['size']);
        $this->redirect(Yii::app()->baseurl . '/admin/survey/'.$_GET['id'].'/group');
    }

    public function actionsurveyGroupAnswerManagement()
    {
        // $this->render('survey/group/form', Array(
        //     "title"  => FormModel::getTitle($_GET['id'])[0],
        //     "survey" => QuestionnaireModel::surveyListWithoutGroup($_GET['id']),
        //     "data" => FormModel::generate($_GET['id']),
        //     'years' => QuestionnaireModel::getYears(),
        //     'masterlist' => GenericDataModel::list()
        // ));
    }

    public function actionSurveyGroupAnswerAction()
    {
        // print_r($_POST);
        // FormModel::orderMap($_GET['survey_id'], $_POST['form']);
        // $this->redirect(Yii::app()->baseurl . '/admin/survey/'.$_GET['id'].'/group');
    }

    public function actionSurveyMangeForm()
    {
        $this->render('survey/mange/form', Array(
            "title"  => FormModel::getTitle($_GET['id'])[0],
            "survey" => QuestionnaireModel::surveyListWithGroup($_GET['id']),
            'years' => QuestionnaireModel::getYears(),
        ));
    } 

    public function actionSurveyMangeCreate()
    {
        $this->render('survey/mange/create', Array(
            "title"  => FormModel::getTitle($_GET['id'])[0],
            "survey" => QuestionnaireModel::surveyListWithoutGroup($_GET['id']),
            "questions" => FormModel::questionList($_GET['id']),
            'years' => QuestionnaireModel::getYears(),
            'masterlist' => GenericDataModel::list()
        ));
    } 

    public function actionSurveyMangeInsert()
    {
        if (isset($_POST)) {  
            foreach($_POST as $key => $value){

                $exp_key = explode('-', $key);
                if($exp_key[0] == 'form'){
                    $criteria = new CDbCriteria();
                    $criteria->compare('question_group_id',(int)$_GET['id']);
                    $criteria->compare('question_id',$exp_key[1]);
                    $criteria->compare('survey_id',$_POST['survey']);
                    $QuestionMange = QuestionMange::model()->find($criteria);
                    if (!$QuestionMange) {
                        $model = new QuestionMange;
                        $model->question_group_id = (int)$_GET['id'];
                        $model->question_id = $exp_key[1];
                        $model->question_status = 1;
                        $model->survey_id = $_POST['survey'];
                        $model->sortorder = $value;
                        $model->save();
                    }
                }
            }
            $this->render('survey/mange/form', Array(
            "title"  => FormModel::getTitle($_GET['id'])[0],
            "survey" => QuestionnaireModel::surveyListWithGroup($_GET['id']),
            'years' => QuestionnaireModel::getYears(),
        ));
        }
        
    }  

    public function actionSurveyMangeEdit()
    {
        $this->render('survey/mange/edit', Array(
            "title"  => FormModel::getTitle($_GET['id'])[0],
            "survey" => QuestionnaireModel::surveyListWithoutGroup($_GET['id']),
            "questions" => FormModel::questionListMange($_GET['id']),
            'years' => QuestionnaireModel::getYears(),
            'masterlist' => GenericDataModel::list(),
            'data' => QuestionnaireModel::getSurveyGroup($_GET['survey_id'], $_GET['id'])
        ));
    }

    public function actionSurveyMangeUpdate()
    {
        if (isset($_POST)) {
            $array_question_id = array();
            foreach($_POST as $key => $value){
                $exp_key = explode('-', $key);
                if($exp_key[0] == 'form'){
                    $criteria = new CDbCriteria();
                    $criteria->compare('question_group_id',(int)$_GET['id']);
                    $criteria->compare('question_id',$exp_key[1]);
                    $criteria->compare('survey_id',$_POST['survey']);
                    $QuestionMange = QuestionMange::model()->find($criteria);
                    if (!$QuestionMange) {
                        $model = new QuestionMange;
                        $model->question_group_id = (int)$_GET['id'];
                        $model->question_id = $exp_key[1];
                        $model->question_status = 1;
                        $model->survey_id = $_POST['survey'];
                        $model->sortorder = $value;
                        $model->save();
                    }else{
                        $QuestionMange->question_status = 1;
                        $QuestionMange->sortorder = $value;
                        $QuestionMange->save();
                    }

                    $array_question_id[] = $exp_key[1];
                }
            }
            if (isset($array_question_id)) {
                $criteria = new CDbCriteria();
                $criteria->compare('question_group_id',(int)$_GET['id']);
                $criteria->addNotIncondition('question_id',$array_question_id);
                $criteria->compare('survey_id',$_POST['survey']);
                $Question = QuestionMange::model()->findAll($criteria);
                if ($Question) {
                    foreach ($Question as $key => $value) {
                        $value->question_status = 0;
                        $value->save();
                    }
                }
            }
            $this->render('survey/mange/form', Array(
                "title"  => FormModel::getTitle($_GET['id'])[0],
                "survey" => QuestionnaireModel::surveyListWithGroup($_GET['id']),
                'years' => QuestionnaireModel::getYears(),
            ));
        }
    }
    
}