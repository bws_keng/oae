<?php

/**
 * This is the model class for table "{{log_resign}}".
 *
 * The followings are the available columns in table '{{log_resign}}':
 * @property integer $id
 * @property integer $user_id
 * @property string $fullname
 * @property string $fullname_en
 * @property string $staff_id
 * @property string $identification
 * @property string $hire_date
 * @property string $action_status
 * @property integer $created_by
 * @property string $created_date
 */
class LogResign extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'db_oae.log_resign';
	}

	public function beforeSave() 
	{    	

		if($this->isNewRecord)
		{
			$this->created_by = Yii::app()->user->id;
			$this->created_date = date("Y-m-d H:i:s");
		}

		return parent::beforeSave();
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, created_by', 'numerical', 'integerOnly'=>true),
			array('fullname, fullname_en, staff_id, identification', 'length', 'max'=>255),
			array('action_status', 'length', 'max'=>1),
			array('hire_date, created_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, user_id, fullname, fullname_en, staff_id, identification, hire_date, action_status, created_by, created_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'fullname' => 'Fullname',
			'fullname_en' => 'Fullname En',
			'staff_id' => 'Staff',
			'identification' => 'Identification',
			'hire_date' => 'Hire Date',
			'action_status' => 'Action Status',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('fullname',$this->fullname,true);
		$criteria->compare('fullname_en',$this->fullname_en,true);
		$criteria->compare('staff_id',$this->staff_id,true);
		$criteria->compare('identification',$this->identification,true);
		$criteria->compare('hire_date',$this->hire_date,true);
		$criteria->compare('action_status',$this->action_status,true);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('created_date',$this->created_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LogResign the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
