Log การใช้งานผู้เรียน<?php
$formNameModel = 'LogUserAuto';
$titleName = 'Log การใช้งานหน้าบ้าน';

Yii::app()->clientScript->registerScript('search', "
    $('#SearchFormAjax').submit(function(){
        $.fn.yiiGridView.update('$formNameModel-grid', {
            data: $(this).serialize()
        });
        return false;
    });
");
Yii::app()->clientScript->registerScript('updateGridView', <<<EOD
    $.updateGridView = function(gridID, name, value) {
        $("#"+gridID+" input[name*="+name+"], #"+gridID+" select[name*="+name+"]").val(value);
        $.fn.yiiGridView.update(gridID, {data: $.param(
            $("#"+gridID+" input, #"+gridID+" .filters select")
        )});
    }
    $.appendFilter = function(name, varName) {
        var val = eval("$."+varName);
        $("#$formNameModel-grid").append('<input type="hidden" name="'+name+'" value="">');
    }
    $.appendFilter("LogUserAuto[news_per_page]", "news_per_page");
EOD
, CClientScript::POS_READY);

?>
<div class="innerLR">
    <div class="widget" style="margin-top: -1px;">
        <div class="widget-head">
            <h4 class="heading glyphicons show_thumbnails_with_lines"><i></i> <?php echo $titleName;?></h4>
        </div>
        <div class="widget-body">
            <div class="separator bottom form-inline small">
                <span class="pull-right">
                    <label class="strong">แสดงแถว:</label>
                    <?php echo $this->listPageShow($formNameModel);?>
                </span> 
            </div>
            <div class="clear-div"></div>
            <div class="overflow-table">
                <?php $this->widget('AGridView', array(
                    'id'=>$formNameModel.'-grid',
                    'dataProvider'=>$model->search(),
                    'filter'=>$model,
                    'selectableRows' => 2,
                    'rowCssClassExpression'=>'"items[]_{$data->log_id}"',
                    'htmlOptions' => array(
                        'style'=> "margin-top: -1px;",
                    ),
                    'afterAjaxUpdate'=>'function(id, data){
                        $.appendFilter("LogUserAuto[news_per_page]");
                        }',
                        'columns'=>array(
                            array(
                                'header' => 'ลำดับ',
                                'sortable' => false,
                                'htmlOptions' => array(
                                    'width' => '40px',
                                    'text-align' => 'center',
                                ),
                                'value'=>'$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
                            ),
                            'log_idcard',
                            array(
                                'header' => 'ชื่อ - นามสกุล',
                                'name'=>'create_by',
                                'type'=>'raw',
                                'value'=>function($data){
                                    return $data->member->pro_fname . ' ' . $data->member->pro_lname;
                                }
                            ),
                            'log_action',
                            array(
                                'header' => 'รายละเอียด',
                                'name'=>'log_status',
                                'type'=>'raw',
                                // 'filter' => false,
                                'value'=>function($data){

                                    if(strpos($data->log_status, "ออกเลข") !== false){
                                        // $ex_data = explode("ic_code=", $data->log_status);
                                        // return "สมัครเรียนสำเร็จ รหัส : ".$ex_data[1];
                                        $ex_data = explode("||", $data->log_status);
                                        return $ex_data[0];
                                    }elseif(strpos($data->log_status, "เลือก คุณสมบัติ") !== false){
                                        $ex_data = explode(" ", $data->log_status);

                                        $ex_1data = explode("=", $ex_data[1]);
                                        $qua_id = "";
                                        $qua_text = "";
                                        if(isset($ex_1data[1])){
                                            $qua_id = $ex_1data[1];
                                            $MQualification = MQualification::model()->findByPk($qua_id);
                                            $qua_text = $MQualification->qua_name;
                                        }

                                        if(isset($ex_data[2])){
                                            $ex_2data = explode("=", $ex_data[2]);
                                            $ic_id = "";
                                            if(isset($ex_2data[1])){
                                                $ic_id = $ex_2data[1];
                                            }
                                        }
                                        return "เลือก คุณสมบัติ";
                                    }else{
                                        return $data->log_status;
                                    }
                                }
                            ),
                            'create_date',
                        ),
                    )); ?>
                </div>
            </div>
        </div>
    </div>
