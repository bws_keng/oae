<?php
class GenericDataModel
{
    public static function get($name){
        $sql = 'SELECT
            masterdata."name", 
            masterdata."key", 
            masterdata.target_field, 
            masterdata.target_table
        FROM
            "'.schema_name.'".masterdata
        WHERE
            masterdata."name" = province
        ';
        $masterTable = GenericDataModel::query($sql);
        if(count($masterTable) > 0){
            return GenericDataModel::query('
            SELECT
                masterdata."name", 
                masterdata."key", 
                masterdata.target_field, 
                masterdata.target_table
            FROM
                "'.schema_name.'".masterdata');
        }
    }

    public static function list(){
        return GenericDataModel::query('( SELECT DISTINCT generic_data."group" AS NAME FROM "'.schema_name.'".generic_data ) UNION ( SELECT masterdata."name" FROM "'.schema_name.'".masterdata )');
    }

    public static function query($sql){
        $command = Yii::app()->db->createCommand($sql);
        return $command->queryAll();
    }
}
?>