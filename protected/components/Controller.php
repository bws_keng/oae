<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
    public function init()
    {   
        if(!Yii::app()->user->isGuest)
        { 
            $user = User::model()->findByPk(Yii::app()->user->id);
            $user->lastactivity = time();
            $user->last_activity = date('Y-m-d H:i:s');
            if($user->online_status == 0){
                $user->online_status = 1;
            }
            $user->update();

            //AccessControl::check_access_filemanager($user->superuser);

            // $chatstatus=CometchatStatus::model()->find('userid='.Yii::app()->user->id);
            // if(!$chatstatus){
            //     $chatstatus = new CometchatStatus;
            //     $chatstatus->userid =  Yii::app()->user->id;
            //     $chatstatus->status = "available";
            //     $chatstatus->save();
            // }
        }
        // if(!Yii::app()->user->isGuest)
        // {
        //     $nameAdmin = Yii::app()->getModule('user')->user();
        //     $modelLevel = Level::model()->findByPk($nameAdmin->superuser);
        //     if($modelLevel->level_login != "1")
        //     {
        //         Yii::app()->session->clear();
        //         Yii::app()->session->destroy();
        //         $this->redirect(array('/user/login'));
        //         Yii::app()->end();
        //     }
        // }

        // if(Yii::app()->user->id != null){
        //       // var_dump(Yii::app()->request->cookies['token_login']); exit();
        //         $this->checkToken(Yii::app()->request->cookies['token_login']->value);
        // }
    }

    private function checkToken($token){

        $logoutid = User::model()->findByPk(Yii::app()->user->id);
        if($logoutid->avatar != $token){
            $this->gotoLogout();
        }
    }

    private function gotoLogout()
    {
        // if(Yii::app()->user->id){
        //     Helpers::lib()->getControllerActionId();
        // }
        $logoutid = User::model()->findByPk(Yii::app()->user->id);
        $logoutid->lastvisit_at = date("Y-m-d H:i:s",time()) ;
        $logoutid->online_status = '0';
        $logoutid->save(false);
        Yii::app()->user->logout();
        // $this->redirect(array('login'));
    }
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='//layouts/column1';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();

	 public static function DeleteAll( $menu = array() )
    {
        $Sum = 0;
        $countArray = count($menu);
        if($countArray != 0)
        {
            foreach ($menu as $key => $value) 
            {
                if(Yii::app()->user->checkAccess($value) === true)
                {
                    $Sum = $Sum+1; 
                }
            }
        }
        if($Sum >= 2) $check = true; else $check = false;

        return $check; 
    }

	public function listPageShowNonCss($model)
	{
        $list = CHtml::listData(Page::model()->findAll(), 'page_num', 'page_num');
        return CHtml::DropDownList('per_page', '', $list, array(
            'empty' => 'ค่าเริ่มต้น (10)',
             'class'=>'form-control',
            'data-style'=>'btn-default btn-small',
            'onchange'=>"$.updateGridView('".$model."-grid', 'per_page', this.value)"
        ));
	}



    public static function PButton( $menu = array() )
    {
        /*=============================================
        =             Check Permissions              =
        =============================================*/
        $sum = 0;
        $countArray = count($menu);
        $return = false;        
                
        if($countArray != 0)
        {
            foreach ($menu as $key => $value)
            {
                            $val = explode('.',$value);
                            $controller_name = $val[0];
                            $action_name = $val[1];
                            $check = AccessControl::check_access_action($controller_name,$action_name);
                            if($check){
                                $return = true;
                            }else{
                                $return = false;
                            }
            }
        }
        return $return;
    }
}