<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl; ?>/theme/assets/js/bootstrap-daterangepicker/jquery.datetimepicker.css">
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/theme/assets/js/bootstrap-daterangepicker/jquery.datetimepicker2.full.min.js"></script>
<div class="container-fluid">
    <!-- start row -->
    <div class="row">
        <div class="col-lg-12 set-height set-padding">
            <div id="section1">
                <div class="card">
                    <div class="card-header">
                        <h5 class="mb-1">จัดการคำถาม</h5>
                    </div>
                </div>
                <div class="card table-card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-4 mb-3">
                                <div class="form-group">
                                    <label for="">ปี พ.ศ. เริ่มต้น</label>
                                    <select class="form-control digits" id="year_start">
                                        <option selected="" disabled=""></option>
                                        <?php
                                            foreach($years as $year){
                                                echo '<option value="'.$year['year'].'">'.$year['year'].'</option>';
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4 mb-3">
                                <div class="form-group">
                                    <label for="">ปี พ.ศ. สิ้นสุด</label>
                                    <select class="form-control digits" id="year_end">
                                        <option selected="" disabled=""></option>
                                        <?php
                                            foreach($years as $year){
                                                echo '<option value="'.$year['year'].'">'.$year['year'].'</option>';
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4 mb-3">
                                <label for="">ช่วงเวลา เริ่มต้น</label>
                                <input class="form-control datetimepicker" type="text" id="date_start" value="" data-original-title="" title="">
                                <!-- <div class="date-picker">
                                                        <label for="validationDefault03">วัน/เดือน/ปี</label>
                                                        <div class="input-group">
                                                            <input class="datepicker-here form-control digits" type="text" data-language="th">
                                                        </div>
                                                        </div>
                                                    </div> -->
                            </div>
                            <div class="col-md-4 mb-3">
                                <label for="">ช่วงเวลา สิ้นสุด</label>
                                <input class="form-control datetimepicker" type="text" id="date_end" value="" data-original-title="" title="">
                                <!-- <div class="date-picker">
                                                        <label for="validationDefault03">วัน/เดือน/ปี</label>
                                                        <div class="input-group">
                                                            <input class="datepicker-here form-control digits" type="text" data-language="th">
                                                        </div>
                                                        </div>
                                                    </div> -->
                            </div>
                            <div class="col-md-4 mb-3">
                                <button class="btn btn-lg btn-search text-white" type="submit" onclick="SearchData();"><i
                                        class="icon-search"></i>&nbsp;ค้นหา</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-end">
                    <div class="col-md-3">
                        <a href="<?php echo Yii::app()->baseurl . '/admin/questionaire/survey/create'; ?>">
                            <div class="card o-hidden create-main">
                                <div class="b-r-4 card-body">
                                    <div class="media static-top-widget">
                                        <div class="align-self-center text-center"><i data-feather="plus"></i></div>
                                        <div class="media-body">
                                            <h6 class="mb-0 counter">เพิ่มคำถาม</h6><i class="icon-bg"
                                                data-feather="database"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="card table-card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-main" id="">
                                <!-- <table class="table table-striped table-main" id="basic-2"> -->
                                <thead>
                                    <tr>
                                        <th width="5%">ลำดับ</th>
                                        <th>ปีแบบสอบถาม</th>
                                        <th>แบบสอบถาม</th>
                                        <th>จัดฟอร์ม</th>
                                        <th width="25%">จัดการ</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i = 1;
                                        foreach($survey as $key => $row){
                                            echo '
                                            <tr>
                                                <td>'.$i++.'</td>
                                                <td>'.($row['year']+543).'</td>
                                                <td>'.$row['name'].'</td>
                                                <td>
                                                    <a href="'.Yii::app()->baseurl.'/admin/questionaire/survey/'.$row['id'].'/form">
                                                        <button class="btn btn-md btn-search text-white mt-0" type="submit">จัดฟอร์ม</button>
                                                    </a> 
                                                </td>
                                                <td>
                                                    <a type="button" class="btn btn-icon text-success" href="'.Yii::app()->baseurl.'/admin/questionaire/survey/'.$row['id'].'/edit"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit"><path d="M20 14.66V20a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V6a2 2 0 0 1 2-2h5.34"></path><polygon points="18 2 22 6 12 16 8 16 8 12 18 2"></polygon></svg></a>
                                                    <button type="button" class="btn btn-icon text-danger" onclick="sendDelete('.$row['id'].', \''.$row['name'].'\')"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path></svg></button>
                                                </td>
                                            </tr>
                                        ';
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="table-responsive-search" style="display: none;"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end row -->
    <div class="container position-fixed fixed-top pt-5" id="deleteForm" style="display:none;">
        <div class="card col-12 p-0">
            <div class="card-header">
                <h5>ลบข้อมูล</h5>
            </div>
            <div class="card-body">
                <h5 class="card-title">เมื่อข้อมูลถูกลบจะไม่สามารถย้อนคืนได้</h5>
                <div class="card-text">
                    <form method="POST" action="<?php echo Yii::app()->baseurl; ?>/admin/questionaire/survey/delete">
                        <label for="deleteKey">รหัสอ้างอิง</label>
                        <input id="deleteKey" name="key" class="form-control" value="" readonly/>
                        <label for="deleteValue">ข้อมูล</label>
                        <input id="deleteValue" name="value" class="form-control" value="" readonly/>
                        <button class="btn btn-danger btn-lg mt-2">ลบข้อมูล</button>
                        <button type="button" class="btn btn-light btn-lg mt-2" onclick='$("#deleteForm").hide();'>ยกเลิก</button>
                    </form>
                </div>
            </div>
        </div>

    </div>
</div>
<script type="text/javascript">
    $.datetimepicker.setLocale('th');
    $('.datetimepicker').datetimepicker({
       timepicker:false,
                format:'d-m-Y',  // กำหนดรูปแบบวันที่ ที่ใช้ เป็น 00-00-0000            
                lang:'th',  // แสดงภาษาไทย                     
               // yearOffset:543,  // ใช้ปี พ.ศ. บวก 543 เพิ่มเข้าไปในปี ค.ศ
                closeOnDateSelect:true,
                onSelectDate:function(dp,$input){
                var yearT=new Date(dp).getFullYear()-0;  
                var yearTH=yearT+543;
                var fulldate=$input.val();
                var fulldateTH=fulldate.replace(yearT,yearTH);
                $input.val(fulldateTH);
                 },
    });

    function SearchData(){

     var year_start = $("#year_start").val();
     var year_end = $("#year_end").val();
     var date_start = $("#date_start").val();
     var date_end = $("#date_end").val();
     if (year_start != "" && year_end != "" || date_start != "" && date_end) {

         $.ajax({
            url: "<?= $this->createUrl('/admin/SearchQuestionaire'); ?>", 
            type: "GET",
            data:  {
              year_start:year_start,
              year_end:year_end,
              date_start:date_start,
              date_end:date_end,
          },
          success: function(data){

            if (data) {
                $('.table-responsive').remove();
                $('.table-responsive-search').show();
                $('.table-responsive-search').html(data);
            }
       }                           
   });
     }else{
    
      alert("ป้อนข้อมูลให้ถูกต้องแล้วกดค้นหา");
      location.reload();
  }

}
$("#date_start").change(function () {
        var first = new Date($("#date_start").val());
        var current = new Date($(this).val());

        if (first.getTime() > current.getTime()) {
                alert("ไม่สามารถเลือกช่วงเวลาสิ้นสุดมากกว่าช่วงเวลาเริ่มต้นได้");
            $(this).val(null);
        }
        $("#year_start").val(null);
        $("#year_end").val(null);
    });

    $("#year_start").change(function () {
        $("#year_end").val(null);
        $("#date_start").val(null);
        $("#date_end").val(null);
    });
    $("#Year_end").change(function () {
        var first = $("#year_end").val();
        var current = $(this).val();
        if (first >= current && current != "") {
                alert("ไม่สามารถเลือกช่วงปีสิ้นสุดมากกว่าช่วงปีเริ่มต้นได้");
                   
            $(this).val(null);
        }
        $("#date_start").val(null);
        $("#date_end").val(null);
    });

</script>
