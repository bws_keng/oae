
        <div class="table-responsive">
            <table class="table table-striped table-main" id="">
                <!-- <table class="table table-striped table-main" id="basic-2"> -->
                    <thead>
                        <tr>
                            <th width="5%">ลำดับ</th>
                            <th width="5%">รหัสคำถาม</th>
                            <th>คำถาม</th>
                            <th>คำอธิบาย</th>
                            <th width="25%">จัดการ</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach($survey as $key => $row){
                            echo '
                            <tr>
                            <td>'.($key+1).'</td>
                            <td>'.$row['question_id'].'</td>
                            <td>'.$row['title'].'</td>
                            <td>'.$row['subtitle'].'</td>
                            <td>
                            <a class="btn btn-icon text-success" href="'.Yii::app()->baseurl.'/admin/questionaire/question/'.$row['question_id'].'/edit">
                            <i data-feather="edit"></i>
                            </a>
                            <button type="button" class="btn btn-icon text-danger" onclick="sendDelete('.$row['question_id'].', \''.$row['title'].'\')">
                            <i data-feather="trash"></i>
                            </button>
                            </td>
                            </tr>
                            ';
                        }
                        ?>
                    </tbody>
                </table>
            </div>
  <script type="text/javascript">
                           $(document).ready(function(){
                                   feather.replace();
                                });
                         </script>