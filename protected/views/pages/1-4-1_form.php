<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="description" content="Cuba admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, Cuba admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="pixelstrap">
    <title>หมวดที่ 4 รายจ่ายเงินสดเกษตรทางพืช </title>
    <?php include 'theme/assets/include/inc-head.php'; ?>
</head>

<body data-spy="scroll" data-target="#myScrollspy">
    <div class="tap-top"><i data-feather="chevrons-up"></i></div>

    <div class="page-wrapper compact-wrapper" id="pageWrapper">

        <?php include 'theme/assets/include/inc-header.php'; ?>

        <div class="page-body-wrapper horizontal-menu">
            <?php include 'theme/assets/include/inc-menuleft.php'; ?>

 
                <div class="container-fluid">

                   <!-- <div id="myScrollspy" class="custom-scrollbar nav sidenav">
                        <div class="sidenav-content">
                            <a href="#section1" data-original-title="" title="" class="nav-link active">ข้อมูลเบื้องต้น</a>
                            <a href="#section2" data-original-title="" title="" class="nav-link ">รวมค่าใช้จ่ายการจ้างเเรงงานที่จ่ายจริงทั้งหมด</a>
                            <a href="#section3" data-original-title="" title="" class="nav-link">รวมค่าใช้จ่ายด้านวัสดุอุปกรณ์ทางการเกษตรที่จ่ายจริงทั้งหมด</a>
                        </div>
                    </div> -->

                    <!-- start row -->
                    <div class="row">
                        <div class="col-lg-12 set-height set-padding">
                            <div id="section1">
                                <div class="card">
                                    <div class="card-header">
                                        <h5 class="mb-1">หมวดที่ 4 รายจ่ายเงินสดเกษตร<span class="line-danger">ทางพืช</span> <span>(ปี พ.ศ.2562 - พ.ศ.2563)</span></h5>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-body">

                                        <form>
                                            <div class="row">
                                                <div class="col-md-2 mb-3">
                                                    <label for="">จังหวัด</label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-2 mb-3">
                                                    <label for="">อำเภอ</label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-2 mb-3">
                                                    <label for="">ตำบล</label>
                                                    <div class="input-group">
                                                        <input class="form-control" id="" type="text" placeholder="" aria-describedby="" required="" data-original-title="" title="">
                                                    </div>
                                                </div>
                                                <div class="col-md-2 mb-3">
                                                    <label for="">หมู่ที่</label>
                                                    <div class="input-group">
                                                        <input class="form-control" id="" type="text" placeholder="" aria-describedby="" required="" data-original-title="" title="">
                                                    </div>
                                                </div>
                                                <div class="col-md-2 mb-3">
                                                    <label for="">พวกที่</label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-2 mb-3">
                                                    <label for="">ตัวอย่างที่</label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                            </div>
                                        </form>

                                    </div>
                                </div>
                            </div>

                            <div class="card">
                                <div class="card-body">

                                    <form>
                                        <div class="row">
                                            <div class="col-md-6 mb-3">
                                                <label for="">รหัสสินค้า <small class="text-danger">(1)</small></label>
                                                <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                            </div>
                                            <div class="col-md-6 mb-3">
                                                <label for="">ชื่อพืช <small class="text-danger">(2)</small></label>
                                                <input class="form-control" disabled id="" type="text" placeholder="" required="" data-original-title="" title="">
                                            </div>
                                        </div>
                                        <hr>

                                        <div id="section2">
                                            <div class="row">
                                                <div class="col-12">
                                                    <h6 class="text-head mb-3"><i class="icon-pencil-alt"></i> รวมค่าใช้จ่ายการจ้างแรงงานที่จ่ายจริงทั้งหมด (บาท)</h6>
                                                    <h6 class="text-under">การปลูก</h6>
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">เตรียมดินแปลงปลูก <small class="text-danger">(3)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">ผลิตต้นกล้า <small class="text-danger">(4)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">ปลูก/ปลูกซ่อม (ถ้ามี)<small class="text-danger">(5)</small></label>
                                                    <div class="input-group">
                                                        <input class="form-control" id="" type="text" placeholder="" aria-describedby="" required="" data-original-title="" title="">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-12">
                                                    <h6 class="text-under">การดูแล</h6>
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">พรวนดิน/ถอนหญ้า/ดายหญ้า <small class="text-danger">(6)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">ให้น้ำ <small class="text-danger">(7)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-12">
                                                    <h6 class="text-under">การใส่ปุ๋ย/พ่นยา</h6>
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">ใส่ปุ๋ย <small class="text-danger">(ปุ๋ยเคมีและปุ๋ยอินทรีย์) (8)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                     <div class="row"><label for="">พ่นยา <small class="text-danger"> (ยาปราบศัตรูพืช/ยาคุม/ยาฆ่า ฮอร์โมน ฯลฯ) (9)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                            </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-12">
                                                    <h6 class="text-under">การเก็บเกี่ยว</h6>
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">เก็บเกี่ยว <small class="text-danger">(ปุ๋ยเคมีและปุ๋ยอินทรีย์) (10)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">กำ/มัด/หอบ ฯลฯ <small class="text-danger">(11)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">นวด/สี/ฝัด ฯลฯ <small class="text-danger">(12)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-12">
                                                    <h6 class="text-under">การขาย</h6>
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">ขนในฟาร์ม <small class="text-danger">(13)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">ขนไปขายนอกฟาร์ม <small class="text-danger">(14)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-4 mb-3">
                                                    <label for="">ค่าอาหารแลกเปลี่ยนแรงงาน <small class="text-danger">(15)</small></label><br><br>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">ค่าจ้างแรงงานอื่นๆ <small class="text-danger">(16)</small></label> <br><br>                                                 
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">ค่าจ้างแรงงานเหมารวม <small class="text-danger">(17) </small><br> 
                                                    <small class="text-danger">(กรณีแบ่งแยกกิจกรรมที่ 3-16 ไม่ได้) </small></label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                        </div>
                                        <hr>

                                        <div id="section3">

                                            <div class="row">
                                                <div class="col-12">
                                                    <h6 class="text-head mb-3"><i class="icon-pencil-alt"></i> รวมค่าใช้จ่ายด้านวัสดุอุปกรณ์ทางการเกษตรที่จ่ายจริงทั้งหมด (บาท)</h6>
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">ค่าพันธุ์ <small class="text-danger">(18)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-12">
                                                    <h6 class="text-under">ค่าสาร</h6>
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">ค่าสารปราบศัตรูพืช <small class="text-danger">(19)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">ค่าสารปราบวัชพืช <small class="text-danger">(20)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">ค่าสารป้องกันโรค/รักษาโรค <small class="text-danger">(21)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-4 mb-3">
                                                    <label for="">ค่าฮอร์โมน <small class="text-danger">(22)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">ค่าน้ำหมักชีวภาพ <small class="text-danger">(23)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">ค่าสารอื่นๆ (เช่น ปูนขาว) <small class="text-danger">(24)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-12">
                                                    <h6 class="text-under">ค่าปุ๋ย</h6>
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">ค่าปุ๋ยเคมี <small class="text-danger">(25)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">ค่าปุ๋ยอินทรีย์ <small class="text-danger">(26)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-12">
                                                    <h6 class="text-under">ค่าน้ำมัน/ค่าน้ำ/ค่าไฟ</h6>
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">ค่าน้ำมันเชื้อเพลิงและน้ำมันหล่อลื่น <small class="text-danger">(27)</small></label><br><br>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">ค่าน้ำการเกษตร <small class="text-danger">(28)</small></label><br><br>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">ค่าไฟฟ้าการเกษตร <small class="text-danger">(29)</small><br>
                                                    <small class="text-danger">(รวมค่าไฟฟ้าที่ใช้สูบน้ำ/ให้แสง)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-4 mb-3">
                                                    <label for="">ค่าวัสดุสิ้นเปลือง <small class="text-danger">(30)</small><br>
                                                    <small class="text-danger">(เช่น ฟาง ถุง เชือก ไม้ไผ่ ผ้าใบ)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">ค่าอุปกรณ์ <small class="text-danger">(31)</small><br>
                                                    <small class="text-danger">(เช่น สายยาง ท่อน้ำ ถังน้ำ)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">ค่าวัสดุอุปกรณ์เหมารวม <small class="text-danger">(32)</small><br>
                                                    <small class="text-danger">(กรณีแบ่งแยกกิจกรรมที่ 18-31 ไม่ได้)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                        </div>

                                    </form>

                                </div>
                            </div>

                            <div class="row text-center group-submit">
                                <button type="button" class="btn btn-primary btn-lg">บันทึกข้อมูล</button>
                                <button type="button" class="btn btn-outline-secondary btn-lg">ยกเลิก</button>
                            </div>
                        </div>
                    </div>
                    <!-- end row -->

                </div>


 