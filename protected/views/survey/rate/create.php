<form action="<?php echo Yii::app()->baseUrl . "/survey/" . $_GET['id']. "/rate/". $_GET['Questionnaire'] ."/insert" ?>" method="POST">
    <div class="container-fluid">
        <!-- start row -->
        <div class="row">
            <div class="col-lg-12 set-height set-padding">
                <div id="section1">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="mb-1">
                                <?php echo $title; ?>
                            </h5>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">

                                <div class="row">
                                    <div class="col-md-2 mb-3">
                                        <label for="">จังหวัด</label>
                                        <input class="form-control" name="maindata-province" type="text" placeholder=""
                                            required="" title="" readonly>
                                    </div>
                                    <div class="col-md-2 mb-3">
                                        <label for="">อำเภอ</label>
                                        <input class="form-control" name="maindata-district" type="text" placeholder=""
                                            required="" title="" readonly>
                                    </div>
                                    <div class="col-md-2 mb-3">
                                        <label for="">ตำบล</label>
                                        <div class="input-group">
                                            <input class="form-control" name="maindata-sub-district" type="text"
                                                placeholder="" aria-describedby="" required="" title="" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-2 mb-3">
                                        <label for="">หมู่ที่</label>
                                        <div class="input-group">
                                            <input class="form-control" name="maindata-village-no" type="text"
                                                placeholder="" aria-describedby="" required="" title="" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-2 mb-3">
                                        <label for="">พวกที่</label>
                                        <input class="form-control" name="maindata-group" type="text" placeholder=""
                                            required="" title="" readonly>
                                    </div>
                                    <div class="col-md-2 mb-3">
                                        <label for="">ตัวอย่างที่</label>
                                        <input class="form-control" name="maindata-sample" type="text" placeholder=""
                                            required="" title="" readonly>
                                    </div>
                                </div>

                        </div>
                    </div>
                </div>

                <div id="section2">
                    <?php 

                        if(isset($answer)){
                            ?>
                            <h5 class="mb-4"> &nbsp;
                                <button class="btn btn-lg btn-secondary btn-note float-right mb-2" type="button" data-toggle="modal" data-target=".bd-example-modal-lg"><i class="fa fa-list"></i> &nbsp;รายการ</button>
                                <div class="modal fade bd-example-modal-lg modal-main" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h6 class="modal-title" id="myLargeModalLabel">รายการ</h6>
                                                <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="row">
                                                    <?php
                                                    foreach ($answer as $key => $value) {
                                                        ?>
                                                            <div class="col-md-6">
                                                                <div class="card">
                                                                    <div class="card-header">
                                                                        <label for="">(<?php echo ($value['readable_id']); ?>) <?php echo $value['title']; ?></label>
                                                                    </div>
                                                                    <div class="card-body">
                                                                        <div class="row">
                                                                            <?php
                                                                                $answer_bullet = json_decode($value['description_json']);
                                                                                foreach ($answer_bullet as $item_key => $item) {
                                                                                    echo '
                                                                                        <div class="col-md-12">
                                                                                            <small for="">'.$item.'</small>
                                                                                        </div>
                                                                                    ';
                                                                                }
                                                                            ?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        <?php
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </h5>
                            <?php
                        }
                    ?>
                    <style type="text/css">
                        th {
                          text-align: left;
                        }
                        td {
                          text-align: left;
                        }
                    </style>
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <?php
                                $criteria = new CDbCriteria();
                                $criteria->compare('question_group_id',$_GET['id']);
                                $QuestionGroup = QuestionGroup::model()->find($criteria);
                                if ($QuestionGroup) {
                                    echo $QuestionGroup->description;
                                }
                                ?>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="table-responsive">
                                    <table class="table table-striped table-main" id="">
                                        <thead>
                                            <tr class="rate-th">
                                                <th width="85%">คำถาม</th>
                                                <th width="15%">คะแนน</th>
                                            </tr>
                                        </thead>
                                        <tbody><?php
                                        $criteria = new CDbCriteria();
                                        $criteria->compare('question_group_id',$_GET['id']);
                                        $criteria->order = 'sortorder ASC';
                                        $Question_model = Question::model()->findAll($criteria);
                                        if ($Question_model) {
                                            foreach ($Question_model as $key => $value) { ?>
                                                <tr class="rate-title">
                                                    <td colspan="10" ><?php echo $value->question ?></td>  
                                                </tr>
                                                <?php 
                                                $criteria = new CDbCriteria();
                                                $criteria->compare('question_id',$value->question_id);
                                                $QuestionSub = QuestionSub::model()->findAll($criteria);
                                                foreach ($QuestionSub as $keySub => $valueSub) { ?>
                                                    <tr class="rate-title">
                                                        <td ><?php echo $valueSub->question_detail ?></td> 
                                                        <td ><red>คะแนน 1-<?=$valueSub->range?></red><input class="form-control" type="text" name="QuestionSub-<?=$valueSub->id?>" onchange="maxNumber(<?=$valueSub->range?>,<?=$valueSub->id?>)" required ></td> 
                                                    </tr>
                                                <?php 
                                                } 
                                            }
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>

               <!--  <div class="form-check" style="text-align: center;">
                    <?php $return = isset($_GET['return']) ? $_GET['return'] : "false"; ?>
                    <input type="hidden" name="return" value="<?=$return?>">
                  <input
                  class="form-check-input"
                  type="checkbox"
                  id="flexCheckChecked"
                  name = "checkresend"
                  />
                  <label class="form-check-label" for="flexCheckChecked">
                    บันทึกข้อมูลและเพิ่มอีกครั้ง
                </label>
            </div> -->

                <div class="row text-center group-submit">
                    <input type="submit" type="button" class="btn btn-primary btn-lg" value="บันทึกข้อมูล" />
                    <button type="button" class="btn btn-outline-secondary btn-lg" onclick="window.history.go(-1); return false;">ยกเลิก</button>
                </div>
            </div>
        </div>
        <!-- end row -->

    </div>
</form>
<script type="text/javascript">
    function maxNumber(number,id){

       var val = $("input[name='QuestionSub-"+id+"']").val();
       if (val > number) {
         alert("กรอกคะแนนเกินจำนวนที่กำหนด");
         $("input[name='QuestionSub-"+id+"']").val(null);
       }
       if (val == 0) {
         alert("กรอกคะแนนน้อยกว่าที่กำหนด");
         $("input[name='QuestionSub-"+id+"']").val(null);
       }
    }

  $(document).ready(function() {
        var id = <?php echo $_GET['Questionnaire'] ?>;
        if (id != null || id != 'undefined') {
            $.ajax({
                url: "<?= $this->createUrl('/survey/SearchQuestionnaire'); ?>", 
                type: "POST",
                data:  {
                    id:id
                },
                success: function(data){
                   if (data) {
                    var obj = JSON.parse(data);
                    $("input[name='maindata-province']").val(obj[2]);
                    $("input[name='maindata-district']").val(obj[3]);
                    $("input[name='maindata-sub-district']").val(obj[4]);
                    $("input[name='maindata-village-no']").val(obj[5]);
                    $("input[name='maindata-group']").val(obj[6]);
                    $("input[name='maindata-sample']").val(obj[7]);
                   }
                },
                error: function(data)
                {
                alert('ERROR');
                }  
            });
        }                             
    });

  $(document).ready(function() {
    <?php if (isset($_GET["checkresend"])) { ?>
        if (<?=$_GET["checkresend"]?> == true) {
            // alert("บันทึกข้อมูลสำเร็จ");
            $("#regis-user-condition").modal();
        }
    <?php } ?>

});
</script>

<div class="modal" id="regis-user-condition" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">บันทึกสำเร็จ</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>กดปิดเพื่อเพิ่มข้อมูลอีกครั้ง</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิด</button>
      </div>
    </div>
  </div>
</div>