<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="description" content="Cuba admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, Cuba admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="pixelstrap">
    <title>หมวดที่ 9 ทรัพย์สินของครัวเรือนเกษตร</title>
    <?php include 'theme/assets/include/inc-head.php'; ?>
</head>

<body data-spy="scroll" data-target="#myScrollspy">
    <div class="tap-top"><i data-feather="chevrons-up"></i></div>

    <div class="page-wrapper compact-wrapper" id="pageWrapper">

        <?php include 'theme/assets/include/inc-header.php'; ?>

        <div class="page-body-wrapper horizontal-menu">
            <?php include 'theme/assets/include/inc-menuleft.php'; ?>

 
                <div class="container-fluid">
                    
                    <div class="row">
                        <div class="col-lg-12 set-height set-padding">
                            <div id="section1">
                                <div class="card">
                                    <div class="card-header">
                                        <h5 class="mb-1">หมวดที่ 9 ทรัพย์สินของครัวเรือนเกษตร (ปี พ.ศ.2562 - พ.ศ.2563)</span></h5>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-body">

                                        <form>
                                            <div class="row">
                                                <div class="col-md-2 mb-3">
                                                    <label for="">จังหวัด</label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-2 mb-3">
                                                    <label for="">อำเภอ</label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-2 mb-3">
                                                    <label for="">ตำบล</label>
                                                    <div class="input-group">
                                                        <input class="form-control" id="" type="text" placeholder="" aria-describedby="" required="" data-original-title="" title="">
                                                    </div>
                                                </div>
                                                <div class="col-md-2 mb-3">
                                                    <label for="">หมู่ที่</label>
                                                    <div class="input-group">
                                                        <input class="form-control" id="" type="text" placeholder="" aria-describedby="" required="" data-original-title="" title="">
                                                    </div>
                                                </div>
                                                <div class="col-md-2 mb-3">
                                                    <label for="">พวกที่</label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-2 mb-3">
                                                    <label for="">ตัวอย่างที่</label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                            </div>
                                        </form>

                                    </div>
                                </div>
                            </div>

                            <h5 class="mb-3">รายการทรัพย์สิน</h5>
                            <div class="card">
                                <div class="card-body">

                                    <form>
                                        <div id="section2">
                                            <div class="row">
                                                <div class="col-12">
                                                    <h6 class="text-head mb-3"><i class="icon-pencil-alt"></i> ทรัพย์สินคงที่ในการเกษตรไม่รวมมูลค่าที่ดิน</h6>
                                                </div>
                                                <div class="col-md-8 mb-3">
                                                    <label for="">มูลค่าของต้นไม้ สำหรับ ไม้ผล ไม้ยืนต้น ไม้ดอก ไม้ประดับ<small class="text-danger"> (รหัส 1)</small></label>
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">มูลค่าทรัพย์สินรวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-8 mb-3">
                                                    <label for="">โรงเพาะพันธุ์พืช โรงเรือนเก็บพืชผล ยุ้งฉางของพืช<small class="text-danger"> (รหัส 2)</small></label>
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">มูลค่าทรัพย์สินรวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-8 mb-3">
                                                    <label for="">โรงเรือนเลี้ยงสัตว์/คอกสัตว์<small class="text-danger"> (รหัส 3)</small></label>
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">มูลค่าทรัพย์สินรวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-8 mb-3">
                                                    <label for="">บ่อเพาะพันธุ์/บ่อเลี้ยงสัตว์น้ำ (ที่ก่อสร้างขึ้น)<small class="text-danger"> (รหัส 4)</small></label>
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">มูลค่าทรัพย์สินรวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-8 mb-3">
                                                    <label for="">บ่อบาดาล/สระน้ำเพื่อการเกษตร (ที่ก่อสร้างขึ้น)<small class="text-danger"> (รหัส 5)</small></label>
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">มูลค่าทรัพย์สินรวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-8 mb-3">
                                                    <label for="">ทรัพย์สินคงที่ในการเกษตรอื่นๆ เช่น ห้างนา...<small class="text-danger"> (รหัส 6)</small></label>
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">มูลค่าทรัพย์สินรวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>
                                        </div>
                                        <hr>

                                        <div id="section2">
                                            <div class="row">
                                                <div class="col-12">
                                                    <h6 class="text-head mb-3"><i class="icon-pencil-alt"></i> เครื่องจักรกลการเกษตร</h6>
                                                </div>
                                                <div class="col-md-12 mb-3">
                                                    <label for="">รถไถ 4 ล้อ/รถไถเดินตาม รวมอุปกรณ์ <small class="text-danger"> (รหัส 7)</small></label>
                                                </div>
                                                <div class="col-md-8 mb-3">
                                                    <label for="">จำนวน <small class="text-danger">(ชิ้น)</small></label>
                                                    <input class="form-control col-6" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">มูลค่าทรัพย์สินรวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12 mb-3">
                                                    <label for="">รถแทรกเตอร์ รวมอุปกรณ์<small class="text-danger"> (รหัส 8)</small></label>
                                                </div>
                                                <div class="col-md-8 mb-3">
                                                    <label for="">จำนวน <small class="text-danger">(ชิ้น)</small></label>
                                                    <input class="form-control col-6" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">มูลค่าทรัพย์สินรวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12 mb-3">
                                                    <label for="">รถอีแต๋น รวมอุปกรณ์<small class="text-danger"> (รหัส 9)</small></label>
                                                </div>
                                                <div class="col-md-8 mb-3">
                                                    <label for="">จำนวน <small class="text-danger">(ชิ้น)</small></label>
                                                    <input class="form-control col-6" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">มูลค่าทรัพย์สินรวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12 mb-3">
                                                    <label for="">รถไถ/รถคราด/รถขลุก รวมอุปกรณ์ <small class="text-danger"> (รหัส 10)</small></label>
                                                </div>
                                                <div class="col-md-8 mb-3">
                                                    <label for="">จำนวน <small class="text-danger">(ชิ้น)</small></label>
                                                    <input class="form-control col-6" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">มูลค่าทรัพย์สินรวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12 mb-3">
                                                    <label for="">รถ-เครื่องปลูก แบบโรย/หยอด/พ่นเมล็ด รวมอุปกรณ์ <small class="text-danger"> (รหัส 11)</small></label>
                                                </div>
                                                <div class="col-md-8 mb-3">
                                                    <label for="">จำนวน <small class="text-danger">(ชิ้น)</small></label>
                                                    <input class="form-control col-6" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">มูลค่าทรัพย์สินรวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12 mb-3">
                                                    <label for="">รถ-เครื่องเกี่ยว/นวด รวมอุปกรณ์ <small class="text-danger"> (รหัส 12)</small></label>
                                                </div>
                                                <div class="col-md-8 mb-3">
                                                    <label for="">จำนวน <small class="text-danger">(ชิ้น)</small></label>
                                                    <input class="form-control col-6" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">มูลค่าทรัพย์สินรวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12 mb-3">
                                                    <label for="">เครื่องสี/ฝัด/นวด รวมอุปกรณ์ <small class="text-danger"> (รหัส 13)</small></label>
                                                </div>
                                                <div class="col-md-8 mb-3">
                                                    <label for="">จำนวน <small class="text-danger">(ชิ้น)</small></label>
                                                    <input class="form-control col-6" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">มูลค่าทรัพย์สินรวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12 mb-3">
                                                    <label for="">เครื่องพ่นยา/พ่นปุ๋ย รวมอุปกรณ์ <small class="text-danger"> (รหัส 14)</small></label>
                                                </div>
                                                <div class="col-md-8 mb-3">
                                                    <label for="">จำนวน <small class="text-danger">(ชิ้น)</small></label>
                                                    <input class="form-control col-6" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">มูลค่าทรัพย์สินรวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12 mb-3">
                                                    <label for="">เครื่องสูบน้ำ/ปั๊มน้ำ รวมอุปกรณ์ เช่น สายยาง ท่อส่งน้ำ และอื่นๆ <small class="text-danger"> (รหัส 15)</small></label>
                                                </div>
                                                <div class="col-md-8 mb-3">
                                                    <label for="">จำนวน <small class="text-danger">(ชิ้น)</small></label>
                                                    <input class="form-control col-6" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">มูลค่าทรัพย์สินรวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12 mb-3">
                                                    <label for="">เครื่อง/ระหัดวิดน้ำ กังหันลม รวมอุปกรณ์ <small class="text-danger"> (รหัส 16)</small></label>
                                                </div>
                                                <div class="col-md-8 mb-3">
                                                    <label for="">จำนวน <small class="text-danger">(ชิ้น)</small></label>
                                                    <input class="form-control col-6" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">มูลค่าทรัพย์สินรวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12 mb-3">
                                                    <label for="">[สัตว์] เครื่องรีดนม รวมอุปกรณ์ <small class="text-danger"> (รหัส 17)</small></label>
                                                </div>
                                                <div class="col-md-8 mb-3">
                                                    <label for="">จำนวน <small class="text-danger">(ชิ้น)</small></label>
                                                    <input class="form-control col-6" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">มูลค่าทรัพย์สินรวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12 mb-3">
                                                    <label for="">[สัตว์] เครื่องบดอาหารสัตว์ รวมอุปกรณ์ <small class="text-danger"> (รหัส 18)</small></label>
                                                </div>
                                                <div class="col-md-8 mb-3">
                                                    <label for="">จำนวน <small class="text-danger">(ชิ้น)</small></label>
                                                    <input class="form-control col-6" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">มูลค่าทรัพย์สินรวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12 mb-3">
                                                    <label for="">[สัตว์] เครื่องให้อาหาร/ให้น้ำของสัตว์ รวมอุปกรณ์ <small class="text-danger"> (รหัส 19)</small></label>
                                                </div>
                                                <div class="col-md-8 mb-3">
                                                    <label for="">จำนวน <small class="text-danger">(ชิ้น)</small></label>
                                                    <input class="form-control col-6" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">มูลค่าทรัพย์สินรวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12 mb-3">
                                                    <label for="">เครื่องจักรกลการเกษตรอื่นๆ เช่น เครื่องตัดหญ้า <small class="text-danger"> (รหัส 20)</small></label>
                                                </div>
                                                <div class="col-md-8 mb-3">
                                                    <label for="">จำนวน <small class="text-danger">(ชิ้น)</small></label>
                                                    <input class="form-control col-6" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">มูลค่าทรัพย์สินรวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>
                                            <hr>

                                            <div id="section2">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <h6 class="text-head mb-3"><i class="icon-pencil-alt"></i> ทรัพย์สินหมุนเวียนในการเกษตร</h6>
                                                    </div>
                                                    <div class="col-md-8 mb-3">
                                                        <label class="text-danger" for="">เงินสดหมุนเวียน/เงินออมที่มีไว้เพื่อทำการเกษตรของครัวเรือนที่เก็บไว้กับตัว ในธนาคาร หรือในสถาบันการเงินอื่นๆ<small class="text-danger"> (รหัส 21)</small></label>
                                                    </div>
                                                    <div class="col-md-4 mb-3">
                                                        <label for="">มูลค่าทรัพย์สินรวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                        <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-8 mb-3">
                                                        <label class="text-danger" for="">มูลค่าปัจจัยการผลิตคงเหลือ เช่น ปุ๋ย ยา อาหารสัตว์
                                                            และอื่นๆ<small class="text-danger"> (รหัส 22)</small></label>
                                                    </div>
                                                    <div class="col-md-4 mb-3">
                                                        <label for="">มูลค่าทรัพย์สินรวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                        <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>

                                            <div id="section2">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <h6 class="text-head mb-3"><i class="icon-pencil-alt"></i> อุปกรณ์ทางการเกษตร</h6>
                                                    </div>
                                                    <div class="col-md-8 mb-3">
                                                        <label for="">อุปกรณ์การเกษตร เช่น จอบ เสียม พลั่ว และอื่นๆ<small class="text-danger"> (รหัส 23)</small></label>
                                                    </div>
                                                    <div class="col-md-4 mb-3">
                                                        <label for="">มูลค่าทรัพย์สินรวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                        <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-8 mb-3">
                                                        <label for="">อุปกรณ์การกรีดยาง/ทำแผ่นยาง<small class="text-danger"> (รหัส 24)</small></label>
                                                    </div>
                                                    <div class="col-md-4 mb-3">
                                                        <label for="">มูลค่าทรัพย์สินรวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                        <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-8 mb-3">
                                                        <label for="">[สัตว์] อุปกรณ์การเลี้ยงสัตว์-สัตว์น้ำ <small class="text-danger"> (รหัส 25)</small></label>
                                                    </div>
                                                    <div class="col-md-4 mb-3">
                                                        <label for="">มูลค่าทรัพย์สินรวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                        <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-8 mb-3">
                                                        <label for="">แทงค์น้ำ รวมอุปกรณ์ <small class="text-danger"> (รหัส 26)</small></label>
                                                    </div>
                                                    <div class="col-md-4 mb-3">
                                                        <label for="">มูลค่าทรัพย์สินรวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                        <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-8 mb-3">
                                                        <label for="">อุปกรณ์ทางการเกษตรอื่นๆ <small class="text-danger">(รหัส 27)</small></label>
                                                        <input class="form-control col-6" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                    </div>
                                                    <div class="col-md-4 mb-3">
                                                        <label for="">มูลค่าทรัพย์สินรวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                        <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>

                                            <div id="section2">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <h6 class="text-head mb-3"><i class="icon-pencil-alt"></i> ทรัพย์สินคงที่นอกการเกษตร</h6>
                                                    </div>
                                                    <div class="col-md-8 mb-3">
                                                        <label for="">บ้านที่อยู่อาศัยรวมรั้ว <u class="text-danger">ไม่รวมมูลค่าที่ดิน</u><small class="text-danger"> (รหัส 28)</small></label>
                                                    </div>
                                                    <div class="col-md-4 mb-3">
                                                        <label for="">มูลค่าทรัพย์สินรวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                        <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-8 mb-3">
                                                        <label for="">โรงเก็บของ/ที่เก็บของอื่นๆ <u class="text-danger">ไม่รวมมูลค่าที่ดิน</u><small class="text-danger"> (รหัส 29)</small></label>
                                                    </div>
                                                    <div class="col-md-4 mb-3">
                                                        <label for="">มูลค่าทรัพย์สินรวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                        <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-8 mb-3">
                                                        <label for="">อาคาร/สิ่งปลูกสร้างที่ใช้ประโยชน์นอกการเกษตรอื่นๆ
                                                            <u class="text-danger">ไม่รวมมูลค่าที่ดิน</u> <small class="text-danger"> (รหัส 30)</small></label>
                                                    </div>
                                                    <div class="col-md-4 mb-3">
                                                        <label for="">มูลค่าทรัพย์สินรวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                        <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>

                                            <div id="section2">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <h6 class="text-head mb-3"><i class="icon-pencil-alt"></i> ทรัพย์สินนอกการเกษตร</h6>
                                                    </div>
                                                    <div class="col-md-8 mb-3">
                                                        <label for="">รถจักรยานยนต์<small class="text-danger"> (รหัส 31)</small></label>
                                                    </div>
                                                    <div class="col-md-4 mb-3">
                                                        <label for="">มูลค่าทรัพย์สินรวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                        <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-8 mb-3">
                                                        <label for="">รถยนต์/รถกระบะ/รถบรรทุก<small class="text-danger"> (รหัส 32)</small></label>
                                                    </div>
                                                    <div class="col-md-4 mb-3">
                                                        <label for="">มูลค่าทรัพย์สินรวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                        <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-8 mb-3">
                                                        <label for="">รถจักรยาน 2 ล้อ/รถเข็น/พาหนะขนาดเล็กอื่นๆ<small class="text-danger"> (รหัส 33)</small></label>
                                                    </div>
                                                    <div class="col-md-4 mb-3">
                                                        <label for="">มูลค่าทรัพย์สินรวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                        <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-8 mb-3">
                                                        <label for="">เรือยนต์/เรือพาย<small class="text-danger"> (รหัส 34)</small></label>
                                                    </div>
                                                    <div class="col-md-4 mb-3">
                                                        <label for="">มูลค่าทรัพย์สินรวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                        <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-8 mb-12">
                                                        <label for="">เครื่องใช้ไฟฟ้าในบ้าน เช่น พัดลม ทีวี เครื่องเล่นวีดีโอ/วีดีโอ เครื่องซักผ้า เครื่องทำน้ำอุ่น เครื่องดูดฝุ่น เครื่องฟอกอากาศ และอื่นๆ<small class="text-danger"> (รหัส 35)</small> </label>
                                                    </div>
                                                    <div class="col-md-4 mb-3">
                                                        <label for="">มูลค่าทรัพย์สินรวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                        <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-8 mb-12">
                                                        <label for="">เครื่องใช้ไฟฟ้าในครัว เช่น ตู้เย็น เตาแก๊ส เตาไฟฟ้า เครื่องปั่น/ผสมอาหาร และอื่นๆ<small class="text-danger"> (รหัส 36)</small></label>
                                                    </div>
                                                    <div class="col-md-4 mb-3">
                                                        <label for="">มูลค่าทรัพย์สินรวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                        <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-8 mb-3">
                                                        <label for="">โทรศัพท์บ้าน/โทรศัพท์มือถือ/อุปกรณ์สื่อสารอื่นๆ <small class="text-danger"> (รหัส 37)</small></label>
                                                    </div>
                                                    <div class="col-md-4 mb-3">
                                                        <label for="">มูลค่าทรัพย์สินรวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                        <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-8 mb-3">
                                                        <label for="">คอมพิวเตอร์/โน๊ตบุ๊ค/แท็ปเล็ต/อุปกรณ์อิเล็กทรอนิกส์อื่นๆ <small class="text-danger"> (รหัส 38)</small></label>
                                                    </div>
                                                    <div class="col-md-4 mb-3">
                                                        <label for="">มูลค่าทรัพย์สินรวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                        <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-8 mb-3">
                                                        <label for="">เครื่องปั่นไฟในบ้าน <small class="text-danger"> (รหัส 39)</small></label>
                                                    </div>
                                                    <div class="col-md-4 mb-3">
                                                        <label for="">มูลค่าทรัพย์สินรวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                        <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-8 mb-3">
                                                        <label for="">เครื่องปั๊มน้ำ/สูบน้ำในบ้าน <small class="text-danger"> (รหัส 40)</small></label>
                                                    </div>
                                                    <div class="col-md-4 mb-3">
                                                        <label for="">มูลค่าทรัพย์สินรวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                        <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-8 mb-3">
                                                        <label for="">จักรเย็บผ้า/อุปกรณ์เพื่อการทำหัตถกรรมในครัวเรือน <small class="text-danger"> (รหัส 41)</small></label>
                                                    </div>
                                                    <div class="col-md-4 mb-3">
                                                        <label for="">มูลค่าทรัพย์สินรวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                        <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-8 mb-3">
                                                        <label for="">เฟอร์นิเจอร์/เครื่องเรือนในบ้าน <small class="text-danger"> (รหัส 42)</small></label>
                                                    </div>
                                                    <div class="col-md-4 mb-3">
                                                        <label for="">มูลค่าทรัพย์สินรวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                        <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-8 mb-3">
                                                        <label for="">เครื่องมือ/เครื่องใช้ในบ้าน <small class="text-danger"> (รหัส 43)</small></label>
                                                    </div>
                                                    <div class="col-md-4 mb-3">
                                                        <label for="">มูลค่าทรัพย์สินรวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                        <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-8 mb-3">
                                                        <label for="">ทรัพย์สินนอกการเกษตรอื่นๆ <small class="text-danger"> (รหัส 44)</small></label>
                                                    </div>
                                                    <div class="col-md-4 mb-3">
                                                        <label for="">มูลค่าทรัพย์สินรวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                        <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="row text-center group-submit">
                                <button type="button" class="btn btn-primary btn-lg">บันทึกข้อมูล</button>
                                <button type="button" class="btn btn-outline-secondary btn-lg">ยกเลิก</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php include 'theme/assets/include/inc-footer.php'; ?>
        </div>
    </div>
    <?php include 'theme/assets/include/inc-script.php'; ?>
</body>

</html>