<?php

/**
 * This is the model class for table "survey".
 *
 * The followings are the available columns in table 'survey':
 * @property integer $survey
 * @property string $timestamp
 */
class SurveyMap extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'oae-survey.survey_map';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('survey,question,size,order', 'numerical', 'integerOnly'=>true),
			array('survey,question,size,order', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('survey,question,size,order', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'question_table' => array(self::BELONGS_TO, 'Question', 'question'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'survey' => 'survey',
			'question' => 'question',
			'size' => 'size',
			'order' => 'order',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with timestamps from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('survey',$this->survey);
		$criteria->compare('question',$this->question);
		$criteria->compare('size',$this->size);
		$criteria->compare('order',$this->order);
		$criteria->compare('isRequired',$this->isRequired,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Surveytimestamp the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
