<?php

/**
 * This is the model class for table "p_action".
 *
 * The followings are the available columns in table 'p_action':
 * @property integer $id
 * @property integer $controller_id
 * @property string $title
 * @property string $action
 * @property string $create_date
 * @property string $create_by
 * @property string $update_date
 * @property string $update_by
 */
class Question extends CActiveRecord
{

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'oae-survey.question';
	}


    // public function beforeSave()
    // {
    //     if ($this->isNewRecord){
    //         $this->create_date= new CDbExpression('NOW()');
    //         $this->create_by= Yii::app()->user->id;
    //     }else{
    //         $this->update_date= new CDbExpression('NOW()');
    //         $this->update_by= Yii::app()->user->id;
    //     }
    //     return true;
    // }


	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            // array('title, action', 'required'),
			array('question_id, question_group_id, readable_id, options, sortorder', 'numerical', 'integerOnly'=>true),
			array('question_id, question_group_id, readable_id, options, question, type, masterdata, sortorder', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('question_id, question_group_id, readable_id, options, question, type, masterdata, sortorder', 'safe', 'on'=>'search'),
		);
	}


	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
      		// 'survey' => array(self::BELONGS_TO, 'Survey', 'survey_id'),
      		// 'those' => array(self::BELONGS_TO, 'Those', 'group'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'question_id' => 'question_id',
			'question_group_id' => 'question_group_id',
			'readable_id' => 'readable_id',
			'options' => 'options',
			'question' => 'question',
			'type'=> 'type',
			'masterdata' => 'masterdata',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('question_id',$this->question_id);
		$criteria->compare('question_group_id',$this->question_group_id);
		$criteria->compare('readable_id',$this->readable_id,true);
		$criteria->compare('options',$this->options,true);
		$criteria->compare('question',$this->question,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('masterdata',$this->masterdata,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PAction the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
