
<!DOCTYPE html>
<html lang="en">
<body data-spy="scroll" data-target="#myScrollspy">
  <!-- start row -->
  <div class="row">
    <div class="col-lg-12 set-height set-padding">
      <div id="section1">
        <div class="card">
          <div class="card-header">
            <h5 class="mb-1">นำออกค่าคูณขยาย</h5>
          </div>
        </div>
        <div class="card table-card">
          <div class="card-header">
            <div class="row">
              <form enctype="multipart/form-data" id="sutdent-form" method="POST"
              action="<?php echo Yii::app()->baseurl.'/Processing/ExportExcel'; ?>">
              <div class="col-md-12 mb-12">
                <div class="form-group">
                  <label for="">ปีแบบสอบถาม<font color="red">*</font></label>
                  <select class="form-control digits" id="export_survey" name="export[survey]">
                    <?php
                    if (!empty($survey)) {                                                    
                      foreach ($survey as $key => $value) { ?>
                        <option value="<?= $value->id;  ?>" data-id="<?= $value->id; ?>"<?php if(isset($_GET["export"]["survey"]) && $_GET["export"]["survey"] == $value->id){ echo "selected"; } ?> ><?= $value->name;  ?></option>
                       <?php
                     }
                   }
                   ?>
                 </select>
               </div>
               <div class="form-actions m-t-20">
                <button type="submit" class="btn btn-primary glyphicons circle_ok p-15"><i></i>นำออก
                  excel
                </button>

              </div>
            </div>

          </form>
        </div>
        <hr>
        
      </div>
    </div>
  </div>
</div>
</div>


</body>

</html>

<script type="text/javascript">
  $( document ).ready(function() {
    <?php if (isset($_GET["search"]) && $_GET["search"] == "false") {?>
      alert("ไม่พบข้อมูล");
    <?php } ?>
  });
</script>