<?php
header("Content-Type: application/vnd.ms-excel");
header('Content-Disposition: attachment; filename="MyXls.xls"');
$strExcelFileName = 'template-import-'.$group["question_group_name"] . ".xls";
header("Content-Type: application/x-msexcel; name=\"" . $strExcelFileName . "\"");
header("Content-Disposition: inline; filename=\"" . $strExcelFileName . "\"");
header('Content-Type: text/plain; charset=UTF-8');
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Pragma:no-cache");
?>

<html xmlns:o=”urn:schemas-microsoft-com:office:office”

xmlns:x=”urn:schemas-microsoft-com:office:excel”

xmlns=”http://www.w3.org/TR/REC-html40″>

<HTML>

<HEAD>
  <meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />
</HEAD>
<BODY>
    <table>
        <thead>
            <tr>
                <th>ลำดับ</th>
                <!-- <th>เจ้าหน้าที่สำรวจ(ชื่อ)</th>
                <th>เจ้าหน้าที่สำรวจ(นามสกุล)</th>
                <th>เจ้าหน้าที่สำรวจ(รหัส)</th>
                <th>เจ้าหน้าที่สำรวจ(วัน/เดือน/ปี)</th> -->
                <th>สศท.</th>
                <th>จังหวัด</th>
                <th>อำเภอ</th>
                <th>ตำบล</th>
                <th>หมู่ที่</th>
                <!-- <th>บ้านเลขที่</th> -->
                <th>พวกที่</th>
<!--                 <th>ผู้ขึ้นทะเบียนเกษตรกร(ชื่อ)</th>
                <th>ผู้ขึ้นทะเบียนเกษตรกร(นามสกุล)</th> -->
                <th>ผู้ขึ้นทะเบียนเกษตรกร(ตัวอย่างที่)</th>
                <!-- <th>ผู้ขึ้นทะเบียนเกษตรกร(เลขบัตรประจำตัวประชาชน)</th> -->
                <!-- <th>ผู้ขึ้นทะเบียนเกษตรกร(ผู้ขึ้นทะเบียนเกษตร)</th> -->
<!--                 <th>ชื่อผู้ให้ข้อมูล(ชื่อ)</th>
                <th>ชื่อผู้ให้ข้อมูล(นามสกุล)</th>
                <th>ชื่อผู้ให้ข้อมูล(ความสัมพันธ์กับหัวหน้าครัวเรือน)</th>
                <th>ชื่อผู้ให้ข้อมูล(เลขบัตรประจำตัวประชาชน)</th>
                <th>ชื่อผู้ให้ข้อมูล(เบอร์ติดต่อ)</th> -->
                <!-- <th>ครัวเรือนเกษตรมีที่ดินทำการเกษตรอย่างน้อย 1 แปลง อยู่ในเขตชลประทาน</th> -->
                <!-- <?php foreach ($QuestionnaireQuestion as $key => $value) {?>
                    <th>QuestionnaireQuestion-<?=$value->question_id?></th>
                <?php } ?> -->
                <?php foreach ($QuestionMap as $key => $value) {?>
                    <th>Question-<?=$value->question_table->question_id?></th>
                <?php } ?>
            </tr>
        </thead>
        <tbody>
            <td>1</td>
            <!-- <td><?=$Questionnaire->staff_name?></td>
            <td><?=$Questionnaire->staff_lname?></td>
            <td>'<?=$Questionnaire->staff_id?></td>
            <td><?=$Questionnaire->survey_time?></td> -->
            <td><?=$Questionnaire->office_id?></td>
            <td>'<?=$Questionnaire->province?></td>
            <td>'<?=$Questionnaire->district?></td>
            <td>'<?=$Questionnaire->subdistrict?></td>
            <td>'<?=$Questionnaire->village?></td>
            <!-- <td>'<?=$Questionnaire->addressNumber?></td> -->
            <td><?=$Questionnaire->group?></td>
            <!-- <td><?=$Questionnaire->farmer_name?></td>
            <td><?=$Questionnaire->farmer_lastname?></td> -->
            <td><?=$Questionnaire->sample?></td>
            <!-- <td>'<?=$Questionnaire->farmer_id?></td>
            <td><?=$Questionnaire->farmer_registered?></td>
            <td><?=$Questionnaire->informant_name?></td>
            <td><?=$Questionnaire->informant_lastname?></td>
            <td><?=$Questionnaire->informant_relation?></td>
            <td>'<?=$Questionnaire->informant_id?></td>
            <td>'<?=$Questionnaire->informant_tel?></td>
            <td><?=$Questionnaire->inIrrigatedArea?></td> -->
        </tbody>
    </table>

</BODY>

</HTML>
