 
 
                <div class="container-fluid">

                   <!-- <div id="myScrollspy" class="custom-scrollbar nav sidenav">
                        <div class="sidenav-content">
                            <a href="#section1" data-original-title="" title="" class="nav-link active">ข้อมูลเบื้องต้น</a>
                            <a href="#section2" data-original-title="" title="" class="nav-link ">มูลค่าผลผลิตและผลิตภัณฑ์สัตว์ที่ได้มาระหว่างปี</a>
                            <a href="#section3" data-original-title="" title="" class="nav-link">การขายผลผลิตและผลิตภัณฑ์สัตว์</a>
                            <a href="#section4" data-original-title="" title="" class="nav-link">ปริมาณการกระจายผลผลิต และผลิตภัณฑสัตว์ระหว่างปี</a>
                        </div>
                    </div> -->

                    <!-- start row -->
                    <div class="row">
                        <div class="col-lg-12 set-height set-padding">
                            <div id="section1">
                                <div class="card">
                                    <div class="card-header">
                                        <h5 class="mb-1">หมวดที่ 3 การเลี้ยงสัตว์และการกระจายผลผลิตทางสัตว์ใน<span class="line-danger">หน่วยมูลค่า</span> ตอบเป็น <span class="line-danger">“บาท”</span> <span>(ปี พ.ศ.2562 - พ.ศ.2563)</span></h5>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-body">

                                        <form>
                                            <div class="row">
                                                <div class="col-md-2 mb-3">
                                                    <label for="">จังหวัด</label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-2 mb-3">
                                                    <label for="">อำเภอ</label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-2 mb-3">
                                                    <label for="">ตำบล</label>
                                                    <div class="input-group">
                                                        <input class="form-control" id="" type="text" placeholder="" aria-describedby="" required="" data-original-title="" title="">
                                                    </div>
                                                </div>
                                                <div class="col-md-2 mb-3">
                                                    <label for="">หมู่ที่</label>
                                                    <div class="input-group">
                                                        <input class="form-control" id="" type="text" placeholder="" aria-describedby="" required="" data-original-title="" title="">
                                                    </div>
                                                </div>
                                                <div class="col-md-2 mb-3">
                                                    <label for="">พวกที่</label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-2 mb-3">
                                                    <label for="">ตัวอย่างที่</label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                            </div>
                                        </form>

                                    </div>
                                </div>
                            </div>

                            <div class="card">
                                <div class="card-body">

                                    <form>
                                        <div class="row">
                                            <div class="col-md-6 mb-3">
                                                <label for="">รหัสสินค้า <small class="text-danger">(1)</small></label>
                                                <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                            </div>
                                            <div class="col-md-6 mb-3">
                                                <label for="">ชื่อสัตว์และผลิตภัณฑ์สัตว์ <small class="text-danger">(2)</small></label>
                                                <input class="form-control" disabled id="" type="text" placeholder="" required="" data-original-title="" title="">
                                            </div>
                                        </div>
                                        <hr>
                                        <div id="section2">
                                            <div class="row">
                                                <div class="col-12">
                                                    <h6 class="text-head mb-3"><i class="icon-pencil-alt"></i> มูลค่าผลผลิตและผลิตภัณฑ์สัตว์ที่ได้มาระหว่างปี</h6>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4 mb-3">
                                                    <label for="">คงเหลือต้นปี <small class="text-danger">(บาท)(3)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">ที่ผลิตได้เอง/ที่เกิดใหม่ <small class="text-danger">(บาท)(4)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4 mb-3">
                                                    <label for="">ที่ซื้อมา <small class="text-danger">(บาท)(5)</small></label>
                                                    <div class="input-group">
                                                        <input class="form-control" id="" type="text" placeholder="" aria-describedby="" required="" data-original-title="" title="">
                                                    </div>
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">ที่ได้รับมาฟรี <small class="text-danger">(บาท)(6)</small></label>
                                                    <div class="input-group">
                                                        <input class="form-control" id="" type="text" placeholder="" aria-describedby="" required="" data-original-title="" title="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                        <div id="section3">
                                            <div class="row">
                                                <div class="col-12">
                                                    <h6 class="text-head mb-3"><i class="icon-pencil-alt"></i> การขายผลผลิตและผลิตภัณฑ์สัตว์</h6>
                                                </div>
                                                <div class="col-md-8 mb-3">
                                                    <label for="">ปริมาณการขายทั้งปี <small class="text-danger">(กก./ตัว/ฟอง/ลิตร) (7)</small></label>
                                                    <div class="row">
                                                        <div class="col-6">
                                                            <div class="input-group">
                                                                <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                            </div>
                                                        </div>
                                                        <div class="col-4">
                                                           
                                                                <div class="form-control digits" id="">
                                                                    
                                                            </div>
                                                            
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">หน่วยของปริมาณการขาย <small class="text-danger">(1-4) (8)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-4 mb-3">
                                                    <label for="">ลักษณะการขายส่วนใหญ่ <small class="text-danger">(1-8)(9)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">ราคาที่ขายได้ <small class="text-danger">(บาท/หน่วย)(10)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-4 mb-3">
                                                    <label for="" class="text-danger">มูลค่าการขายทั้งปี <small class="text-danger">(บาท) (11)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                        </div>

                                        <hr>
                                        <div id="section4">
                                            <div class="row">
                                                <div class="col-12">
                                                    <h6 class="text-head mb-3"><i class="icon-pencil-alt"></i> มูลค่าการกระจายผลผลิตและผลิตภัณฑ์สัตว์ระหว่างปี</h6>
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">จ่ายเป็นค่าเช่า <small class="text-danger">(บาท)(12)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">จ่ายเพื่อใช้หนี้ <small class="text-danger">(บาท)(13)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">ใช้เลี้ยงสัตว์ <small class="text-danger">(บาท)(14)</small></label>
                                                    <div class="input-group">
                                                        <input class="form-control" id="" type="text" placeholder="" aria-describedby="" required="" data-original-title="" title="">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-4 mb-3">
                                                    <label for="">ใช้บริโภคในครัวเรือน <small class="text-danger">(บาท)(15)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">ใช้เพื่อแปรรูป <small class="text-danger">(บาท)(16)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">ให้ฟรีผู้อื่น/สูญหาย <small class="text-danger">(บาท)(17)</small></label>
                                                    <div class="input-group">
                                                        <input class="form-control" id="" type="text" placeholder="" aria-describedby="" required="" data-original-title="" title="">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-4 mb-3">
                                                    <label for="">คงเหลือปลายปี <small class="text-danger">(บาท)(18)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                        </div>

                                    </form>

                                </div>
                            </div>

                            <div class="row text-center group-submit">
                                <button type="button" class="btn btn-primary btn-lg">บันทึกข้อมูล</button>
                                <button type="button" class="btn btn-outline-secondary btn-lg">ยกเลิก</button>
                            </div>
                        </div>
                    </div>
                    <!-- end row -->

                </div>


 