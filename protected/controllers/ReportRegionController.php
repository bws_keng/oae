<?php

class ReportRegionController extends Controller
{

    public $layout = 'admin';

    public function init()
    {
        if(!Yii::app()->user->id ){
            $this->redirect(Yii::app()->baseurl.'/user/login?admin=1');
        }else{
            $user = User::model()->findByPk(Yii::app()->user->id);
            if($user->superuser != 1){
                $this->redirect(Yii::app()->baseurl);
            }
        }
    }
    public function actionIndex($id)
    {
        $survey = Survey::model()->findAll();
        $this->render("index", Array(
            'id'=>$id,
            'survey'=>$survey,
        ));
    }

    public function actionExportExcel($id)
    {
      $this->layout=false;
      $this->render("export",Array(
        'id'=>$id,
      ));
    }

    public function actionHoldland() //หมวด 1 การถือครองและการใช้ประโยชน์ที่ดินครัวเรือนเกษตร
    {
        $this->render("Holdland");
    }

   public function actionReceiptplant() //หมวด 2 องค์ประกอบรายได้เงินสดเกษตรทางพืช
   {
       $this->render("Receiptplant");
   }

   public function actionReceiptanimal() //หมวด 3 องค์ประกอบรายได้เงินสดเกษตรทางสัตว์
   {
       $this->render("Receiptanimal");
   }

   public function actionExpenseplant() //หมวด 4 องค์ประกอบรายจ่ายเงินสดเกษตรทางพืช
   {
       $this->render("Expenseplant");
   }

   public function actionExpenseanimal() //หมวด 5 องค์ประกอบรายจ่ายเงินสดเกษตรทางสัตว์
   {
       $this->render("Expenseanimal");
   }

   public function actionReceiptother($id) //หมวด 6 องค์ประกอบรายได้เงินสดอื่น
   {
      $this->render("Receiptother",Array(
        'id'=>$id,
      ));
   }

   public function actionExportExcelother($id)
    {
      $this->layout=false;
      $this->render("export-other",Array(
        'id'=>$id,
      ));
    }

    public function actionHouseholddebt() //หมวด 8 แสดงรายละเอียดหนี้สินปลายปีของครัวเรือนเกษตร
    {
        $this->render("Householddebt");
    }

    public function actionHouseholdproperty() //หมวด 9 ทรัพย์สินปลายปีครัวเรือนเกษตร
    {
        $this->render("Householdproperty");
    }

    public function actionHouseholdbasicdata() //หมวด 10 ข้อมูลพื้นฐานของครัวเรือน และหัวหน้าครัวเรือนเกษตร
    {
        $this->render("Householdbasicdata");
    }

    public function actionPoplutiondata() //หมวด 10 ข้อมูลพื้นฐานของประชากรเกษตร
    {
        $this->render("Poplutiondata");
    }

    public function actionLaborbasicdata() //หมวด 10 ข้อมูลพื้นฐานของแรงงานเกษตร
    {
        $this->render("Laborbasicdata");
    }
}
?>