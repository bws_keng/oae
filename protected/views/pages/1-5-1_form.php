<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="description" content="Cuba admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, Cuba admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="pixelstrap">
    <title>หมวดที่ 5 รายจ่ายสดเงินสดเกษตรทางสัตว์ (ปี พ.ศ.2562 - พ.ศ.2563)</title>
    <?php include 'theme/assets/include/inc-head.php'; ?>
</head>

<body data-spy="scroll" data-target="#myScrollspy">
    <div class="tap-top"><i data-feather="chevrons-up"></i></div>

    <div class="page-wrapper compact-wrapper" id="pageWrapper">

        <?php include 'theme/assets/include/inc-header.php'; ?>

        <div class="page-body-wrapper horizontal-menu">
            <?php include 'theme/assets/include/inc-menuleft.php'; ?>

 
                <div class="container-fluid">

                  <!--  <div id="myScrollspy" class="custom-scrollbar nav sidenav">
                        <div class="sidenav-content">
                            <a href="#section1" data-original-title="" title="" class="nav-link active">ข้อมูลเบื้องต้น</a>
                            <a href="#section2" data-original-title="" title="" class="nav-link ">รวมค่าใช้จ่ายการจ้างแรงงานที่จ่ายจริงทั้งหมด</a>
                            <a href="#section3" data-original-title="" title="" class="nav-link">รวมค่าใช้จ่ายด้านวัสดุอุปกรณ์ทางการเกษตรที่จ่ายจริงทั้งหมด</a>
                        </div>
                    </div> -->

                    <!-- start row -->
                    <div class="row">
                        <div class="col-lg-12 set-height set-padding">
                            <div id="section1">
                                <div class="card">
                                    <div class="card-header">
                                        <h5 class="mb-1">หมวดที่ 5 รายจ่ายเงินสดเกษตร<span class="line-danger">ทางสัตว์</span> <span>(ปี พ.ศ.2562 - พ.ศ.2563)</span></h5>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-body">

                                        <form>
                                            <div class="row">
                                                <div class="col-md-2 mb-3">
                                                    <label for="">จังหวัด</label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-2 mb-3">
                                                    <label for="">อำเภอ</label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-2 mb-3">
                                                    <label for="">ตำบล</label>
                                                    <div class="input-group">
                                                        <input class="form-control" id="" type="text" placeholder="" aria-describedby="" required="" data-original-title="" title="">
                                                    </div>
                                                </div>
                                                <div class="col-md-2 mb-3">
                                                    <label for="">หมู่ที่</label>
                                                    <div class="input-group">
                                                        <input class="form-control" id="" type="text" placeholder="" aria-describedby="" required="" data-original-title="" title="">
                                                    </div>
                                                </div>
                                                <div class="col-md-2 mb-3">
                                                    <label for="">พวกที่</label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-2 mb-3">
                                                    <label for="">ตัวอย่างที่</label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                            </div>
                                        </form>

                                    </div>
                                </div>
                            </div>

                            <div class="card">
                                <div class="card-body">

                                    <form>
                                        <div class="row">
                                            <div class="col-md-6 mb-3">
                                                <label for="">รหัสสินค้า <small class="text-danger">(1)</small></label>
                                                <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                            </div>
                                            <div class="col-md-6 mb-3">
                                                <label for="">ชื่อสัตว์และผลิตภัณฑ์สัตว์ <small class="text-danger">(2)</small></label>
                                                <input class="form-control" disabled id="" type="text" placeholder="" required="" data-original-title="" title="">
                                            </div>
                                        </div>
                                        <hr>

                                        <div id="section2">
                                            <div class="row">
                                                <div class="col-12">
                                                    <h6 class="text-head mb-3"><i class="icon-pencil-alt"></i> รวมค่าใช้<u>จ่ายการจ้างแรงงาน</u>ที่จ่ายจริงทั้งหมด (บาท) </h6>
                                                    <h6 class="text-under">การเตรียมพื้นที่ก่อนเลี้ยง</h6>
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">เตรียมโรงเลี้ยง/คอกสัตว์ <small class="text-danger">(3)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">ขุดบ่อ/ลอกบ่อเลี้ยง <small class="text-danger">(4)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">ผสมพันธุ์/ตอนพันธ์ุ<small class="text-danger">(ไม่รวมค่าพันธุ์/ค่าน้ำเชื้อ)(5)</small></label>
                                                    <div class="input-group">
                                                        <input class="form-control" id="" type="text" placeholder="" aria-describedby="" required="" data-original-title="" title="">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-12">
                                                    <h6 class="text-under">การดูแล</h6>
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">เตรียมอาหาร/ผสมอาหาร <small class="text-danger">(6)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">ให้อาหาร <small class="text-danger">(7)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">เฝ้า / ดูแล <small class="text-danger">(8)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>

                                                <div class="col-md-4 mb-3">
                                                    <label for="">ทำความสะอาดโรงเลี้ยง/คอกสัตว์ <small class="text-danger">(9)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-12">
                                                    <h6 class="text-under">การขาย</h6>
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">จับไปขาย <small class="text-danger">(10)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">ขนไปขาย<small class="text-danger">(11)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4 mb-3">
                                                    <label for="">ค่าอาหารแลกเปลี่ยนแรงงาน <small class="text-danger">(12)</small></label><br><br>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">ค่าจ้างแรงงานอื่นๆ<small class="text-danger">(13)</small></label><br><br>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">ค่าจ้างแรงงานเหมารวม <small class="text-danger">(14)</small><br>
                                                    <small class="text-danger">(กรณีแบ่งแยกกิจกรรมที่ 3-13 ไม่ได้)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                        </div>

                                        <hr>

                                        <div id="section3">
                                            <div class="row">
                                                <div class="col-12">
                                                    <h6 class="text-head mb-3"><i class="icon-pencil-alt"></i> รวมค่าใช้<u>ด้านวัสดุอุปกรณ์ทางการเกษตร</u>ที่จ่ายจริงทั้งหมด (บาท) </h6>
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">ค่าพันธุ์/ค่าน้ำเชื้อ <small class="text-danger">(15)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-12">
                                                    <h6 class="text-under">ค่าอาหาร/ค่ายา</h6>
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">ค่าอาหารรวม <small class="text-danger">(16)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">ค่ายาป้องกัน/รักษาโรค<small class="text-danger">(17)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-12">
                                                    <h6 class="text-under">ค่าน้ำมัน/ค่าน้ำ/ค่าไฟ</h6>
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">ค่าน้ำมันเชื้อเพลิงและน้ำมันหล่อลื่น <small class="text-danger">(18)</small></label><br><br>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">ค่าน้ำการเกษตร <small class="text-danger">(19)</small></label><br><br>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">ค่าไฟฟ้าการเกษตร <small class="text-danger">(20)</small><br>
                                                    <small class="text-danger">(รวมค่าไฟฟ้าที่ใช้สูบน้ำ/ให้แสง)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-4 mb-3">
                                                    <label for="">ค่าวัสดุสิ้นเปลือง <small class="text-danger">(21)</small><br>
                                                    <small class="text-danger">(เช่น ฟาง ถุง เชือก ไม้ไผ่ ผ้าใบ)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">ค่าอุปกรณ์ <small class="text-danger">(22)</small><br>
                                                    <small class="text-danger">(เช่น สายยาง ท่อน้ำ ถังน้ำ รางให้อาหาร/น้ำ)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">ค่าวัสดุอุปกรณ์เหมารวม <small class="text-danger">(23)</small><br>
                                                    <small class="text-danger">(กรณีแบ่งแยกรายการที่ 15-22 ไม่ได้)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                        </div>

                                    </form>

                                </div>
                            </div>

                            <div class="row text-center group-submit">
                                <button type="button" class="btn btn-primary btn-lg">บันทึกข้อมูล</button>
                                <button type="button" class="btn btn-outline-secondary btn-lg">ยกเลิก</button>
                            </div>
                        </div>
                    </div>
                    <!-- end row -->

                </div>


 