<!-- footer start-->
<footer class="footer footer-oae">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-6 footer-copyright">
        <p class="mb-0"><small>Copyright ©<?=date("Y")?> สำนักงานเศรษฐกิจการเกษตร</small></p>
      </div>
    </div>
  </div>
</footer>