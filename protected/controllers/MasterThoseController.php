<?php

class MasterThoseController extends Controller
{
	public $layout='admin';
    public function actionIndex()
    {
        $model = new MasterThose;
		$criteria = new CDbCriteria;
		$criteria->compare('active', 1);
		$criteria->order = 'those_name ASC';
		$data = MasterThose::model()->findAll($criteria);
        $this->render('index', Array(
			'data' => $data,
			'model' => $model,
		));
    }

    public function actionCreate()
    {
        $model = new MasterThose;
		if(isset($_POST['MasterThose']))
    	{
            // var_dump($model->attributes);exit();
			$model->attributes = $_POST['MasterThose'];
			// $model->those_name = $_POST['MasterThose']['those_name']."XXX"; //3XXX
			$thoseCodeLength = strlen($_POST['MasterThose']['those_code']);
			// var_dump($thoseCodeLength);exit();

			if ($thoseCodeLength == 1){
				$model->those_code = "0".$_POST['MasterThose']['those_code'];
			}
			$model->active = 1;
			if($model->validate()){
				if($model->save()){
					$this->redirect(array('view', 'id' => $model->those_id));
				}
			}
		}
		$this->render('create', Array(
			'model' => $model
		));
    }

    public function actionView($id)
    {
        $this->render('view',array(
    		'model'=>$this->loadModel($id),
    	));
    }

    public function actionEdit($id)
	{
		$model=$this->loadModel($id);
		$arr = str_split($model->those_code);
		if($arr[0] == 0){
			$model->those_code = $arr[1];
			// var_dump($model->those_code);exit();
		}
		if(isset($_POST['MasterThose']))
    	{
			$model->attributes = $_POST['MasterThose'];
			$thoseCodeLength = strlen($_POST['MasterThose']['those_code']);
			// var_dump($thoseCodeLength);exit();
			if ($thoseCodeLength == 1){
				$model->those_code = "0".$_POST['MasterThose']['those_code'];
			}
			if($model->validate()){
				if($model->save()){
					$this->redirect(array('view', 'id' => $model->those_id));
				}
			}
		}
		$this->render('update', Array(
			'model' => $model
		));
	}

    public function actionDelete($id)
	{
		$model = $this->loadModel($id);
		$model->active = 0;

		if($model->save())
		{
			$this->redirect(array('index'));
		}
	}

    public function loadModel($id)
	{
		$model = MasterThose::model()->findByPk($id);
		return $model;
	}
}

?>