<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="description" content="Cuba admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, Cuba admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="pixelstrap">
    <title>หมวดที่ 7 รายจ่ายเงินสดเกษตรในฟาร์มและรายจ่ายเพื่ออุปโภคบริโภค</title>
    <?php include 'theme/assets/include/inc-head.php'; ?>
</head>

<body data-spy="scroll" data-target="#myScrollspy">
    <div class="tap-top"><i data-feather="chevrons-up"></i></div>

    <div class="page-wrapper compact-wrapper" id="pageWrapper">

        <?php include 'theme/assets/include/inc-header.php'; ?>

        <div class="page-body-wrapper horizontal-menu">
            <?php include 'theme/assets/include/inc-menuleft.php'; ?>

 
                <div class="container-fluid">



                    <!-- start row -->
                    <div class="row">
                        <div class="col-lg-12 set-height set-padding">
                            <div id="section1">
                                <div class="card">
                                    <div class="card-header">
                                        <h5 class="mb-1">หมวดที่ 7 รายจ่ายเงินสดเกษตรในฟาร์มและรายจ่ายเพื่อการอุปโภคบริโภค (ปี พ.ศ.2562 - พ.ศ.2563)</span></h5>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-body">

                                        <form>
                                            <div class="row">
                                                <div class="col-md-2 mb-3">
                                                    <label for="">จังหวัด</label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-2 mb-3">
                                                    <label for="">อำเภอ</label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-2 mb-3">
                                                    <label for="">ตำบล</label>
                                                    <div class="input-group">
                                                        <input class="form-control" id="" type="text" placeholder="" aria-describedby="" required="" data-original-title="" title="">
                                                    </div>
                                                </div>
                                                <div class="col-md-2 mb-3">
                                                    <label for="">หมู่ที่</label>
                                                    <div class="input-group">
                                                        <input class="form-control" id="" type="text" placeholder="" aria-describedby="" required="" data-original-title="" title="">
                                                    </div>
                                                </div>
                                                <div class="col-md-2 mb-3">
                                                    <label for="">พวกที่</label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-2 mb-3">
                                                    <label for="">ตัวอย่างที่</label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                            </div>
                                        </form>

                                    </div>
                                </div>
                            </div>

                            <h5 class="mb-3">รายการรายจ่าย</h5>
                            <div class="card">
                                <div class="card-body">

                                    <form>
                                        <div id="section2">
                                            <div class="row">
                                                <div class="col-12">
                                                    <h6 class="text-head mb-3"><i class="icon-pencil-alt"></i> รายจ่ายเงินสดเกษตรในฟาร์ม</h6>
                                                    <h6 class="text-under">ที่ดินทางการเกษตร</h6>
                                                </div>
                                                <div class="col-md-8 ">
                                                    <label for="">ค่าเช่าที่ดินทางการเกษตร<small class="text-danger"> (รหัส 1)</small><br><span class="text-danger"> **สำหรับที่ดินที่เช่าผู้อื่นเพื่อทำการเกษตร </span></label>

                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">รายจ่ายรวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-8 mb-3">
                                                    <label for="">ค่าภาษีที่ดินทางการเกษตร<small class="text-danger"> (รหัส 2)</small></label>

                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">รายจ่ายรวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-8 mb-3">
                                                    <label for="">ค่าจัดรูป/ปรับรูป/บุกเบิกที่ดินทางการเกษตร<small class="text-danger"> (รหัส 3)</small></label>

                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">รายจ่ายรวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-8 mb-3">
                                                    <label for="">ค่าไถ่ถอนที่ดินทางการเกษตร<small class="text-danger"> (รหัส 4)</small></label>

                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">รายจ่ายรวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-8 mb-3">
                                                    <label for=""><u>ซื้อที่ดินทางการเกษตร</u>เพิ่มเติมในปีเพาะปลูกนี้<small class="text-danger"> (รหัส 5)</small></label>

                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">รายจ่ายรวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-12">
                                                    <h6 class="text-under">ทรัพย์สินทางการเกษตรอื่นๆ</h6>
                                                </div>
                                                <div class="col-md-8 mb-3">
                                                    <label for="">ค่าเช่าทรัพย์สินทางการเกษตรอื่นๆ เช่นโรงเรือน
                                                        เครื่องจักรกลทางการเกษตร อุปกรณ์ทางการเกษตร<small class="text-danger"> (รหัส 6)</small></label>

                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">รายจ่ายรวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-8 mb-3">
                                                    <label for="">ค่าซ่อม/บำรุงรักษาทรัพย์สินทางการเกษตรอื่นๆ เช่น โรงเรือน เครื่องจักรกลการเกษตร อุปกรณ์ทางการเกษตร<small class="text-danger"> (รหัส 7)</small></label>

                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">รายจ่ายรวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-8 mb-3">
                                                    <label for=""><u>ซื้อทรัพย์สินทางการเกษตรอื่นๆ</u> เพิ่มเติม ในปีเพาะปลูกนี้ เช่นโรงเรือน เครื่องจักรกลทางการเกษตร อุปกรณ์ทางการเกษตร<small class="text-danger"> (รหัส 8)</small></label>

                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">รายจ่ายรวมทั้งหมด<small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-8 mb-3">
                                                    <label for="">ค่าใช้จ่ายเกี่ยวกับการชลประทานของพื้นที่ทำการเกษตร เช่น ค่าขุดบ่อ/สระน้ำ ค่าลอกบ่อ/สระน้ำ ค่าทำคันกั้นน้ำ<small class="text-danger"> (รหัส 9)</small></label>

                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">รายจ่ายรวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-8 mb-3">
                                                    <label for="">ค่าจ้างแรงงานเกษตรรายเดือน รวมทุกกิจกรรม<small class="text-danger"> (รหัส 10)</small></label>

                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">รายจ่ายรวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-8 mb-3">
                                                    <label for="">รายจ่ายเงินสดเกษตรในฟาร์มอื่นๆ<small class="text-danger"> (รหัส 11)</small></label>

                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">รายจ่ายรวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                        </div>
                                        <hr>

                                        <div id="section2">
                                            <div class="row">
                                                <div class="col-12">
                                                    <h6 class="text-head mb-3"><i class="icon-pencil-alt"></i> รายจ่ายเงินสดเพื่อการบริโภค</h6>
                                                </div>
                                                <div class="col-md-8 mb-3">
                                                    <label for=""><u>ข้าวสาร</u>เพื่อการบริโภค<small class="text-danger"> (รหัส 12)</small></label>

                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">รายจ่ายรวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-8 mb-3">
                                                    <label for=""><u>อาหารสด/อาหารแปรรูป สำหรับนำมาประกอบอาหารและ/หรือรับประทานในครัวเรือน</u> 
                                                    <small class="text-danger"> (รหัส 13)</small></label>

                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">รายจ่ายรวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-8 mb-3">
                                                    <label for=""><u>อาหารสำเร็จรูปหรืออาหารที่ซื้อนอกบ้านและ/หรือรับประทานนอกบ้าน</u> รวมถึง การรับประทานอาหารนอกบ้าน การซื้ออาหารสำเร็จรูป
                                                        กลับไปรับประทานที่บ้าน การซื้ออาหารรับประทานที่ทำงาน
                                                        การซื้อเครื่องดื่มที่ไม่มีแอลกอฮอล์/ขนมขบเคี้ยวรับประทานนอกบ้าน<span class="text-danger"> **ไม่รวมค่าอาหาร/ค่าขนมของบุตรหลานที่ให้ไปโรงเรียน</span>
                                                        <small class="text-danger">(รหัส 14)</small></label>

                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">รายจ่ายรวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-8 mb-3">
                                                    <label for=""><u>เครื่องปรุงต่างๆ</u> ได้แก่ น้ำมันพืชและน้ำมันชนิดอื่นๆ เครื่องปรุงรสต่างๆ เช่น น้ำปลา น้ำตาล น้ำมันหอย เครื่องเทศต่างๆ<small class="text-danger"> (รหัส 15)</small></label>

                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">รายจ่ายรวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-8 mb-3">
                                                    <label for=""><u>เครื่องดื่มที่มีแอลกอฮอล์และยาสูบ</u> เช่น สุรา เบียร์ ไวน์ วิสกี้ บรั่นดี บุหรี่ ยาเส้น ยาสูบ<small class="text-danger"> (รหัส 16)</small></label>

                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">รายจ่ายรวมทั้งหมด<small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                        </div>
                                        <hr>

                                        <div id="section2">
                                            <div class="row">
                                                <div class="col-12">
                                                    <h6 class="text-head mb-3"><i class="icon-pencil-alt"></i> รายจ่ายเงินสดเพื่อการอุปโภค</h6>
                                                    <h6 class="text-under">ค่าสาธารณูปโภคและเชื้อเพลิง</h6>
                                                </div>
                                                <div class="col-md-8 mb-3">
                                                    <label for="">ค่าไฟฟ้าในครัวเรือน<small class="text-danger"> (รหัส 17)</small></label>

                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">รายจ่ายรวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-8 mb-3">
                                                    <label for="">ค่าน้ำใช้ในครัวเรือน<small class="text-danger"> (รหัส 18)</small></label>

                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">รายจ่ายรวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-8 mb-3">
                                                    <label for="">ค่าแก๊สหุงต้มในครัวเรือน<small class="text-danger"> (รหัส 19)</small></label>

                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">รายจ่ายรวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-8 mb-13">
                                                    <label for="">ค่าน้ำมันเชื้อเพลิงสำหรับยานพาหนะและให้แสงสว่างในครัวเรือน<small class="text-danger"> (รหัส 20)</small></label>

                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">รายจ่ายรวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-8 mb-3">
                                                    <label for="">ค่าโทรศัพท์บ้านและโทรศัพท์มือถือ<small class="text-danger"> (รหัส 21)</small></label>

                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">รายจ่ายรวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-12">
                                                    <h6 class="text-under">ที่ดินและทรัพย์สินคงที่นอกการเกษตร</h6>
                                                </div>
                                                <div class="col-md-8 mb-3">
                                                    <label for="">ค่าเช่าบ้าน/ที่อยู่อาศัย/อาคาร/ที่ดินนอกการเกษตร<small class="text-danger"> (รหัส 22)</small></label>

                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">รายจ่ายรวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-8 mb-3">
                                                    <label for="">ค่าภาษีที่ดิน/ภาษีโรงเรือน/ค่าเบี้ยประกัน สำหรับทรัพย์สินคงที่นอกการเกษตร<small class="text-danger"> (รหัส 23)</small></label>

                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">รายจ่ายรวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-8 mb-3">
                                                    <label for="">ค่าซ่อม/บำรุงรักษาบ้าน/ที่อยู่อาศัย/อาคารนอกการเกษตร<small class="text-danger"> (รหัส 24)</small></label>

                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">รายจ่ายรวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-8 mb-3">
                                                    <label for=""><u>ซื้อทรัพย์สินคงที่นอกการเกษตร</u> เช่น ที่อยู่อาศัย/อาคาร/ที่ดินนอกการเกษตร<small class="text-danger"> (รหัส 25)</small></label>

                                                </div>

                                                <div class="col-md-4 mb-3">
                                                    <label for="">รายจ่ายรวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-12">
                                                    <h6 class="text-under">ทรัพย์สินนอกการเกษตรอื่นๆ</h6>
                                                </div>
                                                <div class="col-md-8 mb-3">
                                                    <label for="">ค่าภาษี/ค่าธรรมเนียม/ค่าเบี้ยประกัน สำหรับทรัพย์สิน
                                                        นอกการเกษตรอื่นๆ เช่น ค่าต่อทะเบียนรถ ค่าทำประกันภัยทรัพย์สินนอกการเกษตรอื่นๆ<small class="text-danger"> (รหัส 26)</small></label>

                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">รายจ่ายรวมทั้งหมด<small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-8 mb-3">
                                                    <label for="">ค่าซ่อม/บำรุงรักษาทรัพย์สินนอกการเกษตรอื่นๆ เช่น
                                                        ค่าซ่อมรถหรือยานพาหนะอื่นๆค่าซ่อมอุปกรณ์/เครื่องใช้ภายในบ้าน<small class="text-danger"> (รหัส 27)</small></label>

                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">รายจ่ายรวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-8 mb-3">
                                                    <label for=""><u>ซื้อทรัพย์สินนอกการเกษตรอื่นๆ</u> เพิ่มเติม ในปีเพาะปลูกนี้ เช่น
                                                        โทรศัพท์มือถือ คอมพิวเตอร์ รถยนต์ ยานพาหนะอื่นๆเครื่องเรือน เครื่องมือ/เครื่องใช้ในครัวเรือน <small class="text-danger"> (รหัส 28)</small></label>

                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">รายจ่ายรวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-8 mb-3">
                                                    <label for="">ค่ารักษาพยาบาล ส่วนที่เบิกไม่ได้<small class="text-danger"> (รหัส 29)</small></label>

                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">รายจ่ายรวมทั้งหมด<small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-8 mb-3">
                                                    <label for="">ค่าใช้จ่ายด้านการศึกษาของบุตรหลาน<small class="text-danger"> (รหัส 30)</small></label>

                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">รายจ่ายรวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-8 mb-3">
                                                    <label for="">ค่าของใช้ในครัวเรือน เช่น สบู่ ยาสีฟัน ผงซักฟอก น้ำยาล้างจาน ของใช้ส่วนบุคคลอื่นๆ<small class="text-danger"> (รหัส 31)</small></label>

                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">รายจ่ายรวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-8 mb-3">
                                                    <label for="">ค่าเสื้อผ้า เครื่องนุ่งห่ม ค่าเสริมสวย
                                                        เครื่องประดับ/ตกแต่งร่างกาย<small class="text-danger"> (รหัส 32)</small></label>

                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">รายจ่ายรวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-8 mb-3">
                                                    <label for="">ค่าใช้จ่ายในการเดินทาง/ขนส่ง<small class="text-danger"> (รหัส 33)</small></label>

                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">รายจ่ายรวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-8 mb-3">
                                                    <label for="">ค่าใช้จ่ายในการเสี่ยงโชค บันเทิง และมหรสพต่างๆ<small class="text-danger"> (รหัส 34</small></label>

                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">รายจ่ายรวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-8 mb-3">
                                                    <label for="">เงินทำบุญ เงินบริจาค เงินช่วยเหลือในกิจกรรมทางศาสนา การกุศลหรือพิธีกรรมต่างๆ<small class="text-danger"> (รหัส 35)</small></label>

                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">รายจ่ายรวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-8 mb-3">
                                                    <label for=""><u>การซื้อของเงินผ่อน</u>ที่ต้องผ่อนชำระรายเดือน(รวมดอกเบี้ย)
                                                        /ค่าดอกเบี้ยแชร์<small class="text-danger"> (รหัส 36)</small></label>

                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">รายจ่ายรวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-8 mb-3">
                                                    <label for="">รายจ่ายเงินสดนอกการเกษตรอื่นๆ <small class="text-danger"> (รหัส 37)</small></label>

                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">รายจ่ายรวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>



                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-md-8 mb-3">

                                            </div>
                                            <div class="col-md-4 mb-3">
                                                <label for="">ผลรวมรายจ่ายเงินสด เกษตรในฟาร์มและรายจ่ายเพื่อการอุปโภคบริโภคตลอดทั้งปี <small class="text-danger">(บาท)</small></label>
                                                <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                            </div>
                                        </div>



                                    </form>

                                </div>
                            </div>

                            <div class="row text-center group-submit">
                                <button type="button" class="btn btn-primary btn-lg">บันทึกข้อมูล</button>
                                <button type="button" class="btn btn-outline-secondary btn-lg">ยกเลิก</button>
                            </div>
                        </div>
                    </div>
                    <!-- end row -->

                </div>


 