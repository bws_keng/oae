  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
  <link rel="stylesheet" href="http://cdn.datatables.net/1.10.2/css/jquery.dataTables.min.css"></style>
<script type="text/javascript" src="http://cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js"></script>

<div class="container-fluid">
  <!-- start row -->
  <div class="row">
    <div class="col-lg-12 set-height set-padding">
      <div id="section1">
        <div class="card">
          <div class="card-header">
            <h5 class="mb-1">
              <?php
                echo $title;
              ?>
            </h5>
          </div>
        </div>
        <div class="row justify-content-end">
          <div class="col-md-3">
            <a href="#" data-toggle="modal" data-target="#exampleModal">
              <div class="card o-hidden create-main">
                <div class="b-r-4 card-body">
                  <div class="media static-top-widget">
                    <div class="align-self-center text-center"><i data-feather="plus"></i></div>
                    <div class="media-body">
                      <h6 class="mb-0 counter">เพิ่มข้อมูล</h6><i class="icon-bg" data-feather="database"></i>
                    </div>
                  </div>
                </div>
              </div>
            </a>
          </div>
        </div>
        <div class="card table-card">
         <!--  <form method="GET" accept-charset="UTF-8" id="form_search" action="<?php echo $this->createUrl('/survey/'.$_GET['id'].'/searchdata'); ?>">  -->
            <div class="card-header">
              <div class="row">

                <div class="col-md-3 mb-3">
                  <div class="form-group">
                    <label for="">ปีเพาะปลูก<font color="red">*</font></label>
                    <select class="form-control digits" id="search_year" name="search[year]">
                      <option selected disabled>เลือกปีเพาะปลูก</option>
                      <?php
                      if (!empty($Survey)) {
                        foreach($Survey as $key => $value){
                         $year = $value->year + 543; 
                                                                // echo '<option value="'.$year['year'].'">'.$year['year'].'</option>';?>
                                                                <option value="<?= $value->id;  ?>" <?php if(isset($_GET["search"]["year"]) && $_GET["search"]["year"] == $value->id){ echo "selected"; } ?> ><?= $year;  ?>/63</option>
                                                              <?php   }
                                                            }
                                                            ?>

                                                          </select>
                                                        </div>
                                                      </div>
                                                      <div class="col-md-3 mb-3">
                                                        <div class="form-group">
                                                          <label for="">จังหวัด<font color="red">*</font></label>
                                                          <select class="form-control digits" id="search_Province" name="search[Province]">
                                                            <option selected disabled>เลือกจังหวัด</option>
                                                            <?php
                                                            if (!empty($MtProvince)) {                                                    
                                                              foreach ($MtProvince as $key => $value) { ?>
                                                               <option value="<?= $value->province_code;  ?>" data-id="<?= $value->id; ?>"<?php if(isset($_GET["search"]["Province"]) && $_GET["search"]["Province"] == $value->province_code){ echo "selected"; } ?> ><?= $value->province_code;  ?> -> <?= $value->province_name_th;  ?></option>
                                                               <?php
                                                             }
                                                           }
                                                           ?>
                                                         </select>
                                                       </div>
                                                     </div>
                                                     <div class="col-md-3 mb-3">
                                                      <div class="form-group">
                                                        <label for="">อำเภอ</label>
                                                        <select class="form-control digits" id="search_District" name="search[District]">
                                                          <option selected disabled>เลือกอำเภอ</option>
                                                          
                                                       </select>
                                                     </div>
                                                   </div>
                                                   <div class="col-md-3 mb-3">
                                                    <div class="form-group">
                                                      <label for="">ตำบล</label>
                                                      <select class="form-control digits" id="search_subdistrict" name="search[subdistrict]">
                                                        <option selected  disabled>เลือกตำบล</option>
                                                        
                                                     </select>
                                                   </div>
                                                 </div>
                                                 <div class="col-md-3 mb-3">
                                                  <div class="form-group">
                                                    <label for="">หมู่ที่</label>
                                                    <select class="form-control digits" id="search_village" name="search[village]">
                                                      <option selected disabled>เลือกหมู่ที่</option>
                                                                                                          </select>
                                                  </div>
                                                </div>
                                                <div class="col-md-3 mb-3">
                                                  <div class="form-group">
                                                    <label for="">พวกที่</label>
                                                    <select class="form-control digits" id="search_group" name="search[group]">
                                                      <option selected disabled>เลือกพวกที่</option>
                                                      <?php
                                                        foreach ($Those as $key => $value) { ?>
                                                         <option value="<?= $value->those_id;  ?>"<?php if(isset($_GET["search"]["Those"]) && $_GET["search"]["Those"] == $value->those_id){ echo "selected"; } ?> ><?= $value->those_code;  ?> -> <?= $value->those_name;  ?></option>
                                                         <?php
                                                       }
                                                       ?>
                                                                                                          </select>
                                                  </div>
                                                </div>
                                                <div class="col-md-3 mb-3">
                                                  <div class="form-group">
                                                    <label for="">ตัวอย่างที่</label>
                                                      <input class="form-control digits" type="text" name="search[sample]" id="search_sample">
                                                  </div>
                                                </div>

                                                <div class="col-md-3 mb-3">
                                                  <button class="btn btn-lg btn-search text-white" id="search-data" type="submit"><i class="icon-search"></i>&nbsp;ค้นหา</button>
                                                </div>
                                              </div>
                                            </div>
                                   <!--        </form> -->
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-striped table-main" id="myTable">
                <!-- <table class="table table-striped table-main" id="basic-2"> -->
                <thead>
                  <tr>
                    <th width="5%">ลำดับ</th>
                    <th width="10%">ปีเพาะปลูก</th>
                    <th width="25%">ชื่อผู้ขึ้นทะเบียนเกษตร</th>
                    <th width="25%">นามสกุลผู้ขึ้นทะเบียนเกษตร</th>
                    <th width="10%">วันที่</th>
                    <th width="25%">จัดการข้อมูล</th>
                  </tr>
                </thead>
                <tbody>
                  
                <?php
                $no = 1;
                  foreach ($data as $key => $value) {
                    $survey_times = explode("-", $value->survey_time);
                    $survey_0 = $survey_times[0] + 543;
                    $survey_times = $survey_times[2]."-".$survey_times[1]."-".$survey_0;
                   ?><tr>
                      <td><?= $no++;?></td>
                      <td><?= ($value->survey->year+543);?>/63</td>
                      <td><?= $value->farmer_name;?></td>
                      <td><?= $value->farmer_lastname;?></td>
                      <td><?= $survey_times;?></td>
                      <td>
                        <button type="button" class="btn btn-icon text-primary"><i data-feather="eye"></i></button>
                        <a type="button" class="btn btn-icon text-success" href="<?= Yii::app()->baseurl.'/survey/'.$_GET['id'].'/'.$value->questionnaire_id.'/rate/edit' ?>"><i data-feather="edit"></i></a>
                        <a type="button" class="btn btn-icon text-danger" href="<?= Yii::app()->baseurl.'/survey/'.$_GET['id'].'/'.$value->questionnaire_id.'/rate/delete' ?>"><i data-feather="trash"></i></a>
                      </td>
                    </tr>
                  <?php  }
                      // foreach($data['data'] as $key => $body){
                      //   echo '<tr><td>'.$no.'</td>';
                      //   foreach($data['schema'] as $header){
                      //     echo '<td>'.$body[$header['question_id']].'</td>';
                      //   }
                      //   echo '<td>
                      //   <button type="button" class="btn btn-icon text-primary"><i data-feather="eye"></i></button>
                      //   <a type="button" class="btn btn-icon text-success" href="'.Yii::app()->baseurl.'/survey/'.$_GET['id'].'/'.$key.'/edit"><i data-feather="edit"></i></a>
                      //   <button type="button" class="btn btn-icon text-danger" onclick="sendDelete('.$key.', \'';
                      //   foreach($data['schema'] as $header){
                      //     echo $body[$header['question_id']].', ';
                      //   }
                      //   echo'\')"><i data-feather="trash"></i></button>
                      // </td></tr>';
                      
                      // }
                    ?>

                </tbody>
              </table>
            </div>
            <div class="table-responsive2" style="display: none;"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- end row -->
  <div class="container position-fixed fixed-top pt-5" id="deleteForm" style="display:none;">
      <div class="card col-12 p-0">
          <div class="card-header">
              <h5>ลบข้อมูล</h5>
          </div>
          <div class="card-body">
              <h5 class="card-title">เมื่อข้อมูลถูกลบจะไม่สามารถย้อนคืนได้</h5>
              <div class="card-text">
                  <form method="POST" action="<?php echo Yii::app()->baseurl; ?>/survey/<?php echo $_GET['id']; ?>/delete">
                      <label for="deleteKey">รหัสอ้างอิง</label>
                      <input id="deleteKey" name="key" class="form-control" value="" readonly/>
                      <label for="deleteValue">ข้อมูล</label>
                      <input id="deleteValue" name="value" class="form-control" value="" readonly/>
                      <button class="btn btn-danger btn-lg mt-2">ลบข้อมูล</button>
                      <button type="button" class="btn btn-light btn-lg mt-2" onclick='$("#deleteForm").hide();'>ยกเลิก</button>
                  </form>
              </div>
          </div>
      </div>

  </div>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body">
       <div class="col-md-12 mb-6">
        <label for="">ผู้ขึ้นทะเบียนเกษตรกร</label>
        <?php
        $SurveySubValue = SurveySubValue::model()->findAll();
        $Array_questionnaire_id = Array();
        foreach ($SurveySubValue as $key => $value) {
            array_push($Array_questionnaire_id, $value->questionnaire_id);
        } 
        $questionnaire_id = array_unique($Array_questionnaire_id);

        $criteria=new CDbCriteria;
        if (isset($_GET['search']['Province'])) {
          $criteria->compare('province',$_GET['search']['Province']);
        }
        if (isset($_GET['search']['District'])) {
          $criteria->compare('district',$_GET['search']['District']);
        }
        if (isset($_GET['search']['subdistrict'])) {
          $criteria->compare('subdistrict',$_GET['search']['subdistrict']);
        }
        if (isset($_GET['search']['village'])) {
          $criteria->compare('village',$_GET['search']['village']);
        }
        $User = User::model()->findByPk(Yii::app()->user->id);
        $profile = $User->profile;
        if ($User->superuser != 1) {
          $criteria->compare('office_id',$profile->county_id);
        }
        $criteria->addNotIncondition('questionnaire_id',$questionnaire_id);
        $criteria->limit = 1000;
        $model = Questionnaire::model()->findAll($criteria);

        ?>
        <select class="form-control digits" id="List-Questionnaire">
          <option selected disabled>เลือกชื่อผู้ขึ้นทะเบียนเกษตรกร</option>
          <?php
          if (!empty($model)) {
            foreach ($model as $key => $value) { 
              $survey_time = explode("-", $value->survey_time);
              $survey_0 = $survey_time[0] + 543;
              $survey_time = $survey_time[2]."-".$survey_time[1]."-".$survey_0;
              ?>
              <option value="<?= $value->questionnaire_id ;?>"><?php echo $value->farmer_name.' '.$value->farmer_lastname.' เมื่อวันที่ '.$survey_time ?></option>
              <?php 
            }
          }
          ?>
        </select>
      </div>
    </div>
    <div class="modal-footer">
      <a href="" class="btn btn-primary create-data" > เพิ่ม </a>
      <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิด</button>
    </div>
  </div>
</div>
</div>

<script type="text/javascript">
   $(document).ready(function(){
    $('#myTable').dataTable({
      responsive: true,
        "pageLength": 10,
        "searching": false,
      "info": false,
      "lengthChange": false

    });

});
  $(".create-data").click(function(){
   if ($('#List-Questionnaire').val() == null) {
     alert("กรุณาเลือกแบบสอบถาม");
     return false;
   } 
  });
  $('#List-Questionnaire').change(function(event) {
        var id = $(this).val();
        if (id != null) {
          var _href = $(".create-data").attr("href");
          var a_href = "<?php echo Yii::app()->baseUrl.'/survey/' . $_GET['id'].'/rate/create' ?>";
          var a = $('.create-data').attr("href",_href+a_href+'?Questionnaire='+id);
        }                             
    });
</script>

<script type="text/javascript">

   $("#search-data").click(function(){

    var id =  <?php echo $_GET['id'] ?>;
    var year = $("#search_year").val();
    var province = $("#search_Province").val();
    var district = $("#search_District").val();
    var subdistrict = $("#search_subdistrict").val();
    var village = $("#search_village").val();
    var group = $("#search_group").val();
    var sample = $("#search_sample").val();
    // if (typeof  year != 'undefined' || typeof  province != 'undefined' || typeof  district != 'undefined' || typeof  subdistrict != 'undefined' ||typeof  village != 'undefined') {
   if (year != null && province != null) {
      $.ajax({
                type: 'GET',
                url: "<?= $this->createUrl('/Survey/SearchRateData'); ?>", 
                data: {
                    id:id,
                    year: year,
                    province: province,
                    district: district,
                    subdistrict: subdistrict,
                    village: village,
                    group:group,
                    sample:sample
                },
                success: function(data) {
        
                  $(".table-responsive").remove();
                  $(".table-responsive2").show();
                  $(".table-responsive2").html(data);
                }
            });
    }else{
      alert("กรุณาเลือกปีเพาะปลูกและเลือกจังหวัด");
    }
  });

    $("#search_Province").change(function() {
        var id = $("#search_Province option:selected").attr('data-id');

        if (id != "") {
            $.ajax({
                type: 'POST',
                url: "<?= $this->createUrl('/Questionnaire/SearchDistrict'); ?>", 
                data: {
                    id: id,
                },
                success: function(data) {

                $('#search_District').empty();
                $('#search_subdistrict').empty();
                $('#search_village').empty();
                $('#search_District').append(data);

                }
            });
        }
    });
    $("#search_District").change(function() {
        var id = $("#search_District option:selected").attr('data-id');
        if (id != "") {
            $.ajax({
                type: 'POST',
                url: "<?= $this->createUrl('/Questionnaire/SearchSubDistrict'); ?>", 
                data: {
                    id: id,
                },
                success: function(data) {

                $('#search_subdistrict').empty();
                $('#search_village').empty();
                $('#search_subdistrict').append(data);

                }
            });
        }
    });

    $("#search_subdistrict").change(function() {
        var id = $("#search_subdistrict option:selected").attr('data-id');
        if (id != "") {
            $.ajax({
                type: 'POST',
                url: "<?= $this->createUrl('/Questionnaire/SearchVillage'); ?>", 
                data: {
                    id: id,
                },
                success: function(data) {
                $('#search_village').empty();
                $('#search_village').append(data);

                }
            });
        }
    });

    $("#search_Province").select2( {
     placeholder: "เลือกจังหวัด",
     allowClear: true
     });

    $("#search_District").select2( {
     placeholder: "เลือกอำเภอ",
     allowClear: true
     });

    $("#search_subdistrict").select2( {
     placeholder: "เลือกตำบล",
     allowClear: true
     });

    $("#search_village").select2( {
     placeholder: "เลือกหมู่ที่",
     allowClear: true
     });

    $("#search_group").select2( {
     placeholder: "เลือกพวกที่",
     allowClear: true
     });

    $("#List-Questionnaire").select2( {
     placeholder: "เลือกชื่อผู้ขึ้นทะเบียนเกษตรกร",
     allowClear: true,
      width: '100%',
      dropdownParent: $("#exampleModal")
     });

</script>