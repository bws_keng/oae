<?php
function PermissionsMenu($menu = array())
{
  /*=============================================
    =             Check Permissions              =
    =============================================*/
  $sum = 0;
  $countArray = count($menu);
  $return = false;

  if ($countArray != 0) {
    foreach ($menu as $key => $value) {
      $val = explode('.', $value);
      $controller_name = strtolower($val[0]);

      // if($controller_name == "monthcheck"){
      //  var_dump($val);
      //  exit();
      // }
      $action_name = strtolower($val[1]);
      $check = AccessControl::check_access_action(strtolower($controller_name), strtolower($action_name));
      if ($check) {
        $return = true;
      } else {
        $return = false;
      }
    }
  }

  // if($controller_name == "monthcheck"){
  //      var_dump($check);
  //      exit();
  //    }
  return $return;
}
?>
<header class="main-nav">
  <!-- <div class="logo-wrapper">
    <div class="back-btn"><i class="fa fa-angle-left"></i></div>
  </div> -->
  <nav>
    <div class="main-navbar">
      <div class="left-arrow" id="left-arrow"><i data-feather="arrow-left"></i></div>
      <div id="mainnav">
        <ul class="nav-menu custom-scrollbar">
          <li class="back-btn">
            <div class="mobile-back text-right"><span>Back</span><i class="fa fa-angle-right pl-2" aria-hidden="true"></i></div>
          </li>
          <li class="dropdown"><a class="nav-link menu-title link-nav" href="<?php echo Yii::app()->baseUrl; ?>/admin/"><i data-feather="home"> </i><span>หน้าหลัก</span></a></li>

          <?php
          $checkQues_question = PermissionsMenu(array(
            'Questionaire.*',
            'Questionaire.question'
          ));
          $checkQues_survey = PermissionsMenu(array(
            'Questionaire.*',
            'Questionaire.survey'
          ));
          $checkQues_excel = PermissionsMenu(array(
            'Questionaire.*',
            'Questionaire.excel'
          ));

          $checkQues_export = PermissionsMenu(array(
            'Questionaire.*',
            'Questionaire.export'
          ));
            $checkCSV = PermissionsMenu(array(
            'survey.*',
            'survey.Excel'
          ));
          if ($checkCSV) { ?>
            <li class="dropdown"><a class="nav-link menu-title" href="#"><i data-feather="list"></i><span>ระบบนำเข้าข้อมูล CSV</span></a>
              <ul class="nav-submenu menu-content">
                <?php if ($checkCSV) { ?>
                  <li><a href="<?php echo Yii::app()->baseUrl . '/admin/survey/excel'; ?>">นำเข้าข้อมูล CSV</a></li>
                <?php } ?>
              </ul>
            </li>
          <? }

          if ($checkQues_question || $checkQues_survey || $checkQues_excel) { ?>

            <li class="dropdown"><a class="nav-link menu-title" href="#"><i data-feather="list"></i><span>ระบบจัดการแบบสอบถาม</span></a>
              <ul class="nav-submenu menu-content">

                <?php if ($checkQues_question) { ?>
                  <li><a href="<?php echo Yii::app()->baseUrl . '/admin/questionaire/question'; ?>">จัดการคำถาม</a></li>
                <?php } ?>
                <?php if ($checkQues_survey) { ?>
                  <li><a href="<?php echo Yii::app()->baseUrl . '/admin/questionaire/survey'; ?>">จัดการแบบสอบถาม</a></li>
                <?php } ?>

                <?php if ($checkQues_excel) { ?>
                  <li><a href="<?php echo Yii::app()->baseUrl . '/admin/questionaire/excel'; ?>">Import Excel</a></li>
                <?php } ?>

                <?php if ($checkQues_export) { ?>
                  <li><a href="<?php echo Yii::app()->baseUrl . '/admin/questionaire/export'; ?>">Export Excel</a></li>
                <?php } ?>
              </ul>
            </li>
          <?php } ?>

          <?php
          $checkCategory = PermissionsMenu(array(
            'Category.*',
            'Category.index'
          ));

          if ($checkCategory) { ?>
            <li class="dropdown"><a class="nav-link menu-title" href="#"><i data-feather="edit"></i><span>ระบบจัดการหมวดหมู่</span></a>
              
              <?php $this->widget('AdminMenu'); ?>
            </li>
          <?php } ?>


          <?php
          $checksysques = PermissionsMenu(array(
            'Sysques.*',
            'Sysques.index'
          ));
          $checkService = PermissionsMenu(array(
            'Service.*',
            'Service.index'
          ));
          $checkProcessing = PermissionsMenu(array(
            'Processing.*',
            'Processing.index'
          ));
          ?>


          <?php $this->widget('UserMenuAdmin'); ?>

          <?php if ($checkProcessing) { ?>
              <li class="dropdown"><a class="nav-link menu-title link-nav" href="<?php echo Yii::app()->baseUrl.'/Processing/index'; ?>"><i data-feather="edit"> </i><span>ระบบการประมวลผลข้อมูล</span></a></li> 
          <?php } ?>

          <!--  <li class="dropdown"><a class="nav-link menu-title" href="#"><i data-feather="edit"></i></i><span>ระบบกำหนดสิทธิ์การใช้งาน</span></a>
            <ul class="nav-submenu menu-content">
              <li><a href="admin-14-1.php">จัดการเจ้าหน้าที่สำนักเขต</a></li>
              <li><a href="admin-14-3.php">จัดการผู้ดูแลระบบ</a></li>
              <li><a href="admin-14-5.php">จัดการกลุ่มผู้ใช้งาน</a></li>
              <li><a href="admin-14-7.php">จัดการสิทธิ์</a></li>
            </ul>
          </li> -->
          <?php
          $checkAuthorities = PermissionsMenu(array(
            'Authorities.index'
          ));
          $checkPgroup = PermissionsMenu(array(
            'Pgroup.index'
          ));
          $checkPcontroller = PermissionsMenu(array(
            'Pcontroller.index'
          ));
          $checkAdminuser = PermissionsMenu(array(
            'Adminuser.index'
          ));
          $checkReport = PermissionsMenu(array(
            'Report.index'
          ));
          $checkReportFormulas = PermissionsMenu(array(
            'Formulas.index'
          ));
          $checkMaster = PermissionsMenu(array(
            'MtProvinceController.*',
            'MtDistrictController.*',
            'MtSubDistrictController.*',
            'MasterVillageController.*',
            'MasterThoseController.*',
            'MtExampleController.*',
            'MasterPatController.*',
            'MasterRelation.*',
          ));

          $checkLogUser = PermissionsMenu(array(
            'LogUser.index'
          ));
          ?>

          <?php if ($checkAuthorities || $checkPgroup || $checkPcontroller || $checkAdminuser) { ?>

            <li class="dropdown"><a class="nav-link menu-title" href="#"><i data-feather="check-square"></i><span>ระบบกำหนดสิทธิ์การใช้งาน</span></a>
              <ul class="nav-submenu menu-content">

                <?php if ($checkAuthorities) { ?>
                  <li><?php echo CHtml::link('จัดการเจ้าหน้าที่สำนักงานเขต', array('//authorities/index')) ?></li>
                  <li><?php echo CHtml::link('จัดการเจ้าหน้าที่สำนักงานเขตลาออก', array('//authorities/resign')) ?></li>
                <?php } ?>

                <?php if ($checkAdminuser) { ?>
                  <li><?php echo CHtml::link('จัดการผู้ดูแลระบบ', array('//adminUser/index')) ?></li>
                <?php } ?>

                <?php if ($checkPgroup) { ?>
                  <li><?php echo CHtml::link('จัดการกลุ่มผู้ใช้งาน', array('//PGroup/index')) ?></li>
                <?php } ?>

                <?php if ($checkPcontroller) { ?>
                  <li><?php echo CHtml::link('จัดการสิทธิ์', array('//PController/index')) ?></li>
                <?php } ?>

              </ul>
            </li>
          <?php } ?>

          <!-- <?php if ($checkReportFormulas) {?>
            <li class="dropdown"><a class="nav-link menu-title link-nav" href="<?php echo Yii::app()->baseUrl.'/formulas/index'; ?>"><i data-feather="printer"> </i><span>จัดการ Report</span></a></li> 
          <?php } ?> -->

          <?php if ($checkReport) { ?>
            <li class="dropdown"><a class="nav-link menu-title" href="#"><i data-feather="printer"> </i><span>ระบบรายงานระดับจังหวัด</span></a>
              <ul class="nav-submenu menu-content">
                <li><?php echo CHtml::link('รายงานการใช้ที่ดิน', array('//ReportProvince/index/1')) ?></li>
                <li><a class="submenu-title" href="#">รายงานข้อมูลพื้นฐานครัวเรือน<span class="sub-arrow"><i class="fa fa-angle-right"></i></span>
                    <div class="according-menu"><i class="fa fa-angle-right"></i></div>
                  </a>
                  <ul class="nav-sub-childmenu submenu-content" style="display: none;">
                    <li><?php echo CHtml::link('- รายงานข้อมูลพื้นฐานครัวเรือน และหัวหน้าครัวเรือนเกษตร', array('//ReportProvince/index/2')) ?></li>
                    <li><?php echo CHtml::link('- รายงานข้อมูลพื้นฐานของประชากรเกษตร', array('//ReportProvince/index/3')) ?></li>
                    <li><?php echo CHtml::link('- รายงานข้อมูลพื้นฐานของแรงงาน', array('//ReportProvince/index/4')) ?></li>
                  </ul>
                </li>
                <li><?php echo CHtml::link('รายงานทรัพย์สิน', array('//ReportProvince/index/5')) ?></li>
                <li><?php echo CHtml::link('รายงานทัศนคติฯ', array('//ReportProvince/Receiptother/6')) ?></li>
                <li><a class="submenu-title" href="#">รายงานประกอบวิเคราะห์น้ำท่วม<span class="sub-arrow"><i class="fa fa-angle-right"></i></span>
                    <div class="according-menu"><i class="fa fa-angle-right"></i></div>
                  </a>
                  <ul class="nav-sub-childmenu submenu-content" style="display: none;">
                    <li><?php echo CHtml::link('- รายงานสรุปรายได้ – รายจ่าย และตัวชี้วัดเศรษฐกิจครัวเรือน', array('//ReportProvince/index/7')) ?></li>
                    <li><?php echo CHtml::link('- รายงานที่มารายได้', array('//ReportProvince/index/8')) ?></li>
                    <li><?php echo CHtml::link('- รายงานที่มารายจ่าย', array('//ReportProvince/index/9')) ?></li>
                  </ul>
                </li>
                <li><a class="submenu-title" href="#">รายงานรายจ่าย<span class="sub-arrow"><i class="fa fa-angle-right"></i></span>
                    <div class="according-menu"><i class="fa fa-angle-right"></i></div>
                  </a>
                  <ul class="nav-sub-childmenu submenu-content" style="display: none;">
                    <li><?php echo CHtml::link('- รายงานองค์ประกอบรายจ่ายเงินสดเกษตรทางพืช', array('//ReportProvince/index/10')) ?></li>
                    <li><?php echo CHtml::link('- รายงานองค์ประกอบรายจ่ายเงินสดเกษตรทางสัตว์', array('//ReportProvince/index/11')) ?></li>
                    <li><?php echo CHtml::link('- รายงานองค์ประกอบรายจ่ายเงินสดเกษตร และบริโภคอุปโภค', array('//ReportProvince/index/12')) ?></li>
                  </ul>
                </li>
                <li><a class="submenu-title" href="#">รายงานรายได้<span class="sub-arrow"><i class="fa fa-angle-right"></i></span>
                    <div class="according-menu"><i class="fa fa-angle-right"></i></div>
                  </a>
                  <ul class="nav-sub-childmenu submenu-content" style="display: none;">
                    <li><?php echo CHtml::link('- รายงานองค์ประกอบรายได้เงินสดเกษตรทางพืช', array('//ReportProvince/index/13')) ?></li>
                    <li><?php echo CHtml::link('- รายงานองค์ประกอบรายได้เงินสดเกษตรทางสัตว์', array('//ReportProvince/index/14')) ?></li>
                    <li><?php echo CHtml::link('- รายงานองค์ประกอบรายได้เงินอื่น', array('//ReportProvince/index/15')) ?></li>
                  </ul>
                </li>
                <li><?php echo CHtml::link('รายงานรายได้ – รายจ่าย', array('//ReportProvince/index/16')) ?></li>
                <li><?php echo CHtml::link('รายงานหนี้สิน', array('//ReportProvince/index/17')) ?></li>
              </ul>
            </li>
            <li class="dropdown"><a class="nav-link menu-title" href="#"><i data-feather="printer"> </i><span>ระบบรายงานระดับประเทศ</span></a>
              <ul class="nav-submenu menu-content">
                <li><?php echo CHtml::link('รายงานการใช้ที่ดิน', array('//ReportRegion/index/1')) ?></li>
                <li><a class="submenu-title" href="#">รายงานข้อมูลพื้นฐานครัวเรือน<span class="sub-arrow"><i class="fa fa-angle-right"></i></span>
                    <div class="according-menu"><i class="fa fa-angle-right"></i></div>
                  </a>
                  <ul class="nav-sub-childmenu submenu-content" style="display: none;">
                    <li><?php echo CHtml::link('- รายงานข้อมูลพื้นฐานครัวเรือน และหัวหน้าครัวเรือนเกษตร', array('//ReportRegion/index/2')) ?></li>
                    <li><?php echo CHtml::link('- รายงานข้อมูลพื้นฐานของประชากรเกษตร', array('//ReportRegion/index/3')) ?></li>
                    <li><?php echo CHtml::link('- รายงานข้อมูลพื้นฐานของแรงงาน', array('//ReportRegion/index/4')) ?></li>
                  </ul>
                </li>
                <li><?php echo CHtml::link('รายงานทรัพย์สิน', array('//ReportRegion/index/5')) ?></li>
                <li><?php echo CHtml::link('รายงานทัศนคติฯ', array('//ReportRegion/Receiptother/6')) ?></li>
                <li><a class="submenu-title" href="#">รายงานประกอบวิเคราะห์น้ำท่วม<span class="sub-arrow"><i class="fa fa-angle-right"></i></span>
                    <div class="according-menu"><i class="fa fa-angle-right"></i></div>
                  </a>
                  <ul class="nav-sub-childmenu submenu-content" style="display: none;">
                    <li><?php echo CHtml::link('- รายงานสรุปรายได้ – รายจ่าย และตัวชี้วัดเศรษฐกิจครัวเรือน', array('//ReportRegion/index/7')) ?></li>
                    <li><?php echo CHtml::link('- รายงานที่มารายได้', array('//ReportRegion/index/8')) ?></li>
                    <li><?php echo CHtml::link('- รายงานที่มารายจ่าย', array('//ReportRegion/index/9')) ?></li>
                  </ul>
                </li>
                <li><a class="submenu-title" href="#">รายงานรายจ่าย<span class="sub-arrow"><i class="fa fa-angle-right"></i></span>
                    <div class="according-menu"><i class="fa fa-angle-right"></i></div>
                  </a>
                  <ul class="nav-sub-childmenu submenu-content" style="display: none;">
                    <li><?php echo CHtml::link('- รายงานองค์ประกอบรายจ่ายเงินสดเกษตรทางพืช', array('//ReportRegion/index/10')) ?></li>
                    <li><?php echo CHtml::link('- รายงานองค์ประกอบรายจ่ายเงินสดเกษตรทางสัตว์', array('//ReportRegion/index/11')) ?></li>
                    <li><?php echo CHtml::link('- รายงานองค์ประกอบรายจ่ายเงินสดเกษตร และบริโภคอุปโภค', array('//ReportRegion/index/12')) ?></li>
                  </ul>
                </li>
                <li><a class="submenu-title" href="#">รายงานรายได้<span class="sub-arrow"><i class="fa fa-angle-right"></i></span>
                    <div class="according-menu"><i class="fa fa-angle-right"></i></div>
                  </a>
                  <ul class="nav-sub-childmenu submenu-content" style="display: none;">
                    <li><?php echo CHtml::link('- รายงานองค์ประกอบรายได้เงินสดเกษตรทางพืช', array('//ReportRegion/index/13')) ?></li>
                    <li><?php echo CHtml::link('- รายงานองค์ประกอบรายได้เงินสดเกษตรทางสัตว์', array('//ReportRegion/index/14')) ?></li>
                    <li><?php echo CHtml::link('- รายงานองค์ประกอบรายได้เงินอื่น', array('//ReportRegion/index/15')) ?></li>
                  </ul>
                </li>
                <li><?php echo CHtml::link('รายงานรายได้ – รายจ่าย', array('//ReportRegion/index/16')) ?></li>
                <li><?php echo CHtml::link('รายงานหนี้สิน', array('//ReportRegion/index/17')) ?></li>
              </ul>
            </li>
          <?php } ?>
          <?php if($checkLogUser){?>
            <li class="dropdown"><a class="nav-link menu-title" href="#"><i data-feather="printer"></i><span>ระบบเก็บ Log การใช้งาน</span></a>
                <ul class="nav-submenu menu-content">
                    <li><?php echo CHtml::link('Log การใช้งานเจ้าหน้าที่', array('//LogUser/Users')) ?></li>
                </ul> 
            </li>
          <?php } ?>

          <?php if ($checkMaster) { ?>

            <li class="dropdown"><a class="nav-link menu-title" href="#"><i data-feather="database"></i><span>Master Data</span></a>
              <ul class="nav-submenu menu-content">

                <li><?php echo CHtml::link('จัดการข้อมูลจังหวัด', array('//MtProvince/index')) ?></li>

                <li><?php echo CHtml::link('จัดการข้อมูลอำเภอ', array('//MtDistrict/index')) ?></li>

                <li><?php echo CHtml::link('จัดการข้อมูลตำบล', array('//MtSubDistrict/index')) ?></li>

                <li><?php echo CHtml::link('จัดการข้อมูลหมู่บ้าน', array('//MasterVillage/index')) ?></li>

                <li><?php echo CHtml::link('จัดการข้อมูลพวกที่', array('//MasterThose/index')) ?></li>

                <li><?php echo CHtml::link('จัดการข้อมูลตัวอย่างที่', array('//MtExample/index')) ?></li>

                <li><?php echo CHtml::link('จัดการประเภทพืช-สัตว์', array('//MasterPat/index')) ?></li>

                <li><?php echo CHtml::link('จัดการความสัมพันธ์กับหัวหน้าครัวเรือน', array('//MasterRelation/index')) ?></li>

              </ul>
            </li>
          <?php } ?>


        </ul>

        <!--ระบบกำหนดสิทธิ์การใช้งาน-->

      </div>
      <div class="right-arrow" id="right-arrow"><i data-feather="arrow-right"></i></div>
    </div>
  </nav>
</header>