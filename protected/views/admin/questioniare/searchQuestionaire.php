 <div class="table-responsive">
                            <table class="table table-striped table-main" id="">
                                <!-- <table class="table table-striped table-main" id="basic-2"> -->
                                <thead>
                                    <tr>
                                        <th width="5%">ลำดับ</th>
                                        <th>ปีแบบสอบถาม</th>
                                        <th>แบบสอบถาม</th>
                                        <th>จัดฟอร์ม</th>
                                        <th width="25%">จัดการ</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i = 1;
                                        foreach($survey as $key => $row){
                                            echo '
                                            <tr>
                                                <td>'.$i++.'</td>
                                                <td>'.($row['year']+543).'</td>
                                                <td>'.$row['name'].'</td>
                                                <td>
                                                    <a href="'.Yii::app()->baseurl.'/admin/questionaire/survey/'.$row['id'].'/form">
                                                        <button class="btn btn-md btn-search text-white mt-0" type="submit">จัดฟอร์ม</button>
                                                    </a> 
                                                </td>
                                                <td>
                                                    <a type="button" class="btn btn-icon text-success" href="'.Yii::app()->baseurl.'/admin/questionaire/survey/'.$row['id'].'/edit"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit"><path d="M20 14.66V20a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V6a2 2 0 0 1 2-2h5.34"></path><polygon points="18 2 22 6 12 16 8 16 8 12 18 2"></polygon></svg></a>
                                                    <button type="button" class="btn btn-icon text-danger" onclick="sendDelete('.$row['id'].', \''.$row['name'].'\')"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path></svg></button>
                                                </td>
                                            </tr>
                                        ';
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
  <script type="text/javascript">
                           $(document).ready(function(){
                                   feather.replace();
                                });
                         </script>