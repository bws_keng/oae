 
 
                <div class="container-fluid">

                    <div class="row">
                        <div class="col-lg-12 set-height set-padding">
                            <div id="section1">
                                <div class="card">
                                    <div class="card-header">
                                        <h5 class="mb-1">หมวดที่ 8 ภาวะหนี้สิน สินเชื่อ และการชำระคืนของครัวเรือนเกษตร (ปี พ.ศ.2562 - พ.ศ.2563)</h5>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-body">

                                        <form>
                                            <div class="row">
                                                <div class="col-md-2 mb-3">
                                                    <label for="">จังหวัด</label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-2 mb-3">
                                                    <label for="">อำเภอ</label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-2 mb-3">
                                                    <label for="">ตำบล</label>
                                                    <div class="input-group">
                                                        <input class="form-control" id="" type="text" placeholder="" aria-describedby="" required="" data-original-title="" title="">
                                                    </div>
                                                </div>
                                                <div class="col-md-2 mb-3">
                                                    <label for="">หมู่ที่</label>
                                                    <div class="input-group">
                                                        <input class="form-control" id="" type="text" placeholder="" aria-describedby="" required="" data-original-title="" title="">
                                                    </div>
                                                </div>
                                                <div class="col-md-2 mb-3">
                                                    <label for="">พวกที่</label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-2 mb-3">
                                                    <label for="">ตัวอย่างที่</label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                            </div>
                                        </form>

                                    </div>
                                </div>
                            </div>

                            <div id="section2">

                                <h5 class="mb-4"> &nbsp;
                                    <button class="btn btn-lg btn-secondary btn-note float-right mb-2" type="button" data-toggle="modal" data-target=".bd-example-modal-lg"><i class="fa fa-list"></i> &nbsp;รายการ</button>
                                    <div class="modal fade bd-example-modal-lg modal-main" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h6 class="modal-title" id="myLargeModalLabel">รายการ</h6>
                                                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="card">
                                                                <div class="card-header">
                                                                    <label for="">(1) ลักษณะสินเชื่อ</label>
                                                                </div>
                                                                <div class="card-body">
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <small for="">1. หนี้สินต้นปี ณ 1 เม.ย. 62</small>
                                                                        </div>
                                                                        <div class="col-md-12">
                                                                            <small for="">2. กู้ยืมระหว่างปีเพาะปลูก ณ 1 เม.ย. 2562 ถึง 30 มี.ค. 2563</small>
                                                                        </div>
                                                                        <div class="col-md-12">
                                                                            <small for="">3. <span class="text-danger">หนี้สินหมุนเวียน เป็นหนี้ที่มีการใช้คืนและกู้ต่อระหว่างปี โดยชำระดอกเบี้ย เช่น หนี้กองทุนหมู่บ้าน ข้อ 10 ในสดมภ์ที่
                                                                                    (6) แหล่งกู้ยืม</span></small>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="card">
                                                                <div class="card-header">
                                                                    <label for="">(5) ระยะเวลากู้ยืม</label>
                                                                </div>
                                                                <div class="card-body">
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <small for="">1. ระยะสั้น น้อยกว่า 1 ปี</small>
                                                                        </div>
                                                                        <div class="col-md-12">
                                                                            <small for="">2. ระยะสั้น มากกว่า 1 ปี แต่น้อยกว่า 5 ปี ขึ้นไป</small>
                                                                        </div>
                                                                        <div class="col-md-12">
                                                                            <small for="">3. ระยะสั้น มากกว่า 5 ปี ขึ้นไป</small>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="card">
                                                                <div class="card-header">
                                                                    <label for="">(6) แหล่งกู้ยืมครอบครัว/คนใกล้ชิด</label>
                                                                </div>
                                                                <div class="card-body">
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <small for="">1. ญาติพี่น้อง</small>
                                                                        </div>
                                                                        <div class="col-md-12">
                                                                            <small for="">2. เพื่อนฝูง/เพื่อนบ้าน</small>
                                                                        </div>
                                                                        <label class="mt-2 mb-2"><u>นายทุน/พ่อค้า</u></label>
                                                                        <div class="col-md-12">
                                                                            <small for="">3. เจ้าของที่ดิน/โรงสี/โรงงาน/ร้านค้า/ผู้ขายปัจจัยการผลิต/
                                                                                ผู้รับซื้อผลผลิต</small>
                                                                        </div>
                                                                        <div class="col-md-12">
                                                                            <small for="">4. นายทุนกู้นอกระบบ</small>
                                                                        </div>
                                                                        <div class="col-md-12">
                                                                            <small for="">5. แหล่งเงินกู้อื่นๆนอกระบบ ระบุ.......</small>
                                                                        </div>
                                                                        <label class="mt-2 mb-2"><u>สถาบันการเงินในระบบ</u></label>
                                                                        <div class="col-md-12">
                                                                            <small for="">6. ธ.ก.ส</small>
                                                                        </div>
                                                                        <div class="col-md-12">
                                                                            <small for="">7. ธนาคารออมสิน ธนาคารอาคารสงเคราะห์
                                                                                ธนาคารพานิชย์อื่นๆ</small>
                                                                        </div>
                                                                        <div class="col-md-12">
                                                                            <small for="">8. บริษัทเงินทุนอื่นๆ</small>
                                                                        </div>
                                                                        <label class="mt-2 mb-2"><u>การรวมกลุ่ม</u></label>
                                                                        <div class="col-md-12">
                                                                            <small for="">9. สหกรณ์</small>
                                                                        </div>
                                                                        <div class="col-md-12">
                                                                            <small for="">10. <span class="text-danger">กองทุนหมู่บ้าน(ลักษณะที่ 3)</span></small>
                                                                        </div>
                                                                        <div class="col-md-12">
                                                                            <small for="">11. กลุ่มออมทรัพย์</small>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="card">
                                                                <div class="card-header">
                                                                    <label for="">(7) วัตถุประสงค์ในการกู้ยืมหลัก</label>
                                                                </div>
                                                                <div class="card-body">
                                                                    <div class="row">
                                                                        <small class="text-danger">**หมายเหตุ : แยกรายการเงินกู้ ออกตามวัตถุประสงค์ในการกู้ยืมหลัก</small>
                                                                        <label class="mt-2 mb-2"><u>ในการเกษตร</u></label>
                                                                        <div class="col-md-12">
                                                                            <small for="">1. ซื้อที่ดินทางการเกษตร</small>
                                                                        </div>
                                                                        <div class="col-md-12">
                                                                            <small for="">2. ซื้อเครื่องจักรกล/เครื่องมือทางการเกษตร</small>
                                                                        </div>
                                                                        <div class="col-md-12">
                                                                            <small for="">3. ซื้อวัสดุอุปกรณ์ทางการเกษตร</small>
                                                                        </div>
                                                                        <div class="col-md-12">
                                                                            <small for="">4. ซื้อปัจจัยการผลิต เช่น พันธุ์ ปุ๋ย ยา อาหารสัตว์</small>
                                                                        </div>
                                                                        <div class="col-md-12">
                                                                            <small for="">5. ค่าใช้จ่ายอื่นๆ เช่น ค่าซ่อม/ค่าบำรุงรักษาทรัพย์สินทางการเกษตร ระบุ.......</small>
                                                                        </div>
                                                                        <label class="mt-2 mb-2"><u>นอกการเกษตร</u></label>
                                                                        <div class="col-md-12">
                                                                            <small for="">6. ซื้อที่ดิน/ทรัพย์สินอื่นๆ นอกจากการเกษตร เช่น รถยนต์ รถจักรยานยนต์ เครื่องใช้ไฟฟ้า</small>
                                                                        </div>
                                                                        <div class="col-md-12">
                                                                            <small for="">7. ลงทุนในธุรกิจ/กิจกรรมนอกการเกษตร</small>
                                                                        </div>
                                                                        <div class="col-md-12">
                                                                            <small for="">8. ค่าใช้จ่ายการศึกษา</small>
                                                                        </div>
                                                                        <div class="col-md-12">
                                                                            <small for="">9. ค่าใช้จ่ายเพื่อการอุปโภคบริโภคในครัวเรือน</small>
                                                                        </div>
                                                                        <div class="col-md-12">
                                                                            <small for="">10. ค่าใช้จ่ายงานพิธีกรรม งานแต่ง งานบวช งานศพ</small>
                                                                        </div>
                                                                        <div class="col-md-12">
                                                                            <small for="">11. ค่าใช้จ่ายอื่นๆ เช่น ค่ารักษาพยาบาล ค่าซ่อม
                                                                                ค่าบำรุงรักษาทรัพย์สินนอกการเกษตร ระบุ.......</small>
                                                                        </div>
                                                                        <label class="mt-2 mb-2"><u>วัตถุประสงค์อื่นๆ</u></label>
                                                                        <div class="col-md-12">
                                                                            <small for="">12. ใช้คืนหนี้สินเดิม</small>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="card">
                                                                <div class="card-header">
                                                                    <label for="">(8) หลักประกันการกู้ยืม</label>
                                                                </div>
                                                                <div class="card-body">
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <small for="">1. ไม่ต้องมีหลักประกันใดๆ/ใช้ตนเองค้ำประกัน</small>
                                                                        </div>
                                                                        <div class="col-md-12">
                                                                            <small for="">2. ใช้เงินออม/เงินฝากของตนเอง</small>
                                                                        </div>
                                                                        <div class="col-md-12">
                                                                            <small for="">3. ใช้บุคคลอื่นค้ำประกัน</small>
                                                                        </div>
                                                                        <div class="col-md-12">
                                                                            <small for="">4. ใช้กลุ่มค้ำประกันให้</small>
                                                                        </div>
                                                                        <div class="col-md-12">
                                                                            <small for="">5. ใช้ที่ดิน/หลักทรัพย์อื่นๆ</small>
                                                                        </div>
                                                                        <div class="col-md-12">
                                                                            <small for="">6. ใช้ผลผลิต</small>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="card">
                                                                <div class="card-header">
                                                                    <label for="">(10) เหตุผลการค้างชำระ</label>
                                                                </div>
                                                                <div class="card-body">
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <small for="">1. ยังไม่ถึงกำหนดชำระ</small>
                                                                        </div>
                                                                        <div class="col-md-12">
                                                                            <small for="">2. รอการขายผลผลิต</small>
                                                                        </div>
                                                                        <div class="col-md-12">
                                                                            <small for="">3. นำรายได้ไปใช้ด้านอื่น</small>
                                                                        </div>
                                                                        <div class="col-md-12">
                                                                            <small for="">4. พืชผลเสียหาย</small>
                                                                        </div>
                                                                        <div class="col-md-12">
                                                                            <small for="">5. พักชำระหนี้/ลดภาระหนี้</small>
                                                                        </div>
                                                                        <div class="col-md-12">
                                                                            <small for="">6. ปรับโครงสร้างหน้านี้</small>
                                                                        </div>
                                                                        <div class="col-md-12">
                                                                            <small for="">7. <span class="text-danger">หนี้สินหมุนเวียนกู้ยืมต่อ</span></small>
                                                                        </div>
                                                                        <div class="col-md-12">
                                                                            <small for="">8. อื่นๆ ระบุ.......</small>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </h5>

                                <div class="card">
                                    <div class="card-body">

                                        <form>
                                            <div class="row">
                                                <div class="col-md-4 mb-3">
                                                    <label for="">ลักษณะสินเชื่อ<small class="text-danger">(1)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="1-3" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">ปีที่กู้ พ.ศ.2563<small class="text-danger">(2)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="เลข 2 หลัก" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">เงินต้นรวมดอกเบี้ย <small class="text-danger">(บาท)(3)</small></label>
                                                    <div class="input-group">
                                                        <input class="form-control" id="" type="text" placeholder="บาท" aria-describedby="" required="" data-original-title="" title="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4 mb-3">
                                                    <label for="">อัตราดอกเบี้ย <small class="text-danger">(4)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="(%ต่อปี)" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">ระยะเวลากู้ยืม <span class="text-danger">สั้น/กลาง/ยาว</span><small class="text-danger">(5)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="(1-3)" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">แหล่งกู้ยืม <small class="text-danger">(6)</small></label>
                                                    <div class="input-group">
                                                        <input class="form-control" id="" type="text" placeholder="(1-12)" aria-describedby="" required="" data-original-title="" title="">
                                                    </div>
                                                </div>
                                                <div class="col-md-8 mb-3">
                                                    <label for="">วัตถุประสงค์ในการกู้ยืมหลัก <small class="text-danger">(7)</small></label>
                                                    <div class="input-group">
                                                        <input class="form-control" id="" type="text" placeholder="(1-12)" aria-describedby="" required="" data-original-title="" title="">
                                                    </div>
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">หลักประกันการกู้ยืม <small class="text-danger">(8)</small></label>
                                                    <div class="input-group">
                                                        <input class="form-control" id="" type="text" placeholder="(1-6)" aria-describedby="" required="" data-original-title="" title="">
                                                    </div>
                                                </div>
                                                <div class="col-md-8 mb-3">
                                                    <label for="">การชำระหนี้คืน <span class="text-danger">เงินต้นรวมดอกเบี้ยตามที่จ่ายจริง (บาท) </span><small class="text-danger">(9)</small></label>
                                                    <div class="input-group">
                                                        <input class="form-control" id="" type="text" placeholder="" aria-describedby="" required="" data-original-title="" title="">
                                                    </div>
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">เหตุผลการค้างชำระ <small class="text-danger">(10)</small></label>
                                                    <div class="input-group">
                                                        <input class="form-control" id="" type="text" placeholder="" aria-describedby="" required="" data-original-title="" title="">
                                                    </div>
                                                </div>
                                                </div>
                                               

                                            <div class="row">
                                                <div class="col-md-8 mb-3">
                                                    <label for="">หนี้สินค้างชำระปลายปี <span class="text-danger">เงินต้นรวมดอกเบี้ย</span> <small class="text-danger">(11)</small></label>
                                                    <div class="input-group">
                                                        <input class="form-control" id="" type="text" placeholder="" aria-describedby="" required="" data-original-title="" title="">
                                                    </div>
                                                </div>
                                            </div>


                                        </form>

                                    </div>
                                </div>
                            </div>

                            <div class="row text-center group-submit">
                                <button type="button" class="btn btn-primary btn-lg">บันทึกข้อมูล</button>
                                <button type="button" class="btn btn-outline-secondary btn-lg">ยกเลิก</button>
                            </div>
                        </div>
                    </div>
                </div>


 