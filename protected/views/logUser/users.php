
  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="http://cdn.datatables.net/1.10.2/css/jquery.dataTables.min.css"></style>
<script type="text/javascript" src="http://cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js"></script>

                <div class="container-fluid">

                    <!-- start row -->
                    <div class="row">
                        <div class="col-lg-12 set-height set-padding">
                            <div id="section1">
                                <div class="card">
                                    <div class="card-header">
                                        <h5 class="mb-1">Log การใช้งานเจ้าหน้าที่</h5>
                                    </div>
                                </div>
                                <div class="card table-card">
                                    <div class="card-body">
                                        <div class="table-responsive" >
                                            <table class="table table-striped table-main" id="myTable" >
                                                <!-- <table class="table table-striped table-main" id="basic-2"> -->
                                                <thead>
                                                    <tr>
                                                        <th width="10%">ลำดับ</th>
                                                        <th width="20%">รหัสเจ้าหน้าที่</th>
                                                        <th width="30%">ชื่อนาม - สกุล</th>
                                                        <th width="10%">ฟังก์ชั่นการใช้งาน</th>
                                                        <th width="10%">รายละเอียด</th>
                                                        <th width="50%">วันและเวลา</th>
                                                    </tr>
                                                </thead>
                                                <tbody >
                                                    <?php
                                                    $i = 1;
                                                    foreach($data as $row){
                                                        // var_dump($data);exit();
                                                        $profile = profile::model()->findBypk($row['user_id']);
                                                        $Users = Users::model()->findBypk($row['user_id']);

                                        // var_dump($profile);exit();
                                                    
                                                            // $year = $row['year'] + 543;
                                                            // $survey_time = explode("-", $row['survey_time']);
                                                            // $survey_0 = $survey_time[0] + 543;
                                                            // $survey_time = $survey_time[2]."-".$survey_time[1]."-".$survey_0;

                                                        // $District = MtDistrict::model()->find(array('condition' => 'province_id =\''.$row['id'].'\' AND district_code =\''. $row["district"] .'\' AND active=1'));

                                                        // $SubDistrict = MtSubDistrict::model()->find(array('condition' => 'district_id =\''.$District->id.'\' AND subdistrict_code =\''. $row["subdistrict"] .'\' AND active=1'));

                                                        // $Village = masterVillage::model()->find(array('condition' => 'subdistrict_id =\''.$SubDistrict->id.'\' AND village_code =\''. $row["village"] .'\' AND active=1'));
                                                    
                                                        // $Those = MasterThose::model()->find(array('condition' => 'those_id =\''.$row["group"].'\' AND active=1'));

                                                        // // $VillageName = !empty($Village) ? $Village->village_name : null ;
                                                        // // $VillageCode = !empty($Village) ? $Village->village_code : null ;
                                                        // // $ThoseName = !empty($Those) ? $Those->those_name : null ;

                                                        // $questionnaire_id = $row['questionnaire_id'];
                                                        // $questionnaire_format = $questionnaire_id.'.jpg';
                                                        // $file_path = $_SERVER['DOCUMENT_ROOT'].Yii::app()->baseurl.'/uploads/'.$questionnaire_format;

                                                        echo '<tr>
                                                            <td>'.$i++.'</td>
                                                            <td>'.$profile->staff_id.'</td>
                                                            <td>'.$profile->fullname.'</td>
                                                            <td>'.$row['controller'].'</td>
                                                            <td>'.$row['action'].'</td>
                                                            <td>'.$row['create_date'].'</td>
                                                        </tr>';
                                                
                                                    }
                                                    ?>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="table-responsive2" style="display: none;"></div>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                    <!-- end row -->
                    <div class="container position-fixed fixed-top pt-5" id="deleteForm" style="display:none;">
        <div class="card col-12 p-0">
            <div class="card-header">
                <h5>ลบข้อมูล</h5>
            </div>
            <div class="card-body">
                <h5 class="card-title">เมื่อข้อมูลถูกลบจะไม่สามารถย้อนคืนได้</h5>
                <div class="card-text">
                    <form method="POST" action="<?php echo Yii::app()->baseurl; ?>/questionnaire/delete">
                        <label for="deleteKey">รหัสอ้างอิง</label>
                        <input id="deleteKey" name="key" class="form-control" value="" readonly/>
                        <label for="deleteValue">ข้อมูล</label>
                        <input id="deleteValue" name="value" class="form-control" value="" readonly/>
                        <button class="btn btn-danger btn-lg mt-2">ลบข้อมูล</button>
                        <button type="button" class="btn btn-light btn-lg mt-2" onclick='$("#deleteForm").hide();'>ยกเลิก</button>
                    </form>
                </div>
            </div>
        </div>

    </div>
                </div>

<script type="text/javascript">
$(document).ready(function(){
    $('#myTable').dataTable({
      responsive: true,
        "pageLength": 10,
        "searching": false,
      "info": false,
      "lengthChange": false

    });

});

  $("#search-data").click(function(){

    var year = $("#search_year").val();
    var province = $("#search_Province").val();
    var district = $("#search_District").val();
    var subdistrict = $("#search_subdistrict").val();
    var village = $("#search_village").val();
    // if (typeof  year != 'undefined' || typeof  province != 'undefined' || typeof  district != 'undefined' || typeof  subdistrict != 'undefined' ||typeof  village != 'undefined') {
   if (year != null && province != null) {
      $.ajax({
                type: 'GET',
                url: "<?= $this->createUrl('/Questionnaire/SearchData'); ?>", 
                data: {
                    year: year,
                    province: province,
                    district: district,
                    subdistrict: subdistrict,
                    village: village,
                },
                success: function(data) {
        
                  $(".table-responsive").remove();
                  $(".table-responsive2").show();
                  $(".table-responsive2").html(data);
                }
            });
    }else{
      alert("กรุณาเลือกปีเพาะปลูกและเลือกจังหวัด");
    }
  });

    $("#search_Province").change(function() {
        var id = $("#search_Province option:selected").attr('data-id');

        if (id != "") {
            $.ajax({
                type: 'POST',
                url: "<?= $this->createUrl('/Questionnaire/SearchDistrict'); ?>", 
                data: {
                    id: id,
                },
                success: function(data) {

                $('#search_District').empty();
                $('#search_subdistrict').empty();
                $('#search_village').empty();
                $('#search_District').append(data);

                }
            });
        }
    });
    $("#search_District").change(function() {
        var id = $("#search_District option:selected").attr('data-id');
        if (id != "") {
            $.ajax({
                type: 'POST',
                url: "<?= $this->createUrl('/Questionnaire/SearchSubDistrict'); ?>", 
                data: {
                    id: id,
                },
                success: function(data) {

                $('#search_subdistrict').empty();
                $('#search_village').empty();
                $('#search_subdistrict').append(data);

                }
            });
        }
    });

    $("#search_subdistrict").change(function() {
        var id = $("#search_subdistrict option:selected").attr('data-id');
        if (id != "") {
            $.ajax({
                type: 'POST',
                url: "<?= $this->createUrl('/Questionnaire/SearchVillage'); ?>", 
                data: {
                    id: id,
                },
                success: function(data) {
                $('#search_village').empty();
                $('#search_village').append(data);

                }
            });
        }
    });

    $("#search_Province").select2( {
     placeholder: "เลือกจังหวัด",
     allowClear: true
     });

    $("#search_District").select2( {
     placeholder: "เลือกอำเภอ",
     allowClear: true
     });

    $("#search_subdistrict").select2( {
     placeholder: "เลือกตำบล",
     allowClear: true
     });

    $("#search_village").select2( {
     placeholder: "เลือกหมู่ที่",
     allowClear: true
     });

//    $(function() {
//   $('.table-responsive').on('shown.bs.dropdown', function(e) {
//     var t = $(this),
//       m = $(e.target).find('.dropdown-menu'),
//       tb = t.offset().top + t.height(),
//       mb = m.offset().top + m.outerHeight(true),
//       d = 20; // Space for shadow + scrollbar.   
//     if (t[0].scrollWidth > t.innerWidth()) {
//       if (mb + d > tb) {
//         t.css('padding-bottom', ((mb + d) - tb));
//       }
//     } else {
//       t.css('overflow', 'visible');
//     }
//   }).on('hidden.bs.dropdown', function() {
//     $(this).css({
//       'padding-bottom': '',
//       'overflow': ''
//     });
//   });
// });
</script>
 
<style type="text/css">
  .table-responsive2 {
  z-index: 999;
  overflow-y: auto !important;
}
  .dropbtn {
    background-color: #8DBF42;
    color: white;
    padding: 16px;
    font-size: 16px;
    border: none;
  }

  .dropdown {
    position: relative;
    display: inline-block;
  }

  .dropdown-content {
    display: none;
    position: absolute;
    background-color: #f1f1f1;
    min-width: 160px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
  }

  .dropdown-content a {
    color: black;
    padding: 12px 16px;
    text-decoration: none;
    display: block;
  }

  .dropdown-content a:hover {background-color: #ddd;}

  .dropdown:hover .dropdown-content {display: block;}

  .dropdown:hover .dropbtn {background-color: #3e8e41;}
</style>
