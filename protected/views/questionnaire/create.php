<form action="<?php echo Yii::app()->baseUrl . "/questionnaire"; ?>" method="POST" enctype="multipart/form-data">
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl; ?>/theme/assets/js/bootstrap-daterangepicker/jquery.datetimepicker.css">
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/theme/assets/js/bootstrap-daterangepicker/jquery.datetimepicker2.full.min.js"></script>
    <div class="container-fluid">


        <!-- start row -->
        <div class="row">
            <div class="col-lg-12 set-height set-padding">
                <div id="section1">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="mb-1">แบบสอบถามภาวะเศรษฐกิจสังคมครัวเรือนและแรงงานเกษตร </h5>
                            <?php
                            $start_explode = explode("-", $start);
                            $start_sum = $start_explode[0] + 543;
                            $start_format = $start_explode[2].'-'.$start_explode[1].'-'.$start_sum;

                            $end_explode = explode("-", $end);
                            $end_sum = $end_explode[0] + 543;
                            $end_format = $end_explode[2].'-'.$end_explode[1].'-'.$end_sum;
                            ?>
                            <p class="mb-0">ระหว่างวันที่ <?php echo $start_format; ?> ถึง <?php echo $end_format; ?></p>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <h4 class="mb-4 text-main"><i class="icofont icofont-ui-user"></i> เจ้าหน้าที่สำรวจ</h4>
                            <hr>
                            <div>
                                <div class="row">
                                    <div class="col-md-4 mb-3">
                                        <label for="">ชื่อ<span class="required">*</span></label>
                                        <input class="form-control" id="" required type="text" name="staff_name">
                                    </div>
                                    <div class="col-md-4 mb-3">
                                        <label for="">นามสกุล<span class="required">*</span></label>
                                        <input class="form-control" id="" required type="text" name="staff_lname">
                                    </div>
                                    <div class="col-md-4 mb-3">
                                        <label for="">รหัส<span class="required">*</span></label>
                                        <div class="input-group">
                                            <input class="form-control" name="staff_id" id="" type="text" placeholder="" aria-describedby="inputGroupPrepend2" required data-original-title="" title="" name="staff_id">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 mb-3">
                                        <label for="">วัน/เดือน/ปี<span class="required">*</span></label>
                               <!--          <input class="form-control digits" type="date" value="" name="survey_time"> -->
                                            <input class="form-control datetimepicker" type="text" value="" required name="survey_time" autocomplete="off"> 
                                                     <!-- <div class="date-picker">
                                                        <div class="input-group">
                                                            <input class="datepicker-here form-control digits" type="text" data-language="th-th">
                                                        </div>
                                                        </div>
                                                    </div>  -->
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div id="section2">
                                <div class="card">
                                    <div class="card-body">
                                        <div>
                                            <div class="row acc-1">
                                                <div class="col-md-8 mb-3">
                                                    <label for=""><strong class="text-main">1.</strong> สำนักงานเศรษฐกิจการเกษตรที่ </label>
                                                    <?php
                                                    $User = User::model()->findByPk(Yii::app()->user->id);
                                                    $profile = $User->profile;
                                                    if ($User->superuser != 1) { ?>
                                                        <input class="form-control" id="master-1-out" type="text" readonly>
                                                   <?php }else{ ?>
                                                        <input class="form-control" id="master-1-out" type="text" disabled="disabled">
                                                  <?php 
                                                    }
                                                    ?>
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">สศท. <small class="text-danger">(เลข 2 หลัก)</small><span class="required">*</span></label>
                                                    <?php
                                                    if ($User->superuser != 1) {
                                                    ?>
                                                        <input class="form-control " id="master-1" maxlength="2" type="text" required name="office"  data-master="county" value="<?= $profile->county_id ?>" readonly>
                                                    <?php
                                                    }else{ ?>
                                                        <input class="form-control " id="master-1" maxlength="2" type="text" required name="office"  data-master="county">
                                                    <?php
                                                    }
                                                    ?>
                                                </div>
                                                
                                                <div class="col-12" role="alert" style="display:none;" id="master-1-alert">
                                                    <p class="text-danger">ข้อมูลผิดพลาด</p>
                                                </div>
                                            </div>

                                            <div class="row acc-1">
                                                <div class="col-md-8 mb-3">
                                                    <label for=""><strong class="text-main">2.</strong> จังหวัด </label>
                                                    <input class="form-control" id="province-out" type="text" disabled="disabled">
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">จังหวัด <small class="text-danger">(เลข 3 หลัก)</small><span class="required">*</span></label>
                                                    <input class="form-control " id="province" maxlength="3" type="text" required name="province" data-master="province">
                                                </div>
                                                
                                                <div class="col-12" role="alert" style="display:none;" id="province-alert">
                                                    <p class="text-danger">ข้อมูลผิดพลาด</p>
                                                </div>
                                            </div>

                                            <div class="row acc-1">
                                                <div class="col-md-8 mb-3">
                                                    <label for=""><strong class="text-main">3.</strong> อำเภอ </label>
                                                    <input class="form-control " id="district-out" type="text" disabled="disabled">
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">อำเภอ <small class="text-danger">(เลข 2 หลัก)</small><span class="required">*</span></label>
                                                    <input class="form-control" id="district" maxlength="2" type="text" required name="district" data-master="district">
                                                </div>
                                                
                                                <div class="col-12" role="alert" style="display:none;" id="district-alert">
                                                    <p class="text-danger">ข้อมูลผิดพลาด</p>
                                                </div>
                                            </div>

                                            <div class="row acc-1">
                                                <div class="col-md-8 mb-3">
                                                    <label for=""><strong class="text-main">4.</strong> ตำบล </label>
                                                    <input class="form-control" id="subdistrict-out" type="text" disabled="disabled">
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">ตำบล <small class="text-danger">(เลข 2 หลัก)</small><span class="required">*</span></label>
                                                    <input class="form-control" id="subdistrict" maxlength="2" type="text" required name="subdistrict" data-master="subdistrict">
                                                </div>
                                                
                                                <div class="col-12" role="alert" style="display:none;" id="subdistrict-alert">
                                                    <p class="text-danger">ข้อมูลผิดพลาด</p>
                                                </div>
                                            </div>

                                            <div class="row acc-1">
                                                <div class="col-md-4 mb-3">
                                                    <label for=""><strong class="text-main">5.</strong> หมู่บ้าน </label>
                                                    <input class="form-control" id="village-out" type="text" disabled="disabled">
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">หมู่ที่ <small class="text-danger">(เลข 2 หลัก)</small><span class="required">*</span></label>
                                                    <input class="form-control text-center" maxlength="2" id="village" type="text" required name="village" data-master="village">
                                                </div>
                                                
                                                <div class="col-md-4 mb-3">
                                                    <label for=""> บ้านเลขที่<span class="required">*</span> </label>
                                                    <input class="form-control" required type="text" name="addressNumber">
                                                </div>
                                                <div class="col-12" role="alert" style="display:none;" id="village-alert">
                                                    <p class="text-danger">ข้อมูลผิดพลาด</p>
                                                </div>
                                            </div>

                                            <div class="row acc-1">
                                                <div class="col-md-8 mb-3">
                                                    <label for=""><strong class="text-main">6.</strong> พวกที่ </label>
                                                    <input class="form-control" id="master-6-out" type="text" disabled="disabled">
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">พวกที่ <small class="text-danger">(เลข 1 หลัก)</small><span class="required">*</span></label>
                                                    <input class="form-control" id="master-6" maxlength="1" type="text" required name="group"  data-master="group">
                                                </div>
                                                <div class="col-12" role="alert" style="display:none;" id="master-6-alert">
                                                    <p class="text-danger">ข้อมูลผิดพลาด</p>
                                                </div>
                                            </div>

                                            <div class="row acc-1">
                                                <div class="col-md-12">
                                                    <label><strong class="text-main">7.</strong> ชื่อผู้ขึ้นทะเบียนเกษตรกร </label>
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">ชื่อ</label>
                                                    <input class="form-control" id="" type="text" name="farmer_name">
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">นามสกุล </label>
                                                    <input class="form-control" id="" type="text" name="farmer_lname">
                                                </div>

                                                <div class="col-md-4 mb-3">
                                                    <label for="">ตัวอย่างที่ <small class="text-danger">(เลข 1 หลัก)</small></label>
                                                    <input class="form-control" id="" maxlength="1" type="text" name="sample">
                                                </div>

                                                <div class="col-md-8 mb-3">
                                                    <label for=""> เลขบัตรประจำตัวประชาชน </label>
                                                    <input class="form-control" id="" type="text" name="farmer_id">
                                                </div>

                                                <div class="col-md-4 mb-3">
                                                    <label for="">ผู้ขึ้นทะเบียนเกษตร <small class="text-danger">มี=1/ ไม่มี=2</small></label>
                                                    <input class="form-control" id="" maxlength="1" type="text" name="farmer_registered">
                                                </div>
                                            </div>

                                            <div class="row acc-1"> 
                                                <div class="col-md-12">
                                                    <div class="checkbox checkbox-primary checkbox-inline">
                                                        <input type="checkbox" name="parent_farmer" id="parent_farmer" value="y">
                                                        <label for="parent_farmer" class="bg-primary text-black">ชื่อผู้ขึ้นทะเบียนเกษตรกร </label>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <label><strong class="text-main">8.</strong> ชื่อผู้ให้ข้อมูล </label>
                                                </div>
                                               
                                                <div class="col-md-4 ">
                                                    <label for="">ชื่อ<span class="required">*</span></label>
                                                    <input class="form-control" id="" type="text" required name="informant_name">
                                                </div>
                                                <div class="col-md-4 ">
                                                    <label for="">นามสกุล<span class="required">*</span> </label>
                                                    <input class="form-control" id="" type="text" required name="informant_lname">
                                                </div>
                                                <div class="col-md-4 ">
                                                    <div class="form-group">
                                                        <label for="">ความสัมพันธ์กับหัวหน้าครัวเรือน</label>
                                                        <?php $relation_data = MasterRelation::model()->findAll(array(
                                                            'condition' => 'active=:active',
                                                            'params' => array(':active' => 1)
                                                        )); 
                                                        ?>
                                                        <select class="form-control digits" name="informant_relation">
                                                            <option selected disabled>เลือกความสัมพันธ์</option>
                                                            <?php foreach ($relation_data as $key => $value) { ?>
                                                            <option value="<?=$value->id?>"><?=$value->relation_name?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-8 mb-3">
                                                    <label for=""> เลขบัตรประจำตัวประชาชน </label>
                                                    <input class="form-control" id="" type="text" name="informant_id">
                                                </div>

                                                <div class="col-md-4 mb-3">
                                                    <label for="">เบอร์ติดต่อ </label>
                                                    <input class="form-control" id="" maxlength="10" type="text" name="informant_tel">
                                                </div>
                                            </div>

                                            <div class="form-group row acc-1">
                                                <label class="col-md-8 col-form-label"><strong class="text-main">9.</strong> ครัวเรือนเกษตรมีที่ดินทำการเกษตรอย่างน้อย 1 แปลง อยู่ในเขตชลประทาน <small class="text-danger">(มี=1 / ไม่มี=2)</small><span class="required">*</span></label>
                                                <div class="col-md-2">
                                                    <input class="form-control text-center" maxlength="1" type="text" data-original-title="" title="" required name="inIrrigatedArea">
                                                </div>
                                            </div>

                                            <div class="row acc-1">
                                                <div class="col-md-12 mb-0">
                                                    <label for=""><strong class="text-main">10.</strong> การมีส่วนร่วมในโครงการหรือนโยบายของภาครัฐ ให้ตอบทุกข้อ (มีส่วนร่วม = 1 / ไม่มีส่วนร่วม = 2)<span class="required">*</span></label>
                                                </div>

                                                <div class="row acc-2">
                                                    <?php
                                                    for ($i=0; $i < count($question); $i++) { 

                                                        echo
                                                        '<label class="col-md-8 col-form-label">
                                                        <p class="mb-0">'.($i+1).') '.$question[$i]['title'].' </p>
                                                        <small class="text-danger">'.$question[$i]['subtitle'].'</small>
                                                        </label>
                                                        <div class="col-md-2">
                                                        <input class="form-control text-center mt-3" maxlength="1" type="text" data-original-title="" title="" name="question-'.$question[$i]['question_id'].'"';
                                                        if($question[$i]['isRequired']){
                                                            echo 'required';
                                                        }
                                                        echo '>
                                                        </div>';
                                                    }
                                                    ?>
                                                </div>
                                            </div>

                                            <div class="row acc-1">
                                                <label class="col-sm-3 col-form-label"><i class="icon-map-alt"></i> แนบไฟล์แผนที่ :</label>
                                                <div class="col-sm-4">
                                                    <input class="form-control" type="file" onchange="ValidateSingleInput(this);"  name="mapfile">
                                                </div>
                                            </div>




                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row text-center group-submit">
                             <input type="submit" type="button" class="btn btn-primary btn-lg" value="บันทึกข้อมูล" />
                             <button type="button" class="btn btn-outline-secondary btn-lg" onclick="window.history.go(-1); return false;">ยกเลิก</button>
                         </div>
                     </div>
                 </div>
                 <!-- end row -->

             </div>


         </form>
         <script type="text/javascript">
           var _validFileExtensions = [".jpg", ".jpeg", ".png"];    
           function ValidateSingleInput(oInput) {
            if (oInput.type == "file") {
                var sFileName = oInput.value;
                if (sFileName.length > 0) {
                    var blnValid = false;
                    for (var j = 0; j < _validFileExtensions.length; j++) {
                        var sCurExtension = _validFileExtensions[j];
                        if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                            blnValid = true;
                            break;
                        }
                    }

                    if (!blnValid) {
                        alert("ขออภัย, " + oInput.value + " ประเภทของไฟล์ไม่ถูกต้อง กรุณาอัพโหลดประเภทไฟล์ดังนี้ " + _validFileExtensions.join(", "));
                        oInput.value = "";
                        return false;
                    }
                }
            }
            return true;
        }  
            $.datetimepicker.setLocale('th');

            $('.datetimepicker').datetimepicker({
                timepicker:false,
                format:'d-m-Y',  // กำหนดรูปแบบวันที่ ที่ใช้ เป็น 00-00-0000            
                lang:'th',  // แสดงภาษาไทย                     
               // yearOffset:543,  // ใช้ปี พ.ศ. บวก 543 เพิ่มเข้าไปในปี ค.ศ
                closeOnDateSelect:true,
                onSelectDate:function(dp,$input){
                var yearT=new Date(dp).getFullYear()-0;  
                var yearTH=yearT+543;
                var fulldate=$input.val();
                var fulldateTH=fulldate.replace(yearT,yearTH);
                $input.val(fulldateTH);
                 },
            });
            </script>

         <script type="text/javascript">
            $( "#master-1" ).change(function() {
                $("#province").attr("data-id", "");
                $("#province").val("");
                $("#province-out").val("");
                $("#province-alert").val("");
                $("#province-alert").hide();

                $("#district").attr("data-id", "");
                $("#district").val("");
                $("#district-out").val("");
                $("#district-alert").val("");
                $("#district-alert").hide();

                $("#subdistrict").attr("data-id", "");
                $("#subdistrict").val("");
                $("#subdistrict-out").val("");
                $("#subdistrict-alert").val("");
                $("#subdistrict-alert").hide();

                $("#village").attr("data-id", "");
                $("#village").val("");
                $("#village-out").val("");
                $("#village-alert").val("");
                $("#village-alert").hide();
            });
            $( "#province" ).change(function() {
                var province_code = $("#province").val();
                var county_code = $("#master-1").val();
                if (province_code !="") {
                    if (county_code != undefined && county_code != "") {
                        $.ajax({
                            type: "POST",
                            url: "<?=$this->createUrl("/ajax/checkprovince");?>",
                            data: { province_code:province_code,county_code:county_code },
                            success: function(data){
                                dataarray = JSON.parse(data);
                                if(dataarray['error'] == "have-data"){
                                    $("#province-out").val(dataarray["name"]);
                                    $("#province").attr("data-id", dataarray['id']);
                                    $("#province-alert").val("");
                                    $("#province-alert").hide();
                                }else{
                                    $("#province-out").val("");
                                    $("#province").attr("data-id", "");
                                    $("#province-alert").val("ไม่พบข้อมูล");
                                    $("#province-alert").show();
                                }
                            },
                        });
                    }else{
                        alert("กรุณากรอก สศท.");
                        $("#province").val("");
                    }
                    
                }else{
                    $("#province").attr("data-id", "");
                    $("#province-out").val("");
                    $("#province-alert").val("");
                    $("#province-alert").hide();

                    $("#district").attr("data-id", "");
                    $("#district").val("");
                    $("#district-out").val("");
                    $("#district-alert").val("");
                    $("#district-alert").hide();

                    $("#subdistrict").attr("data-id", "");
                    $("#subdistrict").val("");
                    $("#subdistrict-out").val("");
                    $("#subdistrict-alert").val("");
                    $("#subdistrict-alert").hide();

                    $("#village").attr("data-id", "");
                    $("#village").val("");
                    $("#village-out").val("");
                    $("#village-alert").val("");
                    $("#village-alert").hide();


                }
                
            });

            $( "#district" ).change(function() {
                var district_code = $("#district").val();
                var province_id = $("#province").attr("data-id");
                if (district_code != "") {
                    if (province_id != undefined && province_id != "") {
                        $.ajax({
                            type: "POST",
                            url: "<?=$this->createUrl("/ajax/checkdistrict");?>",
                            data: { district_code:district_code,province_id:province_id },
                            success: function(data){
                                dataarray = JSON.parse(data);
                                if(dataarray['error'] == "have-data"){
                                    $("#district-out").val(dataarray["name"]);
                                    $("#district").attr("data-id", dataarray['id']);
                                    $("#district-alert").val("");
                                    $("#district-alert").hide();
                                }else{
                                    $("#district-out").val("");
                                    $("#district").attr("data-id", "");
                                    $("#district-alert").val("ไม่พบข้อมูล");
                                    $("#district-alert").show();
                                }
                            },
                        });
                    }else{
                        alert("กรุณากรอกจังหวัด");
                        $("#district").val("");
                    }
                }else{
                    $("#district").attr("data-id", "");
                    $("#district-out").val("");
                    $("#district-alert").val("");
                    $("#district-alert").hide();

                    $("#subdistrict").attr("data-id", "");
                    $("#subdistrict").val("");
                    $("#subdistrict-out").val("");
                    $("#subdistrict-alert").val("");
                    $("#subdistrict-alert").hide();

                    $("#village").attr("data-id", "");
                    $("#village").val("");
                    $("#village-out").val("");
                    $("#village-alert").val("");
                    $("#village-alert").hide();
                }
                
            });

            $( "#subdistrict" ).change(function() {
                var subdistrict_code = $("#subdistrict").val();
                var district_id = $("#district").attr("data-id");
                if (subdistrict_code != "") {
                    if (district_id != undefined && district_id != "") {
                        $.ajax({
                            type: "POST",
                            url: "<?=$this->createUrl("/ajax/checksubdistrict");?>",
                            data: { subdistrict_code:subdistrict_code,district_id:district_id },
                            success: function(data){
                                dataarray = JSON.parse(data);
                                if(dataarray['error'] == "have-data"){
                                    $("#subdistrict-out").val(dataarray["name"]);
                                    $("#subdistrict").attr("data-id", dataarray['id']);
                                    $("#subdistrict-alert").val("");
                                    $("#subdistrict-alert").hide();
                                }else{
                                    $("#subdistrict-out").val("");
                                    $("#subdistrict").attr("data-id", "");
                                    $("#subdistrict-alert").val("ไม่พบข้อมูล");
                                    $("#subdistrict-alert").show();
                                }
                            },
                        });
                    }else{
                        alert("กรุณากรอกอำเภอ");
                        $("#subdistrict").val("");
                    }
                }else{
                    $("#subdistrict").attr("data-id", "");
                    $("#subdistrict-out").val("");
                    $("#subdistrict-alert").val("");
                    $("#subdistrict-alert").hide();

                    $("#village").attr("data-id", "");
                    $("#village").val("");
                    $("#village-out").val("");
                    $("#village-alert").val("");
                    $("#village-alert").hide();
                }
                
                
            });

            $( "#village" ).change(function() {
                var village_code = $("#village").val();
                var subdistrict_id = $("#subdistrict").attr("data-id");
                if (village_code != "") {
                    if (subdistrict_id != undefined && subdistrict_id != "") {
                        $.ajax({
                            type: "POST",
                            url: "<?=$this->createUrl("/ajax/checkvillage");?>",
                            data: { village_code:village_code,subdistrict_id:subdistrict_id },
                            success: function(data){
                                dataarray = JSON.parse(data);
                                if(dataarray['error'] == "have-data"){
                                    $("#village-out").val(dataarray["name"]);
                                    $("#village").attr("data-id", dataarray['id']);
                                    $("#village-alert").val("");
                                    $("#village-alert").hide();
                                }else{
                                    $("#village-out").val("");
                                    $("#village").attr("data-id", "");
                                    $("#village-alert").val("ไม่พบข้อมูล");
                                    $("#village-alert").show();
                                }
                            },
                        });
                    }else{
                        alert("กรุณากรอกตำบล");
                        $("#village").val("");
                    }
                }else{
                    $("#village").attr("data-id", "");
                    $("#village-out").val("");
                    $("#village-alert").val("");
                    $("#village-alert").hide();
                }
                
                
            });

            $("#parent_farmer").change(function(event) {  
                 var parent_farmer = document.getElementById("parent_farmer").checked;

                 if (parent_farmer) {
                    var farmer_name = $("input[name='farmer_name']").val();
                    var farmer_lname = $("input[name='farmer_lname']").val();
                    var farmer_id = $("input[name='farmer_id']").val(); 
                    if (farmer_name != '' && farmer_lname != '') {
        
                         $("input[name='informant_name']").empty();
                         $("input[name='informant_name']").append(farmer_name);
                         $("input[name='informant_name']").val(farmer_name);

                         $("input[name='informant_lname']").empty();
                         $("input[name='informant_lname']").append(farmer_lname);
                         $("input[name='informant_lname']").val(farmer_lname);

                         $("input[name='informant_id']").empty();
                         $("input[name='informant_id']").append(farmer_id);
                         $("input[name='informant_id']").val(farmer_id);

                }else{
                    alert("กรุณากรอกข้อมูล ชื่อผู้ขึ้นทะเบียนเกษตรกร");
                    document.getElementById("parent_farmer").checked = false;
                }
            }else{
                        $("input[name='informant_name']").empty();
                        $("input[name='informant_name']").val(null);

                        $("input[name='informant_lname']").empty();
                        $("input[name='informant_lname']").val(null);

                        $("input[name='informant_id']").empty();
                        $("input[name='informant_id']").val(null);
            }
            });
        </script>