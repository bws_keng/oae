<?php
class CustomFormBuilder extends CWidget
{
	protected $items;

	public function init()
	{
		$this->items = FormModel::generate($_GET['id']);
		parent::init();
	}

	public function run()
	{
		$this->render('form');
	}

	public function renderForm()
	{
		$this->renderRecursive($this->items);
	}

	protected function renderRecursive($items)
	{
		foreach ($items as $item){
			$options = $item['options'];
			switch ($item['type']) {
				case 'master':
					echo '<div class="row m-0 col-md-'.$item['size'].' mb-3">';

						if($options != null){
							echo '<label class="col-md-6 pl-0" for=""> '.$item['question'];
						}else{
							echo '<label class="col-md-12 p-0" for=""> '.$item['question'];
						}
						if($item['readable_id'] != '' && $item['readable_id'] != NULL){
							echo ' <red>('.$item['readable_id'].')</red>';
						}
						echo $item['isRequired'] ? " <span class='required'>*</span>" : "";
						echo '</label>';
						
						if($options != null){
							echo '<label class="col-md-6 pr-0" for=""> '. $options .'</label>';
						}

						echo '<div class="col-md-6 mb-3 pl-0">
							<input class="form-control " id="master-'.$item['question_id'].'" type="text" name="question-'.$item['question_id'].'"  data-master="'.$item['masterdata'].'" '.($item['isRequired']? 'required': '').'>
						</div>
						<div class="col-md-6 mb-3 pr-0">
							<input class="form-control" id="master-'.$item['question_id'].'-out" type="text" disabled="disabled">
						</div>
						
						<div class="col-12" role="alert" style="display:none;" id="master-'.$item['question_id'].'-alert">
							<p class="text-danger">ข้อมูลผิดพลาด</p>
						</div>
					</div>
					';
					break;
				case 'note':
					echo
					'<div class="col-12">
						<hr>
                        <h6><small class="text-danger">หมายเหตุ : '.$item['question'].'</small></h6>
                    </div>
					';
					break;
				case 'bottom-line':
					echo '
					<div class="col-12">
						<hr>
                        <h6 class=" mb-3"><u> '.$item['question'].'</u></h6>
                    </div>
					';
					break;
				case 'bullet':
					echo '
					<div class="col-12">
                        <h6 class=" mb-3"><span>&#8226;</span> '.$item['question'].'</h6>
                    </div>
					';
					break;
				case 'header':
					echo '
					<div class="col-12">
						<hr>
                        <h6 class="text-head mb-3"><i class="icon-pencil-alt"></i> '.$item['question'].'</h6>
                    </div>
					';
					break;
				case 'ques-num-value':
					echo '
					<div class="col-12">
						<hr>
                        <h6 style="margin-left: 20px;" class="mb-3"> '.$item['question'];
                    if($item['readable_id'] != '' && $item['readable_id'] != NULL){
							echo ' <red>('.$item['readable_id'].')</red>';
						}
                    echo '</h6></div>
					';
					break;
				case 'line':
					echo '
					<div class="col-12">
						<hr />
					</div>
					';
					break;
				case 'function':
					echo '<div class="row m-0 col-md-'.$item['size'].' mb-3">';
						echo '<label class="col-md-12 p-0" for=""> '.$item['question'];
						echo $item['isRequired'] ? " <span class='required'>*</span>" : "";
						echo '</label>';
						echo '<div class="col-md-6 mb-3 pl-0">
							<input class="form-control " id="calculate-input-'.$item['question_id'].'" type="float" name="question-'.$item['question_id'].'" data-function=\''.$item['options'].'\' '.($item['isRequired']? 'required': '').'>
						</div>
						<div class="col-md-6 mb-3 pl-0">
							<input class="form-control " id="calculate-output-'.$item['question_id'].'" type="float" name="question-'.$item['question_id'].'" '.($item['isRequired']? 'required': '').' value="" disabled>
						</div>
					</div>
					';
					break;
				case 'assign':
					echo '<div class="row m-0 col-md-'.$item['size'].' mb-3">';
						echo '<label class="col-md-12 p-0" for=""> '.$item['question'] .'</label>';
						echo '<div class="col-md-12 mb-3 pl-0">
							<input class="form-control " id="assign-'.$item['question_id'].'" type="text" name="question-'.$item['question_id'].'" data-assign="'.$item['options'].'" '.($item['isRequired']? 'required': '').'>
						</div>
					</div>
					';
					break;
				case 'output':
					echo '<div class="row m-0 col-md-'.$item['size'].' mb-3">';
						echo '<label class="col-md-12 p-0" for=""> '.$item['question'] .'</label>';
						echo '<div class="col-md-12 mb-3 pl-0">
							<input class="form-control " id="output-'.$item['question_id'].'" type="text" name="question-'.$item['question_id'].'" data-function=\''.$item['options'].'\' '.($item['isRequired']? 'required': '').' readonly value=0>
						</div>
					</div>
					';
					$array_options = json_decode($item["options"]);
					echo '<script type="text/javascript">';
					
					foreach ($array_options as $key => $value) {
						echo '
						$( "#question-sum-'.$value.'" ).keyup(function() {';
						echo "
						var sum".$item['question_id']." = 0;
						";
						foreach ($array_options as $keysub => $valuesub) {
                            echo '
                            if(($( "#question-sum-'.$valuesub.'" ).val()).replaceAll(",", "") > 0){
                                if(sum'.$item['question_id'].' == 0){';
                                    echo 'sum'.$item['question_id'].' = parseFloat(($( "#question-sum-'.$valuesub.'" ).val()).replaceAll(",", ""));';
                                echo '}else{';
                                    echo 'sum'.$item['question_id'].' = sum'.$item['question_id'].' * parseFloat(($( "#question-sum-'.$valuesub.'" ).val()).replaceAll(",", ""));';
                                echo '}';
                            echo "}
                            ";
                        }
						echo '
						if(sum'.$item['question_id'].' > 0){
						  $("#output-'.$item['question_id'].'").val(parseFloat(sum'.$item['question_id'].').toLocaleString());
						  }else{
						  	$("#output-'.$item['question_id'].'").val(0);
						  }
						  ';
						echo '});';
					}
					echo "</script>";
					break;
				case 'number':
					echo
					'<div class="col-md-'.$item['size'].' mb-3">
						<label for=""> '.$item['question'];
						if($item['readable_id'] != '' && $item['readable_id'] != NULL){
							echo ' <red>('.$item['readable_id'].')</red>';
						}
						echo $item['isRequired'] ? " <span class='required'>*</span>" : "";
						echo '</label>
						<input class="form-control" name="question-'.$item['question_id'].'" id="question-sum-'.$item['question_id'].'" type="float" placeholder=""  data-original-title="" title="" '.($item['isRequired']? 'required': '').'>
					</div>';
					break;
				case 'float':
					echo
					'<div class="col-md-'.$item['size'].' mb-3">
						<label for=""> '.$item['question'];
						if($item['readable_id'] != '' && $item['readable_id'] != NULL){
							echo ' <red>('.$item['readable_id'].')</red>';
						}
						echo $item['isRequired'] ? " <span class='required'>*</span>" : "";
						echo '</label>
						<input class="form-control" name="question-'.$item['question_id'].'" id="question-sum-'.$item['question_id'].'" type="'.$item['type'].'" placeholder=""  data-original-title="" title="" '.($item['isRequired']? 'required': '').'>
					</div>';
					break;
				case 'sum-input':
					echo '<div class="row m-0 col-md-'.$item['size'].' mb-3">';
						echo '<label class="col-md-12 p-0" for=""> '.$item['question'] .'</label>';
						echo '<div class="col-md-12 mb-3 pl-0">
							<input class="form-control " id="sum-input-'.$item['question_id'].'" type="text" name="sum-input-'.$item['question_id'].'" data-variable=\''.$item['options'].'\' '.($item['isRequired']? 'required': '').'>
						</div>
					</div>
					';
					break;
				case 'sum-output':
					echo '<div class="row m-0 col-md-'.$item['size'].' mb-3">';
						echo '<label class="col-md-12 p-0" for=""> '.$item['question'] .'</label>';
						echo '<div class="col-md-12 mb-3 pl-0">
							<input class="form-control " id="sum-output-'.$item['question_id'].'" type="text" name="sum-output-'.$item['question_id'].'" data-variable=\''.$item['options'].'\' '.($item['isRequired']? 'required': '').' readonly>
						</div>
					</div>
					';
					$array_options = json_decode($item["options"]);
					echo '<script type="text/javascript">';
					if ($array_options != null) {
						foreach ($array_options as $key => $value) {
							echo '
							$( "#calculate-input-'.$value.'" ).change(function() {';
							echo "
							var sum".$item['question_id']." = 0
							";
							foreach ($array_options as $keysub => $valuesub) {
								echo '
								if(($( "#calculate-output-'.$valuesub.'" ).val()).replaceAll(",", "") > 0){
									';
									echo 'sum'.$item['question_id'].' += parseFloat(($( "#calculate-output-'.$valuesub.'" ).val()).replaceAll(",", ""));';
									echo "}
									";
								}
								echo '
								$("#sum-output-'.$item['question_id'].'").val(parseFloat(sum'.$item['question_id'].').toLocaleString());
								';
								echo '});';
							}
						}

					echo "</script>";
					break;
				case 'ques-ans':
					echo '<div class="row m-0 col-md-'.$item['size'].' mb-3">';
						echo '<label class="col-md-6 p-0" for=""> '.$item['question'] ;
						if($item['readable_id'] != '' && $item['readable_id'] != NULL){
							echo ' <red>('.$item['readable_id'].')</red>';
						}
						echo '</label>';;
						echo '<label class="col-md-6 p-0" for=""> '.$item['options'] .'</label>';
						echo '<div class="col-md-6 mb-3 pl-0">
						</div>';
						echo '<div class="col-md-6 mb-3 pl-0">
							<input class="form-control" name="question-'.$item['question_id'].'" id="question-sum-'.$item['question_id'].'" type="float" placeholder=""  data-original-title="" title="" '.($item['isRequired']? 'required': '').'>
						</div>';
					echo '</div>
					';
					break;
				case 'input-product':
					echo '<div class="row m-0 col-md-'.$item['size'].' mb-3">';
						echo '<label class="col-md-12 p-0" for=""> '.$item['question'] ;
						if($item['readable_id'] != '' && $item['readable_id'] != NULL){
							echo ' <red>('.$item['readable_id'].')</red>';
						}
						echo $item['isRequired'] ? " <span class='required'>*</span>" : "";
						echo '</label>';;
						echo '<div class="col-md-7 mb-3 pl-0">
							<input class="form-control" name="question-'.$item['question_id'].'" id="question-sum-'.$item['question_id'].'" type="float" placeholder=""  data-original-title="" title="" '.($item['isRequired']? 'required': '').'>
						</div>';
						// echo '<div class="col-md-5 mb-3 pl-0">
						// 	<input class="form-control" name="inputquestionparent-'.$item['question_id'].'" id="question-sum-parent-'.$item['question_id'].'" type="text" placeholder=""  data-original-title="" title="" '.($item['isRequired']? 'required': '').'>
						// </div>';
					echo '</div>
					';
					break;
				
				case 'sum-output-number':
					echo '<div class="row m-0 col-md-'.$item['size'].' mb-3">';
						echo '<label class="col-md-6 p-0" for=""> '.$item['question'] .'</label>';
						echo '<div class="col-md-6 mb-3 pl-0">
							<input class="form-control " id="sum-output-number-'.$item['question_id'].'" type="text" name="sum-output-number-'.$item['question_id'].'" data-variable=\''.$item['options'].'\' '.($item['isRequired']? 'required': '').' readonly>
						</div>
					</div>
					';
					$array_options = json_decode($item["options"]);
					echo '<script type="text/javascript">';
					if ($array_options != null) {
						foreach ($array_options as $key => $value) {
							echo '
							$( "#question-sum-'.$value.'" ).keyup(function() {';
							echo "
							var sum".$item['question_id']." = 0
							";
							foreach ($array_options as $keysub => $valuesub) {
								echo '
								if($( "#question-sum-'.$valuesub.'" ).val() > 0){
									';
									echo 'sum'.$item['question_id'].' += parseFloat($( "#question-sum-'.$valuesub.'" ).val());';
									echo "}
									";
								}
								echo '
								$("#sum-output-number-'.$item['question_id'].'").val(sum'.$item['question_id'].');
								';
								echo '});';
							}
						}

					echo "</script>";
					break;

				case 'text':
					echo
					'<div class="col-md-'.$item['size'].' mb-3">
						<label for=""> '.$item['question'];
						if($item['readable_id'] != '' && $item['readable_id'] != NULL){
							echo ' <red>('.$item['readable_id'].')</red>';
						}
						echo $item['isRequired'] ? " <span class='required'>*</span>" : "";
						echo '</label>
						<div class="row col-12">
							<input class="form-control col-10" name="question-'.$item['question_id'].'" type="'.$item['type'].'" placeholder=""  data-original-title="" title="" '.($item['isRequired']? 'required': '').' >
						</div>
					</div>';
					// echo '</label>
					// 	<div class="row col-12">
					// 		<input class="form-control col-10" name="question-'.$item['question_id'].'" type="'.$item['type'].'" placeholder=""  data-original-title="" title="" '.($item['isRequired']? 'required': '').' >
					// 		<div class="row col-2">
					// 			<button type="button" class="btn col-4" onclick="$(`input[name=\'question-'.$item['question_id'].'\']`).val(() => { return $(`input[name=\'question-'.$item['question_id'].'\']`).val() + \'<b></b>\'})"><B>B</B></button>
					// 			<button type="button" class="btn col-4" onclick="$(`input[name=\'question-'.$item['question_id'].'\']`).val(() => { return $(`input[name=\'question-'.$item['question_id'].'\']`).val() + \'<i></i>\'})"><i>i</i></button>
					// 			<button type="button" class="btn col-4" onclick="$(`input[name=\'question-'.$item['question_id'].'\']`).val(() => { return $(`input[name=\'question-'.$item['question_id'].'\']`).val() + \'<red></red>\'})"><red>A</red></button>
					// 		</div>
					// 	</div>
					// </div>';
					break;
				case 'parent-ques-num':
					echo
					'<div style="margin-left: 30px;" class="col-md-'.$item['size'].' mb-3">
						<label for=""> '.$item['question'];
						if($item['readable_id'] != '' && $item['readable_id'] != NULL){
							echo ' <red>('.$item['readable_id'].')</red>';
						}
						echo '</label>
						<div class="row col-12">
							<input class="form-control" name="question-'.$item['question_id'].'" type="number" placeholder=""  data-original-title="" title="" '.($item['isRequired']? 'required': '').' >
						</div>
					</div>';
					break;
				case 'parent-ques-value':
					echo
					'<div class="col-md-'.$item['size'].' mb-3">
						<label for=""> '.$item['question'];
						if($item['readable_id'] != '' && $item['readable_id'] != NULL){
							echo ' <red>('.$item['readable_id'].')</red>';
						}
						echo '</label>
						<div class="row col-12">
							<input class="form-control" name="question-'.$item['question_id'].'" type="float" placeholder=""  data-original-title="" title="" '.($item['isRequired']? 'required': '').' >
						</div>
					</div>';
					break;
				default:
					echo
					'<div class="col-md-'.$item['size'].' mb-3">
						<label for=""> '.$item['question'];
						if($item['readable_id'] != '' && $item['readable_id'] != NULL){
							echo ' <red>('.$item['readable_id'].')</red>';
						}
						echo '</label>
						<input class="form-control" name="question-'.$item['question_id'].'" type="'.$item['type'].'" placeholder=""  data-original-title="" title="" '.($item['isRequired']? 'required': '').'>
					</div>';
					break;
			}
		}
	}
}

?>