<?php


class masterVillage extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'db_oae.master_village';

	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('village_id, created_by, updated_by, active, subdistrict_id', 'numerical', 'integerOnly'=>true),
			array('village_code, village_name', 'length', 'max'=>255),
			array('created_at, updated_at', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('subdistrict_id, village_code, village_name, village_id,created_by, created_at, updated_by, updated_at, active', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */

	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}
    
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'village_id' => 'village_id',
			'village_code' => 'โค๊ดหมู่บ้าน',
            'village_name' => 'ชื่อหมู่บ้าน',
			'subdistrict_id' => 'ชื่อตำบล',
			'created_by' => 'Created By',
			'created_at' => 'Created At',
			'updated_by' => 'Updated By',
			'updated_at' => 'Updated At',
			'active' => 'Active',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('village_id',$this->village_id);
		$criteria->compare('village_name',$this->village_name,true);
		$criteria->compare('village_code',$this->village_code);
        $criteria->compare('subdistrict_id',$this->subdistrict_id);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('updated_by',$this->updated_by);
		$criteria->compare('updated_at',$this->updated_at,true);
		$criteria->compare('active',$this->active);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function beforeSave() 
    {
		if(null !== Yii::app()->user && isset(Yii::app()->user->village_id))
		{
			$id = Yii::app()->user->id;
		}
		else
		{
			$id = 1;
		}

		if($this->isNewRecord)
		{
			$this->created_by = $id;
			$this->created_at = date("Y-m-d H:i:s");
			$this->updated_by = $id;
			$this->updated_at = date("Y-m-d H:i:s");
		}
		else
		{
			$this->updated_by = $id;
			$this->updated_at = date("Y-m-d H:i:s");
		}

        return parent::beforeSave();
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return masterVillage the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getmasterVillageList(){
		$model = masterVillage::model()->findAll();
		$list = CHtml::listData($model,'village_id','village_name');
		return $list;
	}

}
