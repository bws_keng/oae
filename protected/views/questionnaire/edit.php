<form action="<?php echo Yii::app()->baseUrl . "/questionnaire/".$_GET['id'].'/edit'; ?>" method="POST" enctype="multipart/form-data">
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl; ?>/theme/assets/js/bootstrap-daterangepicker/jquery.datetimepicker.css">
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/theme/assets/js/bootstrap-daterangepicker/jquery.datetimepicker2.full.min.js"></script>
    <div class="container-fluid">


        <!-- start row -->
        <div class="row">
            <div class="col-lg-12 set-height set-padding">
                <div id="section1">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="mb-1">แบบสอบถามภาวะเศรษฐกิจสังคมครัวเรือนและแรงงานเกษตร </h5>
                            <?php
                            $start_explode = explode("-", $start);
                            $start_sum = $start_explode[0] + 543;
                            $start_format = $start_explode[2].'-'.$start_explode[1].'-'.$start_sum;

                            $end_explode = explode("-", $end);
                            $end_sum = $end_explode[0] + 543;
                            $end_format = $end_explode[2].'-'.$end_explode[1].'-'.$end_sum;
                            ?>
                            <p class="mb-0">ระหว่างวันที่ <?php echo $start_format; ?> ถึง <?php echo $end_format; ?></p>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <h4 class="mb-4 text-main"><i class="icofont icofont-ui-user"></i> เจ้าหน้าที่สำรวจ</h4>
                            <hr>
                            <div>
                                <div class="row">
                                    <div class="col-md-4 mb-3">
                                        <label for="">ชื่อ<span class="required">*</span></label>
                                        <input class="form-control" id="" type="text" required name="staff_name" value="<?php echo $data['questionaire']['staff_name']; ?>">
                                    </div>
                                    <div class="col-md-4 mb-3">
                                        <label for="">นามสกุล<span class="required">*</span></label>
                                        <input class="form-control" id="" type="text" required name="staff_lname" value="<?php echo $data['questionaire']['staff_lname']; ?>">
                                    </div>
                                    <div class="col-md-4 mb-3">
                                        <label for="">รหัส<span class="required">*</span></label>
                                        <div class="input-group">
                                            <input class="form-control" name="staff_id" value="<?php echo $data['questionaire']['staff_id']; ?>" id="" type="text" required placeholder="" aria-describedby="inputGroupPrepend2" required="" data-original-title="" title="" name="staff_id" value="<?php echo $data['questionaire']['staff_id']; ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 mb-3">
                                        <label for="">วัน/เดือน/ปี<span class="required">*</span></label>
                                        <?php 
                                        $survey_time_explode = explode("-",$data['questionaire']['survey_time']);
                                        $survey_time_implode = $survey_time_explode[1].'-'.$survey_time_explode[2].'-'.$survey_time_explode[0];
                                        ?>
                                        <input class="form-control datetimepicker" type="text" name="survey_time" required value="<?php echo $survey_time_implode; ?>"> 
                                                    <!-- <div class="date-picker">
                                                        <label for="validationDefault03">วัน/เดือน/ปี</label>
                                                        <div class="input-group">
                                                            <input class="datepicker-here form-control digits" type="text" data-language="th">
                                                        </div>
                                                        </div>
                                                    </div> -->
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div id="section2">
                                <div class="card">
                                    <div class="card-body">
                                        <div>
                                            <div class="row acc-1">
                                                <div class="col-md-8 mb-3">
                                                    <label for=""><strong class="text-main">1.</strong> สำนักงานเศรษฐกิจการเกษตรที่ </label>
                                                    <input class="form-control" id="county-1-out" type="text" disabled="disabled">
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">สศท. <small class="text-danger">(เลข 2 หลัก)</small><span class="required">*</span></label>
                                                     <?php
                                                        $office_id = '';
                                                        if ($data['questionaire']['office_id'] < 10) {
                                                            for ($i=0; $i <= $data['questionaire']['office_id'] ; $i++) { 
                                                                $office_id = '0'.$data['questionaire']['office_id'];
                                                            }
                                                        }else{
                                                            $office_id = $data['questionaire']['office_id'];
                                                        }
                                                    ?>
                                                    <input class="form-control " id="county-1" maxlength="2" type="text" required name="office"  value="<?php echo $office_id; ?>" data-master="county">
                                                </div>
                                                
                                                <div class="col-12" role="alert" style="display:none;" id="county-1-alert">
                                                    <p class="text-danger">ข้อมูลผิดพลาด</p>
                                                </div>
                                            </div>

                                            <div class="row acc-1">
                                                <div class="col-md-8 mb-3">
                                                    <label for=""><strong class="text-main">2.</strong> จังหวัด </label>
                                                    <input class="form-control" id="province-out" type="text" disabled="disabled">
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">จังหวัด <small class="text-danger">(เลข 3 หลัก)</small><span class="required">*</span></label>
                                                    <input class="form-control " id="province" maxlength="3" type="text" required name="province" value="<?php echo $data['questionaire']['province']; ?>" data-master="province">
                                                </div>
                                                
                                                <div class="col-12" role="alert" style="display:none;" id="province-alert">
                                                    <p class="text-danger">ข้อมูลผิดพลาด</p>
                                                </div>
                                            </div>

                                            <div class="row acc-1">
                                                <div class="col-md-8 mb-3">
                                                    <label for=""><strong class="text-main">3.</strong> อำเภอ </label>
                                                    <input class="form-control " id="district-out" type="text" disabled="disabled">
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">อำเภอ <small class="text-danger">(เลข 2 หลัก)</small><span class="required">*</span></label>
                                                    <input class="form-control" id="district" maxlength="2" type="text" required name="district" value="<?php echo $data['questionaire']['district']; ?>" data-master="district">
                                                </div>
                                                
                                                <div class="col-12" role="alert" style="display:none;" id="district-alert">
                                                    <p class="text-danger">ข้อมูลผิดพลาด</p>
                                                </div>
                                            </div>

                                            <div class="row acc-1">
                                                <div class="col-md-8 mb-3">
                                                    <label for=""><strong class="text-main">4.</strong> ตำบล </label>
                                                    <input class="form-control" id="subdistrict-out" type="text" disabled="disabled">
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">ตำบล <small class="text-danger">(เลข 2 หลัก)</small><span class="required">*</span></label>
                                                    <input class="form-control" id="subdistrict" maxlength="2" type="text" required name="subdistrict" value="<?php echo $data['questionaire']['subdistrict']; ?>" data-master="subdistrict">
                                                </div>
                                                
                                                <div class="col-12" role="alert" style="display:none;" id="subdistrict-alert">
                                                    <p class="text-danger">ข้อมูลผิดพลาด</p>
                                                </div>
                                            </div>

                                            <div class="row acc-1">
                                                <div class="col-md-4 mb-3">
                                                    <label for=""><strong class="text-main">5.</strong> หมู่บ้าน </label>
                                                    <input class="form-control" id="village-out" type="text" disabled="disabled">
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">หมู่ที่ <small class="text-danger">(เลข 2 หลัก)</small><span class="required">*</span></label>
                                                    <input class="form-control text-center" maxlength="2" id="village" type="text" required name="village" value="<?php echo $data['questionaire']['village']; ?>" data-master="village">
                                                </div>
                                                
                                                <div class="col-md-4 mb-3">
                                                    <label for=""> บ้านเลขที่<span class="required">*</span> </label>
                                                    <input class="form-control"  type="text" required name="addressNumber" value="<?php echo $data['questionaire']['addressNumber']; ?>">
                                                </div>
                                                <div class="col-12" role="alert" style="display:none;" id="village-alert">
                                                    <p class="text-danger">ข้อมูลผิดพลาด</p>
                                                </div>
                                                
                                            </div>

                                            <div class="row acc-1">
                                                <div class="col-md-8 mb-3">
                                                    <label for=""><strong class="text-main">6.</strong> พวกที่ </label>
                                                    <input class="form-control" id="master-6-out" type="text" disabled="disabled">
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">พวกที่ <small class="text-danger">(เลข 1 หลัก)</small><span class="required">*</span></label>
                                                    <input class="form-control" id="master-6" maxlength="1" type="text" name="group" required value="<?php echo $data['questionaire']['group']; ?>" data-master="group">
                                                </div>
                                                <div class="col-12" role="alert" style="display:none;" id="master-6-alert">
                                                    <p class="text-danger">ข้อมูลผิดพลาด</p>
                                                </div>
                                            </div>

                                            <div class="row acc-1">
                                                <div class="col-md-12">
                                                    <label><strong class="text-main">7.</strong> ชื่อผู้ขึ้นทะเบียนเกษตรกร </label>
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">ชื่อ</label>
                                                    <input class="form-control" id="" type="text" name="farmer_name" value="<?php echo $data['questionaire']['farmer_name']; ?>">
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">นามสกุล </label>
                                                    <input class="form-control" id="" type="text" name="farmer_lname" value="<?php echo $data['questionaire']['farmer_lastname']; ?>">
                                                </div>

                                                <div class="col-md-4 mb-3">
                                                    <label for="">ตัวอย่างที่ <small class="text-danger">(เลข 1 หลัก)</small></label>
                                                    <input class="form-control" id="" maxlength="1" type="text" name="sample" value="<?php echo $data['questionaire']['sample']; ?>">
                                                </div>

                                                <div class="col-md-8 mb-3">
                                                    <label for=""> เลขบัตรประจำตัวประชาชน </label>
                                                    <input class="form-control" id="" type="text" name="farmer_id" value="<?php echo $data['questionaire']['farmer_id']; ?>">
                                                </div>

                                                <div class="col-md-4 mb-3">
                                                    <label for="">ผู้ขึ้นทะเบียนเกษตร <small class="text-danger">มี=1/ ไม่มี=2</small></label>
                                                    <input class="form-control" id="" maxlength="1" type="text" name="farmer_registered" value="<?php echo $data['questionaire']['farmer_registered']; ?>">
                                                </div>
                                            </div>

                                            <div class="row acc-1">
                                                <div class="col-md-12">
                                                    <div class="checkbox checkbox-primary checkbox-inline">
                                                        <input type="checkbox" name="parent_farmer" id="parent_farmer" value="<?php echo $data['questionaire']['parent_farmer']; ?>" <?php if ( $data['questionaire']['parent_farmer'] == "y") : ?> checked="checked" <?php  endif ?>>
                                                        <label for="parent_farmer" class="bg-primary text-black">ชื่อผู้ขึ้นทะเบียนเกษตรกร </label>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <label><strong class="text-main">8.</strong> ชื่อผู้ให้ข้อมูล </label>
                                                </div>
                                                <div class="col-md-4 ">
                                                    <label for="">ชื่อ<span class="required">*</span></label>
                                                    <input class="form-control" id="" type="text" name="informant_name" required value="<?php echo $data['questionaire']['informant_name']; ?>">
                                                </div>
                                                <div class="col-md-4 ">
                                                    <label for="">นามสกุล<span class="required">*</span> </label>
                                                    <input class="form-control" id="" type="text" name="informant_lname" required value="<?php echo $data['questionaire']['informant_lastname']; ?>">
                                                </div>
                                                <div class="col-md-4 ">
                                                    <div class="form-group">
                                                        <label for="">ความสัมพันธ์กับหัวหน้าครัวเรือน</label>
                                                        <?php $relation_data = MasterRelation::model()->findAll(array(
                                                            'condition' => 'active=:active',
                                                            'params' => array(':active' => 1)
                                                        )); 
                                                        ?>
                                                        <select class="form-control digits" name="informant_relation" value="<?php echo $data['questionaire']['informant_relation']; ?>">
                                                            <option selected disabled>เลือกความสัมพันธ์</option>
                                                            <?php foreach ($relation_data as $key => $value) { ?>
                                                                <option <?=$data['questionaire']['informant_relation'] == $value->id ? "selected" : ""?> value="<?=$value->id?>"><?=$value->relation_name?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-8 mb-3">
                                                    <label for=""> เลขบัตรประจำตัวประชาชน </label>
                                                    <input class="form-control" id="" type="text" name="informant_id" value="<?php echo $data['questionaire']['informant_id']; ?>">
                                                </div>

                                                <div class="col-md-4 mb-3">
                                                    <label for="">เบอร์ติดต่อ </label>
                                                    <input class="form-control" id="" maxlength="10" type="text" name="informant_tel" value="<?php echo $data['questionaire']['informant_tel']; ?>">
                                                </div>
                                            </div>

                                            <div class="form-group row acc-1">
                                                <label class="col-md-8 col-form-label"><strong class="text-main">9.</strong> ครัวเรือนเกษตรมีที่ดินทำการเกษตรอย่างน้อย 1 แปลง อยู่ในเขตชลประทาน <small class="text-danger">(มี=1 / ไม่มี=2)</small><span class="required">*</span></label>
                                                <div class="col-md-2">
                                                    <input class="form-control text-center" maxlength="1" type="text" data-original-title="" required title="" name="inIrrigatedArea" value="<?php echo $data['questionaire']['inIrrigatedArea']; ?>">
                                                </div>
                                            </div>

                                            <div class="row acc-1">
                                                <div class="col-md-12 mb-0">
                                                    <label for=""><strong class="text-main">10.</strong> การมีส่วนร่วมในโครงการหรือนโยบายของภาครัฐ ให้ตอบทุกข้อ (มีส่วนร่วม = 1 / ไม่มีส่วนร่วม = 2)<span class="required">*</span> </label>
                                                </div>

                                                <div class="row acc-2">
                                                    <?php
                                                    for ($i=0; $i < count($question); $i++) { 

                                                        echo
                                                        '<label class="col-md-8 col-form-label">
                                                        <p class="mb-0">'.($i+1).') '.$question[$i]['title'].' </p>
                                                        <small class="text-danger">'.$question[$i]['subtitle'].'</small>
                                                        </label>
                                                        <div class="col-md-2">
                                                        <input class="form-control text-center mt-3" maxlength="1" type="text" data-original-title="" title="" name="question-'.$question[$i]['question_id'].'"';
                                                        if($question[$i]['isRequired']){
                                                            echo ' required';
                                                        }
                                                        if(array_key_exists($question[$i]['question_id'], $data['value'])){
                                                            echo ' value="'.$data['value'][$question[$i]['question_id']].'"';
                                                        }
                                                        echo '>
                                                        </div>';
                                                    }
                                                    ?>
                                                </div>
                                            </div>

                                            <div class="row acc-1">
                                                <label class="col-sm-3 col-form-label"><i class="icon-map-alt"></i> แนบไฟล์แผนที่ :</label>
                                                <div class="col-sm-4">
                                                    <input class="form-control" type="file" onchange="ValidateSingleInput(this);" name="mapfile">
                                                </div>
                                                <?php
                                            $questionnaire_id = $data['questionaire']['questionnaire_id'];
                                            $questionnaire_format = $questionnaire_id.'.jpg';
    
                                            if (file_exists($_SERVER['DOCUMENT_ROOT'].Yii::app()->baseurl.'/uploads/'.$questionnaire_format)) { 

                                            ?>
                                            <a href="<?php echo Yii::app()->baseurl.'/uploads/'.$questionnaire_format;?>">ไฟล์แผนที่</a>
                                            <?php
                                                }
                                            ?>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row text-center group-submit">
                             <input type="submit" type="button" class="btn btn-primary btn-lg" value="บันทึกข้อมูล" />
                             <button type="button" class="btn btn-outline-secondary btn-lg" onclick="window.history.go(-1); return false;">ยกเลิก</button>
                         </div>
                     </div>
                 </div>
                 <!-- end row -->

             </div>


         </form>
         <script type="text/javascript"> 
            var _validFileExtensions = [".jpg", ".jpeg", ".png"];    
            function ValidateSingleInput(oInput) {
                if (oInput.type == "file") {
                    var sFileName = oInput.value;
                    if (sFileName.length > 0) {
                        var blnValid = false;
                        for (var j = 0; j < _validFileExtensions.length; j++) {
                            var sCurExtension = _validFileExtensions[j];
                            if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                                blnValid = true;
                                break;
                            }
                        }

                        if (!blnValid) {
                            alert("ขออภัย, " + oInput.value + " ประเภทของไฟล์ไม่ถูกต้อง กรุณาอัพโหลดประเภทไฟล์ดังนี้ " + _validFileExtensions.join(", "));
                            oInput.value = "";
                            return false;
                        }
                    }
                }
                return true;
            }
            $.datetimepicker.setLocale('th');
            var thaiYear = function (ct) {
            var leap=3;  
            var dayWeek=["พฤ", "ศ", "ส", "อา","จ", "อ", "พ"];  
            if(ct){  
                var yearL=new Date(ct).getFullYear()-543;  
                leap=(((yearL % 4 == 0) && (yearL % 100 != 0)) || (yearL % 400 == 0))?2:3;  
                if(leap==2){  
                    dayWeek=["ส","อา","จ","อ","พ","พฤ","ศ"];  
                }  
            }              
            this.setOptions({  
                i18n:{ th:{dayOfWeekShort:dayWeek}},dayOfWeekStart:leap,  
            })                
        };    
     

            $('.datetimepicker').datetimepicker({
                timepicker:false,
                format:'d-m-Y',  // กำหนดรูปแบบวันที่ ที่ใช้ เป็น 00-00-0000    
                onChangeMonth:thaiYear,          
                onShow:thaiYear,          
                //lang:'th',  // แสดงภาษาไทย                     
                //yearOffset:543,  // ใช้ปี พ.ศ. บวก 543 เพิ่มเข้าไปในปี ค.ศ
                // closeOnDateSelect:true,
                onSelectDate:function(dp,$input){
                var yearT=new Date(dp).getFullYear()-0;  
                var yearTH=yearT+543;
                var fulldate=$input.val();
                var fulldateTH=fulldate.replace(yearT,yearTH);
                $input.val(fulldateTH);
                 },
            });
        </script>
         <script type="text/javascript">
            $( document ).ready(function() {
                onchangeoffice();
                onchangePuak();
                // setTimeout(function(){ onchangeoffice(); }, 500);
                // setTimeout(function(){ onchangeProvince(); }, 1000);
                // setTimeout(function(){ onchangeDistrict(); }, 2000);
                // setTimeout(function(){ onchangeSubdistrict(); }, 3000);
                // setTimeout(function(){ onchangeVillage(); }, 4000);
                // setTimeout(function(){ onchangePuak(); }, 1000);
            });

            $( "#master-1" ).change(function() {
                $("#province").attr("data-id", "");
                $("#province").val("");
                $("#province-out").val("");
                $("#province-alert").val("");
                $("#province-alert").hide();

                $("#district").attr("data-id", "");
                $("#district").val("");
                $("#district-out").val("");
                $("#district-alert").val("");
                $("#district-alert").hide();

                $("#subdistrict").attr("data-id", "");
                $("#subdistrict").val("");
                $("#subdistrict-out").val("");
                $("#subdistrict-alert").val("");
                $("#subdistrict-alert").hide();

                $("#village").attr("data-id", "");
                $("#village").val("");
                $("#village-out").val("");
                $("#village-alert").val("");
                $("#village-alert").hide();
            });
            $( "#province" ).change(function() {
                var province_code = $("#province").val();
                var county_code = $("#county-1").val();
                if (province_code !="") {
                    if (county_code != undefined && county_code != "") {
                        $.ajax({
                            type: "POST",
                            url: "<?=$this->createUrl("/ajax/checkprovince");?>",
                            data: { province_code:province_code,county_code:county_code },
                            success: function(data){
                                dataarray = JSON.parse(data);
                                if(dataarray['error'] == "have-data"){
                                    $("#province-out").val(dataarray["name"]);
                                    $("#province").attr("data-id", dataarray['id']);
                                    $("#province-alert").val("");
                                    $("#province-alert").hide();
                                }else{
                                    $("#province-out").val("");
                                    $("#province").attr("data-id", "");
                                    $("#province-alert").val("ไม่พบข้อมูล");
                                    $("#province-alert").show();
                                }
                            },
                        });
                    }else{
                        alert("กรุณากรอก สศท.");
                        $("#province").val("");
                    }

                }else{
                    $("#province").attr("data-id", "");
                    $("#province-out").val("");
                    $("#province-alert").val("");
                    $("#province-alert").hide();

                    $("#district").attr("data-id", "");
                    $("#district").val("");
                    $("#district-out").val("");
                    $("#district-alert").val("");
                    $("#district-alert").hide();

                    $("#subdistrict").attr("data-id", "");
                    $("#subdistrict").val("");
                    $("#subdistrict-out").val("");
                    $("#subdistrict-alert").val("");
                    $("#subdistrict-alert").hide();

                    $("#village").attr("data-id", "");
                    $("#village").val("");
                    $("#village-out").val("");
                    $("#village-alert").val("");
                    $("#village-alert").hide();


                }

            });

            $( "#district" ).change(function() {
                var district_code = $("#district").val();
                var province_id = $("#province").attr("data-id");
                if (district_code != "") {
                    if (province_id != undefined && province_id != "") {
                        $.ajax({
                            type: "POST",
                            url: "<?=$this->createUrl("/ajax/checkdistrict");?>",
                            data: { district_code:district_code,province_id:province_id },
                            success: function(data){
                                dataarray = JSON.parse(data);
                                if(dataarray['error'] == "have-data"){
                                    $("#district-out").val(dataarray["name"]);
                                    $("#district").attr("data-id", dataarray['id']);
                                    $("#district-alert").val("");
                                    $("#district-alert").hide();
                                }else{
                                    $("#district-out").val("");
                                    $("#district").attr("data-id", "");
                                    $("#district-alert").val("ไม่พบข้อมูล");
                                    $("#district-alert").show();
                                }
                            },
                        });
                    }else{
                        alert("กรุณากรอกจังหวัด");
                        $("#district").val("");
                    }
                }else{
                    $("#district").attr("data-id", "");
                    $("#district-out").val("");
                    $("#district-alert").val("");
                    $("#district-alert").hide();

                    $("#subdistrict").attr("data-id", "");
                    $("#subdistrict").val("");
                    $("#subdistrict-out").val("");
                    $("#subdistrict-alert").val("");
                    $("#subdistrict-alert").hide();

                    $("#village").attr("data-id", "");
                    $("#village").val("");
                    $("#village-out").val("");
                    $("#village-alert").val("");
                    $("#village-alert").hide();
                }

            });

            $( "#subdistrict" ).change(function() {
                var subdistrict_code = $("#subdistrict").val();
                var district_id = $("#district").attr("data-id");
                if (subdistrict_code != "") {
                    if (district_id != undefined && district_id != "") {
                        $.ajax({
                            type: "POST",
                            url: "<?=$this->createUrl("/ajax/checksubdistrict");?>",
                            data: { subdistrict_code:subdistrict_code,district_id:district_id },
                            success: function(data){
                                dataarray = JSON.parse(data);
                                if(dataarray['error'] == "have-data"){
                                    $("#subdistrict-out").val(dataarray["name"]);
                                    $("#subdistrict").attr("data-id", dataarray['id']);
                                    $("#subdistrict-alert").val("");
                                    $("#subdistrict-alert").hide();
                                }else{
                                    $("#subdistrict-out").val("");
                                    $("#subdistrict").attr("data-id", "");
                                    $("#subdistrict-alert").val("ไม่พบข้อมูล");
                                    $("#subdistrict-alert").show();
                                }
                            },
                        });
                    }else{
                        alert("กรุณากรอกอำเภอ");
                        $("#subdistrict").val("");
                    }
                }else{
                    $("#subdistrict").attr("data-id", "");
                    $("#subdistrict-out").val("");
                    $("#subdistrict-alert").val("");
                    $("#subdistrict-alert").hide();

                    $("#village").attr("data-id", "");
                    $("#village").val("");
                    $("#village-out").val("");
                    $("#village-alert").val("");
                    $("#village-alert").hide();
                }


            });

            $( "#village" ).change(function() {
                var village_code = $("#village").val();
                var subdistrict_id = $("#subdistrict").attr("data-id");
                if (village_code != "") {
                    if (subdistrict_id != undefined && subdistrict_id != "") {
                        $.ajax({
                            type: "POST",
                            url: "<?=$this->createUrl("/ajax/checkvillage");?>",
                            data: { village_code:village_code,subdistrict_id:subdistrict_id },
                            success: function(data){
                                dataarray = JSON.parse(data);
                                if(dataarray['error'] == "have-data"){
                                    $("#village-out").val(dataarray["name"]);
                                    $("#village").attr("data-id", dataarray['id']);
                                    $("#village-alert").val("");
                                    $("#village-alert").hide();
                                }else{
                                    $("#village-out").val("");
                                    $("#village").attr("data-id", "");
                                    $("#village-alert").val("ไม่พบข้อมูล");
                                    $("#village-alert").show();
                                }
                            },
                        });
                    }else{
                        alert("กรุณากรอกตำบล");
                        $("#village").val("");
                    }
                }else{
                    $("#village").attr("data-id", "");
                    $("#village-out").val("");
                    $("#village-alert").val("");
                    $("#village-alert").hide();
                }
            });


            $("#parent_farmer").change(function(event) {  
                 var parent_farmer =  document.getElementById("parent_farmer").checked;
// console.log(parent_farmer);
                 if (parent_farmer) {
                    var farmer_name = $("input[name='farmer_name']").val();
                    var farmer_lname = $("input[name='farmer_lname']").val();
                    var farmer_id = $("input[name='farmer_id']").val(); 
                    if (farmer_name != '' && farmer_lname != '') {

                         $("input[name='informant_name']").empty();
                         $("input[name='informant_name']").append(farmer_name);
                         $("input[name='informant_name']").val(farmer_name);

                         $("input[name='informant_lname']").empty();
                         $("input[name='informant_lname']").append(farmer_lname);
                         $("input[name='informant_lname']").val(farmer_lname);

                         $("input[name='informant_id']").empty();
                         $("input[name='informant_id']").append(farmer_id);
                         $("input[name='informant_id']").val(farmer_id);
                    
                }else{
                    alert("กรุณากรอกข้อมูล ชื่อผู้ขึ้นทะเบียนเกษตรกร");
                    document.getElementById("parent_farmer").checked = false;
                }
            }else{
                        $("input[name='informant_name']").empty();
                        $("input[name='informant_name']").val(null);

                        $("input[name='informant_lname']").empty();
                        $("input[name='informant_lname']").val(null);

                        $("input[name='informant_id']").empty();
                        $("input[name='informant_id']").val(null);

                        // var value = "n";
                        // $("input[name='parent_farmer']").empty();
                        // $("input[name='parent_farmer']").val(value);
            }
            });

            function onchangeoffice() {
                var county_code = $("#county-1").val();
                if (county_code != undefined && county_code != "") {
                    $.ajax({
                        type: "POST",
                        url: "<?=$this->createUrl("/ajax/checkoffice");?>",
                        data: { county_code:county_code },
                        success: function(data){
                            dataarray = JSON.parse(data);
                            if(dataarray['error'] == "have-data"){
                                $("#county-1-out").val(dataarray["name"]);
                                $("#county").attr("data-id", dataarray['id']);
                                $("#county-alert").val("");
                                $("#county-alert").hide();

                                onchangeProvince();
                            }
                        },
                    });
                }
            }


            function onchangeProvince() {
             var province_code = $("#province").val();
             var county_code = $("#county-1").val();
             if (province_code !="") {
                if (county_code != undefined && county_code != "") {
                    $.ajax({
                        type: "POST",
                        url: "<?=$this->createUrl("/ajax/checkprovince");?>",
                        data: { province_code:province_code,county_code:county_code },
                        success: function(data){
                            dataarray = JSON.parse(data);
                            if(dataarray['error'] == "have-data"){
                                $("#province-out").val(dataarray["name"]);
                                $("#province").attr("data-id", dataarray['id']);
                                $("#province-alert").val("");
                                $("#province-alert").hide();

                                onchangeDistrict();
                            }
                        },
                    });
                }

            }
        }

        function onchangeDistrict() {
            var district_code = $("#district").val();
            var province_id = $("#province").attr("data-id");
            if (district_code != "") {
                if (province_id != undefined && province_id != "") {
                    $.ajax({
                        type: "POST",
                        url: "<?=$this->createUrl("/ajax/checkdistrict");?>",
                        data: { district_code:district_code,province_id:province_id },
                        success: function(data){
                            dataarray = JSON.parse(data);
                            if(dataarray['error'] == "have-data"){
                                $("#district-out").val(dataarray["name"]);
                                $("#district").attr("data-id", dataarray['id']);
                                $("#district-alert").val("");
                                $("#district-alert").hide();

                                onchangeSubdistrict();
                            }
                        },
                    });
                }
            }

        }

        function onchangeSubdistrict() {
            var subdistrict_code = $("#subdistrict").val();
            var district_id = $("#district").attr("data-id");
            if (subdistrict_code != "") {
                if (district_id != undefined && district_id != "") {
                    $.ajax({
                        type: "POST",
                        url: "<?=$this->createUrl("/ajax/checksubdistrict");?>",
                        data: { subdistrict_code:subdistrict_code,district_id:district_id },
                        success: function(data){
                            dataarray = JSON.parse(data);
                            if(dataarray['error'] == "have-data"){
                                $("#subdistrict-out").val(dataarray["name"]);
                                $("#subdistrict").attr("data-id", dataarray['id']);
                                $("#subdistrict-alert").val("");
                                $("#subdistrict-alert").hide();

                                onchangeVillage();
                            }
                        },
                    });
                }
            }

        }

        function onchangeVillage() {
            var village_code = $("#village").val();
            var subdistrict_id = $("#subdistrict").attr("data-id");
            if (village_code != "") {
                if (subdistrict_id != undefined && subdistrict_id != "") {
                    $.ajax({
                        type: "POST",
                        url: "<?=$this->createUrl("/ajax/checkvillage");?>",
                        data: { village_code:village_code,subdistrict_id:subdistrict_id },
                        success: function(data){
                            dataarray = JSON.parse(data);
                            if(dataarray['error'] == "have-data"){
                                $("#village-out").val(dataarray["name"]);
                                $("#village").attr("data-id", dataarray['id']);
                                $("#village-alert").val("");
                                $("#village-alert").hide();
                            }
                        },
                    });
                }
            }
        }

        function onchangePuak() {
            var puak_code = $("#master-6").val();
            if (puak_code != "") {
                $.ajax({
                    type: "POST",
                    url: "<?=$this->createUrl("/ajax/checkpuak");?>",
                    data: { puak_code:puak_code },
                    success: function(data){
                        dataarray = JSON.parse(data);
                        if(dataarray['error'] == "have-data"){
                            $("#master-6-out").val(dataarray["name"]);
                            $("#master-6-alert").val("");
                            $("#master-6-alert").hide();
                        }
                    },
                });
            }
        }
    </script>