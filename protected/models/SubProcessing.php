<?php

/**
 * This is the model class for table "p_action".
 *
 * The followings are the available columns in table 'p_action':
 * @property integer $id
 * @property integer $controller_id
 * @property string $title
 * @property string $action
 * @property string $create_date
 * @property string $create_by
 * @property string $update_date
 * @property string $update_by
 */
class SubProcessing extends CActiveRecord
{

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'oae-survey.subprocessing';
	}


    // public function beforeSave()
    // {
    //     if ($this->isNewRecord){
    //         $this->create_date= new CDbExpression('NOW()');
    //         $this->create_by= Yii::app()->user->id;
    //     }else{
    //         $this->update_date= new CDbExpression('NOW()');
    //         $this->update_by= Yii::app()->user->id;
    //     }
    //     return true;
    // }


	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            // array('title, action', 'required'),
			array('id, processing_id, total, random, survey, created_by, updated_by, active', 'numerical', 'integerOnly'=>true),
			array('district, subdistrict, village, created_at, updated_at', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, processing_id, total, random, survey, district, subdistrict, village, created_by, updated_by, active, created_at, updated_at', 'safe', 'on'=>'search'),
		);
	}


	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'district_table' => array(self::BELONGS_TO, 'MtDistrict', 'district'),
			'subdistrict_table' => array(self::BELONGS_TO, 'MtSubDistrict', 'subdistrict'),
			'village_table' => array(self::BELONGS_TO, 'Village', 'village'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'id',
			'processing_id' => 'processing_id',
			'district' => 'district',
			'subdistrict' => 'subdistrict',
			'village'=>'village',
			'total' => 'total',
			'random' => 'random',
			'survey' => 'survey',
		);
	}

	public function beforeSave()
    {
        if ($this->isNewRecord){
            $this->created_at= new CDbExpression('NOW()');
            $this->created_by= Yii::app()->user->id;
            $this->active= 1;
        }else{
            $this->updated_at= new CDbExpression('NOW()');
            $this->updated_by= Yii::app()->user->id;
        }
        return true;
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('processing_id',$this->processing_id);
		$criteria->compare('district',$this->district,true);
		$criteria->compare('subdistrict',$this->subdistrict,true);
		$criteria->compare('village',$this->village,true);
		$criteria->compare('total',$this->total,true);
		$criteria->compare('random',$this->random,true);
		$criteria->compare('survey',$this->survey,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PAction the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
