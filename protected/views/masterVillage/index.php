
 <?php
	if(isset($_GET["masterVillage"])){
		$criteria=new CDbCriteria;
		if($_GET["masterVillage"]['village_name']){
			$criteria->compare('active', 1);
			$criteria->compare('CONCAT(village_name)',$_GET["masterVillage"]['village_name'],true);
		}
		else if($_GET["masterVillage"]['village_name'] == ""){
			$criteria->compare('active', 1);
		}
		if($_GET["masterVillage"]['subdistrict_id']){
			$criteria->compare('active', 1);
			$criteria->compare('subdistrict_id',$_GET["masterVillage"]['subdistrict_id']);
			$model->village_id = $_GET["masterVillage"]['subdistrict_id'];
		}
		$criteria->order = 'village_name ASC';
		$data = masterVillage::model()->findAll($criteria);
	}
	
 
 ?>
 <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
                <div class="container-fluid">

<!-- start row -->
<div class="row">
	<div class="col-lg-12 set-height set-padding">
		<div id="section1">
			<div class="card">
				<div class="card-header">
					<h5 class="mb-1">แบบสอบถามภาวะเศรษฐกิจสังคมครัวเรือนและแรงงานเกษตร ปีเพาะปลูก</h5>
				</div>
			</div>

			<div class="row justify-content-end">
				<div class="col-md-3">
					<a href="<?php echo Yii::app()->baseurl; ?>/masterVillage/create">
						<div class="card o-hidden create-main">
							<div class="b-r-4 card-body">
								<div class="media static-top-widget">
									<div class="align-self-center text-center"><i data-feather="plus"></i></div>
									<div class="media-body">
										<h6 class="mb-0 counter">เพิ่มข้อมูล</h6><i class="icon-bg" data-feather="database"></i>
									</div>
								</div>
							</div>
						</div>
					</a>
				</div>
			</div>

			<div class="card table-card">
				<div class="card-header">
				<?php $form=$this->beginWidget('AActiveForm', array(
					'action'=>YII::app()->createUrl($this->route),
					'method'=>'get'
				)); ?>
					<div class="row">

						<div class="col-md-3 mb-3">
							<div class="form-group">
								<label for="">ค้นหาชื่อหมู่บ้าน</label>
								<?php $village_name = (isset($_GET["masterVillage"]["village_name"]) ?
								 $_GET["masterVillage"]["village_name"] : "")?>
								<input class="form-control" id="searhVillage" type="text" name="masterVillage[village_name]"
								value="<?=$village_name?>" placeholder="ชื่อหมู่บ้าน">
							</div>
						</div>
						<div class="col-md-3 mb-3">
							<div class="form-group">
								<label for="">&nbsp;</label>
								<?php echo Helpers::lib()->listsubdistrictWithSelect($model,'subdistrict_id')?>
							</div>
						</div>
						<div class="col-md-3 mb-3">
							<button class="btn btn-lg btn-search text-white" type="submit"><i class="icon-search"></i>&nbsp;ค้นหา</button>
						</div>
					</div>
					<?php $this->endWidget();?>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-striped table-main" id="">
							<!-- <table class="table table-striped table-main" id="basic-2"> -->
							<thead>
								<tr>
									<th width="10%">ลำดับ</th>
									<th>ตำบล</th>
									<th width="50%">หมู่บ้าน</th>
									<th width="25%">จัดการข้อมูล</th>
								</tr>
							</thead>
							<tbody>
								<?php
								$dataProvider = new CArrayDataProvider($data,array(
									'pagination' => array(
									'pageSize' => 25
									),
								));
								$i = 0;
								$getPages = isset($_GET['page']) ? $_GET['page'] : 1;
								if($getPages !=0){
									$getPages = $getPages -1;
								}
								$start_cnt = $dataProvider->pagination->pageSize * $getPages;
								if(!empty($data)){
								foreach($dataProvider->getData() as $key => $row){		
									$i++;
									$criteria = new CDbCriteria;
      	  							$criteria->compare('id', $row->subdistrict_id);
									$subdis = MtSubDistrict::model()->find($criteria);

									echo '<tr>
										<td>'.$i.'</td>										
										<td>'.$subdis->subdistrict_name_th.'</td>
										<td>'.$row->village_name.'</td>
										<td>
											<a type="button" class="btn btn-icon text-primary" href="'.Yii::app()->baseurl.'/masterVillage/view/'.$row['village_id'].'"><i data-feather="eye"></i></button>
											<a type="button" class="btn btn-icon text-success" href="'.Yii::app()->baseurl.'/masterVillage/edit/'.$row['village_id'].'"><i data-feather="edit"></i></a>
											<a type="button" class="btn btn-icon text-danger" href="javascript:vold(0)" onclick="confirmDelete('.$row->village_id.')"><i data-feather="trash"></i></a>
										</td>
									</tr>';
								}
							}else{
								echo '<tr><td colspan="5">ไม่พบข้อมูล</td></tr>';
							}
								?>
							</tbody>
						</table>
						<?php if(!empty($data)){?>
						<div class="pull-right">
						<?php 
							$this->widget('CLinkPager',array(
								'pages'=>$dataProvider->pagination
								)
							);
						?>
					</div>
					<?php } ?>
				</div>

			</div>

		</div>

	</div>
</div>
<!-- end row -->
<div class="container position-fixed fixed-top pt-5" id="deleteForm" style="display:none;">
<div class="card col-12 p-0">
<div class="card-header">
<h5>ลบข้อมูล</h5>
</div>
<div class="card-body">
<h5 class="card-title">เมื่อข้อมูลถูกลบจะไม่สามารถย้อนคืนได้</h5>
<div class="card-text">
<form method="POST" action="<?php echo Yii::app()->baseurl; ?>/masterVillage/delete">
	<label for="deleteKey">รหัสอ้างอิง</label>
	<input id="deleteKey" name="key" class="form-control" value="" readonly/>
	<label for="deleteValue">ข้อมูล</label>
	<input id="deleteValue" name="value" class="form-control" value="" readonly/>
	<button class="btn btn-danger btn-lg mt-2">ลบข้อมูล</button>
	<button type="button" class="btn btn-light btn-lg mt-2" onclick='$("#deleteForm").hide();'>ยกเลิก</button>
</form>
</div>
</div>
</div>
</div>
</div>
</div>
<script type="text/javascript">
	function confirmDelete(id)
	{
		swal({
			title:"ต้องการลบหรือไม่",
			icon:"warning",
			buttons:true,
			dangerMode:true,
		})
		.then((willDelete) => {
			if(willDelete){
				$.ajax({
					url:"<?php echo $this->createUrl('/MasterVillage/delete');?>",
					type: "GET",
					data:{id: id},
					dataType: "html",
					success: function(){
						swal({
							title: "ลบข้อมูลสำเร็จ",
							icon: "success",
						});
						location.reload(true);
						window.location.href = "/oae/index.php/mastervillage/index";
					}
				});
			}else{
				swal({
					title:"ยกเลิกการลบข้อมูล",
					icon: "error",
				})
			}
		});
	}
</script>


