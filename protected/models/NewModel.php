<?php

class NewModel{

    public static function query($sql){
        $command = Yii::app()->db->createCommand($sql);
        return $command->queryAll();
    }

}