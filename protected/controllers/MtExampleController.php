<?php 

class MtExampleController extends Controller
{
	public $layout='admin';
    public function actionIndex()
    {
        $model = new MtExample;
		$criteria = new CDbCriteria;
		$criteria->compare('active', 1);
		$criteria->order = 'exam_name ASC';
		// var_dump($criteria->order);exit();
		$data = MtExample::model()->findAll($criteria);
		$this->render('index', Array(
			'data' => $data,
			'model' => $model,
		));
    }

	public function actionCreate()
    {
        $model=new MtExample;
		if(isset($_POST['MtExample']))
    	{
			$model->attributes = $_POST['MtExample'];
			$model->active = 1;
			if($model->validate()){
                // var_dump($model->attributes);exit();
				if($model->save()){
					$this->redirect(array('view', 'id' => $model->id));
				}
			}
		}
		$this->render('create', Array(
			'model' => $model
		));
    }

	public function actionView($id)
	{
		$this->render('view', Array(
			'model' => $this->loadModel($id),
		));
	}

	public function actionEdit($id)
	{
		$model = $this->loadModel($id);

		if (isset($_POST['MtExample']))
		{
			$model->attributes = $_POST['MtExample'];
			if ($model->validate()){
				if ($model->save()){
					$this->redirect(array('view', 'id' => $model->id));
				}
			}
		}
		$this->render('update', Array(
			'model' => $model
		));
	}

	public function actionDelete($id)
	{
		$model = $this->loadModel($id);
		$model->active = 0;

		if ($model->save()) {
			$this->redirect(array('index'));
		}
	}

	public function loadModel($id)
	{
		$model = MtExample::model()->findByPk($id);
		return $model;
	}
}

?>