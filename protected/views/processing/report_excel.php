<?php
header("Content-Type: application/vnd.ms-excel");
header('Content-Disposition: attachment; filename="MyXls.xls"');
$strExcelFileName = 'export-'.'ค่าคูณขยาย'."-".date("d-m-Y") . ".xls";
header("Content-Type: application/x-msexcel; name=\"" . $strExcelFileName . "\"");
header("Content-Disposition: inline; filename=\"" . $strExcelFileName . "\"");
header('Content-Type: text/plain; charset=UTF-8');
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Pragma:no-cache");
?>

<html xmlns:o=”urn:schemas-microsoft-com:office:office”

xmlns:x=”urn:schemas-microsoft-com:office:excel”

xmlns=”http://www.w3.org/TR/REC-html40″>

<HTML>

<HEAD>
  <meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />
</HEAD>
<BODY>
    <table>
        <thead>
            <tr>
                <th>ลำดับ</th>
                <th>พวกที่</th>
                <th>จังหวัด</th>
                <th>อำเภอ</th>
                <th>ตำบล</th>
                <th>หมู่ที่</th>
                <th>จำนวนหมู่ทั้งหมด</th>
                <th>จำนวนหมู่สุ่ม</th>
                <th>จำนวนหมู่สำรวจ</th>
                <th>ครัวเรือนเกษตรทั้งหมด</th>
                <th>ครัวเรือนเกษตรตามพวก</th>
                <th>ครัวเรือนเกษตรสำรวจ</th>

            </tr>
        </thead>
        <tbody>
            <?php 
            $number=1;
            

            $arrayProvince = array();
            $arrayThose = array();
            foreach ($Processing as $key => $value) {
                if (!empty($value->province) && $value->province != null) {
                    $arrayProvince[] = $value->province;
                }
                if (!empty($value->group) && $value->group != null) {
                    $arrayThose[] = $value->group;
                }
            }
            $arrayProvince = array_unique($arrayProvince);
            $arrayThose = array_unique($arrayThose);


            $criteria = new CDbCriteria();
            // $criteria->compare('active',1);
            $criteria->addInCondition('those_id',$arrayThose);
            $criteria->order = "those_id ASC";
            $Those = Those::model()->findAll($criteria);

            foreach ($Those as $keyThose => $valueThose) {
                $criteria = new CDbCriteria();
                $criteria->compare('active',1);
                $criteria->addInCondition('id',$arrayProvince);
                $criteria->order = "province_code ASC";
                $MtProvince = MtProvince::model()->findAll($criteria);
                foreach ($MtProvince as $keyMtProvince => $valueMtProvince) {
                    $MtDistrict = MtDistrict::model()->findAll(array(
                        'condition' => 'active=:active AND province_id=:province_id',
                        'params' => array(':active' => '1',':province_id' => $valueMtProvince->id),
                        'order'=>'district_code',
                    ));
                    foreach ($MtDistrict as $keyMtDistrict => $valueMtDistrict) {
                        $MtSubDistrict = MtSubDistrict::model()->findAll(array(
                            'condition' => 'active=:active AND district_id=:district_id',
                            'params' => array(':active' => '1',':district_id' => $valueMtDistrict->id),
                            'order'=>'subdistrict_code',
                        ));
                        foreach ($MtSubDistrict as $keyMtSubDistrict => $valueMtSubDistrict) {
                            $masterVillage = masterVillage::model()->findAll(array(
                                'condition' => 'active=:active AND subdistrict_id=:subdistrict_id',
                                'params' => array(':active' => '1',':subdistrict_id' => $valueMtSubDistrict->id),
                                'order'=>'village_code',
                            ));
                            foreach ($masterVillage as $keymasterVillage => $valuemasterVillage) {
                                $criteria = new CDbCriteria();
                                $criteria->compare('province',$valueMtProvince->id);
                                // $criteria->compare('year',$valueQ->survey_id);
                                $criteria->compare('t.group',$valueThose->those_id);
                                $criteria->compare('active',1);
                                $Processing = Processing::model()->find($criteria);
                                if (!empty($Processing)) {
                                    $criteria = new CDbCriteria();
                                    $criteria->compare('processing_id',$Processing->id);
                                    $criteria->compare('district',$valueMtDistrict->id);
                                    $criteria->compare('subdistrict',$valueMtSubDistrict->id);
                                    $criteria->compare('village',$valuemasterVillage->village_id);
                                    $criteria->compare('active',1);
                                    $SubProcessing = SubProcessing::model()->find($criteria);
                                    if (!empty($SubProcessing)) { ?>
                                        <tr>
                                            <td><?=$number++;?></td>
                                            <td><?=$valueThose->those_id?></td>
                                            <td>'<?=$valueMtProvince->province_code?></td>
                                            <td>'<?=$valueMtDistrict->district_code?></td>
                                            <td>'<?=$valueMtSubDistrict->subdistrict_code?></td>
                                            <td>'<?=$valuemasterVillage->village_code?></td>

                                            <td><?=$Processing->total?></td>
                                            <td><?=$Processing->random?></td>
                                            <td><?=$Processing->survey?></td>
                                            <td><?=$SubProcessing->total?></td>
                                            <td><?=$SubProcessing->random?></td>
                                            <td><?=$SubProcessing->survey?></td>
                                        </tr>
                                    <?php }
                                }
                            }
                        }
                    }
                }
            }
            ?>
        </tbody>
    </table>

</BODY>


</HTML>