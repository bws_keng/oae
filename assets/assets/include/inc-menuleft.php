<header class="main-nav">
  <!-- <div class="logo-wrapper">
    <div class="back-btn"><i class="fa fa-angle-left"></i></div>
  </div> -->
  <nav>
    <div class="main-navbar">
      <div class="left-arrow" id="left-arrow"><i data-feather="arrow-left"></i></div>
      <div id="mainnav">
        <ul class="nav-menu custom-scrollbar">
          <li class="back-btn">
            <div class="mobile-back text-right"><span>Back</span><i class="fa fa-angle-right pl-2" aria-hidden="true"></i></div>
          </li>
          <li class="dropdown"><a class="nav-link menu-title link-nav" href="index.php"><i data-feather="home"> </i><span>หน้าหลัก</span></a></li>
        
          <li class="dropdown"><a class="nav-link menu-title" href="#"><i data-feather="list"></i><span>ระบบจัดการแบบสอบถาม</span></a>
            <ul class="nav-submenu menu-content">
              <li><a href="admin-0-1.php">จัดการคำถาม</a></li>
              <li><a href="admin-0-3.php">จัดการแบบสอบถาม</a></li>
            </ul>
          </li>

          <li class="dropdown"><a class="nav-link menu-title" href="#"><i data-feather="edit"></i><span>ระบบจัดการหมวดหมู่</span></a>
            <ul class="nav-submenu menu-content">
              <li><a class="submenu-title " href="#">หมวดที่ 1<span class="sub-arrow"><i class="fa fa-angle-right"></i></span></a>
                <ul class="nav-sub-childmenu submenu-content">
                  <li><a href="admin-1-0.php">จัดการคำถาม</a></li>
                  <li><a href="admin-1-2.php">จัดการคำตอบ</a></li>
                  <li><a href="admin-1-4.php">จัดการหมวดหมู่</a></li>
                </ul>
              </li>

              <li><a class="submenu-title " href="#">หมวดที่ 2.1<span class="sub-arrow"><i class="fa fa-angle-right"></i></span></a>
                <ul class="nav-sub-childmenu submenu-content">
                  <li><a href="admin-2-1-1.php">จัดการประเภทคำถาม</a></li>
                  <li><a href="admin-2-1-3.php">จัดการคำถาม</a></li>
                  <li><a href="admin-2-1-5.php">จัดการหมวดหมู่</a></li>
                </ul>
              </li>

              <li><a class="submenu-title " href="#">หมวดที่ 2.2<span class="sub-arrow"><i class="fa fa-angle-right"></i></span></a>
                <ul class="nav-sub-childmenu submenu-content">
                  <li><a href="admin-2-2-1.php">จัดการประเภทคำถาม</a></li>
                  <li><a href="admin-2-2-3.php">จัดการคำถาม</a></li>
                  <li><a href="admin-2-2-5.php">จัดการหมวดหมู่</a></li>
                </ul>
              </li>


              <li><a class="submenu-title " href="#">หมวดที่ 3<span class="sub-arrow"><i class="fa fa-angle-right"></i></span></a>
                <ul class="nav-sub-childmenu submenu-content">
                  <li><a href="admin-3-1.php">จัดการประเภทคำถาม</a></li>
                  <li><a href="admin-3-3.php">จัดการคำถาม</a></li>
                  <li><a href="admin-3-5.php">จัดการหมวดหมู่</a></li>
                </ul>
              </li>

              <li><a class="submenu-title " href="#">หมวดที่ 4<span class="sub-arrow"><i class="fa fa-angle-right"></i></span></a>
                <ul class="nav-sub-childmenu submenu-content">
                  <li><a href="admin-4-0.php">จัดการประเภทคำถาม</a></li>
                  <li><a href="admin-4-2.php">จัดการคำถาม</a></li>
                  <li><a href="admin-4-4.php">จัดการหมวดหมู่</a></li>
                </ul>
              </li>

              <li><a class="submenu-title " href="#">หมวดที่ 5<span class="sub-arrow"><i class="fa fa-angle-right"></i></span></a>
                <ul class="nav-sub-childmenu submenu-content">
                  <li><a href="admin-5-0.php">จัดการประเภทคำถาม</a></li>
                  <li><a href="admin-5-2.php">จัดการคำถาม</a></li>
                  <li><a href="admin-5-4.php">จัดการหมวดหมู่</a></li>
                </ul>
              </li>

              <li><a class="submenu-title " href="#">หมวดที่ 6<span class="sub-arrow"><i class="fa fa-angle-right"></i></span></a>
                <ul class="nav-sub-childmenu submenu-content">
                  <li><a href="admin-6-1.php">จัดการประเภทคำถาม</a></li>
                  <li><a href="admin-6-3.php">จัดการคำถาม</a></li>
                  <li><a href="admin-6-5.php">จัดการหมวดหมู่</a></li>
                </ul>
              </li>

              <li><a class="submenu-title " href="#">หมวดที่ 7<span class="sub-arrow"><i class="fa fa-angle-right"></i></span></a>
                <ul class="nav-sub-childmenu submenu-content">
                  <li><a href="admin-7-0.php">จัดการประเภทคำถาม</a></li>
                  <li><a href="admin-7-2.php">จัดการคำถาม</a></li>
                  <li><a href="admin-7-4.php">จัดการหมวดหมู่</a></li>
                </ul>
              </li>

              <li><a class="submenu-title " href="#">หมวดที่ 8<span class="sub-arrow"><i class="fa fa-angle-right"></i></span></a>
                <ul class="nav-sub-childmenu submenu-content">
                  <li><a href="admin-8-1.php">จัดการคำถาม</a></li>
                  <li><a href="admin-8-3.php">จัดการคำตอบ</a></li>
                  <li><a href="admin-8-5.php">จัดการหมวดหมู่</a></li>
                </ul>
              </li>

              <li><a class="submenu-title " href="#">หมวดที่ 9<span class="sub-arrow"><i class="fa fa-angle-right"></i></span></a>
                <ul class="nav-sub-childmenu submenu-content">
                  <li><a href="admin-9-0.php">จัดการประเภทคำถาม</a></li>
                  <li><a href="admin-9-2.php">จัดการคำถาม</a></li>
                  <li><a href="admin-9-4.php">จัดการหมวดหมู่</a></li>
                </ul>
              </li>

              <li><a class="submenu-title " href="#">หมวดที่ 10<span class="sub-arrow"><i class="fa fa-angle-right"></i></span></a>
                <ul class="nav-sub-childmenu submenu-content">
                  <li><a href="admin-10-0.php">จัดการคำถาม</a></li>
                  <li><a href="admin-10-2.php">จัดการคำตอบ</a></li>
                  <li><a href="admin-10-4.php">จัดการหมวดหมู่</a></li>
                </ul>
              </li>

              <li><a class="submenu-title " href="#">หมวดที่ 11<span class="sub-arrow"><i class="fa fa-angle-right"></i></span></a>
                <ul class="nav-sub-childmenu submenu-content">
                  <li><a href="admin-11-0.php">จัดการแบบสอบถาม</a></li>
                </ul>
              </li>
            </ul>
          </li>

          <li class="dropdown"><a class="nav-link menu-title link-nav" href="admin-12.php"><i data-feather="list"> </i><span>ระบบแบบสอบถาม</span></a></li>

          <li class="dropdown"><a class="nav-link menu-title link-nav" href="#"><i data-feather="edit"> </i><span>ระบบและบริการต่างๆ</span></a></li>

          <li class="dropdown"><a class="nav-link menu-title link-nav" href="admin-13.php"><i data-feather="list"> </i><span>ระบบการประมวลผลข้อมูล</span></a></li>

          <li class="dropdown"><a class="nav-link menu-title" href="#"><i data-feather="edit"></i></i><span>ระบบกำหนดสิทธิ์การใช้งาน</span></a>
            <ul class="nav-submenu menu-content">
              <li><a href="admin-14-1.php">จัดการเจ้าหน้าที่สำนักเขต</a></li>
              <li><a href="admin-14-3.php">จัดการผู้ดูแลระบบ</a></li>
              <li><a href="admin-14-5.php">จัดการกลุ่มผู้ใช้งาน</a></li>
              <li><a href="admin-14-7.php">จัดการสิทธิ์</a></li>
            </ul>
          </li>

          <li class="dropdown"><a class="nav-link menu-title link-nav" href="admin-15-1.php"><i data-feather="edit"> </i><span>ระบบรายงาน</span></a></li>
        </ul>

        <!--ระบบกำหนดสิทธิ์การใช้งาน-->

      </div>
      <div class="right-arrow" id="right-arrow"><i data-feather="arrow-right"></i></div>
    </div>
  </nav>
</header>