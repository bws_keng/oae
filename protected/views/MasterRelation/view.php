<?php echo $this->renderPartial('_form', array(
	'model' 	=> $model,
	'formtext' 	=> 'รายละเอียดความสัมพันธ์กับหัวหน้าครัวเรือน'
)); ?>

    <div class="row text-center group-submit">
        <a href="<?php echo Yii::app()->baseurl;?>/MasterRelation/index" class="btn btn-outline-secondary btn-lg" >ย้อนกลับ</a>
    </div>

<script>
    $(document).ready(function () {
        $(':input').attr('disabled','disabled');
        $(':submit').attr('style','display:none;');
        $('#button-cancel').attr('style','display:none;');
    });
</script>