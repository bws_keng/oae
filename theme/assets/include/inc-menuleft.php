<header class="main-nav">
  <!-- <div class="logo-wrapper">
    <div class="back-btn"><i class="fa fa-angle-left"></i></div>
  </div> -->
  <nav>
    <div class="main-navbar">
      <div class="left-arrow" id="left-arrow"><i data-feather="arrow-left"></i></div>
      <div id="mainnav">
        <ul class="nav-menu custom-scrollbar">
          <li class="back-btn">
            <div class="mobile-back text-right"><span>Back</span><i class="fa fa-angle-right pl-2" aria-hidden="true"></i></div>
          </li>
          <li class="dropdown"><a class="nav-link menu-title link-nav" href="index.php"><i data-feather="home"> </i><span>หน้าหลัก</span></a></li>

          <li class="dropdown"><a class="nav-link menu-title" href="#"><i data-feather="check-square"></i><span>แบบสอบถาม</span></a>
            <ul class="nav-submenu menu-content">
              <li><a href="1-0-0_form.php">ภาวะเศรษฐกิจสังคมครัวเรือนและแรงงานเกษตร</a></li>
            </ul>
          </li>

          <li class="dropdown"><a class="nav-link menu-title " href="#"><i data-feather="edit"></i><span>รายการหมวดข้อมูล</span></a>
            <ul class="nav-submenu menu-content">
              <li><a href="1-1_form.php">หมวดที่ 1</a></li>
              <li><a class="submenu-title " href="#">หมวดที่ 2<span class="sub-arrow"><i class="fa fa-angle-right"></i></span></a>
                <ul class="nav-sub-childmenu submenu-content">
                  <li><a href="1-2-1-0_form.php">หมวดที่ 2.1</a></li>
                  <li><a href="1-2-2-0_form.php">หมวดที่ 2.2</a></li>
                </ul>
              </li>
              <li><a href="1-3-0_form.php">หมวดที่ 3</a></li>
              <li><a href="1-4-0_form.php">หมวดที่ 4</a></li>
              <li><a href="1-5-0_form.php">หมวดที่ 5</a></li>
              <li><a href="1-6-0_form.php">หมวดที่ 6</a></li>
              <li><a href="1-7-0_form.php">หมวดที่ 7</a></li>
              <li><a href="1-8-0_form.php">หมวดที่ 8</a></li>
              <li><a href="1-9-0_form.php">หมวดที่ 9</a></li>
              <li><a href="1-10-0_form.php">หมวดที่ 10</a></li>
              <li><a href="1-11-1_form.php">หมวดที่ 11</a></li>
            </ul>
          </li>

      </div>
      <div class="right-arrow" id="right-arrow"><i data-feather="arrow-right"></i></div>
    </div>
  </nav>
</header>