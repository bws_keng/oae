<form class="container-fluid" method="POST" action="<?php echo Yii::app()->baseurl.'/admin/questionaire/survey/'.$_GET['id'].'/form'; ?>">
    <!-- start row -->
    <div class="row">
        <div class="col-lg-12 set-height set-padding">
            <div id="section1">
                <div class="card">
                    <div class="card-header">
                        <h5 class="mb-1">จัดฟอร์มแบบสอบถาม</h5>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <div class="col-md-12 mb-3">
                            <label for="">รายการคำถาม</label>
                        </div>
                        <form>
                            <div class="row ui-sortable" id="draggableMultiple">
                                <?php 
                                    foreach ($data as $key => $value) {
                                        echo
                                            '';
                                        ?>
                                        <div class="col-sm-12 col-xl-12">
                                            <div class="card">
                                                <div class="row card-body">
                                                    <label class="col-md-8 col-form-label">
                                                    <p class="mb-0"><?php echo $value['title'] ; ?> </p>
                                                    <small class="text-danger"><?php echo $value['subtitle'] ; ?></small>
                                                    </label>
                                                        <input type="hidden" class="form-control" name="form[]" value="<?php echo $value['question_id']; ?>" placeholder="">
                                                    <div class="col-md-2">
                                                        <input class="form-control text-center mt-3" maxlength="1" type="number" data-original-title="" title="" disabled />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                ?>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="row text-center group-submit">
                <button class="btn btn-primary btn-lg">บันทึกข้อมูล</button>
            </div>
        </div>
    </div>
    <!-- end row -->
</form>