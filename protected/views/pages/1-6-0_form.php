<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="description" content="Cuba admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, Cuba admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="pixelstrap">
    <title>หมวดที่ 6 รายได้เงินสดจากการเกษตรอื่นๆ และนอกการเกษตร</title>
    <?php include 'theme/assets/include/inc-head.php'; ?>
</head>

<body data-spy="scroll" data-target="#myScrollspy">
    <div class="tap-top"><i data-feather="chevrons-up"></i></div>

    <div class="page-wrapper compact-wrapper" id="pageWrapper">

        <?php include 'theme/assets/include/inc-header.php'; ?>

        <div class="page-body-wrapper horizontal-menu">
            <?php include 'theme/assets/include/inc-menuleft.php'; ?>

 
                <div class="container-fluid">

                    <div class="row">
                        <div class="col-lg-12 set-height set-padding">
                            <div id="section1">
                                <div class="card">
                                    <div class="card-header">
                                        <h5 class="mb-1">หมวดที่ 6 รายได้เงินสดจากการเกษตรอื่นๆ และนอกการเกษตร (ปี พ.ศ.2562 - พ.ศ.2563)</span></h5>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-body">

                                        <form>
                                            <div class="row">
                                                <div class="col-md-2 mb-3">
                                                    <label for="">จังหวัด</label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-2 mb-3">
                                                    <label for="">อำเภอ</label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-2 mb-3">
                                                    <label for="">ตำบล</label>
                                                    <div class="input-group">
                                                        <input class="form-control" id="" type="text" placeholder="" aria-describedby="" required="" data-original-title="" title="">
                                                    </div>
                                                </div>
                                                <div class="col-md-2 mb-3">
                                                    <label for="">หมู่ที่</label>
                                                    <div class="input-group">
                                                        <input class="form-control" id="" type="text" placeholder="" aria-describedby="" required="" data-original-title="" title="">
                                                    </div>
                                                </div>
                                                <div class="col-md-2 mb-3">
                                                    <label for="">พวกที่</label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-2 mb-3">
                                                    <label for="">ตัวอย่างที่</label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                            </div>
                                        </form>

                                    </div>
                                </div>
                            </div>

                            <h5 class="mb-3">รายการรายได้</h5>
                            <div class="card">
                                <div class="card-body">

                                    <form>
                                        <div id="section2">
                                            <div class="row">
                                                <div class="col-12">
                                                    <h6 class="text-head mb-3"><i class="icon-pencil-alt"></i> รายได้เงินสดเกษตรในฟาร์ม</h6>
                                                    <h6 class="text-under">ที่ดินทางการเกษตร</h6>
                                                </div>
                                                <div class="col-md-8 mb-3">
                                                    <label for="">ค่าเช่าที่ดินทางการเกษตร<small class="text-danger"> (รหัส 1)</small></label>

                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">รายได้รวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-8 mb-3">
                                                    <label for="">ขายที่ดินทางการเกษตร<small class="text-danger"> (รหัส 2)</small></label>

                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">รายได้รวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-12">
                                                    <h6 class="text-under">ทรัพย์สินทางการเกษตรอื่นๆ</h6>
                                                </div>
                                                <div class="col-md-8 mb-3">
                                                    <label for="">ค่าเช่าทรัพย์สินทางการเกษตรอื่นๆ เช่น โรงเรียนเครื่องจักร/อุปกรณ์ทางการเกษตร<small class="text-danger"> (รหัส 3)</small></label>

                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">รายได้รวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-8 mb-3">
                                                    <label for="">ขายทรัพย์สินทางการเกษตรอื่นๆ เช่น เครื่องจักร/อุปกรณ์ทางการเกษตร<small class="text-danger"> (รหัส 4)</small></label>

                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">รายได้รวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-8 mb-3">
                                                    <label for=""><span class="text-danger">กำไรสุทธิ</span>จากการแปรรูปผลผลิตทางการเกษตรในฟาร์ม<small class="text-danger"> (รหัส 5)</small></label>

                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">รายได้รวมทั้งหมด<small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-8 mb-3">
                                                    <label for=""><u>เงินช่วยเหลือด้านการเกษตรจากรัฐบาล</u> เช่น เงินชดเชยการเกิดภัยพิบัติกับพื้นที่ทำการ
                                                        เกษตรของครัวเรือนเงินช่วยเหลือด้านปัจจัยการผลิต<small class="text-danger"> (รหัส 6)</small></label>

                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">รายได้รวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-8 mb-3">
                                                    <label for="">รายได้เงินสดเกษตรในฟาร์มอื่นๆ<small class="text-danger"> (รหัส 7)</small></label>

                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">รายได้รวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                        </div>
                                        <hr>

                                        <div id="section2">
                                            <div class="row">
                                                <div class="col-12">
                                                    <h6 class="text-head mb-3"><i class="icon-pencil-alt"></i> รายได้เงินสดเกษตรนอกฟาร์ม</h6>
                                                    <h6 class="text-under">รับจ้างเกษตรนอกฟาร์ม</h6>
                                                </div>
                                                <div class="col-md-8 mb-3">
                                                    <label for="">รับจ้างแรงงานเกษตรนอกฟาร์ม<small class="text-danger"> (รหัส 8)</small></label>

                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">รายได้รวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-8 mb-3">
                                                    <label for="">นำยานพาหนะ/เครื่องมือของตนเองไปรับจ้างเกษตรนอกฟาร์ม
                                                        เช่น นำรถเกี่ยวไปรับจ้างนำรถบรรทุกไปรับจ้างขนผลผลิต
                                                        (<span class="text-danger">รายได้สุทธิ</span> หลังหักค่าใช้จ่าย)<small class="text-danger"> (รหัส 9)</small></label>

                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">รายได้รวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-8 mb-3">
                                                    <label for="">นำสัตว์ไปรับจ้างเกษตรนอกฟาร์ม เช่น รับจ้างพันธ์
                                                        รับจ้างขนผลผลิต รับจ้างแรงงานสัตว์อื่นๆ
                                                        (<span class="text-danger">รายได้สุทธิ</span> หลังหักค่าใช้จ่าย)<small class="text-danger"> (รหัส 10)</small></label>

                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">รายได้รวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-8 mb-3">
                                                    <label for=""><span class="text-danger">กำไรสุทธิ</span>จากการแปรรูปผลผลิตทางการเกษตรนอกฟาร์ม<small class="text-danger"> (รหัส 11)</small></label>

                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">รายได้รวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-8 mb-3">
                                                    <label for=""><span class="text-danger">กำไรสุทธิ</span>จากการค้าขายผลผลิตทางการเกษตร (ซื้อมา-ขายไป)<small class="text-danger"> (รหัส 12)</small></label>

                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">รายได้รวมทั้งหมด<small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-8 mb-3">
                                                    <label for="">รายได้เงินสดเกษตรนอกฟาร์มอื่นๆ<small class="text-danger"> (รหัส 13)</small></label>

                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">รายได้รวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                        </div>
                                        <hr>

                                        <div id="section2">
                                            <div class="row">
                                                <div class="col-12">
                                                    <h6 class="text-head mb-3"><i class="icon-pencil-alt"></i> รายได้เงินสดนอกการเกษตร</h6>
                                                    <h6 class="text-under">ที่ดินนอกการเกษตร</h6>
                                                </div>
                                                <div class="col-md-8 mb-3">
                                                    <label for="">ค่าเช่าที่ดินนอกการเกษตร<small class="text-danger"> (รหัส 14)</small></label>

                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">รายได้รวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-8 mb-3">
                                                    <label for="">ขายที่ดินนอกการเกษตร<small class="text-danger"> (รหัส 15)</small></label>

                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">รายได้รวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-12">
                                                    <h6 class="text-under">ทรัพย์สินนอกการเกษตรอื่นๆ</h6>
                                                </div>
                                                <div class="col-md-8 mb-3">
                                                    <label for="">ค่าเช่าทรัพย์สินนอกการเกษตรอื่นๆ<small class="text-danger"> (รหัส 16)</small></label>

                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">รายได้รวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-8 mb-3">
                                                    <label for="">ขายทรัพย์สินนอกการเกษตรอื่นๆ เช่น ขายรถยนต์
                                                        ขายเครื่องใช้ไฟฟ้า<small class="text-danger"> (รหัส 17)</small></label>

                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">รายได้รวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-12">
                                                    <h6 class="text-under">รับจ้างนอกการเกษตร</h6>
                                                </div>
                                                <div class="col-md-8 mb-3">
                                                    <label for="">รับจ้างแรงงานนอกการเกษตร (รายวัน)
                                                        เช่นรับจ้างก่อสร้างรับจ้างงานช่างทั่วไปขับรถรับจ้าง
                                                        แม่บ้านทำความสะอาด พนักงานรักษาความปลอดภัย<small class="text-danger"> (รหัส 18)</small></label>

                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">รายได้รวมทั้งหมด<small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-8 mb-3">
                                                    <label for="">นำยานพาหนะ/เครื่องมือของตนเองไปรับจ้างนอกการเกษตร
                                                        เช่น นำรถไปรับจ้างส่งของ/ส่งคน
                                                        (<span class="text-danger">รายได้สุทธิ</span> หลังหักค่าใช้จ่าย) หลังหักค่าใช้จ่าย)(<span class="text-danger">รายได้สุทธิ</span> หลังหักค่าใช้จ่าย)<small class="text-danger"> (รหัส 19)</small></label>

                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">รายได้รวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-8 mb-3">
                                                    <label for="">เงินเดือนจากการทำงานประจำ<small class="text-danger"> (รหัส 20)</small></label>

                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">รายได้รวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-12">
                                                    <h6 class="text-under">การประกอบธุรกิจการค้า</h6>
                                                </div>
                                                <div class="col-md-8 mb-3">
                                                    <label for=""><span class="text-danger">กำไรสุทธิ</span>จากการค้าขายนอกการเกษตร (ซื้อมา-ขายไป) เช่น เปิดร้านขายของชำ<small class="text-danger"> (รหัส 21)</small></label>

                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">รายได้รวมทั้งหมด<small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-8 mb-3">
                                                    <label for=""><span class="text-danger">กำไรสุทธิ</span>จากการประกอบธุรกิจให้บริการนอกการเกษตร เช่น ร้านอาหาร ร้านเสริมสวย อู่ซ่อมรถ <small class="text-danger"> (รหัส 22)</small></label>

                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">รายได้รวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-8 mb-3">
                                                    <label for=""><span class="text-danger">กำไรสุทธิ</span>จากการทำหัตถกรรมในครัวเรือน เช่น เย็บผ้า ร้อยพวงมาลัยขาย ปั้นหม้อ<small class="text-danger"> (รหัส 23)</small></label>

                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">รายได้รวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-8 mb-3">
                                                    <label for=""><span class="text-danger">กำไรสุทธิ</span>จากการประกอบธุรกิจนอกการเกษตรอื่นๆ<small class="text-danger"> (รหัส 24)</small></label>

                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">รายได้รวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-12">
                                                    <h6 class="text-under">เงินโอน</h6>
                                                </div>
                                                <div class="col-md-8 mb-3">
                                                    <label for="">ได้รับเงินที่ลูกหลานในต่างถิ่น/ในต่างประเทศส่งให้<small class="text-danger"> (รหัส 25)</small></label>

                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">รายได้รวมทั้งหมด<small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-8 mb-3">
                                                    <label for=""><u>เงินช่วยเหลือนอกการเกษตรจากรัฐบาล</u> เช่น
                                                        เงินชดเชยการเกิดภัยพิบัติกับบ้านเรือนที่อยู่อาศัย
                                                        เบี้ยยังชีพผู้สูงอายุ เบี้ยยังชีพคนพิการ<small class="text-danger"> (รหัส 26)</small></label>

                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">รายได้รวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-8 mb-3">
                                                    <label for="">เงินบำนาญรายเดือน เงินบำเหน็จ<small class="text-danger"> (รหัส 27)</small></label>

                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">รายได้รวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-8 mb-3">
                                                    <label for="">ได้รับดอกเบี้ยเงินฝาก/ดอกเบี้ยเงินกู้/ดอกเบี้ยแชร์/เงินปันผล<small class="text-danger"> (รหัส 28)</small></label>

                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">รายได้รวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-8 mb-3">
                                                    <label for="">ได้รับโบนัส/ค่านายหน้า/เงินประกัน/เงินชดเชยอื่นๆ<small class="text-danger"> (รหัส 29)</small></label>

                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">รายได้รวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-8 mb-3">
                                                    <label for="">ได้รับเงินมรดก<small class="text-danger"> (รหัส 30)</small></label>

                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">รายได้รวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-8 mb-3">
                                                    <label for="">ได้รับเงินจากการพนัน/เสี่ยงโชค<small class="text-danger"> (รหัส 31)</small></label>

                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">รายได้รวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-8 mb-3">
                                                    <label for="">ได้รับเงินช่วยเหลือจากงานพิธีกรรม เช่น งานบวช งานแต่ง งานศพ<small class="text-danger"> (รหัส 32)</small></label>

                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">รายได้รวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-8 mb-3">
                                                    <label for="">รายได้เงินสดนอกการเกษตรอื่นๆ ระบุ<small class="text-danger"> (รหัส 33)</small></label>

                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">รายได้รวมทั้งหมด <small class="text-danger">(บาท)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                                </div>
                                            </div>

                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-md-8 mb-3">

                                            </div>
                                            <div class="col-md-4 mb-3">
                                                <label for="">ผลรวมรายได้เงินสดจากการเกษตรอื่นๆและนอกการเกษตรทั้งปี <small class="text-danger">(บาท)</small></label>
                                                <input class="form-control" id="" type="text" placeholder="บาท" required="" data-original-title="" title="">
                                            </div>
                                        </div>



                                    </form>

                                </div>
                            </div>

                            <div class="row text-center group-submit">
                                <button type="button" class="btn btn-primary btn-lg">บันทึกข้อมูล</button>
                                <button type="button" class="btn btn-outline-secondary btn-lg">ยกเลิก</button>
                            </div>
                        </div>
                    </div>
                    <!-- end row -->

                </div>


 