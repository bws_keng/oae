<?php 
$objPHPExcel = new PHPExcel();

        // Set document properties
        $objPHPExcel->getProperties()->setCreator("K'iin Balam")
             ->setLastModifiedBy("K'iin Balam")
             ->setTitle("YiiExcel Test Document")
             ->setSubject("YiiExcel Test Document")
             ->setDescription("Test document for YiiExcel, generated using PHP classes.")
             ->setKeywords("office PHPExcel php YiiExcel UPNFM")
             ->setCategory("Test result file");        
        
        // Add some data
        $alphabet = array('H1','I1','J1','K1','L1','M1','N1','O1','P1','Q1','R1','S1','T1','U1','V1','W1','X1','Y1','Z1','AA1','AB1','AC1','AD1','AE1','AF1','AG1','AH1','AI1','AJ1','AK1','AL1','AM1','AN1','AO1','AP1','AQ1','AR1','AS1','AT1','AU1','AV1','AW1','AX1','AY1','AZ1');

        $objPHPExcel->setActiveSheetIndex(0)
           ->setCellValue('A1', 'หมวดที่')
            ->setCellValue('B1', 'จังหวัด')
            ->setCellValue('C1', 'อำเภอ')
            ->setCellValue('D1', 'ตำบล')
            ->setCellValue('E1', 'หมู่ที่')
            ->setCellValue('F1', 'พวกที่')
            ->setCellValue('G1', 'ตัวอย่างที่');
        foreach ($items as $key => $value) {
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue($alphabet[$key], $value['question']);
        }
           
        // Miscellaneous glyphs, UTF-8
        $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A2', $items[0]["question_group_number"]);
        
        // Rename worksheet
        $objPHPExcel->getActiveSheet()->setTitle('YiiExcel');
        
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);
        
        // Save a xls file
        $filename = 'YiiExcel';
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="หมวดที่-'.$items[0]["question_group_number"].'.xls"');
        header('Cache-Control: max-age=0');
        
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

        $objWriter->save('php://output');
        unset($this->objWriter);
        unset($this->objWorksheet);
        unset($this->objReader);
        unset($this->objPHPExcel);
        exit();

 ?>