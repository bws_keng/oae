<!-- latest jquery-->
<!-- Bootstrap js-->
<script src="<?php echo Yii::app()->baseUrl; ?>/theme/assets/js/bootstrap/popper.min.js"></script>
<script src="<?php echo Yii::app()->baseUrl; ?>/theme/assets/js/bootstrap/bootstrap.js"></script>
<!-- feather icon js-->
<script src="<?php echo Yii::app()->baseUrl; ?>/theme/assets/js/icons/feather-icon/feather.min.js"></script>
<script src="<?php echo Yii::app()->baseUrl; ?>/theme/assets/js/icons/feather-icon/feather-icon.js"></script>
<!-- Sidebar jquery-->
<script src="<?php echo Yii::app()->baseUrl; ?>/theme/assets/js/sidebar-menu.js"></script>
<script src="<?php echo Yii::app()->baseUrl; ?>/theme/assets/js/config.js"></script>
<!-- Plugins JS start-->
<script src="<?php echo Yii::app()->baseUrl; ?>/theme/assets/js/jquery.ui.min.js"></script>
<script src="<?php echo Yii::app()->baseUrl; ?>/theme/assets/js/dragable/sortable.js"></script>
<script src="<?php echo Yii::app()->baseUrl; ?>/theme/assets/js/dragable/sortable-custom.js"></script>
<script src="<?php echo Yii::app()->baseUrl; ?>/theme/assets/js/datatable/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo Yii::app()->baseUrl; ?>/theme/assets/js/datatable/datatables/datatable.custom.js"></script>
<script src="<?php echo Yii::app()->baseUrl; ?>/theme/assets/js/jsgrid/jsgrid.min.js"></script>
<script src="<?php echo Yii::app()->baseUrl; ?>/theme/assets/js/jsgrid/griddata.js"></script>
<script src="<?php echo Yii::app()->baseUrl; ?>/theme/assets/js/jsgrid/jsgrid.js"></script>
<script src="<?php echo Yii::app()->baseUrl; ?>/theme/assets/js/chart/chartist/chartist.js"></script>
<script src="<?php echo Yii::app()->baseUrl; ?>/theme/assets/js/chart/chartist/chartist-plugin-tooltip.js"></script>
<script src="<?php echo Yii::app()->baseUrl; ?>/theme/assets/js/chart/knob/knob.min.js"></script>
<script src="<?php echo Yii::app()->baseUrl; ?>/theme/assets/js/chart/knob/knob-chart.js"></script>
<script src="<?php echo Yii::app()->baseUrl; ?>/theme/assets/js/chart/apex-chart/apex-chart.js"></script>
<script src="<?php echo Yii::app()->baseUrl; ?>/theme/assets/js/chart/apex-chart/stock-prices.js"></script>
<script src="<?php echo Yii::app()->baseUrl; ?>/theme/assets/js/notify/bootstrap-notify.min.js"></script>
<script src="<?php echo Yii::app()->baseUrl; ?>/theme/assets/js/dashboard/default.js"></script>
<script src="<?php echo Yii::app()->baseUrl; ?>/theme/assets/js/notify/index.js"></script>
<script src="<?php echo Yii::app()->baseUrl; ?>/theme/assets/js/datepicker/date-picker/datepicker.js"></script>
<script src="<?php echo Yii::app()->baseUrl; ?>/theme/assets/js/datepicker/date-picker/datepicker.en.js"></script>
<script src="<?php echo Yii::app()->baseUrl; ?>/theme/assets/js/datepicker/date-picker/datepicker.th.js"></script>
<script src="<?php echo Yii::app()->baseUrl; ?>/theme/assets/js/datepicker/date-picker/datepicker.custom.js"></script>
<script src="<?php echo Yii::app()->baseUrl; ?>/theme/assets/js/typeahead/handlebars.js"></script>
<script src="<?php echo Yii::app()->baseUrl; ?>/theme/assets/js/typeahead/typeahead.bundle.js"></script>
<script src="<?php echo Yii::app()->baseUrl; ?>/theme/assets/js/typeahead/typeahead.custom.js"></script>
<script src="<?php echo Yii::app()->baseUrl; ?>/theme/assets/js/typeahead-search/handlebars.js"></script>
<script src="<?php echo Yii::app()->baseUrl; ?>/theme/assets/js/typeahead-search/typeahead-custom.js"></script>
<script src="<?php echo Yii::app()->baseUrl; ?>/theme/assets/js/tooltip-init.js"></script>
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="<?php echo Yii::app()->baseUrl; ?>/theme/assets/js/script.js"></script>
<!-- <script src="<?php echo Yii::app()->baseUrl; ?>/theme/assets/js/theme-customizer/customizer.js"></script> -->

<script type="text/javascript">
const variables = {}
const data = {}
$("input[id^='master-']:not([id$='out']):not([id$='alert'])").each(async (key, value) => {
    if(value.getAttribute('data-master') != null && value.getAttribute('data-master') != undefined){
        if(value.getAttribute('data-master') in data){
            console.log("Data "+value.getAttribute('data-master')+" has loaded")
        }else{
            fetch('<?php echo Yii::app()->baseurl;?>/api/' + value.getAttribute('data-master'))
            .then(response => response.json())
            .then(out => data[value.getAttribute('data-master')] = out)
            .then(() => console.log("Data "+value.getAttribute('data-master')+" has loaded"))
            .catch(() => console.error("Error to load "+value.getAttribute('data-master')))
        }
    }
});


$("input[id^='master-']:not([id$='out']):not([id$='alert'])").change((e) => {
    const output = $("#"+ e.target.id + "-out")[0]
    const alert = $("#"+ e.target.id + "-alert")
    // console.log(e.target.id, e.target.getAttribute('data-master') ,e.target.value)

    var key = parseInt(e.target.value);
    key = key.toString();
    // console.log(key.length);

    if(key.length <= 1){
        key = "0"+(key.toString());
    }
    // console.log(key);
    console.log(data[e.target.getAttribute('data-master')]['data']);
    if(key in data[e.target.getAttribute('data-master')]['data']){
        const out = data[e.target.getAttribute('data-master')]['data'][key][data[e.target.getAttribute('data-master')]['value']]
        output.value = out
        alert.hide()
    }else{
        output.value = "ข้อมูลไม่ถูกต้อง"
        alert.show()
    }
    
})

$(document).ready(function(){
      $("#master-3").change(function(event) {
         var check_district = $(this).val();
          $.ajax({
            type: 'POST',
            url: "<?= Yii::app()->createUrl('Questionnaire/checkDistrict'); ?>",
            data: {
                id: check_district
         },
          success: function(data) {
            console.log(data);
          }
      });

    });

    for (var i = 1; i <= 6; i++) {
      var Data = $("#master-"+ i).val();
      if (Data != null) {
        switch(i) {
          case 1:
             check_edit_county(Data);
            break;
          case 2:
             check_edit_province(Data);
            break;
          case 3:
             check_edit_district(Data);
            break;
          case 4:
             check_edit_subdistrict(Data);
            break;
          case 5:
             check_edit_village(Data);
            break;
          default:
        }
        
      } 
     }

  });
  
  function check_edit_county(id){
    if (id) {
      $.ajax({
            type: 'POST',
            url: "<?= Yii::app()->createUrl('Questionnaire/editCounty'); ?>",
            data: {
                id: id
         },
          success: function(data) {
            $("#master-1-out").val(data);
          }
      })
    }
  }

   function check_edit_province(id){
    if (id) {
      $.ajax({
            type: 'POST',
            url: "<?= Yii::app()->createUrl('Questionnaire/editProvince'); ?>",
            data: {
                id: id
         },
          success: function(data) {
            $("#master-2-out").val(data);
          }
      })
    }
  }

   function check_edit_district(id){
    if (id) {
      $.ajax({
            type: 'POST',
            url: "<?= Yii::app()->createUrl('Questionnaire/editDistrict'); ?>",
            data: {
                id: id
         },
          success: function(data) {
            $("#master-3-out").val(data);
          }
      })
    }
  }

  function check_edit_subdistrict(id){
    if (id) {
      $.ajax({
            type: 'POST',
            url: "<?= Yii::app()->createUrl('Questionnaire/editSubdistrict'); ?>",
            data: {
                id: id
         },
          success: function(data) {
            $("#master-4-out").val(data);
          }
      })
    }
  }

  function check_edit_village(id){
    if (id) {
      $.ajax({
            type: 'POST',
            url: "<?= Yii::app()->createUrl('Questionnaire/editVillage'); ?>",
            data: {
                id: id
         },
          success: function(data) {
            $("#master-5-out").val(data);
          }
      })
    }
  }

  function calculated(target){
  const input = $(target)
  const id = target.id.split('-')[2]
  const output = $("#calculate-output-" + id)
  var Calobj = $("#question-sum-" +input.data('function')).val();
  Calobj = Calobj.replaceAll(",", "");
  if (input.val() >= 0 && Calobj>=0) {
      const func = "" + input.val() * Calobj;
      try {
        output.val(parseFloat(eval(func)).toLocaleString())
      } catch (error) {
        output.val("ไม่สามารถคำนวนได้")
      }
    }
    
  }
  $("input[id^='calculate-input-']").each((key, element) => {calculated(element)})
  $("input[id^='calculate-input-']").keyup((event) => {calculated(event.target)})

  function updateOutput(){
    $("input[id^='output-']").each(async (key, element) => {
      const output = $(element)
      const func = output.data('function')
      // console.log(func);
      try {
        output.val(eval(func))
      } catch (error) {
        output.val("ไม่สามารถคำนวนได้")
        console.error(error);
      }
    })
  }

  function assign_variable(target){
    const input = $(target)
    // console.log(input.val());
    if(input.val() != undefined && input.val() != null && input.val() != ''){
      variables[input.data('assign')] = parseInt(input.val())
    }else{
      variables[input.data('assign')] = 0.0
    }
    updateOutput()
  }

  $("input[id^='assign-']").each((key, element) => {assign_variable(element)})
  $("input[id^='assign-']").change((event) => {assign_variable(event.target)})

  function sum(variable){
    console.log("Install Sum", variable)
    const input = $("input[data-variable='" + variable + "']:not([id^='sum-out'])")
    const output = $("input[data-variable='" + variable + "']:not([id^='sum-in'])")
    try {
      let total = 0
      input.each((key, item) => {
        total += parseInt(item.value)
      })
      output.val(total)
    } catch (error) {
      output.val("มีการกรอกข้อมูลไม่ถูกต้อง")
    }
  }

  $("input[id^='sum-']").each((key, element) => {sum($(element).data('variable'))})
  $("input[id^='sum-']").change((event) => {sum($(event.target).data('variable'))})

  $("input[type ='float']").focus(function() {
    var num_cal = $(this).val() + "";
    num_cal = num_cal.replaceAll(",", "");
    if (num_cal == "" || num_cal == null) {
      num_cal = 0;
    }

    if (num_cal == 0) {
      $(this).val("");
    } else if (num_cal >= 1) {
      $(this).val(num_cal);
    }

  });
  $("input[type ='float']").blur(function() {
    var num_cal = $(this).val();
    num_cal = num_cal.replaceAll(",", "");
    if (num_cal == "" || num_cal == null) {
      num_cal = 0;
    }
    $(this).val(parseFloat(num_cal).toLocaleString());
  });

  $("input[type ='int']").focus(function() {
    var num_cal = $(this).val() + "";
    num_cal = num_cal.replaceAll(",", "");
    if (num_cal == "" || num_cal == null) {
      num_cal = 0;
    }

    if (num_cal == 0) {
      $(this).val("");
    } else if (num_cal >= 1) {
      $(this).val(num_cal);
    }

  });
  $("input[type ='int']").blur(function() {
    var num_cal = $(this).val();
    num_cal = num_cal.replaceAll(",", "");
    if (num_cal == "" || num_cal == null) {
      num_cal = 0;
    }
    $(this).val(parseFloat(num_cal).toLocaleString());
  });

  $('input[type="float"]').each(function(){
    var num_cal = $(this).val();
    num_cal = num_cal.replaceAll(",", "");
    if (num_cal == "" || num_cal == null) {
      num_cal = 0;
    }
    $(this).val(parseFloat(num_cal).toLocaleString());
  })

  $('input[type="int"]').each(function(){
    var num_cal = $(this).val();
    num_cal = num_cal.replaceAll(",", "");
    if (num_cal == "" || num_cal == null) {
      num_cal = 0;
    }
    $(this).val(parseFloat(num_cal).toLocaleString());
  })
</script>