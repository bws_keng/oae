<?php 
$criteria = new CDbCriteria();
$criteria->compare('id',$id_prcessing);
$criteria->compare('active',1);
$Processing_sub = Processing::model()->find($criteria);

$criteria = new CDbCriteria();
$criteria->compare('id',$Processing_sub->province);
$criteria->compare('active',1);
$Province_sub = Province::model()->find($criteria);
?>
<div class="row">
    <div class="table-responsive">
        <table class="table table-borderless table-modalmain" id="">
            <tbody>
                <tr>
                    <td class="text-right">ปีเพาะปลูก :</td>
                    <?php
                    $Survey = Survey::model()->findAll();
                    ?>
                    <td><span class="text-main">
                        <select class="form-control digits" id="" name="year_sub" disabled="true">
                            <option selected disabled>เลือกปี พ.ศ.</option>
                            <?php
                            if (!empty($Survey)) {
                            foreach ($Survey as $key => $value) { 
                              $year = $value->year + 543;
                              ?>
                               <option value="<?= $value->id;  ?>" <?php if(isset($Processing_sub->year) && $Processing_sub->year == $value->id){ echo "selected"; } ?> ><?= $year;  ?>/63</option>
                               <?php
                               }
                             }
                           ?>
                       </select>
                   </span></td>
               </tr>
               <tr>
                <td class="text-right">พวกที่ :</td>
                <td><span class="text-main">
                    <select class="form-control digits" id="" name="Those_sub" disabled="true">
                        <option selected disabled>เลือกพวกที่</option>
                        <?php
                        if (isset($Processing_sub->group)) {

                           $criteria = new CDbCriteria();
                            $criteria->compare('active',1);
                            $criteria->compare('those_id',$Processing_sub->group);
                            $criteria->order = "those_id ASC";
                            $Those = Those::model()->find($criteria);
                            if ($Those) {
                          ?>
                           <option value="<?= $Those->those_id;  ?>"<?php if(isset($Processing_sub->group) && $Processing_sub->group == $Those->those_id){ echo "selected"; } ?> ><?= $Those->those_code;  ?> -> <?= $Those->those_name;  ?></option>
                           <?php }
                       }
                       ?>
                   </select>
               </span></td>
           </tr>
           <tr>
            <td class="text-right">จังหวัด :</td>
            <td><span class="text-main">
                <select class="form-control digits" id="" name="Province_sub" disabled="true">
                    <option selected disabled>เลือกจังหวัด</option>
                    <?php
                    if (isset($Processing_sub->province)) {
                      $criteria = new CDbCriteria();
                      $criteria->compare('active',1);
                      $criteria->compare('id',$Processing_sub->province);
                      $Province = Province::model()->find($criteria);
                      if ($Province) {
                      ?>
                       <option value="<?= $Province->id;  ?>"  <?php if(isset($Processing_sub->province) && $Processing_sub->province == $Province->id){ echo "selected"; } ?>><?= $Province->province_code;  ?> -> <?= $Province->province_name_th;  ?></option>
                       <?php
                     }
                   }
                   ?>
               </select>
           </span></td>
       </tr>
       <tr>
        <td class="text-right">อำเภอ :<font color="red">*</font></td>
        <?php
        $criteria = new CDbCriteria();
        $criteria->compare('active',1);
        $criteria->compare('province_id',$Province_sub->id);
        $criteria->order = "district_name_th ASC";
        $District = District::model()->findAll($criteria);

        ?>
        <td><span class="text-main">
            <select class="form-control digits" id="District_selected" name="District_sub">
                <option selected disabled>เลือกอำเภอ</option>
                <?php
                if (!empty($District)) {
                foreach ($District as $key => $value) { ?>
        <!--            <option value="<?= $value->id;  ?>"><?= $value->district_code; ?>&nbsp;&nbsp;<?= $value->district_name_th;  ?></option> -->
                      <option value = "<?= $value->id;  ?>" data-id="<?=$value->id ?>"><?= $value->district_code ?> -> <?= $value->district_name_th?> </option>
                   <?php
                 }
               }
               ?>
           </select>
       </span></td>
   </tr>
   <tr>
    <td class="text-right">ตำบล :<font color="red">*</font></td>
    <td><span class="text-main">
        <select class="form-control digits" id="SubDistrict_selected" name="SubDistrict_sub">
            <option selected disabled>เลือกตำบล</option>
       </select>
   </span></td>
</tr>
<tr>
    <td class="text-right">หมู่บ้าน :<font color="red">*</font></td>
    <td><span class="text-main">
        <select class="form-control digits" id="Village_selected" name="Village_sub">
            <option selected disabled>เลือกหมู่บ้าน</option>
      </select></span></td>
  </tr>

</tbody>
</table>
</div>
</div>
<hr>
<div class="row">
    <div class="table-responsive">
        <table class="table table-borderless table-modalmain" id="">
            <tbody>
                <tr>
                    <td class="text-right">ครัวเรือนเกษตรทั้งหมด :</td>
                    <td><span class="text-main"><input class="form-control" type="text" name="total_sub" id="total_sub-data" model-id="0"></span></span></td>
                </tr>
                <tr>
                    <td class="text-right">ครัวเรือนเกษตรตามพวก :</td>
                    <td><span class="text-main"><input class="form-control" type="text" name="Random_sub" id="random_sub-data"></span></td>
                </tr>
                <tr>
                    <td class="text-right">สำรวจ :</td>
                    <td><span class="text-main"><input class="form-control" type="text" name="survey_sub" id="survey_sub-data" ></span></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
    // $("#random_sub-data").keyup(function(){
    //   $("#survey_sub-data").val($(this).val());
    // });
  });
      $("#District_selected").change(function() {
        var id = $(this).val();
        if (id != "") {
            $.ajax({
                type: 'POST',
                url: "<?= $this->createUrl('/Processing/checkSubDistrict'); ?>", 
                data: {
                    id_District: id,
                },
                success: function(data) {

                $('#SubDistrict_selected').empty();
                $('#SubDistrict_selected').append(data);
                $('#Village_selected').empty();

                }
            });
        }
    });

    $("#SubDistrict_selected").change(function() {
        var id = $(this).val();
        if (id != "") {
            $.ajax({
                type: 'POST',
                url: "<?= $this->createUrl('/Processing/checkVillage'); ?>", 
                data: {
                    id_Village: id,
                },
                success: function(data) {
                $('#Village_selected').empty();
                $('#Village_selected').append(data);

                }
            });
        }
    });

     $("#District_selected").select2( {
     placeholder: "เลือกอำเภอ",
     allowClear: true,
      width: '100%',
      dropdownParent: $("#exampleModal2")
     });

    $("#SubDistrict_selected").select2( {
     placeholder: "เลือกตำบล",
     allowClear: true,
      width: '100%',
      dropdownParent: $("#exampleModal2")
     });

    $("#Village_selected").select2( {
     placeholder: "เลือกหมู่ที่",
     allowClear: true,
      width: '100%',
      dropdownParent: $("#exampleModal2")
     });
</script>