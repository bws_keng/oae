<?php $form=$this->beginWidget('AActiveForm', array(
 'id'=>'masterCounty-form',
 'enableClientValidation'=>true,
 'clientOptions'=>array(
  'validateOnSubmit'=>true
),
 'errorMessageCssClass' => 'label label-important',
 'htmlOptions' => array('enctype' => 'multipart/form-data')
)); ?>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12 set-height set-padding">
            <div id="section1">
                <div class="card">
                    <div class="card-header">
                        <h5 class="mb-1"><?=$formtext?> </h5>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <div>
                            <div class="row">
                                <div class="col-md-4 mb-3">
                                    <?php echo $form->labelEx($model,'district_name_th'); ?>
                                    <?php echo Helpers::lib()->listdistrict($model,'district_id') ?>     
                                </div>
                                <div class="col-md-4 mb-3">
                                    <?php echo $form->labelEx($model,'subdistrict_name_th'); ?>
                                    <?php echo $form->textField($model,'subdistrict_name_th',array('size'=>60,'maxlength'=>250, 'class'=>'form-control')); ?>
                                </div>
                                <div class="col-md-4 mb-3">
                                    <?php echo $form->labelEx($model,'subdistrict_code'); ?>
                                    <?php echo $form->textField($model,'subdistrict_code',array('size'=>60,'maxlength'=>250, 'class'=>'form-control')); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row text-center group-submit">
        <?php echo CHtml::tag('button',array('class' => 'btn btn-primary btn-lg'),'<i></i>บันทึกข้อมูล');?>
        <button type="button" id="button-cancel" class="btn btn-outline-secondary btn-lg" onclick="window.history.go(-1); return false;">ยกเลิก</button>
    </div>

    <?php $this->endWidget(); ?>
</div>

