<?php
/**
 * คลาสสำหรับการสร้างหน้า ใช้ชั่วคราวกรณีที่ต้องการจัดการหน้าแทนการใช้ php แบบเดิม
 */
class SurveyController extends Controller
{

 public function init()
 {
  if(!Yii::app()->user->id ){
    $this->redirect(Yii::app()->baseurl.'/user/login');
}else{
    $criteria=new CDbCriteria;
    $criteria->compare('id',Yii::app()->user->id);
    $user = User::model()->find($criteria);
    if ($user->superuser == 1) {
       $this->layout = "admin";
   }
}
}

    // public function actionIndex()
	// {
	// 	$this->render('index');
    // }

    // /**
    //  * ฟังค์ชั่นสำหรับแสดงหน้าเว็บไซต์ตามไฟล์ที่อยู่ใน `/views/pages/`
    //  * โดยนำค่าจาก GET ที่มี key ว่า `page` มาแสดง
    //  *  */ 
    // public function actionView()
    // {
    //     $data = new FormModel($_GET['id']);

    //     // if(count($data->data()) > 0){
            // $this->render('index', Array(
            //     'data' => $data->data(),
            // ));
    //     // }
    // }

    // public function actionCreate()
    // {

    //     $data = new FormModel($_GET['id']);

    //     if($_GET['id'] == 0){
    //         $this->render('create', Array(
    //             'data' => $data->data(),
    //         ));
    //     }else{
    //         $this->render('create', Array(
    //             'data' => $data->data(),
    //         ));
    //     }

    // }

    // public function actionInsert()
    // {
    //     $data = $_POST;
    //     $target = Array();
    //     foreach ($data as $key => $value){
    //         $type = substr($key, 0,strpos($key, '-'));
    //         $question_id = substr($key, strpos($key, '-')+1);
    //         if($type == 'question'){
    //             $question_group_id = substr($question_id, 0,strpos($question_id, '-'));
    //             $question_subid = substr($question_id, strpos($question_id, '-')+1);
    //             $target['value'][$question_subid] = $value;
    //             $target['question_id'] = $question_group_id;
    //         }else{
    //             $target[$question_id] = $value;
    //         }
    //     }
    //     $out = FormModel::create($target);
    //     $this->render('post', Array(
    //         'data' => $target,
    //     ));
    // }

    // public function actionQuestionnaire(){
    //     $this->render('questionnaire', Array(
    //         'question' => FormModel::questionnaireExtend()
    //     ));
    // }

    // public function actionQuestionnairelist(){
    //     $this->render('questionnairelist');
    // }

public function actionexcel($id)
{
    $group = FormModel::getTitle($_GET['id'])[0];
    $criteria=new CDbCriteria;
    $criteria->compare('active',1);
    $criteria->order = "province_code ASC";
    $MtProvince = MtProvince::model()->findAll($criteria);

    $criteria=new CDbCriteria;
    $criteria->order = "id DESC";
    $survey = Survey::model()->find($criteria);

    $question_id = array();
    $criteria = new CDbCriteria;
    $criteria->select = "question_id";
    $criteria->addNotInCondition('type',['note','header','bottom-line','bullet','sum-output-number','parent-ques-num','parent-ques-value']);
    $criteria->compare('question_group_id',$group["question_group_id"]);
    $Question = Question::model()->findAll($criteria);
    foreach ($Question as $key => $value) {
        $question_id[] = $value->question_id;
    }

    $criteria = new CDbCriteria;
    $criteria->compare('survey',$survey->id);
    $criteria->addInCondition('question',$question_id);
    $criteria->order = "t.order ASC";
    $QuestionMap = SurveyMap::model()->findAll($criteria);

    $criteria = new CDbCriteria;
    $QuestionnaireQuestion = QuestionnaireQuestion::model()->findAll($criteria);
    $model = new User('import');
    $this->render('excel',array(
        "model"=>$model,
        'group'=>$group,
        'MtProvince'=>$MtProvince,
        'QuestionMap'=>$QuestionMap,
        'QuestionnaireQuestion'=>$QuestionnaireQuestion,
    ));
}

public function actionTemplateexcel($id)
{
    $group = FormModel::getTitle($_GET['id'])[0];
    $criteria=new CDbCriteria;
    $criteria->compare('active',1);
    $criteria->order = "province_code ASC";
    $MtProvince = MtProvince::model()->findAll($criteria);

    $criteria=new CDbCriteria;
    $criteria->order = "id DESC";
    $survey = Survey::model()->find($criteria);

    $question_id = array();
    $criteria = new CDbCriteria;
    $criteria->select = "question_id";
    $criteria->addNotInCondition('type',['note','header','bottom-line','bullet','ques-num-value']);
    $criteria->compare('question_group_id',$group["question_group_id"]);
    $Question = Question::model()->findAll($criteria);
    foreach ($Question as $key => $value) {
        $question_id[] = $value->question_id;
    }


    $criteria = new CDbCriteria;
    $criteria->compare('survey',$survey->id);
    $criteria->addInCondition('question',$question_id);
    $criteria->order = "t.order ASC";
    $QuestionMap = SurveyMap::model()->findAll($criteria);


    $criteria = new CDbCriteria;
    $QuestionnaireQuestion = QuestionnaireQuestion::model()->findAll($criteria);

    $criteria = new CDbCriteria;
    $criteria->compare('survey_id',$survey->id);
    $criteria->order = "t.questionnaire_id ASC";
    $Questionnaire = Questionnaire::model()->find($criteria);

    $model = new User('import');
    $this->layout=false;
    if ($group["question_group_id"] == 7) {
        $criteria=new CDbCriteria;
        $criteria->addNotInCondition('type',['note','header','bottom-line','bullet','ques-num-value','sum-output-number','parent-ques-num','parent-ques-value']);
        $criteria->compare('question_group_id',$id);
        $criteria->order = "t.readable_id ASC";
        $Question = Question::model()->findAll($criteria);
        $question_id = array();
        foreach ($Question as $key => $value) {
            $question_id[] = $value->question_id;
        }
        $criteria = new CDbCriteria;
        $criteria->compare('survey',$survey->id);
        $criteria->addInCondition('question',$question_id);
        $criteria->order = "t.order ASC";
        $QuestionMap = SurveyMap::model()->findAll($criteria);
        $this->render('excel-template-7',array(
            "Questionnaire"=>$Questionnaire,
            "model"=>$model,
            'group'=>$group,
            'MtProvince'=>$MtProvince,
            'QuestionMap'=>$QuestionMap,
            'QuestionnaireQuestion'=>$QuestionnaireQuestion,
        ));
    }elseif($group["question_group_id"] == 8) {
        $criteria=new CDbCriteria;
        $criteria->addNotInCondition('type',['note','header','bottom-line','bullet','ques-num-value','sum-output-number','parent-ques-num','parent-ques-value']);
        $criteria->compare('question_group_id',$id);
        $criteria->order = "t.readable_id ASC";
        $Question = Question::model()->findAll($criteria);
        $question_id = array();
        foreach ($Question as $key => $value) {
            $question_id[] = $value->question_id;
        }
        $criteria = new CDbCriteria;
        $criteria->compare('survey',$survey->id);
        $criteria->addInCondition('question',$question_id);
        $criteria->order = "t.order ASC";
        $QuestionMap = SurveyMap::model()->findAll($criteria);
        $this->render('excel-template-8',array(
            "Questionnaire"=>$Questionnaire,
            "model"=>$model,
            'group'=>$group,
            'MtProvince'=>$MtProvince,
            'QuestionMap'=>$QuestionMap,
            'QuestionnaireQuestion'=>$QuestionnaireQuestion,
        ));
    }elseif($group["question_group_id"] == 10){
        $criteria=new CDbCriteria;
        $criteria->addNotInCondition('type',['note','header','bottom-line','bullet','sum-output-number','parent-ques-num','parent-ques-value']);
        $criteria->compare('question_group_id',$id);
        $criteria->order = "t.readable_id ASC";
        $Question = Question::model()->findAll($criteria);
        $question_id = array();
        foreach ($Question as $key => $value) {
            $question_id[] = $value->question_id;
        }
        $criteria = new CDbCriteria;
        $criteria->compare('survey',$survey->id);
        $criteria->addInCondition('question',$question_id);
        $criteria->order = "t.order ASC";
        $QuestionMap = SurveyMap::model()->findAll($criteria);
        $this->render('excel-template-10',array(
            "Questionnaire"=>$Questionnaire,
            "model"=>$model,
            'group'=>$group,
            'MtProvince'=>$MtProvince,
            'QuestionMap'=>$QuestionMap,
            'QuestionnaireQuestion'=>$QuestionnaireQuestion,
        ));
    }elseif($group["question_group_id"] == 12){
        $question_id = array();
        $criteria = new CDbCriteria;
        $criteria->addNotInCondition('type',['note','header','bottom-line','bullet','ques-num-value']);
        $criteria->compare('question_group_id',$group["question_group_id"]);
        $criteria->order = "t.sortorder ASC";
        $Question = Question::model()->findAll($criteria);

        $criteria = new CDbCriteria;
        $criteria->addInCondition('question_id',$question_id);
        $QuestionSub = QuestionSub::model()->findAll($criteria);

        $this->render('excel-template-12',array(
            "Questionnaire"=>$Questionnaire,
            "model"=>$model,
            'group'=>$group,
            'MtProvince'=>$MtProvince,
            'Question'=>$Question,
            'QuestionSub'=>$QuestionSub,
            'QuestionnaireQuestion'=>$QuestionnaireQuestion,
        ));

    }else{
        $this->render('excel-template',array(
            "Questionnaire"=>$Questionnaire,
            "model"=>$model,
            'group'=>$group,
            'MtProvince'=>$MtProvince,
            'QuestionMap'=>$QuestionMap,
            'QuestionnaireQuestion'=>$QuestionnaireQuestion,
        ));

    }

}

public function actionexport($id)
{
    $group = FormModel::getTitle($_GET['id'])[0];
    $criteria=new CDbCriteria;
    $criteria->compare('active',1);
    $criteria->order = "province_code ASC";
    $MtProvince = MtProvince::model()->findAll($criteria);

    $criteria=new CDbCriteria;
    $criteria->order = "id DESC";
    $survey = Survey::model()->find($criteria);

    $question_id = array();
    $criteria = new CDbCriteria;
    $criteria->select = "question_id";
    $criteria->addNotInCondition('type',['note','header','bottom-line','bullet']);
    $criteria->compare('question_group_id',$group["question_group_id"]);
    $Question = Question::model()->findAll($criteria);
    if ($id == 7 || $id == 10) {
        $criteria = new CDbCriteria;
        $criteria->addNotInCondition('type',['note','header','bottom-line','bullet','sum-output-number','parent-ques-num','parent-ques-value']);
        $criteria->compare('question_group_id',$group["question_group_id"]);
        $criteria->order = "t.readable_id ASC";
        $Question = Question::model()->findAll($criteria);
    }
    foreach ($Question as $key => $value) {
        $question_id[] = $value->question_id;
    }


    $criteria = new CDbCriteria;
    $criteria->compare('survey',$survey->id);
    $criteria->addInCondition('question',$question_id);
    $criteria->order = "t.order ASC";
    $QuestionMap = SurveyMap::model()->findAll($criteria);


    $criteria = new CDbCriteria;
    $QuestionnaireQuestion = QuestionnaireQuestion::model()->findAll($criteria);

    if ($id == 12) {
        $question_id = array();
        $criteria = new CDbCriteria;
        //$criteria->select = "question_id";
        $criteria->addNotInCondition('type',['note','header','bottom-line','bullet']);
        $criteria->compare('question_group_id',$group["question_group_id"]);
        $criteria->order = "sortorder ASC";
        $Question = Question::model()->findAll($criteria);

        foreach ($Question as $key => $value) {
            $question_id[] = $value->question_id;
        }

        $criteria = new CDbCriteria;
        $criteria->addInCondition('question_id',$question_id);
        $QuestionSub = QuestionSub::model()->findAll($criteria);

        $this->render('export-12',array(
            'group'=>$group,
            'MtProvince'=>$MtProvince,
            'Question'=>$Question,
            'QuestionnaireQuestion'=>$QuestionnaireQuestion,
            'QuestionSub'=>$QuestionSub,
            'QuestionMap'=>$QuestionMap,
        ));
        exit();
    }

    $this->render('export',array(
        'group'=>$group,
        'MtProvince'=>$MtProvince,
        'Question'=>$Question,
        'QuestionnaireQuestion'=>$QuestionnaireQuestion,
        'QuestionMap'=>$QuestionMap,
    ));
}

public function actionexportExcel()
{
    if ($_POST["export"]) {

        $group = FormModel::getTitle($_POST['export']["id"])[0];

        $criteria=new CDbCriteria;
        $criteria->order = "id DESC";
        $survey = Survey::model()->find($criteria);

        if($group["question_group_id"] == 12){
            $criteria=new CDbCriteria;
            $criteria->addNotInCondition('type',['note','header','bottom-line','bullet','sum-output-number','parent-ques-num','parent-ques-value']);
            $criteria->compare('question_group_id',$_POST['export']["id"]);
            $criteria->order = "t.sortorder ASC";
            $Question = Question::model()->findAll($criteria);

            $question_id = array();
            foreach ($Question as $key => $value) {
                $question_id[] = $value->question_id;
            }

            $criteria = new CDbCriteria;
            $criteria->addInCondition('question_id',$question_id);
            $criteria->order = "t.sortorder ASC";
            $QuestionSub = QuestionSub::model()->findAll($criteria);
            $sub_question_id = array();
            foreach ($QuestionSub as $key => $value) {
                $sub_question_id[] = $value->id;
            }
            
            $criteria=new CDbCriteria;
            if ($_POST['export']["Province"] != "all") {
                $criteria->compare('province',$_POST['export']["Province"]);
            }
            $criteria->compare('survey_id',$survey->id);
            $Questionnaire = Questionnaire::model()->findAll($criteria);

            $Questionnaire_id = array();
            foreach ($Questionnaire as $key => $value) {
                $Questionnaire_id[] = $value->questionnaire_id;
            }

            $criteria=new CDbCriteria;
            $criteria->select = "questionnaire_id";
            $criteria->addInCondition('questionnaire_id',$Questionnaire_id);
            $criteria->addInCondition('question_sub_id',$sub_question_id);
            $criteria->group= 'questionnaire_id';
            $SurveySubValue = SurveySubValue::model()->findAll($criteria);

            $Questionnaire_id = array();
            foreach ($SurveySubValue as $key => $value) {
                $Questionnaire_id[] = $value->questionnaire_id;
            }

            $criteria=new CDbCriteria;
            $criteria->addInCondition('questionnaire_id',$Questionnaire_id);
            $criteria->order = "t.province , t.district , t.subdistrict , t.village , t.group , t.sample ASC";
            $Questionnaire = Questionnaire::model()->findAll($criteria);

            if (empty($Questionnaire)) {
                $this->redirect(Yii::app()->baseurl.'/survey/export/'.$_POST['export']["id"]."?search=false");
            }
            $this->layout=false;
            $this->render('report_excel_12',array(
                'group'=>$group,
                'Questionnaire'=>$Questionnaire,
                'Question'=>$Question,
                'question_id'=>$question_id,
                'sub_question_id'=>$sub_question_id,
            ));
        }

        $criteria=new CDbCriteria;
        $criteria->addNotInCondition('type',['note','header','bottom-line','bullet','ques-num-value']);
        $criteria->compare('question_group_id',$_POST['export']["id"]);
        $Question = Question::model()->findAll($criteria);
        $question_id = array();
        foreach ($Question as $key => $value) {
            $question_id[] = $value->question_id;
        }

        $criteria=new CDbCriteria;
        if ($_POST['export']["Province"] != "all") {
            $criteria->compare('province',$_POST['export']["Province"]);
        }
        $criteria->compare('survey_id',$survey->id);
        $Questionnaire = Questionnaire::model()->findAll($criteria);

        $Questionnaire_id = array();
        foreach ($Questionnaire as $key => $value) {
            $Questionnaire_id[] = $value->questionnaire_id;
        }

        $criteria=new CDbCriteria;
        $criteria->select = "questionnaire_id";
        $criteria->addInCondition('questionnaire_id',$Questionnaire_id);
        $criteria->addInCondition('question_id',$question_id);
        $criteria->group= 'questionnaire_id';
        $SurveyValue = SurveyValue::model()->findAll($criteria);

        $Questionnaire_id = array();
        foreach ($SurveyValue as $key => $value) {
            $Questionnaire_id[] = $value->questionnaire_id;
        }

        $criteria=new CDbCriteria;
        $criteria->addInCondition('questionnaire_id',$Questionnaire_id);
        $criteria->order = "t.province , t.district , t.subdistrict , t.village , t.group , t.sample ASC";
        $Questionnaire = Questionnaire::model()->findAll($criteria);

        if (empty($Questionnaire) && $_POST['export']["id"] != 12) {
            $this->redirect(Yii::app()->baseurl.'/survey/export/'.$_POST['export']["id"]."?search=false");
        }

        $criteria = new CDbCriteria;
        $criteria->compare('survey',$survey->id);
        $criteria->addInCondition('question',$question_id);
        $criteria->order = "t.order ASC";
        $QuestionMap = SurveyMap::model()->findAll($criteria);

        $criteria = new CDbCriteria;
        $QuestionnaireQuestion = QuestionnaireQuestion::model()->findAll($criteria);

        $this->layout=false;
        if ($group["question_group_id"] == 7) {
            $criteria=new CDbCriteria;
            $criteria->addNotInCondition('type',['note','header','bottom-line','bullet','ques-num-value','sum-output-number','parent-ques-num','parent-ques-value']);
            $criteria->compare('question_group_id',$_POST['export']["id"]);
            $criteria->order = "t.readable_id ASC";
            $Question = Question::model()->findAll($criteria);
            $question_id = array();
            foreach ($Question as $key => $value) {
                $question_id[] = $value->question_id;
            }
            $this->render('report_excel_7',array(
                'group'=>$group,
                'Questionnaire'=>$Questionnaire,
                'Question'=>$Question,
                'question_id'=>$question_id,
                'QuestionnaireQuestion'=>$QuestionnaireQuestion,
                'QuestionMap'=>$QuestionMap,
            ));
        }elseif($group["question_group_id"] == 8) {
            $criteria=new CDbCriteria;
            $criteria->addNotInCondition('type',['note','header','bottom-line','bullet','ques-num-value','sum-output-number','parent-ques-num','parent-ques-value']);
            $criteria->compare('question_group_id',$_POST['export']["id"]);
            $criteria->order = "t.readable_id ASC";
            $Question = Question::model()->findAll($criteria);
            $question_id = array();
            foreach ($Question as $key => $value) {
                $question_id[] = $value->question_id;
            }
            $this->render('report_excel_8',array(
                'group'=>$group,
                'Questionnaire'=>$Questionnaire,
                'Question'=>$Question,
                'question_id'=>$question_id,
                'QuestionnaireQuestion'=>$QuestionnaireQuestion,
                'QuestionMap'=>$QuestionMap,
            ));
        }elseif($group["question_group_id"] == 10){
            $criteria=new CDbCriteria;
            $criteria->addNotInCondition('type',['note','header','bottom-line','bullet','sum-output-number','parent-ques-num','parent-ques-value']);
            $criteria->compare('question_group_id',$_POST['export']["id"]);
            $criteria->order = "t.readable_id ASC";
            $Question = Question::model()->findAll($criteria);
            $question_id = array();
            foreach ($Question as $key => $value) {
                $question_id[] = $value->question_id;
            }
            $this->render('report_excel_10',array(
                'group'=>$group,
                'Questionnaire'=>$Questionnaire,
                'Question'=>$Question,
                'question_id'=>$question_id,
                'QuestionnaireQuestion'=>$QuestionnaireQuestion,
                'QuestionMap'=>$QuestionMap,
            ));
        }else{
            $this->render('report_excel',array(
                'group'=>$group,
                'Questionnaire'=>$Questionnaire,
                'Question'=>$Question,
                'question_id'=>$question_id,
                'QuestionnaireQuestion'=>$QuestionnaireQuestion,
                'QuestionMap'=>$QuestionMap,
            ));

        }

    }

}

public function actionInsertExcel($id)
{
    $model = new User('import');
    $HisImportArr = array();
    $HisImportErrorArr = array();
    $HisImportAttrErrorArr = array();
    $HisImportErrorMessageArr = array();
    $HisImportUserPassArr = array();
    $data = array();
    $model->excel_file = CUploadedFile::getInstance($model,'excel_file');
    $phpExcelPath = Yii::getPathOfAlias('ext.phpexcel.Classes');
    include($phpExcelPath . DIRECTORY_SEPARATOR . 'PHPExcel.php');

    $model->excel_file = CUploadedFile::getInstance($model,'excel_file');

    $webroot = Yii::app()->basePath."/..";

    $filename = $webroot.'/uploads/' . $model->excel_file->name;
    $model->excel_file->saveAs($filename);

    $sheet_array = Yii::app()->yexcel->readActiveSheet($filename);
    $inputFileName = $filename;
    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objReader->setReadDataOnly(true);
    $objPHPExcel = $objReader->load($inputFileName);
    $objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
    $highestRow = $objWorksheet->getHighestRow();
    $highestColumn = $objWorksheet->getHighestColumn();

    $headingsArray = $objWorksheet->rangeToArray('A1:'.$highestColumn.'1',null, true, true, true);
    $headingsArray = $headingsArray[1];

    $r = -1;
    $namedDataArray = array();
    for ($row = 2; $row <= $highestRow; ++$row) {
        $dataRow = $objWorksheet->rangeToArray('A'.$row.':'.$highestColumn.$row,null, true, true, true);
        if ((isset($dataRow[$row]['A'])) && ($dataRow[$row]['A'] > '')) {
            ++$r;
            foreach($headingsArray as $columnKey => $columnHeading) {
                $namedDataArray[$r][$columnHeading] = $dataRow[$row][$columnKey];
            }
        }
    }

    $question_id = array();
    $criteria = new CDbCriteria;
    $criteria->select = "question_id";
    $criteria->addNotInCondition('type',['note','header','bottom-line','bullet','ques-num-value']);
    $criteria->compare('question_group_id',$id);
    $Question = Question::model()->findAll($criteria);

    if ($id == 7 || $id == 8) {
        $question_id = array();
        $criteria = new CDbCriteria;
        $criteria->addNotInCondition('type',['note','header','bottom-line','bullet','ques-num-value']);
        $criteria->compare('question_group_id',$id);
        $criteria->order = "readable_id ASC";
        $Question = Question::model()->findAll($criteria);
    }

    if ($id == 10) {
        $question_id = array();
        $criteria = new CDbCriteria;
        $criteria->addInCondition('type',['ques-ans','ques-num-value']);
        $criteria->compare('question_group_id',$id);
        $criteria->order = "readable_id ASC";
        $Question = Question::model()->findAll($criteria);
    }

    $criteria = new CDbCriteria;
    $criteria->order = "question_id ASC";
    $QuestionnaireQuestion = QuestionnaireQuestion::model()->findAll($criteria);
    foreach ($Question as $key => $value) {
        $question_id[] = $value->question_id;
    }

    $index = 0;
    $array_questionnaire_id = array();
    foreach($namedDataArray as $key => $result){
        $criteria=new CDbCriteria;
        $criteria->order = "id DESC";
        $survey = Survey::model()->find($criteria);

        $result["จังหวัด"] = str_replace("'","",$result["จังหวัด"]);
        $result["อำเภอ"] = str_replace("'","",$result["อำเภอ"]);
        $result["ตำบล"] = str_replace("'","",$result["ตำบล"]);
        $result["หมู่ที่"] = str_replace("'","",$result["หมู่ที่"]);

        $criteria=new CDbCriteria;
        $criteria->compare('survey_id',$survey->id);
        $criteria->compare('province',$result["จังหวัด"]);
        $criteria->compare('district',$result["อำเภอ"]);
        $criteria->compare('subdistrict',$result["ตำบล"]);
        $criteria->compare('village',$result["หมู่ที่"]);
        $criteria->compare('t.group',$result["พวกที่"]);
        $criteria->compare('sample',$result["ผู้ขึ้นทะเบียนเกษตรกร(ตัวอย่างที่)"]);
        $queryCheck = Questionnaire::model()->findAll($criteria);

        if (count($queryCheck) > 1) {
            foreach ($queryCheck as $key => $value) {
                if ($value->farmer_id == null) {
                    $value->delete();
                }
            }
        }


        $criteria=new CDbCriteria;
        $criteria->compare('survey_id',$survey->id);
        $criteria->compare('province',$result["จังหวัด"]);
        $criteria->compare('district',$result["อำเภอ"]);
        $criteria->compare('subdistrict',$result["ตำบล"]);
        $criteria->compare('village',$result["หมู่ที่"]);
        $criteria->compare('t.group',$result["พวกที่"]);
        $criteria->compare('sample',$result["ผู้ขึ้นทะเบียนเกษตรกร(ตัวอย่างที่)"]);
        $queryCheck = Questionnaire::model()->find($criteria);


        // $result["เจ้าหน้าที่สำรวจ(วัน/เดือน/ปี)"] = date("Y-m-d", strtotime($result["เจ้าหน้าที่สำรวจ(วัน/เดือน/ปี)"]));

        // $result["เจ้าหน้าที่สำรวจ(รหัส)"] = str_replace("'","",$result["เจ้าหน้าที่สำรวจ(รหัส)"]);

        // $result["จังหวัด"] = str_replace("'","",$result["จังหวัด"]);
        // $result["อำเภอ"] = str_replace("'","",$result["อำเภอ"]);
        // $result["ตำบล"] = str_replace("'","",$result["ตำบล"]);
        // $result["หมู่ที่"] = str_replace("'","",$result["หมู่ที่"]);

        // $result["ผู้ขึ้นทะเบียนเกษตรกร(เลขบัตรประจำตัวประชาชน)"] = str_replace("'","",$result["ผู้ขึ้นทะเบียนเกษตรกร(เลขบัตรประจำตัวประชาชน)"]);
        // $result["ชื่อผู้ให้ข้อมูล(เลขบัตรประจำตัวประชาชน)"] = str_replace("'","",$result["ชื่อผู้ให้ข้อมูล(เลขบัตรประจำตัวประชาชน)"]);
        // $result["ชื่อผู้ให้ข้อมูล(เบอร์ติดต่อ)"] = str_replace("'","",$result["ชื่อผู้ให้ข้อมูล(เบอร์ติดต่อ)"]);

        if (empty($queryCheck)) {
            $queryCheck = new Questionnaire();
            // $queryCheck->staff_name = $result["เจ้าหน้าที่สำรวจ(ชื่อ)"];
            // $queryCheck->staff_lname = $result["เจ้าหน้าที่สำรวจ(นามสกุล)"];
            // $queryCheck->staff_id = $result["เจ้าหน้าที่สำรวจ(รหัส)"];
            $queryCheck->survey_time = date("Y-m-d");
            $queryCheck->office_id = $result["สศท."];
            $queryCheck->province = $result["จังหวัด"];
            $queryCheck->district = $result["อำเภอ"];
            $queryCheck->subdistrict = $result["ตำบล"];
            $queryCheck->village = $result["หมู่ที่"];
            // $queryCheck->addressNumber = $result["บ้านเลขที่"];
            $queryCheck->group = $result["พวกที่"];
            // $queryCheck->farmer_name = $result["ผู้ขึ้นทะเบียนเกษตรกร(ชื่อ)"];
            // $queryCheck->farmer_lastname = $result["ผู้ขึ้นทะเบียนเกษตรกร(นามสกุล)"];
            $queryCheck->sample = $result["ผู้ขึ้นทะเบียนเกษตรกร(ตัวอย่างที่)"];
            // $queryCheck->farmer_id = $result["ผู้ขึ้นทะเบียนเกษตรกร(เลขบัตรประจำตัวประชาชน)"];
            // $queryCheck->farmer_registered = $result["ผู้ขึ้นทะเบียนเกษตรกร(ผู้ขึ้นทะเบียนเกษตร)"];
            // $queryCheck->informant_name = $result["ชื่อผู้ให้ข้อมูล(ชื่อ)"];
            // $queryCheck->informant_lastname = $result["ชื่อผู้ให้ข้อมูล(นามสกุล)"];
            // $queryCheck->informant_relation = $result["ชื่อผู้ให้ข้อมูล(ความสัมพันธ์กับหัวหน้าครัวเรือน)"];
            // $queryCheck->informant_id = $result["ชื่อผู้ให้ข้อมูล(เลขบัตรประจำตัวประชาชน)"];
            // $queryCheck->informant_tel = $result["ชื่อผู้ให้ข้อมูล(เบอร์ติดต่อ)"];
            // $queryCheck->inIrrigatedArea = $result["ครัวเรือนเกษตรมีที่ดินทำการเกษตรอย่างน้อย 1 แปลง อยู่ในเขตชลประทาน"];

            $queryCheck->survey_id = $survey->id;
            $queryCheck->save();
        }else{
            // $queryCheck->staff_name = $result["เจ้าหน้าที่สำรวจ(ชื่อ)"];
            // $queryCheck->staff_lname = $result["เจ้าหน้าที่สำรวจ(นามสกุล)"];
            // $queryCheck->staff_id = $result["เจ้าหน้าที่สำรวจ(รหัส)"];
            // $queryCheck->survey_time = $result["เจ้าหน้าที่สำรวจ(วัน/เดือน/ปี)"];
            // $queryCheck->office_id = $result["สศท."];
            // $queryCheck->addressNumber = $result["บ้านเลขที่"];
            // $queryCheck->farmer_name = $result["ผู้ขึ้นทะเบียนเกษตรกร(ชื่อ)"];
            // $queryCheck->farmer_lastname = $result["ผู้ขึ้นทะเบียนเกษตรกร(นามสกุล)"];
            // $queryCheck->farmer_id = $result["ผู้ขึ้นทะเบียนเกษตรกร(เลขบัตรประจำตัวประชาชน)"];
            // $queryCheck->farmer_registered = $result["ผู้ขึ้นทะเบียนเกษตรกร(ผู้ขึ้นทะเบียนเกษตร)"];
            // $queryCheck->informant_name = $result["ชื่อผู้ให้ข้อมูล(ชื่อ)"];
            // $queryCheck->informant_lastname = $result["ชื่อผู้ให้ข้อมูล(นามสกุล)"];
            // $queryCheck->informant_relation = $result["ชื่อผู้ให้ข้อมูล(ความสัมพันธ์กับหัวหน้าครัวเรือน)"];
            // $queryCheck->informant_id = $result["ชื่อผู้ให้ข้อมูล(เลขบัตรประจำตัวประชาชน)"];
            // $queryCheck->informant_tel = $result["ชื่อผู้ให้ข้อมูล(เบอร์ติดต่อ)"];
            // $queryCheck->inIrrigatedArea = $result["ครัวเรือนเกษตรมีที่ดินทำการเกษตรอย่างน้อย 1 แปลง อยู่ในเขตชลประทาน"];

            $queryCheck->survey_id = $survey->id;
            $queryCheck->save();
        }

            // Clear คำตอบเก่า
        if (!in_array($queryCheck->questionnaire_id, $array_questionnaire_id)) {
            $array_questionnaire_id[] = $queryCheck->questionnaire_id;
            $criteria=new CDbCriteria;
            $criteria->compare('questionnaire_id',$queryCheck->questionnaire_id);
            $criteria->addIncondition('question_id',$question_id);
            $SurveyValue = SurveyValue::model()->findAll($criteria);
            $survey_group = array();
            foreach ($SurveyValue as $key => $value) {
                $value->delete();
                if (!in_array($value->survey_group, $survey_group)) {
                    $survey_group[] = $value->survey_group;
                }
            }
            $criteria=new CDbCriteria;
            $criteria->addIncondition('survey_group',$survey_group);
            $SurveyGroup = SurveyGroup::model()->findAll($criteria);
            foreach ($SurveyGroup as $key => $value) {
                $value->delete();
            }
        }
            // Clear คำตอบเก่า

            // เพิ่มคำตอบ *คำถามแบบสอบถาม
        foreach ($QuestionnaireQuestion as $key => $value) {
            if (isset($result["QuestionnaireQuestion-".$value->question_id])) {
                $criteria=new CDbCriteria;
                $criteria->compare('question_id',$queryCheck->questionnaire_id);
                $criteria->compare('questionnaire_question_id',$value->question_id);
                $QuestionnaireQuestionValue = QuestionnaireQuestionValue::model()->find($criteria);
                if (!empty($QuestionnaireQuestionValue)) {
                    $QuestionnaireQuestionValue->value = $result["QuestionnaireQuestion-".$value->question_id];
                    $QuestionnaireQuestionValue->save();
                }else{
                    $QuestionnaireQuestionValue = new QuestionnaireQuestionValue();
                    $QuestionnaireQuestionValue->question_id = $queryCheck->questionnaire_id;
                    $QuestionnaireQuestionValue->questionnaire_question_id = $value->question_id;
                    $QuestionnaireQuestionValue->value = $result["QuestionnaireQuestion-".$value->question_id];
                    $QuestionnaireQuestionValue->save();
                }
            }
        }
            // เพิ่มคำตอบ *คำถามแบบสอบถาม


            // เพิ่มคำตอบใหม่ *คำถามหมวด
        $newGroup = new SurveyGroup();
        $newGroup->timestamp = date("Y-m-d h:i:s");
        $newGroup->save();


        if ($id == 7) {
            foreach ($Question as $key => $value) {
                if (isset($result["Code-6"]) && str_replace(" ","",$result["Code-6"]) == str_replace(" ","",$value->readable_id) ) {
                    $result["Code-6"]=str_replace(" ","",$result["Code-6"]);
                    $value->readable_id = str_replace(" ","",$value->readable_id);
                    $question_id = array();
                    $criteria = new CDbCriteria;
                    $criteria->addNotInCondition('type',['note','header','bottom-line','bullet','ques-num-value']);
                    $criteria->compare('question_group_id',$id);
                    $criteria->compare('readable_id',$value->readable_id);
                    $QuestionReadable = Question::model()->find($criteria);
                    if (!empty($QuestionReadable)) {
                     $modelValue = new SurveyValue();
                     $modelValue->questionnaire_id = $queryCheck->questionnaire_id;
                     $modelValue->question_id = $QuestionReadable->question_id;
                     $modelValue->value = $result["รายได้-6"];
                     $modelValue->survey_group = $newGroup->survey_group;
                     $modelValue->save();
                 }
             }
         }
     }elseif($id == 8) {
        foreach ($Question as $key => $value) {
            if (isset($result["Code-7"]) && str_replace(" ","",$result["Code-7"]) == str_replace(" ","",$value->readable_id) ) {
                $result["Code-7"]=str_replace(" ","",$result["Code-7"]);
                $value->readable_id = str_replace(" ","",$value->readable_id);
                $question_id = array();
                $criteria = new CDbCriteria;
                $criteria->addNotInCondition('type',['note','header','bottom-line','bullet','ques-num-value']);
                $criteria->compare('question_group_id',$id);
                $criteria->compare('readable_id',$value->readable_id);
                $QuestionReadable = Question::model()->find($criteria);
                if (!empty($QuestionReadable)) {
                 $modelValue = new SurveyValue();
                 $modelValue->questionnaire_id = $queryCheck->questionnaire_id;
                 $modelValue->question_id = $QuestionReadable->question_id;
                 $modelValue->value = $result["รายจ่าย-7"];
                 $modelValue->survey_group = $newGroup->survey_group;
                 $modelValue->save();
             }
         }
     }
 }elseif($id == 10) {
    foreach ($Question as $key => $value) {
        if (isset($result["Code-9"]) && str_replace(" ","",$result["Code-9"]) == str_replace(" ","",$value->readable_id) ) {
            $result["Code-9"]=str_replace(" ","",$result["Code-9"]);
            $value->readable_id = str_replace(" ","",$value->readable_id);
            $question_id = array();
            $criteria = new CDbCriteria;
            $criteria->addInCondition('type',['ques-ans','ques-num-value']);
            $criteria->compare('question_group_id',$id);
            $criteria->compare('readable_id',$value->readable_id);
            $QuestionReadable = Question::model()->find($criteria);
            if (!empty($QuestionReadable) && $QuestionReadable->type == "ques-ans") {
             $modelValue = new SurveyValue();
             $modelValue->questionnaire_id = $queryCheck->questionnaire_id;
             $modelValue->question_id = $QuestionReadable->question_id;
             $modelValue->value = $result["มูลค่า-9"];
             $modelValue->survey_group = $newGroup->survey_group;
             $modelValue->save();
         }elseif(!empty($QuestionReadable) && $QuestionReadable->type == "ques-num-value"){
            $criteria = new CDbCriteria;
            $criteria->addInCondition('type',['parent-ques-value','parent-ques-num']);
            $criteria->compare('options',$QuestionReadable->question_id);
            $QuestionReadable = Question::model()->findAll($criteria);
            foreach ($QuestionReadable as $keyQuestionReadable => $valueQuestionReadable) {
                if ($valueQuestionReadable->type == "parent-ques-num") {
                    $modelValue = new SurveyValue();
                    $modelValue->questionnaire_id = $queryCheck->questionnaire_id;
                    $modelValue->question_id = $valueQuestionReadable->question_id;
                    $modelValue->value = $result["จำนวน-9"];
                    $modelValue->survey_group = $newGroup->survey_group;
                    $modelValue->save();
                }elseif($valueQuestionReadable->type == "parent-ques-value"){
                    $modelValue = new SurveyValue();
                    $modelValue->questionnaire_id = $queryCheck->questionnaire_id;
                    $modelValue->question_id = $valueQuestionReadable->question_id;
                    $modelValue->value = $result["มูลค่า-9"];
                    $modelValue->survey_group = $newGroup->survey_group;
                    $modelValue->save();
                }
            }
        }
    }
}
}else{
    foreach ($Question as $key => $value) {
        if (isset($result["Question-".$value->question_id])) {
            $modelValue = new SurveyValue();
            $modelValue->questionnaire_id = $queryCheck->questionnaire_id;
            $modelValue->question_id = $value->question_id;
            $modelValue->value = $result["Question-".$value->question_id];
            $modelValue->survey_group = $newGroup->survey_group;
            $modelValue->save();
        }
    }
}
            // เพิ่มคำตอบใหม่ *คำถามหมวด


    }
    $this->redirect(Yii::app()->baseurl.'/survey/excel/'.$_GET['id']."?success=true");
    exit();
}

public function actionimport($id)
{
    $group = FormModel::getTitle($_GET['id'])[0];
    $criteria=new CDbCriteria;
    $criteria->compare('active',1);
    $criteria->order = "province_code ASC";
    $MtProvince = MtProvince::model()->findAll($criteria);

    $criteria=new CDbCriteria;
    $criteria->order = "id DESC";
    $survey = Survey::model()->find($criteria);

    $question_id = array();
    $criteria = new CDbCriteria;
        //$criteria->select = "question_id";
    $criteria->addNotInCondition('type',['note','header','bottom-line','bullet']);
    $criteria->compare('question_group_id',$group["question_group_id"]);
    $criteria->order = "sortorder ASC";
    $Question = Question::model()->findAll($criteria);

    foreach ($Question as $key => $value) {
        $question_id[] = $value->question_id;
    }

    $criteria = new CDbCriteria;
    $criteria->addInCondition('question_id',$question_id);
    $QuestionSub = QuestionSub::model()->findAll($criteria);

        // $criteria = new CDbCriteria;
        // $criteria->compare('survey',$survey->id);
        // $criteria->addInCondition('question',$question_id);
        // $criteria->order = "t.order ASC";
        // $QuestionMap = SurveyMap::model()->findAll($criteria);

    $criteria = new CDbCriteria;
    $QuestionnaireQuestion = QuestionnaireQuestion::model()->findAll($criteria);

    $model = new User('import');
    $this->render('import',array(
        "model"=>$model,
        'group'=>$group,
        'Question'=>$Question,
        'QuestionSub'=>$QuestionSub,
        'MtProvince'=>$MtProvince,
            //'QuestionMap'=>$QuestionMap,
        'QuestionnaireQuestion'=>$QuestionnaireQuestion,
    ));
}

public function actionImportExcel($id){
    $model = new User('import');
    $HisImportArr = array();
    $HisImportErrorArr = array();
    $HisImportAttrErrorArr = array();
    $HisImportErrorMessageArr = array();
    $HisImportUserPassArr = array();
    $data = array();
    $model->excel_file = CUploadedFile::getInstance($model,'excel_file');
    if (!empty($model->excel_file)) {

        $phpExcelPath = Yii::getPathOfAlias('ext.phpexcel.Classes');
        include($phpExcelPath . DIRECTORY_SEPARATOR . 'PHPExcel.php');

        $model->excel_file = CUploadedFile::getInstance($model,'excel_file');
        $webroot = Yii::app()->basePath."/..";

        $filename = $webroot.'/uploads/' . $model->excel_file->name;
        $model->excel_file->saveAs($filename);

        libxml_use_internal_errors(true);
        $sheet_array = Yii::app()->yexcel->readActiveSheet($filename);
        $inputFileName = $filename;
        $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objReader->setReadDataOnly(true);
        $objPHPExcel = $objReader->load($inputFileName);
        $objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
        $highestRow = $objWorksheet->getHighestRow();
        $highestColumn = $objWorksheet->getHighestColumn();

        $headingsArray = $objWorksheet->rangeToArray('A1:'.$highestColumn.'1',null, true, true, true);
        $headingsArray = $headingsArray[1];

        $r = -1;
        $namedDataArray = array();
        for ($row = 2; $row <= $highestRow; ++$row) {
            $dataRow = $objWorksheet->rangeToArray('A'.$row.':'.$highestColumn.$row,null, true, true, true);
            if ((isset($dataRow[$row]['A'])) && ($dataRow[$row]['A'] > '')) {
                ++$r;
                foreach($headingsArray as $columnKey => $columnHeading) {
                    $namedDataArray[$r][$columnHeading] = $dataRow[$row][$columnKey];
                }
            }
        }

        $question_id = array();
        $criteria = new CDbCriteria;
        $criteria->select = "question_id";
        $criteria->addNotInCondition('type',['note','header','bottom-line','bullet','ques-num-value']);
        $criteria->compare('question_group_id',$id);
        $Question = Question::model()->findAll($criteria);



        $criteria = new CDbCriteria;
        $criteria->order = "question_id ASC";
        $QuestionnaireQuestion = QuestionnaireQuestion::model()->findAll($criteria);

        foreach ($Question as $key => $value) {
            $question_id[] = $value->question_id;
        }

        $criteria = new CDbCriteria;
        $criteria->addInCondition('question_id',$question_id);
        $QuestionSub = QuestionSub::model()->findAll($criteria);

        $question_sub_id = array();
        foreach ($QuestionSub as $key => $value) {
            $question_sub_id[] = $value->id;
        }

        $index = 0;
        $array_questionnaire_id = array();
        foreach($namedDataArray as $key => $result){

            $criteria=new CDbCriteria;
            $criteria->order = "id DESC";
            $survey = Survey::model()->find($criteria);

            $result["จังหวัด"] = str_replace("'","",$result["จังหวัด"]);
            $result["อำเภอ"] = str_replace("'","",$result["อำเภอ"]);
            $result["ตำบล"] = str_replace("'","",$result["ตำบล"]);
            $result["หมู่ที่"] = str_replace("'","",$result["หมู่ที่"]);

            $criteria=new CDbCriteria;
            $criteria->compare('survey_id',$survey->id);
            $criteria->compare('province',$result["จังหวัด"]);
            $criteria->compare('district',$result["อำเภอ"]);
            $criteria->compare('subdistrict',$result["ตำบล"]);
            $criteria->compare('village',$result["หมู่ที่"]);
            $criteria->compare('t.group',$result["พวกที่"]);
            $criteria->compare('sample',$result["ผู้ขึ้นทะเบียนเกษตรกร(ตัวอย่างที่)"]);
            $queryCheck = Questionnaire::model()->findAll($criteria);


            if (count($queryCheck) > 1) {
                foreach ($queryCheck as $key => $value) {
                    if ($value->farmer_id == null) {
                        $value->delete();
                    }
                }
            }


            $criteria=new CDbCriteria;
            $criteria->compare('survey_id',$survey->id);
            $criteria->compare('province',$result["จังหวัด"]);
            $criteria->compare('district',$result["อำเภอ"]);
            $criteria->compare('subdistrict',$result["ตำบล"]);
            $criteria->compare('village',$result["หมู่ที่"]);
            $criteria->compare('t.group',$result["พวกที่"]);
            $criteria->compare('sample',$result["ผู้ขึ้นทะเบียนเกษตรกร(ตัวอย่างที่)"]);
            $queryCheck = Questionnaire::model()->find($criteria);

            // $result["เจ้าหน้าที่สำรวจ(วัน/เดือน/ปี)"] = date("Y-m-d", strtotime($result["เจ้าหน้าที่สำรวจ(วัน/เดือน/ปี)"]));

            // $result["เจ้าหน้าที่สำรวจ(รหัส)"] = str_replace("'","",$result["เจ้าหน้าที่สำรวจ(รหัส)"]);

            // $result["จังหวัด"] = str_replace("'","",$result["จังหวัด"]);
            // $result["อำเภอ"] = str_replace("'","",$result["อำเภอ"]);
            // $result["ตำบล"] = str_replace("'","",$result["ตำบล"]);
            // $result["หมู่ที่"] = str_replace("'","",$result["หมู่ที่"]);

            // $result["ผู้ขึ้นทะเบียนเกษตรกร(เลขบัตรประจำตัวประชาชน)"] = str_replace("'","",$result["ผู้ขึ้นทะเบียนเกษตรกร(เลขบัตรประจำตัวประชาชน)"]);
            // $result["ชื่อผู้ให้ข้อมูล(เลขบัตรประจำตัวประชาชน)"] = str_replace("'","",$result["ชื่อผู้ให้ข้อมูล(เลขบัตรประจำตัวประชาชน)"]);
            // $result["ชื่อผู้ให้ข้อมูล(เบอร์ติดต่อ)"] = str_replace("'","",$result["ชื่อผู้ให้ข้อมูล(เบอร์ติดต่อ)"]);

            if (empty($queryCheck)) {
                $queryCheck = new Questionnaire();
                // $queryCheck->staff_name = $result["เจ้าหน้าที่สำรวจ(ชื่อ)"];
                // $queryCheck->staff_lname = $result["เจ้าหน้าที่สำรวจ(นามสกุล)"];
                // $queryCheck->staff_id = $result["เจ้าหน้าที่สำรวจ(รหัส)"];
                $queryCheck->survey_time = date("Y-m-d");
                $queryCheck->office_id = $result["สศท."];
                $queryCheck->province = $result["จังหวัด"];
                $queryCheck->district = $result["อำเภอ"];
                $queryCheck->subdistrict = $result["ตำบล"];
                $queryCheck->village = $result["หมู่ที่"];
                // $queryCheck->addressNumber = $result["บ้านเลขที่"];
                $queryCheck->group = $result["พวกที่"];
                // $queryCheck->farmer_name = $result["ผู้ขึ้นทะเบียนเกษตรกร(ชื่อ)"];
                // $queryCheck->farmer_lastname = $result["ผู้ขึ้นทะเบียนเกษตรกร(นามสกุล)"];
                $queryCheck->sample = $result["ผู้ขึ้นทะเบียนเกษตรกร(ตัวอย่างที่)"];
                // $queryCheck->farmer_id = $result["ผู้ขึ้นทะเบียนเกษตรกร(เลขบัตรประจำตัวประชาชน)"];
                // $queryCheck->farmer_registered = $result["ผู้ขึ้นทะเบียนเกษตรกร(ผู้ขึ้นทะเบียนเกษตร)"];
                // $queryCheck->informant_name = $result["ชื่อผู้ให้ข้อมูล(ชื่อ)"];
                // $queryCheck->informant_lastname = $result["ชื่อผู้ให้ข้อมูล(นามสกุล)"];
                // $queryCheck->informant_relation = $result["ชื่อผู้ให้ข้อมูล(ความสัมพันธ์กับหัวหน้าครัวเรือน)"];
                // $queryCheck->informant_id = $result["ชื่อผู้ให้ข้อมูล(เลขบัตรประจำตัวประชาชน)"];
                // $queryCheck->informant_tel = $result["ชื่อผู้ให้ข้อมูล(เบอร์ติดต่อ)"];
                // $queryCheck->inIrrigatedArea = $result["ครัวเรือนเกษตรมีที่ดินทำการเกษตรอย่างน้อย 1 แปลง อยู่ในเขตชลประทาน"];

                $queryCheck->survey_id = $survey->id;
                $queryCheck->save();
            }else{
                // $queryCheck->staff_name = $result["เจ้าหน้าที่สำรวจ(ชื่อ)"];
                // $queryCheck->staff_lname = $result["เจ้าหน้าที่สำรวจ(นามสกุล)"];
                // $queryCheck->staff_id = $result["เจ้าหน้าที่สำรวจ(รหัส)"];
                // $queryCheck->survey_time = $result["เจ้าหน้าที่สำรวจ(วัน/เดือน/ปี)"];
                // $queryCheck->office_id = $result["สศท."];
                // $queryCheck->addressNumber = $result["บ้านเลขที่"];
                // $queryCheck->farmer_name = $result["ผู้ขึ้นทะเบียนเกษตรกร(ชื่อ)"];
                // $queryCheck->farmer_lastname = $result["ผู้ขึ้นทะเบียนเกษตรกร(นามสกุล)"];
                // $queryCheck->farmer_id = $result["ผู้ขึ้นทะเบียนเกษตรกร(เลขบัตรประจำตัวประชาชน)"];
                // $queryCheck->farmer_registered = $result["ผู้ขึ้นทะเบียนเกษตรกร(ผู้ขึ้นทะเบียนเกษตร)"];
                // $queryCheck->informant_name = $result["ชื่อผู้ให้ข้อมูล(ชื่อ)"];
                // $queryCheck->informant_lastname = $result["ชื่อผู้ให้ข้อมูล(นามสกุล)"];
                // $queryCheck->informant_relation = $result["ชื่อผู้ให้ข้อมูล(ความสัมพันธ์กับหัวหน้าครัวเรือน)"];
                // $queryCheck->informant_id = $result["ชื่อผู้ให้ข้อมูล(เลขบัตรประจำตัวประชาชน)"];
                // $queryCheck->informant_tel = $result["ชื่อผู้ให้ข้อมูล(เบอร์ติดต่อ)"];
                // $queryCheck->inIrrigatedArea = $result["ครัวเรือนเกษตรมีที่ดินทำการเกษตรอย่างน้อย 1 แปลง อยู่ในเขตชลประทาน"];

                // $queryCheck->survey_id = $survey->id;
                $queryCheck->save();
            }

            // foreach ($QuestionnaireQuestion as $key => $value) {
            //     if (isset($result["QuestionnaireQuestion-".$value->question_id])) {
            //         $criteria=new CDbCriteria;
            //         $criteria->compare('question_id',$queryCheck->questionnaire_id);
            //         $criteria->compare('questionnaire_question_id',$value->question_id);
            //         $QuestionnaireQuestionValue = QuestionnaireQuestionValue::model()->find($criteria);
            //         if (!empty($QuestionnaireQuestionValue)) {
            //             $QuestionnaireQuestionValue->value = $result["QuestionnaireQuestion-".$value->question_id];
            //             $QuestionnaireQuestionValue->save();
            //         }else{
            //             $QuestionnaireQuestionValue = new QuestionnaireQuestionValue();
            //             $QuestionnaireQuestionValue->question_id = $queryCheck->questionnaire_id;
            //             $QuestionnaireQuestionValue->questionnaire_question_id = $value->question_id;
            //             $QuestionnaireQuestionValue->value = $result["QuestionnaireQuestion-".$value->question_id];
            //             $QuestionnaireQuestionValue->save();
            //         }
            //     }
            // }
            // เพิ่มคำตอบ *คำถามแบบสอบถาม

            // Clear คำตอบเก่า
            if (!in_array($queryCheck->questionnaire_id, $array_questionnaire_id)) {
                $array_questionnaire_id[] = $queryCheck->questionnaire_id;
                $criteria=new CDbCriteria;
                $criteria->compare('questionnaire_id',$queryCheck->questionnaire_id);
                $criteria->addIncondition('question_sub_id',$question_sub_id);
                $SurveySubValue = SurveySubValue::model()->findAll($criteria);
                foreach ($SurveySubValue as $key => $value) {
                    $value->delete();
                }
            }
            // Clear คำตอบเก่า
            
            // เพิ่มคำตอบใหม่ *คำถามหมวด
            $newGroup = new SurveyGroup();
            $newGroup->timestamp = date("Y-m-d h:i:s");
            $newGroup->save();


            foreach ($Question as $key => $value) {
                $criteria = new CDbCriteria;
                $criteria->compare('question_id',$value->question_id);
                $QuestionSub = QuestionSub::model()->findAll($criteria);
                foreach ($QuestionSub as $keySub => $valueSub) {

                    if (isset($result["Question-".$value->question_id."/".$valueSub->id])) {
                   // var_dump($result["Question-".$value->question_id."/".$valueSub->id]);   
                        $modelValue = new SurveySubValue();
                        $modelValue->questionnaire_id = $queryCheck->questionnaire_id;
                        $modelValue->question_id = $value->question_id;
                        $modelValue->value = $result["Question-".$value->question_id."/".$valueSub->id];
                        $modelValue->question_sub_id = $valueSub->id;
                        $modelValue->save(false);
                    }
                }
            }//exit();
            // เพิ่มคำตอบใหม่ *คำถามหมวด
        }

    }
    $this->redirect(Yii::app()->baseurl.'/survey/import/'.$_GET['id']."?success=true");
    exit();
}

public function actionList(){
    $group = FormModel::getTitle($_GET['id'])[0];
    $criteria=new CDbCriteria;
    $criteria->compare('active',1);
    $criteria->order = "province_name_th ASC";
    $MtProvince = MtProvince::model()->findAll($criteria);

    $criteria = new CDbCriteria();
    $criteria->compare('active',1);
    $criteria->order = "those_id ASC";
    $Those = Those::model()->findAll($criteria);

    $Survey = Survey::model()->findAll();

    $this->render('index', Array(
        'title' => "หมวดที่ " . $group['question_group_number'] . " " . $group['question_group_name'],
        'question_id' => $group['question_group_id'],
        'MtProvince' => $MtProvince,
        'data' => FormModel::getData($_GET['id']),
        'Those' => $Those,
        'Survey'=> $Survey
    ));
}

public function actionSearchData()
{
    if (isset($_GET)) {

            // $year = isset($_GET['search']['year']) ? $_GET['search']['year'] : null;
            // $province = isset($_GET['search']['Province']) ? $_GET['search']['Province'] : null;
            // $district = isset($_GET['search']['District']) ? $_GET['search']['District'] : null;
            // $subdistrict = isset($_GET['search']['subdistrict']) ? $_GET['search']['subdistrict'] : null;
            // $village = isset($_GET['search']['village']) ? $_GET['search']['village'] : null;
            // $group = isset($_GET['search']['group']) ? $_GET['search']['group'] : null;
            // $sample = isset($_GET['search']['sample']) ? $_GET['search']['sample'] : null;
        $year = isset($_GET['year']) ? $_GET['year'] : null;
        $province = isset($_GET['province']) ? $_GET['province'] : null;
        $district = isset($_GET['district']) ? $_GET['district'] : null;
        $subdistrict = isset($_GET['subdistrict']) ? $_GET['subdistrict'] : null;
        $village = isset($_GET['village']) ? $_GET['village'] : null;
        $group = isset($_GET['group']) ? $_GET['group'] : null;
        $sample = isset($_GET['sample']) ? $_GET['sample'] : null;

            // $criteria=new CDbCriteria;
            // $criteria->compare('active',1);
            // $criteria->order = "province_name_th ASC";
            // $MtProvince = MtProvince::model()->findAll($criteria);

            // $criteria=new CDbCriteria;
            // if(isset($_GET["search"]["Province"])){

            //     $criterianew=new CDbCriteria;
            //     $criterianew->compare('active',1);
            //     $criterianew->compare('province_code',$_GET["search"]["Province"]);
            //     $MtProvince_search = MtProvince::model()->find($criterianew); 

            //     $criteria->compare('province_id',$MtProvince_search->id);
            // }

            // $criteria->compare('active',1);
            // $MtDistrict = MtDistrict::model()->findAll($criteria); 

            // $criteria=new CDbCriteria;
            // $criteria->compare('active',1);
            // if (isset($_GET["search"]["District"])) {

            //     $criterianew=new CDbCriteria;
            //     $criterianew->compare('active',1);
            //     $criterianew->compare('province_id',$MtProvince_search->id);
            //     $criterianew->compare('district_code',$_GET["search"]["District"]);
            //     $MtDistrict_search = MtDistrict::model()->find($criterianew); 

            //     $criteria->compare('district_id',$MtDistrict_search->id);
            // }

            // $MtSubDistrict = MtSubDistrict::model()->findAll($criteria);

            // $criteria=new CDbCriteria;
            // $criteria->compare('active',1);
            // if (isset($_GET["search"]["subdistrict"])) {

            //     $criterianew=new CDbCriteria;
            //     $criterianew->compare('active',1);
            //     $criterianew->compare('district_id',$MtDistrict_search->id);
            //     $criterianew->compare('subdistrict_code',$_GET["search"]["subdistrict"]);
            //     $MtSubDistrict_search = MtSubDistrict::model()->find($criterianew); 

            //     $criteria->compare('subdistrict_id',$MtSubDistrict_search->id);
            // }

            // $masterVillage = masterVillage::model()->findAll($criteria); 

        $data = FormModel::listSearch($_GET['id'],$year,$province,$district,$subdistrict,$village,$group,$sample);
        $group = FormModel::getTitle($_GET['id'])[0];

        $Survey = Survey::model()->findAll();

        $criteria = new CDbCriteria();
        $criteria->compare('active',1);
        $criteria->order = "those_id ASC";
        $Those = Those::model()->findAll($criteria);

            // $this->render('index', Array(
            //     // 'years' => QuestionnaireModel::getYears(),
            //     'MtProvince'=>$MtProvince,
            //     'MtDistrict'=>$MtDistrict,
            //     'MtSubDistrict'=>$MtSubDistrict,
            //     'masterVillage'=>$masterVillage,
            //     'title' => "หมวดที่ " . $group['question_group_number'] . " " . $group['question_group_name'],
            //     'question_id' => $group['question_group_id'],
            //     'data' => $data,
            //     'Those' => $Those,
            //     'Survey'=> $Survey
            // ));
        $this->renderPartial('search', Array(
            'data' => $data,

        ));
    }
}

public function actionForm(){
    $group = FormModel::getTitle($_GET['id'])[0];
    $this->render('create', Array(
        'title' => "หมวดที่ " . $group['question_group_number'] . " " . $group['question_group_name'],
        'question_id' => $group['question_group_id'],
        'data' => Array(
            Array()
        ),
        'answer' => FormModel::getAnswer($_GET['id'])
    ));
}

public function actionInsert(){
    $group = FormModel::getTitle($_GET['id'])[0];
    $questionnaire = FormModel::insert($_POST);
    if (isset($_POST["checkresend"]) && $_POST["checkresend"]=="on") {
        $this->redirect(Yii::app()->baseurl.'/survey/'.$_GET['id']."/create?Questionnaire=".$questionnaire["questionnaire_id"]."&return=".$_POST["return"]."&checkresend=true");
    }
    if (isset($_POST["return"]) && $_POST["return"]=="true") {
        $this->redirect(Yii::app()->baseurl.'/questionnaire/');
    }
    $this->redirect(Yii::app()->baseurl.'/survey/'.$_GET['id']);
}

public function actionDelete(){
    FormModel::delete($_POST['key']);
    $this->redirect(Yii::app()->baseurl.'/survey/'.$_GET['id']);
}

public function actionEdit(){
    $group = FormModel::getTitle($_GET['id'])[0];
    $this->render('edit', Array(
        'title' => "หมวดที่ " . $group['question_group_number'] . " " . $group['question_group_name'],
        'question_id' => $group['question_group_id'],
        'data' => Array(
            Array()
        ),
        'answer' => FormModel::getAnswer($_GET['id']),
        'data' => FormModel::getSurvey($_GET['survey_id'])
    ));
}

public function actionUpdate(){
    $questionnaire = FormModel::update($_GET['survey_id'], $_POST);
    $this->redirect(Yii::app()->baseurl.'/survey/'.$_GET['id']);
}

public function actionSearchQuestionnaire(){
    if (isset($_POST)) {
        $model=Questionnaire::model()->findByPk((int)$_POST['id']);
        if (!empty($model)) {
            foreach ($model as $key) {
                $value[] = $key;
            }
            echo json_encode($value);
        }
    }
}

public function actionListRate(){

    $SurveySubValue = SurveySubValue::model()->findAll();
    $Array_questionnaire_id = Array();
    foreach ($SurveySubValue as $key => $value) {
        array_push($Array_questionnaire_id, $value->questionnaire_id);
    } 
    $questionnaire_id = array_unique($Array_questionnaire_id);

    $criteria = new CDbCriteria();
    $User = User::model()->findByPk(Yii::app()->user->id);
    $profile = $User->profile;
    if ($User->superuser != 1) {
        $criteria->compare('office_id',$profile->county_id);
    }
    $criteria->addIncondition('questionnaire_id',$questionnaire_id);
    $Questionnaire = Questionnaire::model()->findAll($criteria);


    $QuestionGroup = QuestionGroup::model()->findByPk((int)$_GET['id']);

    $criteria = new CDbCriteria();
    $criteria->compare('active',1);
    $criteria->order = "those_id ASC";
    $Those = Those::model()->findAll($criteria);

    $criteria=new CDbCriteria;
    $criteria->compare('active',1);
    $criteria->order = "province_name_th ASC";
    $MtProvince = MtProvince::model()->findAll($criteria);

    $Survey = Survey::model()->findAll();

    $this->render('rate/index', Array(
        'title' => "หมวดที่ " . $QuestionGroup->question_group_number . " " . $QuestionGroup->question_group_name,
            // 'question_id' => $group['question_group_id'],
        'MtProvince' => $MtProvince,
            //'data' => FormModel::getData($_GET['id']),
        'data' => $Questionnaire,
        'Those' => $Those,
        'Survey'=> $Survey
    ));
}

public function actionFormRate(){
    if (isset($_GET['id'])) {

        $group = FormModel::getTitle($_GET['id'])[0];
        $this->render('rate/create', Array(
            'title' => "หมวดที่ " . $group['question_group_number'] . " " . $group['question_group_name'],
            'question_id' => $group['question_group_id'],
            // 'data' => Array(
            //     Array()
            // ),
            'answer' => FormModel::getAnswer($_GET['id'])
        ));
    }
}

public function actionInsertRate()
{
    if (isset($_GET) && isset($_POST)) {
        foreach ($_POST as $key => $value) {
            $exp_key = explode('-', $key);
            if($exp_key[0] == 'QuestionSub'){

                $QuestionSub = QuestionSub::model()->findByPk((int)$exp_key[1]);

                $criteria = new CDbCriteria();
                $criteria->compare('questionnaire_id',$_GET["questionnaire_id"]);
                $criteria->compare('question_sub_id',$exp_key[1]);
                $criteria->compare('question_id',$QuestionSub->question_id);
                $SurveySubValue = SurveySubValue::model()->find($criteria);

                if (!$SurveySubValue) {
                    $model = new SurveySubValue;
                    $model->questionnaire_id = $_GET["questionnaire_id"];
                    $model->question_id = $QuestionSub->question_id;
                    $model->question_sub_id = $exp_key[1];
                    $model->value = $value;
                    $model->save();
                }
            } 
        }
    }
    $QuestionGroup = QuestionGroup::model()->findByPk((int)$_GET['id']);

    $criteria = new CDbCriteria();
    $criteria->compare('active',1);
    $criteria->order = "those_id ASC";
    $Those = Those::model()->findAll($criteria);

    $SurveySubValue = SurveySubValue::model()->findAll();
    $Array_questionnaire_id = Array();
    foreach ($SurveySubValue as $key => $value) {
        array_push($Array_questionnaire_id, $value->questionnaire_id);
    } 
    $questionnaire_id = array_unique($Array_questionnaire_id);

    $criteria = new CDbCriteria();
    $User = User::model()->findByPk(Yii::app()->user->id);
    $profile = $User->profile;
    if ($User->superuser != 1) {
        $criteria->compare('office_id',$profile->county_id);
    }
    $criteria->addIncondition('questionnaire_id',$questionnaire_id);
    $Questionnaire = Questionnaire::model()->findAll($criteria);

    $criteria=new CDbCriteria;
    $criteria->compare('active',1);
    $criteria->order = "province_name_th ASC";
    $MtProvince = MtProvince::model()->findAll($criteria);

    $Survey = Survey::model()->findAll();

    $this->render('rate/index', Array(
        'title' => "หมวดที่ " . $QuestionGroup->question_group_number . " " . $QuestionGroup->question_group_name,
        'data' => $Questionnaire,
        'Those' => $Those,
        'MtProvince' => $MtProvince,
        'Survey'=> $Survey
    ));
}

public function actionEditRate()
{
    if (isset($_GET['id'])) {

        $group = FormModel::getTitle($_GET['id'])[0];
        $this->render('rate/edit', Array(
            'title' => "หมวดที่ " . $group['question_group_number'] . " " . $group['question_group_name'],
            'question_id' => $group['question_group_id'],

            'answer' => FormModel::getAnswer($_GET['id'])
        ));
    }
}

public function actionUpdateRate()
{
    if (isset($_GET) && isset($_POST)) {
        foreach ($_POST as $key => $value) {
            $exp_key = explode('-', $key);
            if($exp_key[0] == 'QuestionSub'){

                $QuestionSub = QuestionSub::model()->findByPk((int)$exp_key[1]);

                $criteria = new CDbCriteria();
                $criteria->compare('questionnaire_id',$_GET["questionnaire_id"]);
                $criteria->compare('question_sub_id',$exp_key[1]);
                $criteria->compare('question_id',$QuestionSub->question_id);
                $SurveySubValue = SurveySubValue::model()->find($criteria);

                if ($SurveySubValue) {

                    $SurveySubValue->value = $value;
                    $SurveySubValue->save();

                }else{

                    $model = new SurveySubValue;
                    $model->questionnaire_id = $_GET["questionnaire_id"];
                    $model->question_id = $QuestionSub->question_id;
                    $model->question_sub_id = $exp_key[1];
                    $model->value = $value;
                    $model->save();
                } 
            } 
            }//exit();
        }
        $QuestionGroup = QuestionGroup::model()->findByPk((int)$_GET['id']);

        $criteria = new CDbCriteria();
        $criteria->compare('active',1);
        $criteria->order = "those_id ASC";
        $Those = Those::model()->findAll($criteria);

        $SurveySubValue = SurveySubValue::model()->findAll();
        $Array_questionnaire_id = Array();
        foreach ($SurveySubValue as $key => $value) {
            array_push($Array_questionnaire_id, $value->questionnaire_id);
        } 
        $questionnaire_id = array_unique($Array_questionnaire_id);

        $criteria = new CDbCriteria();
        $User = User::model()->findByPk(Yii::app()->user->id);
        $profile = $User->profile;
        if ($User->superuser != 1) {
            $criteria->compare('office_id',$profile->county_id);
        }
        $criteria->addIncondition('questionnaire_id',$questionnaire_id);
        $Questionnaire = Questionnaire::model()->findAll($criteria);

        $criteria=new CDbCriteria;
        $criteria->compare('active',1);
        $criteria->order = "province_name_th ASC";
        $MtProvince = MtProvince::model()->findAll($criteria);

        $Survey = Survey::model()->findAll();

        $this->render('rate/index', Array(
            'title' => "หมวดที่ " . $QuestionGroup->question_group_number . " " . $QuestionGroup->question_group_name,
            'Those' => $Those,
            'data' => $Questionnaire,
            'MtProvince' => $MtProvince,
            'Survey'=> $Survey
        ));
    }

    public function actionDeleteRate()
    {
        if (isset($_GET["questionnaire_id"])) {

            $criteria = new CDbCriteria();
            $criteria->compare('questionnaire_id',$_GET["questionnaire_id"]);
            SurveySubValue::model()->deleteAll($criteria);
        }
        
        $QuestionGroup = QuestionGroup::model()->findByPk((int)$_GET['id']);

        $criteria = new CDbCriteria();
        $criteria->compare('active',1);
        $criteria->order = "those_id ASC";
        $Those = Those::model()->findAll($criteria);

        $SurveySubValue = SurveySubValue::model()->findAll();
        $Array_questionnaire_id = Array();
        foreach ($SurveySubValue as $key => $value) {
            array_push($Array_questionnaire_id, $value->questionnaire_id);
        } 
        $questionnaire_id = array_unique($Array_questionnaire_id);

        $criteria = new CDbCriteria();
        $User = User::model()->findByPk(Yii::app()->user->id);
        $profile = $User->profile;
        if ($User->superuser != 1) {
            $criteria->compare('office_id',$profile->county_id);
        }
        $criteria->addIncondition('questionnaire_id',$questionnaire_id);
        $Questionnaire = Questionnaire::model()->findAll($criteria);

        $criteria=new CDbCriteria;
        $criteria->compare('active',1);
        $criteria->order = "province_name_th ASC";
        $MtProvince = MtProvince::model()->findAll($criteria);

        $Survey = Survey::model()->findAll();

        $this->render('rate/index', Array(
            'title' => "หมวดที่ " . $QuestionGroup->question_group_number . " " . $QuestionGroup->question_group_name,
            'Those' => $Those,
            'data' => $Questionnaire,
            'MtProvince' => $MtProvince,
            'Survey'=> $Survey
        ));
    }

    public function actionSearchRateData()
    {
        if (isset($_GET)) {

            $year = isset($_GET['year']) ? $_GET['year'] : null;
            $province = isset($_GET['province']) ? $_GET['province'] : null;
            $district = isset($_GET['district']) ? $_GET['district'] : null;
            $subdistrict = isset($_GET['subdistrict']) ? $_GET['subdistrict'] : null;
            $village = isset($_GET['village']) ? $_GET['village'] : null;
            $group = isset($_GET['group']) ? $_GET['group'] : null;
            $sample = isset($_GET['sample']) ? $_GET['sample'] : null;
            
            $criteria = new CDbCriteria();
            if ($year != null) {
             $criteria->compare('survey_id',$year);
         }
         if ($province != null) {
             $criteria->compare('province',$province);
         }
         if ($district != null) {
             $criteria->compare('district',$district);
         }
         if ($subdistrict != null) {
             $criteria->compare('subdistrict',$subdistrict);
         }
         if ($village != null) {
             $criteria->compare('village',$village);
         }
         if ($group != null) {
             $criteria->compare('group',$group);
         }
         if ($sample != null) {
             $criteria->compare('sample',$sample);
         }

         $Questionnaire = Questionnaire::model()->findAll($criteria);

         $this->renderPartial('rate/search', Array(
            'data' => $Questionnaire,

        ));
     }
 }

 public function actionSearchPlant()
 { 
    $name_Plant = "";
    $code = $_POST['code'];
    if ($code != null) {
        $criteria=new CDbCriteria;
        $criteria->compare('pa_code',$code);
        $MasterPlant = MasterPlant::model()->find($criteria);

        if ($MasterPlant) {
            $name_Plant = $MasterPlant->pa_name;
        }
    }
    echo $name_Plant;
}

}