<?php

class ProcessingController extends Controller
{

	public $layout='admin';

	private $_model;
	/**
	 * Declares class-based actions.
	 */

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{

		$this->render('index');
	}

	public function actionexcel()
	{
		$model = new User('import');
		$this->render('excel',array(
			"model"=>$model,
		));
	}

	public function actionInsertExcel()
	{
		$model = new User('import');
		$HisImportArr = array();
		$HisImportErrorArr = array();
		$HisImportAttrErrorArr = array();
		$HisImportErrorMessageArr = array();
		$HisImportUserPassArr = array();
		$data = array();
		$model->excel_file = CUploadedFile::getInstance($model,'excel_file');
		$phpExcelPath = Yii::getPathOfAlias('ext.phpexcel.Classes');
		include($phpExcelPath . DIRECTORY_SEPARATOR . 'PHPExcel.php');

		$model->excel_file = CUploadedFile::getInstance($model,'excel_file');

		$webroot = Yii::app()->basePath."/..";

		$filename = $webroot.'/uploads/' . $model->excel_file->name;
		$model->excel_file->saveAs($filename);

		$sheet_array = Yii::app()->yexcel->readActiveSheet($filename);
		$inputFileName = $filename;
		$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
		$objReader = PHPExcel_IOFactory::createReader($inputFileType);
		$objReader->setReadDataOnly(true);
		$objPHPExcel = $objReader->load($inputFileName);
		$objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
		$highestRow = $objWorksheet->getHighestRow();
		$highestColumn = $objWorksheet->getHighestColumn();

		$headingsArray = $objWorksheet->rangeToArray('A1:'.$highestColumn.'1',null, true, true, true);
		$headingsArray = $headingsArray[1];

		$r = -1;
		$namedDataArray = array();
		for ($row = 2; $row <= $highestRow; ++$row) {
			$dataRow = $objWorksheet->rangeToArray('A'.$row.':'.$highestColumn.$row,null, true, true, true);
			if ((isset($dataRow[$row]['A'])) && ($dataRow[$row]['A'] > '')) {
				++$r;
				foreach($headingsArray as $columnKey => $columnHeading) {
					$namedDataArray[$r][$columnHeading] = $dataRow[$row][$columnKey];
				}
			}
		}

		$index = 0;

		foreach($namedDataArray as $key => $result){
			$MtProvince = MtProvince::model()->find(array(
				'condition' => 'active=:active AND province_code=:province_code',
				'params' => array(':active' => '1',':province_code' => $result["รหัสจังหวัด"])
			));
			if (!empty($MtProvince)) {
				$criteria = new CDbCriteria();
				$criteria->compare('province',$MtProvince->id);
				$criteria->compare('t.group',$result["พวกที่"]);
				$criteria->compare('active',1);
				$Processing = Processing::model()->find($criteria);
				if (empty($Processing)) {
					$Processing = new Processing;
					$Processing->province = $MtProvince->id;
					$Processing->year = 50;
					$Processing->group = $result["พวกที่"];
					$Processing->total = $result['N'];
					$Processing->random = $result['n'];
					$Processing->survey = $result['n'];
					$Processing->save();
				}
				if (!empty($Processing)) {
					$MtDistrict = MtDistrict::model()->find(array(
						'condition' => 'active=:active AND district_code=:district_code AND province_id=:province_id',
						'params' => array(':active' => '1',':district_code' => $result["รหัสอำเภอ"],':province_id' => $MtProvince->id)
					));
					if (!empty($MtDistrict)) {
						$MtSubDistrict = MtSubDistrict::model()->find(array(
							'condition' => 'active=:active AND subdistrict_code=:subdistrict_code AND district_id=:district_id',
							'params' => array(':active' => '1',':subdistrict_code' => $result["รหัสตำบล"],':district_id' => $MtDistrict->id)
						));
						if (!empty($MtSubDistrict)) {
							$masterVillage = masterVillage::model()->find(array(
								'condition' => 'active=:active AND village_code=:village_code AND subdistrict_id=:subdistrict_id',
								'params' => array(':active' => '1',':village_code' => $result["รหัสหมู่บ้าน"],':subdistrict_id' => $MtSubDistrict->id)
							));
							if (!empty($masterVillage)) {
								$criteria = new CDbCriteria();
								$criteria->compare('processing_id',$Processing->id);
								$criteria->compare('district',$MtDistrict->id);
								$criteria->compare('subdistrict',$MtSubDistrict->id);
								$criteria->compare('village',$masterVillage->village_id);
								$criteria->compare('active',1);
								$CheckSubProcessing = SubProcessing::model()->find($criteria);
								if (empty($CheckSubProcessing)) {
									$SubProcessing = new SubProcessing;
									$SubProcessing->processing_id = $Processing->id;
									$SubProcessing->district = $MtDistrict->id;
									$SubProcessing->subdistrict = $MtSubDistrict->id;
									$SubProcessing->village = $masterVillage->village_id;
									$SubProcessing->total = $result['M'];
									$SubProcessing->random = $result['m'];
									$SubProcessing->survey = $result['m'];
									$SubProcessing->save();
								}
							}
						}
					}
				}
			}
		}
		$this->redirect('excel');
		exit();
	}

	public function actionexport()
	{

		$criteria=new CDbCriteria;
		$criteria->order = "id DESC";
		$survey = Survey::model()->findAll($criteria);


		$this->render('export',array(
			"survey"=>$survey,
		));
	}

	public function actionExportExcel()
	{
		$this->layout=false;
		$criteria = new CDbCriteria();
		$criteria->compare('active',1);
		if (isset($_POST["export"]["survey"])) {
			$criteria->compare('year',$_POST["export"]["survey"]);
		}
		$criteria->order = "province ASC";
		$Processing = Processing::model()->findAll($criteria);

		if (empty($Processing)) {
            $this->redirect(Yii::app()->baseurl.'/processing/export/?search=false');
        }


		$this->render('report_excel',array(
			"Processing"=>$Processing,
		));
		exit();
	}

	public function actionSearch()
	{
		if(!empty($_GET)){
			
			$criteria = new CDbCriteria();
			if ($_GET['Survey'] != "") {
				$criteria->compare('year',$_GET['Survey']);
			}
			if ($_GET['Those'] != "") {
				$criteria->compare('t.group',$_GET['Those']);
			}
			$criteria->compare('active',1);
			$criteria->order = "created_at DESC";
			$Processing = Processing::model()->findAll($criteria);

			$this->renderPartial('index_search', array(
				'model'=>$Processing,
			));
		}
	}

	public function actionLoadData()
	{
		if ($_POST['action'] = 'add') {
			$Survey = Survey::model()->findAll();

			$criteria = new CDbCriteria();
			$criteria->compare('active',1);
			$criteria->order = "those_id ASC";
			$Those = Those::model()->findAll($criteria);

			$criteria = new CDbCriteria();
			$criteria->compare('active',1);
			$criteria->order = "province_name_th ASC";
			$Province = Province::model()->findAll($criteria);

			$respon = '';
			$respon = $this->renderPartial('create', array('Survey' => $Survey, 'Those' => $Those , 'Province' => $Province));
		}
	}

	public function actionLoadSubData()
	{
		if ($_POST['action'] = 'add') {
			// $Survey = Survey::model()->findAll();

			// $criteria = new CDbCriteria();
   //          $criteria->compare('active',1);
   //          $criteria->order = "those_id ASC";
   //          $Those = Those::model()->findAll($criteria);

   //          $criteria = new CDbCriteria();
   //          $criteria->compare('active',1);
   //          $criteria->order = "province_name_th ASC";
   //          $Province = Province::model()->findAll($criteria);
			$id_prcessing = isset($_POST['id_prcessing']) ? $_POST['id_prcessing'] : '';        

			$respon = '';
			$respon = $this->renderPartial('createsub', array('id_prcessing' => $id_prcessing));
		}
	}

	public function actionSaveData()
	{
		$response = array();

		if (!empty($_POST)) {   
			$model = Processing::model()->findByPk((int)$_POST['id']);

			if ($model) {

				$model->province = $_POST['province'];
				$model->year = $_POST['year'];
				$model->group = $_POST['group'];
				$model->total = $_POST['total'];
				$model->random = $_POST['random'];
				$model->survey = $_POST['survey'];

				if ($model->save()) {
					$response = [ 
						'success' => true,
						'action'=>'edit',
						'message' => 'SAVE DATA SUCCESSFUL'
					];
				}else{
					$response = [ 
						'success' => false,
						'message' => 'SAVE DATA ERROR'
					];
				}
			}else{

				$model = new Processing;
				$model->province = $_POST['province'];
				$model->year = $_POST['year'];
				$model->group = $_POST['group'];
				$model->total = $_POST['total'];
				$model->random = $_POST['random'];
				$model->survey = $_POST['survey'];

				if ($model->save()) {
					$response = [ 
						'success' => true,
						'action'=>'add',
						'message' => 'SAVE DATA SUCCESSFUL'
					];			
				}else{
					$response = [ 
						'success' => false,
						'message' => 'SAVE DATA ERROR'
					];
				}
			}
			echo json_encode($response); 
		}else{
			$response = [ 
				'success' => false,
				'message' => 'SAVE DATA ERROR'
			];
			echo json_encode($response); 
		}
	}

	public function actionSubProcessing()
	{
		$id = $_POST['id_data'];

		if ($id != "") {
			$criteria = new CDbCriteria();
			$criteria->compare('processing_id',(int)$id);
			$criteria->compare('active',1);
			$SubProcessing = SubProcessing::model()->findAll($criteria);

			$respon = '';
			if (!empty($SubProcessing)) {

				$respon = $this->renderPartial('sub_processing',array('model_SubProcessing' => $SubProcessing));
			}else{

				$respon = $this->renderPartial('sub_processing',array('model_SubProcessing' => $SubProcessing));
			}
			echo $respon;

		}
	}

	public function actionCheckSubDistrict()
	{
		if ($_POST['id_District'] != "") {
			$criteria=new CDbCriteria;
			$criteria->compare('district_id',$_POST['id_District']);
			$criteria->compare('active',1);
			$model = SubDistrict::model()->findAll($criteria);

			if (!empty($model)) {

				$data = '<option value ="">เลือกตำบล</option>';
				foreach ($model as $key => $value) {
					$data .= '<option value = "'.$value->id.'"'.'>'.$value->subdistrict_code.' -> '.$value->subdistrict_name_th.'</option>';
				}
				echo ($data);
			}else{
				$data = '<option value ="">ไม่พบข้อมูล</option>';
				echo ($data);
			}
		}        

	}

	public function actionCheckVillage()
	{
		if ($_POST['id_Village'] != "") {
			$criteria=new CDbCriteria;
			$criteria->compare('subdistrict_id',$_POST['id_Village']);
			$criteria->compare('active',1);
			$model = masterVillage::model()->findAll($criteria);

			if (!empty($model)) {

				$data = '<option value ="">เลือกหมู่บ้าน</option>';
				foreach ($model as $key => $value) {
					$data .= '<option value = "'.$value->village_id.'"'.'>'.$value->village_code.' -> '.$value->village_name.'</option>';
				}
				echo ($data);
			}else{
				$data = '<option value ="">ไม่พบข้อมูล</option>';
				echo ($data);
			}
		}       
	}

	public function actionCheckGroup()
	{

		if (!empty($_POST)) { 
			$criteria = new CDbCriteria();
			if ($_POST['id_group'] != "") {
				$criteria->compare('t.group',$_POST['id_group']);
			}
			$criteria->compare('active',1);
			$Processing = Processing::model()->findAll($criteria);

			$array_province = array();

			foreach ($Processing as $key => $value) {
				$array_province[] = $value->province;
			}

			$criteria = new CDbCriteria();
			$criteria->compare('active',1);
			$criteria->order = "province_name_th ASC";
			$Province = Province::model()->findAll($criteria);

			$data = '<option value ="">เลือกจังหวัด</option>';
			foreach ($Province as $key => $value) {
				if (in_array($value->id, $array_province)) {
					$data .= '<option value = "'.$value->id.'"'.' disabled >'.$value->province_code.' -> '.$value->province_name_th.'</option>';
				}else{
					$data .= '<option value = "'.$value->id.'"'.'>'.$value->province_code.' -> '.$value->province_name_th.'</option>';
				}
			}
			echo($data);

		}
	}

	public function actionSaveSubData()
	{
		$return = true;
		if (!empty($_POST)) {    

			$SubProcessing = SubProcessing::model()->findByPk((int)$_POST['id']);

			if ($SubProcessing) {
				$SubProcessing->district = $_POST['district'];
				$SubProcessing->subdistrict = $_POST['subdistrict'];
				$SubProcessing->village = $_POST['village'];
				$SubProcessing->total = $_POST['total'];
				$SubProcessing->random = $_POST['random'];
				$SubProcessing->survey = $_POST['survey'];

				if ($SubProcessing->save()) {
					$return = true;
				}else{
					$return = false;
				}
			}else{
				$model = new SubProcessing;
				$model->processing_id = $_POST['processing_id'];
				$model->district = $_POST['district'];
				$model->subdistrict = $_POST['subdistrict'];
				$model->village = $_POST['village'];
				$model->total = $_POST['total'];
				$model->random = $_POST['random'];
				$model->survey = $_POST['survey'];

				if ($model->save()) {
					$return = true;
				}else{
					$return = false;
				}
			}
			echo $return;
		}else{
			$return = false;
			echo $return;
		}
	}

	public function actionUpdateProcessing()
	{
		$model = $this->loadModel($_POST['id_Processing']);
		if ($model) {
			$Survey = Survey::model()->findAll();

			$criteria = new CDbCriteria();
			$criteria->compare('active',1);
			$criteria->order = "those_id ASC";
			$Those = Those::model()->findAll($criteria);

			$criteria = new CDbCriteria();
			$criteria->compare('active',1);
			$criteria->order = "province_name_th ASC";
			$Province = Province::model()->findAll($criteria);

			$respon = '';
			$respon = $this->renderPartial('update',array('model' => $model, 'Survey' => $Survey, 'Those' => $Those , 'Province' => $Province));
		}

	}

	public function actionUpdateSubProcessing()
	{

		$model = $this->loadSubModel($_POST['id_subProcessing']);

		if (!empty($model)) {
	    	// var_dump($model->processing_id);exit();
			$respon = '';
			$respon = $this->renderPartial('updatesub',array('model' => $model));
		}
		
	}

	public function actionDeleteProcessing()
	{
		$return = true;
		$model = $this->loadModel($_POST['id_Processing']);
		if ($model) {
			$model->active = 0;
			if ($model->save()) {
				$return = true;
			}else{
				$return = false;
			}

			echo $return;
		}
	}

	public function actionDeleteSubProcessing()
	{
		$return = true;
		$model = $this->loadSubModel($_POST['id_Processing']);
		if ($model) {
			$model->active = 0;
			if ($model->save()) {
				$return = true;
			}else{
				$return = false;
			}

			echo $return;
		}
	}

	public function loadModel($id)
	{
		$model=Processing::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	public function loadSubModel($id)
	{
		$model=SubProcessing::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

}