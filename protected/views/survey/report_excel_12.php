<?php
header("Content-Type: application/vnd.ms-excel");
header('Content-Disposition: attachment; filename="MyXls.xls"');
$strExcelFileName = 'export-'.$group["question_group_name"]."-".date("d-m-Y") . ".xls";
header("Content-Type: application/x-msexcel; name=\"" . $strExcelFileName . "\"");
header("Content-Disposition: inline; filename=\"" . $strExcelFileName . "\"");
header('Content-Type: text/plain; charset=UTF-8');
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Pragma:no-cache");
?>

<html xmlns:o=”urn:schemas-microsoft-com:office:office”

xmlns:x=”urn:schemas-microsoft-com:office:excel”

xmlns=”http://www.w3.org/TR/REC-html40″>

<HTML>

<HEAD>
  <meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />
</HEAD>
<BODY>
    <table>
        <thead>
            <tr>
                <th>ลำดับ</th>
                <!-- <th>เจ้าหน้าที่สำรวจ(ชื่อ)</th>
                <th>เจ้าหน้าที่สำรวจ(นามสกุล)</th>
                <th>เจ้าหน้าที่สำรวจ(รหัส)</th>
                <th>เจ้าหน้าที่สำรวจ(วัน/เดือน/ปี)</th> -->
                <th>สศท.</th>
                <th>จังหวัด</th>
                <th>อำเภอ</th>
                <th>ตำบล</th>
                <th>หมู่ที่</th>
                <!-- <th>บ้านเลขที่</th> -->
                <th>พวกที่</th>
                <!-- <th>ผู้ขึ้นทะเบียนเกษตรกร(ชื่อ)</th>
                    <th>ผู้ขึ้นทะเบียนเกษตรกร(นามสกุล)</th> -->
                    <th>ผู้ขึ้นทะเบียนเกษตรกร(ตัวอย่างที่)</th>
                <!-- <th>ผู้ขึ้นทะเบียนเกษตรกร(เลขบัตรประจำตัวประชาชน)</th>
                <th>ผู้ขึ้นทะเบียนเกษตรกร(ผู้ขึ้นทะเบียนเกษตร)</th>
                <th>ชื่อผู้ให้ข้อมูล(ชื่อ)</th>
                <th>ชื่อผู้ให้ข้อมูล(นามสกุล)</th>
                <th>ชื่อผู้ให้ข้อมูล(ความสัมพันธ์กับหัวหน้าครัวเรือน)</th>
                <th>ชื่อผู้ให้ข้อมูล(เลขบัตรประจำตัวประชาชน)</th>
                <th>ชื่อผู้ให้ข้อมูล(เบอร์ติดต่อ)</th>
                <th>ครัวเรือนเกษตรมีที่ดินทำการเกษตรอย่างน้อย 1 แปลง อยู่ในเขตชลประทาน</th> -->
                <?php foreach ($Question as $keyQuestion => $valueQuestion) {
                    $criteria = new CDbCriteria;
                    $criteria->compare('question_id',$valueQuestion->question_id);
                    $criteria->order = "t.sortorder ASC";
                    $QuestionSub = QuestionSub::model()->findAll($criteria);
                    foreach ($QuestionSub as $keyQuestionSub => $valueQuestionSub) { ?>
                        <th>Question-<?=$valueQuestion->question_id?>/<?=$valueQuestionSub->id?></th>
                    <?php }
                    ?>
                <?php } ?>
                <!-- <th>จำนวนหมู่ทั้งหมด</th>
                <th>จำนวนหมู่สุ่ม</th>
                <th>จำนวนหมู่สำรวจ</th>
                <th>ครัวเรือนเกษตรทั้งหมด</th>
                <th>ครัวเรือนเกษตรตามพวก</th>
                <th>ครัวเรือนเกษตรสำรวจ</th> -->
            </tr>
        </thead>
        <tbody>
            <?php $key =1; ?>
            <?php foreach ($Questionnaire as $keyQ => $valueQ) { ?>
                <tr>
                    <td><?=$key++?></td>
                        <!-- <td><?=$valueQ->staff_name?></td>
                        <td><?=$valueQ->staff_lname?></td>
                        <td>'<?=$valueQ->staff_id?></td>
                        <td><?=$valueQ->survey_time?></td> -->
                        <td><?=$valueQ->office_id?></td>
                        <td>'<?=$valueQ->province?></td>
                        <td>'<?=$valueQ->district?></td>
                        <td>'<?=$valueQ->subdistrict?></td>
                        <td>'<?=$valueQ->village?></td>
                        <!-- <td>'<?=$valueQ->addressNumber?></td> -->
                        <td><?=$valueQ->group?></td>
                        <!-- <td><?=$valueQ->farmer_name?></td>
                            <td><?=$valueQ->farmer_lastname?></td> -->
                            <td><?=$valueQ->sample?></td>
                       <!--  <td>'<?=$valueQ->farmer_id?></td>
                        <td><?=$valueQ->farmer_registered?></td>
                        <td><?=$valueQ->informant_name?></td>
                        <td><?=$valueQ->informant_lastname?></td>
                        <td><?=$valueQ->informant_relation?></td>
                        <td>'<?=$valueQ->informant_id?></td>
                        <td>'<?=$valueQ->informant_tel?></td>
                        <td><?=$valueQ->inIrrigatedArea?></td> -->
                        <?php foreach ($Question as $keyQuestion => $valueQuestion) {
                            $criteria = new CDbCriteria;
                            $criteria->compare('question_id',$valueQuestion->question_id);
                            $criteria->order = "t.sortorder ASC";
                            $QuestionSub = QuestionSub::model()->findAll($criteria);
                            foreach ($QuestionSub as $keyQuestionSub => $valueQuestionSub) { ?>
                                <?php 
                                $criteria=new CDbCriteria;
                                $criteria->compare('questionnaire_id',$valueQ->questionnaire_id);
                                $criteria->compare('question_sub_id',$valueQuestionSub->id);
                                $criteria->compare('question_id',$valueQuestion->question_id);
                                $SurveySubValue = SurveySubValue::model()->find($criteria);
                                if (!empty($SurveySubValue)) { ?>
                                    <td><?=$SurveySubValue->value?></td>
                                <?php }else{ ?>
                                    <td>-</td>
                                <?php }
                                ?>
                            <?php }
                            ?>
                        <?php } ?>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </BODY>



</HTML>
