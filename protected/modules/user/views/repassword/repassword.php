
<style type="text/css">
  body{
    overflow: hidden;
  }
  .main-nav,.oae-header,.footer-oae{
    display: none !important;
  }
  .page-body{
    padding: 0 !important;
    margin-left: 0 !important;
  }
  .error{
    margin: 0 !important
  }
   .label-red{
    color: red !important;
   }
   .btn-login-main{
    margin: 15px !important;
   }
</style>
<!-- Content -->
<section class="content" id="contact-us">

 <?php
   $form = $this->beginWidget('CActiveForm',array(
	'action'=>Yii::app()->createUrl('user/Repassword/Repassword'),
    ));
?>
        <div class="page-wrapper">
	      <div class="container-fluid p-0">
	        <div class="row d-flex justify-content-center">
	          <div class="col-12 col-lg-5">     
	            <div class="login-card">
	              <div>
	                <div class="login-main"> 
	                 
	                    <h4>สร้างรหัสผ่านของท่าน</h4>
	                    <div class="form-group">
	                      <label class="col-form-label">รหัสผ่านใหม่</label>
	                      <div class="form-input position-relative">
	                        <?php echo $form->passwordField($model, 'password', array('class' => 'form-control input-lg' , 'required' => true)); ?>
                        	<?php echo $form->error($model, 'password'); ?>
	                        <!-- <div class="show-hide"><span class="show"></span></div> -->
	                      </div>
	                    </div>
	                    <div class="form-group">
	                      <label class="col-form-label">ยืนยันรหัสผ่านใหม่</label>
	                      <?php echo $form->passwordField($model, 'verifyPassword', array('class' => 'form-control input-lg' , 'required' => true)); ?>
                        <?php echo $form->error($model, 'verifyPassword'); ?>
                        <?php echo $form->hiddenField($model, 'id', array('class' => 'form-control input-lg','hidden')); ?>
	                    </div>
	                    <div class="form-group mb-0">
	                      <button class="btn btn-primary btn-block w-100" type="submit">ยืนยันสร้างรหัสผ่านใหม่</button>
	                    </div>
	                
	                </div>
	              </div>
	            </div>
	          </div>
	        </div>
	      </div>
	    </div>
	      <?php $this->endWidget();?>
</section>
