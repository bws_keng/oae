<?php 

class MtSubDistrictController extends Controller
{
	public $layout='admin';
    public function actionIndex()
    {
        $model = new MtSubDistrict;
		$criteria = new CDbCriteria;
		$criteria->compare('active', 1);
		$criteria->order = 'subdistrict_name_th ASC';
		$data = MtSubDistrict::model()->findAll($criteria);
		$this->render('index', Array(
			'data' => $data,
			'model' => $model,
		));
    }

    public function actionCreate()
    {
        $model=new MtSubDistrict;
		if(isset($_POST['MtSubDistrict']))
    	{
			$model->attributes = $_POST['MtSubDistrict'];
			$model->active = 1;
			if($model->validate()){
                // var_dump($model->attributes);exit();
				if($model->save()){
					$this->redirect(array('view', 'id' => $model->id));
				}
			}
		}
		$this->render('create', Array(
			'model' => $model
		));
    }

    public function actionView($id)
    {
        $this->render('view',array(
    		'model'=>$this->loadModel($id),
    	));
    }

    public function actionEdit($id)
	{
		$model=$this->loadModel($id);
		
		if(isset($_POST['MtSubDistrict']))
    	{
			$model->attributes = $_POST['MtSubDistrict'];
			if($model->validate()){
				if($model->save()){
					$this->redirect(array('view', 'id' => $model->id));
				}
			}
		}
		$this->render('update', Array(
			'model' => $model
		));
	}

    public function actionDelete($id)
	{
		$model = $this->loadModel($id);
		$model->active = 0;

		if($model->save())
		{
			$this->redirect(array('index'));
		}
	}

    public function loadModel($id){
		$model = MtSubDistrict::model()->findByPk($id);
		return $model;
	}
}

?>