 
 
                <div class="container-fluid">

            

                    <!-- start row -->
                    <div class="row">
                        <div class="col-lg-12 set-height set-padding">
                            <div id="section1">
                                <div class="card">
                                    <div class="card-header">
                                        <h5 class="mb-1">หมวดที่ 10 ข้อมูลส่วนบุคคล การเคลื่อนย้าย และการประกอบอาชีพของสมาชิกในครัวเรือนเกษตร (ปี พ.ศ.2562 - พ.ศ.2563)</h5>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-body">

                                        <form>
                                            <div class="row">
                                                <div class="col-md-2 mb-3">
                                                    <label for="">จังหวัด</label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-2 mb-3">
                                                    <label for="">อำเภอ</label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-2 mb-3">
                                                    <label for="">ตำบล</label>
                                                    <div class="input-group">
                                                        <input class="form-control" id="" type="text" placeholder="" aria-describedby="" required="" data-original-title="" title="">
                                                    </div>
                                                </div>
                                                <div class="col-md-2 mb-3">
                                                    <label for="">หมู่ที่</label>
                                                    <div class="input-group">
                                                        <input class="form-control" id="" type="text" placeholder="" aria-describedby="" required="" data-original-title="" title="">
                                                    </div>
                                                </div>
                                                <div class="col-md-2 mb-3">
                                                    <label for="">พวกที่</label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-2 mb-3">
                                                    <label for="">ตัวอย่างที่</label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                            </div>
                                        </form>

                                    </div>
                                </div>
                            </div>

                            <div id="section2">

                                <h5 class="mb-4"> &nbsp;
                                    <button class="btn btn-lg btn-secondary btn-note float-right" type="button" data-toggle="modal" data-target=".bd-example-modal-lg"><i class="fa fa-list"></i> &nbsp;รายการ</button><br>
                                    <div class="modal fade bd-example-modal-lg modal-main" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h6 class="modal-title" id="myLargeModalLabel">รายการ</h6>
                                                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="card">
                                                                <div class="card-header">
                                                                    <label for="">(2) สถานภาพในครัวเรือน</label>
                                                                </div>
                                                                <div class="card-body">
                                                                    <div class="row">
                                                                        <label class="mb-2"><u>คนในครอบครัว</u></label>
                                                                        <div class="col-md-12">
                                                                            <small for="">1. หัวหน้าครัวเรือน</small>
                                                                        </div>
                                                                        <div class="col-md-12">
                                                                            <small for="">2. สามี/ภรรยา</small>
                                                                        </div>
                                                                        <div class="col-md-12">
                                                                            <small for="">3. บุตร</small>
                                                                        </div>
                                                                        <div class="col-md-12">
                                                                            <small for="">4. พ่อ/แม่/พ่อตา/แม่ยาย</small>
                                                                        </div>
                                                                        <div class="col-md-12">
                                                                            <small for="">5. บุตรเขย/สะใภ้/หลาน</small>
                                                                        </div>
                                                                        <div class="col-md-12">
                                                                            <small for="">6. พี่น้อง/เครือญาติ อื่นๆ</small>
                                                                        </div>
                                                                        <label class="mt-2 mb-2"><u>ผู้อาศัยอื่นๆ</u></label>
                                                                        <div class="col-md-12">
                                                                            <small for="">7. ผู้อาศัยอื่นๆ เช่นลูกจ้างที่กินอยู่ด้วยกัน</small>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="card">
                                                                <div class="card-header">
                                                                    <label for="">(5) ระดับการศึกษาสูงสุด</label>
                                                                </div>
                                                                <div class="card-body">
                                                                    <div class="row">
                                                                        <label class="mb-2"><u>สำเร็จการศึกษาสูงสุดในระดับ</u></label>
                                                                        <div class="col-md-12">
                                                                            <small for="">1. ประถมต้น (ป.4)</small>
                                                                        </div>
                                                                        <div class="col-md-12">
                                                                            <small for="">2. ประถมปลาย (ป.6,7)</small>
                                                                        </div>
                                                                        <div class="col-md-12">
                                                                            <small for="">3. มัธยมต้น (ม.3)</small>
                                                                        </div>
                                                                        <div class="col-md-12">
                                                                            <small for="">4. มัธยมปลาย (ม.6)</small>
                                                                        </div>
                                                                        <div class="col-md-12">
                                                                            <small for="">5. อนุปริญญา อาชีวะศึกษา</small>
                                                                        </div>
                                                                        <div class="col-md-12">
                                                                            <small for="">6. ปริญญาตรี หรือสูงกว่า</small>
                                                                        </div>
                                                                        <label class="mt-2 mb-2"><u>ไม่สำเร็จการศึกษา</u></label>
                                                                        <div class="col-md-12">
                                                                            <small for="">7. ไม่รู้หนังสือ อ่าน/เขียนไม่ได้</small>
                                                                        </div>
                                                                        <div class="col-md-12">
                                                                            <small for="">8. อ่านออกเขียนได้</small>
                                                                        </div>
                                                                        <label class="mt-2 mb-2"><u>เด็กเล็ก</u></label>
                                                                        <div class="col-md-12">
                                                                            <small for="">9. เด็กเล็กที่ยังไม่เข้าเรียน/ยังไม่จบชั้นประถมต้น</small>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="card">
                                                                <div class="card-header">
                                                                    <label for="">(6) อาชีพหลัก</label>
                                                                </div>
                                                                <div class="card-body">
                                                                    <div class="row">
                                                                        <label class="mb-2"><u>ในการเกษตร</u></label>
                                                                        <div class="col-md-12">
                                                                            <small for="" class="text-danger">1. เกษตรกรเต็มเวลา ในฟาร์มของตนเอง</small>
                                                                        </div>
                                                                        <div class="col-md-12">
                                                                            <small for="" class="text-danger">2. รับจ้างในการเกษตร นอกฟาร์ม (รายวัน/รายเดือน)</small>
                                                                        </div>
                                                                        <label class="mt-2 mb-2"><u>นอกการเกษตร</u></label>
                                                                        <div class="col-md-12">
                                                                            <small for="">3. รับจ้างนอกการเกษตร (รายวัน/รายเดือน)</small>
                                                                        </div>
                                                                        <div class="col-md-12">
                                                                            <small for="">4. ทำงานมีเงินเดือนประจำ (บริษัท โรงงาน ราชการ)</small>
                                                                        </div>
                                                                        <div class="col-md-12">
                                                                            <small for="">5. ค้าขาย/ธุรกิจส่วนตัว/กิจการในครัวเรือน</small>
                                                                        </div>
                                                                        <label class="mt-2 mb-2"><u>ไม่ได้ทำงาน/ไม่มีรายได้จากภายนอก</u></label>
                                                                        <div class="col-md-12">
                                                                            <small for="">6. เรียนหนังสือ</small>
                                                                        </div>
                                                                        <div class="col-md-12">
                                                                            <small for="">7. ว่างงาน/แม่บ้าน</small>
                                                                        </div>
                                                                        <div class="col-md-12">
                                                                            <small for="">8. คนชรา ผู้พิการ คนที่ทำงานไม่ได้ เด็กเล็ก</small>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="card">
                                                                <div class="card-header">
                                                                    <label for="">(7) การเป็นสมาชิกกลุ่ม</label>
                                                                </div>
                                                                <div class="card-body">
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <small for="">1. ธ.ก.ส.</small>
                                                                        </div>
                                                                        <div class="col-md-12">
                                                                            <small for="">2. สหกรณ์</small>
                                                                        </div>
                                                                        <div class="col-md-12">
                                                                            <small for="">3. กลุ่มเกษตรกร/กลุ่มออมทรัพย์</small>
                                                                        </div>
                                                                        <div class="col-md-12">
                                                                            <small for="">4. กลุ่มองค์กรชุมชนท้องถิ่น เช่น สภาหมู่บ้าน สภาตำบล</small>
                                                                        </div>
                                                                        <div class="col-md-12">
                                                                            <small for="">5. กลุ่มเพื่อจุดประสงค์อื่นๆ เช่น กลุ่มแม่บ้าน กลุ่มสตรี</small>
                                                                        </div>
                                                                        <div class="col-md-12">
                                                                            <small for="">6. ไม่ได้เป็นสมาชิกกลุ่มใดๆ</small>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="card">
                                                                <div class="card-header">
                                                                    <label for="">(10) เหตุผลการเคลื่อนย้าย</label>
                                                                </div>
                                                                <div class="card-body">
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <small for="">1. ย้ายตามฤดูกาลเพราะภัยธรรมชาติ</small>
                                                                        </div>
                                                                        <div class="col-md-12">
                                                                            <small for="">2. ย้ายตามครอบครัว/เหตุผลส่วนบุคคล</small>
                                                                        </div>
                                                                        <div class="col-md-12">
                                                                            <small for="">3. ย้ายเพราะเรื่องงาน (ได้งานใหม่/เปลี่ยนตำแหน่ง/ถูกเลิกจ้าง)</small>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                        </div>
                                                        <div class="col-md-6 ">
                                                            <div class="card">
                                                                <div class="card-body">
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <small for="" class="text-danger">ถ้าสมาชิกอยู่ประจำ คำตอบในสดมภ์ที่ (8)-1 ให้กรอกข้อมูลในสดมภ์ที่ (9)
                                                                                และ (10) ดังนี้</small>
                                                                        </div>
                                                                        <div class="col-md-12">
                                                                            <small for="" class="text-danger">- เดือนที่ย้ายเข้า/ย้ายออก สดมภ์(9)=99</small>
                                                                        </div>
                                                                        <div class="col-md-12">
                                                                            <small for="" class="text-danger">- เหตุผลการเคลื่อนย้าย สดมภ์ที่(10)=99</small>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </h5>

                                <div class="card">
                                    <div class="card-body">

                                        <form>
                                            <div class="row">
                                                <div class="col-12">
                                                    <h6><small class="text-danger">หมายเหตุ : สอบถามถึงสมาชิกทุกคน รวมถึงญาติพี่น้องและลูกจ้างที่อาศัย กิน และอยู่ร่วมกันในครัวเรือนระหว่างปีเพาะปลูกและ/หรือมีภาระผูกพันร่วมกันกับครัวเรือน</small></h6>
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">คนที่ <small class="text-danger">(1)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">สถานภาพในครัวเรือน <small class="text-danger">(2)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="(1-7)" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">เพศ <small class="text-danger"> ชาย = 1 , หญิง = 2 (3)</small></label>
                                                    <div class="input-group">
                                                        <input class="form-control" id="" type="text" placeholder="(1-2)" aria-describedby="" required="" data-original-title="" title="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4 mb-3">
                                                    <label for="">อายุ <small class="text-danger">(4)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="(ปี)" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">ระดับการศึกษา<u>สูงสุด</u> <small class="text-danger">(5)</small></label>
                                                    <input class="form-control" id="" type="text" placeholder="(1-9)" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">อาชีพหลัก <small class="text-danger">(6)</small></label>
                                                    <div class="input-group">
                                                        <input class="form-control" id="" type="text" placeholder="(1-8)" aria-describedby="" required="" data-original-title="" title="">
                                                    </div>
                                                </div>
                                                <div class="col-md-12 mb-3">
                                                    <label for="">การเป็นสมาชิกกลุ่ม <small class="text-danger">(7)</small></label>
                                                    <div class="input-group">
                                                        <input class="form-control" id="" type="text" placeholder="(1-6)" aria-describedby="" required="" data-original-title="" title="">
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="row">
                                                <div class="col-12">
                                                    <h6 class="text-head mb-3"><i class="icon-pencil-alt"></i> การเคลื่อนย้ายของสมาชิกในครัวเรือน</h6>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-7 mb-3">
                                                    <label for=""><u>ลักษณะการเคลื่อนย้าย</u> <span class="text-danger">ประจำ = 1, ย้ายเข้า = 2, ย้ายออก = 3</span> </label>
                                                    <input class="form-control" id="" type="text" placeholder="(1-3)" required="" data-original-title="" title="">
                                                </div>

                                            </div><label for=""><u>เดือนที่ย้ายเข้า/ย้ายออก</u> <span class="text-danger">ม.ค.=1, ก.พ.=2, พ.ย.=11, ธ.ค.=12 หรือ 99</span> </label>
                                            <div class="row">
                                                <div class="col-md-7 mb-3">
                                                    
                                                    <input class="form-control" id="" type="text" placeholder="(1-12) หรือ 99" required="" data-original-title="" title="">
                                                </div>

                                            </div>
                                            <div class="row">
                                                <div class="col-md-7 mb-3">
                                                    <label for=""><u>เหตุผลการเคลื่อนย้าย</u> <span class="text-danger">ถ้าประจำ ใส่ 99</span> </label>
                                                    <input class="form-control" id="" type="text" placeholder="(1-3) หรือ 99" required="" data-original-title="" title="">
                                                </div>

                                            </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                        </form>

                    </div>
                </div>
            </div>

            <div class="row text-center group-submit">
                <button type="button" class="btn btn-primary btn-lg">บันทึกข้อมูล</button>
                <button type="button" class="btn btn-outline-secondary btn-lg">ยกเลิก</button>
            </div>
        </div>
    </div>
    <!-- end row -->

    </div>


    </div>

    <?php include 'theme/assets/include/inc-footer.php'; ?>
    </div>
    </div>

    <?php include 'theme/assets/include/inc-script.php'; ?>

</body>

</html>