<div class="container-fluid">
    <!-- start row -->
    <div class="row">
        <div class="col-lg-12 set-height set-padding">
            <div id="section1">
                <div class="card">
                    <div class="card-header">
                        <h5 class="mb-1">หมวดที่ <?php echo $title["question_group_number"]; ?> จัดการคำถาม</h5>
                    </div>
                </div>
                <div class="card table-card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-4 mb-3">
                                <label for="">รหัสคำถาม</label>
                                <input class="form-control" id="question_search" maxlength="4" type="text" placeholder="" required=""
                                    data-original-title="" title="">
                            </div>
                            <!-- <div class="col-md-4 mb-3">
                                <div class="form-group">
                                    <label for="">ปี พ.ศ. แบบสอบถาม</label>
                                    <select class="form-control digits" id="year_search">
                                        <option selected disabled></option>
                                        <?php
                                            foreach($years as $year){
                                                echo '<option value="'.$year['year'].'">'.$year['year'].'</option>';
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div> -->
                            <div class="col-md-3 mb-3">
                                <button class="btn btn-lg btn-search text-white" type="submit" onclick="SearchData();"><i
                                        class="icon-search"></i>&nbsp;ค้นหา</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-end">
                    <div class="col-md-3">
                        <a href="<?php echo Yii::app()->baseurl . '/admin/survey/'.$title["question_group_id"].'/question/create'; ?>">
                            <div class="card o-hidden create-main">
                                <div class="b-r-4 card-body">
                                    <div class="media static-top-widget">
                                        <div class="align-self-center text-center"><i data-feather="plus"></i></div>
                                        <div class="media-body">
                                            <h6 class="mb-0 counter">เพิ่มคำถาม</h6><i class="icon-bg"
                                                data-feather="database"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="card table-card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-main" id="">
                                <!-- <table class="table table-striped table-main" id="basic-2"> -->
                                <thead>
                                    <tr>
                                        <th width="5%">ลำดับ</th>
                                        <th width="10%">เลขที่คำถาม</th>
                                        <th>คำถาม</th>
                                        <th width="25%">จัดการ</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        
                                        foreach($survey as $key => $row){
                                            echo '
                                            <tr>
                                                <td>'.($key+1).'</td>
                                                <td>'.($row['readable_id'] != "" ? $row['readable_id']: "-").'</td>
                                                <td>'.$row['question'].'</td>
                                                <td>
                                                    <a type="button" class="btn btn-icon text-success" href="'. Yii::app()->baseurl.'/admin/survey/'.$_GET['id'].'/question/'.$row['question_id'].'/edit">
                                                        <i data-feather="edit"></i>
                                                    </a>
                                                    <button type="button" class="btn btn-icon text-danger" onclick="sendDelete('.$row['question_id'].', \''.$row['question'].'\')"><i
                                                            data-feather="trash"></i></button>
                                                </td>
                                            </tr>
                                        ';
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="table-responsive-search" style="display: none;"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end row -->
    <div class="container position-fixed fixed-top pt-5" id="deleteForm" style="display:none;">
        <div class="card col-12 p-0">
            <div class="card-header">
                <h5>ลบข้อมูล</h5>
            </div>
            <div class="card-body">
                <h5 class="card-title">เมื่อข้อมูลถูกลบจะไม่สามารถย้อนคืนได้</h5>
                <div class="card-text">
                    <form method="POST" action="<?php echo Yii::app()->baseurl; ?>/admin/survey/<?php echo $_GET['id']; ?>/question/delete">
                        <label for="deleteKey">รหัสอ้างอิง</label>
                        <input id="deleteKey" name="key" class="form-control" value="" readonly/>
                        <label for="deleteValue">ข้อมูล</label>
                        <input id="deleteValue" name="value" class="form-control" value="" readonly/>
                        <button class="btn btn-danger btn-lg mt-2">ลบข้อมูล</button>
                        <button type="button" class="btn btn-light btn-lg mt-2" onclick='$("#deleteForm").hide();'>ยกเลิก</button>
                    </form>
                </div>
            </div>
        </div>

    </div>
</div>
<script type="text/javascript">
    
    function SearchData(){
     var id = <?php echo $_GET['id']?>;
     var question_search = $("#question_search").val();
     // var year_search = $("#year_search").val();

     if (question_search != "" ) {

         $.ajax({
            url: "<?= $this->createUrl('/admin/SearchSurveyQuestion'); ?>", 
            type: "GET",
            data:  {
                id:id,
              question:question_search,
             // year:year_search,
          },
          success: function(data){

            if (data) {
                $('.table-responsive').remove();
                $('.table-responsive-search').show();
                $('.table-responsive-search').html(data);
            }
       }                           
   });
     }else{
    
      alert("ป้อนข้อมูลให้ถูกต้องแล้วกดค้นหา");
      location.reload();
  }

}
</script>