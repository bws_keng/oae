<?php

class User extends CActiveRecord
{
	const STATUS_NOACTIVE=0;
	const STATUS_ACTIVE=1;
	const STATUS_BANNED=-1;

	//TODO: Delete for next version (backward compatibility)
	const STATUS_BANED=-1;

	/**
	 * The followings are the available columns in table 'users':
	 * @var integer $id
	 * @var string $username
	 * @var string $password
	 * @var string $email
	 * @var string $activkey
	 * @var integer $createtime
	 * @var integer $lastvisit
	 * @var integer $superuser
	 * @var integer $status
     * @var timestamp $create_at
     * @var timestamp $lastvisit_at
     * @var string $group

	 */
	public $position_name;
	public $verifyPassword;
	public $verifyCode;
	public $pic_user;
	// public $orgchart_lv2;
	public $newpassword;
	public $confirmpass;
	public $idensearch;
	public $excel_file = null;
	public $supper_user_status;
	public $typeuser;
	public $type_employee;
	public $dateRang;
	public $user_id;
	public $nameSearch;
	public $month;
	public $fullname;
	public $news_per_page;
	public $passport;
	public $del_status;



	//public $register_status;

	/**
	 * Returns the static model of the specified AR class.
	 * @return CActiveRecord the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		// return '{{users}}';
		//return Yii::app()->getModule('user')->tableUsers;
		return 'db_oae.t_user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.CConsoleApplication
		return ((get_class(Yii::app())=='CConsoleApplication' || (get_class(Yii::app())!='CConsoleApplication' && Yii::app()->getModule('user')->isAdmin()))?array(

			array('password', 'length', 'max'=>128, 'min' => 4,'message' => UserModule::t("Incorrect password (minimal length 4 symbols).")),

			array('status', 'in', 'range'=>array(self::STATUS_NOACTIVE,self::STATUS_ACTIVE,self::STATUS_BANNED)),
			array('superuser', 'in', 'range'=>array(0,1,2)),
            array('create_at', 'default', 'value' => date('Y-m-d H:i:s'), 'setOnEmpty' => true, 'on' => 'insert'),
          //  array('lastvisit_at, last_activity', 'default', 'value' => '0000-00-00 00:00:00', 'setOnEmpty' => true, 'on' => 'insert'),
			// array('username, email, superuser, status,password,company_id,division_id,department_id,position_id,position_name', 'required'),
			// array('username, email, superuser, status,password', 'required'),
			array('superuser, status, email', 'required'),
			// array('identification', 'required','on' => 'general'),
			array('email', 'unique','message' => "อีเมลนี้ถูกใช้งานแล้ว"),
			
			array('password', 'required', 'on' => 'reset_password'),
			array('superuser, status, online_status', 'numerical', 'integerOnly'=>true),
			array('pic_user', 'file', 'types'=>'jpg, png, gif','allowEmpty' => true, 'on'=>'insert'),
			array('pic_user', 'file', 'types'=>'jpg, png, gif','allowEmpty' => true, 'on'=>'update'),
			array('news_per_page', 'safe'),
			array('id, username, active, password, email, activkey, create_at, lastvisit_at, superuser, status, online_status, group,idensearch,identification, avatar,del_status', 'safe', 'on'=>'search'),
			// array('verifyPassword', 'compare', 'compareAttribute'=>'password', 'message' => UserModule::t("Retype Password is incorrect.")),
			array('newpassword', 'length', 'max'=>128, 'min' => 4,'message' => UserModule::t("Incorrect password (minimal length 4 symbols).")),
			//array('confirmpass', 'compare', 'compareAttribute'=>'password', 'message' => UserModule::t("Retype Password is incorrect.")),
			//array('email','checkEmail'),
		):((Yii::app()->user->id==$this->id)?array(
			array('username,password', 'required'),
			array('superuser, status, online_status', 'numerical', 'integerOnly'=>true),

			array('idensearch', 'length', 'max'=>13),
			//array('username','checkEmail'),
			array('excel_file', 'required', 'on'=>'import'),
            array('excel_file', 'file', 'allowEmpty'=>false, 'types'=>'xls', 'on'=>'import'),
		):array()));
	}

	public function checkEmail(){
		if(Yii::app()->controller->action->id == 'update') return true;
		$email = User::model()->find(array(
			'condition' => 'del_status=:del_status AND email=:email',
			'params' => array('del_status'=>'0','email'=>$this->username)));
		!empty($email) ? $this->addError("username",UserModule::t("This user's email address already exists.")) : '';
	}

	public static function getOnlineUsers()
	{
		$sql = "SELECT session.user_id, tbl_users.username FROM session LEFT JOIN tbl_users ON tbl_users.id=session.user_id";
		$command = Yii::app()->db->createCommand($sql);

		return $command->queryAll();
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
         $relations = Yii::app()->getModule('user')->relations;
        // if (!isset($relations['profile']))
            $relations['profile'] = array(self::HAS_ONE, 'Profile', 'user_id');

        $relations['typeUsers'] = array(
            self::BELONGS_TO, 'TypeUser', array('id'=>'type_register')
        );

        $relations['group'] = array(
            self::HAS_MANY,'PGroup',array('id'=>'group')
        );

        return $relations;
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => UserModule::t("Id"),
			'username'=>UserModule::t("username"),
			'password'=>UserModule::t("password"),
			'verifyPassword'=>UserModule::t("Retype Password"),
			'email'=>UserModule::t("E-mail"),
			'verifyCode'=>UserModule::t("Verification Code"),
			'activkey' => UserModule::t("activation key"),
			'createtime' => UserModule::t("Registration date"),
			'create_at' => UserModule::t("Registration date"),
			'lastvisit_at' => UserModule::t("Last visit"),
			'superuser' => UserModule::t("Superuser"),
			'status' => UserModule::t("Status"),
			'newpassword' => 'รหัสผ่านใหม่',
			'confirmpass' => 'ยืนยันรหัสผ่านใหม่',
			'group' => 'กลุ่มผู้ใช้งาน',
			'idensearch' => 'รหัสบัตรประชาชน',
			'identification' => 'รหัสบัตรประชาชน',
			'repass_status' => 'สถานะการเปลี่ยนรหัสผ่าน',
			'excel_file' => 'ไฟล์ Excel Import',
			'typeuser' =>'ประเภทผู้ใช้งาน',
			'dateRang' => 'เลือกระยะเวลา',
		);
	}

	public function scopes()
    {
        return array(
            'active'=>array(
                'condition'=>'status='.self::STATUS_ACTIVE,
            ),
            'notactive'=>array(
                'condition'=>'status='.self::STATUS_NOACTIVE,
            ),
//            'banned'=>array(
//                'condition'=>'status='.self::STATUS_BANNED,
//            ),
            'superuser'=>array(
                'condition'=>'superuser=1',
            ),
            'notsafe'=>array(
            	'select' => 'id, username, password, email, activkey, create_at, superuser, status, online_status, repass_status',
            ),
        );
    }

	public function defaultScope()
    {
        return CMap::mergeArray(Yii::app()->getModule('user')->defaultScope,array(
            'alias'=>'user',
            'select' => 'user.id, user.username user.email, user.create_at, user.lastvisit_at, user.superuser, user.status, user.online_status,group,user.identification,user.avatar,user.repass_status,user.del_status',
        ));
    }

	public static function itemAlias($type,$code=NULL) {
		$_items = array(
			'UserStatus' => array(
				self::STATUS_NOACTIVE => UserModule::t('ระงับการใช้งาน'),
				self::STATUS_ACTIVE => UserModule::t('เปิดการใช้งาน'),
//				self::STATUS_BANNED => UserModule::t('Banned'),
			),
			'AdminStatus' => array(
				'0' => UserModule::t('ผู้ใช้งาน'),
				'1' => UserModule::t('ผู้ดูแลระบบ'),
			),
			'Online' => array(
				'1' => UserModule::t('ออนไลน์'),
				'0' => UserModule::t('ออฟไลน์'),
			)
		);
		if (isset($code))
			return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
		else
			return isset($_items[$type]) ? $_items[$type] : false;
	}


public function validateIdCard($attribute,$params){
        $str = $this->identification;
        $chk = strlen($str);
        if($chk == "13"){
            $id = str_split(str_replace('-', '', $this->identification)); //ตัดรูปแบบและเอา ตัวอักษร ไปแยกเป็น array $id
            $sum = 0;
            $total = 0;
            $digi = 13;
            for ($i = 0; $i < 12; $i++) {
                $sum = $sum + (intval($id[$i]) * $digi);
                $digi--;
            }
            $total = (11 - ($sum % 11)) % 10;
            if ($total != $id[12]) { //ตัวที่ 13 มีค่าไม่เท่ากับผลรวมจากการคำนวณ ให้ add error
                $this->addError('identification', 'เลขบัตรประชาชนนี้ไม่ถูกต้อง ตามการคำนวณของระบบฐานข้อมูลทะเบียนราษฎร์*');
            }
        }
    }
	public static function chk_online($id,$lastactivity,$online_status)
	{	
		if ($id!='' && $online_status == '0') {
			echo "<span style='color: #c6c6c6'>ออฟไลน์</span>";
		} else {
			$id_chk = User::model()->findByPk($id);
			$lasttime = time();
			$time = $lasttime - $lastactivity;
			$chktime = date("i:s",$time);
			if($id!='' && $chktime > '30.00' && $online_status == '1'){
				$id_chk->online_status = '0';
				$id_chk->save(false);
				echo "<span style='color: #c6c6c6'>ออฟไลน์</span>";
			}else{
				echo "<span style='color: #00A000;'>ออนไลน์</span>";
			}
		}
		
	}

	public function getFullnamee(){
		$profile = Profile::model()->findByPk($this->id);
		$text = $profile->firstname_en.' '.$profile->lastname_en;
		if($text == " "){
			$text = "";
		}
		return $text;
	}

/**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.
        $criteria=new CDbCriteria;
        $criteria->with = array('profile');

        // if(isset($_GET['User']['fullname'])){
        // 	$ex_fullname = explode(" ", $_GET['User']['fullname']);

        // 	if(isset($ex_fullname[0])){
        // 		$pro_fname = $ex_fullname[0];
        // 		$criteria->compare('profile.firstname_en', $pro_fname, true);
        // 		$criteria->compare('profile.lastname_en', $pro_fname, true, 'OR');
        // 	}
        	
        // 	if(isset($ex_fullname[1])){
        // 		$pro_lname = $ex_fullname[1];
        // 		$criteria->compare('profile.lastname_en',$pro_lname,true);
        // 	}
        // }

        $criteria->compare('id',$this->id);
        $criteria->compare('username',$this->username,true);
        $criteria->compare('password',$this->password);
        $criteria->compare('pic_user',$this->pic_user);
        // $criteria->compare('station_id',$this->station_id);
        // $criteria->compare('position_id',$this->position_id);
        // $criteria->compare('user.department_id',$this->department_id);
        // $criteria->compare('branch_id',$this->branch_id);
        $criteria->compare('email',$this->email,true);
        $criteria->compare('activkey',$this->activkey);
        $criteria->compare('create_at',$this->create_at);
        $criteria->compare('lastvisit_at',$this->lastvisit_at);
        $criteria->compare('lastactivity',$this->lastactivity);
        // $criteria->compare('report_authority',$this->report_authority);
        // $criteria->compare('superuser',$this->superuser);
        if(empty($this->supper_user_status)){
        	$criteria->compare('superuser',1);
        }else{
        	$criteria->compare('superuser',0);
        }     
        $criteria->compare('status',$this->status);
        $criteria->compare('del_status',0);
 		$criteria->compare('online_status',$this->online_status);
 		// $criteria->compare('online_user',$this->online_user);
 		$criteria->compare('group',$this->group);
 		$criteria->compare('identification',$this->identification);
 		// $criteria->compare('profile.type_user',$this->typeuser);
 		// $criteria->compare('profile.type_employee',$this->type_employee);
 		// $criteria->compare('profile.passport',$this->passport);
 		// $criteria->order = 'user.id DESC';
 	   

 		// $criteria->compare('profile.identification',$this->idensearch,true);

      $dataProvider = array('criteria'=>$criteria);
		// Page
		if(isset($this->news_per_page))
		{
			$dataProvider['pagination'] = array( 'pageSize'=> intval($this->news_per_page) );
		}
		
		return new CActiveDataProvider($this, $dataProvider);
    }

    public function searchResign()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.
        $criteria=new CDbCriteria;
        $criteria->with = array('profile');
        $criteria->compare('id',$this->id);
        $criteria->compare('username',$this->username,true);
        $criteria->compare('password',$this->password);
        $criteria->compare('pic_user',$this->pic_user);

        $criteria->compare('email',$this->email,true);
        $criteria->compare('activkey',$this->activkey);
        $criteria->compare('create_at',$this->create_at);
        $criteria->compare('lastvisit_at',$this->lastvisit_at);
        $criteria->compare('lastactivity',$this->lastactivity);

        // if(empty($this->supper_user_status)){
        // 	$criteria->compare('superuser',1);
        // }else{
        	$criteria->compare('superuser',0);
        // }     
        $criteria->compare('status',$this->status);
        $criteria->compare('del_status',1);
 		$criteria->compare('online_status',$this->online_status);
 		$criteria->compare('group',$this->group);
 		$criteria->compare('identification',$this->identification);


      $dataProvider = array('criteria'=>$criteria);
		// Page
		if(isset($this->news_per_page))
		{
			$dataProvider['pagination'] = array( 'pageSize'=> intval($this->news_per_page) );
		}
		
		return new CActiveDataProvider($this, $dataProvider);
    }

  

    public function getCreatetime() {
        return strtotime($this->create_at);
    }

    public function setCreatetime($value) {
    	
        $this->create_at=date('Y-m-d H:i:s',$value);
    }

    public function getLastvisit() {
        return strtotime($this->lastvisit_at);
    }

    public function setLastvisit($value) {
        $this->lastvisit_at=date('Y-m-d H:i:s',$value);
    }



      public function getregisstatusList()
    {
        $getregisstatusList = array(
            '1'=>'รอการตรวจสอบ',
            '2'=>'ไม่อนุมัติ'
        );
        return $getregisstatusList;
    }
     public function getapproveList()
    {
        $getregisstatusList = array(
            '1'=>'รออนุมัติ ',
            '0'=>'ไม่ผ่านอนุมัติ '
        );
        return $getregisstatusList;
    }
}
