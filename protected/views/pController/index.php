
    <?php
    $titleName = 'Controller';
    // $this->headerText = $titleName;
    $formNameModel = 'PController';
    $formNameId = "PController-grid";
    $title = "จัดการสิทธิ์";
    $this->breadcrumbs = array('จัดการ' . $titleName);
    Yii::app()->clientScript->registerScript('updateGridView', <<<EOD
	$.updateGridView = function(gridID, name, value) {
	    $("#"+gridID+" input[name*="+name+"], #"+gridID+" select[name*="+name+"]").val(value);
	    $.fn.yiiGridView.update(gridID, {data: $.param(
	        $("#"+gridID+" input, #"+gridID+" .filters select")
	    )});
	}
	$.appendFilter = function(name, varName) {
	    var val = eval("$."+varName);
	    $("#$formNameModel-grid").append('<input type="hidden" name="'+name+'" value="">');
	}
	$.appendFilter("PController[news_per_page]", "news_per_page");
EOD
        , CClientScript::POS_READY);



    $str_js = "
        var fixHelper = function(e, ui) {
            ui.children().each(function() {
                $(this).width($(this).width());
            });
            return ui;
        };
 		function installSortable() {
            $('#".$formNameId." tbody').sortable({
                items: 'tr',
                //handle: '.row_move',
                forceHelperSize: false,
                forcePlaceholderSize: true,
                tolerance: 'pointer',
                axis: 'y',
                update : function () {
                    serial = $('#".$formNameId." tbody').sortable('serialize', {key: 'items[]', attribute: 'class'});
                    $.ajax({
                        'url': '" . $this->createUrl('PController/priority',array('model'=>$formNameModel)) . "',
                        'type': 'post',
                        'data': serial,
                        'success': function(data){
                           // $.fn.yiiGridView.update('".$formNameId."');
                            location.reload();
                        },
                        'error': function(request, status, error){
                            alert('We are unable to set the sort order at this time.  Please try again in a few minutes.');
                        }
                    });
                },
                helper: fixHelper
            });
            $('#".$formNameId." tbody').disableSelection();
        }
        installSortable();
        
        function reInstallSortable(id, data) {
            installSortable();
            
        }
    ";

    Yii::app()->clientScript->registerScript('controller', $str_js,CClientScript::POS_END);

    ?>
<div class="container-fluid">
  <!-- start row -->
  <div class="row">
    <div class="col-lg-12 set-height set-padding">
      <div id="section1">
        <div class="card">
          <div class="card-header">
            <h5 class="mb-1">
              <?php
                echo $title;
              ?>
            </h5>
          </div>
        </div>
        <div class="row justify-content-end">
          <div class="col-md-3">
            <a href="<?php echo Yii::app()->createUrl('pController/create'); ?>">
              <div class="card o-hidden create-main">
                <div class="b-r-4 card-body">
                  <div class="media static-top-widget">
                    <div class="align-self-center text-center"><i data-feather="plus"></i></div>
                    <div class="media-body">
                      <h6 class="mb-0 counter">เพิ่มสิทธิ์</h6><i class="icon-bg" data-feather="database"></i>
                    </div>
                  </div>
                </div>
              </div>
            </a>
          </div>
        </div>
        <div class="card table-card">
          <!-- <div class="card-header">
            <div class="row">
              <div class="col-md-3 mb-3">
                <label for="">ผืนที่</label>
                <input class="form-control" id="" maxlength="1" type="text" placeholder="" required=""
                  data-original-title="" title="">
              </div>
              <div class="col-md-3 mb-3">
                <div class="form-group">
                  <label for="">จังหวัด</label>
                  <select class="form-control digits" id="">
                    <option selected disabled>เลือกจังหวัด</option>
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                  </select>
                </div>
              </div>
              <div class="col-md-3 mb-3">
                <div class="form-group">
                  <label for="">ปี พ.ศ.</label>
                  <select class="form-control digits" id="">
                    <option selected disabled>เลือกปี พ.ศ.</option>
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                  </select>
                </div>
              </div>
              <div class="col-md-3 mb-3">
                <button class="btn btn-lg btn-search text-white" type="submit"><i
                    class="icon-search"></i>&nbsp;ค้นหา</button>
              </div>
            </div>
          </div> -->
          <div class="card-body">
            <div class="table-responsive">
             <?php $this->widget('AGridView', array(
        'id'=>'PController-grid',
        'dataProvider' => $model->search(),
       // 'filter' => $model,
        'afterAjaxUpdate'=>'function(id, data){
            $.appendFilter("PController[news_per_page]");
            reInstallSortable();
        }',
        'selectableRows' => 2,
        'rowCssClassExpression' => '"items[]_{$data->id}"',
        'summaryText' => false, // 1st way
        'columns' => array(
            array(
                'class' => 'CCheckBoxColumn',
                'id' => 'chk',
            ),
            array(
                'header' => 'No.',
                'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            ),
            array(
                'name' => 'title',
                'type' => 'html',
                'value' => '$data->title'
            ),
            array(
                'name' => 'controller',
                'type' => 'html',
                'value' => '$data->controller'
            ),
            array(            // display 'author.username' using an expression
                'header'=>'priority',
                'type'=>'raw', //because of using html-code <br/>
                'htmlOptions'=>array('style'=>'text-align: center; width:100px;'),
                'value'=>'CHtml::label("<i class=\"fa fa-arrows\"></i>","");',
            ),
            array(
                'class' => 'zii.widgets.grid.CButtonColumn',
                'htmlOptions' => array('style' => 'white-space: nowrap'),
                'afterDelete' => 'function(link,success,data) { if (success && data) alert(data); }',
                'template' => '{view} {update} {delete}',
                'buttons' => array(
                    'view' => array(
                        'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => 'View'),
                        'label' => '<button type="button" class="btn btn-icon text-primary"><i data-feather="eye"></i></button>',
                        'imageUrl' => false,
                    ),
                    'update' => array(
                        'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => 'Update'),
                        'label' => '<button type="button" class="btn btn-icon text-success"><i data-feather="edit"></i></button>',
                        'imageUrl' => false,
                        'url'=>'Yii::app()->createUrl("pController/update",array("id"=>$data->id))',
                    ),
                    'delete' => array(
                        'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => 'Delete'),
                        'label' => '<button type="button" class="btn btn-icon text-danger"><i data-feather="trash"></i></button>',
                        'imageUrl' => false,
                       // 'url'=>'Yii::app()->createUrl("pController/delete",array("id"=>$data->id))',
                    )
                )
            ),
        ),
    )); ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
      <?php if (Controller::DeleteAll(array("PController.*", "PController.Delete", "PController.MultiDelete"))) : ?>
        <!-- Options -->
        <div class="separator top form-inline small">
            <!-- With selected actions -->
            <div class="buttons pull-left">
                <?php echo CHtml::link("<i></i> ลบข้อมูลที่เลือก", "#", array(
                    "class" => "btn btn-primary btn-icon glyphicons circle_minus",
                    "onclick" => "return multipleDelete('" . $this->createUrl('//' . $formNameModel . '/MultiDelete') . "','$formNameId');"
                )); ?>
            </div>
            <!-- // With selected actions END -->
            <div class="clearfix"></div>
        </div>
        <!-- // Options END -->
    <?php endif; ?>
  <!-- end row -->
</div>
<script type="text/javascript">
$(document).ready(function(){
    $('.ui-sortable-handle').each(function(){
     $(this).removeClass('ui-sortable-handle');
    });
});
</script>