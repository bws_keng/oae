<!DOCTYPE html>
<html lang="en">

<head>
  <!-- <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="author" content=""> -->
  <title>เข้าสู่ระบบ</title>
  <?php  Yii::app()->baseUrl.'/theme/assets/include/inc-head.php'; ?>
</head>

<style type="text/css">
  body{
    overflow: hidden;
  }
  .main-nav,.oae-header,.footer-oae{
    display: none !important;
  }
  .page-body{
    padding: 0 !important;
    margin-left: 0 !important;
  }
  .error{
    margin: 0 !important
  }
   .label-red{
    color: red !important;
   }
   .btn-login-main{
    margin: 15px !important;
   }
</style>

<body>
  <div class="tap-top"><i data-feather="chevrons-up"></i></div>

    <div class="row">
      <div class="bg-login col-lg-6" >
        <div class="overlay-video"></div>
        <video id="login-bg" autoplay="" mute="" loop="">
          <source src="<?php echo Yii::app()->baseUrl; ?>/theme/assets/video/login-bg-new.mp4" type="video/mp4">
          Your browser does not support HTML video.
        </video>
        <script type="text/javascript">
          var autoPlayVideo = document.getElementById("login-bg");
          autoPlayVideo.oncanplaythrough = function() {
              autoPlayVideo.muted = true;
              autoPlayVideo.loop = true;
              autoPlayVideo.play();
          }
        </script>
      </div>
      <?php
    $this->pageTitle=Yii::app()->name . ' - '.UserModule::t("Login");
    $this->breadcrumbs=array( UserModule::t("Login") );
    ?>
      <div class="login-main col-sm-12 col-lg-6">
        <div class="row justify-content-center">
          <div class="col-lg-8">
             <div class="auth-innerright">
                  <div class="authentication-box">
                    <div class="mt-4">
                      <?php echo CHtml::beginForm(); ?>
                      <form class="theme-form">
                        <div class="text-center mb-3">
                          <img src="<?php echo Yii::app()->baseUrl; ?>/theme/assets/images/logo/logo-login.png">
                        </div>
                        <h4 class="text-center">ลงชื่อเข้าใช้งานระบบ</h4>
                        <?php if(Yii::app()->user->hasFlash('loginMessage')): ?>
                                    <div class="success">
                                        <?php echo Yii::app()->user->getFlash('loginMessage'); ?>
                                    </div>
                                <?php endif; ?>
                        <div class="form-group m-t-40">
                          <label class="col-form-label pt-0">ชื่อผู้ใช้งาน</label>
                          <!-- <input class="form-control" placeholder="ชื่อผู้ใช้งาน" type="text" required=""> -->
                                <?php echo CHtml::activeTextField($model,'username', array(
                                    'class' => 'form-control',
                                    'placeholder' => 'ชื่อผู้ใช้งาน',
                                    ));?>
                                <?php echo CHtml::error($model,'username',array('class'=>'label label-important  label-red')); ?>
                        </div>

                        <div class="form-group">
                          <label class="col-form-label">รหัสผ่าน</label>
                          <!-- <input class="form-control" placeholder="รหัสผ่าน" type="password" required=""> -->
                          <?php echo CHtml::activePasswordField($model,'password', array(
                                        'class' => 'form-control',
                                        'placeholder' => 'รหัสผ่าน',
                                        ));?>
                          <?php echo CHtml::error($model,'password',array('class'=>'label label-important label-red')); ?>
                        </div>

                      <!--   <div class="checkbox p-0 checkbox-solid-primary">
                          <input id="checkbox1" type="checkbox" >
                          <label for="checkbox1">จดจำฉัน</label>
                        </div> -->

                        <div class="form-group row mt-3 mb-0">
                          <!-- <button class="btn btn-primary btn-block btn-lg" type="submit">เข้าสู่ระบบ</button> -->
                          <?php echo CHtml::submitButton(UserModule::t("เข้าสู่ระบบ"), array('class'=>'btn btn-primary btn-block btn-lg btn-login-main')); ?>
                        </div>
                        <br>
                        <div class="text-center mb-3">
                        <a href="#">ลืมรหัสผ่าน</a>
                        </div>
                      </form>

                    </div>
                  </div>
                </div>
          </div>  
        </div>
      </div>
    </div>

  <?php Yii::app()->baseUrl.'/theme/assets/include/inc-script.php'; ?>

</body>

</html>

<script type="text/javascript">
  $( document ).ready(function() {
    <?php if (isset($_GET["login"]) && $_GET["login"] == "resign") {?>
      alert("ผู้ใช้งานนี้อยู่ในสถานะ : ลาออก");
    <?php } ?>
  });
</script>