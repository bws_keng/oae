<?php

/**
 * This is the model class for table "p_action".
 *
 * The followings are the available columns in table 'p_action':
 * @property integer $id
 * @property integer $controller_id
 * @property string $title
 * @property string $action
 * @property string $create_date
 * @property string $create_by
 * @property string $update_date
 * @property string $update_by
 */
class Questionnaire extends CActiveRecord
{

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'oae-survey.questionnaire';
	}


    // public function beforeSave()
    // {
    //     if ($this->isNewRecord){
    //         $this->create_date= new CDbExpression('NOW()');
    //         $this->create_by= Yii::app()->user->id;
    //     }else{
    //         $this->update_date= new CDbExpression('NOW()');
    //         $this->update_by= Yii::app()->user->id;
    //     }
    //     return true;
    // }


	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            // array('title, action', 'required'),
			array('questionnaire_id, survey_id', 'numerical', 'integerOnly'=>true),
			array('province, district, subdistrict, village, group, sample, farmer_name, farmer_lastname, farmer_id, farmer_registered, informant_name, informant_lastname, informant_relation, informant_id, informant_tel, inIrrigatedArea, staff_id, survey_time, addressNumber, staff_name, staff_lname, office_id, page, question_key', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('questionnaire_id, survey_id, province, district, subdistrict, village, group, sample, farmer_name, farmer_lastname, farmer_id, farmer_registered, informant_name, informant_lastname, informant_relation, informant_id, informant_tel, inIrrigatedArea, staff_id, survey_time, addressNumber, staff_name, staff_lname, office_id, page, question_key', 'safe', 'on'=>'search'),
		);
	}


	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
      		'survey' => array(self::BELONGS_TO, 'Survey', 'survey_id'),
      		'those' => array(self::BELONGS_TO, 'Those', 'group'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'questionnaire_id' => 'questionnaire_id',
			'survey_id' => 'survey_id',
			'province' => 'province',
			'district' => 'district',
			'subdistrict' => 'subdistrict',
			'village'=> 'village',
			'group' => 'group',
			'sample' => 'sample',
			'farmer_name' => 'farmer_name',
			'farmer_lastname' => 'farmer_lastname',
			'farmer_id' => 'farmer_id',
			'farmer_registered' => 'farmer_registered',
			'informant_name' =>' informant_name',
			'informant_lastname' => 'informant_lastname',
			'informant_relation' => 'informant_relation',
			'informant_id'=> 'informant_id',
			'informant_tel' => 'informant_tel',
			'inIrrigatedArea' => 'inIrrigatedArea',
			'staff_id' => 'staff_id',
			'survey_time' => 'survey_time',
			'addressNumber' => 'addressNumber',
			'staff_name' => 'staff_name',
			'staff_lname' => 'staff_lname',
			'office_id' => 'office_id',
			'page' => 'page',
			'question_key' => 'question_key',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('questionnaire_id',$this->questionnaire_id);
		$criteria->compare('survey_id',$this->survey_id);
		$criteria->compare('province',$this->province,true);
		$criteria->compare('district',$this->district,true);
		$criteria->compare('subdistrict',$this->subdistrict,true);
		$criteria->compare('village',$this->village,true);
		$criteria->compare('group',$this->group,true);
		$criteria->compare('sample',$this->sample,true);
		$criteria->compare('farmer_name',$this->farmer_name,true);
		$criteria->compare('farmer_lastname',$this->farmer_lastname,true);
		$criteria->compare('farmer_id',$this->farmer_id,true);
		$criteria->compare('farmer_registered',$this->farmer_registered,true);
		$criteria->compare('informant_name',$this->informant_name,true);
		$criteria->compare('informant_lastname',$this->informant_lastname,true);
		$criteria->compare('informant_relation',$this->informant_relation,true);
		$criteria->compare('informant_id',$this->informant_id,true);
		$criteria->compare('informant_tel',$this->informant_tel,true);
		$criteria->compare('inIrrigatedArea',$this->inIrrigatedArea,true);
		$criteria->compare('staff_id',$this->staff_id,true);
		$criteria->compare('survey_time',$this->survey_time,true);
		$criteria->compare('addressNumber',$this->addressNumber,true);
		$criteria->compare('staff_name',$this->staff_name,true);
		$criteria->compare('staff_lname',$this->staff_lname,true);
		$criteria->compare('office_id',$this->office_id,true);
		$criteria->compare('page',$this->page,true);
		$criteria->compare('question_key',$this->question_key,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PAction the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
