<?php

class LogUserController extends Controller
{
    public $layout = 'admin';

    public function init()
    {
      if(!Yii::app()->user->id ){
        $this->redirect(Yii::app()->baseurl.'/user/login?admin=1');
      }else{
        $user = User::model()->findByPk(Yii::app()->user->id);
        if($user->superuser != 1){
        $this->redirect(Yii::app()->baseurl);

        }
      }
    }

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            // 'rights- toggle, switch, qtoggle',
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
  

	public function actionIndex()
	{
        $model=new LogUsers('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['LogAdmin']))
            $model->attributes=$_GET['LogAdmin'];

        $this->render('index',array(
            'model'=>$model,
        ));
	}

    // public function actionAuto()
    // {
    //     $model = new LogUserAuto('search');
    //     $model->unsetAttributes();  // clear any default values

    //     // var_dump($model); exit();
        
    //     if(isset($_GET['LogUserAuto'])){
    //         $model->attributes=$_GET['LogUserAuto'];
    //     }

    //     $this->render('auto',array(
    //         'model'=>$model,
    //     ));
    // }

      public function actionUsers()
    {   
        if (Yii::app()->user->id) {
            $User = User::model()->findByPk(Yii::app()->user->id);
            $profile = $User->profile;
            // if ($User->superuser != 1) {
            //     $criteria=new CDbCriteria;
            //     $criteria->compare('office_id',$profile->county_id);
            //     $Questionnaire = Questionnaire::model()->findAll($criteria);
            // }else{
            //     $Questionnaire = Questionnaire::model()->findAll();
            // }
                $Questionnaire = LogUsers::model()->findAll();

            }
        

            // $criteria=new CDbCriteria;
            // $criteria->compare('active',1);
            // $criteria->addIncondition('subdistrict_id',$subdistrict_array);
            // $masterVillage = masterVillage::model()->findAll($criteria);

            $this->render('users', Array(
                        // 'years' => QuestionnaireModel::getYears(),
                // 'Survey'=>$Survey,
                // 'MtProvince'=>$MtProvince,
                // 'MtDistrict'=>$MtDistrict,
                // 'MtSubDistrict'=>$MtSubDistrict,
                // 'masterVillage'=>$masterVillage,
                'data' => LogUsers::model()->findAll()
            ));
        
        // $this->render('list', Array(
        //     'data' => QuestionnaireModel::list(),
        // ));
    }

    public function actionEmail()
    {
        $model=new LogEmail('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['LogEmail']))
            $model->attributes=$_GET['LogEmail'];

        $this->render('email',array(
            'model'=>$model,
        ));
    }
}