<?php
$titleName = 'Create Controller';
// $this->headerText = $titleName;
$formNameModel = 'PController';
$title = "จัดการสิทธิ์";
?>
    <div class="container-fluid">
        <!-- start row -->
        <div class="row">
            <div class="col-lg-12 set-height set-padding">
                <div id="section1">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="mb-1">
                                <?php echo $title; ?>
                            </h5>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">

                                <?php $form = $this->beginWidget('AActiveForm', array(
                                    'id' => 'pcontroller-grid',
                                //'type' => 'horizontal',
                                    'enableAjaxValidation' => true,
                                    'enableClientValidation' => true,
                                    'clientOptions' => array(
                                        'validateOnSubmit' => true
                                    ),
                                'htmlOptions' => array('class' => 'well'), // for inset effect
                            )); ?>
                            <p class="help-block">Fields with <span class="required">*</span> are required.</p>

                            <?php echo $form->errorSummary($model_c); ?>
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <?php echo $form->textField($model_c, 'title', array('class' => 'form-control', 'placeholder' => 'Title')); ?>
                                </div>
                            </div>    
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <?php echo $form->textField($model_c, 'controller', array('class' => 'form-control', 'placeholder' => 'Controller')); ?>
                                </div>
                            </div>
                             <?php
                                    if(!$model_c->isNewRecord){ ?>

                                <div class="row">
                                    <label class="col-sm-3 control-label"></label>
                                        <div class="controls col-sm-9">
                                            <div class="action entry well">
                                                <button class="btn btn-success btn-add col-sm-9" type="button" style="width: 100%;">เพิ่ม
                                                    <span class="glyphicon glyphicon-plus"></span>
                                                </button>
                                            </div>
                                        </div>
                                </div>
                                     <?php
                                    }
                                    ?>
                            <div class="row">
                                <label class="col-sm-3 control-label"><?php echo $model_c->isNewRecord ? "Action" : ""; ?></label>
                                <div class="col-sm-9">
                                    <?php
                                    if(!$model_c->isNewRecord){

                                        $count_a = count($model_a);

                                        foreach ($model_a as $key=>$value){
                                            ?>
                                                <div class="entry well" style="margin-bottom: 0">
                                                    <div class="form-group ">
                                                        <label class="col-sm-9 control-label required" for="PAction_title">Title <span class="required">*</span></label>
                                                        <div class="col-md-9 mb-3">
                                                            <?php echo $form->textField($value, '['.$key.']title', array('class' => 'form-control', 'placeholder' => '')); ?>
                                                        </div>
                                                        <label class="col-sm-9 control-label required" for="PAction_title">Action <span class="required">*</span></label>
                                                        <div class="col-md-9 mb-3">
                                                            <?php echo $form->textField($value, '['.$key.']action', array('class' => 'form-control', 'placeholder' => '')); ?>
                                                        </div>
                                                    </div>
                                                    <div class="form-group" style="margin-bottom: 0;">
                                                        <label class="control-label"></label>
                                                        <button class="btn btn-danger btn-remove col-sm-9" type="button" style="width: 100%;">
                                                            <span class="glyphicon glyphicon-minus">ลบ</span>
                                                        </button>
                                                    </div>
                                                </div><br>
                                                <?php
                                            } ?>

                                            <?php
                                        }else{
                                            ?>
                                            <div class="entry well" style="margin-bottom: 0">
                                                <div class="form-group ">
                                                    <label class="col-sm-9 control-label required" for="PAction_title">Title <span class="required">*</span></label>
                                                    <div class="col-md-9 mb-3">
                                                        <?php echo $form->textField($model_a, '[0]title', array('class' => 'form-control', 'placeholder' => 'Title')); ?>
                                                    </div>

                                                    <label class="col-sm-9 control-label required" for="PAction_title">Action <span class="required">*</span></label>
                                                    <div class="col-md-9 mb-3">
                                                        <?php echo $form->textField($model_a, '[0]action',array('class' => 'form-control', 'placeholder' => 'Action')); ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                    ?>

                                </div>    
                                <?php
                                if ($model_c->isNewRecord) {
                            
                                ?>
                                <div class="row">
                                    <label class="col-sm-3 control-label"></label>
                                        <div class="controls col-sm-9">
                                            <div class="action entry well">
                                                <button class="btn btn-success btn-add col-sm-9" type="button" style="width: 100%;">เพิ่ม
                                                    <span class="glyphicon glyphicon-plus"></span>
                                                </button>
                                            </div>
                                        </div>
                                </div>
                                <?php
                                    }
                                ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row text-center group-submit">
                    <!-- <input type="submit" type="button" class="btn btn-primary btn-lg" value="บันทึกข้อมูล" />
                        <button type="button" class="btn btn-outline-secondary btn-lg">ยกเลิก</button> -->
                        <?php echo CHtml::tag('button',array('class' => 'btn btn-primary btn-lg btn-icon glyphicons ok_2'),'<i></i>บันทึกข้อมูล');?>
                        <button type="button" class="btn btn-outline-secondary btn-lgy" onclick="window.history.go(-1); return false;">ยกเลิก</button>
                    </div>
                </div>
                <?php $this->endWidget(); ?>
        <!-- end row -->
    </div>
<?php
Yii::app()->clientScript->registerScript('settings-script', <<<EOD

    $(function()
    {
        $(document).on('click', '.btn-add', function(e)
        {
            e.preventDefault();
            var numItems = $('.entry').length
            var data_html = 
             '<div class="row">'+
             '<div class="col-sm-12 control-label">'+
            // '<label class="col-sm-12 control-label">Action</label>'+
            '<div class="entry well" >'+
            '<div class="form-group"><label class="col-sm-9 control-label required" for="PAction_title">Title <span class="required">*</span></label>'+
            '<div class="col-sm-9 mb-3"><input class="form-control" maxlength="255" required placeholder="Title" name="PAction['+ numItems +'][title]" id="PAction_title" type="text">'+
            '<div class="help-block error" id="PAction_title_em_'+ numItems +'" style="display:none"></div>'+
            '</div>'+
            '</div>'+
            '<div class="form-group"><label class="col-sm-9 control-label required" for="PAction_action">Action <span class="required">*</span></label>'+
            '<div class="col-sm-9 mb-3"><input class="form-control " required maxlength="255" placeholder="Action" name="PAction['+ numItems +'][action]" id="PAction_action" type="text">'+
            '<div class="help-block error" id="PAction_action_em_'+ numItems +'"  style="display:none"></div>'+
            '</div>'+
            '</div>'+
            '<div class="form-group">'+
            '<label class="control-label"></label>'+
            '<button class="btn btn-danger btn-remove col-sm-9" type="button" style="width: 100%;">ลบ'+
            '<span class="glyphicon glyphicon-minus"></span>'+
            '</button>'+
            '</div>'+
             '</div>'+
             '</div>'+
            '</div>';
            
            var controlForm = $('.controls .action:first'),
            currentEntry = $(this).parents('.entry:first'),
            newEntry = $(data_html).appendTo(controlForm);

            newEntry.find('input').val('');
            controlForm.find('.entry:not(:last) .btn-add')
            .removeClass('btn-add').addClass('btn-remove')
            .removeClass('btn-success').addClass('btn-danger')
            .html('<span class="glyphicon glyphicon-minus"></span>');
            }).on('click', '.btn-remove', function(e)
            {
                $(this).parents('.entry:first').remove();

                e.preventDefault();
                return false;
                });
                });


EOD
, CClientScript::POS_END);
                ?>