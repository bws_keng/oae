<header class="main-nav">
  <!-- <div class="logo-wrapper">
    <div class="back-btn"><i class="fa fa-angle-left"></i></div>
  </div> -->
  <nav>
    <div class="main-navbar">
      <div class="left-arrow" id="left-arrow"><i data-feather="arrow-left"></i></div>
      <div id="mainnav">
        <ul class="nav-menu custom-scrollbar">
          <li class="back-btn">
            <div class="mobile-back text-right"><span>Back</span><i class="fa fa-angle-right pl-2" aria-hidden="true"></i></div>
          </li>
          <li class="dropdown"><a class="nav-link menu-title link-nav" href="<?php echo Yii::app()->baseurl.'/'; ?>"><i data-feather="home"> </i><span>หน้าหลัก</span></a></li>

          <?php $this->widget('UserMenu'); ?>


      </div>
      <div class="right-arrow" id="right-arrow"><i data-feather="arrow-right"></i></div>
    </div>
  </nav>
</header>