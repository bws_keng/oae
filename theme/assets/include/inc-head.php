<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="icon" href="theme/assets/images/favicon.png" type="image/x-icon">
<link rel="shortcut icon" href="theme/assets/images/favicon.png" type="image/x-icon">

<!-- Google font-->
<link href="https://fonts.googleapis.com/css?family=Rubik:400,400i,500,500i,700,700i&amp;display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900&amp;display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css2?family=Sarabun:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800&display=swap" rel="stylesheet">
<!-- Font Awesome-->
<link rel="stylesheet" type="text/css" href="theme/assets/css/fontawesome.css">
<!-- ico-font-->
<link rel="stylesheet" type="text/css" href="theme/assets/css/icofont.css">
<!-- Themify icon-->
<link rel="stylesheet" type="text/css" href="theme/assets/css/themify.css">
<!-- Flag icon-->
<link rel="stylesheet" type="text/css" href="theme/assets/css/flag-icon.css">
<!-- Feather icon-->
<link rel="stylesheet" type="text/css" href="theme/assets/css/feather-icon.css">
<!-- Plugins css start-->
<link rel="stylesheet" type="text/css" href="theme/assets/css/prism.css">
<link rel="stylesheet" type="text/css" href="theme/assets/css/animate.css">
<link rel="stylesheet" type="text/css" href="theme/assets/css/chartist.css">
<link rel="stylesheet" type="text/css" href="theme/assets/css/date-picker.css">
<link rel="stylesheet" type="text/css" href="theme/assets/css/datatables.css">
<link rel="stylesheet" type="text/css" href="theme/assets/css/jsgrid.css">
<!-- Plugins css Ends-->
<!-- Bootstrap css-->
<link rel="stylesheet" type="text/css" href="theme/assets/css/bootstrap.css">
<!-- App css-->
<link rel="stylesheet" type="text/css" href="theme/assets/css/style.css">
<link rel="stylesheet" type="text/css" href="theme/assets/css/main.css">
<link rel="stylesheet" type="text/css" href="theme/assets/css/scrollspyNav.css">
<link id="color" rel="stylesheet" href="theme/assets/css/color-1.css" media="screen">
<!-- Responsive css-->
<link rel="stylesheet" type="text/css" href="theme/assets/css/responsive.css">

<script src="theme/assets/js/jquery-3.5.1.min.js"></script>