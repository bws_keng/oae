
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.4/js/bootstrap-switch.js" data-turbolinks-track="true"></script>
<?php
 require dirname(__FILE__)."/../../extensions/booster/widgets/TbActiveForm.php";
 require dirname(__FILE__)."/../../extensions/booster/widgets/TbSwitch.php";
$formNameModel = 'PController';
?>
    <div class="container-fluid">
        <!-- start row -->
        <div class="row">
            <div class="col-lg-12 set-height set-padding">
                <div id="section1">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="mb-1">
                                <?php echo $formNameModel; ?>
                            </h5>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">

                                <?php $form = $this->beginWidget('TbActiveForm', array(
                                    'id' => 'pgroup-grid',
                                    //'type' => 'horizontal',
                                    'enableAjaxValidation' => true,
                                    'enableClientValidation' => true,
                                    'clientOptions' => array(
                                        //'validateOnSubmit' => true
                                    ),
                                    'htmlOptions' => array('class' => 'well'), 
                                )); ?>
                                <p class="help-block">Fields with <span class="required">*</span> are required.</p>

                                <?php echo $form->errorSummary($model); ?>
                                <div class="form-group">
                                    <div class="col-md-12 mb-3">
                                     <?php echo $form->textField($model, 'group_name', array('class' => 'form-control', 'placeholder' => 'กลุ่มผู้ใช้งาน')); ?>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="form-group">
                                    <label class="col-sm-3 control-label">Permission :</label>
                                    <div class="col-sm-9">
                                        <?php
                                        if ($pController) {

                                            foreach ($pController as $ckey => $controller) { 
                                                $action = CHtml::listData($controller->pAction, 'action', 'title');
                                                if (isset($model_p->permission)){
                                                    $permission = (array)json_decode($model_p->permission);
                                                    if(!empty($permission[$controller->controller])){
                                                    $model_p->action = $permission[$controller->controller]->action;
                                                    $model_p->active = $permission[$controller->controller]->active;
                                                    }
                                                }
                                              
                                                ?>
                                                <div class="entry input-group col-xs-12">
                                                    <div class="well bg-white">
                                                            <div class="form-group">
                                                                <label class="control-label col-xs-3">Controller</label>
                                                                <div class="col-xs-9">
                                                                    <?= $controller->title; ?>
                                                                </div>
                                                                <div class="col-xs-12">
                                                            
                                                                    <?php echo $form->checkboxListGroup(
                                                                        $model_p,
                                                                        '[' . $controller->controller . ']action',
                                                                        array(
                                                                            'widgetOptions' => array(
                                                                                'data' => $action,
                                                                            ),
                                                                            'wrapperHtmlOptions' => array(
                                                                                'class' => 'col-sm-5',
                                                                            ),
                                                                            'inline' => true,
                                                                        )
                                                                    ); 
                                                                    ?>
                                                                    <?php echo $form->switchGroup($model_p, '[' . $controller->controller . ']active',
                                                                        array(
                                                                            'wrapperHtmlOptions' => array(
                                                                                'class' => 'col-sm-5',
                                                                            ),
                                                                            'widgetOptions' => array(
                                                                                'events' => array(
                                                                                    'switchChange' => 'js:function(event, state) {
                                                      console.log(this); // DOM element
                                                      console.log(event); // jQuery event
                                                      console.log(state); // true | false
                                                    }'
                                                                                )
                                                                            )
                                                                        )
                                                                    ); ?>

                                                                </div>
                                                            </div>
                                                    </div>
                                                </div>
                                                <?php
                                            }
                                        }
                                        ?>
                                        <small>Press <span class="glyphicon glyphicon-plus gs"></span> to add another form field :)</small>
                                    </div>
                                </div>
                                </div>
                        </div>
                    </div>
                </div>

                <div class="row text-center group-submit">
                  <!--   <input type="submit" type="button" class="btn btn-primary btn-lg" value="บันทึกข้อมูล" />
                    <button type="button" class="btn btn-outline-secondary btn-lg">ยกเลิก</button> -->
                    <?php echo CHtml::tag('button',array('class' => 'btn btn-primary btn-lg btn-icon glyphicons ok_2'),'<i></i>บันทึกข้อมูล');?>
                    <button type="button" class="btn btn-outline-secondary btn-lgy" onclick="window.history.go(-1); return false;">ยกเลิก</button>
                </div>
            </div>
            <?php $this->endWidget(); ?>
        </div>
        <!-- end row -->

    </div>
<script type="text/javascript">
        function upload()
    {
        alert('โปรดรอสักครู่ ระบบกำลังประมวลผล')
        // swal({
        //     title: "โปรดรอสักครู่",
        //     text: "ระบบกำลังประมวลผล",
        //     type: "info",
        //     confirmButtonText: "ตกลง",
        // });
    }
    </script>