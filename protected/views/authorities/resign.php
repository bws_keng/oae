<?php
$this->breadcrumbs=array(
  // UserModule::t('Users')=>array('/user'),
  UserModule::t('Manage'),
);
$formNameModel = 'User';
$title = "จัดการเจ้าหน้าที่สำนักงานเขตลาออก";

Yii::app()->clientScript->registerScript('search', "
  $('#SearchFormAjax').submit(function(){
      $.fn.yiiGridView.update('$formNameModel-grid', {
          data: $(this).serialize()
      });
      return false;
  });
");

Yii::app()->clientScript->registerScript('updateGridView', <<<EOD
  $.updateGridView = function(gridID, name, value) {
      $("#"+gridID+" input[name*="+name+"], #"+gridID+" select[name*="+name+"]").val(value);
      $.fn.yiiGridView.update(gridID, {data: $.param(
          $("#"+gridID+" input, #"+gridID+" .filters select")
      )});
  }
  $.appendFilter = function(name, varName) {
      var val = eval("$."+varName);
      $("#$formNameModel-grid").append('<input type="hidden" name="'+name+'" value="">');
  }
  $.appendFilter("User[news_per_page]", "news_per_page");
EOD
, CClientScript::POS_READY);

?>
<div class="container-fluid">
  <!-- start row -->
  <div class="row">
    <div class="col-lg-12 set-height set-padding">
      <div id="section1">
        <div class="card">
          <div class="card-header">
            <h5 class="mb-1">
              <?php
                echo $title;
              ?>
            </h5>
          </div>
        </div>
        <div class="row justify-content-end">
          <div class="col-md-3">
            <a href="<?php echo Yii::app()->createUrl('authorities/create'); ?>">
              <div class="card o-hidden create-main">
               <!--  <div class="b-r-4 card-body">
                  <div class="media static-top-widget">
                    <div class="align-self-center text-center"><i data-feather="plus"></i></div>
                    <div class="media-body">
                      <h6 class="mb-0 counter">เพิ่มเจ้าหน้าที่สำนักเขต</h6><i class="icon-bg" data-feather="database"></i>
                    </div> 
                  </div>
                </div> -->
              </div>
            </a>
          </div>
        </div>
        <div class="card table-card">
          <div class="card-body">
            <div class="table-responsive">
              <?php $this->widget('AGridView', array(

                  'id'=>$formNameModel.'-grid',
                  'dataProvider'=>$model->searchResign(),
                  //'filter'=>$model,
                  'selectableRows' => 2,
                  //'rowCssClassExpression'=>'"items[]_{$data->id}"',
                  'htmlOptions' => array(
                    'style'=> "margin-top: -1px;width:100%;",
                  ),
                  'afterAjaxUpdate'=>'function(id, data){
                    $.appendFilter("User[news_per_page]");
                    InitialSortTable(); 
                    }',

                  'columns'=>array(
                    array(
                  'header'=>'No.',
                  'value'=>'$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
                ),
                array(
                  'name' => 'staff_id',
                  'header' => 'รหัสพนักงาน',
                  'type'=>'raw',
                  'value' => '$data->profile->staff_id',
                  'filterHtmlOptions'=>array('style'=>'width:100px'),
                ),
                array(
                      'header' => 'ชื่อ - นามสกุล',
                      'type'=>'html',
                      'value'=>function($data){
                          return $data->profile->firstname . ' ' . $data->profile->lastname;

                      }
                    ),

                    
                    //   array(
                    //   'header' => 'เลขประจำตัวบัตรประชาชน',
                    //   'name'=>'identification',
                    //   'type'=>'html',
                    //   'value'=>function($data){
                    //     return $data->identification;
                    //   },
                    //   'filterHtmlOptions'=>array('style'=>'width:30px'),
                    // ),
                    array(
                      'header'=>'Email',
                      'type'=>'raw',
                      'value'=>'CHtml::link(UHtml::markSearch($data,"email"), "mailto:".$data->email)',
                      'filterHtmlOptions'=>array('style'=>'width:30px'),
      
                    ),
                //     array(
                //   'header' => 'วันที่ลาออก',
                //   'type'=>'raw',
                //   'value'=>function($data){
                //     $LogResign = LogResign::model()->find(array(
                //       'select'=>'date_laid_off',
                //       'condition'=>'action_status="1" AND user_id="'.$data->id.'"',
                //       'order'=>'id DESC'
                //     ));

                //     // if($LogResign->date_laid_off != ""){
                //     //  $LogResign->date_laid_off = date("d/m/Y", strtotime($LogResign->date_laid_off));
                //     // }
                //     return $LogResign->date_laid_off;
                //   }
                // ),
                    
                   array(
                  'header'=>'ยกเลิกลาออก',
                  'type'=>'raw',
                  'htmlOptions' => array(
                    'style' => 'width:100px; text-align:center;',
                  ),
                  'value'=>function($data){

                    $text = '
                    <button class="btn btn-danger" title="ยกเลิกลาออก" onclick="resignuser('.$data->id.')" style="background: #b60000;">ยกเลิกลาออก</button>
                    ';


                    return $text;
                  },


                ),
                  ),
));


?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- end row -->
</div>


  <script type="text/javascript">




    function resignuser(user_id){

      if(confirm("คุณต้องการยกเลิกการลาออกของเจ้าหน้าที่ใช่หรือไม่")){

        if(user_id != ""){
          $.ajax({
            type: 'POST',
            url: "<?=Yii::app()->createUrl('Authorities/ResignUserAdd');?>",
            data:{ user_id:user_id},
            success: function(data) {
              // if(data == "success"){

              // }else{
              //   alert("ทำรายการไม่สำเร็จ กรุณาทำรายการใหม่");
              // }
              location.reload();
            }
          });
        }


      }else{
        console.log("no resign");
      }
    }


  </script>