<?php
class MasterData
{
    private $database = 'db_oae';
    private $ref = 'ref';
    private $field = "value";
    private $data;
    private $map = Array();

    function __construct($model, $where = null, $select = '*'){
        $command = Yii::app()->db->createCommand();

        $map = Yii::app()->db->createCommand('SELECT * FROM "'.schema_name.'"."masterdata"')->queryAll();
        // print_r($map);exit();
        foreach ($map as $key => $value) {
            $this->map[$value['name']] = Array(
                'table'=>$value['target_table'],
                'reference'=>$value['key'],
                'field'=>$value['target_field']
            );
        }

        // print_r($this->map);exit();

        // เช็คว่าใน Map ไม่มีข้อมูลนั้นๆ ใช่หรือไม่ ถ้าไม่มี ให้ Throw Exception
        if(!isset($this->map[$model])){
            throw new Exception("ข้อมูล master ไม่มีอยู่", 1);
        }
        $table = $this->map[$model]['table'];
        $this->ref = $this->map[$model]['reference'];
        $this->field = $this->map[$model]['field'];
        
        $sql = $command->select($select)->from($this->database.'.'.$table);
        if(isset($where) && $where != null){
            $sql= $sql->where($where);
        }
        $this->data = $sql->queryAll();
        
    }

    public function data(){
        $tmp = Array(
            "key"=>$this->ref,
            "value"=>$this->field,
            "data" => Array()
        );
        foreach ($this->data as $value) {
            $tmp['data'][$value[$this->ref]] = $value;
        }
        return $tmp;
    }
}


?>