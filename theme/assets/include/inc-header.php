<div class="page-main-header oae-header">
    <div class="main-header-right row m-0">
        <form class="form-inline search-full" action="#" method="get">
            <div class="form-group w-100">
                <div class="Typeahead Typeahead--twitterUsers">
                    <div class="u-posRelative">
                        <input class="demo-input Typeahead-input form-control-plaintext w-100" type="text" placeholder="Search Cuba .." name="q" title="" autofocus>
                        <div class="spinner-border Typeahead-spinner" role="status"><span class="sr-only">Loading...</span></div><i class="close-search" data-feather="x"></i>
                    </div>
                    <div class="Typeahead-menu"></div>
                </div>
            </div>
        </form>
        <div class="main-header-left logo-main">
            <div class="logo-wrapper"><a href="#"><img class="img-fluid" src="theme/assets/images/logo/logo-new.png" alt=""></a></div>
            <div class="toggle-sidebar"><i class="status_toggle middle" data-feather="grid" id="sidebar-toggle"> </i></div>
        </div>
        <div class="left-menu-header col horizontal-wrapper pl-0 logo-main">
            <a href="#"><img class="img-fluid" src="theme/assets/images/logo/logo-new.png" alt=""></a>
        </div>
        <div class="nav-right col-8 pull-right right-menu">
            <ul class="nav-menus">
              <!--  <li class="maximize"><a class="text-dark" href="#!" onclick="javascript:toggleFullScreen()"><i data-feather="maximize"></i></a></li> -->
                <li class="profile-nav onhover-dropdown p-0">
                    <div class="media profile-media">
                        <img class="b-r-10" src="theme/assets/images/dashboard/profile.jpg" alt="">
                        <div class="media-body"><span>เจ้าหน้าที่สำนักงานเขต</span>
                            <p class="mb-0 font-roboto">Admin <i class="middle fa fa-angle-down"></i></p>
                        </div>
                    </div>
                    <ul class="profile-dropdown onhover-show-div">
                        <li><i data-feather="settings"></i><span>Settings</span></li>
                        <li><!-- <a href="login.php"><i data-feather="log-in"> </i><span>Logout</span></a> --><?php echo CHtml::link(UserModule::t("Logout"), array('//user/logout'),array('class'=>'btn btn-default btn-mini pull-right'));?></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>

</div>