<?php

class RepasswordController extends Controller
{
	
	
	public function actionRepassword() {

        if (Users::model()->findbyPk(Yii::app()->user->id)->repass_status=='0'){
            $model = new Users();
            
            if (isset($_POST['Users'])) {

                $model = Users::model()->findbyattributes(array('id'=>Yii::app()->user->id));

       
                $model->password = $_POST['Users']['password'];
                $model->verifyPassword = $_POST['Users']['verifyPassword'];
                if ($model->validate()) {
					
                    $model->password = UserModule::encrypting($model->password);
                    $model->verifyPassword = UserModule::encrypting($model->verifyPassword);
                    $model->repass_status = 1;

                    if ($model->save(false)) {

                        $status = "เปลี่ยนรหัสผ่านสำเร็จ";
                        $type_status = "success";
                    } else {
                        $status = "เปลี่ยนรหัสผ่าน ไม่สำเร็จ";
                        $type_status = "error";
                    }
                    //$this->redirect(array('site/index','status' => $status,'type_status'=> $type_status));
                    $this->redirect(Yii::app()->controller->module->returnUrl);
                }
            }

            $this->render('repassword',array('model'=>$model));
        }else {
            $this->redirect(Yii::app()->controller->module->returnUrl);
        }
    }
}
