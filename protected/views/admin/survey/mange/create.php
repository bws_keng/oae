<form class="container-fluid" method="POST" action="<?php echo Yii::app()->baseurl.'/admin/survey/'.$_GET['id'].'/mange/insert'; ?>">
    <div class="container-fluid">

        <!-- start row -->
        <div class="row">
            <div class="col-lg-12 set-height set-padding">
                <div id="section1">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="mb-1">เพิ่มข้อมูลจัดการข้อมูลแสดงผล</h5>
                        </div>
                    </div>

                    <div class="card table-card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <div class="form-group">
                                        <label for="">ปี พ.ศ. แบบสอบถาม</label>
                                        <select class="form-control digits " id="" name="survey" required>
                                            <option selected disabled></option>
                                            <?php
                                            $survey = QuestionnaireModel::surveyList();
                                        foreach($survey as $key => $row){
                                            echo '<option value="'.$row['id'].'">'.$row['year'].' - '.$row['name'].'</option>';
                                        }
                                        ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-12 mb-3">
                                    <label for="">ฟอร์มคำถามจัดการข้อมูลแสดงผล</label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-7 m-t-15 mb-0">
                                    <div class="checkbox checkbox-solid-white">
                                        <input id="all" type="checkbox" >
                                        <label for="all">ทั้งหมด</label>
                                    </div>
                                </div>
                                <div class="form-group col-md-5 m-t-15 mb-0">
                                    <label for="all">ลำดับ</label>
                                </div>
                            </div>
                            <div class="row">
                                
                            </div>
                            <?php
                        foreach($questions as $key => $row){
                            echo '<div class="row">
                                <div class="form-group col-md-7 m-t-15 mb-0">
                                    <div class="checkbox checkbox-solid-white">
                                        <input id="solid-'.$key.'" type="checkbox" class="checkboxmange" name="question-'.$row['question_id'].'" value="1">
                                        <label for="solid-'.$key.'">'.$row['question'].'</label>
                                    </div>
                                </div>
                                <div class="form-group col-md-5 m-t-12 mb-0">
                                    <input class="form-control" id="form-'.$key.'" type="text" name="form-'.$row['question_id'].'" disabled >
                                </div>
                            </div>
                        ';
                        }
                    ?>
                        </div>


                    </div>

                </div>

            </div>
        </div>
        <!-- end row -->
        <div class="row text-center group-submit">
            <input type="submit" class="btn btn-primary btn-lg" value="บันทึกข้อมูล">
            <button type="button" class="btn btn-outline-secondary btn-lg" onclick="window.history.go(-1); return false;">ยกเลิก</button>
        </div>
    </div>
</form>
<script type="text/javascript">
    $(document).ready(function(){
        $('.checkboxmange').click(function() {
            var question  = $(this).attr("id");
            var checked =  document.getElementById(question).checked;
            if (checked === true) {
                if (question) {
                    var res = question.substr(6);
                    $('#form-'+res+'').removeAttr('disabled');
                    $('#form-'+res+'').prop('required', true);
                } 
            }else{
              var res = question.substr(6);
              $('#form-'+res+'').prop('disabled', true);
              $('#form-'+res+'').removeAttr('required');
          }           
      });
    });
   
</script>