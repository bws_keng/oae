<?php

class MasterRelationController extends Controller
{
	public $layout='admin';
    public function actionIndex()
    {
        $model = new MasterRelation;
		$criteria = new CDbCriteria;
		$criteria->compare('active', 1);
		$criteria->order = 'relation_name DESC';
		$data = MasterRelation::model()->findAll($criteria);
        $this->render('index', Array(
			'data' => $data,
			'model' => $model,
		));
    }

    public function actionCreate()
    {
        $model = new MasterRelation;
		if(isset($_POST['MasterRelation']))
    	{
			$model->attributes = $_POST['MasterRelation'];
			$model->active = 1;
			if($model->validate()){
				if($model->save()){
					$this->redirect(array('view', 'id' => $model->id));
				}
			}
		}
		$this->render('create', Array(
			'model' => $model
		));
    }

    public function actionView($id)
    {
        $this->render('view',array(
    		'model'=>$this->loadModel($id),
    	));
    }

    public function actionEdit($id)
	{
		$model=$this->loadModel($id);

		if(isset($_POST['MasterRelation']))
    	{
			$model->attributes = $_POST['MasterRelation'];

			if($model->validate()){
				if($model->save()){
					$this->redirect(array('view', 'id' => $model->id));
				}
			}
		}
		$this->render('update', Array(
			'model' => $model
		));
	}

    public function actionDelete($id)
	{
		$model = $this->loadModel($id);
		$model->active = 0;

		if($model->save())
		{
			$this->redirect(array('index'));
		}
	}

    public function loadModel($id)
	{
		$model = MasterRelation::model()->findByPk($id);
		return $model;
	}
}

?>