<?php

// Yii::import('zii.widgets.CWidget');

class UserMenuAdmin extends CWidget
{
	protected $items;

	public function init()
	{
		$this->items = FormModel::list();
		parent::init();
	}

	public function run()
	{
		$this->render('UserMenuAdmin');
	}

	public function renderList()
	{
		$this->renderMenuRecursive($this->items);
	}

	protected function renderMenuRecursive($items)
	{
		$menus = Array();
		foreach ($items as $item){
			$menu = explode('.', $item['question_group_number']);
			if(!isset($menus[$menu[0]])){
				$menus[$menu[0]] = Array();
			}
			array_push($menus[$menu[0]], $item);
			
		}
		foreach ($menus as $key=>$values){
			if(count($values) > 1){
				echo '<li><a class="submenu-title" href="#">หมวดที่ 2<span class="sub-arrow"><i class="fa fa-angle-right"></i></span><div class="according-menu"><i class="fa fa-angle-right"></i></div></a>
					<ul class="nav-sub-childmenu submenu-content" style="display: none;">';
			}
			foreach($values as $value){
				echo '<li>';
				if(isset($value['question_group_number']) && $value['question_group_id'] != null){
					if ($value['question_group_id'] === 12) {
						echo '<a href="'.Yii::app()->baseurl.'/survey/'.$value['question_group_id'].'/rate">หมวด '.$value['question_group_number'].'</a>';
					}else{
						echo '<a href="'.Yii::app()->baseurl.'/survey/'.$value['question_group_id'].'">หมวด '.$value['question_group_number'].'</a>';
					}
				}
				echo '</li>';
			}
			if(count($values) > 1){
				echo '</ul>
				</li>';
			}
			
		}
		// print_r($menus);exit();
	}

	public function renderAuthority()
	{
		$this->renderMenuAuthority($this->items);
	}

	protected function renderMenuAuthority($items)
	{
		//var_dump($items);exit();
		// foreach ($items as $item){

		// 	echo CHtml::openTag('li');
		// 	if(isset($item['question_group_id']) && $item['question_group_id'] != null){
		// 		echo '
		// 		<li><a class="submenu-title " href="#">หมวดที่ '.$item['question_group_number'].'<span class="sub-arrow"><i class="fa fa-angle-right"></i></span></a>
		// 			<ul class="nav-sub-childmenu submenu-content">
		// 				<li>'.CHtml::link('จัดการคำถาม',array('admin/survey/'.$item['question_group_id'].'/question')).'</li>
		// 				<li>'.CHtml::link('จัดการคำตอบ',array('admin/survey/'.$item['question_group_id'].'/answer')).'</li>
		// 				<li>'.CHtml::link('จัดการหมวดหมู่',array('admin/survey/'.$item['question_group_id'].'/group')).'</li>
		// 			</ul>
		// 		</li>'
		// 		;
				
		// 	}
		// 	echo CHtml::closeTag('li');
		// }
	}
}
