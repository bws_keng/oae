<!-- <style type="text/css">
    .dropdown-menu {
    display: none;
    position: absolute;
    background-color: #f1f1f1;
    min-width: 160px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);

    ul {
      overflow-y: visible !important;
    }
    li {
      overflow-y: visible !important;
    }
    li:hover {
      cursor: pointer;
      cursor: hand;
    }
  }
  .dropdown-menu a {
    color: black;
    padding: 12px 16px;
    text-decoration: none;
    display: block;
  }

  .dropdown-menu a:hover {background-color: #ddd;}

  .dropdown:hover .dropdown-menu {display: block;}

.title-element-name {

    padding: .75rem;
}
.btn-sm {
    font-size: 14px;
}

.table-responsive {
  z-index: 999;
  overflow-y: auto !important;
}
  </style> -->
  <link rel="stylesheet" href="http://cdn.datatables.net/1.10.2/css/jquery.dataTables.min.css"></style>
<script type="text/javascript" src="http://cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js"></script>
<style type="text/css">
  .dropbtn {
    background-color: #8DBF42;
    color: white;
    padding: 16px;
    font-size: 16px;
    border: none;
  }

  .dropdown {
    position: relative;
    display: inline-block;
  }

  .dropdown-content {
    display: none;
    position: absolute;
    background-color: #f1f1f1;
    min-width: 160px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
  }

  .dropdown-content a {
    color: black;
    padding: 12px 16px;
    text-decoration: none;
    display: block;
  }

  .dropdown-content a:hover {background-color: #ddd;}

  .dropdown:hover .dropdown-content {display: block;}

  .dropdown:hover .dropbtn {background-color: #3e8e41;}
  
</style>

            <!--            <div class="table-responsive" > -->
                                            <table class="table table-striped table-main" id="myTable" >
                                                <!-- <table class="table table-striped table-main" id="basic-2"> -->
                                                <thead>
                                                    <tr>
                                                        <th width="10%">ลำดับ</th>
                                                        <th>ปีเพาะปลูก</th>
                                                        <th width="50%">รหัสจังหวัด</th>
                                                        <th width="50%">จังหวัด</th>
                                                        <th width="50%">รหัสอำเภอ</th>
                                                        <th width="50%">อำเภอ</th>
                                                        <th width="50%">รหัสตำบล</th>
                                                        <th width="50%">ตำบล</th>
                                                        <th width="50%">รหัสหมู่ที่</th>
                                                        <th width="50%">หมู่ที่</th>
                                                        <th width="50%">พวกที่</th>
                                                        <th width="50%">ตัวอย่างที่</th>
                                                        <th width="50%">เพิ่มหมวด</th>
                                                        <th width="50%">แผนที่</th>
                                                        <th width="25%">จัดการข้อมูล</th>
                                                    </tr>
                                                </thead>
                                                <tbody >
                                                    <?php
                                                    $i = 1;
                                                    foreach($data as $row){
                                                     
                                                        $year = $row['year'] + 543;
                                                        $survey_time = explode("-", $row['survey_time']);
                                                        $survey_0 = $survey_time[0] + 543;
                                                        $survey_time = $survey_time[2]."-".$survey_time[1]."-".$survey_0;

                                                        // $criteria=new CDbCriteria;
                                                        // $criteria->compare('province_id',$row['id']);
                                                        // $criteria->compare('district_code',$row["district"]);
                                                        // $criteria->compare('active',1);
                                                        // $District = MtDistrict::model()->find($criteria); 

                                                        // $criteria=new CDbCriteria;
                                                        // $criteria->compare('district_id',$District->id);
                                                        // $criteria->compare('subdistrict_code',$row["subdistrict"]);
                                                        // $criteria->compare('active',1);
                                                        // $SubDistrict = MtSubDistrict::model()->find($criteria);

                                                        // $criteria=new CDbCriteria;
                                                        // $criteria->compare('subdistrict_id',$SubDistrict->id);
                                                        // $criteria->compare('village_code',$row["village"]);
                                                        // $criteria->compare('active',1);
                                                        // $Village = masterVillage::model()->find($criteria);  

                                                        $criteria=new CDbCriteria;
                                                        $criteria->compare('those_id',$row["group"]);
                                                        $criteria->compare('active',1);
                                                        $Those = MasterThose::model()->find($criteria);

                                                        // $VillageName = !empty($Village) ? $Village->village_name : null ;
                                                        // $VillageCode = !empty($Village) ? $Village->village_code : null ;
                                                        // $ThoseName = !empty($Those) ? $Those->those_name : null ;

                                                        $questionnaire_id = $row['questionnaire_id'];
                                                        $questionnaire_format = $questionnaire_id.'.jpg';
                                                        $file_path = $_SERVER['DOCUMENT_ROOT'].Yii::app()->baseurl.'/uploads/'.$questionnaire_format;
                                                        $fileMap = "";
                                                        if (file_exists($file_path)) {
                                                           //$fileMap = '<a target="_blank" href="'.Yii::app()->baseurl.'/uploads/'.$questionnaire_format.'">แผนที่</a>';
                                                          $fileMap = '<a type="button" class="btn btn-icon text-primary" href="'.Yii::app()->baseurl.'/questionnaire/'.$row['questionnaire_id'].'/loadmap">แผนที่</a>';
                                                        }else{
                                                           $fileMap = 'ไม่พบไฟล์';
                                                        }
                                                        

                                                        echo '<tr>
                                                            <td>'.$i++.'</td>
                                                            <td>'.$year.'/63</td>
                                                            <td>'.$row['province_code'].'</td>
                                                            <td>'.$row['province_name_th'].'</td>
                                                            <td>'.$row['district_code'].'</td>
                                                            <td>'.$row['district_name_th'].'</td>
                                                            <td>'.$row['subdistrict_code'].'</td>
                                                            <td>'.$row['subdistrict_name_th'].'</td>
                                                            <td>'.$row['village_code'].'</td>
                                                            <td>'.$row['village_name'].'</td>
                                                            <td>'.(!empty($Those) ? $Those->those_name : null ).'</td>
                                                            <td>'.$row['sample'].'</td>';
                                                        // echo '<td> <div class="btn-group">
                                                        //     <button type="button" class="btn btn-default btn-transparent btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        //       <span class="title-element-name">เลือกหมวด </span><span class="caret"></span>
                                                        //     </button>
                                                        //     <ul class="dropdown-menu">
                                                        //       <li><a target="_blank" href="'.Yii::app()->baseurl.'/survey/1/create?Questionnaire='.$row['questionnaire_id'].'">หมวด 1</a></li>
                                                        //       <li><a target="_blank" href="'.Yii::app()->baseurl.'/survey/2/create?Questionnaire='.$row['questionnaire_id'].'">หมวด 2.1</a></li>
                                                        //       <li><a target="_blank" href="'.Yii::app()->baseurl.'/survey/3/create?Questionnaire='.$row['questionnaire_id'].'">หมวด 2.2</a></li>
                                                        //       <li><a target="_blank" href="'.Yii::app()->baseurl.'/survey/4/create?Questionnaire='.$row['questionnaire_id'].'">หมวด 3</a></li>
                                                        //       <li><a target="_blank" href="'.Yii::app()->baseurl.'/survey/5/create?Questionnaire='.$row['questionnaire_id'].'">หมวด 4</a></li>
                                                        //       <li><a target="_blank" href="'.Yii::app()->baseurl.'/survey/6/create?Questionnaire='.$row['questionnaire_id'].'">หมวด 5</a></li>
                                                        //       <li><a target="_blank" href="'.Yii::app()->baseurl.'/survey/7/create?Questionnaire='.$row['questionnaire_id'].'">หมวด 6</a></li>
                                                        //       <li><a target="_blank" href="'.Yii::app()->baseurl.'/survey/8/create?Questionnaire='.$row['questionnaire_id'].'">หมวด 7</a></li>
                                                        //       <li><a target="_blank" href="'.Yii::app()->baseurl.'/survey/9/create?Questionnaire='.$row['questionnaire_id'].'">หมวด 8</a></li>
                                                        //       <li><a target="_blank" href="'.Yii::app()->baseurl.'/survey/10/create?Questionnaire='.$row['questionnaire_id'].'">หมวด 9</a></li>
                                                        //       <li><a target="_blank" href="'.Yii::app()->baseurl.'/survey/11/create?Questionnaire='.$row['questionnaire_id'].'">หมวด 10</a></li>
                                                        //       <li><a target="_blank" href="'.Yii::app()->baseurl.'/survey/12/create?Questionnaire='.$row['questionnaire_id'].'">หมวด 11</a></li>
                                                        //     </ul>
                                                        //   </div></td>';
                                                        echo '<td>
                                                                <div class="dropdown">
                                                                  <button class="dropbtn">เลือกหมวด</button>
                                                                  <div class="dropdown-content">
                                                                    <a target="_blank" href="'.Yii::app()->baseurl.'/survey/1/create?Questionnaire='.$row['questionnaire_id'].'">หมวด 1</a>
                                                                    <a target="_blank" href="'.Yii::app()->baseurl.'/survey/2/create?Questionnaire='.$row['questionnaire_id'].'">หมวด 2.1</a>
                                                                    <a target="_blank" href="'.Yii::app()->baseurl.'/survey/3/create?Questionnaire='.$row['questionnaire_id'].'">หมวด 2.2</a>
                                                                    <a target="_blank" href="'.Yii::app()->baseurl.'/survey/4/create?Questionnaire='.$row['questionnaire_id'].'">หมวด 3</a>
                                                                    <a target="_blank" href="'.Yii::app()->baseurl.'/survey/5/create?Questionnaire='.$row['questionnaire_id'].'">หมวด 4</a>
                                                                    <a target="_blank" href="'.Yii::app()->baseurl.'/survey/6/create?Questionnaire='.$row['questionnaire_id'].'">หมวด 5</a>
                                                                    <a target="_blank" href="'.Yii::app()->baseurl.'/survey/7/create?Questionnaire='.$row['questionnaire_id'].'">หมวด 6</a>
                                                                    <a target="_blank" href="'.Yii::app()->baseurl.'/survey/8/create?Questionnaire='.$row['questionnaire_id'].'">หมวด 7</a>
                                                                    <a target="_blank" href="'.Yii::app()->baseurl.'/survey/9/create?Questionnaire='.$row['questionnaire_id'].'">หมวด 8</a>
                                                                    <a target="_blank" href="'.Yii::app()->baseurl.'/survey/10/create?Questionnaire='.$row['questionnaire_id'].'">หมวด 9</a>
                                                                    <a target="_blank" href="'.Yii::app()->baseurl.'/survey/11/create?Questionnaire='.$row['questionnaire_id'].'">หมวด 10</a>
                                                                    <a target="_blank" href="'.Yii::app()->baseurl.'/survey/12/create?Questionnaire='.$row['questionnaire_id'].'">หมวด 11</a>
                                                                  </div>
                                                                </div>
                                                            </td>';
                                                          echo '<td>'.$fileMap.'
                                                          </td>
                                                            <td>
                                                            <a type="button" class="btn btn-icon text-primary" href="'.Yii::app()->baseurl.'/questionnaire/'.$row['questionnaire_id'].'/show"><i data-feather="eye"></i></a>
                                                            <a type="button" class="btn btn-icon text-success" href="'.Yii::app()->baseurl.'/questionnaire/'.$row['questionnaire_id'].'/edit"><i data-feather="edit"></i></a>
                                                            <button type="button" class="btn btn-icon text-danger" onclick="sendDelete('.$row['questionnaire_id'].', \''.$row['staff_name'].' '.$row['staff_lname'].' เมื่อวันที่ '.$row['survey_time'].'\')"><i data-feather="trash"></i></button>
                                                            </td>
                                                            
                                                        </tr>';
                                                    }
                                                    ?>
                                                </tbody>
                                            </table>
                            <!--             </div> -->
                         <script type="text/javascript">
                           $(document).ready(function(){
                                $('#myTable').dataTable({
                                  responsive: true,
                                  "pageLength": 10,
                                  "searching": false,
                                  "info": false,
                                  "lengthChange": false

                                });
                                   feather.replace();
                                });
                         </script>
