<?php

/**
 * This is the model class for table "{{users}}".
 *
 * The followings are the available columns in table '{{users}}':
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property string $email
 * @property string $pic_user
 * @property string $orgchart_lv2
 * @property string $department_id
 * @property string $activkey
 * @property string $create_at
 * @property string $lastvisit_at
 * @property integer $superuser
 * @property integer $status
 * @property integer $online_status
 * @property integer $online_user
 * @property string $last_ip
 * @property string $last_activity
 * @property integer $lastactivity
 * @property string $avatar
 * @property string $company_id
 * @property string $division_id
 * @property string $position_id
 * @property string $bookkeeper_id
 * @property string $pic_cardid
 * @property integer $auditor_id
 * @property string $pic_cardid2
 * @property string $group
 * @property string $identification
 *
 * The followings are the available model relations:
 * @property Profiles $profiles
 */
class UsersAdmin extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public $repeat_password;
	public $update_password;
	public $repeat_updatepassword;
	public $search_username;
	public $id_passport;
	public $course_id;
	public $news_per_page;
	public $name_search;
	public function tableName()
	{
		return 'db_oae.t_user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('username, password, email, create_at', 'required'),
			array('superuser, status', 'numerical', 'integerOnly'=>true),
			array('username', 'length', 'max'=>20),
			array('password, email, activkey', 'length', 'max'=>128),
			array('pic_user', 'length', 'max'=>255),
			array('lastvisit_at,name_search', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, username, password, email, pic_user, activkey, create_at, lastvisit_at, superuser, status,name_search', 'safe', 'on'=>'search'),
		);
	}

	// protected function afterSave()
	// {
	// 	if(Yii::app()->user->id){
	// 		Helpers::lib()->getControllerActionId();
	// 	}
	// 	parent::afterSave();
	// }

	public function validateIdCard($attribute,$params)
	{
		$str = $this->username;
		$chk = strlen($str);
		if($chk =="13"){
	        $id = str_split(str_replace('-', '', $this->username)); //ตัดรูปแบบและเอา ตัวอักษร ไปแยกเป็น array $id
	        $sum = 0;
	        $total = 0;
	        $digi = 13;
	        for ($i = 0; $i < 12; $i++) {
	        	$sum = $sum + (intval($id[$i]) * $digi);
	        	$digi--;
	        }
	        $total = (11 - ($sum % 11)) % 10;
	        if ($total != $id[12]) { //ตัวที่ 13 มีค่าไม่เท่ากับผลรวมจากการคำนวณ ให้ add error
	        	$this->addError('username', 'หมายเลขบัตรประชาชนไม่ถูกต้อง');
	        }
	    }
	}
	public function validateIdCard2($attribute,$params)
	{
		$str = $this->identification;
		$chk = strlen($str);
		if($chk == "13"){
	        $id = str_split(str_replace('-', '', $this->identification)); //ตัดรูปแบบและเอา ตัวอักษร ไปแยกเป็น array $id
	        $sum = 0;
	        $total = 0;
	        $digi = 13;
	        for ($i = 0; $i < 12; $i++) {
	        	$sum = $sum + (intval($id[$i]) * $digi);
	        	$digi--;
	        }
	        $total = (11 - ($sum % 11)) % 10;
	        if ($total != $id[12]) { //ตัวที่ 13 มีค่าไม่เท่ากับผลรวมจากการคำนวณ ให้ add error
	        	$this->addError('identification', 'หมายเลขบัตรประชาชนไม่ถูกต้อง');
	        }
	    }
	}
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			// 'member' => array(self::HAS_ONE, 'MMember', array('m_id' => 'id')),
			'profiles' => array(self::HAS_ONE, 'Profile', array('user_id' => 'id')),

			);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'username' => 'Username',
			'password' => 'Password',
			'email' => 'Email',
			'pic_user' => 'Pic User',
			'department_id' => 'Department',
			'activkey' => 'Activkey',
			'create_at' => 'Create At',
			'lastvisit_at' => 'Lastvisit At',
			'superuser' => 'Superuser',
			'status' => 'Status',
			'name_search'=>'ชื่อ - นามสกุล(ไทย-Eng), บัตรประชาชน, พาสปอร์ต',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->order = 'create_at DESC';
		$criteria->compare('del_status',0);
		$criteria->with = array('profiles');
		$criteria->compare('id',$this->id);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('activkey',$this->activkey,true);
		$criteria->compare('create_at',$this->create_at,true);
		$criteria->compare('lastvisit_at',$this->lastvisit_at,true);
		$criteria->compare('superuser',$this->superuser);
		$criteria->compare('status',$this->status);
		// $criteria->compare(
  //           'CONCAT(profiles.firstname," "
  //           ,profiles.lastname," ",profiles.firstname," "
  //           ," ",profiles.identification)',$this->name_search,true);

		$poviderArray = array('criteria' => $criteria);

		if (isset($this->news_per_page)) {
			$poviderArray['pagination'] = array('pageSize' => intval($this->news_per_page));
		} else {
			$poviderArray['pagination'] = array('pageSize' => intval(20));
		}

		return new CActiveDataProvider($this, $poviderArray);
	}


	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Users the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
