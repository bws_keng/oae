<form class="container-fluid" method="POST" action="<?php echo Yii::app()->baseurl.'/admin/questionaire/question'; ?>">
    <script type="text/javascript">

        function selectMasterData() {
            const selectBox = document.getElementById("selectType");
            const selectedValue = selectBox.options[selectBox.selectedIndex].value;
            const targetBlock = document.getElementById("selectDataBlock");
            const targetForm = document.getElementById("selectData");
            if(selectedValue == 'master'){
                targetBlock.style.display = "flex";
                
            }else{
                targetBlock.style.display = "none";
            }
        }

    </script>
    <!-- start row -->
    <div class="row">
        <div class="col-lg-12 set-height set-padding">
            <div id="section1">
                <div class="card">
                    <div class="card-header">
                        <h5 class="mb-1">เพิ่มคำถาม</h5>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <div>
                        <div class="row">
                                <div class="col-md-12 mb-3">
                                    <label>รหัสคำถาม</label>
                                    <input class="form-control" type="number" name="question_id" value="<?php echo $lastid+1; ?>" readonly>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 mb-3">
                                    <label class="col-12">ชื่อคำถาม</label>
                                    <div class="row col-12">
                                        <input class="form-control col-9" type="text" name="title">
                                        <button type="button" class="btn col-1" onclick="$(`input[name='title']`).val(() => { return $(`input[name='title']`).val() + '<b></b>'})"><B>B</B></button>
                                        <button type="button" class="btn col-1" onclick="$(`input[name='title']`).val(() => { return $(`input[name='title']`).val() + '<i></i>'})"><i>i</i></button>
                                        <button type="button" class="btn col-1" onclick="$(`input[name='title']`).val(() => { return $(`input[name='title']`).val() + '<red></red>'})"><red>A</red></button>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 mb-3">
                                    <label>คำอธิบาย</label>
                                    <div class="row col-12">
                                        <input class="form-control col-9" type="text" name="subtitle">
                                        <button type="button" class="btn col-1" onclick="$(`input[name='subtitle']`).val(() => { return $(`input[name='subtitle']`).val() + '<b></b>'})"><B>B</B></button>
                                        <button type="button" class="btn col-1" onclick="$(`input[name='subtitle']`).val(() => { return $(`input[name='subtitle']`).val() + '<i></i>'})"><i>i</i></button>
                                        <button type="button" class="btn col-1" onclick="$(`input[name='subtitle']`).val(() => { return $(`input[name='subtitle']`).val() + '<red></red>'})"><red>A</red></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row text-center group-submit">
                <input type="submit" class="btn btn-primary btn-lg" value="บันทึกข้อมูล" />
                <button type="button" class="btn btn-outline-secondary btn-lg" onclick="window.history.go(-1); return false;">ยกเลิก</button>
            </div>
        </div>
    </div>
    <!-- end row -->
</form>