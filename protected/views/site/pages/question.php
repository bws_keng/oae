 
 
<div class="container-fluid">

<!-- <div id="myScrollspy" class="custom-scrollbar nav sidenav">
    <div class="sidenav-content">
        <a href="#section1" data-original-title="" title="" class="nav-link active">ข้อมูลเบื้องต้น</a>
        <a href="#section2" data-original-title="" title="" class="nav-link">การถือครองที่ดิน</a>
    </div>
</div> -->

<!-- start row -->
<div class="row">
    <div class="col-lg-12 set-height set-padding">
        <div id="section1">
            <div class="card">
                <div class="card-header">
                    <h5 class="mb-1">หมวดที่ 1 การถือครองและการใช้ประโยชน์ที่ดิน (ปี พ.ศ.2562 - พ.ศ.2563)</h5>
                </div>
            </div>
            <div class="card">
                <div class="card-body">

                    <form>
                        <div class="row">
                            <div class="col-md-2 mb-3">
                                <label for="">จังหวัด</label>
                                <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                            </div>
                            <div class="col-md-2 mb-3">
                                <label for="">อำเภอ</label>
                                <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                            </div>
                            <div class="col-md-2 mb-3">
                                <label for="">ตำบล</label>
                                <div class="input-group">
                                    <input class="form-control" id="" type="text" placeholder="" aria-describedby="" required="" data-original-title="" title="">
                                </div>
                            </div>
                            <div class="col-md-2 mb-3">
                                <label for="">หมู่ที่</label>
                                <div class="input-group">
                                    <input class="form-control" id="" type="text" placeholder="" aria-describedby="" required="" data-original-title="" title="">
                                </div>
                            </div>
                            <div class="col-md-2 mb-3">
                                <label for="">พวกที่</label>
                                <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                            </div>
                            <div class="col-md-2 mb-3">
                                <label for="">ตัวอย่างที่</label>
                                <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>

        <div id="section2">

            <h5 class="mb-4"> การถือครองที่ดินของครัวเรือนเกษตร <span class="text-danger">(ต้นปีเพาะปลูก)</span>
                <button class="btn btn-lg btn-secondary btn-note float-right" type="button" data-toggle="modal" data-target=".bd-example-modal-lg"><i class="fa fa-info-circle"></i> &nbsp;ข้อควรระวัง</button>
                <div class="modal fade bd-example-modal-lg modal-main" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h6 class="modal-title" id="myLargeModalLabel">ข้อควรระวัง : ข้อมูลที่ดินแต่ละแปลงจะมีแค่ 1 แถว เท่านั้น ห้ามเกิน</h6>
                                <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="card">
                                            <div class="card-header">
                                                <label for="">(4) การถือครองที่ดิน</label>
                                            </div>
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <small for="">1. ที่เป็นของตนเองที่มีกรรมสิทธิ์ครอบครองเป็นเจ้าของ
                                                            เช่น โฉนดที่ดิน น.ส.4 น.ส.3 น.ส.3ก ส.ค.1 น.ส.2 น.ส.5 หรือใบเหยียบย่ำ</small>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <small for="">2. ที่มีสิทธิ์เข้าทำประโยชน์ แต่ไม่มีกรรมสิทธิ์ครอบครอง
                                                            เป็นเจ้าของ เช่น น.ค.3 ส.ท.ก. ภ.บ.ท.5-6 ส.ป.ก.4-01 หรือที่ดินในเขต ส.ป.ก.</small>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <small for="">3. ที่เช่าผู่อื่น (มีกรรมสิทธิ์ครอบครองและจ่ายค่าเช่าเป็นเงินสดผลผลิต/อื่นๆ)</small>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <small for="">4. ที่ได้ทำฟรี (ไม่มีกรรมสิทธิ์ครอบครองและไม่ได้จ่ายค่าเช่า)</small>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <small for="">5. ที่รับจำนองรับขายฝากผู้อื่น <span class="text-danger">* ที่ครัวเรือนนำมาใช้ประโยชน์เอง</span></small>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="card">
                                            <div class="card-header">
                                                <label for="">(5) การจำนอง / ฝากขาย</label>
                                            </div>
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <small for="">1. จำนอง/ฝากขายผู้อื่น <span class="text-danger">[คำตอบในสดมภ์ที่ (5)=1 ได้ก็ต่อเมื่อ (4)-1 หรือ 2</span> </small>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <small for="">2. รับจำนอง/รับฝากขายผู้อื่น <span class="text-danger">[คำตอบในสดมภ์ที่ (5)=2 ได้ก็ต่อเมื่อ (4)-5</span></small>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <small for="">3. ไม่มีการจำนอง/ขายฝาก</small>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="card">
                                            <div class="card-header">
                                                <label for="">(6) การใช้ประโยชน์ที่ดิน</label>
                                            </div>
                                            <div class="card-body">
                                                <div class="row">

                                                    <label class="mb-2 col-md-6"><u>ที่ดินอื่นๆ</u></label>

                                                    <label class="mb-2 col-md-6"><u>ที่ดินอื่นๆ</u></label>

                                                    <div class="col-md-6">
                                                        <small for="">1. ที่นา</small>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <small for="">10. ที่ปลูกบ้านเรือนอยู่อาศัย</small>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <small for="">2. ที่พืชไร่</small>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <small for="">11. ที่รกร้างว่างเปล่าที่ถือครอง</small>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <small for="">3. ที่สวนผลไม้/ไม้ยืนต้น</small>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <small for="">12. ที่ห้วย/หนอง/คลอง/บึงสระที่ถือครอง</small>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <small for="">4. ที่สวนผัก/สมุนไพร</small>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <small for="">13. ที่ป่าถือครอง</small>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <small for="">5. ที่ไม้ดอก/ไม้ประดับ</small>
                                                    </div>

                                                    <label class="mt-2 mb-2 col-md-6"><u>ที่ดินเกษตรที่ใช้ประโยชน์ทางปศุสัตว์</u></label>
                                                    <label class="mt-2 mb-2 col-md-6"><u>ที่ดินอื่นๆ</u></label>
                                                    <div class="col-md-6">
                                                        <small for="">6. ที่เลี้ยงปศุสัตว์ที่เป็นโรงเรือน/โรงเลี้ยง<br>/คอกสัตว์</small>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <small for="">14. ที่ดินนอกการเกษตรที่ดินสำหรับสิ่งปลูกสร้างที่ใช้ประโยชน์
                                                            นอกการเกษตร</small>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <small for="">7. ที่ทุ่งหญ้าเลี้ยงสัตว์/ที่ปลูกหญ้าเลี้ยงสัตว์ ที่ดินเกษตรที่ใช้ประโยชน์ทางประมง</small>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <small for="">15. ที่ให้ผู้อื่นเช่าทำประโยชน์/ให้ทำฟรีทั้งในการเกษตรและนอก
                                                            การเกษตร</small>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <small for="">8. ที่เพาะเลี้ยงประมงตามธรรมชาติ/ที่สร้างขึ้นเอง เช่น บ่ออนุบาล บ่อเลี้ยง บ่อพักน้ำ และอื่นๆ</small>
                                                    </div>
                                                    <div class="col-md-6">

                                                    </div>

                                                    <label class="mt-2 mb-2 col-md-6"><u>ที่ดินเกษตรที่ใช้ประโยชน์หลายอย่างและแบ่งแยกไม่ได้</u></label>
                                                    <label class="mt-2 mb-2 col-md-6"></label>
                                                    <div class="col-md-6">
                                                        <small for="">9. ที่ทำฟาร์มผสม/เกษตรผสมผสาน</small>
                                                    </div>
                                                    <label class="mt-2 mb-2 col-md-6">รายการที่ 14</label>
                                                    <div class="col-md-6">

                                                    </div>
                                                    <div class="col-md-6">
                                                        <small class="text-danger" for="">เป็นที่ดินที่ครัวเรือนใช้ประโยชน์เอง
                                                            ทั้งในการเกษตรและนอกการเกษตร</small>
                                                    </div>
                                                    <label class="mt-2 mb-2 col-md-6"></label>
                                                    <label class="mt-2 mb-2 col-md-6">รายการที่ 15</label>
                                                    <div class="col-md-6">

                                                    </div>
                                                    <div class="col-md-6">
                                                        <small class="text-danger" for="">เป็นที่ดินที่ครัวเรือนไม่ได้ให้ประโยชน์
                                                            แต่ปล่อยให้ผู่อื่นเช่าทำประโยชน์/ให้ทำฟรี
                                                            ทั้งในการเกษตรและนอกการเกษตร</small>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="card">
                                            <div class="card-header">
                                                <label for="">(8) การชลประทาน</label>
                                            </div>
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <small for="">1. ในเขตชลประทาน</small>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <small for="">2. นอกเขตชลประทาน</small>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="card">
                                            <div class="card-header">
                                                <label for="">(9) แหล่งน้ำหลักที่ใช้</label>
                                            </div>
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <small for="">1. น้ำฝน</small>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <small for="">2. น้ำชลประทาน</small>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <small for="">3. สูบเองจากแหละน้ำธรรมชาติ เช่น แม่น้ำ ห้วย คลอง หนอง บึง สระ</small>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <small for="">4. โครงการเอกชน/ราชการ สูบน้ำจากแหล่งน้ำธรรมชาต</small>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <small for="">5. กลุ่มสถามบันเกษตรกร สูบน้ำจากแหล่งน้ำธรรมชาติ</small>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <small for="">6. ซื้อจากผู้ให้บริการ (รวมถึงน้ำประปา)</small>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </h5>

            <div class="card">
                <div class="card-body">

                    <form>
                        <div class="row">
                            <div class="col-md-4 mb-3">
                                <label for="">ผืนที่ <small class="text-danger">(1)</small></label>
                                <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                            </div>
                            <div class="col-md-4 mb-3">
                                <label for="">แปลงที่ <small class="text-danger">(2)</small></label>
                                <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                            </div>
                            <div class="col-md-4 mb-3">
                                <label for="">เนื้อที่ถือครอง <small class="text-danger">(3)</small></label>
                                <div class="input-group">
                                    <input class="form-control" id="" type="text" placeholder="" aria-describedby="" required="" data-original-title="" title="">
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-12">
                                <h6><small class="text-danger">หมายเหตุ : ที่ดินแต่ละแปลงจะมีลักษณะการ<u>ถือครองและการใช้ประโยชน์</u>ที่แยกออกจากกันอย่างสิ้นเชิง</small></h6>
                            </div>
                            <div class="col-md-4 mb-3">
                                <label for="">การถือครองที่ดิน <small class="text-danger">(4)</small></label>
                                <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                            </div>
                            <div class="col-md-4 mb-3">
                                <label for="">การจำนอง/การขายฝาก <small class="text-danger">(5)</small></label>
                                <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                            </div>
                            <div class="col-md-4 mb-3">
                                <label for="">การใช้ประโยชน์ที่ดิน <small class="text-danger">(6)</small></label>
                                <div class="input-group">
                                    <input class="form-control" id="" type="text" placeholder="" aria-describedby="" required="" data-original-title="" title="">
                                </div>
                            </div>
                        </div>
                        <label for="">มูลค่าที่ดินที่มีกรรมสิทธิ์ครอบครอง/เข้าทำประโยชน์ <span class="text-danger">(ปลายปีเพาะปลูก)</span> <small class="text-danger">(7)</small></label>
                        <div class="row">
                            <div class="col-md-4 mb-3">
                                
                                <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4 mb-3">
                                <label for="">การชลประทาน <small class="text-danger">(8)</small></label>
                                <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                            </div>
                            <div class="col-md-4 mb-3">
                                <label for="">แหล่งน้ำหลักที่ใช้ <small class="text-danger">(9)</small></label>
                                <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                            </div> 
                            <div class="col-md-4 mb-3">
                                    <label for="">รหัสพืชที่ปลูก/ปศุสัตว์/ประมง <small class="text-danger">(10)</small></label>
                                    <div class="row">
                                    <div class="col-4">
                                        <div class="input-group">
                                            <input class="form-control" id="" type="text" placeholder="" aria-describedby="" required="" data-original-title="" title="">
                                        </div>
                                    </div>
                                    <div class="col-8">
                                        <div class="input-group">
                                            <input class="form-control" disabled id="" type="text" value="ชื่อพืช" placeholder="" aria-describedby="" required="" data-original-title="" title="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>

                    </form>

                </div>
            </div>
        </div>

        <div class="row text-center group-submit">
            <button type="button" class="btn btn-primary btn-lg">บันทึกข้อมูล</button>
            <button type="button" class="btn btn-outline-secondary btn-lg">ยกเลิก</button>
        </div>
    </div>
</div>
<!-- end row -->

</div>


