<?php
$array_formtext =[
    1 =>"รายงานการใช้ที่ดิน",
    2 =>"รายงานข้อมูลพื้นฐานครัวเรือน -> รายงานข้อมูลพื้นฐานครัวเรือน และหัวหน้าครัวเรือนเกษตร",
    3 =>"รายงานข้อมูลพื้นฐานครัวเรือน -> รายงานข้อมูลพื้นฐานของประชากรเกษตร",
    4 =>"รายงานข้อมูลพื้นฐานครัวเรือน -> รายงานข้อมูลพื้นฐานของแรงงาน",
    5 =>"รายงานทรัพย์สิน",
    6 =>"รายงานทัศนคติฯ",
    7 =>"รายงานประกอบวิเคราะห์น้ำท่วม -> รายงานสรุปรายได้ – รายจ่าย และตัวชี้วัดเศรษฐกิจครัวเรือน",
    8 =>"รายงานประกอบวิเคราะห์น้ำท่วม -> รายงานที่มารายได้",
    9 =>"รายงานประกอบวิเคราะห์น้ำท่วม -> รายงานที่มารายจ่าย",
    10 =>"รายงานรายจ่าย -> รายงานองค์ประกอบรายจ่ายเงินสดเกษตรทางพืช",
    11 =>"รายงานรายจ่าย -> รายงานองค์ประกอบรายจ่ายเงินสดเกษตรทางสัตว์",
    12 =>"รายงานรายจ่าย -> รายงานองค์ประกอบรายจ่ายเงินสดเกษตร และบริโภคอุปโภค",
    13 =>"รายงานรายได้ -> รายงานองค์ประกอบรายได้เงินสดเกษตรทางพืช",
    14 =>"รายงานรายได้ -> รายงานองค์ประกอบรายจ่ายเงินสดเกษตรทางสัตว์",
    15 =>"รายงานรายได้ -> รายงานองค์ประกอบรายได้เงินอื่น",
    16 =>"รายงานรายได้ – รายจ่าย",
    17 =>"รายงานหนี้สิน",
];
?>
<?php
header("Content-Type: application/vnd.ms-excel");
header('Content-Disposition: attachment; filename="MyXls.xls"');
$strExcelFileName = $array_formtext[$id].".xls";
header("Content-Type: application/x-msexcel; name=\"" . $strExcelFileName . "\"");
header("Content-Disposition: inline; filename=\"" . $strExcelFileName . "\"");
header('Content-Type: text/plain; charset=UTF-8');
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Pragma:no-cache");
?>

<html xmlns:o=”urn:schemas-microsoft-com:office:office”

xmlns:x=”urn:schemas-microsoft-com:office:excel”

xmlns=”http://www.w3.org/TR/REC-html40″>

<HTML>

<HEAD>
  <meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />
</HEAD>
<BODY>

    <style>
    table, td, th {
      border: 1px solid black;
  }
</style>
    <?php 
    $_GET["sector"] = [1,2,3,4];
    if (isset($_GET["sector"])) {
    // หาค่าคูณขยายระดับหมู่บ้าน START
        $criteria = new CDbCriteria();
    // $criteria->compare('active',1);
        $criteria->order = "those_id ASC";
        $Those = Those::model()->findAll($criteria);
        $array_thoseByVillage = array();
        foreach ($Those as $key => $value) {
            $array_thoseByVillage[$value->those_id] = array();
        }
        foreach ($array_thoseByVillage as $keyThose => $valueThose) {
            $criteria = new CDbCriteria();
            $criteria->compare('t.group',$keyThose);
            $criteria->compare('active',1);
            $Processing = Processing::model()->findAll($criteria);
            if (!empty($Processing)) {
                foreach ($Processing as $keyProcessing => $valueProcessing) {
                    $array_thoseByVillage[$keyThose][$valueProcessing->province_table->province_code] = array();
                    $criteria = new CDbCriteria();
                    $criteria->compare('processing_id',$valueProcessing->id);
                    $criteria->compare('active',1);
                    $SubProcessing = SubProcessing::model()->findAll($criteria);
                    $array_thoseByVillage[$keyThose][$valueProcessing->province_table->province_code] = array();
                    foreach ($SubProcessing as $keySubProcessing => $valueSubProcessing) {
                        if (!isset($array_thoseByVillage[$keyThose][$valueProcessing->province_table->province_code][$valueSubProcessing->district_table->district_code])) {
                            $array_thoseByVillage[$keyThose][$valueProcessing->province_table->province_code][$valueSubProcessing->district_table->district_code] = array();
                        }
                        if (!isset($array_thoseByVillage[$keyThose][$valueProcessing->province_table->province_code][$valueSubProcessing->district_table->district_code][$valueSubProcessing->subdistrict_table->subdistrict_code])) {
                            $array_thoseByVillage[$keyThose][$valueProcessing->province_table->province_code][$valueSubProcessing->district_table->district_code][$valueSubProcessing->subdistrict_table->subdistrict_code] = array();
                        }
                        if (!isset($array_thoseByVillage[$keyThose][$valueProcessing->province_table->province_code][$valueSubProcessing->district_table->district_code][$valueSubProcessing->subdistrict_table->subdistrict_code][$valueSubProcessing->village_table->village_code])) {
                            $array_thoseByVillage[$keyThose][$valueProcessing->province_table->province_code][$valueSubProcessing->district_table->district_code][$valueSubProcessing->subdistrict_table->subdistrict_code][$valueSubProcessing->village_table->village_code] = array();
                        }
                        $array_thoseByVillage[$keyThose][$valueProcessing->province_table->province_code][$valueSubProcessing->district_table->district_code][$valueSubProcessing->subdistrict_table->subdistrict_code][$valueSubProcessing->village_table->village_code]["N"] = $valueProcessing->total;
                        $array_thoseByVillage[$keyThose][$valueProcessing->province_table->province_code][$valueSubProcessing->district_table->district_code][$valueSubProcessing->subdistrict_table->subdistrict_code][$valueSubProcessing->village_table->village_code]["n"] = $valueProcessing->survey;
                        $array_thoseByVillage[$keyThose][$valueProcessing->province_table->province_code][$valueSubProcessing->district_table->district_code][$valueSubProcessing->subdistrict_table->subdistrict_code][$valueSubProcessing->village_table->village_code]["M"] = $valueSubProcessing->total;
                        $array_thoseByVillage[$keyThose][$valueProcessing->province_table->province_code][$valueSubProcessing->district_table->district_code][$valueSubProcessing->subdistrict_table->subdistrict_code][$valueSubProcessing->village_table->village_code]["m"] = $valueSubProcessing->survey;
                    }
                }
            }
        }
    // หาค่าคูณขยายระดับหมู่บ้าน START

    // หาค่าคูณขยายระดับจังหวัด START
        $array_processingByProvince = array();
        $array_checkProvinceThose = array();
        foreach ($array_thoseByVillage as $keythose => $valuethose) {
            foreach ($valuethose as $keyvaluethose => $valuevaluethose) {
                foreach ($valuevaluethose as $keyvaluevaluethose => $valuevaluevaluethose) {
                    foreach ($valuevaluevaluethose as $keyvaluevaluevaluethose => $valuevaluevaluevaluethose) {
                        foreach ($valuevaluevaluevaluethose as $keyvaluevaluevaluevaluethose => $valuevaluevaluevaluevaluethose) {
                            if (!isset($array_processingByProvince[$keyvaluethose])) {
                                $array_processingByProvince[$keyvaluethose] = array();
                            }
                            if (!isset($array_checkProvinceThose[$keythose][$keyvaluethose])) {
                                $array_checkProvinceThose[$keythose] = array();
                                $array_checkProvinceThose[$keythose][$keyvaluethose] = array();
                                if (!isset($array_processingByProvince[$keyvaluethose]["N"])) {
                                    $array_processingByProvince[$keyvaluethose]["N"] = $valuevaluevaluevaluevaluethose["N"];
                                }else{
                                    $array_processingByProvince[$keyvaluethose]["N"] += $valuevaluevaluevaluevaluethose["N"];
                                }
                                if (!isset($array_processingByProvince[$keyvaluethose]["n"])) {
                                    $array_processingByProvince[$keyvaluethose]["n"] = $valuevaluevaluevaluevaluethose["n"];
                                }else{
                                    $array_processingByProvince[$keyvaluethose]["n"] += $valuevaluevaluevaluevaluethose["n"];
                                }
                            }

                            if (!isset($array_processingByProvince[$keyvaluethose]["M"])) {
                                $array_processingByProvince[$keyvaluethose]["M"] = $valuevaluevaluevaluevaluethose["M"];
                            }else{
                                $array_processingByProvince[$keyvaluethose]["M"] += $valuevaluevaluevaluevaluethose["M"];
                            }
                            if (!isset($array_processingByProvince[$keyvaluethose]["m"])) {
                                $array_processingByProvince[$keyvaluethose]["m"] = $valuevaluevaluevaluevaluethose["m"];
                            }else{
                                $array_processingByProvince[$keyvaluethose]["m"] += $valuevaluevaluevaluevaluethose["m"];
                            }

                        }
                    }
                }
            }
        }
    // หาค่าคูณขยายระดับจังหวัด END

    //เริ่มคำนวณค่าคูณขยายระดับหมู่บ้าน
        $criteria=new CDbCriteria;
        $criteria->compare('formulas_group_id',$id);
        $criteria->order = "formulas_id ASC";
        $QuestionFormulas = QuestionFormulas::model()->findAll($criteria);
        $Choice = array();
        $Array_ans =array();
        $Choice_ans = array();
        foreach ($QuestionFormulas as $key => $value) {
            $Choice[$value->question_id] = $value->formulas_name;

            $criteria=new CDbCriteria;
            $criteria->compare('formulas_id',$value->formulas_id);
            $criteria->order = "choice ASC";
            $QuestionFormulasChoice = QuestionFormulasChoice::model()->findAll($criteria);
            $Array_ans[$value->question_id] = array();
            foreach ($QuestionFormulasChoice as $keyQuestionFormulasChoice => $valueQuestionFormulasChoice) {
                $Array_ans[$value->question_id][$valueQuestionFormulasChoice->choice] = $valueQuestionFormulasChoice->choice_name;
            }
        }
        $criteria = new CDbCriteria;
        $criteria->addInCondition('sector_id',$_GET["sector"]);
        $MasterSectorData = MasterSector::model()->findAll($criteria);
        $array_id_county = array();
        foreach ($MasterSectorData as $keyMasterSectorData => $valueMasterSectorData) {
            $MasterCountyData = MasterCounty::model()->findAll(array(
                'condition' => 'sector_id=:sector_id',
                'params' => array(':sector_id' => $valueMasterSectorData->sector_id)
            ));
            foreach ($MasterCountyData as $keyMasterCountyData => $valueMasterCountyData) {
                $array_id_county[] = $valueMasterCountyData->county_id;
            }
        }
        $criteria = new CDbCriteria;
        $criteria->addInCondition('county_id',$array_id_county);
        $criteria->order = "province_code ASC";
        $ProvinceData = MtProvince::model()->findAll($criteria);
        $Province_code_array = array();
        foreach ($ProvinceData as $key => $value) {
            $Province_code_array[] = $value->province_code;
        }
        $criteria = new CDbCriteria;
        $criteria->addInCondition('province',$Province_code_array);
        $criteria->order = "questionnaire_id ASC";
        $QuestionnaireData = Questionnaire::model()->findAll($criteria);

        $QuestionnaireDataCheckStart = array();
        foreach ($QuestionnaireData as $key => $value) {
            $QuestionnaireDataCheckStart[] = $value->questionnaire_id;
        }

        $question_array = array();
        foreach ($QuestionFormulas as $key => $value) {
            $question_array[] = $value->question_id;
            $question_array[] = $value->question_id_ans;
        }
        $question_array = array_unique($question_array);


        $criteria=new CDbCriteria;
        $criteria->select = "questionnaire_id";
        $criteria->addInCondition('questionnaire_id',$QuestionnaireDataCheckStart);
        $criteria->addInCondition('question_id',$question_array);
        $criteria->group= 'questionnaire_id';
        $SurveyValueCheck = SurveyValue::model()->findAll($criteria);

        $Questionnaire_id = array();
        foreach ($SurveyValueCheck as $key => $value) {
            $Questionnaire_id[] = $value->questionnaire_id;
        }

        $criteria = new CDbCriteria;
        $criteria->addInCondition('questionnaire_id',$Questionnaire_id);
        $criteria->order = "questionnaire_id ASC";
        $QuestionnaireData = Questionnaire::model()->findAll($criteria);

        $array_province = array();
        $array_check_questionnaire = array();
        $count =array();
        foreach ($Choice as $keyChoice => $valueChoice) {
            $criteria=new CDbCriteria;
            $criteria->compare('formulas_group_id',$id);
            $criteria->compare('question_id',$keyChoice);
            $QuestionFormulasCheck = QuestionFormulas::model()->find($criteria);
            $array_check_questionnaire[$QuestionFormulasCheck->formulas_id] = array();
            $count[$keyChoice] = array();
        }
        foreach ($QuestionnaireData as $keyQuestionnaireData => $valueQuestionnaireData) {
            foreach ($Choice as $keyChoice => $valueChoice) {
                $SurveyvalueData = SurveyValue::model()->findAll(array(
                    'condition' => 'questionnaire_id=:questionnaire_id AND question_id=:question_id',
                    'params' => array(':questionnaire_id' => $valueQuestionnaireData->questionnaire_id,':question_id' => $keyChoice)
                ));
                if (empty($SurveyvalueData)) {
                    $criteria=new CDbCriteria;
                    $criteria->compare('formulas_group_id',$id);
                    $criteria->compare('question_id',$keyChoice);
                    $QuestionFormulasCheck = QuestionFormulas::model()->find($criteria);
                    if (!empty($QuestionFormulasCheck)) {
                        $SurveyvalueData = SurveyValue::model()->findAll(array(
                            'condition' => 'questionnaire_id=:questionnaire_id AND question_id=:question_id',
                            'params' => array(':questionnaire_id' => $valueQuestionnaireData->questionnaire_id,':question_id' => $QuestionFormulasCheck->question_id_ans)
                        ));
                    }
                }
                foreach ($SurveyvalueData as $keySurveyvalueData => $valueSurveyvalueData) {
                    if ($valueSurveyvalueData->value != null || $valueSurveyvalueData->value == "") {
                        $criteria=new CDbCriteria;
                        $criteria->compare('formulas_group_id',$id);
                        $criteria->compare('question_id',$keyChoice);
                        $QuestionFormulasCheck = QuestionFormulas::model()->find($criteria);
                    if ($QuestionFormulasCheck->type =="count-multiplication") { //แบบนับจำนวน Min , Max จะทำการคูณค่าคูณขยายแค่ระดับหมู่บ้านเท่านั้น
                        
                        if (!in_array($valueQuestionnaireData->questionnaire_id, $array_check_questionnaire[$QuestionFormulasCheck->formulas_id])) {
                            $criteria=new CDbCriteria;
                            $criteria->compare('formulas_id',$QuestionFormulasCheck->formulas_id);
                            $criteria->order = "choice ASC";
                            $QuestionFormulasChoiceCheck = QuestionFormulasChoice::model()->findAll($criteria);
                            $criteria = new CDbCriteria;
                            $criteria->compare('province',$valueQuestionnaireData->province);
                            $criteria->compare('district',$valueQuestionnaireData->district);
                            $criteria->compare('subdistrict',$valueQuestionnaireData->subdistrict);
                            $criteria->compare('village',$valueQuestionnaireData->village);
                            $criteria->compare('t.group',$valueQuestionnaireData->group);
                            $criteria->compare('t.sample',$valueQuestionnaireData->sample);

                            $criteria->order = "questionnaire_id ASC";
                            $QuestionnaireDataCheck = Questionnaire::model()->findAll($criteria);
                            $array_questionnaire = array();
                            foreach ($QuestionnaireDataCheck as $keyQuestionnaireDataCheck => $valueQuestionnaireDataCheck) {
                                $array_check_questionnaire[$QuestionFormulasCheck->formulas_id][] = $valueQuestionnaireDataCheck->questionnaire_id;
                                $array_questionnaire[] = $valueQuestionnaireDataCheck->questionnaire_id;
                            }
                            $criteria = new CDbCriteria;
                            $criteria->addInCondition('questionnaire_id',$array_questionnaire);
                            $criteria->compare('question_id',$QuestionFormulasCheck->question_id_ans);
                            $SurveyvalueDataCheck = SurveyValue::model()->findAll($criteria);
                            $SurveyvalueDataCheckTotal=0;
                            foreach ($SurveyvalueDataCheck as $keySurveyvalueDataCheck => $valueSurveyvalueDataCheck) {
                                $SurveyvalueDataCheckTotal +=$valueSurveyvalueDataCheck->value;
                            }
                            foreach ($QuestionFormulasChoiceCheck as $keyQuestionFormulasChoiceCheck => $valueQuestionFormulasChoiceCheck) {
                                if ($valueQuestionFormulasChoiceCheck->min == null && $valueQuestionFormulasChoiceCheck->max > 0 && $SurveyvalueDataCheckTotal <= $valueQuestionFormulasChoiceCheck->max) {
                                    if(!isset($array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice])){
                                        if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                            $array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice] = 1*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]);
                                        }
                                    }else{
                                        if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                            $array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice]+=($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]);
                                        }

                                    }
                                }
                                elseif ($valueQuestionFormulasChoiceCheck->min > 0 && $valueQuestionFormulasChoiceCheck->max > 0 && $SurveyvalueDataCheckTotal >= $valueQuestionFormulasChoiceCheck->min && $SurveyvalueDataCheckTotal <= $valueQuestionFormulasChoiceCheck->max) {
                                    if(!isset($array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice])){
                                        if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                            $array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice] = 1*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]);
                                        }

                                    }else{
                                        if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                            $array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice]+=(
                                                $array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]
                                                /
                                                $array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])
                                                *(
                                            $array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]
                                            /
                                            $array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]);
                                            }

                                        }
                                    }
                                    elseif ($valueQuestionFormulasChoiceCheck->max == null && $valueQuestionFormulasChoiceCheck->min > 0 && $SurveyvalueDataCheckTotal > $valueQuestionFormulasChoiceCheck->min) {
                                        if(!isset($array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice])){
                                            if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                                $array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice] = 1*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]);
                                            }

                                        }else{
                                            if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                                $array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice]+=(
                                                    $array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]
                                                    /
                                                    $array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])
                                                *(
                                                $array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]
                                                /
                                                $array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]);
                                            }

                                        }
                                    }
                                }
                                
                            }
                    }elseif ($QuestionFormulasCheck->type =="multiplication"){ // แบบคำนวณเนื้อที่โดยทำการคูณค่าคูณขยายระดับหมู่บ้านและจังหวัด
                        $criteria=new CDbCriteria;
                        $criteria->compare('formulas_group_id',$id);
                        $criteria->compare('question_id',$keyChoice);
                        $QuestionFormulasCheck = QuestionFormulas::model()->find($criteria);
                        $SurveyvalueDataCheck = SurveyValue::model()->findAll(array(
                            'condition' => 'questionnaire_id=:questionnaire_id AND question_id=:question_id AND survey_group=:survey_group',
                            'params' => array(':questionnaire_id' => $valueQuestionnaireData->questionnaire_id,':question_id' => $QuestionFormulasCheck->question_id_ans,':survey_group' => $valueSurveyvalueData->survey_group)
                        ));
                        $SurveyvalueDataCheckTotal=0;
                        foreach ($SurveyvalueDataCheck as $keySurveyvalueDataCheck => $valueSurveyvalueDataCheck) {
                            $SurveyvalueDataCheckTotal +=$valueSurveyvalueDataCheck->value;
                        }
                        if(!isset($array_province[$valueQuestionnaireData->province][$keyChoice][$valueSurveyvalueData->value])){
                            if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                $array_province[$valueQuestionnaireData->province][$keyChoice][$valueSurveyvalueData->value] = $SurveyvalueDataCheckTotal*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]);
                            }
                        }else{
                            if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                $array_province[$valueQuestionnaireData->province][$keyChoice][$valueSurveyvalueData->value] = $array_province[$valueQuestionnaireData->province][$keyChoice][$valueSurveyvalueData->value] + ($SurveyvalueDataCheckTotal*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]));
                            }
                        }
                    }elseif ($QuestionFormulasCheck->type =="numerate") {
                        $criteria=new CDbCriteria;
                        $criteria->compare('formulas_id',$QuestionFormulasCheck->formulas_id);
                        $criteria->order = "choice ASC";
                        $QuestionFormulasChoiceCheck = QuestionFormulasChoice::model()->findAll($criteria);
                        if (!in_array($valueQuestionnaireData->questionnaire_id, $array_check_questionnaire[$QuestionFormulasCheck->formulas_id])) {
                            $criteria = new CDbCriteria;
                            $criteria->compare('province',$valueQuestionnaireData->province);
                            $criteria->compare('district',$valueQuestionnaireData->district);
                            $criteria->compare('subdistrict',$valueQuestionnaireData->subdistrict);
                            $criteria->compare('village',$valueQuestionnaireData->village);
                            $criteria->compare('t.group',$valueQuestionnaireData->group);
                            $criteria->order = "questionnaire_id ASC";
                            $QuestionnaireDataCheck = Questionnaire::model()->findAll($criteria);
                            foreach ($QuestionnaireDataCheck as $keyQuestionnaireDataCheck => $valueQuestionnaireDataCheck) {
                                $array_check_questionnaire[$QuestionFormulasCheck->formulas_id][] = $valueQuestionnaireDataCheck->questionnaire_id;
                            }
                            $SurveyvalueDataCheckTotal = count($QuestionnaireDataCheck);
                            if(!isset($array_province[$valueQuestionnaireData->province][$keyChoice][$valueSurveyvalueData->value])){
                                if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                    $array_province[$valueQuestionnaireData->province][$keyChoice][$valueSurveyvalueData->value] = $SurveyvalueDataCheckTotal*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]);
                                }
                            }else{
                                if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                    $array_province[$valueQuestionnaireData->province][$keyChoice][$valueSurveyvalueData->value] = $array_province[$valueQuestionnaireData->province][$keyChoice][$valueSurveyvalueData->value] + ($SurveyvalueDataCheckTotal*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]));
                                }
                            }
                        }
                    }elseif ($QuestionFormulasCheck->type =="numerate-member") {
                        $criteria=new CDbCriteria;
                        $criteria->compare('formulas_id',$QuestionFormulasCheck->formulas_id);
                        $criteria->order = "choice ASC";
                        $QuestionFormulasChoiceCheck = QuestionFormulasChoice::model()->findAll($criteria);
                        $SurveyvalueDataCheck = SurveyValue::model()->findAll(array(
                            'condition' => 'questionnaire_id=:questionnaire_id AND question_id=:question_id',
                            'params' => array(':questionnaire_id' => $valueQuestionnaireData->questionnaire_id,':question_id' => $QuestionFormulasCheck->question_id_ans)
                        ));
                        $SurveyvalueDataCheckTotal = count($SurveyvalueDataCheck);
                        if (!in_array($valueQuestionnaireData->questionnaire_id, $array_check_questionnaire[$QuestionFormulasCheck->formulas_id])) {
                            $array_check_questionnaire[$QuestionFormulasCheck->formulas_id][] = $valueQuestionnaireData->questionnaire_id;
                            if(!isset($array_province[$valueQuestionnaireData->province][$keyChoice][1])){
                                if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                    $array_province[$valueQuestionnaireData->province][$keyChoice][1] = $SurveyvalueDataCheckTotal*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]);
                                }
                            }else{
                                if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                    $array_province[$valueQuestionnaireData->province][$keyChoice][1] = $array_province[$valueQuestionnaireData->province][$keyChoice][1] + ($SurveyvalueDataCheckTotal*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]));
                                }
                            }
                        }

                    }elseif ($QuestionFormulasCheck->type =="age-hh") {
                        $criteria=new CDbCriteria;
                        $criteria->compare('formulas_id',$QuestionFormulasCheck->formulas_id);
                        $criteria->order = "choice ASC";
                        $QuestionFormulasChoiceCheck = QuestionFormulasChoice::model()->findAll($criteria);
                        if (!in_array($valueQuestionnaireData->questionnaire_id, $array_check_questionnaire[$QuestionFormulasCheck->formulas_id])) {
                            $array_check_questionnaire[$QuestionFormulasCheck->formulas_id][] = $valueQuestionnaireData->questionnaire_id;

                            $criteria = new CDbCriteria;
                            $criteria->compare('questionnaire_id',$valueQuestionnaireData->questionnaire_id);
                            $criteria->compare('question_id',$QuestionFormulasCheck->question_id_ans);
                            $SurveyvalueDataCheck = SurveyValue::model()->findAll($criteria);
                            $group_by = array();
                            foreach ($SurveyvalueDataCheck as $key => $value) {
                                $group_by[] = $value->survey_group;
                            }

                            $criteria = new CDbCriteria;
                            $criteria->addInCondition('survey_group',$group_by);
                            $criteria->compare('question_id',305);
                            $criteria->compare('value',1);
                            $SurveyvalueDataCheck = SurveyValue::model()->findAll($criteria);
                            $group_by = array();
                            foreach ($SurveyvalueDataCheck as $key => $value) {
                                $group_by[] = $value->survey_group;
                            }
                            $criteria = new CDbCriteria;
                            $criteria->select="value";
                            $criteria->compare('questionnaire_id',$valueQuestionnaireData->questionnaire_id);
                            $criteria->addInCondition('survey_group',$group_by);
                            $criteria->compare('question_id',$QuestionFormulasCheck->question_id_ans);
                            $criteria->group="value";
                            $SurveyvalueDataCheck = SurveyValue::model()->findAll($criteria);
                            $SurveyvalueDataCheckTotal = 0;
                            if (!empty($SurveyvalueDataCheck)) {
                                foreach ($SurveyvalueDataCheck as $key => $value) {
                                    $SurveyvalueDataCheckTotal +=($value->value);
                                }
                                $SurveyvalueDataCheckTotal = $SurveyvalueDataCheckTotal/ count($SurveyvalueDataCheck);
                            }

                            if(!isset($array_province[$valueQuestionnaireData->province][$keyChoice][1])){
                                if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                    $array_province[$valueQuestionnaireData->province][$keyChoice][1] = $SurveyvalueDataCheckTotal;
                                    $count[$keyChoice][$valueQuestionnaireData->province]=1;
                                }
                            }else{
                                if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                    $array_province[$valueQuestionnaireData->province][$keyChoice][1] = $array_province[$valueQuestionnaireData->province][$keyChoice][1] + ($SurveyvalueDataCheckTotal);
                                    $count[$keyChoice][$valueQuestionnaireData->province]++;
                                }
                            }
                        }
                    }elseif ($QuestionFormulasCheck->type =="sex-man-hh" || $QuestionFormulasCheck->type =="sex-man-mm" || $QuestionFormulasCheck->type =="sex-man-labor") {
                        $criteria=new CDbCriteria;
                        $criteria->compare('formulas_id',$QuestionFormulasCheck->formulas_id);
                        $criteria->order = "choice ASC";
                        $QuestionFormulasChoiceCheck = QuestionFormulasChoice::model()->findAll($criteria);
                        if (!in_array($valueQuestionnaireData->questionnaire_id, $array_check_questionnaire[$QuestionFormulasCheck->formulas_id])) {
                            $criteria = new CDbCriteria;
                            $criteria->compare('province',$valueQuestionnaireData->province);
                            $criteria->compare('district',$valueQuestionnaireData->district);
                            $criteria->compare('subdistrict',$valueQuestionnaireData->subdistrict);
                            $criteria->compare('village',$valueQuestionnaireData->village);
                            $criteria->compare('t.group',$valueQuestionnaireData->group);
                            $criteria->order = "questionnaire_id ASC";
                            $QuestionnaireDataCheck = Questionnaire::model()->findAll($criteria);
                            $array_questionnaire = array();
                            foreach ($QuestionnaireDataCheck as $keyQuestionnaireDataCheck => $valueQuestionnaireDataCheck) {
                                $array_check_questionnaire[$QuestionFormulasCheck->formulas_id][] = $valueQuestionnaireDataCheck->questionnaire_id;
                                $array_questionnaire[] = $valueQuestionnaireDataCheck->questionnaire_id;
                            }

                            $criteria = new CDbCriteria;
                            $criteria->addInCondition('questionnaire_id',$array_questionnaire);
                            $criteria->compare('question_id',$QuestionFormulasCheck->question_id_ans);
                            $SurveyvalueDataCheck = SurveyValue::model()->findAll($criteria);
                            $group_by = array();
                            foreach ($SurveyvalueDataCheck as $key => $value) {
                                $group_by[] = $value->survey_group;
                            }
                            $criteria = new CDbCriteria;
                            $criteria->addInCondition('survey_group',$group_by);
                            $criteria->compare('question_id',305);
                            if ($QuestionFormulasCheck->type =="sex-man-hh") {
                                $criteria->compare('value',1);
                            }
                            $SurveyvalueDataCheck = SurveyValue::model()->findAll($criteria);
                            $group_by = array();
                            foreach ($SurveyvalueDataCheck as $key => $value) {
                                $group_by[] = $value->survey_group;
                            }
                            $criteria = new CDbCriteria;
                            $criteria->addInCondition('survey_group',$group_by);
                            $criteria->compare('question_id',306);
                            $criteria->compare('value',1);
                            $SurveyvalueDataCheck = SurveyValue::model()->findAll($criteria);
                            if ($QuestionFormulasCheck->type =="sex-man-labor") {
                                $group_by = array();
                                foreach ($SurveyvalueDataCheck as $key => $value) {
                                    $group_by[] = $value->survey_group;
                                }
                                $criteria = new CDbCriteria;
                                $criteria->addInCondition('survey_group',$group_by);
                                $criteria->compare('question_id',307);
                                $SurveyvalueDataCheck = SurveyValue::model()->findAll($criteria);
                                $SurveyvalueDataCheckTotal = 0;
                                foreach ($SurveyvalueDataCheck as $key => $value) {
                                    if ($value->value >= 15 && $value->value <= 64) {
                                        $SurveyvalueDataCheckTotal++;
                                    }
                                }
                            }else{
                                $SurveyvalueDataCheckTotal = count($SurveyvalueDataCheck);
                            }
                            if(!isset($array_province[$valueQuestionnaireData->province][$keyChoice][1])){
                                if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                    $array_province[$valueQuestionnaireData->province][$keyChoice][1] = $SurveyvalueDataCheckTotal*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]);
                                }
                            }else{
                                if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                    $array_province[$valueQuestionnaireData->province][$keyChoice][1] = $array_province[$valueQuestionnaireData->province][$keyChoice][1] + ($SurveyvalueDataCheckTotal*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]));
                                }
                            }

                        }
                    }elseif ($QuestionFormulasCheck->type =="sex-women-hh" || $QuestionFormulasCheck->type =="sex-women-mm" || $QuestionFormulasCheck->type =="sex-women-labor") {
                        $criteria=new CDbCriteria;
                        $criteria->compare('formulas_id',$QuestionFormulasCheck->formulas_id);
                        $criteria->order = "choice ASC";
                        $QuestionFormulasChoiceCheck = QuestionFormulasChoice::model()->findAll($criteria);
                        if (!in_array($valueQuestionnaireData->questionnaire_id, $array_check_questionnaire[$QuestionFormulasCheck->formulas_id])) {
                            $criteria = new CDbCriteria;
                            $criteria->compare('province',$valueQuestionnaireData->province);
                            $criteria->compare('district',$valueQuestionnaireData->district);
                            $criteria->compare('subdistrict',$valueQuestionnaireData->subdistrict);
                            $criteria->compare('village',$valueQuestionnaireData->village);
                            $criteria->compare('t.group',$valueQuestionnaireData->group);
                            $criteria->order = "questionnaire_id ASC";
                            $QuestionnaireDataCheck = Questionnaire::model()->findAll($criteria);
                            $array_questionnaire = array();
                            foreach ($QuestionnaireDataCheck as $keyQuestionnaireDataCheck => $valueQuestionnaireDataCheck) {
                                $array_check_questionnaire[$QuestionFormulasCheck->formulas_id][] = $valueQuestionnaireDataCheck->questionnaire_id;
                                $array_questionnaire[] = $valueQuestionnaireDataCheck->questionnaire_id;
                            }

                            $criteria = new CDbCriteria;
                            $criteria->addInCondition('questionnaire_id',$array_questionnaire);
                            $criteria->compare('question_id',$QuestionFormulasCheck->question_id_ans);
                            $SurveyvalueDataCheck = SurveyValue::model()->findAll($criteria);
                            $group_by = array();
                            foreach ($SurveyvalueDataCheck as $key => $value) {
                                $group_by[] = $value->survey_group;
                            }
                            $criteria = new CDbCriteria;
                            $criteria->addInCondition('survey_group',$group_by);
                            $criteria->compare('question_id',305);
                            if ($QuestionFormulasCheck->type =="sex-women-hh") {
                                $criteria->compare('value',1);
                            }
                            $SurveyvalueDataCheck = SurveyValue::model()->findAll($criteria);
                            $group_by = array();
                            foreach ($SurveyvalueDataCheck as $key => $value) {
                                $group_by[] = $value->survey_group;
                            }
                            $criteria = new CDbCriteria;
                            $criteria->addInCondition('survey_group',$group_by);
                            $criteria->compare('question_id',306);
                            $criteria->compare('value',2);
                            $SurveyvalueDataCheck = SurveyValue::model()->findAll($criteria);
                            if ($QuestionFormulasCheck->type =="sex-women-labor") {
                                $group_by = array();
                                foreach ($SurveyvalueDataCheck as $key => $value) {
                                    $group_by[] = $value->survey_group;
                                }
                                $criteria = new CDbCriteria;
                                $criteria->addInCondition('survey_group',$group_by);
                                $criteria->compare('question_id',307);
                                $SurveyvalueDataCheck = SurveyValue::model()->findAll($criteria);
                                $SurveyvalueDataCheckTotal = 0;
                                foreach ($SurveyvalueDataCheck as $key => $value) {
                                    if ($value->value >= 15 && $value->value <= 64) {
                                        $SurveyvalueDataCheckTotal++;
                                    }
                                }
                            }else{
                                $SurveyvalueDataCheckTotal = count($SurveyvalueDataCheck);
                            }
                            if(!isset($array_province[$valueQuestionnaireData->province][$keyChoice][1])){
                                if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                    $array_province[$valueQuestionnaireData->province][$keyChoice][1] = $SurveyvalueDataCheckTotal*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]);
                                }
                            }else{
                                if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                    $array_province[$valueQuestionnaireData->province][$keyChoice][1] = $array_province[$valueQuestionnaireData->province][$keyChoice][1] + ($SurveyvalueDataCheckTotal*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]));
                                }
                            }

                        }
                    }elseif($QuestionFormulasCheck->type =="age-hh-length" || $QuestionFormulasCheck->type =="age-mm-length") { 
                        $criteria=new CDbCriteria;
                        $criteria->compare('formulas_id',$QuestionFormulasCheck->formulas_id);
                        $criteria->order = "choice ASC";
                        $QuestionFormulasChoiceCheck = QuestionFormulasChoice::model()->findAll($criteria);
                        if (!in_array($valueQuestionnaireData->questionnaire_id, $array_check_questionnaire[$QuestionFormulasCheck->formulas_id])) {
                            $criteria = new CDbCriteria;
                            $criteria->compare('province',$valueQuestionnaireData->province);
                            $criteria->compare('district',$valueQuestionnaireData->district);
                            $criteria->compare('subdistrict',$valueQuestionnaireData->subdistrict);
                            $criteria->compare('village',$valueQuestionnaireData->village);
                            $criteria->compare('t.group',$valueQuestionnaireData->group);
                            $criteria->order = "questionnaire_id ASC";
                            $QuestionnaireDataCheck = Questionnaire::model()->findAll($criteria);

                            $array_questionnaire = array();
                            foreach ($QuestionnaireDataCheck as $keyQuestionnaireDataCheck => $valueQuestionnaireDataCheck) {
                                $array_check_questionnaire[$QuestionFormulasCheck->formulas_id][] = $valueQuestionnaireDataCheck->questionnaire_id;
                                $array_questionnaire[] = $valueQuestionnaireDataCheck->questionnaire_id;
                            }
                            $criteria = new CDbCriteria;
                            $criteria->addInCondition('questionnaire_id',$array_questionnaire);
                            $criteria->compare('question_id',$QuestionFormulasCheck->question_id_ans);
                            $SurveyvalueDataCheck = SurveyValue::model()->findAll($criteria);
                            $group_by = array();
                            foreach ($SurveyvalueDataCheck as $key => $value) {
                                $group_by[] = $value->survey_group;
                            }
                            $criteria = new CDbCriteria;
                            $criteria->addInCondition('survey_group',$group_by);
                            $criteria->compare('question_id',305);
                            if ($QuestionFormulasCheck->type =="age-hh-length") {
                                $criteria->compare('value',1);
                            }
                            $SurveyvalueDataCheck = SurveyValue::model()->findAll($criteria);
                            $group_by = array();
                            foreach ($SurveyvalueDataCheck as $key => $value) {
                                $group_by[] = $value->survey_group;
                            }

                            $criteria = new CDbCriteria;
                            $criteria->addInCondition('survey_group',$group_by);
                            $criteria->compare('question_id',$QuestionFormulasCheck->question_id_ans);
                            $SurveyvalueDataCheck = SurveyValue::model()->findAll($criteria);
                            foreach ($SurveyvalueDataCheck as $keySurveyvalueDataCheck => $valueSurveyvalueDataCheck) {
                                foreach ($QuestionFormulasChoiceCheck as $keyQuestionFormulasChoiceCheck => $valueQuestionFormulasChoiceCheck) {
                                    if ($valueQuestionFormulasChoiceCheck->min == null && $valueQuestionFormulasChoiceCheck->max > 0 && $valueSurveyvalueDataCheck->value <= $valueQuestionFormulasChoiceCheck->max) {
                                        if(!isset($array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice])){
                                            if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                                $array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice] = 1*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]);
                                            }

                                        }else{
                                            if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                                $array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice]+=($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]);
                                            }

                                        }
                                    }elseif ($valueQuestionFormulasChoiceCheck->min > 0 && $valueQuestionFormulasChoiceCheck->max > 0 && $valueSurveyvalueDataCheck->value >= $valueQuestionFormulasChoiceCheck->min && $valueSurveyvalueDataCheck->value <= $valueQuestionFormulasChoiceCheck->max) {
                                        if(!isset($array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice])){
                                            if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                                # code...
                                                $array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice] = 1*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]);
                                            }

                                        }else{
                                            if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                                # code...
                                                $array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice]+=($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]);
                                            }

                                        }
                                    }elseif ($valueQuestionFormulasChoiceCheck->max == null && $valueQuestionFormulasChoiceCheck->min > 0 && $valueSurveyvalueDataCheck->value >= $valueQuestionFormulasChoiceCheck->min) {
                                        if(!isset($array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice])){
                                            if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                                # code...
                                                $array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice] = 1*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]);
                                            }

                                        }else{
                                            if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                                # code...
                                                $array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice]+=(
                                                    $array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]
                                                    /
                                                    $array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])
                                                *(
                                                $array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]
                                                /
                                                $array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        }elseif ($QuestionFormulasCheck->type =="multiplication-hh-not-province"){ // แบบคำนวณเนื้อที่โดยทำการคูณค่าคูณขยายระดับหมู่บ้านและจังหวัด
                            if (!in_array($valueQuestionnaireData->questionnaire_id, $array_check_questionnaire[$QuestionFormulasCheck->formulas_id])) {
                                $criteria = new CDbCriteria;
                                $criteria->compare('province',$valueQuestionnaireData->province);
                                $criteria->compare('district',$valueQuestionnaireData->district);
                                $criteria->compare('subdistrict',$valueQuestionnaireData->subdistrict);
                                $criteria->compare('village',$valueQuestionnaireData->village);
                                $criteria->compare('t.group',$valueQuestionnaireData->group);
                                $criteria->order = "questionnaire_id ASC";
                                $QuestionnaireDataCheck = Questionnaire::model()->findAll($criteria);

                                $array_questionnaire = array();
                                foreach ($QuestionnaireDataCheck as $keyQuestionnaireDataCheck => $valueQuestionnaireDataCheck) {
                                    $array_check_questionnaire[$QuestionFormulasCheck->formulas_id][] = $valueQuestionnaireDataCheck->questionnaire_id;
                                    $array_questionnaire[] = $valueQuestionnaireDataCheck->questionnaire_id;
                                }
                                $criteria = new CDbCriteria;
                                $criteria->addInCondition('questionnaire_id',$array_questionnaire);
                                $criteria->compare('question_id',$QuestionFormulasCheck->question_id_ans);
                                $SurveyvalueDataCheck = SurveyValue::model()->findAll($criteria);
                                $group_by = array();
                                foreach ($SurveyvalueDataCheck as $key => $value) {
                                    $group_by[] = $value->survey_group;
                                }
                                $criteria = new CDbCriteria;
                                $criteria->addInCondition('survey_group',$group_by);
                                $criteria->compare('question_id',305);
                                $criteria->compare('value',1);
                                $SurveyvalueDataCheck = SurveyValue::model()->findAll($criteria);
                                $group_by = array();
                                foreach ($SurveyvalueDataCheck as $key => $value) {
                                    $group_by[] = $value->survey_group;
                                }
                                $criteria = new CDbCriteria;
                                $criteria->addInCondition('survey_group',$group_by);
                                $criteria->compare('question_id',$QuestionFormulasCheck->question_id_ans);
                                $SurveyvalueDataCheck = SurveyValue::model()->findAll($criteria);

                                foreach ($SurveyvalueDataCheck as $keySurveyvalueDataCheck => $valueSurveyvalueDataCheck) {
                                    if(!isset($array_province[$valueQuestionnaireData->province][$keyChoice][$valueSurveyvalueDataCheck->value])){
                                        if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                            $array_province[$valueQuestionnaireData->province][$keyChoice][$valueSurveyvalueDataCheck->value] = ($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]);
                                        }
                                    }else{
                                        if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                            $array_province[$valueQuestionnaireData->province][$keyChoice][$valueSurveyvalueDataCheck->value] = $array_province[$valueQuestionnaireData->province][$keyChoice][$valueSurveyvalueDataCheck->value] + (($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]));
                                        }
                                    }
                                }
                            }
                            
                        }elseif ($QuestionFormulasCheck->type =="multiplication-mm-not-province" || $QuestionFormulasCheck->type =="multiplication-mm-not-province-in" || $QuestionFormulasCheck->type =="multiplication-mm-not-province-out" || $QuestionFormulasCheck->type =="multiplication-labor-not-province"){ // แบบคำนวณเนื้อที่โดยทำการคูณค่าคูณขยายระดับหมู่บ้านและจังหวัด
                            if (!in_array($valueQuestionnaireData->questionnaire_id, $array_check_questionnaire[$QuestionFormulasCheck->formulas_id])) {
                                $criteria = new CDbCriteria;
                                $criteria->compare('province',$valueQuestionnaireData->province);
                                $criteria->compare('district',$valueQuestionnaireData->district);
                                $criteria->compare('subdistrict',$valueQuestionnaireData->subdistrict);
                                $criteria->compare('village',$valueQuestionnaireData->village);
                                $criteria->compare('t.group',$valueQuestionnaireData->group);
                                $criteria->order = "questionnaire_id ASC";
                                $QuestionnaireDataCheck = Questionnaire::model()->findAll($criteria);

                                $array_questionnaire = array();
                                foreach ($QuestionnaireDataCheck as $keyQuestionnaireDataCheck => $valueQuestionnaireDataCheck) {
                                    $array_check_questionnaire[$QuestionFormulasCheck->formulas_id][] = $valueQuestionnaireDataCheck->questionnaire_id;
                                    $array_questionnaire[] = $valueQuestionnaireDataCheck->questionnaire_id;
                                }
                                $criteria = new CDbCriteria;
                                $criteria->addInCondition('questionnaire_id',$array_questionnaire);
                                $criteria->compare('question_id',$QuestionFormulasCheck->question_id_ans);
                                $SurveyvalueDataCheck = SurveyValue::model()->findAll($criteria);
                                $group_by = array();
                                foreach ($SurveyvalueDataCheck as $key => $value) {
                                    $group_by[] = $value->survey_group;
                                }
                                $criteria = new CDbCriteria;
                                $criteria->addInCondition('survey_group',$group_by);
                                $criteria->compare('question_id',305);
                                $SurveyvalueDataCheck = SurveyValue::model()->findAll($criteria);
                                $group_by = array();
                                foreach ($SurveyvalueDataCheck as $key => $value) {
                                    $group_by[] = $value->survey_group;
                                }

                                if ($QuestionFormulasCheck->type =="multiplication-mm-not-province-in" || $QuestionFormulasCheck->type =="multiplication-mm-not-province-out") {
                                    $criteria = new CDbCriteria;
                                    $criteria->addInCondition('survey_group',$group_by);
                                    $criteria->compare('question_id',311);
                                    if ($QuestionFormulasCheck->type =="multiplication-mm-not-province-in") {
                                        $criteria->compare('value',2);
                                    }elseif ($QuestionFormulasCheck->type =="multiplication-mm-not-province-out") {
                                        $criteria->compare('value',3);
                                    }
                                    $SurveyvalueDataCheck = SurveyValue::model()->findAll($criteria);
                                    $group_by = array();
                                    foreach ($SurveyvalueDataCheck as $key => $value) {
                                        $group_by[] = $value->survey_group;
                                    }
                                }

                                if ($QuestionFormulasCheck->type =="multiplication-labor-not-province") {
                                    $group_by = array();
                                    foreach ($SurveyvalueDataCheck as $key => $value) {
                                        $group_by[] = $value->survey_group;
                                    }
                                    $criteria = new CDbCriteria;
                                    $criteria->addInCondition('survey_group',$group_by);
                                    $criteria->compare('question_id',307);
                                    $SurveyvalueDataCheck = SurveyValue::model()->findAll($criteria);
                                    $SurveyvalueDataCheckTotal = 0;
                                    $group_by = array();
                                    foreach ($SurveyvalueDataCheck as $key => $value) {
                                        if ($value->value >= 15 && $value->value <= 64) {
                                            $group_by[] = $value->survey_group;
                                        }
                                    }
                                }

                                $criteria = new CDbCriteria;
                                $criteria->addInCondition('survey_group',$group_by);
                                $criteria->compare('question_id',$QuestionFormulasCheck->question_id_ans);
                                $SurveyvalueDataCheck = SurveyValue::model()->findAll($criteria);

                                foreach ($SurveyvalueDataCheck as $keySurveyvalueDataCheck => $valueSurveyvalueDataCheck) {
                                    if(!isset($array_province[$valueQuestionnaireData->province][$keyChoice][$valueSurveyvalueDataCheck->value])){
                                        if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                            $array_province[$valueQuestionnaireData->province][$keyChoice][$valueSurveyvalueDataCheck->value] = ($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]);
                                        }
                                    }else{
                                        if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                            $array_province[$valueQuestionnaireData->province][$keyChoice][$valueSurveyvalueDataCheck->value] = $array_province[$valueQuestionnaireData->province][$keyChoice][$valueSurveyvalueDataCheck->value] + (($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]));
                                        }
                                    }
                                }
                            }
                            
                        }elseif ($QuestionFormulasCheck->type =="numerate-labor" || $QuestionFormulasCheck->type =="numerate-debt"){ // แบบคำนวณเนื้อที่โดยทำการคูณค่าคูณขยายระดับหมู่บ้านและจังหวัด
                            if (!in_array($valueQuestionnaireData->questionnaire_id, $array_check_questionnaire[$QuestionFormulasCheck->formulas_id])) {
                                $criteria = new CDbCriteria;
                                $criteria->compare('province',$valueQuestionnaireData->province);
                                $criteria->compare('district',$valueQuestionnaireData->district);
                                $criteria->compare('subdistrict',$valueQuestionnaireData->subdistrict);
                                $criteria->compare('village',$valueQuestionnaireData->village);
                                $criteria->compare('t.group',$valueQuestionnaireData->group);
                                $criteria->order = "questionnaire_id ASC";
                                $QuestionnaireDataCheck = Questionnaire::model()->findAll($criteria);

                                $array_questionnaire = array();
                                foreach ($QuestionnaireDataCheck as $keyQuestionnaireDataCheck => $valueQuestionnaireDataCheck) {
                                    $array_check_questionnaire[$QuestionFormulasCheck->formulas_id][] = $valueQuestionnaireDataCheck->questionnaire_id;
                                    $array_questionnaire[] = $valueQuestionnaireDataCheck->questionnaire_id;
                                }
                                $criteria = new CDbCriteria;
                                $criteria->addInCondition('questionnaire_id',$array_questionnaire);
                                $criteria->compare('question_id',$QuestionFormulasCheck->question_id_ans);
                                $SurveyvalueDataCheck = SurveyValue::model()->findAll($criteria);
                                $SurveyvalueDataCheckTotal = 0 ;
                                if ($QuestionFormulasCheck->type =="numerate-labor") {
                                    foreach ($SurveyvalueDataCheck as $key => $value) {
                                        if ($value->value >= 15 && $value->value <= 64) {
                                            $SurveyvalueDataCheckTotal++;
                                        }
                                    }
                                }elseif($QuestionFormulasCheck->type =="numerate-debt"){
                                    foreach ($SurveyvalueDataCheck as $key => $value) {
                                        $SurveyvalueDataCheckTotal+=$value->value;
                                    }
                                }

                                if(!isset($array_province[$valueQuestionnaireData->province][$keyChoice][1])){
                                    if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                        $array_province[$valueQuestionnaireData->province][$keyChoice][1] = $SurveyvalueDataCheckTotal*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]);
                                    }
                                }else{
                                    if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                        $array_province[$valueQuestionnaireData->province][$keyChoice][1] = $array_province[$valueQuestionnaireData->province][$keyChoice][1] + ($SurveyvalueDataCheckTotal*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]));
                                    }
                                }
                            }
                            
                        }elseif($QuestionFormulasCheck->type =="cost-multiplication-plant") {
                            $criteria=new CDbCriteria;
                            $criteria->compare('formulas_id',$QuestionFormulasCheck->formulas_id);
                            $criteria->order = "choice ASC";
                            $QuestionFormulasChoiceCheck = QuestionFormulasChoice::model()->findAll($criteria);
                            if (!in_array($valueQuestionnaireData->questionnaire_id, $array_check_questionnaire[$QuestionFormulasCheck->formulas_id])) {
                                $criteria = new CDbCriteria;
                                $criteria->compare('province',$valueQuestionnaireData->province);
                                $criteria->compare('district',$valueQuestionnaireData->district);
                                $criteria->compare('subdistrict',$valueQuestionnaireData->subdistrict);
                                $criteria->compare('village',$valueQuestionnaireData->village);
                                $criteria->compare('t.group',$valueQuestionnaireData->group);
                                $criteria->order = "questionnaire_id ASC";
                                $QuestionnaireDataCheck = Questionnaire::model()->findAll($criteria);

                                $array_questionnaire = array();
                                foreach ($QuestionnaireDataCheck as $keyQuestionnaireDataCheck => $valueQuestionnaireDataCheck) {
                                    $array_check_questionnaire[$QuestionFormulasCheck->formulas_id][] = $valueQuestionnaireDataCheck->questionnaire_id;
                                    $array_questionnaire[] = $valueQuestionnaireDataCheck->questionnaire_id;
                                }
                                $criteria = new CDbCriteria;
                                $criteria->addInCondition('questionnaire_id',$array_questionnaire);
                                $criteria->compare('question_id',$QuestionFormulasCheck->question_id_ans);
                                $SurveyvalueDataCheck = SurveyValue::model()->findAll($criteria);
                                foreach ($SurveyvalueDataCheck as $keySurveyvalueDataCheck => $valueSurveyvalueDataCheck) {
                                    $criteria = new CDbCriteria;
                                    $criteria->compare('survey_group',$valueSurveyvalueDataCheck->survey_group);
                                    $criteria->compare('question_id',25);
                                    $group_value1 = SurveyValue::model()->find($criteria);
                                    $criteria = new CDbCriteria;
                                    $criteria->compare('survey_group',$valueSurveyvalueDataCheck->survey_group);
                                    $criteria->compare('question_id',27);
                                    $group_value2 = SurveyValue::model()->find($criteria);
                                    $value_check = 0;
                                    if (!empty($group_value1)) {
                                        $value_check = $group_value1->value;
                                    }
                                    if (!empty($group_value2)) {
                                        $value_check = $value_check * $group_value2->value;
                                    }
                                    if ($value_check > 0) {
                                        foreach ($QuestionFormulasChoiceCheck as $keyQuestionFormulasChoiceCheck => $valueQuestionFormulasChoiceCheck) {
                                            if ($valueQuestionFormulasChoiceCheck->min == null && $valueQuestionFormulasChoiceCheck->max > 0 && $valueSurveyvalueDataCheck->value <= $valueQuestionFormulasChoiceCheck->max) {
                                                if(!isset($array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice])){
                                                    if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                                        $array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice] = $value_check*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]);
                                                    }

                                                }else{
                                                    if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                                        $array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice]+= $value_check *($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]);
                                                    }

                                                }
                                            }elseif ($valueQuestionFormulasChoiceCheck->min > 0 && $valueQuestionFormulasChoiceCheck->max > 0 && $valueSurveyvalueDataCheck->value >= $valueQuestionFormulasChoiceCheck->min && $valueSurveyvalueDataCheck->value <= $valueQuestionFormulasChoiceCheck->max) {
                                                if(!isset($array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice])){
                                                    if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                                        # code...
                                                        $array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice] = $value_check *($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]);
                                                    }

                                                }else{
                                                    if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                                        # code...
                                                        $array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice]+= $value_check *($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]);
                                                    }

                                                }
                                            }elseif ($valueQuestionFormulasChoiceCheck->max == null && $valueQuestionFormulasChoiceCheck->min > 0 && $valueSurveyvalueDataCheck->value >= $valueQuestionFormulasChoiceCheck->min) {
                                                if(!isset($array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice])){
                                                    $array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice] = $value_check *($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]);
                                                }else{
                                                    $array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice]+= $value_check *(
                                                        $array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]
                                                        /
                                                        $array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])
                                                *(
                                                    $array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]
                                                    /
                                                    $array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]);
                                            }
                                        }
                                    }
                                }
                            }
                            $criteria = new CDbCriteria;
                            $criteria->addInCondition('questionnaire_id',$array_questionnaire);
                            $criteria->compare('question_id',38);
                            $SurveyvalueDataCheck = SurveyValue::model()->findAll($criteria);

                            foreach ($SurveyvalueDataCheck as $keySurveyvalueDataCheck => $valueSurveyvalueDataCheck) {
                                $criteria = new CDbCriteria;
                                $criteria->compare('survey_group',$valueSurveyvalueDataCheck->survey_group);
                                $criteria->compare('question_id',92);
                                $group_value = SurveyValue::model()->find($criteria);
                                $value_check = 0;
                                if (!empty($group_value)) {
                                    $value_check = $group_value->value;
                                }
                                if ($value_check > 0) {
                                    foreach ($QuestionFormulasChoiceCheck as $keyQuestionFormulasChoiceCheck => $valueQuestionFormulasChoiceCheck) {
                                        if ($valueQuestionFormulasChoiceCheck->min == null && $valueQuestionFormulasChoiceCheck->max > 0 && $valueSurveyvalueDataCheck->value <= $valueQuestionFormulasChoiceCheck->max) {
                                            if(!isset($array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice])){
                                                if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                                    # code...
                                                    $array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice] = $value_check*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]);
                                                }

                                            }else{
                                                if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                                    # code...
                                                    $array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice]+= $value_check *($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]);
                                                }

                                            }
                                        }elseif ($valueQuestionFormulasChoiceCheck->min > 0 && $valueQuestionFormulasChoiceCheck->max > 0 && $valueSurveyvalueDataCheck->value >= $valueQuestionFormulasChoiceCheck->min && $valueSurveyvalueDataCheck->value <= $valueQuestionFormulasChoiceCheck->max) {
                                            if(!isset($array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice])){
                                                if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                                    # code...
                                                    $array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice] = $value_check *($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]);
                                                }

                                            }else{
                                                if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                                    $array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice]+= $value_check *($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]);
                                                }

                                            }
                                        }elseif ($valueQuestionFormulasChoiceCheck->max == null && $valueQuestionFormulasChoiceCheck->min > 0 && $valueSurveyvalueDataCheck->value >= $valueQuestionFormulasChoiceCheck->min) {
                                            if(!isset($array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice])){
                                                if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                                    # code...
                                                    $array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice] = $value_check *($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]);
                                                }

                                            }else{
                                                if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                                    # code...
                                                    $array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice]+= $value_check *(
                                                        $array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]
                                                        /
                                                        $array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])
                                                *(
                                                    $array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]
                                                    /
                                                    $array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]);
                                            }

                                        }
                                    }
                                }
                            }
                        }
                    }
                }elseif($QuestionFormulasCheck->type =="cost-multiplication-animal") {
                    $criteria=new CDbCriteria;
                    $criteria->compare('formulas_id',$QuestionFormulasCheck->formulas_id);
                    $criteria->order = "choice ASC";
                    $QuestionFormulasChoiceCheck = QuestionFormulasChoice::model()->findAll($criteria);
                    if (!in_array($valueQuestionnaireData->questionnaire_id, $array_check_questionnaire[$QuestionFormulasCheck->formulas_id])) {
                        $criteria = new CDbCriteria;
                        $criteria->compare('province',$valueQuestionnaireData->province);
                        $criteria->compare('district',$valueQuestionnaireData->district);
                        $criteria->compare('subdistrict',$valueQuestionnaireData->subdistrict);
                        $criteria->compare('village',$valueQuestionnaireData->village);
                        $criteria->compare('t.group',$valueQuestionnaireData->group);
                        $criteria->order = "questionnaire_id ASC";
                        $QuestionnaireDataCheck = Questionnaire::model()->findAll($criteria);

                        $array_questionnaire = array();
                        foreach ($QuestionnaireDataCheck as $keyQuestionnaireDataCheck => $valueQuestionnaireDataCheck) {
                            $array_check_questionnaire[$QuestionFormulasCheck->formulas_id][] = $valueQuestionnaireDataCheck->questionnaire_id;
                            $array_questionnaire[] = $valueQuestionnaireDataCheck->questionnaire_id;
                        }
                        $criteria = new CDbCriteria;
                        $criteria->addInCondition('questionnaire_id',$array_questionnaire);
                        $criteria->compare('question_id',$QuestionFormulasCheck->question_id_ans);
                        $SurveyvalueDataCheck = SurveyValue::model()->findAll($criteria);


                        foreach ($SurveyvalueDataCheck as $keySurveyvalueDataCheck => $valueSurveyvalueDataCheck) {
                            $criteria = new CDbCriteria;
                            $criteria->compare('survey_group',$valueSurveyvalueDataCheck->survey_group);
                            $criteria->compare('question_id',103);
                            $group_value = SurveyValue::model()->find($criteria);
                            $value_check = 0;
                            if (!empty($group_value)) {
                                $value_check = $group_value->value;
                            }
                            if ($value_check > 0) {
                                foreach ($QuestionFormulasChoiceCheck as $keyQuestionFormulasChoiceCheck => $valueQuestionFormulasChoiceCheck) {
                                    if ($valueQuestionFormulasChoiceCheck->min == null && $valueQuestionFormulasChoiceCheck->max > 0 && $valueSurveyvalueDataCheck->value <= $valueQuestionFormulasChoiceCheck->max) {
                                        if(!isset($array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice])){
                                            if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                                    # code...
                                                $array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice] = $value_check*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]);
                                            }

                                        }else{
                                            if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                                    # code...
                                                $array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice]+= $value_check *($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]);
                                            }

                                        }
                                    }elseif ($valueQuestionFormulasChoiceCheck->min > 0 && $valueQuestionFormulasChoiceCheck->max > 0 && $valueSurveyvalueDataCheck->value >= $valueQuestionFormulasChoiceCheck->min && $valueSurveyvalueDataCheck->value <= $valueQuestionFormulasChoiceCheck->max) {
                                        if(!isset($array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice])){
                                            if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                                    # code...
                                                $array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice] = $value_check *($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]);
                                            }

                                        }else{
                                            if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                                    # code...
                                                $array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice]+= $value_check *($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]);
                                            }

                                        }
                                    }elseif ($valueQuestionFormulasChoiceCheck->max == null && $valueQuestionFormulasChoiceCheck->min > 0 && $valueSurveyvalueDataCheck->value >= $valueQuestionFormulasChoiceCheck->min) {
                                        if(!isset($array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice])){
                                            if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                                    # code...
                                                $array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice] = $value_check *($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]);
                                            }

                                        }else{
                                            if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                                    # code...
                                                $array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice]+= $value_check *(
                                                    $array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]
                                                    /
                                                    $array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])
                                                *(
                                                $array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]
                                                /
                                                $array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]);
                                            }

                                        }
                                    }
                                }
                            }

                        }
                    }
                        }elseif ($QuestionFormulasCheck->type =="count-multiplication-begin-year" || $QuestionFormulasCheck->type =="count-multiplication-between-year" || $QuestionFormulasCheck->type =="count-multiplication-end-year"){ // แบบคำนวณเนื้อที่โดยทำการคูณค่าคูณขยายระดับหมู่บ้านและจังหวัด
                            if (!in_array($valueQuestionnaireData->questionnaire_id, $array_check_questionnaire[$QuestionFormulasCheck->formulas_id])) {
                                $criteria = new CDbCriteria;
                                $criteria->compare('province',$valueQuestionnaireData->province);
                                $criteria->compare('district',$valueQuestionnaireData->district);
                                $criteria->compare('subdistrict',$valueQuestionnaireData->subdistrict);
                                $criteria->compare('village',$valueQuestionnaireData->village);
                                $criteria->compare('t.group',$valueQuestionnaireData->group);
                                $criteria->order = "questionnaire_id ASC";
                                $QuestionnaireDataCheck = Questionnaire::model()->findAll($criteria);

                                $array_questionnaire = array();
                                foreach ($QuestionnaireDataCheck as $keyQuestionnaireDataCheck => $valueQuestionnaireDataCheck) {
                                    $array_check_questionnaire[$QuestionFormulasCheck->formulas_id][] = $valueQuestionnaireDataCheck->questionnaire_id;
                                    $array_questionnaire[] = $valueQuestionnaireDataCheck->questionnaire_id;
                                }
                                $criteria = new CDbCriteria;
                                $criteria->addInCondition('questionnaire_id',$array_questionnaire);
                                $criteria->compare('question_id',249);
                                $SurveyvalueDataCheck = SurveyValue::model()->findAll($criteria);
                                $group_by = array();
                                foreach ($SurveyvalueDataCheck as $key => $value) {
                                    $group_by[] = $value->survey_group;
                                }
                                $criteria = new CDbCriteria;
                                $criteria->addInCondition('survey_group',$group_by);
                                if ($QuestionFormulasCheck->type =="count-multiplication-begin-year") {
                                    $criteria->compare('question_id',247);
                                    $criteria->compare('value',1);
                                }elseif($QuestionFormulasCheck->type =="count-multiplication-between-year"){
                                    $criteria->compare('question_id',247);
                                    $criteria->addInCondition('value',['2','3']);
                                }elseif ($QuestionFormulasCheck->type =="count-multiplication-end-year") {
                                    $criteria->compare('question_id',258);
                                }
                                $SurveyvalueDataCheck = SurveyValue::model()->findAll($criteria);
                                $group_by = array();
                                foreach ($SurveyvalueDataCheck as $key => $value) {
                                    if ($QuestionFormulasCheck->type =="count-multiplication-begin-year" || $QuestionFormulasCheck->type =="count-multiplication-between-year") {
                                        $group_by[] = $value->survey_group;
                                    }elseif($QuestionFormulasCheck->type =="count-multiplication-end-year"){
                                        if ($value->value > 0) {
                                            $group_by[] = $value->survey_group;
                                        }
                                    }
                                }

                                $criteria = new CDbCriteria;
                                $criteria->addInCondition('survey_group',$group_by);
                                $criteria->compare('question_id',$QuestionFormulasCheck->question_id_ans);
                                $SurveyvalueDataCheck = SurveyValue::model()->findAll($criteria);
                                // $group_by = array();
                                // foreach ($SurveyvalueDataCheck as $key => $value) {
                                //     $group_by[] = $value->survey_group;
                                // }

                                // $criteria = new CDbCriteria;
                                // $criteria->addInCondition('survey_group',$group_by);
                                // $criteria->compare('question_id',$QuestionFormulasCheck->question_id_ans);
                                // $SurveyvalueDataCheck = SurveyValue::model()->findAll($criteria);

                                foreach ($SurveyvalueDataCheck as $keySurveyvalueDataCheck => $valueSurveyvalueDataCheck) {
                                    $criteria = new CDbCriteria;
                                    $criteria->compare('survey_group',$valueSurveyvalueDataCheck->survey_group);
                                    if ($QuestionFormulasCheck->type =="count-multiplication-begin-year" || $QuestionFormulasCheck->type =="count-multiplication-between-year") {
                                        $criteria->compare('question_id',249);
                                    }elseif($QuestionFormulasCheck->type =="count-multiplication-end-year"){
                                        $criteria->compare('question_id',258);
                                    }
                                    $valueDataCheck = SurveyValue::model()->find($criteria);
                                    if(!isset($array_province[$valueQuestionnaireData->province][$keyChoice][$valueSurveyvalueDataCheck->value])){
                                        if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                            $array_province[$valueQuestionnaireData->province][$keyChoice][$valueSurveyvalueDataCheck->value] = $valueDataCheck->value * ($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]);
                                        }
                                    }else{
                                        if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                            $array_province[$valueQuestionnaireData->province][$keyChoice][$valueSurveyvalueDataCheck->value] += $valueDataCheck->value * (($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]));
                                        }
                                    }
                                }
                            }
                            
                        }elseif($QuestionFormulasCheck->type =="count-multiplication-begin-year-length" || $QuestionFormulasCheck->type =="count-multiplication-begin-year-length-objective" || $QuestionFormulasCheck->type =="count-multiplication-begin-year-length-source" || $QuestionFormulasCheck->type =="count-multiplication-between-year-length" || $QuestionFormulasCheck->type =="count-multiplication-between-year-length-objective" || $QuestionFormulasCheck->type =="count-multiplication-between-year-length-source" || $QuestionFormulasCheck->type =="count-multiplication-end-year-length" || $QuestionFormulasCheck->type =="count-multiplication-end-year-length-objective" || $QuestionFormulasCheck->type =="count-multiplication-end-year-length-source") {
                            $criteria=new CDbCriteria;
                            $criteria->compare('formulas_id',$QuestionFormulasCheck->formulas_id);
                            $criteria->order = "choice ASC";
                            $QuestionFormulasChoiceCheck = QuestionFormulasChoice::model()->findAll($criteria);
                            if (!in_array($valueQuestionnaireData->questionnaire_id, $array_check_questionnaire[$QuestionFormulasCheck->formulas_id])) {
                                $criteria = new CDbCriteria;
                                $criteria->compare('province',$valueQuestionnaireData->province);
                                $criteria->compare('district',$valueQuestionnaireData->district);
                                $criteria->compare('subdistrict',$valueQuestionnaireData->subdistrict);
                                $criteria->compare('village',$valueQuestionnaireData->village);
                                $criteria->compare('t.group',$valueQuestionnaireData->group);
                                $criteria->order = "questionnaire_id ASC";
                                $QuestionnaireDataCheck = Questionnaire::model()->findAll($criteria);

                                $array_questionnaire = array();
                                foreach ($QuestionnaireDataCheck as $keyQuestionnaireDataCheck => $valueQuestionnaireDataCheck) {
                                    $array_check_questionnaire[$QuestionFormulasCheck->formulas_id][] = $valueQuestionnaireDataCheck->questionnaire_id;
                                    $array_questionnaire[] = $valueQuestionnaireDataCheck->questionnaire_id;
                                }

                                $criteria = new CDbCriteria;
                                $criteria->addInCondition('questionnaire_id',$array_questionnaire);
                                $criteria->compare('question_id',249);
                                $SurveyvalueDataCheck = SurveyValue::model()->findAll($criteria);
                                $group_by = array();
                                foreach ($SurveyvalueDataCheck as $key => $value) {
                                    $group_by[] = $value->survey_group;
                                }

                                $criteria = new CDbCriteria;
                                $criteria->addInCondition('survey_group',$group_by);
                                if ($QuestionFormulasCheck->type =="count-multiplication-begin-year-length" || $QuestionFormulasCheck->type =="count-multiplication-begin-year-length-objective" || $QuestionFormulasCheck->type =="count-multiplication-begin-year-length-source") {
                                    $criteria->compare('question_id',247);
                                    $criteria->compare('value',1);
                                }elseif($QuestionFormulasCheck->type =="count-multiplication-between-year-length" || $QuestionFormulasCheck->type =="count-multiplication-between-year-length-objective" || $QuestionFormulasCheck->type =="count-multiplication-between-year-length-source"){
                                    $criteria->compare('question_id',247);
                                    $criteria->addInCondition('value',['2','3']);
                                }elseif($QuestionFormulasCheck->type =="count-multiplication-end-year-length" || $QuestionFormulasCheck->type =="count-multiplication-end-year-length-objective" || $QuestionFormulasCheck->type =="count-multiplication-end-year-length-source"){
                                    $criteria->compare('question_id',258);
                                }
                                $SurveyvalueDataCheck = SurveyValue::model()->findAll($criteria);
                                $group_by = array();
                                foreach ($SurveyvalueDataCheck as $key => $value) {
                                    if ($QuestionFormulasCheck->type =="count-multiplication-begin-year-length" || $QuestionFormulasCheck->type =="count-multiplication-begin-year-length-objective" || $QuestionFormulasCheck->type =="count-multiplication-begin-year-length-source" || $QuestionFormulasCheck->type =="count-multiplication-between-year-length" || $QuestionFormulasCheck->type =="count-multiplication-between-year-length-objective" || $QuestionFormulasCheck->type =="count-multiplication-between-year-length-source") {
                                        $group_by[] = $value->survey_group;
                                    }elseif($QuestionFormulasCheck->type =="count-multiplication-end-year-length" || $QuestionFormulasCheck->type =="count-multiplication-end-year-length-objective" || $QuestionFormulasCheck->type =="count-multiplication-end-year-length-source"){
                                        if ($value->value > 0) {
                                            $group_by[] = $value->survey_group;
                                        }

                                    }
                                }
                                if ($QuestionFormulasCheck->type =="count-multiplication-begin-year-length" || $QuestionFormulasCheck->type =="count-multiplication-between-year-length" || $QuestionFormulasCheck->type =="count-multiplication-end-year-length") {
                                    $criteria = new CDbCriteria;
                                    $criteria->addInCondition('survey_group',$group_by);
                                    $criteria->compare('question_id',253);
                                    $SurveyvalueDataCheck = SurveyValue::model()->findAll($criteria);
                                }elseif($QuestionFormulasCheck->type =="count-multiplication-begin-year-length-objective" || $QuestionFormulasCheck->type =="count-multiplication-between-year-length-objective" || $QuestionFormulasCheck->type =="count-multiplication-end-year-length-objective"){
                                    $criteria = new CDbCriteria;
                                    $criteria->addInCondition('survey_group',$group_by);
                                    $criteria->compare('question_id',254);
                                    $SurveyvalueDataCheck = SurveyValue::model()->findAll($criteria);
                                }elseif($QuestionFormulasCheck->type =="count-multiplication-begin-year-length-source" || $QuestionFormulasCheck->type =="count-multiplication-between-year-length-source" || $QuestionFormulasCheck->type =="count-multiplication-end-year-length-source"){
                                    $criteria = new CDbCriteria;
                                    $criteria->addInCondition('survey_group',$group_by);
                                    $criteria->compare('question_id',253);
                                    $SurveyvalueDataCheck = SurveyValue::model()->findAll($criteria);
                                }

                                

                                foreach ($SurveyvalueDataCheck as $keySurveyvalueDataCheck => $valueSurveyvalueDataCheck) {
                                    $criteria = new CDbCriteria;
                                    $criteria->compare('survey_group',$valueSurveyvalueDataCheck->survey_group);
                                    if ($QuestionFormulasCheck->type =="count-multiplication-begin-year-length" || $QuestionFormulasCheck->type =="count-multiplication-between-year-length" || $QuestionFormulasCheck->type =="count-multiplication-begin-year-length-objective" || $QuestionFormulasCheck->type =="count-multiplication-between-year-length-objective" || $QuestionFormulasCheck->type =="count-multiplication-begin-year-length-source" || $QuestionFormulasCheck->type =="count-multiplication-between-year-length-source") {
                                        $criteria->compare('question_id',249);
                                    }elseif($QuestionFormulasCheck->type =="count-multiplication-end-year-length" || $QuestionFormulasCheck->type =="count-multiplication-end-year-length-objective" || $QuestionFormulasCheck->type =="count-multiplication-end-year-length-source"){
                                        $criteria->compare('question_id',258);
                                    }
                                    $group_value = SurveyValue::model()->find($criteria);
                                    $value_check = 0;
                                    if (!empty($group_value)) {
                                        $value_check = $group_value->value;
                                    }
                                    if ($value_check > 0) {
                                        foreach ($QuestionFormulasChoiceCheck as $keyQuestionFormulasChoiceCheck => $valueQuestionFormulasChoiceCheck) {
                                            if ($valueQuestionFormulasChoiceCheck->min == null && $valueQuestionFormulasChoiceCheck->max > 0 && $valueSurveyvalueDataCheck->value <= $valueQuestionFormulasChoiceCheck->max) {
                                                if(!isset($array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice])){
                                                    if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                                        # code...
                                                        $array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice] = $value_check*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]);
                                                    }

                                                }else{
                                                    if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                                        # code...
                                                        $array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice]+= $value_check *($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]);
                                                    }

                                                }
                                            }elseif ($valueQuestionFormulasChoiceCheck->min > 0 && $valueQuestionFormulasChoiceCheck->max > 0 && $valueSurveyvalueDataCheck->value >= $valueQuestionFormulasChoiceCheck->min && $valueSurveyvalueDataCheck->value <= $valueQuestionFormulasChoiceCheck->max) {
                                                if(!isset($array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice])){
                                                    if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                                        # code...
                                                        $array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice] = $value_check *($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]);
                                                    }

                                                }else{
                                                    if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                                        # code...
                                                        $array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice]+= $value_check *($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]);
                                                    }

                                                }
                                            }elseif ($valueQuestionFormulasChoiceCheck->max == null && $valueQuestionFormulasChoiceCheck->min > 0 && $valueSurveyvalueDataCheck->value >= $valueQuestionFormulasChoiceCheck->min) {
                                                if(!isset($array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice])){
                                                    if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                                        # code...
                                                        $array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice] = $value_check *($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]);
                                                    }

                                                }else{
                                                    if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                                        # code...
                                                        $array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice]+= $value_check *(
                                                            $array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]
                                                            /
                                                            $array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])
                                                *(
                                                        $array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]
                                                        /
                                                        $array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]);
                                            }

                                        }
                                    }
                                }
                            }

                        }
                    }
                }elseif($QuestionFormulasCheck->type =="multiplication-begin-year" || $QuestionFormulasCheck->type =="multiplication-between-year" || $QuestionFormulasCheck->type =="multiplication-end-year"){
                    if (!in_array($valueQuestionnaireData->questionnaire_id, $array_check_questionnaire[$QuestionFormulasCheck->formulas_id])) {
                        $criteria = new CDbCriteria;
                        $criteria->compare('province',$valueQuestionnaireData->province);
                        $criteria->compare('district',$valueQuestionnaireData->district);
                        $criteria->compare('subdistrict',$valueQuestionnaireData->subdistrict);
                        $criteria->compare('village',$valueQuestionnaireData->village);
                        $criteria->compare('t.group',$valueQuestionnaireData->group);
                        $criteria->compare('t.sample',$valueQuestionnaireData->sample);
                        $criteria->order = "questionnaire_id ASC";
                        $QuestionnaireDataCheck = Questionnaire::model()->findAll($criteria);


                        $array_questionnaire = array();
                        foreach ($QuestionnaireDataCheck as $keyQuestionnaireDataCheck => $valueQuestionnaireDataCheck) {
                            $array_check_questionnaire[$QuestionFormulasCheck->formulas_id][] = $valueQuestionnaireDataCheck->questionnaire_id;
                            $array_questionnaire[] = $valueQuestionnaireDataCheck->questionnaire_id;
                        }

                        $criteria = new CDbCriteria;
                        $criteria->addInCondition('questionnaire_id',$array_questionnaire);
                        if ($QuestionFormulasCheck->type =="multiplication-begin-year" || $QuestionFormulasCheck->type =="multiplication-between-year") {
                            $criteria->compare('question_id',247);
                            if ($QuestionFormulasCheck->type =="multiplication-begin-year") {
                                $criteria->compare('value',"1");
                            }elseif($QuestionFormulasCheck->type =="multiplication-between-year"){
                                $criteria->addInCondition('value',['2','3']);
                            }
                        }elseif($QuestionFormulasCheck->type =="multiplication-end-year"){
                            $criteria->compare('question_id',258);
                        }
                        $SurveyvalueDataCheck = SurveyValue::model()->findAll($criteria);


                        $group_by = array();
                        foreach ($SurveyvalueDataCheck as $key => $value) {
                            if ($QuestionFormulasCheck->type =="multiplication-begin-year" || $QuestionFormulasCheck->type =="multiplication-between-year") {
                                $group_by[] = $value->survey_group;
                            }elseif($QuestionFormulasCheck->type =="multiplication-end-year"){
                                if ($value->value > 0) {
                                    $group_by[] = $value->survey_group;
                                }
                            }
                        }

                        $criteria=new CDbCriteria;
                        $criteria->compare('formulas_id',$QuestionFormulasCheck->formulas_id);
                        $criteria->order = "choice ASC";
                        $QuestionFormulasChoiceCheck = QuestionFormulasChoice::model()->findAll($criteria);

                        foreach ($QuestionFormulasChoiceCheck as $key => $value) {
                            $criteria = new CDbCriteria;
                            $criteria->addInCondition('survey_group',$group_by);
                            $criteria->compare('question_id',$QuestionFormulasCheck->question_id_ans);
                            $criteria->compare('value',$value->choice);
                            $SurveyvalueDataCheck = SurveyValue::model()->findAll($criteria);
                            if (!empty($SurveyvalueDataCheck)) {
                                $groupCheck = array();
                                foreach ($SurveyvalueDataCheck as $keySurveyvalueDataCheck => $valueSurveyvalueDataCheck) {
                                    $groupCheck[] = $valueSurveyvalueDataCheck->survey_group;
                                }
                                $criteria = new CDbCriteria;
                                $criteria->addInCondition('survey_group',$groupCheck);
                                $criteria->compare('question_id',249);
                                $SurveyvalueDataCheck2 = SurveyValue::model()->findAll($criteria);
                                $groupCheck = array();
                                foreach ($SurveyvalueDataCheck2 as $key2 => $value2) {
                                    if ($value2->value > 0) {
                                        $groupCheck[] = $value2->questionnaire_id;
                                    }
                                }
                                $groupCheck = array_unique($groupCheck);
                                $SurveyvalueDataCheckTotal = count($groupCheck);
                                if ($SurveyvalueDataCheckTotal > 0) {
                                    if(!isset($array_province[$valueQuestionnaireData->province][$keyChoice][$value->choice])){
                                        if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                            $array_province[$valueQuestionnaireData->province][$keyChoice][$value->choice] = 1*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]);
                                        }
                                    }else{
                                        if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                            $array_province[$valueQuestionnaireData->province][$keyChoice][$value->choice] = $array_province[$valueQuestionnaireData->province][$keyChoice][$value->choice] + (1*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }elseif($QuestionFormulasCheck->type =="multiplication-begin-year-length" || $QuestionFormulasCheck->type =="multiplication-between-year-length" || $QuestionFormulasCheck->type =="multiplication-end-year-length"){
                    if (!in_array($valueQuestionnaireData->questionnaire_id, $array_check_questionnaire[$QuestionFormulasCheck->formulas_id])) {
                        $criteria = new CDbCriteria;
                        $criteria->compare('province',$valueQuestionnaireData->province);
                        $criteria->compare('district',$valueQuestionnaireData->district);
                        $criteria->compare('subdistrict',$valueQuestionnaireData->subdistrict);
                        $criteria->compare('village',$valueQuestionnaireData->village);
                        $criteria->compare('t.group',$valueQuestionnaireData->group);
                        $criteria->compare('t.sample',$valueQuestionnaireData->sample);
                        $criteria->order = "questionnaire_id ASC";
                        $QuestionnaireDataCheck = Questionnaire::model()->findAll($criteria);


                        $array_questionnaire = array();
                        foreach ($QuestionnaireDataCheck as $keyQuestionnaireDataCheck => $valueQuestionnaireDataCheck) {
                            $array_check_questionnaire[$QuestionFormulasCheck->formulas_id][] = $valueQuestionnaireDataCheck->questionnaire_id;
                            $array_questionnaire[] = $valueQuestionnaireDataCheck->questionnaire_id;
                        }

                        $criteria = new CDbCriteria;
                        $criteria->addInCondition('questionnaire_id',$array_questionnaire);
                        if ($QuestionFormulasCheck->type =="multiplication-begin-year-length" || $QuestionFormulasCheck->type =="multiplication-between-year-length") {
                            $criteria->compare('question_id',247);
                            if ($QuestionFormulasCheck->type =="multiplication-begin-year-length") {
                                $criteria->compare('value',"1");
                            }elseif($QuestionFormulasCheck->type =="multiplication-between-year-length"){
                                $criteria->addInCondition('value',['2','3']);
                            }
                        }elseif($QuestionFormulasCheck->type =="multiplication-end-year-length"){
                            $criteria->compare('question_id',258);
                        }
                        
                        $SurveyvalueDataCheck = SurveyValue::model()->findAll($criteria);


                        $group_by = array();
                        foreach ($SurveyvalueDataCheck as $key => $value) {
                            if ($QuestionFormulasCheck->type =="multiplication-begin-year-length" || $QuestionFormulasCheck->type =="multiplication-between-year-length") {
                                $group_by[] = $value->survey_group;
                            }elseif($QuestionFormulasCheck->type =="multiplication-end-year-length"){
                                if ($value->value >0) {
                                    $group_by[] = $value->survey_group;
                                }

                            }
                        }

                        $criteria=new CDbCriteria;
                        $criteria->compare('formulas_id',$QuestionFormulasCheck->formulas_id);
                        $criteria->order = "choice ASC";
                        $QuestionFormulasChoiceCheck = QuestionFormulasChoice::model()->findAll($criteria);

                        foreach ($QuestionFormulasChoiceCheck as $key => $value) {
                            $criteria = new CDbCriteria;
                            $criteria->addInCondition('survey_group',$group_by);
                            $criteria->compare('question_id',$QuestionFormulasCheck->question_id_ans);
                                // $criteria->compare('value',$value->choice);
                            $SurveyvalueDataCheck = SurveyValue::model()->findAll($criteria);
                            $array_check_survey = array();
                            foreach ($SurveyvalueDataCheck as $keycheck => $valuecheck) {
                                if ($value->min == null && $value->max > 0 && $valuecheck->value <= $value->max) {
                                    $array_check_survey[] = $valuecheck->survey_group;
                                }elseif($value->min > 0 && $value->max > 0 && $valuecheck->value >= $value->min && $valuecheck->value <= $value->max){
                                    $array_check_survey[] = $valuecheck->survey_group;
                                }elseif($value->max == null && $value->min > 0 && $valuecheck->value >= $value->min){
                                    $array_check_survey[] = $valuecheck->survey_group;
                                }
                            }
                            $criteria = new CDbCriteria;
                            $criteria->addInCondition('survey_group',$array_check_survey);
                            $criteria->compare('question_id',$QuestionFormulasCheck->question_id_ans);
                            $SurveyvalueDataCheck = SurveyValue::model()->findAll($criteria);
                            if (!empty($SurveyvalueDataCheck)) {
                                $groupCheck = array();
                                foreach ($SurveyvalueDataCheck as $keySurveyvalueDataCheck => $valueSurveyvalueDataCheck) {
                                    $groupCheck[] = $valueSurveyvalueDataCheck->survey_group;
                                }
                                $criteria = new CDbCriteria;
                                $criteria->addInCondition('survey_group',$groupCheck);
                                $criteria->compare('question_id',249);
                                $SurveyvalueDataCheck2 = SurveyValue::model()->findAll($criteria);
                                $groupCheck = array();
                                foreach ($SurveyvalueDataCheck2 as $key2 => $value2) {
                                    if ($value2->value > 0) {
                                        $groupCheck[] = $value2->questionnaire_id;
                                    }
                                }
                                $groupCheck = array_unique($groupCheck);
                                $SurveyvalueDataCheckTotal = count($groupCheck);
                                if ($SurveyvalueDataCheckTotal > 0) {
                                    if(!isset($array_province[$valueQuestionnaireData->province][$keyChoice][$value->choice])){
                                        if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                            $array_province[$valueQuestionnaireData->province][$keyChoice][$value->choice] = 1*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]);
                                        }
                                    }else{
                                        if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                            $array_province[$valueQuestionnaireData->province][$keyChoice][$value->choice] = $array_province[$valueQuestionnaireData->province][$keyChoice][$value->choice] + (1*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }elseif($QuestionFormulasCheck->type =="numerate-debt-household"){
                    if (!in_array($valueQuestionnaireData->questionnaire_id, $array_check_questionnaire[$QuestionFormulasCheck->formulas_id])) {
                        $criteria = new CDbCriteria;
                        $criteria->compare('province',$valueQuestionnaireData->province);
                        $criteria->compare('district',$valueQuestionnaireData->district);
                        $criteria->compare('subdistrict',$valueQuestionnaireData->subdistrict);
                        $criteria->compare('village',$valueQuestionnaireData->village);
                        $criteria->compare('t.group',$valueQuestionnaireData->group);
                        $criteria->compare('t.sample',$valueQuestionnaireData->sample);
                        $criteria->order = "questionnaire_id ASC";
                        $QuestionnaireDataCheck = Questionnaire::model()->findAll($criteria);


                        $array_questionnaire = array();
                        foreach ($QuestionnaireDataCheck as $keyQuestionnaireDataCheck => $valueQuestionnaireDataCheck) {
                            $array_check_questionnaire[$QuestionFormulasCheck->formulas_id][] = $valueQuestionnaireDataCheck->questionnaire_id;
                            $array_questionnaire[] = $valueQuestionnaireDataCheck->questionnaire_id;
                        }

                        $criteria = new CDbCriteria;
                        $criteria->addInCondition('questionnaire_id',$array_questionnaire);
                        $criteria->compare('question_id',256);
                        $SurveyvalueDataCheck = SurveyValue::model()->findAll($criteria);
                        $SurveyvalueDataCheckTotal = 0;
                        foreach ($SurveyvalueDataCheck as $key => $value) {
                            if ($value->value > 0) {
                                $SurveyvalueDataCheckTotal =+ $value->value;
                            }
                        }

                        if ($SurveyvalueDataCheckTotal > 0) {
                            if(!isset($array_province[$valueQuestionnaireData->province][$keyChoice][1])){
                                if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                    $array_province[$valueQuestionnaireData->province][$keyChoice][1] = 1*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]);
                                }
                            }else{
                                if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                    $array_province[$valueQuestionnaireData->province][$keyChoice][1] = $array_province[$valueQuestionnaireData->province][$keyChoice][1] + (1*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]));
                                }
                            }
                        }
                    }
                }elseif($QuestionFormulasCheck->type == "count_income_infarm"){
                    if (!in_array($valueQuestionnaireData->questionnaire_id, $array_check_questionnaire[$QuestionFormulasCheck->formulas_id])) {
                        $criteria=new CDbCriteria;
                        $criteria->compare('formulas_id',$QuestionFormulasCheck->formulas_id);
                        $criteria->order = "choice ASC";
                        $QuestionFormulasChoiceCheck = QuestionFormulasChoice::model()->findAll($criteria);
                        foreach ($QuestionFormulasChoiceCheck as $keyQuestionFormulasChoiceCheck => $valueQuestionFormulasChoiceCheck) {
                            $criteria = new CDbCriteria;
                            $criteria->compare('question_id',$QuestionFormulasCheck->question_id_ans);
                            $QuestionCheckGroup = Question::model()->find($criteria);
                            if (!empty($QuestionCheckGroup)) {
                                $criteria = new CDbCriteria;
                                $criteria->compare('question_group_id',$QuestionCheckGroup->question_group_id);
                                $criteria->compare('readable_id',$valueQuestionFormulasChoiceCheck->choice);
                                $QuestionCheck = Question::model()->find($criteria);
                                if (!empty($QuestionCheck)) {
                                    $criteria = new CDbCriteria;
                                    $criteria->compare('province',$valueQuestionnaireData->province);
                                    $criteria->compare('district',$valueQuestionnaireData->district);
                                    $criteria->compare('subdistrict',$valueQuestionnaireData->subdistrict);
                                    $criteria->compare('village',$valueQuestionnaireData->village);
                                    $criteria->compare('t.group',$valueQuestionnaireData->group);
                                    $criteria->order = "questionnaire_id ASC";
                                    $QuestionnaireDataCheck = Questionnaire::model()->findAll($criteria);


                                    $array_questionnaire = array();
                                    foreach ($QuestionnaireDataCheck as $keyQuestionnaireDataCheck => $valueQuestionnaireDataCheck) {
                                        $array_check_questionnaire[$QuestionFormulasCheck->formulas_id][] = $valueQuestionnaireDataCheck->questionnaire_id;
                                        $array_questionnaire[] = $valueQuestionnaireDataCheck->questionnaire_id;
                                    }
                                    $criteria = new CDbCriteria;
                                    $criteria->addInCondition('questionnaire_id',$array_questionnaire);
                                    $criteria->compare('question_id',$QuestionCheck->question_id);
                                    $SurveyvalueDataCheck = SurveyValue::model()->findAll($criteria);


                                    $SurveyvalueDataCheckTotal = 0;
                                    foreach ($SurveyvalueDataCheck as $key => $value) {
                                        if ($value->value > 0) {
                                            $SurveyvalueDataCheckTotal += $value->value;
                                        }
                                    }
                                    if ($SurveyvalueDataCheckTotal > 0) {
                                        if(!isset($array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice])){
                                            if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                                $array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice] = $SurveyvalueDataCheckTotal * ($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]);
                                            }
                                        }else{
                                            if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                                $array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice] = $array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice] + ($SurveyvalueDataCheckTotal * ($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]));
                                            }
                                        }
                                    }

                                }
                            }
                        }
                    }
                }elseif($QuestionFormulasCheck->type == "sum_expend_of_plant"){
                    if (!in_array($valueQuestionnaireData->questionnaire_id, $array_check_questionnaire[$QuestionFormulasCheck->formulas_id])) {
                        $criteria = new CDbCriteria;
                        $criteria->compare('province',$valueQuestionnaireData->province);
                        $criteria->compare('district',$valueQuestionnaireData->district);
                        $criteria->compare('subdistrict',$valueQuestionnaireData->subdistrict);
                        $criteria->compare('village',$valueQuestionnaireData->village);
                        $criteria->compare('t.group',$valueQuestionnaireData->group);
                        $criteria->order = "questionnaire_id ASC";
                        $QuestionnaireDataCheck = Questionnaire::model()->findAll($criteria);

                        $array_questionnaire = array();
                        foreach ($QuestionnaireDataCheck as $keyQuestionnaireDataCheck => $valueQuestionnaireDataCheck) {
                            $array_check_questionnaire[$QuestionFormulasCheck->formulas_id][] = $valueQuestionnaireDataCheck->questionnaire_id;
                            $array_questionnaire[] = $valueQuestionnaireDataCheck->questionnaire_id;
                        }
                        $criteria = new CDbCriteria;
                        $criteria->addInCondition('questionnaire_id',$array_questionnaire);
                        $criteria->compare('question_id',$QuestionFormulasCheck->question_id_ans);
                        $SurveyvalueDataCheck = SurveyValue::model()->findAll($criteria);
                        $group_by = array();
                        foreach ($SurveyvalueDataCheck as $keySurveyvalueDataCheck => $valueSurveyvalueDataCheck) {
                            if (
                                ($valueSurveyvalueDataCheck->value >= 101 && $valueSurveyvalueDataCheck->value <= 102) ||

                                ($valueSurveyvalueDataCheck->value >= 103 && $valueSurveyvalueDataCheck->value <= 104) || 

                                ($valueSurveyvalueDataCheck->value >= 105 && $valueSurveyvalueDataCheck->value <= 114) || 

                                ($valueSurveyvalueDataCheck->value >= 130 && $valueSurveyvalueDataCheck->value <= 131) || 

                                ($valueSurveyvalueDataCheck->value >= 137 && $valueSurveyvalueDataCheck->value <= 141) ||

                                ($valueSurveyvalueDataCheck->value >= 115 && $valueSurveyvalueDataCheck->value <= 129) ||

                                ($valueSurveyvalueDataCheck->value >= 132 && $valueSurveyvalueDataCheck->value <= 136) ||

                                ($valueSurveyvalueDataCheck->value >= 142 && $valueSurveyvalueDataCheck->value <= 199) ||

                                ($valueSurveyvalueDataCheck->value >= 201 && $valueSurveyvalueDataCheck->value <= 213) ||

                                ($valueSurveyvalueDataCheck->value >= 214 && $valueSurveyvalueDataCheck->value <= 299) ||

                                ($valueSurveyvalueDataCheck->value >= 301 && $valueSurveyvalueDataCheck->value <= 304) ||

                                ($valueSurveyvalueDataCheck->value >= 305 && $valueSurveyvalueDataCheck->value <= 399) ||

                                ($valueSurveyvalueDataCheck->value == 434) ||

                                ($valueSurveyvalueDataCheck->value == 465) ||

                                ($valueSurveyvalueDataCheck->value >= 401 && $valueSurveyvalueDataCheck->value <= 433) ||

                                ($valueSurveyvalueDataCheck->value >= 435 && $valueSurveyvalueDataCheck->value <= 464) ||

                                ($valueSurveyvalueDataCheck->value >= 466 && $valueSurveyvalueDataCheck->value <= 487) ||

                                ($valueSurveyvalueDataCheck->value >= 488 && $valueSurveyvalueDataCheck->value <= 499) ||

                                ($valueSurveyvalueDataCheck->value >= 501 && $valueSurveyvalueDataCheck->value <= 549) ||

                                ($valueSurveyvalueDataCheck->value >= 550 && $valueSurveyvalueDataCheck->value <= 599) ||

                                ($valueSurveyvalueDataCheck->value >= 601 && $valueSurveyvalueDataCheck->value <= 618) ||

                                ($valueSurveyvalueDataCheck->value >= 619 && $valueSurveyvalueDataCheck->value <= 699) ||

                                ($valueSurveyvalueDataCheck->value >= 701 && $valueSurveyvalueDataCheck->value <= 703) ||

                                ($valueSurveyvalueDataCheck->value >= 704 && $valueSurveyvalueDataCheck->value <= 799) ||

                                ($valueSurveyvalueDataCheck->value < 101) ||

                                ($valueSurveyvalueDataCheck->value > 799)
                            ){
                                $group_by[] = $valueSurveyvalueDataCheck->survey_group;
                            }
                        }
                        $criteria = new CDbCriteria;
                        $criteria->addInCondition('survey_group',$group_by);
                        $criteria->addInCondition('question_id',[352,353,354,356,357,359,360,362,363,364,366,367,368,369,370,372,374,375,376,377,378,379,381,382,384,385,386,387,388,389]);
                        $SurveyvalueDataCheck = SurveyValue::model()->findAll($criteria);

                        $SurveyvalueDataCheckTotal = 0;
                        foreach ($SurveyvalueDataCheck as $keySurveyvalueDataCheck => $valueSurveyvalueDataCheck) {
                            $SurveyvalueDataCheckTotal += intval($valueSurveyvalueDataCheck->value);
                        }
                        if ($SurveyvalueDataCheckTotal > 0) {
                            if(!isset($array_province[$valueQuestionnaireData->province][$keyChoice][1])){
                                if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                    $array_province[$valueQuestionnaireData->province][$keyChoice][1] = $SurveyvalueDataCheckTotal*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]);
                                }
                            }else{
                                if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                    $array_province[$valueQuestionnaireData->province][$keyChoice][1] = $array_province[$valueQuestionnaireData->province][$keyChoice][1] + ($SurveyvalueDataCheckTotal*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]));
                                }
                            }
                        }
                    }
                    
                }elseif($QuestionFormulasCheck->type == "sum_wage"){
                    if (!in_array($valueQuestionnaireData->questionnaire_id, $array_check_questionnaire[$QuestionFormulasCheck->formulas_id])) {
                        $criteria=new CDbCriteria;
                        $criteria->compare('formulas_id',$QuestionFormulasCheck->formulas_id);
                        $criteria->order = "choice ASC";
                        $QuestionFormulasChoiceCheck = QuestionFormulasChoice::model()->findAll($criteria);
                        foreach ($QuestionFormulasChoiceCheck as $keyQuestionFormulasChoiceCheck => $valueQuestionFormulasChoiceCheck) {
                            $criteria = new CDbCriteria;
                            $criteria->compare('province',$valueQuestionnaireData->province);
                            $criteria->compare('district',$valueQuestionnaireData->district);
                            $criteria->compare('subdistrict',$valueQuestionnaireData->subdistrict);
                            $criteria->compare('village',$valueQuestionnaireData->village);
                            $criteria->compare('t.group',$valueQuestionnaireData->group);
                            $criteria->order = "questionnaire_id ASC";
                            $QuestionnaireDataCheck = Questionnaire::model()->findAll($criteria);

                            $array_questionnaire = array();
                            foreach ($QuestionnaireDataCheck as $keyQuestionnaireDataCheck => $valueQuestionnaireDataCheck) {
                                $array_check_questionnaire[$QuestionFormulasCheck->formulas_id][] = $valueQuestionnaireDataCheck->questionnaire_id;
                                $array_questionnaire[] = $valueQuestionnaireDataCheck->questionnaire_id;
                            }

                            $criteria = new CDbCriteria;
                            $criteria->addInCondition('questionnaire_id',$array_questionnaire);
                            $criteria->compare('question_id',$valueQuestionFormulasChoiceCheck->question_id);
                            $SurveyvalueDataCheck = SurveyValue::model()->findAll($criteria);
                            $SurveyvalueDataCheckTotal = 0;
                            foreach ($SurveyvalueDataCheck as $keySurveyvalueDataCheck => $valueSurveyvalueDataCheck) {
                                $SurveyvalueDataCheckTotal += intval($valueSurveyvalueDataCheck->value);
                            }

                            if ($SurveyvalueDataCheckTotal > 0) {
                                if(!isset($array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice])){
                                    if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                        $array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice] = $SurveyvalueDataCheckTotal*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]);
                                    }
                                }else{
                                    if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                        $array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice] = $array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice] + ($SurveyvalueDataCheckTotal*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]));
                                    }
                                }
                            }
                        }
                    }

                }elseif($QuestionFormulasCheck->type == "sum_expend_of_animal"){
                    if (!in_array($valueQuestionnaireData->questionnaire_id, $array_check_questionnaire[$QuestionFormulasCheck->formulas_id])) {
                        $criteria = new CDbCriteria;
                        $criteria->compare('province',$valueQuestionnaireData->province);
                        $criteria->compare('district',$valueQuestionnaireData->district);
                        $criteria->compare('subdistrict',$valueQuestionnaireData->subdistrict);
                        $criteria->compare('village',$valueQuestionnaireData->village);
                        $criteria->compare('t.group',$valueQuestionnaireData->group);
                        $criteria->order = "questionnaire_id ASC";
                        $QuestionnaireDataCheck = Questionnaire::model()->findAll($criteria);

                        $array_questionnaire = array();
                        foreach ($QuestionnaireDataCheck as $keyQuestionnaireDataCheck => $valueQuestionnaireDataCheck) {
                            $array_check_questionnaire[$QuestionFormulasCheck->formulas_id][] = $valueQuestionnaireDataCheck->questionnaire_id;
                            $array_questionnaire[] = $valueQuestionnaireDataCheck->questionnaire_id;
                        }
                        $criteria = new CDbCriteria;
                        $criteria->addInCondition('questionnaire_id',$array_questionnaire);
                        $criteria->compare('question_id',$QuestionFormulasCheck->question_id_ans);
                        $SurveyvalueDataCheck = SurveyValue::model()->findAll($criteria);
                        $group_by = array();
                        foreach ($SurveyvalueDataCheck as $keySurveyvalueDataCheck => $valueSurveyvalueDataCheck) {
                            if (
                                ($valueSurveyvalueDataCheck->value >= 801 && $valueSurveyvalueDataCheck->value <= 833) ||

                                ($valueSurveyvalueDataCheck->value >= 834 && $valueSurveyvalueDataCheck->value <= 899) || 

                                ($valueSurveyvalueDataCheck->value >= 901 && $valueSurveyvalueDataCheck->value <= 919) || 

                                ($valueSurveyvalueDataCheck->value >= 920 && $valueSurveyvalueDataCheck->value <= 999) || 

                                ($valueSurveyvalueDataCheck->value < 801) ||

                                ($valueSurveyvalueDataCheck->value > 999)
                            ){
                                $group_by[] = $valueSurveyvalueDataCheck->survey_group;
                            }
                        }
                        $criteria = new CDbCriteria;
                        $criteria->addInCondition('survey_group',$group_by);
                        $criteria->addInCondition('question_id',[398,402,403,405,406,407,408,410,411,412,413,414,416,418,419,421,422,423,424,425,426]);
                        $SurveyvalueDataCheck = SurveyValue::model()->findAll($criteria);

                        $SurveyvalueDataCheckTotal = 0;
                        foreach ($SurveyvalueDataCheck as $keySurveyvalueDataCheck => $valueSurveyvalueDataCheck) {
                            $SurveyvalueDataCheckTotal += intval($valueSurveyvalueDataCheck->value);
                        }
                        if ($SurveyvalueDataCheckTotal > 0) {
                            if(!isset($array_province[$valueQuestionnaireData->province][$keyChoice][1])){
                                if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                    $array_province[$valueQuestionnaireData->province][$keyChoice][1] = $SurveyvalueDataCheckTotal*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]);
                                }
                            }else{
                                if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                    $array_province[$valueQuestionnaireData->province][$keyChoice][1] = $array_province[$valueQuestionnaireData->province][$keyChoice][1] + ($SurveyvalueDataCheckTotal*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]));
                                }
                            }
                        }
                    }
                    
                }elseif($QuestionFormulasCheck->type == "group_sum_wage_plant" || $QuestionFormulasCheck->type == "group_sum_item_plant" || $QuestionFormulasCheck->type == "group_sum_all_plant" || $QuestionFormulasCheck->type == "group_sum_wage_animal" || $QuestionFormulasCheck->type == "group_sum_item_animal" || $QuestionFormulasCheck->type == "group_sum_all_animal"){
                    if (!in_array($valueQuestionnaireData->questionnaire_id, $array_check_questionnaire[$QuestionFormulasCheck->formulas_id])) {
                        $criteria=new CDbCriteria;
                        $criteria->compare('formulas_id',$QuestionFormulasCheck->formulas_id);
                        $criteria->order = "choice ASC";
                        $QuestionFormulasChoiceCheck = QuestionFormulasChoice::model()->findAll($criteria);
                        $criteria = new CDbCriteria;
                        $criteria->compare('province',$valueQuestionnaireData->province);
                        $criteria->compare('district',$valueQuestionnaireData->district);
                        $criteria->compare('subdistrict',$valueQuestionnaireData->subdistrict);
                        $criteria->compare('village',$valueQuestionnaireData->village);
                        $criteria->compare('t.group',$valueQuestionnaireData->group);
                        $criteria->order = "questionnaire_id ASC";
                        $QuestionnaireDataCheck = Questionnaire::model()->findAll($criteria);

                        $array_questionnaire = array();
                        foreach ($QuestionnaireDataCheck as $keyQuestionnaireDataCheck => $valueQuestionnaireDataCheck) {
                            $array_check_questionnaire[$QuestionFormulasCheck->formulas_id][] = $valueQuestionnaireDataCheck->questionnaire_id;
                            $array_questionnaire[] = $valueQuestionnaireDataCheck->questionnaire_id;
                        }

                        $criteria = new CDbCriteria;
                        $criteria->addInCondition('questionnaire_id',$array_questionnaire);
                        $criteria->compare('question_id',$QuestionFormulasCheck->question_id_ans);
                        $SurveyvalueDataCheck = SurveyValue::model()->findAll($criteria);
                        foreach ($SurveyvalueDataCheck as $keySurveyvalueDataCheck => $valueSurveyvalueDataCheck) {
                            $criteria = new CDbCriteria;
                            if ($QuestionFormulasCheck->type == "group_sum_wage_plant") {
                                $criteria->addInCondition('question_id',[352,353,354,356,357,359,360,362,363,364,366,367,368,369,370]);
                            }elseif($QuestionFormulasCheck->type == "group_sum_item_plant"){
                                $criteria->addInCondition('question_id',[372,374,375,376,377,378,379,381,382,384,385,386,387,388,389]);
                            }
                            elseif($QuestionFormulasCheck->type == "group_sum_all_plant"){
                                $criteria->addInCondition('question_id',[352,353,354,356,357,359,360,362,363,364,366,367,368,369,370,372,374,375,376,377,378,379,381,382,384,385,386,387,388,389]);
                            }elseif($QuestionFormulasCheck->type == "group_sum_wage_animal"){
                                $criteria->addInCondition('question_id',[398,402,403,405,406,407,408,410,411,412,413]);
                            }elseif($QuestionFormulasCheck->type == "group_sum_item_animal"){
                                $criteria->addInCondition('question_id',[414,416,418,419,421,422,423,424,425,426]);
                            }elseif($QuestionFormulasCheck->type == "group_sum_all_animal"){
                                $criteria->addInCondition('question_id',[398,402,403,405,406,407,408,410,411,412,413,414,416,418,419,421,422,423,424,425,426]);
                            }
                            $criteria->compare('survey_group',$valueSurveyvalueDataCheck->survey_group);
                            $SurveyvalueDataCheck = SurveyValue::model()->findAll($criteria);
                            $SurveyvalueDataCheckTotal = 0;
                            foreach ($SurveyvalueDataCheck as $keySurveyvalueDataCheck => $valueSurveyvalueDataCheckData) {
                                $SurveyvalueDataCheckTotal += intval($valueSurveyvalueDataCheckData->value);
                            }
                            if ($SurveyvalueDataCheckTotal > 0) {
                                foreach ($QuestionFormulasChoiceCheck as $keyQuestionFormulasChoiceCheck => $valueQuestionFormulasChoiceCheck) {
                                    if ($valueQuestionFormulasChoiceCheck->min == null && $valueQuestionFormulasChoiceCheck->max > 0 && $valueSurveyvalueDataCheck->value <= $valueQuestionFormulasChoiceCheck->max) {
                                        if(!isset($array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice])){
                                            if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                                    # code...
                                                $array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice] = $SurveyvalueDataCheckTotal*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]);
                                            }

                                        }else{
                                            if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                                    # code...
                                                $array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice]+= $SurveyvalueDataCheckTotal *($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]);
                                            }

                                        }
                                    }elseif ($valueQuestionFormulasChoiceCheck->min > 0 && $valueQuestionFormulasChoiceCheck->max > 0 && $valueSurveyvalueDataCheck->value >= $valueQuestionFormulasChoiceCheck->min && $valueSurveyvalueDataCheck->value <= $valueQuestionFormulasChoiceCheck->max) {
                                        if(!isset($array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice])){
                                            if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                                    # code...
                                                $array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice] = $SurveyvalueDataCheckTotal *($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]);
                                            }

                                        }else{
                                            if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                                    # code...
                                                $array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice]+= $SurveyvalueDataCheckTotal *($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]);
                                            }

                                        }
                                    }elseif ($valueQuestionFormulasChoiceCheck->max == null && $valueQuestionFormulasChoiceCheck->min > 0 && $valueSurveyvalueDataCheck->value >= $valueQuestionFormulasChoiceCheck->min) {
                                        if(!isset($array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice])){
                                            if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                                    # code...
                                                $array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice] = $SurveyvalueDataCheckTotal *($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]);
                                            }

                                        }else{
                                            if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                                    # code...
                                                $array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice]+= $SurveyvalueDataCheckTotal *(
                                                    $array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]
                                                    /
                                                    $array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])
                                                *(
                                                $array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]
                                                /
                                                $array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]);
                                            }

                                        }
                                    }
                                }
                            }
                        }

                        
                    }

                    
                }elseif($QuestionFormulasCheck->type == "sum_wage_plant" || $QuestionFormulasCheck->type == "sum_wage_animal"){
                    if (!in_array($valueQuestionnaireData->questionnaire_id, $array_check_questionnaire[$QuestionFormulasCheck->formulas_id])) {
                        $criteria=new CDbCriteria;
                        $criteria->compare('formulas_id',$QuestionFormulasCheck->formulas_id);
                        $criteria->order = "choice ASC";
                        $QuestionFormulasChoiceCheck = QuestionFormulasChoice::model()->findAll($criteria);
                        foreach ($QuestionFormulasChoiceCheck as $keyQuestionFormulasChoiceCheck => $valueQuestionFormulasChoiceCheck) {
                            $criteria = new CDbCriteria;
                            $criteria->compare('province',$valueQuestionnaireData->province);
                            $criteria->compare('district',$valueQuestionnaireData->district);
                            $criteria->compare('subdistrict',$valueQuestionnaireData->subdistrict);
                            $criteria->compare('village',$valueQuestionnaireData->village);
                            $criteria->compare('t.group',$valueQuestionnaireData->group);
                            $criteria->order = "questionnaire_id ASC";
                            $QuestionnaireDataCheck = Questionnaire::model()->findAll($criteria);

                            $array_questionnaire = array();
                            foreach ($QuestionnaireDataCheck as $keyQuestionnaireDataCheck => $valueQuestionnaireDataCheck) {
                                $array_check_questionnaire[$QuestionFormulasCheck->formulas_id][] = $valueQuestionnaireDataCheck->questionnaire_id;
                                $array_questionnaire[] = $valueQuestionnaireDataCheck->questionnaire_id;
                            }
                            if ($QuestionFormulasCheck->type == "sum_wage_plant") {
                                if ($valueQuestionFormulasChoiceCheck->choice == 1) {
                                    $criteria = new CDbCriteria;
                                    $criteria->addInCondition('questionnaire_id',$array_questionnaire);
                                    $criteria->addInCondition('question_id',[352,353,354,356,357,359,360,362,363,364,366,367,368,369,370]);
                                    $SurveyvalueDataCheck = SurveyValue::model()->findAll($criteria);
                                }elseif($valueQuestionFormulasChoiceCheck->choice == 2){
                                    $criteria = new CDbCriteria;
                                    $criteria->addInCondition('questionnaire_id',$array_questionnaire);
                                    $criteria->addInCondition('question_id',[372,374,375,376,377,378,379,381,382,384,385,386,387,388,389]);
                                    $SurveyvalueDataCheck = SurveyValue::model()->findAll($criteria);
                                }
                            }elseif($QuestionFormulasCheck->type == "sum_wage_animal"){
                                if ($valueQuestionFormulasChoiceCheck->choice == 1) {
                                    $criteria = new CDbCriteria;
                                    $criteria->addInCondition('questionnaire_id',$array_questionnaire);
                                    $criteria->addInCondition('question_id',[398,402,403,405,406,407,408,410,411,412,413,414]);
                                    $SurveyvalueDataCheck = SurveyValue::model()->findAll($criteria);
                                }elseif($valueQuestionFormulasChoiceCheck->choice == 2){
                                    $criteria = new CDbCriteria;
                                    $criteria->addInCondition('questionnaire_id',$array_questionnaire);
                                    $criteria->addInCondition('question_id',[416,418,419,421,422,423,424,425,426]);
                                    $SurveyvalueDataCheck = SurveyValue::model()->findAll($criteria);
                                }
                            }
                            
                            $SurveyvalueDataCheckTotal = 0;
                            foreach ($SurveyvalueDataCheck as $keySurveyvalueDataCheck => $valueSurveyvalueDataCheck) {
                                $SurveyvalueDataCheckTotal += intval($valueSurveyvalueDataCheck->value);
                            }

                            if ($SurveyvalueDataCheckTotal > 0) {
                                if(!isset($array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice])){
                                    if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                        $array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice] = $SurveyvalueDataCheckTotal*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]);
                                    }
                                }else{
                                    if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                        $array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice] = $array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice] + ($SurveyvalueDataCheckTotal*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]));
                                    }
                                }
                            }
                        }
                    }

                }elseif($QuestionFormulasCheck->type == "sum_plant_all_year"){
                    if (!in_array($valueQuestionnaireData->questionnaire_id, $array_check_questionnaire[$QuestionFormulasCheck->formulas_id])) {
                        $criteria = new CDbCriteria;
                        $criteria->compare('province',$valueQuestionnaireData->province);
                        $criteria->compare('district',$valueQuestionnaireData->district);
                        $criteria->compare('subdistrict',$valueQuestionnaireData->subdistrict);
                        $criteria->compare('village',$valueQuestionnaireData->village);
                        $criteria->compare('t.group',$valueQuestionnaireData->group);
                        $criteria->order = "questionnaire_id ASC";
                        $QuestionnaireDataCheck = Questionnaire::model()->findAll($criteria);

                        $array_questionnaire = array();
                        foreach ($QuestionnaireDataCheck as $keyQuestionnaireDataCheck => $valueQuestionnaireDataCheck) {
                            $array_check_questionnaire[$QuestionFormulasCheck->formulas_id][] = $valueQuestionnaireDataCheck->questionnaire_id;
                            $array_questionnaire[] = $valueQuestionnaireDataCheck->questionnaire_id;
                        }

                        $criteria=new CDbCriteria;
                        $criteria->compare('formulas_id',$QuestionFormulasCheck->formulas_id);
                        $criteria->order = "choice ASC";
                        $QuestionFormulasChoiceCheck = QuestionFormulasChoice::model()->findAll($criteria);
                        
                        $criteria = new CDbCriteria;
                        $criteria->addInCondition('questionnaire_id',$array_questionnaire);
                        $criteria->compare('question_id',$QuestionFormulasCheck->question_id_ans);
                        $SurveyvalueDataCheck = SurveyValue::model()->findAll($criteria);

                            // for loop
                        foreach ($SurveyvalueDataCheck as $keySurveyvalueDataCheck => $valueSurveyvalueDataCheck) {
                            $criteria = new CDbCriteria;
                            $criteria->compare('question_id',18);
                            $criteria->compare('survey_group',$valueSurveyvalueDataCheck->survey_group);
                            $SurveyvalueDataCheckGroup = SurveyValue::model()->find($criteria);
                            $SurveyvalueDataCheckTotal = 0;
                            if ($SurveyvalueDataCheckGroup->value > 0) {
                                $SurveyvalueDataCheckTotal = $SurveyvalueDataCheckGroup->value;
                            }
                            if ($SurveyvalueDataCheckTotal > 0) {
                                foreach ($QuestionFormulasChoiceCheck as $keyQuestionFormulasChoiceCheck => $valueQuestionFormulasChoiceCheck) {
                                    if ($valueQuestionFormulasChoiceCheck->min == null && $valueQuestionFormulasChoiceCheck->max > 0 && $valueSurveyvalueDataCheck->value <= $valueQuestionFormulasChoiceCheck->max) {
                                        if(!isset($array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice])){
                                            if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                                    # code...
                                                $array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice] = $SurveyvalueDataCheckTotal*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]);
                                            }

                                        }else{
                                            if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                                    # code...
                                                $array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice]+= $SurveyvalueDataCheckTotal *($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]);
                                            }

                                        }
                                    }elseif ($valueQuestionFormulasChoiceCheck->min > 0 && $valueQuestionFormulasChoiceCheck->max > 0 && $valueSurveyvalueDataCheck->value >= $valueQuestionFormulasChoiceCheck->min && $valueSurveyvalueDataCheck->value <= $valueQuestionFormulasChoiceCheck->max) {
                                        if(!isset($array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice])){
                                            if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                                    # code...
                                                $array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice] = $SurveyvalueDataCheckTotal *($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]);
                                            }

                                        }else{
                                            if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                                    # code...
                                                $array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice]+= $SurveyvalueDataCheckTotal *($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]);
                                            }

                                        }
                                    }elseif ($valueQuestionFormulasChoiceCheck->max == null && $valueQuestionFormulasChoiceCheck->min > 0 && $valueSurveyvalueDataCheck->value >= $valueQuestionFormulasChoiceCheck->min) {
                                        if(!isset($array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice])){
                                            if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                                    # code...
                                                $array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice] = $SurveyvalueDataCheckTotal *($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]);
                                            }

                                        }else{
                                            if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                                    # code...
                                                $array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice]+= $SurveyvalueDataCheckTotal *(
                                                    $array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]
                                                    /
                                                    $array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])
                                                *(
                                                $array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]
                                                /
                                                $array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]);
                                            }

                                        }
                                    }
                                }
                            }
                            
                            } //for loop

                            $criteria = new CDbCriteria;
                            $criteria->addInCondition('questionnaire_id',$array_questionnaire);
                            $criteria->compare('question_id',38);
                            $SurveyvalueDataCheck = SurveyValue::model()->findAll($criteria);

                            // for loop
                            foreach ($SurveyvalueDataCheck as $keySurveyvalueDataCheck => $valueSurveyvalueDataCheck) {
                                $criteria = new CDbCriteria;
                                $criteria->compare('question_id',42);
                                $criteria->compare('survey_group',$valueSurveyvalueDataCheck->survey_group);
                                $SurveyvalueDataCheckGroup = SurveyValue::model()->find($criteria);
                                $SurveyvalueDataCheckTotal = 0;
                                if ($SurveyvalueDataCheckGroup->value > 0) {
                                    $SurveyvalueDataCheckTotal = $SurveyvalueDataCheckGroup->value;
                                }
                                if ($SurveyvalueDataCheckTotal > 0) {
                                    foreach ($QuestionFormulasChoiceCheck as $keyQuestionFormulasChoiceCheck => $valueQuestionFormulasChoiceCheck) {
                                        if ($valueQuestionFormulasChoiceCheck->min == null && $valueQuestionFormulasChoiceCheck->max > 0 && $valueSurveyvalueDataCheck->value <= $valueQuestionFormulasChoiceCheck->max) {
                                            if(!isset($array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice])){
                                                if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                                    # code...
                                                    $array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice] = $SurveyvalueDataCheckTotal*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]);
                                                }

                                            }else{
                                                if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                                    # code...
                                                    $array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice]+= $SurveyvalueDataCheckTotal *($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]);
                                                }

                                            }
                                        }elseif ($valueQuestionFormulasChoiceCheck->min > 0 && $valueQuestionFormulasChoiceCheck->max > 0 && $valueSurveyvalueDataCheck->value >= $valueQuestionFormulasChoiceCheck->min && $valueSurveyvalueDataCheck->value <= $valueQuestionFormulasChoiceCheck->max) {
                                            if(!isset($array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice])){
                                                if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                                    # code...
                                                    $array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice] = $SurveyvalueDataCheckTotal *($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]);
                                                }

                                            }else{
                                                if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                                    # code...
                                                    $array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice]+= $SurveyvalueDataCheckTotal *($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]);
                                                }

                                            }
                                        }elseif ($valueQuestionFormulasChoiceCheck->max == null && $valueQuestionFormulasChoiceCheck->min > 0 && $valueSurveyvalueDataCheck->value >= $valueQuestionFormulasChoiceCheck->min) {
                                            if(!isset($array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice])){
                                                if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                                    # code...
                                                    $array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice] = $SurveyvalueDataCheckTotal *($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]);
                                                }

                                            }else{
                                                if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                                    # code...
                                                    $array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice]+= $SurveyvalueDataCheckTotal *(
                                                        $array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]
                                                        /
                                                        $array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])
                                                *(
                                                    $array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]
                                                    /
                                                    $array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]);
                                            }
                                            
                                        }
                                    }
                                }
                            }
                            
                            } //for loop
                        }
                    }elseif($QuestionFormulasCheck->type == "sum_plant_question"){
                        if (!in_array($valueQuestionnaireData->questionnaire_id, $array_check_questionnaire[$QuestionFormulasCheck->formulas_id])) {
                            $criteria = new CDbCriteria;
                            $criteria->compare('province',$valueQuestionnaireData->province);
                            $criteria->compare('district',$valueQuestionnaireData->district);
                            $criteria->compare('subdistrict',$valueQuestionnaireData->subdistrict);
                            $criteria->compare('village',$valueQuestionnaireData->village);
                            $criteria->compare('t.group',$valueQuestionnaireData->group);
                            $criteria->order = "questionnaire_id ASC";
                            $QuestionnaireDataCheck = Questionnaire::model()->findAll($criteria);

                            $array_questionnaire = array();
                            foreach ($QuestionnaireDataCheck as $keyQuestionnaireDataCheck => $valueQuestionnaireDataCheck) {
                                $array_check_questionnaire[$QuestionFormulasCheck->formulas_id][] = $valueQuestionnaireDataCheck->questionnaire_id;
                                $array_questionnaire[] = $valueQuestionnaireDataCheck->questionnaire_id;
                            }
                            $criteria=new CDbCriteria;
                            $criteria->compare('formulas_id',$QuestionFormulasCheck->formulas_id);
                            $criteria->order = "choice ASC";
                            $QuestionFormulasChoiceCheck = QuestionFormulasChoice::model()->findAll($criteria);
                            
                            $criteria = new CDbCriteria;
                            $criteria->addInCondition('questionnaire_id',$array_questionnaire);
                            $criteria->compare('question_id',$QuestionFormulasCheck->question_id_ans);
                            $SurveyvalueDataCheck = SurveyValue::model()->findAll($criteria);
                            foreach ($SurveyvalueDataCheck as $keySurveyvalueDataCheck => $valueSurveyvalueDataCheck) {
                                foreach ($QuestionFormulasChoiceCheck as $keyQuestionFormulasChoiceCheck => $valueQuestionFormulasChoiceCheck) {
                                    if ($valueQuestionFormulasChoiceCheck->choice == "1") {
                                        $criteria = new CDbCriteria;
                                        $criteria->compare('question_id',21);
                                        $criteria->compare('survey_group',$valueSurveyvalueDataCheck->survey_group);
                                        $SurveyvalueDataCheckData1 = SurveyValue::model()->find($criteria);
                                        $criteria = new CDbCriteria;
                                        $criteria->compare('question_id',27);
                                        $criteria->compare('survey_group',$valueSurveyvalueDataCheck->survey_group);
                                        $SurveyvalueDataCheckData2 = SurveyValue::model()->find($criteria);
                                        if (!empty($SurveyvalueDataCheckData2)) {
                                            $SurveyvalueDataCheckData2 = $SurveyvalueDataCheckData2->value;
                                        }
                                    }elseif($valueQuestionFormulasChoiceCheck->choice == "2"){
                                        $criteria = new CDbCriteria;
                                        $criteria->compare('question_id',22);
                                        $criteria->compare('survey_group',$valueSurveyvalueDataCheck->survey_group);
                                        $SurveyvalueDataCheckData1 = SurveyValue::model()->find($criteria);
                                        $criteria = new CDbCriteria;
                                        $criteria->compare('question_id',27);
                                        $criteria->compare('survey_group',$valueSurveyvalueDataCheck->survey_group);
                                        $SurveyvalueDataCheckData2 = SurveyValue::model()->find($criteria);
                                        if (!empty($SurveyvalueDataCheckData2)) {
                                            $SurveyvalueDataCheckData2 = $SurveyvalueDataCheckData2->value;
                                        }
                                    }elseif($valueQuestionFormulasChoiceCheck->choice == "3"){
                                        $criteria = new CDbCriteria;
                                        $criteria->compare('question_id',23);
                                        $criteria->compare('survey_group',$valueSurveyvalueDataCheck->survey_group);
                                        $SurveyvalueDataCheckData1 = SurveyValue::model()->find($criteria);
                                        $criteria = new CDbCriteria;
                                        $criteria->compare('question_id',27);
                                        $criteria->compare('survey_group',$valueSurveyvalueDataCheck->survey_group);
                                        $SurveyvalueDataCheckData2 = SurveyValue::model()->find($criteria);
                                        if (!empty($SurveyvalueDataCheckData2)) {
                                            $SurveyvalueDataCheckData2 = $SurveyvalueDataCheckData2->value;
                                        }
                                    }
                                    $SurveyvalueDataCheckTotal = 0;
                                    if (!empty($SurveyvalueDataCheckData1)) {
                                        $SurveyvalueDataCheckTotal = $SurveyvalueDataCheckData1->value;
                                    }
                                    if (!empty($SurveyvalueDataCheckData2)) {
                                        $SurveyvalueDataCheckTotal = $SurveyvalueDataCheckTotal * $SurveyvalueDataCheckData2;
                                    }
                                    if ($SurveyvalueDataCheckTotal > 0) {
                                        if(!isset($array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice])){
                                            if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                                $array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice] = $SurveyvalueDataCheckTotal*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]);
                                            }
                                        }else{
                                            if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                                $array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice] = $array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice] + ($SurveyvalueDataCheckTotal*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]));
                                            }
                                        }
                                    }
                                }
                            }
                            $criteria = new CDbCriteria;
                            $criteria->addInCondition('questionnaire_id',$array_questionnaire);
                            $criteria->compare('question_id',38);
                            $SurveyvalueDataCheck = SurveyValue::model()->findAll($criteria);
                            foreach ($SurveyvalueDataCheck as $keySurveyvalueDataCheck => $valueSurveyvalueDataCheck) {
                                foreach ($QuestionFormulasChoiceCheck as $keyQuestionFormulasChoiceCheck => $valueQuestionFormulasChoiceCheck) {
                                    if ($valueQuestionFormulasChoiceCheck->choice == "1") {
                                        $criteria = new CDbCriteria;
                                        $criteria->compare('question_id',45);
                                        $criteria->compare('survey_group',$valueSurveyvalueDataCheck->survey_group);
                                        $SurveyvalueDataCheckData = SurveyValue::model()->find($criteria);
                                    }elseif($valueQuestionFormulasChoiceCheck->choice == "2"){
                                        $criteria = new CDbCriteria;
                                        $criteria->compare('question_id',46);
                                        $criteria->compare('survey_group',$valueSurveyvalueDataCheck->survey_group);
                                        $SurveyvalueDataCheckData = SurveyValue::model()->find($criteria);
                                    }elseif($valueQuestionFormulasChoiceCheck->choice == "3"){
                                        $criteria = new CDbCriteria;
                                        $criteria->compare('question_id',47);
                                        $criteria->compare('survey_group',$valueSurveyvalueDataCheck->survey_group);
                                        $SurveyvalueDataCheckData = SurveyValue::model()->find($criteria);
                                    }
                                    $SurveyvalueDataCheckTotal = 0;
                                    if (!empty($SurveyvalueDataCheckData)) {
                                        if ($SurveyvalueDataCheckData->value > 0) {
                                            $SurveyvalueDataCheckTotal = $SurveyvalueDataCheckData->value;
                                        }
                                    }
                                    if ($SurveyvalueDataCheckTotal > 0) {
                                        if(!isset($array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice])){
                                            if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                                $array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice] = $SurveyvalueDataCheckTotal*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]);
                                            }
                                        }else{
                                            if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                                $array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice] = $array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice] + ($SurveyvalueDataCheckTotal*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]));
                                            }
                                        }
                                    }
                                }
                            }
                        }

                    }elseif($QuestionFormulasCheck->type == "sum_muad_9" || $QuestionFormulasCheck->type == "num_sum_muad_9" || $QuestionFormulasCheck->type == "cost_sum_muad_9"){
                        if (!in_array($valueQuestionnaireData->questionnaire_id, $array_check_questionnaire[$QuestionFormulasCheck->formulas_id])) {
                            $criteria = new CDbCriteria;
                            $criteria->compare('province',$valueQuestionnaireData->province);
                            $criteria->compare('district',$valueQuestionnaireData->district);
                            $criteria->compare('subdistrict',$valueQuestionnaireData->subdistrict);
                            $criteria->compare('village',$valueQuestionnaireData->village);
                            $criteria->compare('t.group',$valueQuestionnaireData->group);
                            $criteria->order = "questionnaire_id ASC";
                            $QuestionnaireDataCheck = Questionnaire::model()->findAll($criteria);

                            $array_questionnaire = array();
                            foreach ($QuestionnaireDataCheck as $keyQuestionnaireDataCheck => $valueQuestionnaireDataCheck) {
                                $array_check_questionnaire[$QuestionFormulasCheck->formulas_id][] = $valueQuestionnaireDataCheck->questionnaire_id;
                                $array_questionnaire[] = $valueQuestionnaireDataCheck->questionnaire_id;
                            }


                            $criteria=new CDbCriteria;
                            $criteria->compare('formulas_id',$QuestionFormulasCheck->formulas_id);
                            $criteria->order = "choice ASC";
                            $QuestionFormulasChoiceCheck = QuestionFormulasChoice::model()->findAll($criteria);
                            foreach ($QuestionFormulasChoiceCheck as $keyQuestionFormulasChoiceCheck => $valueQuestionFormulasChoiceCheck) {
                                $criteria = new CDbCriteria;
                                $criteria->compare('question_id',$QuestionFormulasCheck->question_id_ans);
                                $QuestionCheckGroup = Question::model()->find($criteria);
                                if (!empty($QuestionCheckGroup)) {
                                    $criteria = new CDbCriteria;
                                    $criteria->compare('question_group_id',$QuestionCheckGroup->question_group_id);
                                    $criteria->compare('readable_id',$valueQuestionFormulasChoiceCheck->choice);
                                    $QuestionCheck = Question::model()->find($criteria);
                                    if (!empty($QuestionCheck)) {
                                        if ($QuestionCheck->type == "ques-num-value") {
                                            $criteria = new CDbCriteria;
                                            $criteria->compare('options',$QuestionCheck->question_id);
                                            if ($QuestionFormulasCheck->type == "num_sum_muad_9") {
                                                $criteria->compare('type',"parent-ques-num");
                                            }elseif ($QuestionFormulasCheck->type == "cost_sum_muad_9") {
                                                $criteria->compare('type',"parent-ques-value");
                                            }
                                            $QuestionCheck = Question::model()->find($criteria);
                                        }
                                        $criteria = new CDbCriteria;
                                        $criteria->addInCondition('questionnaire_id',$array_questionnaire);
                                        $criteria->compare('question_id',$QuestionCheck->question_id);
                                        $SurveyvalueDataCheck = SurveyValue::model()->findAll($criteria);

                                        $SurveyvalueDataCheckTotal = 0;
                                        foreach ($SurveyvalueDataCheck as $keySurveyvalueDataCheck => $valueSurveyvalueDataCheck) {
                                            if ($valueSurveyvalueDataCheck->value > 0) {
                                                $SurveyvalueDataCheckTotal += $valueSurveyvalueDataCheck->value;
                                            }
                                        }
                                        if ($SurveyvalueDataCheckTotal > 0) {
                                            if(!isset($array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice])){
                                                if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                                    $array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice] = $SurveyvalueDataCheckTotal * ($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]);
                                                }
                                            }else{
                                                if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                                    $array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice] = $array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice] + ($SurveyvalueDataCheckTotal * ($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]));
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            
                        }
                    }elseif($QuestionFormulasCheck->type == "sum_all_muad_9"){
                        if (!in_array($valueQuestionnaireData->questionnaire_id, $array_check_questionnaire[$QuestionFormulasCheck->formulas_id])) {
                            $criteria = new CDbCriteria;
                            $criteria->compare('province',$valueQuestionnaireData->province);
                            $criteria->compare('district',$valueQuestionnaireData->district);
                            $criteria->compare('subdistrict',$valueQuestionnaireData->subdistrict);
                            $criteria->compare('village',$valueQuestionnaireData->village);
                            $criteria->compare('t.group',$valueQuestionnaireData->group);
                            $criteria->order = "questionnaire_id ASC";
                            $QuestionnaireDataCheck = Questionnaire::model()->findAll($criteria);

                            $array_questionnaire = array();
                            foreach ($QuestionnaireDataCheck as $keyQuestionnaireDataCheck => $valueQuestionnaireDataCheck) {
                                $array_check_questionnaire[$QuestionFormulasCheck->formulas_id][] = $valueQuestionnaireDataCheck->questionnaire_id;
                                $array_questionnaire[] = $valueQuestionnaireDataCheck->questionnaire_id;
                            }


                            $criteria = new CDbCriteria;
                            $criteria->compare('question_id',$QuestionFormulasCheck->question_id_ans);
                            $QuestionCheckGroup = Question::model()->find($criteria);
                            if (!empty($QuestionCheckGroup)) {
                                $criteria = new CDbCriteria;
                                $criteria->compare('question_group_id',$QuestionCheckGroup->question_group_id);
                                $criteria->addInCondition('type',['ques-ans','ques-num-value']);
                                $QuestionCheck = Question::model()->findAll($criteria);
                                foreach ($QuestionCheck as $key => $value) {
                                    if (!empty($value)) {
                                        if ($value->type == "ques-num-value") {
                                            $criteria = new CDbCriteria;
                                            $criteria->compare('question_group_id',$QuestionCheckGroup->question_group_id);
                                            $criteria->compare('options',$value->question_id);
                                            $criteria->compare('type',"parent-ques-value");
                                            $value = Question::model()->find($criteria);
                                        }
                                        $criteria = new CDbCriteria;
                                        $criteria->addInCondition('questionnaire_id',$array_questionnaire);
                                        $criteria->compare('question_id',$value->question_id);
                                        $SurveyvalueDataCheck = SurveyValue::model()->findAll($criteria);

                                        $SurveyvalueDataCheckTotal = 0;
                                        foreach ($SurveyvalueDataCheck as $keySurveyvalueDataCheck => $valueSurveyvalueDataCheck) {
                                            if ($valueSurveyvalueDataCheck->value > 0) {
                                                $SurveyvalueDataCheckTotal += $valueSurveyvalueDataCheck->value;
                                            }
                                        }
                                        if ($SurveyvalueDataCheckTotal > 0) {
                                            if(!isset($array_province[$valueQuestionnaireData->province][$keyChoice][1])){
                                                if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                                    $array_province[$valueQuestionnaireData->province][$keyChoice][1] = $SurveyvalueDataCheckTotal * ($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]);
                                                }
                                            }else{
                                                if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                                    $array_province[$valueQuestionnaireData->province][$keyChoice][1] = $array_province[$valueQuestionnaireData->province][$keyChoice][1] + ($SurveyvalueDataCheckTotal * ($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]));
                                                }
                                            }
                                        }
                                    }
                                }
                                
                            }
                            
                        }
                    }elseif($QuestionFormulasCheck->type == "sum_market"){
                        if (!in_array($valueQuestionnaireData->questionnaire_id, $array_check_questionnaire[$QuestionFormulasCheck->formulas_id])) {
                            $criteria = new CDbCriteria;
                            $criteria->compare('province',$valueQuestionnaireData->province);
                            $criteria->compare('district',$valueQuestionnaireData->district);
                            $criteria->compare('subdistrict',$valueQuestionnaireData->subdistrict);
                            $criteria->compare('village',$valueQuestionnaireData->village);
                            $criteria->compare('t.group',$valueQuestionnaireData->group);
                            $criteria->order = "questionnaire_id ASC";
                            $QuestionnaireDataCheck = Questionnaire::model()->findAll($criteria);

                            $array_questionnaire = array();
                            foreach ($QuestionnaireDataCheck as $keyQuestionnaireDataCheck => $valueQuestionnaireDataCheck) {
                                $array_check_questionnaire[$QuestionFormulasCheck->formulas_id][] = $valueQuestionnaireDataCheck->questionnaire_id;
                                $array_questionnaire[] = $valueQuestionnaireDataCheck->questionnaire_id;
                            }
                            $criteria=new CDbCriteria;
                            $criteria->compare('formulas_id',$QuestionFormulasCheck->formulas_id);
                            $criteria->order = "choice ASC";
                            $QuestionFormulasChoiceCheck = QuestionFormulasChoice::model()->findAll($criteria);

                            $criteria = new CDbCriteria;
                            $criteria->addInCondition('questionnaire_id',$array_questionnaire);
                            $criteria->compare('question_id',$QuestionFormulasCheck->question_id_ans);
                            $SurveyvalueDataCheck = SurveyValue::model()->findAll($criteria);

                            foreach ($SurveyvalueDataCheck as $keySurveyvalueDataCheck => $valueSurveyvalueDataCheck) {
                                if (!empty($valueSurveyvalueDataCheck->value)) {
                                    $criteria = new CDbCriteria;
                                    $criteria->compare('question_id',25);
                                    $criteria->compare('survey_group',$valueSurveyvalueDataCheck->survey_group);
                                    $SurveyvalueDataCheckData1 = SurveyValue::model()->find($criteria);
                                    $criteria = new CDbCriteria;
                                    $criteria->compare('question_id',27);
                                    $criteria->compare('survey_group',$valueSurveyvalueDataCheck->survey_group);
                                    $SurveyvalueDataCheckData2 = SurveyValue::model()->find($criteria);

                                    $SurveyvalueDataCheckTotal = 0;
                                    if (!empty($SurveyvalueDataCheckData1)) {
                                        $SurveyvalueDataCheckTotal = $SurveyvalueDataCheckData1->value;
                                    }
                                    if (!empty($SurveyvalueDataCheckData2)) {
                                        $SurveyvalueDataCheckTotal = $SurveyvalueDataCheckTotal * $SurveyvalueDataCheckData2->value;
                                    }
                                    if ($SurveyvalueDataCheckTotal > 0) {
                                        if(!isset($array_province[$valueQuestionnaireData->province][$keyChoice][$valueSurveyvalueDataCheck->value])){
                                            if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                                $array_province[$valueQuestionnaireData->province][$keyChoice][$valueSurveyvalueDataCheck->value] = $SurveyvalueDataCheckTotal*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]);
                                            }
                                        }else{
                                            if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                                $array_province[$valueQuestionnaireData->province][$keyChoice][$valueSurveyvalueDataCheck->value] = $array_province[$valueQuestionnaireData->province][$keyChoice][$valueSurveyvalueDataCheck->value] + ($SurveyvalueDataCheckTotal*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]));
                                            }
                                        }
                                    }
                                }
                            }

                            $criteria = new CDbCriteria;
                            $criteria->addInCondition('questionnaire_id',$array_questionnaire);
                            $criteria->compare('question_id',50);
                            $SurveyvalueDataCheck = SurveyValue::model()->findAll($criteria);

                            foreach ($SurveyvalueDataCheck as $keySurveyvalueDataCheck => $valueSurveyvalueDataCheck) {
                                if (!empty($valueSurveyvalueDataCheck->value)) {
                                    $criteria = new CDbCriteria;
                                    $criteria->compare('question_id',92);
                                    $criteria->compare('survey_group',$valueSurveyvalueDataCheck->survey_group);
                                    $SurveyvalueDataCheckData1 = SurveyValue::model()->find($criteria);

                                    $SurveyvalueDataCheckTotal = 0;
                                    if (!empty($SurveyvalueDataCheckData1)) {
                                        $SurveyvalueDataCheckTotal = $SurveyvalueDataCheckData1->value;
                                    }
                                    if ($SurveyvalueDataCheckTotal > 0) {
                                        if(!isset($array_province[$valueQuestionnaireData->province][$keyChoice][$valueSurveyvalueDataCheck->value])){
                                            if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                                $array_province[$valueQuestionnaireData->province][$keyChoice][$valueSurveyvalueDataCheck->value] = $SurveyvalueDataCheckTotal*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]);
                                            }
                                        }else{
                                            if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                                $array_province[$valueQuestionnaireData->province][$keyChoice][$valueSurveyvalueDataCheck->value] = $array_province[$valueQuestionnaireData->province][$keyChoice][$valueSurveyvalueDataCheck->value] + ($SurveyvalueDataCheckTotal*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]));
                                            }
                                        }
                                    }
                                }
                            }

                        }

                    }elseif($QuestionFormulasCheck->type == "supply_distibution"){
                        if (!in_array($valueQuestionnaireData->questionnaire_id, $array_check_questionnaire[$QuestionFormulasCheck->formulas_id])) {
                            $criteria = new CDbCriteria;
                            $criteria->compare('province',$valueQuestionnaireData->province);
                            $criteria->compare('district',$valueQuestionnaireData->district);
                            $criteria->compare('subdistrict',$valueQuestionnaireData->subdistrict);
                            $criteria->compare('village',$valueQuestionnaireData->village);
                            $criteria->compare('t.group',$valueQuestionnaireData->group);
                            $criteria->order = "questionnaire_id ASC";
                            $QuestionnaireDataCheck = Questionnaire::model()->findAll($criteria);

                            $array_questionnaire = array();
                            foreach ($QuestionnaireDataCheck as $keyQuestionnaireDataCheck => $valueQuestionnaireDataCheck) {
                                $array_check_questionnaire[$QuestionFormulasCheck->formulas_id][] = $valueQuestionnaireDataCheck->questionnaire_id;
                                $array_questionnaire[] = $valueQuestionnaireDataCheck->questionnaire_id;
                            }
                            $criteria=new CDbCriteria;
                            $criteria->compare('formulas_id',$QuestionFormulasCheck->formulas_id);
                            $criteria->order = "choice ASC";
                            $QuestionFormulasChoiceCheck = QuestionFormulasChoice::model()->findAll($criteria);

                            $criteria = new CDbCriteria;
                            $criteria->addInCondition('questionnaire_id',$array_questionnaire);
                            $criteria->compare('question_id',$QuestionFormulasCheck->question_id_ans);
                            $SurveyvalueDataCheck = SurveyValue::model()->findAll($criteria);
                            foreach ($SurveyvalueDataCheck as $keySurveyvalueDataCheck => $valueSurveyvalueDataCheck) {
                                foreach ($QuestionFormulasChoiceCheck as $keyQuestionFormulasChoiceCheck => $valueQuestionFormulasChoiceCheck) {
                                    $criteria = new CDbCriteria;
                                    $criteria->compare('question_id',$valueQuestionFormulasChoiceCheck->question_id);
                                    $criteria->compare('survey_group',$valueSurveyvalueDataCheck->survey_group);
                                    $SurveyvalueDataCheckData1 = SurveyValue::model()->find($criteria);
                                    $criteria = new CDbCriteria;
                                    $criteria->compare('question_id',27);
                                    $criteria->compare('survey_group',$valueSurveyvalueDataCheck->survey_group);
                                    $SurveyvalueDataCheckData2 = SurveyValue::model()->find($criteria);

                                    $SurveyvalueDataCheckTotal = 0;
                                    if (!empty($SurveyvalueDataCheckData1)) {
                                        $SurveyvalueDataCheckTotal = $SurveyvalueDataCheckData1->value;
                                    }
                                    if (!empty($SurveyvalueDataCheckData2)) {
                                        $SurveyvalueDataCheckTotal = $SurveyvalueDataCheckTotal * $SurveyvalueDataCheckData2->value;
                                    }
                                    if ($SurveyvalueDataCheckTotal > 0) {
                                        if(!isset($array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice])){
                                            if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                                $array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice] = $SurveyvalueDataCheckTotal*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]);
                                            }
                                        }else{
                                            if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                                $array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice] = $array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice] + ($SurveyvalueDataCheckTotal*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]));
                                            }
                                        }
                                    }
                                }
                            }

                            $criteria = new CDbCriteria;
                            $criteria->addInCondition('questionnaire_id',$array_questionnaire);
                            $criteria->compare('question_id',38);
                            $SurveyvalueDataCheck = SurveyValue::model()->findAll($criteria);
                            foreach ($SurveyvalueDataCheck as $keySurveyvalueDataCheck => $valueSurveyvalueDataCheck) {
                                foreach ($QuestionFormulasChoiceCheck as $keyQuestionFormulasChoiceCheck => $valueQuestionFormulasChoiceCheck) {
                                    $question_check = $valueQuestionFormulasChoiceCheck->question_id;
                                    if ($question_check == "334") {
                                        $question_check = 53;
                                    }elseif($question_check == "335"){
                                        $question_check = 54;

                                    }elseif($question_check == "336"){
                                        $question_check = 55;
                                        
                                    }elseif($question_check == "337"){
                                        $question_check = 56;
                                        
                                    }elseif($question_check == "338"){
                                        $question_check = 57;
                                        
                                    }elseif($question_check == "393"){
                                        $question_check = 58;
                                        
                                    }elseif($question_check == "339"){
                                        $question_check = 59;
                                        
                                    }elseif($question_check == "91"){
                                        $question_check = 60;
                                    }

                                    $criteria = new CDbCriteria;
                                    $criteria->compare('question_id',$question_check);
                                    $criteria->compare('survey_group',$valueSurveyvalueDataCheck->survey_group);
                                    $SurveyvalueDataCheckData1 = SurveyValue::model()->find($criteria);
                                    

                                    $SurveyvalueDataCheckTotal = 0;
                                    if (!empty($SurveyvalueDataCheckData1)) {
                                        $SurveyvalueDataCheckTotal = $SurveyvalueDataCheckData1->value;
                                    }
                                    if ($SurveyvalueDataCheckTotal > 0) {
                                        if(!isset($array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice])){
                                            if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                                $array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice] = $SurveyvalueDataCheckTotal*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]);
                                            }
                                        }else{
                                            if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                                $array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice] = $array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice] + ($SurveyvalueDataCheckTotal*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]));
                                            }
                                        }
                                    }
                                }
                            }

                        }
                    }elseif($QuestionFormulasCheck->type == "sum_animal_question"){
                        if (!in_array($valueQuestionnaireData->questionnaire_id, $array_check_questionnaire[$QuestionFormulasCheck->formulas_id])) {
                            $criteria = new CDbCriteria;
                            $criteria->compare('province',$valueQuestionnaireData->province);
                            $criteria->compare('district',$valueQuestionnaireData->district);
                            $criteria->compare('subdistrict',$valueQuestionnaireData->subdistrict);
                            $criteria->compare('village',$valueQuestionnaireData->village);
                            $criteria->compare('t.group',$valueQuestionnaireData->group);
                            $criteria->order = "questionnaire_id ASC";
                            $QuestionnaireDataCheck = Questionnaire::model()->findAll($criteria);

                            $array_questionnaire = array();
                            foreach ($QuestionnaireDataCheck as $keyQuestionnaireDataCheck => $valueQuestionnaireDataCheck) {
                                $array_check_questionnaire[$QuestionFormulasCheck->formulas_id][] = $valueQuestionnaireDataCheck->questionnaire_id;
                                $array_questionnaire[] = $valueQuestionnaireDataCheck->questionnaire_id;
                            }
                            $criteria=new CDbCriteria;
                            $criteria->compare('formulas_id',$QuestionFormulasCheck->formulas_id);
                            $criteria->order = "choice ASC";
                            $QuestionFormulasChoiceCheck = QuestionFormulasChoice::model()->findAll($criteria);

                            $criteria = new CDbCriteria;
                            $criteria->addInCondition('questionnaire_id',$array_questionnaire);
                            $criteria->compare('question_id',$QuestionFormulasCheck->question_id_ans);
                            $SurveyvalueDataCheck = SurveyValue::model()->findAll($criteria);
                            $groupCheck = array();
                            foreach ($SurveyvalueDataCheck as $keySurveyvalueDataCheck => $valueSurveyvalueDataCheck) {
                                $groupCheck[] = $valueSurveyvalueDataCheck->survey_group;
                            }
                            foreach ($QuestionFormulasChoiceCheck as $keyQuestionFormulasChoiceCheck => $valueQuestionFormulasChoiceCheck) {
                                $criteria = new CDbCriteria;
                                $criteria->addInCondition('survey_group',$groupCheck);
                                $criteria->compare('question_id',$valueQuestionFormulasChoiceCheck->question_id);
                                $SurveyvalueDataCheck = SurveyValue::model()->findAll($criteria);
                                $SurveyvalueDataCheckTotal = 0;
                                foreach ($SurveyvalueDataCheck as $key => $value) {
                                    if ($value->value > 0) {
                                        $SurveyvalueDataCheckTotal += $value->value;
                                    }
                                }
                                if ($SurveyvalueDataCheckTotal > 0) {
                                    if(!isset($array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice])){
                                        if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                            $array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice] = $SurveyvalueDataCheckTotal * ($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]);
                                        }
                                    }else{
                                        if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                            $array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice] = $array_province[$valueQuestionnaireData->province][$keyChoice][$valueQuestionFormulasChoiceCheck->choice] + ($SurveyvalueDataCheckTotal * ($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]));
                                        }
                                    }
                                }

                            }
                        }

                    }elseif($QuestionFormulasCheck->type == "sum_market_animal"){
                        if (!in_array($valueQuestionnaireData->questionnaire_id, $array_check_questionnaire[$QuestionFormulasCheck->formulas_id])) {
                            $criteria = new CDbCriteria;
                            $criteria->compare('province',$valueQuestionnaireData->province);
                            $criteria->compare('district',$valueQuestionnaireData->district);
                            $criteria->compare('subdistrict',$valueQuestionnaireData->subdistrict);
                            $criteria->compare('village',$valueQuestionnaireData->village);
                            $criteria->compare('t.group',$valueQuestionnaireData->group);
                            $criteria->order = "questionnaire_id ASC";
                            $QuestionnaireDataCheck = Questionnaire::model()->findAll($criteria);

                            $array_questionnaire = array();
                            foreach ($QuestionnaireDataCheck as $keyQuestionnaireDataCheck => $valueQuestionnaireDataCheck) {
                                $array_check_questionnaire[$QuestionFormulasCheck->formulas_id][] = $valueQuestionnaireDataCheck->questionnaire_id;
                                $array_questionnaire[] = $valueQuestionnaireDataCheck->questionnaire_id;
                            }
                            $criteria=new CDbCriteria;
                            $criteria->compare('formulas_id',$QuestionFormulasCheck->formulas_id);
                            $criteria->order = "choice ASC";
                            $QuestionFormulasChoiceCheck = QuestionFormulasChoice::model()->findAll($criteria);

                            $criteria = new CDbCriteria;
                            $criteria->addInCondition('questionnaire_id',$array_questionnaire);
                            $criteria->compare('question_id',$QuestionFormulasCheck->question_id_ans);
                            $SurveyvalueDataCheck = SurveyValue::model()->findAll($criteria);


                            foreach ($SurveyvalueDataCheck as $keySurveyvalueDataCheck => $valueSurveyvalueDataCheck) {
                                if (!empty($valueSurveyvalueDataCheck->value)) {
                                    $criteria = new CDbCriteria;
                                    $criteria->compare('question_id',103);
                                    $criteria->compare('survey_group',$valueSurveyvalueDataCheck->survey_group);
                                    $SurveyvalueDataCheckData1 = SurveyValue::model()->find($criteria);

                                    $SurveyvalueDataCheckTotal = 0;
                                    if (!empty($SurveyvalueDataCheckData1)) {
                                        $SurveyvalueDataCheckTotal = $SurveyvalueDataCheckData1->value;
                                    }
                                    if ($SurveyvalueDataCheckTotal > 0) {
                                        if(!isset($array_province[$valueQuestionnaireData->province][$keyChoice][$valueSurveyvalueDataCheck->value])){
                                            if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                                $array_province[$valueQuestionnaireData->province][$keyChoice][$valueSurveyvalueDataCheck->value] = $SurveyvalueDataCheckTotal*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]);
                                            }
                                        }else{
                                            if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                                $array_province[$valueQuestionnaireData->province][$keyChoice][$valueSurveyvalueDataCheck->value] = $array_province[$valueQuestionnaireData->province][$keyChoice][$valueSurveyvalueDataCheck->value] + ($SurveyvalueDataCheckTotal*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]));
                                            }
                                        }
                                    }
                                }
                            }

                        }

                    }
                    else{
                        // $QuestionFormulasCheck->type == "sum_market"
                        echo "Type ผิดพลาด";
                        exit();
                    }

                }
            }
        }
    }
}else{
    $Choice = array();
}
?>

<div class="container-fluid">
    <div id="section">
        <div class="card table-card">
            <div class="card-body ">
                <div class="table-responsive">
                    <table class="table table-striped table-main" id="">
                        <thead>
                            <tr class="report-th" style="background-color: #95c0c0;">
                                <th width="50%">รายการ</th>
                                <?php $Master_province_array = array(); ?>
                                <?php

                                if (isset($_GET["sector"])) {
                                    $criteria = new CDbCriteria;
                                    $criteria->addInCondition('sector_id',$_GET["sector"]);
                                    $MasterSectorData = MasterSector::model()->findAll($criteria);
                                }else{
                                    $MasterSectorData = array();
                                }
                                foreach ($MasterSectorData as $keyMasterSectorData => $valueMasterSectorData) {
                                    ?> 
                                    <th><?=$valueMasterSectorData->sector_name?></th>
                                    <?php
                                    $MasterCountyData = MasterCounty::model()->findAll(array(
                                        'condition' => 'sector_id=:sector_id',
                                        'params' => array(':sector_id' => $valueMasterSectorData->sector_id)
                                    ));
                                    $array_id_county = array();
                                    foreach ($MasterCountyData as $keyMasterCountyData => $valueMasterCountyData) {
                                        $array_id_county[] = $valueMasterCountyData->county_id;
                                    }
                                    $criteria = new CDbCriteria;
                                    $criteria->compare('active', 1);
                                    $criteria->addInCondition('county_id',$array_id_county);
                                    $criteria->order = 'province_code ASC';
                                    $MtProvinceDataSort = MtProvince::model()->findAll($criteria);
                                    foreach ($MtProvinceDataSort as $keyMtProvinceDataSort => $valueMtProvinceDataSort) {
                                        $Master_province_array[$valueMasterSectorData->sector_id][] = $valueMtProvinceDataSort->id;
                                        ?>
                                        <th class="itempro<?=$keyMtProvinceDataSort+1?>"  style=""><?=$valueMtProvinceDataSort->province_name_th?></th>
                                        <?php 
                                    }
                                }

                                ?>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $count_question = array(); ?>
                            <?php foreach ($Choice as $keyChoice => $valueChoice) {
                                foreach ($array_province as $keyarray_province => $valuearray_province) {
                                    if (isset($valuearray_province[$keyChoice])) {
                                        $MtProvinceData = MtProvince::model()->find(array(
                                            'condition' => 'province_code=:province_code',
                                            'params' => array(':province_code' => (string)$keyarray_province)
                                        ));
                                        foreach ($Master_province_array as $keyMaster_province_array => $valueMaster_province_array) {
                                            if (in_array($MtProvinceData->id, $valueMaster_province_array)) {
                                                if (!isset($count_question[$keyMaster_province_array])) {
                                                    $count_question[$keyMaster_province_array] = array();
                                                }
                                                if (!isset($count_question[$keyMaster_province_array][$keyChoice])) {
                                                    $count_question[$keyMaster_province_array][$keyChoice] = 1;
                                                }else{
                                                    $count_question[$keyMaster_province_array][$keyChoice]++;
                                                }
                                                
                                            }
                                        }
                                    }
                                }
                            } ?>
                            <?php foreach ($Choice as $keyChoice => $valueChoice) {
                                $criteria=new CDbCriteria;
                                $criteria->compare('formulas_group_id',$id);
                                $criteria->compare('question_id',$keyChoice);
                                $QuestionFormulasCheck = QuestionFormulas::model()->find($criteria); ?>
                                <div>
                                    <tr class="report-title" style="background-color: #e1f6f6;">
                                        <td colspan="1"><?=$valueChoice?></td>
                                        <?php
                                        foreach ($Master_province_array as $keyMaster_province_array => $valueMaster_province_array) {
                                            $total_result = 0;
                                            foreach ($valueMaster_province_array as $keytotal_result => $valuetotal_result) {
                                                $MtProvinceData = MtProvince::model()->find(array(
                                                    'condition' => 'id=:id',
                                                    'params' => array(':id' => $valuetotal_result)
                                                ));
                                                if (isset($array_province[$MtProvinceData->province_code][$keyChoice])) {
                                                    foreach ($array_province[$MtProvinceData->province_code][$keyChoice] as $key => $value) {
                                                        if ($QuestionFormulasCheck->type == "count-multiplication") {
                                                            $total_result+=($value);
                                                        }elseif($QuestionFormulasCheck->type == "multiplication" || $QuestionFormulasCheck->type == "cost-multiplication-plant" || $QuestionFormulasCheck->type == "cost-multiplication-animal" || $QuestionFormulasCheck->type == "count-multiplication-begin-year" || $QuestionFormulasCheck->type == "count-multiplication-begin-year-length" || $QuestionFormulasCheck->type == "count-multiplication-begin-year-length-objective" || $QuestionFormulasCheck->type == "count-multiplication-begin-year-length-source" || $QuestionFormulasCheck->type == "count-multiplication-between-year" || $QuestionFormulasCheck->type == "count-multiplication-between-year-length" || $QuestionFormulasCheck->type == "count-multiplication-between-year-length-objective" || $QuestionFormulasCheck->type == "count-multiplication-between-year-length-source" || $QuestionFormulasCheck->type == "count-multiplication-end-year" || $QuestionFormulasCheck->type == "count-multiplication-end-year-length" || $QuestionFormulasCheck->type == "count-multiplication-end-year-length-objective" || $QuestionFormulasCheck->type == "count-multiplication-end-year-length-source" || $QuestionFormulasCheck->type == "numerate-debt" || $QuestionFormulasCheck->type == "count_income_infarm" || $QuestionFormulasCheck->type == "sum_expend_of_plant" || $QuestionFormulasCheck->type == "sum_wage" || $QuestionFormulasCheck->type == "sum_expend_of_animal" || $QuestionFormulasCheck->type == "group_sum_wage_plant" || $QuestionFormulasCheck->type == "group_sum_item_plant" || $QuestionFormulasCheck->type == "group_sum_all_plant" || $QuestionFormulasCheck->type == "group_sum_wage_animal" || $QuestionFormulasCheck->type == "group_sum_item_animal" || $QuestionFormulasCheck->type == "group_sum_all_animal" || $QuestionFormulasCheck->type == "sum_wage_plant" || $QuestionFormulasCheck->type == "sum_wage_animal" || $QuestionFormulasCheck->type == "sum_plant_all_year" || $QuestionFormulasCheck->type == "sum_plant_question" || $QuestionFormulasCheck->type == "sum_muad_9" || $QuestionFormulasCheck->type == "num_sum_muad_9" || $QuestionFormulasCheck->type == "cost_sum_muad_9" || $QuestionFormulasCheck->type == "sum_all_muad_9" || $QuestionFormulasCheck->type == "sum_market" || $QuestionFormulasCheck->type == "supply_distibution" || $QuestionFormulasCheck->type == "sum_animal_question" || $QuestionFormulasCheck->type == "sum_market_animal"){
                                                            $total_result+=($value*$array_processingByProvince[$MtProvinceData->province_code]["n"]/$array_processingByProvince[$MtProvinceData->province_code]["N"]/$array_processingByProvince[$MtProvinceData->province_code]["M"]);
                                                        }elseif($QuestionFormulasCheck->type == "numerate" || $QuestionFormulasCheck->type =="numerate-member" || $QuestionFormulasCheck->type =="numerate-labor" || $QuestionFormulasCheck->type =="multiplication-begin-year" || $QuestionFormulasCheck->type =="multiplication-begin-year-length" || $QuestionFormulasCheck->type =="multiplication-between-year" || $QuestionFormulasCheck->type =="multiplication-between-year-length" || $QuestionFormulasCheck->type =="multiplication-end-year" || $QuestionFormulasCheck->type =="multiplication-end-year-length"){
                                                            $total_result+=round($value);
                                                        }elseif($QuestionFormulasCheck->type == "age-hh"){
                                                            $total_result+=($value/$count[$keyChoice][$MtProvinceData->province_code]);
                                                        }elseif($QuestionFormulasCheck->type == "sex-man-hh" || $QuestionFormulasCheck->type == "sex-women-hh" || $QuestionFormulasCheck->type == "age-hh-length" || $QuestionFormulasCheck->type == "age-mm-length" || $QuestionFormulasCheck->type == "multiplication-hh-not-province" || $QuestionFormulasCheck->type == "multiplication-mm-not-province" || $QuestionFormulasCheck->type == "multiplication-mm-not-province-in" || $QuestionFormulasCheck->type == "multiplication-mm-not-province-out" || $QuestionFormulasCheck->type == "sex-man-mm" || $QuestionFormulasCheck->type == "sex-women-mm" || $QuestionFormulasCheck->type == "sex-man-labor" || $QuestionFormulasCheck->type == "sex-women-labor" || $QuestionFormulasCheck->type == "multiplication-labor-not-province" || $QuestionFormulasCheck->type == "numerate-debt-household"){
                                                            $total_result+=round($value);
                                                        }else{

                                                        }

                                                    }
                                                }
                                            }
                                            ?>
                                            <?php if ($total_result > 0) { ?>
                                                <?php if ($QuestionFormulasCheck->type == "numerate" || $QuestionFormulasCheck->type =="numerate-member" || $QuestionFormulasCheck->type =="numerate-labor" || $QuestionFormulasCheck->type =="multiplication-begin-year" || $QuestionFormulasCheck->type =="multiplication-begin-year-length" || $QuestionFormulasCheck->type =="multiplication-between-year" || $QuestionFormulasCheck->type =="multiplication-between-year-length" || $QuestionFormulasCheck->type =="multiplication-end-year" || $QuestionFormulasCheck->type =="multiplication-end-year-length") { ?>
                                                    <td style=""><?=round(($total_result))?></td>
                                                <?php }elseif($QuestionFormulasCheck->type == "age-hh" || $QuestionFormulasCheck->type == "cost-multiplication-plant" || $QuestionFormulasCheck->type == "cost-multiplication-animal" || $QuestionFormulasCheck->type == "count-multiplication-begin-year" || $QuestionFormulasCheck->type == "count-multiplication-begin-year-length" || $QuestionFormulasCheck->type == "count-multiplication-begin-year-length-objective" || $QuestionFormulasCheck->type == "count-multiplication-begin-year-length-source" || $QuestionFormulasCheck->type == "count-multiplication-between-year" || $QuestionFormulasCheck->type == "count-multiplication-between-year-length" || $QuestionFormulasCheck->type == "count-multiplication-between-year-length-objective" || $QuestionFormulasCheck->type == "count-multiplication-between-year-length-source" || $QuestionFormulasCheck->type == "count-multiplication-end-year" || $QuestionFormulasCheck->type == "count-multiplication-end-year-length" || $QuestionFormulasCheck->type == "count-multiplication-end-year-length-objective" || $QuestionFormulasCheck->type == "count-multiplication-end-year-length-source" || $QuestionFormulasCheck->type == "numerate-debt" || $QuestionFormulasCheck->type == "count_income_infarm" || $QuestionFormulasCheck->type == "sum_expend_of_plant" || $QuestionFormulasCheck->type == "sum_wage" || $QuestionFormulasCheck->type == "sum_expend_of_animal" || $QuestionFormulasCheck->type == "group_sum_wage_plant" || $QuestionFormulasCheck->type == "group_sum_item_plant" || $QuestionFormulasCheck->type == "group_sum_all_plant" || $QuestionFormulasCheck->type == "group_sum_wage_animal" || $QuestionFormulasCheck->type == "group_sum_item_animal" || $QuestionFormulasCheck->type == "group_sum_all_animal" || $QuestionFormulasCheck->type == "sum_wage_plant" || $QuestionFormulasCheck->type == "sum_wage_animal" || $QuestionFormulasCheck->type == "sum_plant_question" || $QuestionFormulasCheck->type == "sum_muad_9" || $QuestionFormulasCheck->type == "num_sum_muad_9" || $QuestionFormulasCheck->type == "cost_sum_muad_9" || $QuestionFormulasCheck->type == "sum_all_muad_9" || $QuestionFormulasCheck->type == "sum_market" || $QuestionFormulasCheck->type == "supply_distibution" || $QuestionFormulasCheck->type == "sum_animal_question" || $QuestionFormulasCheck->type == "sum_market_animal"){ ?>
                                                    <td style=""><?=round(($total_result/$count_question[$keyMaster_province_array][$keyChoice]))?></td>
                                                <?php }elseif($QuestionFormulasCheck->type == "sex-man-hh" || $QuestionFormulasCheck->type == "sex-women-hh" || $QuestionFormulasCheck->type == "age-hh-length" || $QuestionFormulasCheck->type == "age-mm-length" || $QuestionFormulasCheck->type == "multiplication-hh-not-province" || $QuestionFormulasCheck->type == "multiplication-mm-not-province" || $QuestionFormulasCheck->type == "multiplication-mm-not-province-in" || $QuestionFormulasCheck->type == "multiplication-mm-not-province-out" || $QuestionFormulasCheck->type == "sex-man-mm" || $QuestionFormulasCheck->type == "sex-women-mm" || $QuestionFormulasCheck->type == "sex-man-labor" || $QuestionFormulasCheck->type == "sex-women-labor" || $QuestionFormulasCheck->type == "multiplication-labor-not-province" || $QuestionFormulasCheck->type == "numerate-debt-household"){ ?>
                                                    <td style=""><?=round(($total_result))?></td>
                                                <?php }elseif($QuestionFormulasCheck->type == "count-multiplication"){ ?>
                                                    <td style=""><?=round($total_result)?></td>

                                                <?php }else{ ?>
                                                    <td style=""><?=sprintf("%.2f",$total_result/$count_question[$keyMaster_province_array][$keyChoice])?></td>
                                                <?php } ?>
                                            <?php }else{ ?>
                                                <td style="">0</td>
                                            <?php } ?>

                                            <?php 
                                            ?>
                                            <?php
                                            foreach ($valueMaster_province_array as $keyvalueMaster_province_array => $valuevalueMaster_province_array) {
                                                $MtProvinceData = MtProvince::model()->find(array(
                                                    'condition' => 'id=:id',
                                                    'params' => array(':id' => $valuevalueMaster_province_array)
                                                ));
                                                $total = 0;
                                                if (isset($array_province[$MtProvinceData->province_code][$keyChoice])) { ?>
                                                    <?php foreach ($array_province[$MtProvinceData->province_code][$keyChoice] as $key => $value) {
                                                        $total +=$value;
                                                    } ?>
                                                    <td class="itempro<?=$keyvalueMaster_province_array+1?>" style=" ">
                                                        <?php if ($QuestionFormulasCheck->type == "count-multiplication") { ?>
                                                            <?=round($total)?>
                                                        <?php }elseif($QuestionFormulasCheck->type == "multiplication" || $QuestionFormulasCheck->type == "sum_plant_all_year"){ ?>
                                                            <?=sprintf("%.2f",$total*$array_processingByProvince[$MtProvinceData->province_code]["n"]/$array_processingByProvince[$MtProvinceData->province_code]["N"]/$array_processingByProvince[$MtProvinceData->province_code]["M"])?>
                                                        <?php }elseif($QuestionFormulasCheck->type == "cost-multiplication-plant" || $QuestionFormulasCheck->type == "cost-multiplication-animal" || $QuestionFormulasCheck->type == "count-multiplication-begin-year" || $QuestionFormulasCheck->type == "count-multiplication-begin-year-length" || $QuestionFormulasCheck->type == "count-multiplication-begin-year-length-objective" || $QuestionFormulasCheck->type == "count-multiplication-begin-year-length-source" || $QuestionFormulasCheck->type == "count-multiplication-between-year" || $QuestionFormulasCheck->type == "count-multiplication-between-year-length" || $QuestionFormulasCheck->type == "count-multiplication-between-year-length-objective" || $QuestionFormulasCheck->type == "count-multiplication-between-year-length-source" || $QuestionFormulasCheck->type == "count-multiplication-end-year" || $QuestionFormulasCheck->type == "count-multiplication-end-year-length" || $QuestionFormulasCheck->type == "count-multiplication-end-year-length-objective" || $QuestionFormulasCheck->type == "count-multiplication-end-year-length-source" || $QuestionFormulasCheck->type == "numerate-debt" || $QuestionFormulasCheck->type == "count_income_infarm" || $QuestionFormulasCheck->type == "sum_expend_of_plant" || $QuestionFormulasCheck->type == "sum_wage" || $QuestionFormulasCheck->type == "sum_expend_of_animal" || $QuestionFormulasCheck->type == "group_sum_wage_plant" || $QuestionFormulasCheck->type == "group_sum_item_plant" || $QuestionFormulasCheck->type == "group_sum_all_plant" || $QuestionFormulasCheck->type == "group_sum_wage_animal" || $QuestionFormulasCheck->type == "group_sum_item_animal" || $QuestionFormulasCheck->type == "group_sum_all_animal" || $QuestionFormulasCheck->type == "sum_wage_plant" || $QuestionFormulasCheck->type == "sum_wage_animal" || $QuestionFormulasCheck->type == "sum_plant_question" || $QuestionFormulasCheck->type == "sum_muad_9" || $QuestionFormulasCheck->type == "num_sum_muad_9" || $QuestionFormulasCheck->type == "cost_sum_muad_9" || $QuestionFormulasCheck->type == "sum_all_muad_9" || $QuestionFormulasCheck->type == "sum_market" || $QuestionFormulasCheck->type == "supply_distibution" || $QuestionFormulasCheck->type == "sum_animal_question" || $QuestionFormulasCheck->type == "sum_market_animal"){ ?>
                                                            <?=round($total*$array_processingByProvince[$MtProvinceData->province_code]["n"]/$array_processingByProvince[$MtProvinceData->province_code]["N"]/$array_processingByProvince[$MtProvinceData->province_code]["M"])?>
                                                        <?php }elseif($QuestionFormulasCheck->type == "numerate" || $QuestionFormulasCheck->type =="numerate-member" || $QuestionFormulasCheck->type =="numerate-labor"){ ?>
                                                            <?=round(($total))?>
                                                        <?php }elseif($QuestionFormulasCheck->type == "age-hh"){ ?>
                                                            <?=round(($total/$count[$keyChoice][$MtProvinceData->province_code]))?>
                                                        <?php }elseif($QuestionFormulasCheck->type == "sex-man-hh" || $QuestionFormulasCheck->type == "sex-women-hh" || $QuestionFormulasCheck->type == "age-hh-length" || $QuestionFormulasCheck->type == "age-mm-length" || $QuestionFormulasCheck->type == "multiplication-hh-not-province" || $QuestionFormulasCheck->type == "multiplication-mm-not-province" || $QuestionFormulasCheck->type == "multiplication-mm-not-province-in" || $QuestionFormulasCheck->type == "multiplication-mm-not-province-out" || $QuestionFormulasCheck->type == "sex-man-mm" || $QuestionFormulasCheck->type == "sex-women-mm" || $QuestionFormulasCheck->type == "sex-man-labor" || $QuestionFormulasCheck->type == "sex-women-labor" || $QuestionFormulasCheck->type == "multiplication-labor-not-province" || $QuestionFormulasCheck->type == "numerate-debt-household" || $QuestionFormulasCheck->type =="multiplication-begin-year" || $QuestionFormulasCheck->type =="multiplication-begin-year-length" || $QuestionFormulasCheck->type =="multiplication-between-year" || $QuestionFormulasCheck->type =="multiplication-between-year-length" || $QuestionFormulasCheck->type =="multiplication-end-year" || $QuestionFormulasCheck->type =="multiplication-end-year-length"){ ?>
                                                            <?=round(($total))?>
                                                        <?php }else{

                                                        } ?>


                                                    </td>
                                                <?php }else{?>
                                                    <td class="itempro<?=$keyvalueMaster_province_array+1?>" style=" ">0</td>
                                                <?php }
                                            }
                                        }
                                        ?>
                                    </tr>
                                    <?php
                                    if (count($Array_ans[$keyChoice]) > 1) {
                                        foreach ($Array_ans[$keyChoice] as $keyArray_ans => $valueArray_ans) { ?>
                                            <tr class="report-ans">

                                                <td class="text-left"><?=$valueArray_ans?></td>
                                                <?php
                                                foreach ($Master_province_array as $keyMaster_province_array => $valueMaster_province_array) {
                                                    $total_result = 0;
                                                    foreach ($valueMaster_province_array as $keytotal_result => $valuetotal_result) {
                                                        $MtProvinceData = MtProvince::model()->find(array(
                                                            'condition' => 'id=:id',
                                                            'params' => array(':id' => $valuetotal_result)
                                                        ));
                                                        if (isset($array_province[$MtProvinceData->province_code][$keyChoice][$keyArray_ans])) {
                                                            if ($QuestionFormulasCheck->type == "count-multiplication") {
                                                                $total_result+=($array_province[$MtProvinceData->province_code][$keyChoice][$keyArray_ans]);
                                                            }elseif($QuestionFormulasCheck->type == "multiplication" || $QuestionFormulasCheck->type == "cost-multiplication-plant" || $QuestionFormulasCheck->type == "cost-multiplication-animal" || $QuestionFormulasCheck->type == "count-multiplication-begin-year" || $QuestionFormulasCheck->type == "count-multiplication-begin-year-length" || $QuestionFormulasCheck->type == "count-multiplication-begin-year-length-objective" || $QuestionFormulasCheck->type == "count-multiplication-begin-year-length-source" || $QuestionFormulasCheck->type == "count-multiplication-between-year" || $QuestionFormulasCheck->type == "count-multiplication-between-year-length" || $QuestionFormulasCheck->type == "count-multiplication-between-year-length-objective" || $QuestionFormulasCheck->type == "count-multiplication-between-year-length-source" || $QuestionFormulasCheck->type == "count-multiplication-end-year" || $QuestionFormulasCheck->type == "count-multiplication-end-year-length" || $QuestionFormulasCheck->type == "count-multiplication-end-year-length-objective" || $QuestionFormulasCheck->type == "count-multiplication-end-year-length-source" || $QuestionFormulasCheck->type == "count_income_infarm" || $QuestionFormulasCheck->type == "sum_wage" || $QuestionFormulasCheck->type == "group_sum_wage_plant" || $QuestionFormulasCheck->type == "group_sum_item_plant" || $QuestionFormulasCheck->type == "group_sum_all_plant" || $QuestionFormulasCheck->type == "group_sum_wage_animal" || $QuestionFormulasCheck->type == "group_sum_item_animal" || $QuestionFormulasCheck->type == "group_sum_all_animal" || $QuestionFormulasCheck->type == "sum_wage_plant" || $QuestionFormulasCheck->type == "sum_wage_animal" || $QuestionFormulasCheck->type == "sum_plant_all_year" || $QuestionFormulasCheck->type == "sum_plant_question" || $QuestionFormulasCheck->type == "sum_muad_9" || $QuestionFormulasCheck->type == "num_sum_muad_9" || $QuestionFormulasCheck->type == "cost_sum_muad_9" || $QuestionFormulasCheck->type == "sum_market" || $QuestionFormulasCheck->type == "supply_distibution" || $QuestionFormulasCheck->type == "sum_animal_question" || $QuestionFormulasCheck->type == "sum_market_animal"){
                                                                $total_result+=($array_province[$MtProvinceData->province_code][$keyChoice][$keyArray_ans]*$array_processingByProvince[$MtProvinceData->province_code]["n"]/$array_processingByProvince[$MtProvinceData->province_code]["N"]/$array_processingByProvince[$MtProvinceData->province_code]["M"]);
                                                            }elseif($QuestionFormulasCheck->type == "numerate" || $QuestionFormulasCheck->type =="numerate-member" || $QuestionFormulasCheck->type =="numerate-labor" || $QuestionFormulasCheck->type =="multiplication-begin-year" || $QuestionFormulasCheck->type =="multiplication-begin-year-length" || $QuestionFormulasCheck->type =="multiplication-between-year" || $QuestionFormulasCheck->type =="multiplication-between-year-length" || $QuestionFormulasCheck->type =="multiplication-end-year" || $QuestionFormulasCheck->type =="multiplication-end-year-length"){
                                                                $total_result+=($array_province[$MtProvinceData->province_code][$keyChoice][$keyArray_ans]);
                                                            }elseif($QuestionFormulasCheck->type == "age-hh"){
                                                                $total_result+=($array_province[$MtProvinceData->province_code][$keyChoice][$keyArray_ans]/$count[$keyChoice][$MtProvinceData->province_code]);
                                                            }elseif($QuestionFormulasCheck->type == "age-hh-length" || $QuestionFormulasCheck->type == "age-mm-length" || $QuestionFormulasCheck->type == "multiplication-hh-not-province" || $QuestionFormulasCheck->type == "multiplication-mm-not-province" || $QuestionFormulasCheck->type == "multiplication-mm-not-province-in" || $QuestionFormulasCheck->type == "multiplication-mm-not-province-out" || $QuestionFormulasCheck->type == "multiplication-labor-not-province" || $QuestionFormulasCheck->type == "numerate-debt-household"){
                                                                $total_result+=round($array_province[$MtProvinceData->province_code][$keyChoice][$keyArray_ans]);
                                                            }else{

                                                            }
                                                        }
                                                    }
                                                    ?>
                                                    <?php if ($total_result > 0) { ?>
                                                        <?php if ($QuestionFormulasCheck->type == "age-hh-length" || $QuestionFormulasCheck->type == "age-mm-length" || $QuestionFormulasCheck->type == "multiplication-hh-not-province" || $QuestionFormulasCheck->type == "multiplication-mm-not-province" || $QuestionFormulasCheck->type == "multiplication-mm-not-province-in" || $QuestionFormulasCheck->type == "multiplication-mm-not-province-out" || $QuestionFormulasCheck->type == "multiplication-labor-not-province" || $QuestionFormulasCheck->type == "numerate-debt-household" || $QuestionFormulasCheck->type =="multiplication-begin-year" || $QuestionFormulasCheck->type =="multiplication-begin-year-length" || $QuestionFormulasCheck->type =="multiplication-between-year" || $QuestionFormulasCheck->type =="multiplication-between-year-length" || $QuestionFormulasCheck->type =="multiplication-end-year" || $QuestionFormulasCheck->type =="multiplication-end-year-length" || $QuestionFormulasCheck->type == "group_sum_wage_plant" || $QuestionFormulasCheck->type == "group_sum_item_plant" || $QuestionFormulasCheck->type == "group_sum_all_plant" || $QuestionFormulasCheck->type == "group_sum_wage_animal" || $QuestionFormulasCheck->type == "group_sum_item_animal" || $QuestionFormulasCheck->type == "group_sum_all_animal") {?>
                                                            <td>
                                                                <?=round($total_result)?>
                                                            </td>
                                                        <?php }elseif($QuestionFormulasCheck->type == "count-multiplication-begin-year" || $QuestionFormulasCheck->type == "count-multiplication-begin-year-length" || $QuestionFormulasCheck->type == "count-multiplication-begin-year-length-objective" || $QuestionFormulasCheck->type == "count-multiplication-begin-year-length-source" || $QuestionFormulasCheck->type == "count-multiplication-between-year" || $QuestionFormulasCheck->type == "count-multiplication-between-year-length" || $QuestionFormulasCheck->type == "count-multiplication-between-year-length-objective" || $QuestionFormulasCheck->type == "count-multiplication-between-year-length-source" || $QuestionFormulasCheck->type == "count-multiplication-end-year" || $QuestionFormulasCheck->type == "count-multiplication-end-year-length" || $QuestionFormulasCheck->type == "count-multiplication-end-year-length-objective" || $QuestionFormulasCheck->type == "count-multiplication-end-year-length-source" || $QuestionFormulasCheck->type == "count_income_infarm" || $QuestionFormulasCheck->type == "sum_wage" || $QuestionFormulasCheck->type == "cost-multiplication-plant" || $QuestionFormulasCheck->type == "cost-multiplication-animal" || $QuestionFormulasCheck->type == "sum_wage_plant" || $QuestionFormulasCheck->type == "sum_wage_animal" || $QuestionFormulasCheck->type == "sum_plant_question" || $QuestionFormulasCheck->type == "sum_muad_9" || $QuestionFormulasCheck->type == "num_sum_muad_9" || $QuestionFormulasCheck->type == "cost_sum_muad_9" || $QuestionFormulasCheck->type == "sum_market" || $QuestionFormulasCheck->type == "supply_distibution" || $QuestionFormulasCheck->type == "sum_animal_question" || $QuestionFormulasCheck->type == "sum_market_animal"){ ?>
                                                            <td>
                                                                <?=round($total_result/$count_question[$keyMaster_province_array][$keyChoice])?>
                                                            </td>
                                                        <?php }elseif($QuestionFormulasCheck->type == "count-multiplication"){ ?>
                                                            <td>
                                                                <?=round($total_result)?>
                                                            </td>
                                                        <?php }else{ ?>
                                                            <td>
                                                                <?=sprintf("%.2f",$total_result/$count_question[$keyMaster_province_array][$keyChoice])?>
                                                            </td>
                                                        <?php } ?>
                                                    <?php }else{ ?>
                                                        <td>
                                                            0
                                                        </td>
                                                    <?php } ?>


                                                    <?php 
                                                    foreach ($valueMaster_province_array as $keyvalueMaster_province_array => $valuevalueMaster_province_array) {
                                                        $MtProvinceData = MtProvince::model()->find(array(
                                                            'condition' => 'id=:id',
                                                            'params' => array(':id' => $valuevalueMaster_province_array)
                                                        ));
                                                        if (isset($array_province[$MtProvinceData->province_code][$keyChoice][$keyArray_ans])) { ?>
                                                            <?php 

                                                            if ($QuestionFormulasCheck->type == "count-multiplication") { ?>
                                                                <td class="itempro<?=$keyvalueMaster_province_array+1?>" style="">
                                                                    <?=round($array_province[$MtProvinceData->province_code][$keyChoice][$keyArray_ans])?>
                                                                </td>
                                                            <?php }elseif($QuestionFormulasCheck->type == "multiplication" || $QuestionFormulasCheck->type == "sum_plant_all_year"){ ?>
                                                                <td class="itempro<?=$keyvalueMaster_province_array+1?>" style=""><?=sprintf("%.2f",$array_province[$MtProvinceData->province_code][$keyChoice][$keyArray_ans]*$array_processingByProvince[$MtProvinceData->province_code]["n"]/$array_processingByProvince[$MtProvinceData->province_code]["N"]/$array_processingByProvince[$MtProvinceData->province_code]["M"])?></td>
                                                            <?php }elseif($QuestionFormulasCheck->type == "cost-multiplication-plant" || $QuestionFormulasCheck->type == "cost-multiplication-animal" || $QuestionFormulasCheck->type == "count-multiplication-begin-year" || $QuestionFormulasCheck->type == "count-multiplication-begin-year-length" || $QuestionFormulasCheck->type == "count-multiplication-begin-year-length-objective" || $QuestionFormulasCheck->type == "count-multiplication-begin-year-length-source" || $QuestionFormulasCheck->type == "count-multiplication-between-year" || $QuestionFormulasCheck->type == "count-multiplication-between-year-length" || $QuestionFormulasCheck->type == "count-multiplication-between-year-length-objective" || $QuestionFormulasCheck->type == "count-multiplication-between-year-length-source" || $QuestionFormulasCheck->type == "count-multiplication-end-year" || $QuestionFormulasCheck->type == "count-multiplication-end-year-length" || $QuestionFormulasCheck->type == "count-multiplication-end-year-length-objective" || $QuestionFormulasCheck->type == "count-multiplication-end-year-length-source" || $QuestionFormulasCheck->type == "count_income_infarm" || $QuestionFormulasCheck->type == "sum_wage" || $QuestionFormulasCheck->type == "group_sum_wage_plant" || $QuestionFormulasCheck->type == "group_sum_item_plant" || $QuestionFormulasCheck->type == "group_sum_all_plant" || $QuestionFormulasCheck->type == "group_sum_wage_animal" || $QuestionFormulasCheck->type == "group_sum_item_animal" || $QuestionFormulasCheck->type == "group_sum_all_animal" || $QuestionFormulasCheck->type == "sum_wage_plant" || $QuestionFormulasCheck->type == "sum_wage_animal" || $QuestionFormulasCheck->type == "sum_plant_question" || $QuestionFormulasCheck->type == "sum_muad_9" || $QuestionFormulasCheck->type == "num_sum_muad_9" || $QuestionFormulasCheck->type == "cost_sum_muad_9" || $QuestionFormulasCheck->type == "sum_market" || $QuestionFormulasCheck->type == "supply_distibution" || $QuestionFormulasCheck->type == "sum_animal_question" || $QuestionFormulasCheck->type == "sum_market_animal"){ ?>
                                                                <td class="itempro<?=$keyvalueMaster_province_array+1?>" style=""><?=round($array_province[$MtProvinceData->province_code][$keyChoice][$keyArray_ans]*$array_processingByProvince[$MtProvinceData->province_code]["n"]/$array_processingByProvince[$MtProvinceData->province_code]["N"]/$array_processingByProvince[$MtProvinceData->province_code]["M"])?></td>
                                                            <?php }elseif($QuestionFormulasCheck->type == "numerate" || $QuestionFormulasCheck->type =="numerate-member" || $QuestionFormulasCheck->type =="numerate-labor" || $QuestionFormulasCheck->type =="multiplication-begin-year" || $QuestionFormulasCheck->type =="multiplication-begin-year-length" || $QuestionFormulasCheck->type =="multiplication-between-year" || $QuestionFormulasCheck->type =="multiplication-between-year-length" || $QuestionFormulasCheck->type =="multiplication-end-year" || $QuestionFormulasCheck->type =="multiplication-end-year-length"){?>
                                                                <td class="itempro<?=$keyvalueMaster_province_array+1?>" style=""><?=round(($array_province[$MtProvinceData->province_code][$keyChoice][$keyArray_ans]))?></td>
                                                            <?php }elseif($QuestionFormulasCheck->type == "age-hh"){ ?>
                                                                <td class="itempro<?=$keyvalueMaster_province_array+1?>" style=""><?=round(($array_province[$MtProvinceData->province_code][$keyChoice][$keyArray_ans]/$count[$keyChoice][$MtProvinceData->province_code]))?></td>
                                                            <?php }elseif($QuestionFormulasCheck->type == "age-hh-length" || $QuestionFormulasCheck->type == "age-mm-length" || $QuestionFormulasCheck->type == "multiplication-hh-not-province" || $QuestionFormulasCheck->type == "multiplication-mm-not-province" || $QuestionFormulasCheck->type == "multiplication-mm-not-province-in" || $QuestionFormulasCheck->type == "multiplication-mm-not-province-out" || $QuestionFormulasCheck->type == "multiplication-labor-not-province" || $QuestionFormulasCheck->type == "numerate-debt-household"){ ?>
                                                                <td class="itempro<?=$keyvalueMaster_province_array+1?>" style=""><?=round(($array_province[$MtProvinceData->province_code][$keyChoice][$keyArray_ans]))?></td>
                                                            <?php }else{

                                                            } ?>

                                                        <?php }else{ ?>
                                                            <td class="itempro<?=$keyvalueMaster_province_array+1?>" style="">0</td>
                                                        <?php }
                                                    }
                                                } ?><?php
                                            }
                                            ?>
                                        </tr>
                                    <?php } ?>
                                </div>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div><!--  card close -->
    </div>
</div>

</BODY>

</HTML>