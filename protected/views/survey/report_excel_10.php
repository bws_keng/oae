<?php
header("Content-Type: application/vnd.ms-excel");
header('Content-Disposition: attachment; filename="MyXls.xls"');
$strExcelFileName = 'export-'.$group["question_group_name"]."-".date("d-m-Y") . ".xls";
header("Content-Type: application/x-msexcel; name=\"" . $strExcelFileName . "\"");
header("Content-Disposition: inline; filename=\"" . $strExcelFileName . "\"");
header('Content-Type: text/plain; charset=UTF-8');
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Pragma:no-cache");
?>

<html xmlns:o=”urn:schemas-microsoft-com:office:office”

xmlns:x=”urn:schemas-microsoft-com:office:excel”

xmlns=”http://www.w3.org/TR/REC-html40″>

<HTML>

<HEAD>
  <meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />
</HEAD>
<BODY>
    <table>
        <thead>
            <tr>
                <th>ลำดับ</th>
                <!-- <th>เจ้าหน้าที่สำรวจ(ชื่อ)</th>
                <th>เจ้าหน้าที่สำรวจ(นามสกุล)</th>
                <th>เจ้าหน้าที่สำรวจ(รหัส)</th>
                <th>เจ้าหน้าที่สำรวจ(วัน/เดือน/ปี)</th> -->
                <th>สศท.</th>
                <th>จังหวัด</th>
                <th>อำเภอ</th>
                <th>ตำบล</th>
                <th>หมู่ที่</th>
                <!-- <th>บ้านเลขที่</th> -->
                <th>พวกที่</th>
                <!-- <th>ผู้ขึ้นทะเบียนเกษตรกร(ชื่อ)</th>
                <th>ผู้ขึ้นทะเบียนเกษตรกร(นามสกุล)</th> -->
                <th>ผู้ขึ้นทะเบียนเกษตรกร(ตัวอย่างที่)</th>
                <!-- <th>ผู้ขึ้นทะเบียนเกษตรกร(เลขบัตรประจำตัวประชาชน)</th>
                <th>ผู้ขึ้นทะเบียนเกษตรกร(ผู้ขึ้นทะเบียนเกษตร)</th>
                <th>ชื่อผู้ให้ข้อมูล(ชื่อ)</th>
                <th>ชื่อผู้ให้ข้อมูล(นามสกุล)</th>
                <th>ชื่อผู้ให้ข้อมูล(ความสัมพันธ์กับหัวหน้าครัวเรือน)</th>
                <th>ชื่อผู้ให้ข้อมูล(เลขบัตรประจำตัวประชาชน)</th>
                <th>ชื่อผู้ให้ข้อมูล(เบอร์ติดต่อ)</th>
                <th>ครัวเรือนเกษตรมีที่ดินทำการเกษตรอย่างน้อย 1 แปลง อยู่ในเขตชลประทาน</th> -->
                <th>Code-9</th>
                <th>จำนวน-9</th>
                <th>มูลค่า-9</th>
                <!-- <th>จำนวนหมู่ทั้งหมด</th>
                <th>จำนวนหมู่สุ่ม</th>
                <th>จำนวนหมู่สำรวจ</th>
                <th>ครัวเรือนเกษตรทั้งหมด</th>
                <th>ครัวเรือนเกษตรตามพวก</th>
                <th>ครัวเรือนเกษตรสำรวจ</th> -->
            </tr>
        </thead>
        <tbody>
            <?php 
            $key = 1;
            foreach ($Questionnaire as $keyQuestionnaire => $valueQuestionnaire) {
             $criteria=new CDbCriteria;
             $criteria->select = "survey_group";
             $criteria->compare('questionnaire_id',$valueQuestionnaire->questionnaire_id);
             $criteria->addInCondition('question_id',$question_id);
             $criteria->group= 'survey_group';
             $criteria->order = "t.survey_group ASC";
             $SurveyValue = SurveyValue::model()->findAll($criteria);
             $Array_group = array();
             foreach ($SurveyValue as $keySurveyValue => $valueSurveyValue) {
                $Array_group[] = $valueSurveyValue->survey_group;
            }
            foreach ($Array_group as $keyArray_group => $valueArray_group) {
                foreach ($question_id as $keyquestion_id => $valuequestion_id) {
                    $criteria=new CDbCriteria;
                    $criteria->compare('question_id',$valuequestion_id);
                    $QuestionCheck = Question::model()->find($criteria);
                    if ($QuestionCheck->type == "ques-num-value") {
                        $criteria=new CDbCriteria;
                        $criteria->compare('options',$valuequestion_id);
                        $criteria->compare('type',"parent-ques-num");
                        $QuestionOptionsNum = Question::model()->find($criteria);

                        $criteria=new CDbCriteria;
                        $criteria->compare('options',$valuequestion_id);
                        $criteria->compare('type',"parent-ques-value");
                        $QuestionOptionsValue = Question::model()->find($criteria);
                        
                        if (!empty($QuestionOptionsNum)) {
                            $criteria=new CDbCriteria;
                            $criteria->compare('survey_group',$valueArray_group);
                            $criteria->compare('question_id',$QuestionOptionsNum->question_id);
                            $SurveyValueNum = SurveyValue::model()->find($criteria);
                        }

                        if (!empty($QuestionOptionsValue)) {
                            $criteria=new CDbCriteria;
                            $criteria->compare('survey_group',$valueArray_group);
                            $criteria->compare('question_id',$QuestionOptionsValue->question_id);
                            $SurveyValueValue = SurveyValue::model()->find($criteria);
                        }
                        if (!empty($SurveyValueNum) || !empty($SurveyValueValue)) { ?>
                            <tr>
                                <td><?=$key++?></td>
                                <!-- <td><?=$valueQuestionnaire->staff_name?></td>
                                <td><?=$valueQuestionnaire->staff_lname?></td>
                                <td>'<?=$valueQuestionnaire->staff_id?></td>
                                <td><?=$valueQuestionnaire->survey_time?></td> -->
                                <td><?=$valueQuestionnaire->office_id?></td>
                                <td>'<?=$valueQuestionnaire->province?></td>
                                <td>'<?=$valueQuestionnaire->district?></td>
                                <td>'<?=$valueQuestionnaire->subdistrict?></td>
                                <td>'<?=$valueQuestionnaire->village?></td>
                                <!-- <td>'<?=$valueQuestionnaire->addressNumber?></td> -->
                                <td><?=$valueQuestionnaire->group?></td>
                                <!-- <td><?=$valueQuestionnaire->farmer_name?></td>
                                <td><?=$valueQuestionnaire->farmer_lastname?></td> -->
                                <td><?=$valueQuestionnaire->sample?></td>
                                <!-- <td>'<?=$valueQuestionnaire->farmer_id?></td>
                                <td><?=$valueQuestionnaire->farmer_registered?></td>
                                <td><?=$valueQuestionnaire->informant_name?></td>
                                <td><?=$valueQuestionnaire->informant_lastname?></td>
                                <td><?=$valueQuestionnaire->informant_relation?></td>
                                <td>'<?=$valueQuestionnaire->informant_id?></td>
                                <td>'<?=$valueQuestionnaire->informant_tel?></td>
                                <td><?=$valueQuestionnaire->inIrrigatedArea?></td> -->
                                <td><?=$QuestionCheck->readable_id?></td>
                                <td><?=(!empty($SurveyValueNum)) ? $SurveyValueNum->value : "0"?></td>
                                <td><?=(!empty($SurveyValueValue)) ? $SurveyValueValue->value : "0"?></td>
                                <!-- <?php 
                                $MtProvince = MtProvince::model()->find(array(
                                    'condition' => 'active=:active AND province_code=:province_code',
                                    'params' => array(':active' => '1',':province_code' => $valueQuestionnaire->province)
                                ));
                                if (!empty($MtProvince)) {
                                    $criteria = new CDbCriteria();
                                    $criteria->compare('province',$MtProvince->id);
                            // $criteria->compare('year',$valueQuestionnaire->survey_id);
                                    $criteria->compare('t.group',$valueQuestionnaire->group);
                                    $criteria->compare('active',1);
                                    $Processing = Processing::model()->find($criteria);
                                    if (!empty($Processing)) { ?>
                                        <td><?=$Processing->total?></td>
                                        <td><?=$Processing->random?></td>
                                        <td><?=$Processing->survey?></td>
                                    <?php }else{ ?>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                    <?php }
                                    $MtDistrict = MtDistrict::model()->find(array(
                                        'condition' => 'active=:active AND district_code=:district_code AND province_id=:province_id',
                                        'params' => array(':active' => '1',':district_code' => $valueQuestionnaire->district,':province_id' => $MtProvince->id)
                                    ));
                                    if (!empty($MtDistrict)) {
                                        $MtSubDistrict = MtSubDistrict::model()->find(array(
                                            'condition' => 'active=:active AND subdistrict_code=:subdistrict_code AND district_id=:district_id',
                                            'params' => array(':active' => '1',':subdistrict_code' => $valueQuestionnaire->subdistrict,':district_id' => $MtDistrict->id)
                                        ));
                                        if (!empty($MtSubDistrict)) {
                                            $masterVillage = masterVillage::model()->find(array(
                                                'condition' => 'active=:active AND village_code=:village_code AND subdistrict_id=:subdistrict_id',
                                                'params' => array(':active' => '1',':village_code' => $valueQuestionnaire->village,':subdistrict_id' => $MtSubDistrict->id)
                                            ));
                                            if (!empty($masterVillage) && !empty($Processing)) {
                                                $criteria = new CDbCriteria();
                                                $criteria->compare('processing_id',$Processing->id);
                                                $criteria->compare('district',$MtDistrict->id);
                                                $criteria->compare('subdistrict',$MtSubDistrict->id);
                                                $criteria->compare('village',$masterVillage->village_id);
                                                $criteria->compare('active',1);
                                                $SubProcessing = SubProcessing::model()->find($criteria);
                                                if (!empty($SubProcessing)) { ?>
                                                    <td><?=$SubProcessing->total?></td>
                                                    <td><?=$SubProcessing->random?></td>
                                                    <td><?=$SubProcessing->survey?></td>
                                                <?php }else{ ?>
                                                    <td>0</td>
                                                    <td>0</td>
                                                    <td>0</td>
                                                <?php }
                                            }else{ ?>
                                                <td>0</td>
                                                <td>0</td>
                                                <td>0</td>
                                            <?php }
                                        }
                                    }
                                }
                                ?> -->
                            </tr>
                        <?php }
                    }else{
                        $criteria=new CDbCriteria;
                        $criteria->compare('survey_group',$valueArray_group);
                        $criteria->compare('question_id',$QuestionCheck->question_id);
                        $SurveyValue = SurveyValue::model()->find($criteria);
                        if (!empty($SurveyValue)) { ?>
                            <tr>
                                <td><?=$key++?></td>
                                <!-- <td><?=$valueQuestionnaire->staff_name?></td>
                                <td><?=$valueQuestionnaire->staff_lname?></td>
                                <td>'<?=$valueQuestionnaire->staff_id?></td>
                                <td><?=$valueQuestionnaire->survey_time?></td> -->
                                <td><?=$valueQuestionnaire->office_id?></td>
                                <td>'<?=$valueQuestionnaire->province?></td>
                                <td>'<?=$valueQuestionnaire->district?></td>
                                <td>'<?=$valueQuestionnaire->subdistrict?></td>
                                <td>'<?=$valueQuestionnaire->village?></td>
                                <!-- <td>'<?=$valueQuestionnaire->addressNumber?></td> -->
                                <td><?=$valueQuestionnaire->group?></td>
                                <!-- <td><?=$valueQuestionnaire->farmer_name?></td>
                                <td><?=$valueQuestionnaire->farmer_lastname?></td> -->
                                <td><?=$valueQuestionnaire->sample?></td>
                               <!--  <td>'<?=$valueQuestionnaire->farmer_id?></td>
                                <td><?=$valueQuestionnaire->farmer_registered?></td>
                                <td><?=$valueQuestionnaire->informant_name?></td>
                                <td><?=$valueQuestionnaire->informant_lastname?></td>
                                <td><?=$valueQuestionnaire->informant_relation?></td>
                                <td>'<?=$valueQuestionnaire->informant_id?></td>
                                <td>'<?=$valueQuestionnaire->informant_tel?></td>
                                <td><?=$valueQuestionnaire->inIrrigatedArea?></td> -->
                                <td><?=$QuestionCheck->readable_id?></td>
                                <td>0</td>
                                <?php if ($SurveyValue->value != NULL) { ?>
                                    <td><?=$SurveyValue->value?></td>
                                <?php }else{ ?>
                                    <td>0</td>
                                <?php } ?>
                                <!-- <?php 
                                $MtProvince = MtProvince::model()->find(array(
                                    'condition' => 'active=:active AND province_code=:province_code',
                                    'params' => array(':active' => '1',':province_code' => $valueQuestionnaire->province)
                                ));
                                if (!empty($MtProvince)) {
                                    $criteria = new CDbCriteria();
                                    $criteria->compare('province',$MtProvince->id);
                            // $criteria->compare('year',$valueQuestionnaire->survey_id);
                                    $criteria->compare('t.group',$valueQuestionnaire->group);
                                    $criteria->compare('active',1);
                                    $Processing = Processing::model()->find($criteria);
                                    if (!empty($Processing)) { ?>
                                        <td><?=$Processing->total?></td>
                                        <td><?=$Processing->random?></td>
                                        <td><?=$Processing->survey?></td>
                                    <?php }else{ ?>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                    <?php }
                                    $MtDistrict = MtDistrict::model()->find(array(
                                        'condition' => 'active=:active AND district_code=:district_code AND province_id=:province_id',
                                        'params' => array(':active' => '1',':district_code' => $valueQuestionnaire->district,':province_id' => $MtProvince->id)
                                    ));
                                    if (!empty($MtDistrict)) {
                                        $MtSubDistrict = MtSubDistrict::model()->find(array(
                                            'condition' => 'active=:active AND subdistrict_code=:subdistrict_code AND district_id=:district_id',
                                            'params' => array(':active' => '1',':subdistrict_code' => $valueQuestionnaire->subdistrict,':district_id' => $MtDistrict->id)
                                        ));
                                        if (!empty($MtSubDistrict)) {
                                            $masterVillage = masterVillage::model()->find(array(
                                                'condition' => 'active=:active AND village_code=:village_code AND subdistrict_id=:subdistrict_id',
                                                'params' => array(':active' => '1',':village_code' => $valueQuestionnaire->village,':subdistrict_id' => $MtSubDistrict->id)
                                            ));
                                            if (!empty($masterVillage) && !empty($Processing)) {
                                                $criteria = new CDbCriteria();
                                                $criteria->compare('processing_id',$Processing->id);
                                                $criteria->compare('district',$MtDistrict->id);
                                                $criteria->compare('subdistrict',$MtSubDistrict->id);
                                                $criteria->compare('village',$masterVillage->village_id);
                                                $criteria->compare('active',1);
                                                $SubProcessing = SubProcessing::model()->find($criteria);
                                                if (!empty($SubProcessing)) { ?>
                                                    <td><?=$SubProcessing->total?></td>
                                                    <td><?=$SubProcessing->random?></td>
                                                    <td><?=$SubProcessing->survey?></td>
                                                <?php }else{ ?>
                                                    <td>0</td>
                                                    <td>0</td>
                                                    <td>0</td>
                                                <?php }
                                            }else{ ?>
                                                <td>0</td>
                                                <td>0</td>
                                                <td>0</td>
                                            <?php }
                                        }
                                    }
                                }
                                ?> -->
                            </tr>
                        <?php }
                    }
                }
            }


        }
        ?>
    </tbody>
</table>

</BODY>

</HTML>
