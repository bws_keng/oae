<!DOCTYPE html>
<html lang="en">

<head>
    <?php 
        $prefix = 'user';
    ?>
    <meta name="description" content="Cuba admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, Cuba admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="pixelstrap">
    <title>oae</title>
    <?php include $prefix.'/inc-head.php'; ?>
</head>

<body>
    <div class="tap-top"><i data-feather="chevrons-up"></i></div>
    
    <div class="page-wrapper compact-wrapper" id="pageWrapper">
        <?php include $prefix.'/inc-header.php'; ?>
        <div class="page-body-wrapper horizontal-menu">
            <?php include $prefix.'/inc-menuleft.php'; ?>
            
            <div class="page-body">
                <?php echo $content; ?>
            </div>

            <?php include $prefix.'/inc-footer.php'; ?>
        </div>
    </div>

    <?php include $prefix.'/inc-script.php'; ?>
    <script>
    $("#all").on('change', (e) => {
        $('input[type="checkbox"]:not([id$="isrequired"]):not([id^="all"])').prop('checked', e.target.checked);
    })
    $("#all-isrequired").on('change', (e) => {
        if(e.target.checked){
            $('input[type="checkbox"]').prop("checked", e.target.checked);
            $('input[type="checkbox"]:not([id$="isrequired"]):not([id^="all"])').prop('value', true);
        }else{
            $('input[type="checkbox"][id$="isrequired"]:not([id^="all"])').prop("checked", e.target.checked);
            $('input[type="checkbox"]:not([id$="isrequired"]):not([id^="all"])').prop('value', false);
        }
    })
    $('input[type="checkbox"][id$="isrequired"]:not([id^="all"])').on('change', (e) => {
        if(e.target.checked){
            $('#'+e.target.id.slice(0,-11)).prop("checked", e.target.checked);
            $('#'+e.target.id.slice(0,-11)).prop("value", "true");
        }else{
            $('#'+e.target.id.slice(0,-11)).prop("value", "false");
        }
        
    })

    const insertInput = (target_id, name) => {
        // $('#answer').append('<input class=\'form-control\' type=\'text\' name=\'description[]\' value=\'>');
        console.log(name)
        $('#'+target_id).append('<input class="form-control" type="text" name="'+name+'[]">');
    }

    </script>
    <script>
    const sendDelete = (key, value) => {
        $("#deleteKey").prop("value", key);
        $("#deleteValue").prop("value", value);
        $("#deleteForm").show();
    }
    </script>
</body>

</html>