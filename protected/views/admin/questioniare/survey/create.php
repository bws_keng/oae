<form class="container-fluid" method="POST" action="<?php echo Yii::app()->baseurl.'/admin/questionaire/survey'; ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl; ?>/theme/assets/js/bootstrap-daterangepicker/jquery.datetimepicker.css">
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/theme/assets/js/bootstrap-daterangepicker/jquery.datetimepicker2.full.min.js"></script>
<div class="container-fluid">
    <!-- start row -->
    <div class="row">
        <div class="col-lg-12 set-height set-padding">
            <div id="section1">
                <div class="card">
                    <div class="card-header">
                        <h5 class="mb-1">เพิ่มแบบสอบถาม</h5>
                    </div>
                </div>
                <div class="card table-card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-12 mb-3">
                                <div class="form-group">
                                    <label for="">ชื่อแบบสอบถาม</label>
                                    <input class="form-control" id="" type="text" placeholder=""
                                        required="" data-original-title="" title="" name="survey-name">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 mb-3">
                                <div class="form-group">
                                    <label for="">ปี พ.ศ. เริ่มต้น</label>
                                    <select class="form-control digits " id="" name="year-start" required>
                                        <option selected disabled></option>
                                        <?php
                                            for($i=0; $i<20; $i++){
                                                echo '<option value="'.(2562+$i).'">'.(2562+$i).'</option>';
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <div class="form-group">
                                    <label for="">ปี พ.ศ. สิ้นสุด</label>
                                    <select class="form-control digits" id="" name="year-end" required>
                                        <option selected disabled></option>
                                        <?php
                                            for($i=0; $i<20; $i++){
                                                echo '<option value="'.(2562+$i).'">'.(2562+$i).'</option>';
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 mb-3">
                                <label for="">ช่วงเวลาเริ่มต้น</label>
                                <input class="form-control datetimepicker" type="text" value="" name="date-start" required>
                                <!-- <div class="date-picker">
                                   <label for="validationDefault03">วัน/เดือน/ปี</label>
                                   <div class="input-group">
                                       <input class="datepicker-here form-control digits" type="text" data-language="th">
                                   </div>
                                   </div>
                               </div> -->
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="">ช่วงเวลาสิ้นสุด</label>
                                <input class="form-control datetimepicker" type="text" value="" name="date-end" required>
                                <!-- <div class="date-picker">
                                   <label for="validationDefault03">วัน/เดือน/ปี</label>
                                   <div class="input-group">
                                       <input class="datepicker-here form-control digits" type="text" data-language="th">
                                   </div>
                                   </div>
                               </div> -->
                            </div>
                        </div>
                        <div class="row">
                        <div class="col-md-12 mb-3">
                            <label for="">CLONE จากแบบสอบถามเก่า</label>
                            <select class="form-control" id="clone-survey" name="clone-survey">
                                <option value="none" selected>เลือกแบบสอบถาม</option>
                                <?php foreach ($surveylist as $keylist => $valuelist) {
                                    echo '<option value="'.($valuelist["id"]).'">'.($valuelist["name"]).'</option>';
                                } ?>
                                
                            </select>
                        </div>
                        </div>
                        <script type="text/javascript">
                            $( document ).ready(function() {
                                $( "#clone-survey" ).change(function() {
                                    if ($(this).val() == "none") {
                                        $( "#hidden-clone" ).show();
                                    }else{
                                        $( "#hidden-clone" ).hide();
                                    }
                                });
                            });
                        </script>
                        <div id="hidden-clone">
                            <hr>
                            <div class="row">
                                <div class="col-md-12 mb-3">
                                    <label for="">ฟอร์มคำถามที่ใช้ในแบบสอบถาม</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6 m-t-15 mb-0">
                                    <div class="checkbox checkbox-solid-white">
                                        <input id="all" type="checkbox">
                                        <label for="all">ทั้งหมด</label>
                                    </div>
                                </div>
                                <div class="form-group col-md-6 m-t-15 mb-0">
                                    <div class="checkbox checkbox-solid-white">
                                        <input id="all-isrequired" type="checkbox">
                                        <label for="all-isrequired">บังคับทั้งหมด</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <?php
                                // print_r($survey);
                                foreach($survey as $key => $row){
                                    echo '
                                    <div class="form-group col-md-6 m-t-15 mb-0">
                                    <div class="checkbox checkbox-solid-white">
                                    <input id="solid-'.$key.'" type="checkbox" name="question-'.$row['question_id'].'" value="false">
                                    <label for="solid-'.$key.'">'.$row['title'].'</label>
                                    </div>
                                    </div>
                                    <div class="form-group col-md-6 m-t-15 mb-0">
                                    <div class="checkbox checkbox-solid-white">
                                    <input id="solid-'.$key.'-isrequired" type="checkbox">
                                    <label for="solid-'.$key.'-isrequired">บังคับ</label>
                                    </div>
                                    </div>
                                    ';
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end row -->
    <div class="row text-center group-submit">
        <input type="submit" class="btn btn-primary btn-lg" value="บันทึกข้อมูล" />
        <button type="button" class="btn btn-outline-secondary btn-lg" onclick="window.history.go(-1); return false;">ยกเลิก</button>
    </div>
</div>
</form>
<script type="text/javascript">
    $.datetimepicker.setLocale('th');
    $('.datetimepicker').datetimepicker({
       timepicker:false,
                format:'d-m-Y',  // กำหนดรูปแบบวันที่ ที่ใช้ เป็น 00-00-0000            
                lang:'th',  // แสดงภาษาไทย                     
             //   yearOffset:543,  // ใช้ปี พ.ศ. บวก 543 เพิ่มเข้าไปในปี ค.ศ
                closeOnDateSelect:true,
                onSelectDate:function(dp,$input){
                var yearT=new Date(dp).getFullYear()-0;  
                var yearTH=yearT+543;
                var fulldate=$input.val();
                var fulldateTH=fulldate.replace(yearT,yearTH);
                $input.val(fulldateTH);
                 },
    });
</script>