<table class="table table-striped table-main" id="">
    <!-- <table class="table table-striped table-main" id="basic-2"> -->
        <thead>
            <tr>
                <th width="5%">ลำดับ</th>
                <th width="10%">เลขที่คำถาม</th>
                <th>คำถาม</th>
                <th width="25%">จัดการ</th>
            </tr>
        </thead>
        <tbody>
            <?php
            
            foreach($survey as $key => $row){
                echo '
                <tr>
                <td>'.($key+1).'</td>
                <td>'.($row['readable_id'] != "" ? $row['readable_id']: "-").'</td>
                <td>'.$row['question'].'</td>
                <td>
                <a type="button" class="btn btn-icon text-success" href="'. Yii::app()->baseurl.'/admin/survey/'.$_GET['id'].'/question/'.$row['question_id'].'/edit">
                <i data-feather="edit"></i>
                </a>
                <button type="button" class="btn btn-icon text-danger" onclick="sendDelete('.$row['question_id'].', \''.$row['question'].'\')"><i
                data-feather="trash"></i></button>
                </td>
                </tr>
                ';
            }
            ?>
        </tbody>
    </table>
    <script type="text/javascript">
     $(document).ready(function(){
         feather.replace();
     });
 </script>