<div class="container-fluid">
  <!-- start row -->
  <div class="row">
    <div class="col-lg-12 set-height set-padding">
      <div id="section1">
        <div class="card">
          <div class="card-header">
            <h5 class="mb-1">
              <?php

                 $FormText = 'จัดการผู้ดูแลระบบ';
                // $this->breadcrumbs=array(
                //     'ข้อมูลผู้ดูแลระบบ'=>array('index'),
                //     $FormText,
                // );
                 echo $FormText;
              ?>
            </h5>
          </div>
        </div>
        <div class="row justify-content-end">
          <div class="col-md-3">
            <a href="<?php echo Yii::app()->createUrl('adminUser/create'); ?>">
              <div class="card o-hidden create-main">
                <div class="b-r-4 card-body">
                  <div class="media static-top-widget">
                    <div class="align-self-center text-center"><i data-feather="plus"></i></div>
                    <div class="media-body">
                      <h6 class="mb-0 counter">เพิ่มผู้ดูแลระบบ</h6><i class="icon-bg" data-feather="database"></i>
                    </div>
                  </div>
                </div>
              </div>
            </a>
          </div>
        </div>
        <div class="card table-card">
          <!-- <div class="card-header">
            <div class="row">
              <div class="col-md-3 mb-3">
                <label for="">ผืนที่</label>
                <input class="form-control" id="" maxlength="1" type="text" placeholder="" required=""
                  data-original-title="" title="">
              </div>
              <div class="col-md-3 mb-3">
                <div class="form-group">
                  <label for="">จังหวัด</label>
                  <select class="form-control digits" id="">
                    <option selected disabled>เลือกจังหวัด</option>
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                  </select>
                </div>
              </div>
              <div class="col-md-3 mb-3">
                <div class="form-group">
                  <label for="">ปี พ.ศ.</label>
                  <select class="form-control digits" id="">
                    <option selected disabled>เลือกปี พ.ศ.</option>
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                  </select>
                </div>
              </div>
              <div class="col-md-3 mb-3">
                <button class="btn btn-lg btn-search text-white" type="submit"><i
                    class="icon-search"></i>&nbsp;ค้นหา</button>
              </div>
            </div>
          </div> -->
          <div class="card-body">
            <div class="table-responsive">
             <!--  <table class="table table-striped table-main" id="">
                <thead>
                  
                </thead>
                <tbody>
                
                </tbody>
              </table> -->
              <?php  $this->widget('AGridView',array(
           // 'summaryText' => false, // 1st way
                    'id'=>'AdminUser-grid',
                    'dataProvider'=>$model->search(),
                    //'filter'=>$model,
                    'selectableRows' => 2,
                    'htmlOptions' => array(
                        'style'=> "margin-top: -1px;",
                    ),
            // 'afterAjaxUpdate'=>'function(id, data){
            //     $.appendFilter("AdminUser[news_per_page]");
            // }',
                    
            'columns'=>array(
                    array(
                        'header'=>'No.',
                        'value'=>'$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
                    ),
                    array(
                        'header' => 'ชื่อ - สกุล',
                        'type'=>'raw',
                        'value' => function($val){
                                $username = $val->profiles->firstname.' '.$val->profiles->lastname;
                            if(empty($username)){
                                return 'ไม่มีข้อมูล';
                            } else {
                                return $username;
                            }
                        }
                    ),
                     array(
                       'header' => 'อีเมล',
                       'type'=>'raw',
                       'value' =>'$data->email'
                    ),

                    array(
                        'header' => 'วันที่สมัคร',
                        'type'=>'raw',
                        'value' => function($val){
                                return Helpers::lib()->changeFormatDate($val->create_at,'datetime');
                        }
                    ),
                     array(
                        'header' => 'วันที่เข้าใช้งานล่าสุด',
                        'type'=>'raw',
                        'value' => function($val){
                                return Helpers::lib()->changeFormatDate($val->lastvisit_at,'datetime');
                        }
                    ),
                    array(
                    'class' => 'zii.widgets.grid.CButtonColumn',
                    'htmlOptions' => array('style' => 'white-space: nowrap'),
                    'afterDelete' => 'function(link,success,data) { if (success && data) alert(data); }',
                    'template' => '{view} {update} {btn_delete}',
                    'buttons' => array(
                        'view' => array(

                            'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => 'View'),
                            'label' => '<button type="button" class="btn btn-icon text-primary"><i data-feather="eye"></i></button>',
                            'imageUrl' => false,
                        ),
                        'update' => array(
                            'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => 'Update'),
                            'label' => '<button type="button" class="btn btn-icon text-success"><i data-feather="edit"></i></button>',
                            'imageUrl' => false,
                            'url'=> function($data) {
                                return Yii::app()->controller->createUrl('adminUser/update', ['id' => $data->id]);
                            }
                        ),
                        'btn_delete' => array(
                            'click' => 'function() {if(!confirm("คุณต้องการลบช้อมูลใช่หรือไม่?")) {return false;}}',
                            'options' => array(
                                'rel' => 'tooltip', 
                                'data-toggle' => 'tooltip', 
                                'title' => 'Delete',
                                'class' => 'btn_del'
                                ), 
                                'url'=> function($data) {
                                return Yii::app()->controller->createUrl('adminUser/delete', ['id' => $data->id]);
                                },
                            'label' => '<button type="button" class="btn btn-icon text-danger"><i data-feather="trash"></i></button>',
                            'imageUrl' => false,
                            //'Url' => false,
                            )
                        )
                    ),
            ),
        )); ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- end row -->
</div>