<?php

// Yii::import('zii.widgets.CWidget');

class AdminMenu extends CWidget
{
	protected $items;

	public function init()
	{
		$this->items = FormModel::list();
		parent::init();
	}

	public function run()
	{
		$this->render('adminMenu');
		// $this->renderMenu($this->items);
	}

	public function renderList()
	{
		$this->renderMenuRecursive($this->items);
	}


	public static function PermissionsMenu( $menu = array() )
	{
		/*=============================================
		=             Check Permissions	             =
		=============================================*/
		$sum = 0;
		$countArray = count($menu);
		$return = false;        

		if($countArray != 0)
		{
			foreach ($menu as $key => $value)
			{
				$val = explode('.',$value);
				$controller_name = strtolower($val[0]);

				// if($controller_name == "monthcheck"){
				// 	var_dump($val);
				// 	exit();
				// }
				$action_name = strtolower($val[1]);
				$check = AccessControl::check_access_action(strtolower($controller_name),strtolower($action_name));
				if($check){
					$return = true;
				}else{
					$return = false;
				}
			}
		}

		// if($controller_name == "monthcheck"){
		// 			var_dump($check);
		// 			exit();
		// 		}
		return $return;
	}


	protected function renderMenuRecursive($items)
	{
		// var_dump($items);exit();
		
		// echo '<li>'.CHtml::link('เพิ่มข้อมูล Excel',array('admin/survey/excel')).'</li>';

		foreach ($items as $item){
			// print_r($item);
			// if($item['question_group_number'] == '2.1'){
			if($item['question_group_number'] == '2.1'){
				$chkCategory = '21';
			}else if($item['question_group_number'] == '2.2'){
				$chkCategory = '22';
			}else{
				$chkCategory = $item['question_group_number'];
			}
				$check = self::PermissionsMenu(array(
					'Category.'.$chkCategory.''
				));
			if($check){
				echo CHtml::openTag('li');
				if(isset($item['question_group_id']) && $item['question_group_id'] != null){
					echo '

					<li><a class="submenu-title " href="#">หมวดที่ '.$item['question_group_number'].'<span class="sub-arrow"><i class="fa fa-angle-right"></i></span></a>
					<ul class="nav-sub-childmenu submenu-content"> ';
		            if ($item['question_group_id'] == 12) {
		            	echo ' <li>'.CHtml::link('จัดการคำถาม',array('admin/survey/'.$item['question_group_id'].'/rate')).'</li>
		            	<li>'.CHtml::link('Import ข้อมูล',array('survey/import/'.$item['question_group_id'])).'</li>
						<li>'.CHtml::link('Export ข้อมูล',array('survey/export/'.$item['question_group_id'])).'</li>';
		            }else{
					echo ' <li>'.CHtml::link('จัดการคำถาม',array('admin/survey/'.$item['question_group_id'].'/question')).'</li>
					<li>'.CHtml::link('จัดการคำตอบ',array('admin/survey/'.$item['question_group_id'].'/answer')).'</li>
					<li>'.CHtml::link('จัดการหมวดหมู่',array('admin/survey/'.$item['question_group_id'].'/group')).'</li>
					<li>'.CHtml::link('จัดการข้อมูลการแสดงผล',array('admin/survey/'.$item['question_group_id'].'/mange')).'</li>
					<li>'.CHtml::link('Import ข้อมูล',array('survey/excel/'.$item['question_group_id'])).'</li>
					<li>'.CHtml::link('Export ข้อมูล',array('survey/export/'.$item['question_group_id'])).'</li>
					';
					}
					echo '</ul>
					</li>'
					;
					

				}
				echo CHtml::closeTag('li');
			}
		// }
			}

	}

}
