<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'My Web Application',

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'zii.widgets.*',
		'zii.widgets.grid.*',
		'application.models.*',
		'application.components.*',
        'application.modules.user.*',
		'application.modules.user.models.*',
		'application.extensions.jtogglecolumn.*',
        'application.modules.user.components.*',
        'application.modules.user.UserModule',
        'application.modules.rights.*',
        'application.modules.rights.components.*',
		'application.extensions.yush.*',
		'ext.giix.giix-components.*',
		'application.extensions.phpexcel.Classes.PHPExcel',
		'ext.chosen2.*',
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool
		
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'12345',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
		),
		'user'=>array(
            # encrypting method (php hash function)
            'hash'					=> 'md5',
            # send activation email
            'sendActivationMail' 	=> false,
            # allow access for non-activated users
            'loginNotActiv' 		=> false,
            # activate user on registration (only sendActivationMail = false)
            'activeAfterRegister' 	=> true,
            # automatically login from registration
            'autoLogin' 			=> true,
            # registration path
            'registrationUrl' 		=> array('/user/registration'),
            # recovery password path
            'recoveryUrl' 			=> array('/user/recovery'),
            # login form path
            'loginUrl' 				=> array('/user/login'),
            # page after login
            'returnUrl' 			=> array('/site/index'),
            # page after logout
            'returnLogoutUrl' 		=> array('/user/login'),
        ),
        'rights' => array(
            'flashSuccessKey'       => 'success', // Key to use for setting success flash messages.
            'flashErrorKey'         => 'error', // Key to use for setting error flash messages.
            'layout'              	=> '//layouts/column2', // Layout to use for displaying Rights.
            'install'               => false, // Whether to enable installer.
			'enableBizRule' => false,
        ),
		
	),

	// application components
	'components'=>array(

		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
		),
		'mailer' => array(
			'class' => 'ext.mailer.EMailer',
		),

		'yush' => array(
	        'class' => 'ext.yush.YushComponent',
	        'baseDirectory' => '../uploads',
	    ),
	    'yexcel' => array(
		    'class' => 'ext.yexcel.Yexcel'
		),
		// NOTE: ทำการแก้ไข URL Manager เพื่อเพิ่ม path ของ API ไว้
		'urlManager'=>array(
			'urlFormat'=>'path',
			
			'rules'=>array(
				// REST API
				array('api/list', 'pattern'=>'api/<model:\w+>', 'verb'=>'GET'),
				array('api/view', 'pattern'=>'api/<model:\w+>/<id:\d+>', 'verb'=>'GET'),
				array('api/update', 'pattern'=>'api/<model:\w+>/<id:\d+>', 'verb'=>'PUT'),
				array('api/delete', 'pattern'=>'api/<model:\w+>/<id:\d+>/delete', 'verb'=>'DELETE'),
				array('api/create', 'pattern'=>'api/<model:\w+>', 'verb'=>'POST'),
				
				array('questionnaire/list', 'pattern'=>'questionnaire', 'verb'=>'GET'),
				array('questionnaire/createForm', 'pattern'=>'questionnaire/create', 'verb'=>'GET'),
				array('questionnaire/delete', 'pattern'=>'questionnaire/delete', 'verb'=>'POST'),
				array('questionnaire/edit', 'pattern'=>'questionnaire/<id:\d+>/edit', 'verb'=>'GET'), // Edit form
				array('questionnaire/show', 'pattern'=>'questionnaire/<id:\d+>/show', 'verb'=>'GET'), // Edit form
				array('questionnaire/loadmap', 'pattern'=>'questionnaire/<id:\d+>/loadmap', 'verb'=>'GET'), // Edit form
				array('questionnaire/update', 'pattern'=>'questionnaire/<id:\d+>/edit', 'verb'=>'POST'), // Edit form
				array('questionnaire/create', 'pattern'=>'questionnaire', 'verb'=>'POST'),


				// array('user/admin/index', 'pattern'=>'user/admin/index', 'verb'=>'POST'),
				// array('user/admin/create', 'pattern'=>'user/admin/create', 'verb'=>'GET'),
				// array('user/admin/update', 'pattern'=>'//user/admin/update', 'verb'=>'POST'),
				// array('user/admin/delete', 'pattern'=>'user/admin/delete/', 'verb'=>'POST'),

				array('pController/index', 'pattern'=>'pController/index', 'verb'=>'POST'),
				array('pController/update', 'pattern'=>'pController/update/', 'verb'=>'POST'),
				array('pController/create', 'pattern'=>'pController/create', 'verb'=>'POST'),
				array('pController/delete', 'pattern'=>'pController/delete/', 'verb'=>'POST'),
				array('pController/priority', 'pattern'=>'pController/priority/', 'verb'=>'GET'),

				
				array('pgroup/index', 'pattern'=>'pgroup/index', 'verb'=>'POST'),
				array('pgroup/delete', 'pattern'=>'pgroup/delete/', 'verb'=>'POST'),
				array('pgroup/update', 'pattern'=>'pgroup/update/', 'verb'=>'POST'),
				array('pgroup/create', 'pattern'=>'pgroup/create/', 'verb'=>'POST'),

				array('survey/list', 'pattern'=>'survey/<id:\d+>', 'verb'=>'GET'),
				array('survey/searchdata', 'pattern'=>'survey/<id:\d+>/searchdata', 'verb'=>'GET'),
				array('survey/form', 'pattern'=>'survey/<id:\d+>/create', 'verb'=>'GET'),
				array('survey/delete', 'pattern'=>'survey/<id:\d+>/delete', 'verb'=>'POST'),
				array('survey/edit', 'pattern'=>'survey/<id:\d+>/<survey_id:\d+>/edit', 'verb'=>'GET'), // Edit form
				array('survey/update', 'pattern'=>'survey/<id:\d+>/<survey_id:\d+>/edit', 'verb'=>'POST'), // Edit form
				array('survey/insert', 'pattern'=>'survey/<id:\d+>', 'verb'=>'POST'),

				array('survey/listRate', 'pattern'=>'survey/<id:\d+>/rate', 'verb'=>'GET'),
				array('survey/formRate', 'pattern'=>'survey/<id:\d+>/rate/create', 'verb'=>'GET'),
				array('survey/insertRate', 'pattern'=>'survey/<id:\d+>/rate/<questionnaire_id:\d+>/insert', 'verb'=>'POST'),
				array('survey/editRate', 'pattern'=>'survey/<id:\d+>/<questionnaire_id:\d+>/rate/edit', 'verb'=>'GET'), // Edit form
				array('survey/updateRate', 'pattern'=>'survey/<id:\d+>/rate/<questionnaire_id:\d+>/update', 'verb'=>'POST'),
				array('survey/deleteRate', 'pattern'=>'survey/<id:\d+>/<questionnaire_id:\d+>/rate/delete', 'verb'=>'GET'), 
				
				array('admin/index', 'pattern'=>'admin', 'verb'=>'GET'),

				array('admin/surveyRate', 'pattern'=>'/admin/survey/<id:\d+>/rate', 'verb'=>'GET'),
				array('admin/surveyRateForm', 'pattern'=>'/admin/survey/<id:\d+>/rate/create', 'verb'=>'GET'),
				array('admin/surveyRateInsert', 'pattern'=>'/admin/survey/<id:\d+>/rate/insert', 'verb'=>'POST'),
				array('admin/surveyRateEdit', 'pattern'=>'/admin/survey/<id:\d+>/rate/<question_id:\d+>/edit', 'verb'=>'GET'), // Edit form
				array('admin/surveyRateUpdate', 'pattern'=>'/admin/survey/<id:\d+>/rate/<question_id:\d+>/update', 'verb'=>'POST'), // Edit form
				array('admin/surveyRateDelete', 'pattern'=>'/admin/survey/<id:\d+>/rate/<question_id:\d+>/delete', 'verb'=>'GET'), 

				array('admin/surveyQuestion', 'pattern'=>'/admin/survey/<id:\d+>/question', 'verb'=>'GET'),
				array('admin/surveyQuestionForm', 'pattern'=>'/admin/survey/<id:\d+>/question/create', 'verb'=>'GET'),
				array('admin/surveyQuestionDelete', 'pattern'=>'/admin/survey/<id:\d+>/question/delete', 'verb'=>'POST'), // ok
				array('admin/surveyQuestionEdit', 'pattern'=>'/admin/survey/<id:\d+>/question/<question_id:\d+>/edit', 'verb'=>'GET'), // Edit form
				array('admin/surveyQuestionUpdate', 'pattern'=>'/admin/survey/<id:\d+>/question/<question_id:\d+>/edit', 'verb'=>'POST'), // Edit form
				array('admin/surveyQuestionInsert', 'pattern'=>'/admin/survey/<id:\d+>/question', 'verb'=>'POST'),

				array('admin/surveyGroup', 'pattern'=>'/admin/survey/<id:\d+>/group', 'verb'=>'GET'),
				array('admin/surveyGroupForm', 'pattern'=>'/admin/survey/<id:\d+>/group/create', 'verb'=>'GET'),
				array('admin/surveyGroupDelete', 'pattern'=>'/admin/survey/<id:\d+>/group/delete', 'verb'=>'POST'), // ok
				array('admin/surveyGroupEdit', 'pattern'=>'/admin/survey/<id:\d+>/group/<survey_id:\d+>/edit', 'verb'=>'GET'), // Edit form
				array('admin/surveyGroupUpdate', 'pattern'=>'/admin/survey/<id:\d+>/group/<survey_id:\d+>/edit', 'verb'=>'POST'), // Edit form
				array('admin/surveyGroupManagement', 'pattern'=>'/admin/survey/<id:\d+>/group/<survey_id:\d+>/form', 'verb'=>'GET'), // Form management
				array('admin/surveyGroupAction', 'pattern'=>'/admin/survey/<id:\d+>/group/<survey_id:\d+>/form', 'verb'=>'POST'), // Form management
				array('admin/surveyGroupAnswerManagement', 'pattern'=>'/admin/survey/<id:\d+>/group/<survey_id:\d+>/answer/form', 'verb'=>'GET'), // Form management

				array('admin/SurveyGroupAnswerAction', 'pattern'=>'/admin/survey/<id:\d+>/group/<survey_id:\d+>/answer/form', 'verb'=>'POST'), // Form management
				array('admin/surveyGroupInsert', 'pattern'=>'/admin/survey/<id:\d+>/group', 'verb'=>'POST'),

				array('admin/surveyAnswer', 'pattern'=>'/admin/survey/<id:\d+>/answer', 'verb'=>'GET'),
				// array('admin/surveyExcel', 'pattern'=>'/admin/survey/<id:\d+>/excel', 'verb'=>'GET'),
				// array('admin/surveyExport', 'pattern'=>'/admin/survey/<id:\d+>/export', 'verb'=>'GET'),

				array('admin/surveyExcel', 'pattern'=>'/admin/survey/excel', 'verb'=>'GET'),
				array('admin/surveyExport', 'pattern'=>'/admin/survey/export', 'verb'=>'GET'),

				array('admin/surveyMangeForm', 'pattern'=>'/admin/survey/<id:\d+>/mange', 'verb'=>'GET'),
				array('admin/SurveyMangeInsert', 'pattern'=>'/admin/survey/<id:\d+>/mange/insert', 'verb'=>'POST'),
				array('admin/SurveyMangeUpdate', 'pattern'=>'/admin/survey/<id:\d+>/mange/update', 'verb'=>'POST'),
				array('admin/surveyMangeCreate', 'pattern'=>'/admin/survey/<id:\d+>/mange/create', 'verb'=>'GET'),
				array('admin/surveyMangeEdit', 'pattern'=>'/admin/survey/<id:\d+>/mange/<survey_id:\d+>/edit', 'verb'=>'GET'), // Edit form

				array('admin/surveyAnswerForm', 'pattern'=>'/admin/survey/<id:\d+>/answer/create', 'verb'=>'GET'),
				array('admin/surveyAnswerDelete', 'pattern'=>'/admin/survey/<id:\d+>/answer/delete', 'verb'=>'POST'), // ok
				array('admin/surveyAnswerEdit', 'pattern'=>'/admin/survey/<id:\d+>/answer/<answer_id:\d+>/edit', 'verb'=>'GET'), // Edit form
				array('admin/surveyAnswerUpdate', 'pattern'=>'/admin/survey/<id:\d+>/answer/<answer_id:\d+>/edit', 'verb'=>'POST'), // Edit form
				array('admin/surveyAnswerInsert', 'pattern'=>'/admin/survey/<id:\d+>/answer', 'verb'=>'POST'),
				array('admin/surveyInsertExcel', 'pattern'=>'/admin/survey/<id:\d+>/excel', 'verb'=>'POST'),


				array('admin/questionaire', 'pattern'=>'/admin/questionaire/question', 'verb'=>'GET'),
				array('admin/questionaireExcel', 'pattern'=>'/admin/questionaire/excel', 'verb'=>'GET'),

				array('admin/questionaireForm', 'pattern'=>'/admin/questionaire/question/create', 'verb'=>'GET'),
				array('admin/questionaireDelete', 'pattern'=>'/admin/questionaire/question/delete', 'verb'=>'POST'), // ok
				array('admin/questionaireEdit', 'pattern'=>'admin/questionaire/question/<id:\d+>/edit', 'verb'=>'GET'), // Edit form
				array('admin/questionaireUpdate', 'pattern'=>'admin/questionaire/question/<id:\d+>/edit', 'verb'=>'POST'), // Edit form
				array('admin/questionaireInsert', 'pattern'=>'/admin/questionaire/question', 'verb'=>'POST'),
				array('admin/questionaireInsertExcel', 'pattern'=>'/admin/questionaire/excel', 'verb'=>'POST'),

				array('admin/Questionairetemplateexcel', 'pattern'=>'/admin/questionaire/Templateexcel', 'verb'=>'GET'),

				array('admin/questionaireExport', 'pattern'=>'/admin/questionaire/export', 'verb'=>'GET'),
				array('admin/questionaireExportexcel', 'pattern'=>'/admin/questionaire/exportexcel', 'verb'=>'POST'),

				array('admin/questionaireSurvey', 'pattern'=>'/admin/questionaire/survey', 'verb'=>'GET'),
				array('admin/questionaireSurveyForm', 'pattern'=>'/admin/questionaire/survey/create', 'verb'=>'GET'),
				array('admin/questionaireSurveyDelete', 'pattern'=>'/admin/questionaire/survey/delete', 'verb'=>'POST'), // ok
				array('admin/questionaireSurveyEdit', 'pattern'=>'admin/questionaire/survey/<id:\d+>/edit', 'verb'=>'GET'), // Edit form
				array('admin/questionaireSurveyUpdate', 'pattern'=>'admin/questionaire/survey/<id:\d+>/edit', 'verb'=>'POST'), // Edit form
				array('admin/questionaireSurveyFormManagement', 'pattern'=>'admin/questionaire/survey/<id:\d+>/form', 'verb'=>'GET'), // Form management
				array('admin/questionaireSurveyFormAction', 'pattern'=>'admin/questionaire/survey/<id:\d+>/form', 'verb'=>'POST'), // Form management
				array('admin/questionaireSurveyInsert', 'pattern'=>'/admin/questionaire/survey', 'verb'=>'POST'),

				array('admin/surveyAnswer', 'pattern'=>'/admin/survey/<id:\d+>/answer', 'verb'=>'GET'),
				array('admin/surveyGroup', 'pattern'=>'/admin/survey/<id:\d+>/group', 'verb'=>'GET'),

				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
				// '<module:\w+>/<controller:\w+>/<action:\w+>'=>'<module>/<controller>/<action>',
				// '<module:\w+>/<controller:\w+>/<action:\w+>/<id:\d+>'=>'<module>/<controller>/<action>',
			),
		),

		// database settings are configured in database.php
		'db'=>require(dirname(__FILE__).'/database.php'),

		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>YII_DEBUG ? null : 'site/error',
		),

		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),

		'booster' => array(
            'class' => 'ext.booster.components.Booster',
        ),

	),


	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'webmaster@example.com',
	),
);
