<?php 

class MasterpatController extends Controller 
{
	public $layout='admin';
    public function actionIndex()
    {
        $model = new MasterPat;
		$criteria = new CDbCriteria;
		$criteria->compare('active', 1);
		$criteria->order = 'pat_name ASC';
		// var_dump($criteria->order);exit();
		$data = MasterPat::model()->findAll($criteria);
		$this->render('index', Array(
			'data' => $data,
			'model' => $model,
		));
    }

    public function actionCreate()
    {
        $model=new MasterPat;
		if(isset($_POST['MasterPat']))
    	{
			$model->attributes = $_POST['MasterPat'];
			$model->active = 1;
			if($model->validate()){
                // var_dump($model->attributes);exit();
				if($model->save()){
					$this->redirect(array('view', 'id' => $model->pat_id));
				}
			}
		}
		$this->render('create', Array(
			'model' => $model
		));
    }

    public function actionView($id)
    {
        $this->render('view', Array(
			'model' => $this->loadModel($id),
		));
    }

    public function actionEdit($id)
    {
        $model = $this->loadModel($id);

		if (isset($_POST['MasterPat']))
		{
			$model->attributes = $_POST['MasterPat'];
			if ($model->validate()){
				if ($model->save()){
					$this->redirect(array('view', 'id' => $model->pat_id));
				}
			}
		}
		$this->render('update', Array(
			'model' => $model
		));
    }

    public function actionDelete($id)
	{
		$model = $this->loadModel($id);
		$model->active = 0;

		if ($model->save()) {
			$this->redirect(array('index'));
		}
	}

    public function loadModel($id)
	{
		$model = MasterPat::model()->findByPk($id);
		return $model;
	}
}

?>