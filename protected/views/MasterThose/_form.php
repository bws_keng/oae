<?php $form=$this->beginWidget('AActiveForm', array(
 'id'=>'masterThose-form',
 'enableClientValidation'=>true,
 'clientOptions'=>array(
  'validateOnSubmit'=>true
),
 'errorMessageCssClass' => 'label label-important',
 'htmlOptions' => array('enctype' => 'multipart/form-data')
)); ?>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12 set-height set-padding">
            <div id="section1">
                <div class="card">
                    <div class="card-header">
                        <h5 class="mb-1"><?=$formtext?> </h5>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <div>
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <?php echo $form->labelEx($model,'those_name'); ?>
                                    <?php echo $form->textField($model,'those_name',array('size'=>60,'maxlength'=>250, 'class'=>'form-control', 'maxlength' => 2)); ?>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <?php echo $form->labelEx($model,'those_code'); ?>
                                    <?php echo $form->numberField($model,'those_code',array('size'=>60,'maxlength'=>250, 'class'=>'form-control', 'min' => 1, 'max' => 9)); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row text-center group-submit">
        <?php echo CHtml::tag('button',array('class' => 'btn btn-primary btn-lg'),'<i></i>บันทึกข้อมูล');?>
        <button type="button" id="button-cancel" class="btn btn-outline-secondary btn-lg" onclick="window.history.go(-1); return false;">ยกเลิก</button>
    </div>

    <?php $this->endWidget(); ?>
</div>

<script type="text/javascript">

    function isNumberKey(evt) //Typing num only
    {
        var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
    }

    document.getElementById('MasterThose_those_name').onkeypress = function(evt){
        return isNumberKey(evt);
    }

    document.getElementById('MasterThose_those_code').onkeypress = function(evt){
        return isNumberKey(evt);
    }
 
    var maxLength_chars = 1; //Max Length
    $('#MasterThose_those_code').keydown( function(e){
        if ($(this).val().length >= maxLength_chars) { 
            $(this).val($(this).val().substr(0, maxLength_chars));
        }
    });

    $('#MasterThose_those_code').keyup( function(e){
        if ($(this).val().length >= maxLength_chars) { 
            $(this).val($(this).val().substr(0, maxLength_chars));
        }
    });
    // set char to "1"
    // if first char is "0"
    $("#MasterThose_those_code").on("input", function() { 
        if (/^0/.test(this.value)) {
            this.value = this.value.replace(/^0/, "1")
        }
    })
</script>

