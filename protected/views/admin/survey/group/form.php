<form class="container-fluid" method="POST" action="<?php echo Yii::app()->baseurl.'/admin/survey/'.$_GET['id'].'/group/'.$_GET['survey_id'].'/form'; ?>">
    <!-- start row -->
    <div class="row">
        <div class="col-lg-12 set-height set-padding">
            <div id="section1">
                <div class="card">
                    <div class="card-header">
                        <h5 class="mb-1">จัดฟอร์มแบบสอบถาม</h5>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <div class="col-md-12 mb-3">
                            <label for="">รายการคำถาม</label>
                        </div>
                        <form>
                            <div class="row ui-sortable" id="draggableMultiple">
                                <?php 
                                    foreach ($data as $key => $value) {
                                        ?>
                                        <div class="col-<?php echo $value['size']; ?>" id="form-question-<?php echo $value['question_id']; ?>">
                                            <div class="card">
                                                <div class="row card-body">
                                                    <label class="col-6 col-form-label">
                                                        <p class="mb-0"><?php echo $value['question'] ; ?> 
                                                            <?php
                                                            if($value['readable_id'] != null){
                                                                echo '<red>('.$value['readable_id'].')</red>';
                                                            }
                                                            ?>
                                                        </p>
                                                    </label>
                                                    <div class="row col-6">
                                                        <label class="col-12 text-right col-form-label">ขนาด</label>
                                                        <input class="col-12 form-control" min="1" max="12" type="number" name="size[]" value="<?php echo $value['size']; ?>" onchange="document.getElementById('form-question-<?php echo $value['question_id']; ?>').className = 'col-'+(this.value)"/>
                                                    </div>
                                                    <input type="hidden" class="form-control" name="form[]" value="<?php echo $value['question_id']; ?>" placeholder="">
                                                    <!-- <div class="col-12">
                                                        <input class="form-control text-center mt-3" maxlength="1" type="number" data-original-title="" title="" disabled />
                                                    </div> -->
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                ?>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="row text-center group-submit">
                <button class="btn btn-primary btn-lg">บันทึกข้อมูล</button>
            </div>
        </div>
    </div>
    <!-- end row -->
</form>