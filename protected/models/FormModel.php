<?php
class FormModel extends NewModel
{
    public static function generate($form_id,$survey_id=null){
        if ($survey_id == null) {
            $sql = 'SELECT
            question_group.question_group_name, 
            question_group.question_group_number, 
            question.question, 
            question."type", 
            question.masterdata, 
            question.question_id, 
            question.readable_id,
            question.options,
            survey."id", 
            survey."year", 
            survey."start", 
            survey."end",
            survey_map."size",
            survey_map."order",
            survey_map."isRequired"
            FROM
            "'.schema_name.'".question_group
            INNER JOIN
            "'.schema_name.'".question
            ON 
            question_group.question_group_id = question.question_group_id
            INNER JOIN
            "'.schema_name.'".survey_map
            ON 
            question.question_id = survey_map.question
            INNER JOIN
            "'.schema_name.'".survey
            ON 
            survey_map.survey = survey."id"
            WHERE
            survey."id" = (SELECT survey."id" FROM "'.schema_name.'".survey ORDER BY survey."id" DESC LIMIT 1 OFFSET 0) AND
            question_group."question_group_id" = '.$form_id.'
            ORDER BY
            survey_map."order" ASC';
        }else{
            $sql = 'SELECT
            question_group.question_group_name, 
            question_group.question_group_number, 
            question.question, 
            question."type", 
            question.masterdata, 
            question.question_id, 
            question.readable_id,
            question.options,
            survey."id", 
            survey."year", 
            survey."start", 
            survey."end",
            survey_map."size",
            survey_map."order",
            survey_map."isRequired"
            FROM
            "'.schema_name.'".question_group
            INNER JOIN
            "'.schema_name.'".question
            ON 
            question_group.question_group_id = question.question_group_id
            INNER JOIN
            "'.schema_name.'".survey_map
            ON 
            question.question_id = survey_map.question
            INNER JOIN
            "'.schema_name.'".survey
            ON 
            survey_map.survey = survey."id"
            WHERE
            survey."id" = (SELECT survey."id" FROM "'.schema_name.'".survey WHERE survey."id" = '.$survey_id.'  ORDER BY survey."id" DESC LIMIT 1 OFFSET 0) AND
            question_group."question_group_id" = '.$form_id.'
            ORDER BY
            survey_map."order" ASC';
        }
        
        return FormModel::query($sql);
    }

    public static function list(){
        $sql = 'SELECT * FROM "'.schema_name.'"."question_group"';
        return FormModel::query($sql);
    }

    public static function getTitle($id){
        $sql = 'SELECT * FROM "'.schema_name.'"."question_group" WHERE "question_group_id" = '.$id;
        return FormModel::query($sql);
    }

    public static function insert($data){
        $sql = 'SELECT
            questionnaire.*
        FROM
            "'.schema_name.'".questionnaire
        WHERE
          questionnaire.province = '."'".($data['maindata-province'])."'".' AND
            questionnaire.district = '."'".($data['maindata-district'])."'".' AND
            questionnaire.subdistrict = '."'".($data['maindata-sub-district'])."'".' AND
            questionnaire.village = '."'".($data['maindata-village-no'])."'".' AND
            questionnaire.group = '."'".($data['maindata-group'])."'".' AND
            questionnaire.sample = '."'".($data['maindata-sample'])."'".'';

        $query = FormModel::query($sql);
        if(count($query) <= 0){
            throw new Exception("ข้อมูลแบบสอบถามไม่มีอยู่", 1);
        }
        $questionnaire = $query[0];
        $tmp = Array();
        $survey_group = FormModel::query('INSERT INTO "'.schema_name.'"."survey_group"("timestamp") VALUES (NOW()) RETURNING *')[0]['survey_group'];
        
        foreach($data as $key => $value){
            $exp_key = explode("-", $key);
            if($exp_key[0] == 'question'){
                $value = str_replace(",","",$value);
                array_push($tmp, FormModel::query('INSERT INTO "'.schema_name.'"."survey_value"("questionnaire_id", "question_id", "value", "survey_group") 
                VALUES ('.$questionnaire['questionnaire_id'].', '.$exp_key[1].', \''.$value.'\', '.$survey_group.') RETURNING *')[0]);
            }
            // if ($exp_key[0] == "inputquestion") {
            //     $value_array = ["inputquestion"=>$value,"inputquestionparent"=>$data["inputquestionparent-".$exp_key[1]]];
            //     $json_value_array = json_encode($value_array);
            //     array_push($tmp, FormModel::query('INSERT INTO "'.schema_name.'"."survey_value"("questionnaire_id", "question_id", "value", "survey_group") 
            //     VALUES ('.$questionnaire['questionnaire_id'].', '.$exp_key[1].', \''.$json_value_array.'\', '.$survey_group.') RETURNING *')[0]);
            // }
        }
        
        return $questionnaire;
    }

    public static function delete($key){
        $sql = 'DELETE FROM "'.schema_name.'"."survey_value" WHERE "questionnaire_id" = '.intval($key);
        FormModel::query($sql);
        $sql = 'DELETE FROM "'.schema_name.'"."survey_group" WHERE "survey_group" = '.intval($key);
        FormModel::query($sql);
    }

    public static function update($survey_group, $data)
    {
        $out = Array();
        foreach($data as $key => $value){
            $exp_key = explode("-", $key);
            if($exp_key[0] == 'question'){
                $value = str_replace(",","",$value);
                array_push($out, FormModel::query('UPDATE "oae-survey"."survey_value" SET "value" = \''.$value.'\' WHERE "survey_group" = '.intval($survey_group).' AND "question_id" = '.intval($exp_key[1]).' RETURNING *')[0]);
            }
        }
        return $out;
    }

    public static function getData($question_group_id){

        $sqlSchema =  'SELECT
                        "'.schema_name.'".question_mange.question_id
                        FROM
                        "'.schema_name.'".question_mange
                        WHERE
                        "'.schema_name.'".question_mange.question_group_id = \''.$question_group_id.'\'
                        AND 
                        "'.schema_name.'".question_mange.question_status = \''."1".'\' 
                        ORDER BY
                        "'.schema_name.'".question_mange.sortorder ASC';
        $schemasearchQuery = FormModel::query($sqlSchema);

        if (empty($schemasearchQuery)) {

            $schema = FormModel::query('SELECT question.question_id,question.question FROM "'.schema_name.'".question WHERE question.question_group_id = '.$question_group_id.' AND question.type NOT IN (\''."header".'\',\''."note".'\',\''."line".'\',\''."bottom-line".'\',\''."header".'\',\''."bullet".'\')');
            $schemasearch = "";
            foreach ($schema as $key => $value) {
                $schemasearch = $schemasearch . $value["question_id"] . ",";
            }
            $schemasearch = substr($schemasearch, 0, -1);
            $sqlSchema =  'SELECT
                            "'.schema_name.'".survey_map.question
                            FROM
                            "'.schema_name.'".survey_map
                            WHERE
                            "'.schema_name.'".survey_map.question IN ('.$schemasearch.') 
                            ORDER BY
                                "'.schema_name.'".survey_map."order" ASC LIMIT 3';

            $schemasearchQuery = FormModel::query($sqlSchema);

            $schemasearch = "";
            foreach ($schemasearchQuery as $key => $value) {
               $schemasearch = $schemasearch . $value["question"] . ",";
            }
            $schemasearch = substr($schemasearch, 0, -1);
            $schemaarray = array();
            foreach ($schemasearchQuery as $key => $value) {
                $schema = FormModel::query('SELECT question.question_id,question.question FROM "'.schema_name.'".question WHERE question.question_id = '.$value["question"].'');
                array_push($schemaarray, [
                    'question_id' => $schema[0]["question_id"],
                    'question' => $schema[0]["question"],
                ]);
            }
            $schema = $schemaarray;
        }else{
       
            $schemasearch = "";
            foreach ($schemasearchQuery as $key => $value) {
               $schemasearch = $schemasearch . $value["question_id"] . ",";
            }
            $schemasearch = substr($schemasearch, 0, -1);
            $schemaarray = array();
            foreach ($schemasearchQuery as $key => $value) {
                $schema = FormModel::query('SELECT question.question_id,question.question FROM "'.schema_name.'".question WHERE question.question_id = '.$value["question_id"].'');
                array_push($schemaarray, [
                    'question_id' => $schema[0]["question_id"],
                    'question' => $schema[0]["question"],
                ]);
            }
            $schema = $schemaarray;
        }

        // $schema = FormModel::query('SELECT question.question_id,question.question FROM "'.schema_name.'".question WHERE question.question_id IN ('.$schemasearch.')');
        // var_dump($schemasearch);exit();
        $sql = 'SELECT
            survey_value.*
        FROM
            "'.schema_name.'".survey_value
            INNER JOIN
            "'.schema_name.'".question
            ON 
                survey_value.question_id = question.question_id
            INNER JOIN 
            "'.schema_name.'".questionnaire 
            ON 
            "'.schema_name.'".survey_value.questionnaire_id = "oae-survey".questionnaire.questionnaire_id
        WHERE
            survey_value.question_id IN ( '.$schemasearch.' )
        ';

        ///////// เจ้าหน้าที่เห็นเฉพาะเขตตัวเอง
        $User = User::model()->findByPk(Yii::app()->user->id);
        $profile = $User->profile;
        if ($User->superuser != 1) {
            $sql = $sql .' AND
            questionnaire."office_id" = \''.$profile->county_id.'\' ';
        }
        /////////////////////////////////////////////////////////////
        $sql = $sql .' LIMIT 1000';

        $items = FormModel::query($sql);

        $data = Array();
        foreach($items as $item){
            $data[$item['survey_group']][$item['question_id']]= $item['value'];
        }
        return Array('schema'=>$schema, 'data' => $data);
    }

    public static function listSearch($question_group_id,$year,$Province,$District,$subdistrict,$village,$group,$sample){
        $sqlSchema =  'SELECT
                        "'.schema_name.'".question_mange.question_id
                        FROM
                        "'.schema_name.'".question_mange
                        WHERE
                        "'.schema_name.'".question_mange.question_group_id = \''.$question_group_id.'\'
                        AND 
                        "'.schema_name.'".question_mange.question_status = \''."1".'\' 
                        ORDER BY
                        "'.schema_name.'".question_mange.sortorder ASC';
        $schemasearchQuery = FormModel::query($sqlSchema);
        if (empty($schemasearchQuery)) {
            $schema = FormModel::query('SELECT question.question_id,question.question FROM "'.schema_name.'".question WHERE question.question_group_id = '.$question_group_id.' AND question.type NOT IN (\''."header".'\',\''."note".'\',\''."line".'\',\''."bottom-line".'\',\''."header".'\',\''."bullet".'\')  LIMIT 3');
            $sql = 'SELECT
                survey_value.*
            FROM
                "'.schema_name.'".survey_value
                INNER JOIN
                "'.schema_name.'".question
                ON 
                    survey_value.question_id = question.question_id
                
                INNER JOIN 
                "'.schema_name.'".questionnaire 
                ON 
                    "'.schema_name.'".survey_value.questionnaire_id = "oae-survey".questionnaire.questionnaire_id
            WHERE
                survey_value.question_id IN ( SELECT question.question_id FROM "'.schema_name.'".question WHERE question.question_group_id = '.$question_group_id.' AND question.type NOT IN (\''."header".'\',\''."note".'\',\''."line".'\',\''."bottom-line".'\',\''."header".'\',\''."bullet".'\') LIMIT 3 ) ';
            if($year != null){
                $sql = $sql .' AND
                questionnaire."survey_id" = \''.$year.'\'';
            }
            if ($Province != null) {
                $sql = $sql .' AND
                questionnaire."province" = \''.$Province.'\'';
            }
            if ($District != null) {
                $sql = $sql .' AND
                questionnaire."district" = \''.$District.'\'';
            }
            if ($subdistrict != null) {
                $sql = $sql .' AND
                questionnaire."subdistrict" = \''.$subdistrict.'\'';
            }
            if ($village != null) {
                $sql = $sql .' AND
                questionnaire."village" = \''.$village.'\'';
            }
            if ($group != null) {
                $sql = $sql .' AND
                questionnaire."group" = \''.$group.'\'';
            }
            if ($sample != null) {
                $sql = $sql .' AND
                questionnaire."sample" = \''.$sample.'\'';
            }
            ///////// เจ้าหน้าที่เห็นเฉพาะเขตตัวเอง
            $User = User::model()->findByPk(Yii::app()->user->id);
            $profile = $User->profile;
            if ($User->superuser != 1) {
                $sql = $sql .' AND
                questionnaire."office_id" = \''.$profile->county_id.'\'';
            }
            /////////////////////////////////////////////////////////////
            $items = FormModel::query($sql); 
              
        }else{
           
            $schemasearch = "";
            foreach ($schemasearchQuery as $key => $value) {
               $schemasearch = $schemasearch . $value["question_id"] . ",";
            }
            $schemasearch = substr($schemasearch, 0, -1);
             $schema = FormModel::query('SELECT question.question_id,question.question FROM "'.schema_name.'".question WHERE 
                question.question_id IN ( '.$schemasearch.' ) AND question.question_group_id = '.$question_group_id.' AND question.type NOT IN (\''."header".'\',\''."note".'\',\''."line".'\',\''."bottom-line".'\',\''."header".'\',\''."bullet".'\') ');
             $sql = 'SELECT
                survey_value.*
            FROM
                "'.schema_name.'".survey_value
                INNER JOIN
                "'.schema_name.'".question
                ON 
                    survey_value.question_id = question.question_id
                
                INNER JOIN 
                "'.schema_name.'".questionnaire 
                ON 
                    "'.schema_name.'".survey_value.questionnaire_id = "oae-survey".questionnaire.questionnaire_id
            WHERE
                survey_value.question_id IN ( SELECT question.question_id FROM "'.schema_name.'".question WHERE question.question_id IN ( '.$schemasearch.' ) AND question.question_group_id = '.$question_group_id.' AND question.type NOT IN (\''."header".'\',\''."note".'\',\''."line".'\',\''."bottom-line".'\',\''."header".'\',\''."bullet".'\')) ';
            if($year != null){
                $sql = $sql .' AND
                questionnaire."survey_id" = \''.$year.'\'';
            }
            if ($Province != null) {
                $sql = $sql .' AND
                questionnaire."province" = \''.$Province.'\'';
            }
            if ($District != null) {
                $sql = $sql .' AND
                questionnaire."district" = \''.$District.'\'';
            }
            if ($subdistrict != null) {
                $sql = $sql .' AND
                questionnaire."subdistrict" = \''.$subdistrict.'\'';
            }
            if ($village != null) {
                $sql = $sql .' AND
                questionnaire."village" = \''.$village.'\'';
            }
            if ($group != null) {
                $sql = $sql .' AND
                questionnaire."group" = \''.$group.'\'';
            }
            if ($sample != null) {
                $sql = $sql .' AND
                questionnaire."sample" = \''.$sample.'\'';
            }
            ///////// เจ้าหน้าที่เห็นเฉพาะเขตตัวเอง
            $User = User::model()->findByPk(Yii::app()->user->id);
            $profile = $User->profile;
            if ($User->superuser != 1) {
                $sql = $sql .' AND
                questionnaire."office_id" = \''.$profile->county_id.'\'';
            }
             /////////////////////////////////////////////////////////////

            $sql = $sql .' LIMIT 1000';
            $items = FormModel::query($sql); 
        }
     
        $data = Array();
        foreach($items as $item){
            $data[$item['survey_group']][$item['question_id']]= $item['value'];
        }
        return Array('schema'=>$schema, 'data' => $data);
    }

    public static function questionCreate($input){
        if($input['question_number'] == NULL){
            $input['question_number'] = 'NULL';
        }
        if(!isset($input['options'])){
            $input['options'] = 'NULL';
        }else{
            $input['options'] = '\''.$input['options'].'\'';
        }
        if($input['type'] == 'master'){
            $sql = 'INSERT INTO "'.schema_name.'"."question"("question_group_id", "question", "type", "masterdata", "readable_id", "options") VALUES ('.$input['question_group'].', \''.$input['question'].'\', \''.$input['type'].'\', \''.$input['master'].'\', '.$input['question_number'].', '.$input['options'].') RETURNING *';
        }else{
            $sql = 'INSERT INTO "'.schema_name.'"."question"("question_group_id", "question", "type", "readable_id", "options") VALUES ('.$input['question_group'].', \''.$input['question'].'\', \''.$input['type'].'\', '.$input['question_number'].', '.$input['options'].') RETURNING *';
        }
        return FormModel::query($sql);
    }

    public static function getQuestion($key, $group){
        $sql = 'SELECT * FROM "'.schema_name.'"."question" WHERE "question_id" = \''.$key.'\' AND "question_group_id" = \''.$group.'\';';
        return FormModel::query($sql);
    }
    public static function questionDelete($key){
        $sql = 'DELETE FROM "'.schema_name.'"."survey_value" WHERE "question_id" = '.intval($key);
        FormModel::query($sql);
        $sql = 'DELETE FROM "'.schema_name.'"."survey_map" WHERE "question" = '.intval($key);
        FormModel::query($sql);
        $sql = 'DELETE FROM "'.schema_name.'"."question" WHERE "question_id" = '.intval($key);
        return FormModel::query($sql);
    }
    public static function questionUpdate($key, $group, $data){
        if($data['question_number'] == NULL){
            $data['question_number'] = 'NULL';
        }
        if ($data["type"] =="output" ||$data["type"] =="sum-output" || $data["type"]=="sum-output-number" ) {
            $array_option = array();
            foreach ($data["optionsselect"] as $keyCheck => $value) {
                array_push($array_option, $value);
            }
            $data["options"] = json_encode($array_option);
        }
        $sql = 'UPDATE "'.schema_name.'"."question" SET "question" = \''.$data['question'].'\', "type" = \''.$data['type'].'\', "options" = \''.$data['options'].'\', "masterdata" = '.($data['master'] == null? 'NULL': '\''.$data['master'].'\'').', "readable_id" = '.$data['question_number'].' WHERE "question_id" = '.intval($key).' RETURNING *;';
        return FormModel::query($sql);
    }

    public static function questionList($id){
        $sql = '
        SELECT
            question.question, 
            question.question_group_id, 
            question."type", 
            question.masterdata, 
            question.question_id,
            question.readable_id
        FROM
            "'.schema_name.'".question
        WHERE
            question.question_group_id = '.$id.'
        ORDER BY
            question.question_id DESC';
        return FormModel::query($sql);
    }

    public static function questionListSearch($id,$question){
        $sql = '
        SELECT
            question.question, 
            question.question_group_id, 
            question."type", 
            question.masterdata, 
            question.question_id,
            question.readable_id
        FROM
            "'.schema_name.'".question
        WHERE
            question.question_group_id = '.$id.'';
        if ($question != null) {
            $sql = $sql .' AND "oae-survey".question.readable_id ='.$question;
        }

        $sql = $sql .' ORDER BY
            question.question_id DESC';
        return FormModel::query($sql);
    }

    public static function questionListRate($id){
        $sql = '
        SELECT
            question.question, 
            question.question_group_id, 
            question."type", 
            question.masterdata, 
            question.question_id
        FROM
            "'.schema_name.'".question
        WHERE
            question.question_group_id = '.$id.'
        ORDER BY
            question.sortorder ASC';
        return FormModel::query($sql);
    }

    public static function questionListMange($id){
        // $sql = '
        // SELECT
        //     question.question, 
        //     question.question_group_id, 
        //     question."type", 
        //     question.masterdata, 
        //     question.question_id,
        //     question_mange."question_status"
        // FROM
        //     "'.schema_name.'".question
        // INNER JOIN
        //     "'.schema_name.'".question_mange
        // ON 
        //     question_mange.question_id = question.question_id
        // WHERE
        //     question.question_group_id = '.$id;
        // return FormModel::query($sql);
        $sql = '
        SELECT
            question.question, 
            question.question_group_id, 
            question."type", 
            question.masterdata, 
            question.question_id
        FROM
            "'.schema_name.'".question
        WHERE
            question.question_group_id = '.$id;
        return FormModel::query($sql);
    }

    public static function getAnswer($id, $filter='group'){
        switch ($filter) {
            case 'group':
                $sql = '
                    SELECT
                        answer.*, 
                        question.question_group_id,
                        question."readable_id" AS readable_id
                    FROM
                        "'.schema_name.'".answer
                        INNER JOIN
                        "'.schema_name.'".question
                        ON 
                            answer.question_id = question.question_id
                        INNER JOIN
                        "'.schema_name.'".survey_map
                        ON 
                            question.question_id = survey_map.question
                    WHERE
                        question.question_group_id = '.$id.'
                    ORDER BY
                        readable_id ASC';
                break;
            case 'id':
                $sql = '
                SELECT
                    answer.*, 
                    question.question_group_id,
                    question.readable_id
                FROM
                    "'.schema_name.'".answer
                    INNER JOIN
                    "'.schema_name.'".question
                    ON 
                        answer.question_id = question.question_id
                WHERE
                    answer.answer_id = '.$id;
                break;
            
        }
    
        return FormModel::query($sql);
    }

    public static function getSurvey($group){
        $out = Array();
        $value = 'SELECT * FROM "'.schema_name.'"."survey_value" WHERE "survey_group" = \''.$group.'\'';
        $tmp = Array();
        $value_query = FormModel::query($value);
        foreach ($value_query as $key => $value) {
            $out['value'][$value['question_id']] = $value['value'];
        }
        if(count($out['value']) > 0){
            $questionaire = 'SELECT * FROM "'.schema_name.'"."questionnaire" WHERE "questionnaire_id" = \''.$value_query[0]['questionnaire_id'].'\'';
            $out['questionaire'] = FormModel::query($questionaire)[0];
        }
        // print_r($out);exit();
        return $out;
    }

    public static function orderMap($survey, $data, $size)
    {
        foreach ($data as $key => $value) {
            // $sql = 'UPDATE "'.schema_name.'"."survey_map" SET "order" = '.$key.' WHERE "survey" = '.$survey.' AND "question" = '.$value.';';
            $sql = 'UPDATE "'.schema_name.'"."survey_map" SET "size" = '.$size[$key].',"order" = '.intval($key).' WHERE "survey" = '.intval($survey).' AND "question" = '.intval($value).';';
            // echo $sql;
            QuestionnaireModel::query($sql);
        }
        
    }
    
}
?>