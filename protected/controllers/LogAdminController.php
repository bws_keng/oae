<?php

class LogUserController extends Controller
{
    public function init()
    {
        parent::init();
        $this->lastactivity();
        
    }

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            // 'rights- toggle, switch, qtoggle',
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
  

	public function actionIndex()
	{
        $model=new LogUsers('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['LogAdmin']))
            $model->attributes=$_GET['LogAdmin'];

        $this->render('index',array(
            'model'=>$model,
        ));
	}

    public function actionAuto()
    {
        $model = new LogUserAuto('search');
        $model->unsetAttributes();  // clear any default values

        // var_dump($model); exit();
        
        if(isset($_GET['LogUserAuto'])){
            $model->attributes=$_GET['LogUserAuto'];
        }

        $this->render('auto',array(
            'model'=>$model,
        ));
    }

    public function actionUsers()
    {
        $model=new LogUsers('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['LogUsers']))
            $model->attributes=$_GET['LogUsers'];

        $this->render('users',array(
            'model'=>$model,
        ));
    }

    public function actionEmail()
    {
        $model=new LogEmail('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['LogEmail']))
            $model->attributes=$_GET['LogEmail'];

        $this->render('email',array(
            'model'=>$model,
        ));
    }
}