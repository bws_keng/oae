<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="description" content="Cuba admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, Cuba admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="pixelstrap">
    <title>หมวดที่ 5 รายจ่ายเงินสดเกษตรทางสัตว์</title>
    <?php include 'theme/assets/include/inc-head.php'; ?>
</head>

<body data-spy="scroll" data-target="#myScrollspy">
    <div class="tap-top"><i data-feather="chevrons-up"></i></div>

    <div class="page-wrapper compact-wrapper" id="pageWrapper">

        <?php include 'theme/assets/include/inc-header.php'; ?>

        <div class="page-body-wrapper horizontal-menu">
            <?php include 'theme/assets/include/inc-menuleft.php'; ?>

 
                <div class="container-fluid">

                    <!-- start row -->
                    <div class="row">
                        <div class="col-lg-12 set-height set-padding">
                            <div id="section1">
                                <div class="card">
                                    <div class="card-header">
                                        <h5 class="mb-1">หมวดที่ 5 รายจ่ายเงินสดเกษตร<span class="line-danger">ทางสัตว์ </span> <span>(ปี พ.ศ.2562 - พ.ศ.2563)</span></h5>
                                    </div>
                                </div>

                                <div class="row justify-content-end">
                                    <div class="col-md-3">
                                        <a href="1-5-1_form.php">
                                            <div class="card o-hidden create-main">
                                                <div class="b-r-4 card-body">
                                                    <div class="media static-top-widget">
                                                        <div class="align-self-center text-center"><i data-feather="plus"></i></div>
                                                        <div class="media-body">
                                                            <h6 class="mb-0 counter">เพิ่มข้อมูล</h6><i class="icon-bg" data-feather="database"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>

                                <div class="card table-card">
                                    <div class="card-header">
                                        <div class="row">
                                            <div class="col-md-3 mb-3">
                                                <label for="">รหัสสินค้า</label>
                                                <input class="form-control" id="" maxlength="4"  type="text" placeholder="" required="" data-original-title="" title="">
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <div class="form-group">
                                                    <label for="">จังหวัด</label>
                                                    <select class="form-control digits" id="">
                                                        <option selected disabled>เลือกจังหวัด</option>
                                                        <option>1</option>
                                                        <option>2</option>
                                                        <option>3</option>
                                                        <option>4</option>
                                                        <option>5</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-3 mb-3">
                                                <div class="form-group">
                                                    <label for="">ปี พ.ศ.</label>
                                                    <select class="form-control digits" id="">
                                                        <option selected disabled>เลือกปี พ.ศ.</option>
                                                        <option>1</option>
                                                        <option>2</option>
                                                        <option>3</option>
                                                        <option>4</option>
                                                        <option>5</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-3 mb-3">
                                                <button class="btn btn-lg btn-search text-white" type="submit"><i class="icon-search"></i>&nbsp;ค้นหา</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-main" id="">
                                                <!-- <table class="table table-striped table-main" id="basic-2"> -->
                                                <thead>
                                                    <tr>
                                                        <th width="5%">ลำดับ</th>
                                                        <th>รหัสสินค้า</th>
                                                        <th>ชื่อสัตว์และผลิตภัณฑ์สัตว์</th>
                                                        <th>จังหวัด</th>
                                                        <th width="25%">จัดการข้อมูล</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>1</td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td>
                                                            <button type="button" class="btn btn-icon text-primary"><i data-feather="eye"></i></button>
                                                            <button type="button" class="btn btn-icon text-success"><i data-feather="edit"></i></button>
                                                            <button type="button" class="btn btn-icon text-danger"><i data-feather="trash"></i></button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>2</td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td>
                                                            <button type="button" class="btn btn-icon text-primary"><i data-feather="eye"></i></button>
                                                            <button type="button" class="btn btn-icon text-success"><i data-feather="edit"></i></button>
                                                            <button type="button" class="btn btn-icon text-danger"><i data-feather="trash"></i></button>
                                                        </td>
                                                    </tr>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                </div>

                            </div>

                        </div>
                    </div>
                    <!-- end row -->

                </div>


 