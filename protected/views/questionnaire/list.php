
  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="http://cdn.datatables.net/1.10.2/css/jquery.dataTables.min.css"></style>
<script type="text/javascript" src="http://cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js"></script>

                <div class="container-fluid">

                    <!-- start row -->
                    <div class="row">
                        <div class="col-lg-12 set-height set-padding">
                            <div id="section1">
                                <div class="card">
                                    <div class="card-header">
                                        <h5 class="mb-1">แบบสอบถามภาวะเศรษฐกิจสังคมครัวเรือนและแรงงานเกษตร ปีเพาะปลูก</h5>
                                    </div>
                                </div>

                                <div class="row justify-content-end">
                                    <div class="col-md-3">
                                        <a href="<?php echo Yii::app()->baseurl; ?>/questionnaire/create">
                                            <div class="card o-hidden create-main">
                                                <div class="b-r-4 card-body">
                                                    <div class="media static-top-widget">
                                                        <div class="align-self-center text-center"><i data-feather="plus"></i></div>
                                                        <div class="media-body">
                                                            <h6 class="mb-0 counter">เพิ่มข้อมูล</h6><i class="icon-bg" data-feather="database"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                
                                <div class="card table-card">
                         <!--            <form method="GET" accept-charset="UTF-8" id="form_search" action="<?php echo $this->createUrl('/Questionnaire/SearchData'); ?>">  -->
                                    <div class="card-header">
                                        <div class="row">

                                            <div class="col-md-3 mb-3">
                                                <div class="form-group">
                                                    <label for="">ปีเพาะปลูก<font color="red">*</font></label>
                                                    <select class="form-control digits" id="search_year" name="search[year]" >
                                                        <option selected disabled>เลือกปีเพาะปลูก</option>
                                                        <?php
                                                        if (!empty($Survey)) {
                                                            foreach($Survey as $key => $value){
                                                                 $year = $value->year + 543; 
                                                                // echo '<option value="'.$year['year'].'">'.$year['year'].'</option>';?>
                                                                 <option value="<?= $value->id;  ?>" <?php if(isset($_GET["search"]["year"]) && $_GET["search"]["year"] == $value->id){ echo "selected"; } ?> ><?= $year;  ?>/63</option>
                                                         <?php   }
                                                       }
                                                        ?>
                                                        
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <div class="form-group">
                                                  <label for="">จังหวัด<font color="red">*</font></label>
                                                  <select class="form-control digits" id="search_Province" name="search[Province]" >
                                                    <option selected disabled>เลือกจังหวัด</option>
                                                    <?php
                                                    if (!empty($MtProvince)) {                                                    
                                                    foreach ($MtProvince as $key => $value) { ?>
                                                     <option value="<?= $value->province_code;  ?>" data-id="<?= $value->id; ?>"<?php if(isset($_GET["search"]["Province"]) && $_GET["search"]["Province"] == $value->province_code){ echo "selected"; } ?> ><?= $value->province_code;  ?> -> <?= $value->province_name_th;  ?></option>
                                                     <?php
                                                   }
                                                 }
                                                   ?>
                                                 </select>
                                               </div>
                                             </div>
                                             <div class="col-md-3 mb-3">
                                                <div class="form-group">
                                                  <label for="">อำเภอ</label>
                                                  <select class="form-control digits" id="search_District" name="search[District]">
                                                    <option selected disabled>เลือกอำเภอ</option>
                                                    <?php
                                                    if (!empty($MtDistrict)) {
                                                    foreach ($MtDistrict as $key => $value) {
                                                     ?>
                                                      <option value="<?= $value->district_code;  ?>" data-id="<?= $value->id; ?>" <?php if(isset($_GET["search"]["District"]) && $_GET["search"]["District"] == $value->district_code){ echo "selected"; } ?> ><?= $value->district_code;  ?> -> <?= $value->district_name_th;  ?></option>  
                                                     <?php
                                                   }
                                                 }
                                                   ?>
                                                 </select>
                                               </div>
                                             </div>
                                             <div class="col-md-3 mb-3">
                                                <div class="form-group">
                                                  <label for="">ตำบล</label>
                                                  <select class="form-control digits" id="search_subdistrict" name="search[subdistrict]">
                                                    <option selected  disabled>เลือกตำบล</option>
                                                    <?php
                                                    if (!empty($MtSubDistrict)) {
                                                    foreach ($MtSubDistrict as $key => $value) {
                                                     ?>
                                                      <option value="<?= $value->subdistrict_code;  ?>" data-id="<?= $value->id; ?>"<?php if(isset($_GET["search"]["subdistrict"]) && $_GET["search"]["subdistrict"] == $value->subdistrict_code){ echo "selected"; } ?> ><?= $value->subdistrict_code;  ?> -> <?= $value->subdistrict_name_th;  ?></option>  
                                                     <?php
                                                      }
                                                   }
                                                   ?>
                                                 </select>
                                               </div>
                                             </div>
                                             <div class="col-md-3 mb-3">
                                                <div class="form-group">
                                                  <label for="">หมู่ที่</label>
                                                  <select class="form-control digits" id="search_village" name="search[village]">
                                                    <option selected disabled>เลือกหมู่ที่</option>
                                                    <?php
                                                    if (!empty($masterVillage)) {
                                                    foreach ($masterVillage as $key => $value) {
                                                    ?>
                                                     <option value="<?= $value->village_code;  ?>" data-id="<?= $value->village_id; ?>"<?php if(isset($_GET["search"]["village"]) && $_GET["search"]["village"] == $value->village_code){ echo "selected"; } ?> ><?= $value->village_code;  ?> -> <?= $value->village_name;  ?></option>  
                                                     <?php
                                                    }
                                                   }
                                                   ?>
                                                 </select>
                                               </div>
                                             </div>

                                            <div class="col-md-3 mb-3">
                                                <button class="btn btn-lg btn-search text-white" type="submit" id="search-data"><i class="icon-search"></i>&nbsp;ค้นหา</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="table-responsive" >
                                            <table class="table table-striped table-main" id="myTable" >
                                                <!-- <table class="table table-striped table-main" id="basic-2"> -->
                                                <thead>
                                                    <tr>
                                                        <th width="10%">ลำดับ</th>
                                                        <th>ปีเพาะปลูก</th>
                                                        <th width="50%">รหัสจังหวัด</th>
                                                        <th width="50%">จังหวัด</th>
                                                        <th width="50%">รหัสอำเภอ</th>
                                                        <th width="50%">อำเภอ</th>
                                                        <th width="50%">รหัสตำบล</th>
                                                        <th width="50%">ตำบล</th>
                                                        <th width="50%">รหัสหมู่ที่</th>
                                                        <th width="50%">หมู่ที่</th>
                                                        <th width="50%">พวกที่</th>
                                                        <th width="50%">ตัวอย่างที่</th>
                                                        <th width="40%">เพิ่มหมวด</th>
                                                        <th width="50%">ข้อมูลการเพิ่มหมวด</th>
                                                        <th width="50%">แผนที่</th>
                                                        <th width="25%">จัดการข้อมูล</th>
                                                    </tr>
                                                </thead>
                                                <tbody >
                                                    <?php
                                                    $i = 1;
                                                    foreach($data as $row){
                                           
                                                        $year = $row['year'] + 543;
                                                        $survey_time = explode("-", $row['survey_time']);
                                                        $survey_0 = $survey_time[0] + 543;
                                                        $survey_time = $survey_time[2]."-".$survey_time[1]."-".$survey_0;

                                                        // $District = MtDistrict::model()->find(array('condition' => 'province_id =\''.$row['id'].'\' AND district_code =\''. $row["district"] .'\' AND active=1'));

                                                        // $SubDistrict = MtSubDistrict::model()->find(array('condition' => 'district_id =\''.$District->id.'\' AND subdistrict_code =\''. $row["subdistrict"] .'\' AND active=1'));

                                                        // $Village = masterVillage::model()->find(array('condition' => 'subdistrict_id =\''.$SubDistrict->id.'\' AND village_code =\''. $row["village"] .'\' AND active=1'));
                                                    
                                                        $Those = MasterThose::model()->find(array('condition' => 'those_id =\''.$row["group"].'\' AND active=1'));

                                                        // $VillageName = !empty($Village) ? $Village->village_name : null ;
                                                        // $VillageCode = !empty($Village) ? $Village->village_code : null ;
                                                        // $ThoseName = !empty($Those) ? $Those->those_name : null ;

                                                        $questionnaire_id = $row['questionnaire_id'];
                                                        $questionnaire_format = $questionnaire_id.'.jpg';
                                                        $file_path = $_SERVER['DOCUMENT_ROOT'].Yii::app()->baseurl.'/uploads/'.$questionnaire_format;

                                                        $fileMap = "";
                                                        if (file_exists($file_path)) {
                                                          // $fileMap = '<a target="_blank" href="'.Yii::app()->baseurl.'/uploads/'.$questionnaire_format.'">แผนที่</a>';
                                                          $fileMap = '<a type="button" class="btn btn-icon text-primary" href="'.Yii::app()->baseurl.'/questionnaire/'.$row['questionnaire_id'].'/loadmap">แผนที่</a>';
                                                       
                                                        }else{
                                                           $fileMap = 'ไม่พบไฟล์';
                                                        }

                                                        echo '<tr>
                                                            <td>'.$i++.'</td>
                                                            <td>'.$year.'/63</td>
                                                            <td>'.$row['province_code'].'</td>
                                                            <td>'.$row['province_name_th'].'</td>
                                                            <td>'.$row['district_code'].'</td>
                                                            <td>'.$row['district_name_th'].'</td>
                                                            <td>'.$row['subdistrict_code'].'</td>
                                                            <td>'.$row['subdistrict_name_th'].'</td>
                                                            <td>'.$row['village_code'].'</td>
                                                            <td>'.$row['village_name'].'</td>
                                                            <td>'.(!empty($Those) ? $Those->those_name : null ).'</td>
                                                            <td>'.$row['sample'].'</td> ';
                                                        echo '<td>
                                                                <div class="container">
                                                                    <div class="row">
                                                                        <div class="col-lg-12">
                                                                            <div class="btn-group">
                                                                                <button type="button" class="btn dropbtn dropdown-toggle" data-toggle="dropdown">เพิ่มหมวด<span class="caret"></span></button>
                                                                                <ul class="dropdown-menu scrollable-menu" role="menu">
                                                                                    <li><a href="'.Yii::app()->baseurl.'/survey/1/create?Questionnaire='.$row['questionnaire_id'].'&return=true">หมวด 1</a></li>
                                                                                    <li><a href="'.Yii::app()->baseurl.'/survey/2/create?Questionnaire='.$row['questionnaire_id'].'&return=true">หมวด 2.1</a></li>
                                                                                    <li><a href="'.Yii::app()->baseurl.'/survey/3/create?Questionnaire='.$row['questionnaire_id'].'&return=true">หมวด 2.2</a></li>
                                                                                    <li><a href="'.Yii::app()->baseurl.'/survey/4/create?Questionnaire='.$row['questionnaire_id'].'&return=true">หมวด 3</a></li>
                                                                                    <li><a href="'.Yii::app()->baseurl.'/survey/5/create?Questionnaire='.$row['questionnaire_id'].'&return=true">หมวด 4</a></li>
                                                                                    <li><a href="'.Yii::app()->baseurl.'/survey/6/create?Questionnaire='.$row['questionnaire_id'].'&return=true">หมวด 5</a></li>
                                                                                    <li><a href="'.Yii::app()->baseurl.'/survey/7/create?Questionnaire='.$row['questionnaire_id'].'&return=true">หมวด 6</a></li>
                                                                                    <li><a href="'.Yii::app()->baseurl.'/survey/8/create?Questionnaire='.$row['questionnaire_id'].'&return=true">หมวด 7</a></li>
                                                                                    <li><a href="'.Yii::app()->baseurl.'/survey/9/create?Questionnaire='.$row['questionnaire_id'].'&return=true">หมวด 8</a></li>
                                                                                    <li><a href="'.Yii::app()->baseurl.'/survey/10/create?Questionnaire='.$row['questionnaire_id'].'&return=true">หมวด 9</a></li>
                                                                                    <li><a href="'.Yii::app()->baseurl.'/survey/11/create?Questionnaire='.$row['questionnaire_id'].'&return=true">หมวด 10</a></li>
                                                                                    <li><a target="_blank" href="'.Yii::app()->baseurl.'/survey/12/rate/create?Questionnaire='.$row['questionnaire_id'].'">หมวด 11</a></li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        <td><a type="button" class="btn dropbtn btn-icon-toggle" href="'.Yii::app()->baseurl.'/questionnaire/'.$row['questionnaire_id'].'/show">ดูข้อมูล</a></td>
                                                        <td><a type="button" class="btn btn-icon text-primary" href="'.Yii::app()->baseurl.'/questionnaire/'.$row['questionnaire_id'].'/loadmap">แผนที่</a>
                                                         </td>
                                                            <td>
                                                            <a type="button" class="btn btn-icon text-primary" href="'.Yii::app()->baseurl.'/questionnaire/'.$row['questionnaire_id'].'/show"><i data-feather="eye"></i></a>
                                                            <a type="button" class="btn btn-icon text-success" href="'.Yii::app()->baseurl.'/questionnaire/'.$row['questionnaire_id'].'/edit"><i data-feather="edit"></i></a>
                                                            <button type="button" class="btn btn-icon text-danger" onclick="sendDelete('.$row['questionnaire_id'].', \''.$row['staff_name'].' '.$row['staff_lname'].' เมื่อวันที่ '.$row['survey_time'].'\')"><i data-feather="trash"></i></button>
                                                            </td>
                                                            
                                                        </tr>';
                                                
                                                    }
                                                    ?>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="table-responsive2" style="display: none;"></div>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                    <!-- end row -->
                    <div class="container position-fixed fixed-top pt-5" id="deleteForm" style="display:none;">
        <div class="card col-12 p-0">
            <div class="card-header">
                <h5>ลบข้อมูล</h5>
            </div>
            <div class="card-body">
                <h5 class="card-title">เมื่อข้อมูลถูกลบจะไม่สามารถย้อนคืนได้</h5>
                <div class="card-text">
                    <form method="POST" action="<?php echo Yii::app()->baseurl; ?>/questionnaire/delete">
                        <label for="deleteKey">รหัสอ้างอิง</label>
                        <input id="deleteKey" name="key" class="form-control" value="" readonly/>
                        <label for="deleteValue">ข้อมูล</label>
                        <input id="deleteValue" name="value" class="form-control" value="" readonly/>
                        <button class="btn btn-danger btn-lg mt-2">ลบข้อมูล</button>
                        <button type="button" class="btn btn-light btn-lg mt-2" onclick='$("#deleteForm").hide();'>ยกเลิก</button>
                    </form>
                </div>
            </div>
        </div>

    </div>
                </div>

<script type="text/javascript">
$(document).ready(function(){
    $('#myTable').dataTable({
      responsive: true,
        "pageLength": 10,
        "searching": false,
      "info": false,
      "lengthChange": false

    });

});

  $("#search-data").click(function(){

    var year = $("#search_year").val();
    var province = $("#search_Province").val();
    var district = $("#search_District").val();
    var subdistrict = $("#search_subdistrict").val();
    var village = $("#search_village").val();
    // if (typeof  year != 'undefined' || typeof  province != 'undefined' || typeof  district != 'undefined' || typeof  subdistrict != 'undefined' ||typeof  village != 'undefined') {
   if (year != null && province != null) {
      $.ajax({
                type: 'GET',
                url: "<?= $this->createUrl('/Questionnaire/SearchData'); ?>", 
                data: {
                    year: year,
                    province: province,
                    district: district,
                    subdistrict: subdistrict,
                    village: village,
                },
                success: function(data) {
        
                  $(".table-responsive").remove();
                  $(".table-responsive2").show();
                  $(".table-responsive2").html(data);
                }
            });
    }else{
      alert("กรุณาเลือกปีเพาะปลูกและเลือกจังหวัด");
    }
  });

    $("#search_Province").change(function() {
        var id = $("#search_Province option:selected").attr('data-id');

        if (id != "") {
            $.ajax({
                type: 'POST',
                url: "<?= $this->createUrl('/Questionnaire/SearchDistrict'); ?>", 
                data: {
                    id: id,
                },
                success: function(data) {

                $('#search_District').empty();
                $('#search_subdistrict').empty();
                $('#search_village').empty();
                $('#search_District').append(data);

                }
            });
        }
    });
    $("#search_District").change(function() {
        var id = $("#search_District option:selected").attr('data-id');
        if (id != "") {
            $.ajax({
                type: 'POST',
                url: "<?= $this->createUrl('/Questionnaire/SearchSubDistrict'); ?>", 
                data: {
                    id: id,
                },
                success: function(data) {

                $('#search_subdistrict').empty();
                $('#search_village').empty();
                $('#search_subdistrict').append(data);

                }
            });
        }
    });

    $("#search_subdistrict").change(function() {
        var id = $("#search_subdistrict option:selected").attr('data-id');
        if (id != "") {
            $.ajax({
                type: 'POST',
                url: "<?= $this->createUrl('/Questionnaire/SearchVillage'); ?>", 
                data: {
                    id: id,
                },
                success: function(data) {
                $('#search_village').empty();
                $('#search_village').append(data);

                }
            });
        }
    });

    $("#search_Province").select2( {
     placeholder: "เลือกจังหวัด",
     allowClear: true
     });

    $("#search_District").select2( {
     placeholder: "เลือกอำเภอ",
     allowClear: true
     });

    $("#search_subdistrict").select2( {
     placeholder: "เลือกตำบล",
     allowClear: true
     });

    $("#search_village").select2( {
     placeholder: "เลือกหมู่ที่",
     allowClear: true
     });

//    $(function() {
//   $('.table-responsive').on('shown.bs.dropdown', function(e) {
//     var t = $(this),
//       m = $(e.target).find('.dropdown-menu'),
//       tb = t.offset().top + t.height(),
//       mb = m.offset().top + m.outerHeight(true),
//       d = 20; // Space for shadow + scrollbar.   
//     if (t[0].scrollWidth > t.innerWidth()) {
//       if (mb + d > tb) {
//         t.css('padding-bottom', ((mb + d) - tb));
//       }
//     } else {
//       t.css('overflow', 'visible');
//     }
//   }).on('hidden.bs.dropdown', function() {
//     $(this).css({
//       'padding-bottom': '',
//       'overflow': ''
//     });
//   });
// });
</script>
 
<style type="text/css">
  .table-responsive2 {
  z-index: 999;
  overflow-y: auto !important;
}
  .dropbtn {
    background-color: #8DBF42;
    color: white;
    padding: 16px;
    font-size: 16px;
    border: none;
  }

  .dropdown {
    position: relative;
    display: inline-block;
  }

  .dropdown-content {
    display: none;
    position: absolute;
    background-color: #f1f1f1;
    min-width: 160px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
  }

  .dropdown-content a {
    color: black;
    padding: 12px 16px;
    text-decoration: none;
    display: block;
  }

  .dropdown-content a:hover {background-color: #ddd;}

  .dropdown:hover .dropdown-content {display: block;}

  .dropdown:hover .dropbtn {background-color: #3e8e41;}
</style>
