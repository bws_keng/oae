<style type="text/css">
    /*div .checkbox {
    display: inline-block;
}*/
/*div .first-box {
    display: inline-block;
}*/
input[type='radio'] {
  clear: both;
  float: left;
  width: 20px;
  height: 10px;
  opacity: 0;
}
.radio {
  border: 1px solid #b3b4b4;
  border-radius: 50%;
  box-sizing: border-box;
  cursor: pointer;
  display: block;
  height: 16px;
  margin: 0 10px 10px -20px;
  padding: 10px;
  position: relative;
  width: 16px;
}
input[type='radio']:checked + .radio {
  background-color: #218996;
  border-color: #218996;
}
input[type='radio']:active + .radio,
input[type='radio']:focus + .radio {
  border-color: lightblue;
}
input[type='radio']:checked + .radio::before {
  content: "✓ ";
  position: absolute;
  color: white;
  left: 50%;
  top: 50%;
  transform: translate(-50%, -50%);
}
label .radio-check{
  float: left;
  font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 14px;
  font-weight: bold;
  line-height: 20px;
}
</style>
<form class="container-fluid" method="POST" action="<?php echo Yii::app()->baseurl.'/admin/survey/'.$_GET['id'].'/rate/insert'; ?>">
    <!-- start row -->
    <div class="row">
        <div class="col-lg-12 set-height set-padding">
            <div id="section1">
                <div class="card">
                    <div class="card-header">
                        <h5 class="mb-1">เพิ่มคำถาม</h5>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <div>
                            <input type="hidden" name="question_group" value="<?php echo $_GET['id']; ?>">
                   <!--          <div class="row">
                                <div class="col-md-6 mb-3">
                                    <div class="form-group">
                                        <label for="">ปี พ.ศ. แบบสอบถาม</label>
                                        <select class="form-control digits " id="" name="survey" required>
                                            <option selected disabled>เลือกปี พ.ศ. แบบสอบถาม</option>
                                            <?php
                                            $survey = QuestionnaireModel::surveyList();
                                        foreach($survey as $key => $row){
                                            echo '<option value="'.$row['id'].'">'.$row['year'].' - '.$row['name'].'</option>';
                                        }
                                        ?>
                                        </select>
                                    </div>
                                </div>
                            </div> -->
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <div class="form-group">
                                        <label for="">คำอธิบาย</label>
                                        <textarea class="form-control" rows="4" cols="50" name="description"><?php echo $title['description']; ?></textarea>
                                        <button type="button" class="btn col-1" onclick="$(`textarea[name='description']`).val(() => { return $(`textarea[name='description']`).val() + '<b></b>'})"><B>B</B></button>
                                        <button type="button" class="btn col-1" onclick="$(`textarea[name='description']`).val(() => { return $(`textarea[name='description']`).val() + '<i></i>'})"><i>i</i></button>
                                        <button type="button" class="btn col-1" onclick="$(`textarea[name='description']`).val(() => { return $(`textarea[name='description']`).val() + '<red></red>'})"><red>A</red></button>
                                        <button type="button" class="btn col-1" onclick="$(`textarea[name='description']`).val(() => { return $(`textarea[name='description']`).val() + '<u></u>'})"><u>U</u></button>
                                    </div>
                                </div>
                            </div>
                  
                         <!--        <button type="button" class="btn btn-primary">เพิ่มกลุ่มคำถาม</button> -->
                  
                            <div class="row">
                                <div class="col-md-8 mb-3">
                                    <label>ชื่อกลุ่มคำถาม</label>
                                    <div class="row col-12">
                                        <input class="form-control col-9" type="text" name="question" required>
                                        <button type="button" class="btn col-1" onclick="$(`input[name='question']`).val(() => { return $(`input[name='question']`).val() + '<b></b>'})"><B>B</B></button>
                                        <button type="button" class="btn col-1" onclick="$(`input[name='question']`).val(() => { return $(`input[name='question']`).val() + '<i></i>'})"><i>i</i></button>
                                        <button type="button" class="btn col-1" onclick="$(`input[name='question']`).val(() => { return $(`input[name='question']`).val() + '<red></red>'})"><red>A</red></button>
                                        <button type="button" class="btn col-1" onclick="$(`input[name='question']`).val(() => { return $(`input[name='question']`).val() + '<u></u>'})"><u>U</u></button>
                                    </div>
                                </div>
                       <!--   <div class="col-md-4 mb-3">
                                    <label>เลขที่คำถาม</label>
                                    <input class="form-control" type="number" name="question_number">
                                </div> -->
                            </div>
                            <div class="row">
                                    <div class="col-md-8 mb-3 first-box">
                            <button type="button" class="btn btn-success add-question">เพิ่มคำถาม</button>
                                </div>
                            </div>
                            <style type="text/css">
                                  .first-box {
                                    display: flex;
                                    align-items: flex-end;
                                    justify-content: space-between;
                                  }

                                  .first-box .input-container {
                                    flex-grow: 2;
                                  }

                                  .first-box button {
                                    margin-left: 10px;
                                  }
                            </style>
                            <div class="data-sub">
                                <div class="row">
                                    <div class="col-md-8 mb-3 first-box">
                                        <div class="input-container">
                                        <label>เพิ่มคำถาม</label>
                                        <input class="form-control col-10" type="text" name="question_detail-1" required>
                                        </div>
                                        <!-- <button type="button" class="btn btn-danger delete-group" >ลบ</button> -->
                                    </div>
                                    <div class="col-md-4 mb-3">
                                        <label>ระดับคะแนน</label>
                                        <div class="row col-12">
                                            <input id="rang-51" type="radio" name="range-1" tabindex='1' value="5" />
                                            <label for="rang-51" class='radio'></label>
                                            <label for="rang-51" class="radio-check">5 คะแนน</label>
                                            <input id="rang-31" type="radio" name="range-1" tabindex='1' value="3" />
                                            <label for="rang-31" class='radio'></label>
                                            <label for="rang-31" class="radio-check">3 คะแนน</label>
                                            <input id="rang-21" type="radio" name="range-1" tabindex='1' value="2" />
                                            <label for="rang-21" class='radio'></label>
                                            <label for="rang-21" class="radio-check">2 คะแนน</label>
                                        </div>
                                    </div> 
                                </div>
                            </div>
                            <div class="data-sub2"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row text-center group-submit">
                <input type="submit" class="btn btn-primary btn-lg" value="บันทึกข้อมูล" />
                <button type="button" class="btn btn-outline-secondary btn-lg" onclick="window.history.go(-1); return false;">ยกเลิก</button>
            </div>
        </div>
    </div>
    <!-- end row -->
</form>
<script type="text/javascript">
    var max_fields = 10;
    var wrapper = $(".data-sub2");
    var numItems = 10;
    var x = 1;

    $(".add-question").click(function(e) {
        e.preventDefault();
        if (x < max_fields) {
            x++;
            numItems++;
            $(wrapper).append('<div class="row sub-question-'+x+'">'
                                    +'<div class="col-md-8 mb-3 first-box">'
                                        +'<div class="input-container">'
                                        +'<label>เพิ่มคำถาม</label>'
                                        +'<input class="form-control col-11" type="text" name="question_detail-'+x+'" required>'
                                        +'</div>'
                                        +'<button type="button" class="btn btn-danger delete-group" onclick=deleteGroup('+x+')>ลบ</button>'
                                    +'</div>'
                                    +'<div class="col-md-4 mb-3 level">'
                                        +'<label>ระดับคะแนน</label>'
                                        +'<div class="row col-12">'
                                            +'<input id="rang-5'+x+'" type="radio" name="range-'+x+'" tabindex="1" value="5" />'
                                            +'<label for="rang-5'+x+'" class="radio"></label>'
                                            +'<label for="rang-5'+x+'" class="radio-check">5 คะแนน</label>'
                                            +'<input id="rang-3'+x+'" type="radio" name="range-'+x+'" tabindex="1" value="3" />'
                                            +'<label for="rang-3'+x+'" class="radio"></label>'
                                            +'<label for="rang-3'+x+'" class="radio-check">3 คะแนน</label>'
                                            +'<input id="rang-2'+x+'" type="radio" name="range-'+x+'" tabindex="1" value="2" />'
                                            +'<label for="rang-2'+x+'" class="radio"></label>'
                                            +'<label for="rang-2'+x+'" class="radio-check">2 คะแนน</label>'
                                        +'</div>'
                                    +'</div>'
                                +'</div>'); 
        } 
    });

    function deleteGroup(id)
    {
      $('div.row.sub-question-'+id).remove();
    }
    
  
</script>