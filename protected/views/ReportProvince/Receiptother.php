<?php
$array_formtext =[
    1 =>"รายงานการใช้ที่ดิน",
    2 =>"รายงานข้อมูลพื้นฐานครัวเรือน -> รายงานข้อมูลพื้นฐานครัวเรือน และหัวหน้าครัวเรือนเกษตร",
    3 =>"รายงานข้อมูลพื้นฐานครัวเรือน -> รายงานข้อมูลพื้นฐานของประชากรเกษตร",
    4 =>"รายงานข้อมูลพื้นฐานครัวเรือน -> รายงานข้อมูลพื้นฐานของแรงงาน",
    5 =>"รายงานทรัพย์สิน",
    6 =>"รายงานทัศนคติฯ",
    7 =>"รายงานประกอบวิเคราะห์น้ำท่วม -> รายงานสรุปรายได้ – รายจ่าย และตัวชี้วัดเศรษฐกิจครัวเรือน",
    8 =>"รายงานประกอบวิเคราะห์น้ำท่วม -> รายงานที่มารายได้",
    9 =>"รายงานประกอบวิเคราะห์น้ำท่วม -> รายงานที่มารายจ่าย",
    10 =>"รายงานรายจ่าย -> รายงานองค์ประกอบรายจ่ายเงินสดเกษตรทางพืช",
    11 =>"รายงานรายจ่าย -> รายงานองค์ประกอบรายจ่ายเงินสดเกษตรทางสัตว์",
    12 =>"รายงานรายจ่าย -> รายงานองค์ประกอบรายจ่ายเงินสดเกษตร และบริโภคอุปโภค",
    13 =>"รายงานรายได้ -> รายงานองค์ประกอบรายได้เงินสดเกษตรทางพืช",
    14 =>"รายงานรายได้ -> รายงานองค์ประกอบรายจ่ายเงินสดเกษตรทางสัตว์",
    15 =>"รายงานรายได้ -> รายงานองค์ประกอบรายได้เงินอื่น",
    16 =>"รายงานรายได้ – รายจ่าย",
    17 =>"รายงานหนี้สิน",
];

if (isset($_GET["date"])) {

    $criteria = new CDbCriteria();
    // $criteria->compare('active',1);
    $criteria->order = "those_id ASC";
    $Those = Those::model()->findAll($criteria);
    $array_thoseByVillage = array();
    foreach ($Those as $key => $value) {
        $array_thoseByVillage[$value->those_id] = array();
    }

    foreach ($array_thoseByVillage as $keyThose => $valueThose) {
        $criteria = new CDbCriteria();
        $criteria->compare('t.group',$keyThose);
        $criteria->compare('active',1);
        $Processing = Processing::model()->findAll($criteria);
        if (!empty($Processing)) {
            foreach ($Processing as $keyProcessing => $valueProcessing) {
                $array_thoseByVillage[$keyThose][$valueProcessing->province_table->province_code] = array();

                $criteria = new CDbCriteria();
                $criteria->compare('processing_id',$valueProcessing->id);
                $criteria->compare('active',1);
                $SubProcessing = SubProcessing::model()->findAll($criteria);
                $array_thoseByVillage[$keyThose][$valueProcessing->province_table->province_code] = array();
                foreach ($SubProcessing as $keySubProcessing => $valueSubProcessing) {
                    if (!isset($array_thoseByVillage[$keyThose][$valueProcessing->province_table->province_code][$valueSubProcessing->district_table->district_code])) {
                        $array_thoseByVillage[$keyThose][$valueProcessing->province_table->province_code][$valueSubProcessing->district_table->district_code] = array();
                    }
                    if (!isset($array_thoseByVillage[$keyThose][$valueProcessing->province_table->province_code][$valueSubProcessing->district_table->district_code][$valueSubProcessing->subdistrict_table->subdistrict_code])) {
                        $array_thoseByVillage[$keyThose][$valueProcessing->province_table->province_code][$valueSubProcessing->district_table->district_code][$valueSubProcessing->subdistrict_table->subdistrict_code] = array();
                    }
                    if (!isset($array_thoseByVillage[$keyThose][$valueProcessing->province_table->province_code][$valueSubProcessing->district_table->district_code][$valueSubProcessing->subdistrict_table->subdistrict_code][$valueSubProcessing->village_table->village_code])) {
                        $array_thoseByVillage[$keyThose][$valueProcessing->province_table->province_code][$valueSubProcessing->district_table->district_code][$valueSubProcessing->subdistrict_table->subdistrict_code][$valueSubProcessing->village_table->village_code] = array();
                    }
                    $array_thoseByVillage[$keyThose][$valueProcessing->province_table->province_code][$valueSubProcessing->district_table->district_code][$valueSubProcessing->subdistrict_table->subdistrict_code][$valueSubProcessing->village_table->village_code]["N"] = $valueProcessing->total;
                    $array_thoseByVillage[$keyThose][$valueProcessing->province_table->province_code][$valueSubProcessing->district_table->district_code][$valueSubProcessing->subdistrict_table->subdistrict_code][$valueSubProcessing->village_table->village_code]["n"] = $valueProcessing->survey;
                    $array_thoseByVillage[$keyThose][$valueProcessing->province_table->province_code][$valueSubProcessing->district_table->district_code][$valueSubProcessing->subdistrict_table->subdistrict_code][$valueSubProcessing->village_table->village_code]["M"] = $valueSubProcessing->total;
                    $array_thoseByVillage[$keyThose][$valueProcessing->province_table->province_code][$valueSubProcessing->district_table->district_code][$valueSubProcessing->subdistrict_table->subdistrict_code][$valueSubProcessing->village_table->village_code]["m"] = $valueSubProcessing->survey;
                    
                }
            }
        }
    }

       // หาค่าคูณขยายระดับจังหวัด START
    $array_processingByProvince = array();
    $array_checkProvinceThose = array();
    foreach ($array_thoseByVillage as $keythose => $valuethose) {
        foreach ($valuethose as $keyvaluethose => $valuevaluethose) {
            foreach ($valuevaluethose as $keyvaluevaluethose => $valuevaluevaluethose) {
                foreach ($valuevaluevaluethose as $keyvaluevaluevaluethose => $valuevaluevaluevaluethose) {
                    foreach ($valuevaluevaluevaluethose as $keyvaluevaluevaluevaluethose => $valuevaluevaluevaluevaluethose) {
                        if (!isset($array_processingByProvince[$keyvaluethose])) {
                            $array_processingByProvince[$keyvaluethose] = array();
                        }
                        if (!isset($array_checkProvinceThose[$keythose][$keyvaluethose])) {
                            $array_checkProvinceThose[$keythose] = array();
                            $array_checkProvinceThose[$keythose][$keyvaluethose] = array();
                            if (!isset($array_processingByProvince[$keyvaluethose]["N"])) {
                                $array_processingByProvince[$keyvaluethose]["N"] = $valuevaluevaluevaluevaluethose["N"];
                            }else{
                                $array_processingByProvince[$keyvaluethose]["N"] += $valuevaluevaluevaluevaluethose["N"];
                            }
                            if (!isset($array_processingByProvince[$keyvaluethose]["n"])) {
                                $array_processingByProvince[$keyvaluethose]["n"] = $valuevaluevaluevaluevaluethose["n"];
                            }else{
                                $array_processingByProvince[$keyvaluethose]["n"] += $valuevaluevaluevaluevaluethose["n"];
                            }
                        }

                        if (!isset($array_processingByProvince[$keyvaluethose]["M"])) {
                            $array_processingByProvince[$keyvaluethose]["M"] = $valuevaluevaluevaluevaluethose["M"];
                        }else{
                            $array_processingByProvince[$keyvaluethose]["M"] += $valuevaluevaluevaluevaluethose["M"];
                        }
                        if (!isset($array_processingByProvince[$keyvaluethose]["m"])) {
                            $array_processingByProvince[$keyvaluethose]["m"] = $valuevaluevaluevaluevaluethose["m"];
                        }else{
                            $array_processingByProvince[$keyvaluethose]["m"] += $valuevaluevaluevaluevaluethose["m"];
                        }

                    }
                }
            }
        }
    }
    // หาค่าคูณขยายระดับจังหวัด END
//     echo("<pre>");
// var_dump($array_processingByProvince);exit();
//เริ่มคำนวณค่าคูณขยายระดับหมู่บ้าน
  $criteria = new CDbCriteria();
$criteria->compare('question_group_id',12);
$criteria->order = "sortorder ASC";
$Question = Question::model()->findAll($criteria);
foreach ($Question as $key => $value) {
    $Choice[$value->question_id] = $value->question;

    $criteria=new CDbCriteria;
    $criteria->compare('question_id',$value->question_id);
    $criteria->order = "sortorder ASC";
    $QuestionSub = QuestionSub::model()->findAll($criteria);
    $Array_ans[$value->question_id] = array();
    foreach ($QuestionSub as $keyQuestionSub => $valueQuestionSub) {
        $Array_ans[$value->question_id][$valueQuestionSub->id] = $valueQuestionSub->question_detail;
    }
}

$criteria = new CDbCriteria;
$criteria->addInCondition('sector_id',$_GET["sector"]);
$MasterSectorData = MasterSector::model()->findAll($criteria);
$array_id_county = array();
foreach ($MasterSectorData as $keyMasterSectorData => $valueMasterSectorData) {
    $MasterCountyData = MasterCounty::model()->findAll(array(
        'condition' => 'sector_id=:sector_id',
        'params' => array(':sector_id' => $valueMasterSectorData->sector_id)
    ));
    foreach ($MasterCountyData as $keyMasterCountyData => $valueMasterCountyData) {
        $array_id_county[] = $valueMasterCountyData->county_id;
    }
}
$criteria = new CDbCriteria;
$criteria->addInCondition('county_id',$array_id_county);
$criteria->order = "province_code ASC";
$ProvinceData = MtProvince::model()->findAll($criteria);
$Province_code_array = array();
foreach ($ProvinceData as $key => $value) {
    $Province_code_array[] = $value->province_code;
}
$criteria = new CDbCriteria;
$criteria->addInCondition('province',$Province_code_array);
$criteria->order = "questionnaire_id ASC";
$QuestionnaireData = Questionnaire::model()->findAll($criteria);

foreach ($QuestionnaireData as $keyQuestionnaireData => $valueQuestionnaireData) {
    foreach ($Choice as $keyChoice => $valueChoice) {
        foreach ($Array_ans[$keyChoice] as $keyArray_ans => $valueArray_ans) {
            $criteria = new CDbCriteria;
            $criteria->compare('questionnaire_id',$valueQuestionnaireData->questionnaire_id);
            $criteria->compare('question_sub_id',$keyArray_ans);
            // $criteria->order = "sortorder ASC";
            $SurveySubValueData = SurveySubValue::model()->findAll($criteria);
            $SurveyvalueDataCheckTotal=0;
                        foreach ($SurveySubValueData as $keySurveyvalueDataCheck => $valueSurveyvalueDataCheck) {
                            $SurveyvalueDataCheckTotal +=$valueSurveyvalueDataCheck->value;
                        }
                        if(!isset($array_province[$valueQuestionnaireData->province][$keyChoice][$keyArray_ans])){
                            if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                $array_province[$valueQuestionnaireData->province][$keyChoice][$keyArray_ans] = $SurveyvalueDataCheckTotal*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]);
                            }
                        }else{
                            if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                $array_province[$valueQuestionnaireData->province][$keyChoice][$keyArray_ans] = $array_province[$valueQuestionnaireData->province][$keyChoice][$keyArray_ans] + ($SurveyvalueDataCheckTotal*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]));
                            }
                        }
        }
    }
}
} 
?>

<div class="container-fluid">
    <div id="section">
        <div class="card">
            <div class="card-header">
                <h5 class="mb-1"><?=$array_formtext[$id]?><span class="pull-right"></span></h5>
            </div>
        </div>

        <div class="card table-card">
            <div class="card-header">
                <form action="<?php echo Yii::app()->baseUrl ."/ReportProvince/Receiptother/".$id.""?>" method="GET">
                <div class="row">
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="">ปี พ.ศ.</label>
                            <?php
                                 $Survey = Survey::model()->findAll();
                            ?>
                            <select name="date" class="form-control digits" id="">
                                <?php
                                    foreach ($Survey as $key => $value) {
                                ?>
                                <option value="<?=$value->id?> "<?=isset($_GET["date"]) == $value->id ? "selected" : ""; ?>><?=$value->year+543?>/63</option>
                                <?php
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                            <div class="form-group">
                                <label for="region">เลือกภูมิภาคของประเทศไทย</label>
                                <?php $MasterSectorDataCheck = MasterSector::model()->findAll(); ?>
                                <?php $sector = isset($_GET["sector"]) ? $_GET["sector"][0] : null; ?>
                                <select class="form-control" id="regionselect" name="sector[]">
                                    <?php foreach ($MasterSectorDataCheck as $key => $value) { ?>
                                        <option <?=$sector == $value->sector_id ? "selected" : ""; ?> value="<?=$value->sector_id?>"><?=$value->sector_name?></option>
                                    <?php } ?>
                                </select>
                                <script>
                                    $(function() {
                                        $('#regionselect').multiSelect();
                                    });
                                </script>
                            </div>
                        </div>
                    <div class="col-md-2">
                        <button class="btn btn-lg btn-search text-white" type="submit"><i class="icon-search"></i>&nbsp;ค้นหา</button>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group m-t-20 text-right">
                            <?php if (isset($_GET["date"])) { ?>

                                     <a href="<?= $this->createUrl('/ReportProvince/ReceiptotherExcel',array('date' => $_GET["date"],'sector'=>$_GET["sector"])); ?>" class="btn btn-lg btn-excel text-white pull-right " type="submit"><i class="fa fa-file-excel-o"></i>&nbsp;&nbsp;Export Excel</a>
                                     <a href="<?= $this->createUrl('/ReportProvince/ReceiptotherExcelAll',array('id' => $id,'date' => $_GET["date"])); ?>" class="btn btn-lg btn-export text-white pull-right " type="submit"><i class="fa fa-download"></i>&nbsp;&nbsp;Export ภาพรวมทุกจังหวัด</a>

                            <?php } ?>
                            
                            <!-- <button class="btn btn-lg btn-excel text-white pull-right " type="submit"><i class="fa fa-file-excel-o"></i>&nbsp;&nbsp;Export Excel</button> -->
                        </div>
                    </div>
                </div>
            </form>

            </div>
            <div class="card-body ">
                <div class="table-responsive">
                    <table class="table table-striped table-main" id="">
                        <thead>
                           <tr class="report-th">
                            <th width="50%">รายการ</th>
                            <?php $Master_province_array = array(); ?>
                            <?php

                            if (isset($_GET["sector"])) {
                                $criteria = new CDbCriteria;
                                $criteria->addInCondition('sector_id',$_GET["sector"]);
                                $MasterSectorData = MasterSector::model()->findAll($criteria);
                            }else{
                                $MasterSectorData = array();
                            }
                            foreach ($MasterSectorData as $keyMasterSectorData => $valueMasterSectorData) {
                                ?> 
                                <th><?=$valueMasterSectorData->sector_name?></th>
                                <?php
                                $MasterCountyData = MasterCounty::model()->findAll(array(
                                    'condition' => 'sector_id=:sector_id',
                                    'params' => array(':sector_id' => $valueMasterSectorData->sector_id)
                                ));
                                $array_id_county = array();
                                foreach ($MasterCountyData as $keyMasterCountyData => $valueMasterCountyData) {
                                    $array_id_county[] = $valueMasterCountyData->county_id;
                                }
                                $criteria = new CDbCriteria;
                                $criteria->compare('active', 1);
                                $criteria->addInCondition('county_id',$array_id_county);
                                $criteria->order = 'province_code ASC';
                                $MtProvinceDataSort = MtProvince::model()->findAll($criteria);
                                foreach ($MtProvinceDataSort as $keyMtProvinceDataSort => $valueMtProvinceDataSort) {
                                    $Master_province_array[$valueMasterSectorData->sector_id][] = $valueMtProvinceDataSort->id;
                                    ?>
                                    <th class="itempro<?=$keyMtProvinceDataSort+1?>"  style="display: none"><?=$valueMtProvinceDataSort->province_name_th?></th>
                                    <?php 
                                }
                            }

                            ?>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        if (!empty($Choice)) {
                            foreach ($Choice as $keyChoice => $valueChoice) { ?>
                                <tr class="report-title">

                                    <td colspan="6"><?=$valueChoice?></td>

                                    <?php 
                                   // foreach ($Array_ans[$keyChoice] as $keyArray_ans => $valueArray_ans) { ?>
                                      <!-- <tr class="report-ans">   -->
                                       <!-- <td class="text-left">"<?=$valueArray_ans?>"</td> -->

                                       <?php foreach ($Master_province_array as $keyMaster_province_array => $valueMaster_province_array) {
                                        $total_result = 0;
                                        foreach ($valueMaster_province_array as $keytotal_result => $valuetotal_result) {
                                            $MtProvinceData = MtProvince::model()->find(array(
                                                'condition' => 'id=:id',
                                                'params' => array(':id' => $valuetotal_result)
                                            ));
                                            if (isset($array_province[$MtProvinceData->province_code][$keyChoice])) {
                                                foreach ($array_province[$MtProvinceData->province_code][$keyChoice] as $key => $value) {

                                                    $total_result+=($value*$array_processingByProvince[$MtProvinceData->province_code]["n"]/$array_processingByProvince[$MtProvinceData->province_code]["N"]/$array_processingByProvince[$MtProvinceData->province_code]["M"]); ?>
                                                <?php   }
                                            }
                                        } ?>
                                    <?php } 
                                      // var_dump($total_result);
                                    ?>

                               

                                    <?php foreach ($Master_province_array as $keyMaster_province_array => $valueMaster_province_array) {
                                        $total_result = 0;
                                        foreach ($valueMaster_province_array as $keytotal_result => $valuetotal_result) {
                                            $MtProvinceData = MtProvince::model()->find(array(
                                                'condition' => 'id=:id',
                                                'params' => array(':id' => $valuetotal_result)
                                            ));
                                            if (isset($array_province[$MtProvinceData->province_code][$keyChoice])) {
                                                foreach ($array_province[$MtProvinceData->province_code][$keyChoice] as $key => $value) {

                                                    $total_result+=($value*$array_processingByProvince[$MtProvinceData->province_code]["n"]/$array_processingByProvince[$MtProvinceData->province_code]["N"]/$array_processingByProvince[$MtProvinceData->province_code]["M"]); ?>
                                                <?php   }
                                            }
                                            ?>
                                        <?php } } ?>
                                  <!--   </tr> -->
                                <?php// } ?> 
                            </tr>
                            <?php  if (count($Array_ans[$keyChoice]) > 1) {
                                foreach ($Array_ans[$keyChoice] as $keyArray_ans => $valueArray_ans) { ?>
                                    <tr class="report-ans">
                                        <td class="text-left"><?=$valueArray_ans?></td>
                                        <?php foreach ($Master_province_array as $keyMaster_province_array => $valueMaster_province_array) {
                                            $total_result_detail = 0;
                                            foreach ($valueMaster_province_array as $keytotal_result => $valuetotal_result) {
                                                $MtProvinceData = MtProvince::model()->find(array(
                                                    'condition' => 'id=:id',
                                                    'params' => array(':id' => $valuetotal_result)
                                                ));
                                                if (isset($array_province[$MtProvinceData->province_code][$keyChoice][$keyArray_ans])) {
                                                  
                                                        $total_result_detail+=($array_province[$MtProvinceData->province_code][$keyChoice][$keyArray_ans]*$array_processingByProvince[$MtProvinceData->province_code]["n"]/$array_processingByProvince[$MtProvinceData->province_code]["N"]/$array_processingByProvince[$MtProvinceData->province_code]["M"]); ?>
                                                    <?php  
                                                }
                                            } 

                                            ?><td><?=sprintf("%.2f",$total_result_detail/count($MtProvinceDataSort))?></td> 
                                        <?php } 
                                            foreach ($Master_province_array as $keyMaster_province_array => $valueMaster_province_array) {
                                            // $total_result_detail = 0;
                                            foreach ($valueMaster_province_array as $keytotal_result => $valuetotal_result) {
                                                $MtProvinceData = MtProvince::model()->find(array(
                                                    'condition' => 'id=:id',
                                                    'params' => array(':id' => $valuetotal_result)
                                                ));
                                                if (isset($array_province[$MtProvinceData->province_code][$keyChoice][$keyArray_ans])) { 
                                                    ?>
                                                    
                                                      <td class="itempro<?=$keytotal_result+1?>" style="display: none"><?=sprintf("%.2f",$array_province[$MtProvinceData->province_code][$keyChoice][$keyArray_ans]*$array_processingByProvince[$MtProvinceData->province_code]["n"]/$array_processingByProvince[$MtProvinceData->province_code]["N"]/$array_processingByProvince[$MtProvinceData->province_code]["M"])?></td> 
                                               <?php }else{ ?>
                                                    <td class="itempro<?=$keytotal_result+1?>" style="display: none">0</td> 
                                              <?php } 
                                            }

                                        }
                                        ?>
                                    </tr>
                                <?php }
                            }
                        }
                    }    
                    ?>
                </tbody>
        </table>
    </div>
</div>
        </div>
        <?php if (isset($_GET["sector"])) { ?>
            <div class="pull-right">
                <nav aria-label="..." class="">
                    <!-- <span class="sr-only">(current)</span> -->
                    <ul class="pagination pagination-primary">
                        <!-- disabled -->
                        <li class="page-item itempre disabled"><a onclick="myPrevious()" class="page-link" href="javascript:void(0)" tabindex="-1">ย้อนกลับ</a></li>
                        <?php 
                        $count = ceil(($keyMtProvinceDataSort+1) / 4);
                        ?>
                        <input type="hidden" id="proall" value="<?=$keyMtProvinceDataSort+1?>">
                        <input type="hidden" id="countpro" value="<?=$count?>">
                        <input type="hidden" id="logcount" value="1">

                        <?php for ($i=1; $i <= $count ; $i++) {  ?>
                            <li class="<?= $i == 1 ? 'active' : '' ?> page-item item<?=$i?>" ><a class="page-link link<?=$i?>" href="javascript:void(0)" onclick="myFunction(<?=$i?>)"><?= $i ?></a></li>
                        <?php } ?>
                        <li class="page-item itemnext"><a  onclick="myNext()" class="page-link" href="javascript:void(0)">ถัดไป</a></li>
                    </ul>
                </nav>
            </div>
        <?php } ?>
        <!--  card close -->

    </div>
</div>
<script type="text/javascript">

  for (f = 1; f <= 4; f++) {
   $(".itempro"+f).show();
}

function myFunction(id) {
    var le = document.getElementById("countpro").value;
    var firstpro = document.getElementById("proall").value;

    for (i = 1; i <= le; i++) {
        if(i != id){
            $(".item"+i).removeClass("active");
        }else{
            $(".item"+i).addClass("active");
        }
    }

    for (y = 1; y <= firstpro; y++) {
        if(y > (4*(id-1)) && y <= (4*id)){
            $(".itempro"+y).show();
        }else{
            $(".itempro"+y).hide();
        }
    }
    document.getElementById("logcount").value = id;   
    if(id == le){
        $(".itemnext").addClass("disabled");
    }else{
        $(".itemnext").removeClass("disabled");
    }

    if(id == 1){
        $(".itempre").addClass("disabled");
    }else{
        $(".itempre").removeClass("disabled");
    }
}

function myPrevious() {
   var log = document.getElementById("logcount").value;
   if(log > 1){
       var logsum = parseInt(log) - 1;
       $( ".link"+logsum ).click(); 
   }
}

function myNext() {
   var log = document.getElementById("logcount").value;
   var logsum = parseInt(log) + 1;
   $( ".link"+logsum ).click(); 
}


</script>