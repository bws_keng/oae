
<style type="text/css">
.ui-datepicker select.ui-datepicker-month, .ui-datepicker select.ui-datepicker-year{
    color: black;
}
input.form-control{
    height: 40px;
    font-size: 1.0rem !important;
}
.wizard-header{margin-bottom: 2em;}
.form-control{height: 40px;}
label{font-weight: bold;}
.card{padding: 1em;background-color: rgba(255, 255, 255, 0.5);}
.wizard-card .picture{width: 200px;height: 200px;border-radius: 0;}
.wizard-card.ct-wizard-orange .picture:hover {
    border-color: #26A69A;
}
.errorMessage{
    color:red;
}
</style>
<?php  
date_default_timezone_set("Asia/Bangkok");
$this->breadcrumbs=array(
  // UserModule::t('Users')=>array('/user'),
  UserModule::t('Manage'),
);
$formNameModel = 'User';
$title = "จัดการเจ้าหน้าที่สำนักงานเขต";
?>
    <div class="container-fluid">
        <!-- start row -->
        <?php //unset(Yii::app()->session['rule']); else: ?>
        <div class="row">
            <div class="col-lg-12 set-height set-padding">
                <div id="section1">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="mb-1">
                                <?php echo $title; ?>
                            </h5>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">

                           
                                <?php $form = $this->beginWidget('UActiveForm', array(
                                'id'=>'registration-form',
                                'clientOptions'=>array(
                                        'validateOnSubmit'=>true,
                                ),
                                // 'enableAjaxValidation'=>true,
                                // 'disableAjaxValidationAttributes'=>array('RegistrationForm_verifyCode'),
                                'htmlOptions'=>array('enctype'=>'multipart/form-data'),
                                )); ?>
                                    <!-- <?php echo $form->errorSummary(array($model, $profile)); ?> -->
                                <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6 mb-3">
                                         <label><?php echo $form->labelEx($profile, 'county_id'); ?></label>
                                        <?php echo $form->dropDownList($profile, 'county_id', MasterCounty::getMasterCountyList(), array('empty' => '---เลือกเขต---','class' => 'form-control','placeholder' => 'เขต')); ?>
                                        <?php echo $form->error($profile, 'county_id'); ?>
                                    </div>
                                <!-- </div>
                                <div class="row"> -->
                                    <div class="col-md-6 mb-3">
                                         <label><?php echo $form->labelEx($profile, 'staff_id'); ?></label>
                                        <?php echo $form->textField($profile, 'staff_id', array('readonly'=>'true','maxlength'=>4,'minlength'=>4,'class' => 'form-control','placeholder' => 'รหัสพนักงาน')); ?>
                                        <?php echo $form->error($profile, 'staff_id'); ?>
                                    </div>
                                </div> 
                                </div>  
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6 mb-3">
                                        <label><?php echo $form->labelEx($profile, 'title_id'); ?></label>
                                        <?php echo $form->dropDownList($profile, 'title_id', ProfilesTitle::getTitleList(), array('empty' => '---เลือกคำนำหน้าชื่อ---', 'class' => 'form-control')); ?>
                                        <?php echo $form->error($profile, 'title_id'); ?>
                                        </div>
                                        <div class="col-md-6 mb-3">
                                            <label><?php echo $form->labelEx($model, 'email'); ?></label>
                                            <?php echo $form->textField($model, 'email', array('class' => 'form-control', 'placeholder' => 'Email', 'autocomplete'=>'chrome-off')); ?>
                                            <?php echo $form->error($model, 'email'); ?>
                                        </div>
                                    </div>
                                </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label><?php echo $form->labelEx($profile, 'firstname'); ?></label>
                                                <?php echo $form->textField($profile, 'firstname', array('class' => 'form-control', 'placeholder' => 'ชื่อจริง', 'autocomplete'=>'chrome-off')); ?>
                                                <?php echo $form->error($profile, 'firstname'); ?>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label><?php echo $form->labelEx($profile, 'lastname'); ?></label>
                                                <?php echo $form->textField($profile, 'lastname', array('class' => 'form-control', 'placeholder' => 'นามสกุล', 'autocomplete'=>'chrome-off')); ?>
                                                <?php echo $form->error($profile, 'lastname'); ?>
                                            </div>
                                        </div>
                                </div> 
                                
                                <!-- <div class="row">
                                <div class="col-md-6 mb-3">
                                        <label><?php echo $form->labelEx($model, 'username'); ?></label>
                                        <?php echo $form->textField($model, 'username', array('class' => 'form-control', 'placeholder' => 'ชื่อผู้ใช้')); ?>
                                        <?php echo $form->error($model, 'username'); ?>
                                    </div>
                                </div>   -->  
                                <div class="row">
                                    <?php if($model->isNewRecord) { ?>
                                 <!--        <div class="col-md-6">
                                        <div class="form-group">
                                                <label><?php echo $form->labelEx($model, 'username'); ?></label>
                                                <?php echo $form->textField($model, 'username', array('class' => 'form-control', 'placeholder' => 'ชื่อผู้ใช้')); ?>
                                                <?php echo $form->error($model, 'username'); ?>
                                            </div>
                                        </div>  
                                        <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label><?php echo $form->labelEx($model, 'password'); ?></label>
                                                        <?php echo $form->passwordField($model, 'password', array('class' => 'form-control', 'placeholder' => 'รหัสผ่าน (ควรเป็น (A-z0-9) และมากกว่า 4 ตัวอักษร)', 'autocomplete'=>'chrome-off')); ?>
                                                        <?php echo $form->error($model, 'password'); ?>
                                                    </div>
                                                </div> -->
                                                <!-- <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label><?php echo $form->labelEx($model, 'verifyPassword'); ?></label>
                                                        <?php echo $form->passwordField($model, 'verifyPassword', array('class' => 'form-control', 'placeholder' => 'ยืนยันรหัสผ่าน', 'autocomplete'=>'chrome-off')); ?>
                                                        <?php echo $form->error($model, 'verifyPassword'); ?>
                                                    </div>
                                                </div> -->
                                      
                                    <?php } else { ?>
                                  <!--     <div class="col-md-6">
                                        <div class="form-group">
                                                <label><?php echo $form->labelEx($model, 'username'); ?></label>
                                                <?php echo $form->textField($model, 'username', array('class' => 'form-control', 'placeholder' => 'ชื่อผู้ใช้')); ?>
                                                <?php echo $form->error($model, 'username'); ?>
                                            </div>
                                        </div> 
                                        <div class="col-md-6 mb-3">
                                            <div class="form-group">
                                                <label><?php echo $form->labelEx($model, 'newpassword'); ?></label>
                                                <?php echo $form->passwordField($model, 'newpassword', array('class' => 'form-control', 'placeholder' => 'รหัสผ่าน (ควรเป็น (A-z0-9) และมากกว่า 4 ตัวอักษร)')); ?>
                                                <?php echo $form->error($model, 'newpassword'); ?>
                                            </div>
                                        </div>

                                        <div class="col-md-6 mb-3">
                                            <div class="form-group">
                                                <label><?php echo $form->labelEx($model, 'confirmpass'); ?></label>
                                                <?php echo $form->passwordField($model, 'confirmpass', array('class' => 'form-control', 'placeholder' => 'ยืนยันรหัสผ่าน')); ?>
                                                <?php echo $form->error($model, 'confirmpass'); ?>
                                            </div>
                                        </div> --> 
                                    <?php } ?>
                                    </div>
                       
                             <?php // endif; ?>
                        </div>
                    </div>
                </div>
                <div class="row text-center group-submit">
                <!--     <input type="submit" type="button" class="btn btn-primary btn-lg" value="บันทึกข้อมูล" />
                    <button type="button" class="btn btn-outline-secondary btn-lg">ยกเลิก</button> -->
                     <?php echo CHtml::submitButton($model->isNewRecord ? 'บันทึกข้อมูล' : 'บันทึกข้อมูล', array('class' => 'btn btn-primary btn-lg',)); ?>
                     <button type="button" class="btn btn-outline-secondary btn-lg" onclick="window.history.go(-1); return false;">ยกเลิก</button>
                </div>
            </div>
        </div>
        <!-- end row -->
 <?php $this->endWidget(); ?>
    </div>
<script type="text/javascript">
    $('input').removeClass('error');
    $('label').removeClass('error');
    $("#User_email").change(function() {
    var text_mail = $("#User_email").val();
    if (text_mail != "") {
        $.ajax({
            type: 'POST',
            url: "<?= Yii::app()->createUrl('user/admin/CheckMail'); ?>",
            data: {
                text_mail: text_mail
            },
            success: function(data) {
                
                if(data){
                var obj = JSON.parse(data);
                    if (obj['success'] === true) {
                        alert(obj['message']);
                        $("#User_email").empty();
                        $("#User_email").val(null);
                    }
                }
         }
     });
    }
});

     $("#Profile_county_id").change(function() {
        var id = $(this).val();
         $.ajax({
            type: 'POST',
            url: "<?= Yii::app()->createUrl('Authorities/RunStaff'); ?>",
            data: {
                id: id
            },
            success: function(data) {
                $("#Profile_staff_id").empty();
                $("#Profile_staff_id").val(data);
                // console.log(data);   
         }
     });
        // if (id != null) {
        //     if (id < 10) {
        //         for (var i = 0; i < id.length; i++) {
        //             $("#Profile_staff_id").val(10+id);
        //         }
        //     }else{
        //         for (var i = 0; i < id.length; i++) {
        //             $("#Profile_staff_id").val(id+0+1);
        //         }
        //     }
        // }
     });

</script>