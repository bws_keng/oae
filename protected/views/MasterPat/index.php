<?php
	if(isset($_GET["MasterPat"])){
		$criteria=new CDbCriteria;
		if($_GET["MasterPat"]['pat_name']){
			$criteria->compare('active', 1);
			$criteria->compare('CONCAT(pat_name)',$_GET["MasterPat"]['pat_name'],true);
		} 
		else if($_GET["MasterPat"]['pat_name'] == ""){
			$criteria->compare('active', 1);
		}
		$criteria->order = 'pat_name ASC';
		$data = MasterPat::model()->findAll($criteria);
	}
?>

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
 
<div class="container-fluid">
<!-- start row -->
<div class="row">
	<div class="col-lg-12 set-height set-padding">
		<div id="section1">
			<div class="card">
				<div class="card-header">
					<h5 class="mb-1">จัดการข้อมูลประเภทพืช-สัตว์</h5>
				</div>
			</div>

			<div class="row justify-content-end">
				<div class="col-md-3">
					<a href="<?php echo Yii::app()->baseurl; ?>/MasterPat/create">
						<div class="card o-hidden create-main">
							<div class="b-r-4 card-body">
								<div class="media static-top-widget">
									<div class="align-self-center text-center"><i data-feather="plus"></i></div>
									<div class="media-body">
										<h6 class="mb-0 counter">เพิ่มข้อมูล</h6><i class="icon-bg" data-feather="database"></i>
									</div>
								</div>
							</div>
						</div>
					</a>
				</div>
			</div>

			<div class="card table-card">
				<div class="card-header">
				<?php $form=$this->beginWidget('AActiveForm', array(
					'action'=>Yii::app()->createUrl($this->route),
					'method'=>'get'
				)); ?>
					<div class="row">

						<div class="col-md-3 mb-3">
							<div class="form-group">
								<label for="">ค้นหาประเภทพืช-สัตว์</label>
								<?php $pat_name = (isset($_GET["MasterPat"]["pat_name"]) ? $_GET["MasterPat"]["pat_name"] : "")?>
								<input class="form-control" id="searchProvince" type="text" name="MasterPat[pat_name]" value="<?=$pat_name?>" placeholder="ชื่อประเภทพืช-สัตว์">
							</div>
						</div>
						<div class="col-md-3 mb-3">
							<button style="margin-top: 32px" class="btn btn-lg btn-search text-white" type="submit"><i class="icon-search"></i>&nbsp;ค้นหา</button>
						</div>
					</div>
				<?php $this->endWidget(); ?>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-striped table-main" id="">
							<!-- <table class="table table-striped table-main" id="basic-2"> -->
							<thead>
								<tr>
									<th width="10%">ลำดับ</th>
									<th width="45%">ชื่อประเภทพืช-สัตว์</th>
									<th width="20%">จัดการข้อมูล</th>
								</tr>
							</thead>
							<tbody>
							<?php
							
							$dataProvider=new CArrayDataProvider($data, array(
                                'pagination'=>array(
                                'pageSize'=>25
                                ),
                        	));
							$getPages = isset($_GET['page']) ? $_GET['page'] : 1;
							if($getPages != 0 ){ 
                                $getPages = $getPages -1;
                            }
                            $start_cnt = $dataProvider->pagination->pageSize * $getPages; // 25 * จำนวนหน้า
							
							// ----------------- query ------------------
							if (!empty($data)){
								foreach($dataProvider->getData() as $key => $row){ 								
										echo '<tr>
											<td>'.($start_cnt+1).'</td>
											<td>'.$row->pat_name.'</td>
											<td>
												<a type="button" class="btn btn-icon text-primary" href="'.Yii::app()->baseurl.'/MasterPat/view/'.$row->pat_id.'"><i data-feather="eye"></i></a>
												<a type="button" class="btn btn-icon text-success" href="'.Yii::app()->baseurl.'/MasterPat/edit/'.$row->pat_id.'"><i data-feather="edit"></i></a>
												<a type="button" class="btn btn-icon text-danger" href="javascript:vold(0)" onclick="confirmDelete('.$row->pat_id.')"><i data-feather="trash"></i></a>
											</td>
										</tr>';
										$start_cnt++;
								 }
								
							}else{
								echo '<tr><td colspan="3">ไม่พบข้อมูล</td></tr>';
							} ?>
							</tbody>
							<!-- ----------------- query ------------------  -->
						</table>
						<?php if(!empty($data)){ ?>
						<div class="pull-right">
							<?php
								$this->widget('CLinkPager',array(
									'pages'=>$dataProvider->pagination
									)
								);
							?>
							</div>
						<?php } ?>
					</div>
				</div>

			</div>

		</div>

	</div>
</div>
<!-- end row -->
<div class="container position-fixed fixed-top pt-5" id="deleteForm" style="display:none;">
	<div class="card col-12 p-0">
		<div class="card-header">
			<h5>ลบข้อมูล</h5>
		</div>
<div class="card-body">
	<h5 class="card-title">เมื่อข้อมูลถูกลบจะไม่สามารถย้อนคืนได้</h5>
		<div class="card-text">
			<form method="POST" action="<?php echo Yii::app()->baseurl; ?>/questionnaire/delete">
				<label for="deleteKey">รหัสอ้างอิง</label>
				<input id="deleteKey" name="key" class="form-control" value="" readonly/>
				<label for="deleteValue">ข้อมูล</label>
				<input id="deleteValue" name="value" class="form-control" value="" readonly/>
				<button class="btn btn-danger btn-lg mt-2">ลบข้อมูล</button>
				<button type="button" class="btn btn-light btn-lg mt-2" onclick='$("#deleteForm").hide();'>ยกเลิก</button>
			</form>
		</div>
	</div>
</div>

</div>
</div>

<script type="text/javascript">
	function confirmDelete(id)
	{
		swal({
			title: "ต้องการลบหรือไม่",
			icon: "warning",
			buttons: true,
			dangerMode: true,
		})
		.then((willDelete) => {
			if (willDelete) {
				$.ajax({
					url: "<?php echo $this->createUrl('/MasterPat/delete/'); ?>",
					type: "GET",
					data: {id: id},
					dataType: "html",
					success: function () {
						swal({
							title: "ลบข้อมูลสำเร็จ", 
							icon: "success", 
						});
						location.reload(true);
						window.location.href = "/oae/index.php/MasterPat/index";
					}
				});
			} else {
				swal({
					title: "ยกเลิกการลบข้อมูล",
  					icon: "error",
				});
			}
		});
	}
</script>


