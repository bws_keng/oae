<?php

class AjaxController extends Controller
{
	public function actionIndex()
	{
		$this->render('index');
	}

	public function actioncheckoffice()
	{
		$county_code = $_POST["county_code"];

		$county_data = MasterCounty::model()->find(array(
			'condition' => 'county_code=:county_code',
			'params' => array(':county_code' => $county_code)
		));
		$array_data = array();
		if (!empty($county_data)) {
			$array_data['id']= $county_data->county_id;
			$array_data['name']= $county_data->county_name;
			$array_data['error'] = "have-data";
			echo json_encode($array_data);
		}else{
			$array_data['error'] = "no-data";
				echo json_encode($array_data);
		}
	}

	public function actionCheckprovince()
	{
		$province_code = $_POST["province_code"];
		$county_code = $_POST["county_code"];

		// $code = "";
		// if ($county_code < 10) {
		// 	for ($i=0; $i <= $county_code ; $i++) { 
		// 		$code = '0'.$county_code;
		// 	}
		// }else{
		// 	$code = $county_code;
		// }

		$county_data = MasterCounty::model()->find(array(
			'condition' => 'county_id=:county_code',
			'params' => array(':county_code' => $county_code)
		));

		$array_data = array();
		if (!empty($county_data)) {
			$province_data = MtProvince::model()->find(array(
				'condition' => 'province_code=:province_code AND county_id=:county_id',
				'params' => array(':province_code' => $province_code,':county_id' => $county_data->county_id)
			));
			if (!empty($province_data)) {
				$array_data['id']= $province_data->id;
				$array_data['name']= $province_data->province_name_th;
				$array_data['error'] = "have-data";
				echo json_encode($array_data);
			}else{
				$array_data['error'] = "no-data";
				echo json_encode($array_data);
			}
		}else{
			$array_data['error'] = "no-data";
			echo json_encode($array_data);
		}
		

	}

	public function actionCheckdistrict()
	{
		$district_code = $_POST["district_code"];
		$province_id = $_POST["province_id"];
		$district_data = MtDistrict::model()->find(array(
			'condition' => 'district_code=:district_code AND province_id=:province_id',
			'params' => array(':district_code' => $district_code,':province_id' => $province_id)
		));
		$array_data = array();
		if (!empty($district_data)) {
			$array_data['id']= $district_data->id;
			$array_data['name']= $district_data->district_name_th;
			$array_data['error'] = "have-data";
			echo json_encode($array_data);
		}else{
			$array_data['error'] = "no-data";
			echo json_encode($array_data);
		}

	}

	public function actionCheckSubDistrict()
	{
		$subdistrict_code = $_POST["subdistrict_code"];
		$district_id = $_POST["district_id"];
		$SubDistrict_data = MtSubDistrict::model()->find(array(
			'condition' => 'subdistrict_code=:subdistrict_code AND district_id=:district_id',
			'params' => array(':subdistrict_code' => $subdistrict_code,':district_id' => $district_id)
		));
		$array_data = array();
		if (!empty($SubDistrict_data)) {
			$array_data['id']= $SubDistrict_data->id;
			$array_data['name']= $SubDistrict_data->subdistrict_name_th;
			$array_data['error'] = "have-data";
			echo json_encode($array_data);
		}else{
			$array_data['error'] = "no-data";
			echo json_encode($array_data);
		}

	}

	public function actionCheckVillage()
	{
		$village_code = $_POST["village_code"];
		$subdistrict_id = $_POST["subdistrict_id"];
		$Village_data = masterVillage::model()->find(array(
			'condition' => 'village_code=:village_code AND subdistrict_id=:subdistrict_id',
			'params' => array(':village_code' => $village_code,':subdistrict_id' => $subdistrict_id)
		));
		$array_data = array();
		if (!empty($Village_data)) {
			$array_data['id']= $Village_data->village_id;
			$array_data['name']= $Village_data->village_name;
			$array_data['error'] = "have-data";
			echo json_encode($array_data);
		}else{
			$array_data['error'] = "no-data";
			echo json_encode($array_data);
		}
	}

	public function actionCheckpuak()
	{
		$puak_code = $_POST["puak_code"];
		$puak_data = MasterThose::model()->find(array(
			'condition' => 'those_code=:those_code',
			'params' => array(':those_code' => "0".$puak_code)
		));
		$array_data = array();
		if (!empty($puak_data)) {
			$array_data['id']= $puak_data->those_id;
			$array_data['name']= $puak_data->those_name;
			$array_data['error'] = "have-data";
			echo json_encode($array_data);
		}else{
			$array_data['error'] = "no-data";
			echo json_encode($array_data);
		}

	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}