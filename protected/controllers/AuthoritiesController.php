
<?php 
class AuthoritiesController extends Controller
{
	 // public function filters()
  //   {
  //       return array(
  //           'accessControl', // perform access control for CRUD operations
  //       );
  //   }

  //   /**
  //    * Specifies the access control rules.
  //    * This method is used by the 'accessControl' filter.
  //    * @return array access control rules
  //    */
    // public function accessRules()
    // {
    //     return array(
    //         array('allow',  // allow all users to perform 'index' and 'view' actions
    //             'actions' => array('index', 'view'),
    //             'users' => array('*'),
    //         ),
    //         array('allow',
    //             // กำหนดสิทธิ์เข้าใช้งาน actionIndex
    //             'actions' => AccessControl::check_action(),
    //             // ได้เฉพาะ group 1 เท่านั่น
    //             'expression' => 'AccessControl::check_access()',
    //         ),
    //         array('deny',  // deny all users
    //             'users' => array('*'),
    //         ),
    //     );
    // }

	 public function init()
    {
      if(!Yii::app()->user->id ){
        $this->redirect(Yii::app()->baseurl.'/user/login?admin=1');
      }else{
        $user = User::model()->findByPk(Yii::app()->user->id);
        if($user->superuser != 1){
        $this->redirect(Yii::app()->baseurl);

        }
      }
    }
    
	// public function init()
	// {
	// 	// parent::init();
	// 	// $this->lastactivity();
	// 		if(Yii::app()->user->id == null){
	// 			$this->redirect(array('site/index'));
	// 		}
		
	// }
	//ublic $defaultAction = 'admin';
	public $layout='admin';

	private $_model;

	// public function filters() {
	// 		return array(
	// 				'rights',
	// 		);
	// }

	
	private function encrypting($string="") {
		$hash = Yii::app()->getModule('user')->hash;
		if ($hash=="md5")
			return md5($string);
		if ($hash=="sha1")
			return sha1($string);
		else
			return hash($hash,$string);
	}

   private function RandomPassword(){

            $number="abcdefghijklmnopqrstuvwxyz0123456789";
            $i = '';
            $result = '';
        for($i==1;$i<6;$i++){ // จำนวนหลักที่ต้องการสามารถเปลี่ยนได้ตามใจชอบนะครับ จาก 5 เป็น 3 หรือ 6 หรือ 10 เป็นต้น
            $random=rand(0,strlen($number)-1); //สุ่มตัวเลข
            $cut_txt=substr($number,$random,1); //ตัดตัวเลข หรือ ตัวอักษรจากตำแหน่งที่สุ่มได้มา 1 ตัว
            $result.=substr($number,$random,1); // เก็บค่าที่ตัดมาแล้วใส่ตัวแปร
            $number=str_replace($cut_txt,'',$number); // ลบ หรือ แทนที่ตัวอักษร หรือ ตัวเลขนั้นด้วยค่า ว่าง
        }

        return $result;

    }
    public function actionIndex()
    {
    	// var_dump("expression");exit();
    	$model=new UsersAdmin('search');
        $model->unsetAttributes();  // clear any default values
        $model->superuser = 0;
        if(isset($_GET['UsersAdmin'])){

        	$model->name_search=$_GET['UsersAdmin']['name_search'];

        }
        $this->render('index',array(
        	'model'=>$model,
        ));
    }

   
	public function actionIdCard($id)
	{
		// var_dump($id);exit();
		$model= User::model()->findbyPk($id);
		$regis = new RegistrationForm;
		$regis->id = $model->id;
		$profile= $model->profile;

		if(isset($_POST['User']))
		{
			$uploadFile = CUploadedFile::getInstance($model,'pic_user');
			if(isset($uploadFile))
			{
				$uglyName = strtolower($uploadFile->name);
				$mediocreName = preg_replace('/[^a-zA-Z0-9]+/', '_', $uglyName);
				$beautifulName = trim($mediocreName, '_') . "." . $uploadFile->extensionName;
				$model->pic_cardid = $beautifulName;
				$model->save(false);
				Yii::app()->user->setFlash('registration','แก้ไขสำเร็จ');
				if(isset($uploadFile))
				{
					/////////// SAVE IMAGE //////////
					Yush::init($regis);
					$originalPath = Yush::getPath($regis, Yush::SIZE_ORIGINAL, $model->pic_cardid);
					$thumbPath = Yush::getPath($regis, Yush::SIZE_THUMB, $model->pic_cardid);
					$smallPath = Yush::getPath($regis, Yush::SIZE_SMALL, $model->pic_cardid);
					// Save the original resource to disk
					$uploadFile->saveAs($originalPath);

					// Create a small image
					$smallImage = Yii::app()->phpThumb->create($originalPath);
					$smallImage->resize(385, 220);
					$smallImage->save($smallPath);

					// Create a thumbnail
					$thumbImage = Yii::app()->phpThumb->create($originalPath);
					$thumbImage->resize(350, 200);
					$thumbImage->save($thumbPath);

				}
			}
			// var_dump($model->pic_cardid);exit();
		}
		$this->render('id_card',array(
			'model'=>$model,
			'profile'=>$profile
		));
		/*$dataProvider=new CActiveDataProvider('User', array(
			'pagination'=>array(
				'pageSize'=>Yii::app()->controller->module->user_page_size,
			),
		));

		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));//*/
	}

	// private function RandomPassword(){
	// 	$number="ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	// 	$i = '';
	// 	$result = '';
	// 	for($i==1;$i<8;$i++){ // จำนวนหลักที่ต้องการสามารถเปลี่ยนได้ตามใจชอบนะครับ จาก 5 เป็น 3 หรือ 6 หรือ 10 เป็นต้น
	// 		$random=rand(0,strlen($number)-1); //สุ่มตัวเลข
	// 		$cut_txt=substr($number,$random,1); //ตัดตัวเลข หรือ ตัวอักษรจากตำแหน่งที่สุ่มได้มา 1 ตัว
	// 		$result.=substr($number,$random,1); // เก็บค่าที่ตัดมาแล้วใส่ตัวแปร
	// 		$number=str_replace($cut_txt,'',$number); // ลบ หรือ แทนที่ตัวอักษร หรือ ตัวเลขนั้นด้วยค่า ว่าง
	// 	}
	// 	return $result;
	// }


	public function actionExcelOld()
	{
		$HisImportArr = array();
		$HisImportErrorArr = array();
		$HisImportAttrErrorArr = array();
		$HisImportErrorMessageArr = array();
		$HisImportUserPassArr = array();
		//Student
		if (isset($_FILES['excel_import_student'])) {
			$extensionFile = pathinfo($_FILES['excel_import_student']['name'], PATHINFO_EXTENSION);
			$fullPath = Yii::app()->basePath.'/../../uploads/temp_excel.'.$extensionFile;
			if (!move_uploaded_file($_FILES["excel_import_student"]["tmp_name"],$fullPath)) {
				echo "Move File Error!!!";
				exit;
			}
			$file_path = $fullPath;
			$sheet_array = Yii::app()->yexcel->readActiveSheet($file_path);
			$HisImportArr = $sheet_array;
			
			foreach ($sheet_array as $key => $valueRow) {
				if ($key == 1) { // Header first row
				}else { // Data Row ALL 2 -
					// $passwordGen = $this->RandomPassword();
					// $HisImportUserPassArr[$key]['password'] = $passwordGen;

					$modelUser = new User;
					$modelUser->email = $valueRow['A'];
					$modelUser->username = $valueRow['A'];
					$modelUser->password = md5($valueRow['B']); // Random password
					$modelUser->verifyPassword = $modelUser->password;
					// $modelUser->department_id = $valueRow['G'];
					// $modelUser->company_id = $valueRow['F'];
					// $modelUser->division_id = $valueRow['G'];
					// $modelUser->department_id = $valueRow['H'];
					// $modelUser->position_id = $valueRow['I'];
					// $modelUser->orgchart_lv2 = $valueRow['J'];
					$modelUser->type_register = 2;
					$modelUser->superuser = 0;
					// $modelUser->auditor_id = $valueRow['G'];
					// $modelUser->bookkeeper_id = $valueRow['H'];

					$member = Helpers::lib()->ldapTms($modelUser->email);
					if($member['count'] > 0){ //TMS
						$modelUser->type_register = 3;
						Helpers::lib()->_insertLdap($member);
						$modelStation = Station::model()->findByAttributes(array('station_title'=>$member[0]['st'][0]));
						$modelDepartment = Department::model()->findByAttributes(array('dep_title'=>$member[0]['department'][0]));
						$modelDivision = Division::model()->findByAttributes(array('div_title'=>$member[0]['division'][0]));

						$modelUser->division_id = $modelDivision->id;
						$modelUser->station_id = $modelStation->station_id;
						$modelUser->department_id = $modelDepartment->id;
						$modelUser->password = md5($model->email);
						$modelUser->verifyPassword = $model->password;
						$modelUser->status = 1; //bypass not confirm
					}else{ //LMS
						$modelUser->password = md5($valueRow['B']); // Random password
						$modelUser->verifyPassword = $modelUser->password;
						// $modelUser->department_id = 2;
						$modelUser->department_id = 1;
						$modelUser->status = 0;
					}
					
					if ($modelUser->validate()) {
						// insert right
						$modelUser->save();
						$modelProfile = new Profile;
						$modelProfile->user_id = $modelUser->id;
						$modelProfile->title_id = $valueRow['C'];
						$modelProfile->firstname = $valueRow['D'];
						$modelProfile->lastname = $valueRow['E'];
						$modelProfile->identification = $valueRow['B'];
						$modelProfile->phone = $valueRow['F'];
					
						if($modelProfile->validate()){
							$modelProfile->save();
							$Insert_success[$key] = "สร้างชื่อผู้ใช้เรียบร้อย";
						} else {
							$HisImportErrorArr[] = $HisImportArr[$key];
							$msgAllArr = array();
							$attrAllArr = array();
							foreach($modelProfile->getErrors() as $field => $msgArr){
								$attrAllArr[] = $field;
								$msgAllArr[] = $msgArr[0];
							}

							$HisImportErrorMessageArr[$key] = implode(", ",$msgAllArr);
							$HisImportAttrErrorArr[] = $attrAllArr;
							$HisImportArr = $sheet_array;
							$deldata = User::model()->findbyPk($modelUser->id);
							$deldata->delete();
							$Insert_success[$key] = "สร้างชื่อผู้ใช้ไม่สำเร็จ";
						}


						/*$message = '
						<strong>สวัสดี คุณ' . $modelProfile->firstname . ' ' . $modelProfile->lastname . '</strong><br /><br />

						โปรดคลิกลิงค์ต่อไปนี้ เพื่อดำเนินการเข้าสู่ระบบ<br />
						<a href="' . str_replace("/admin","",Yii::app()->getBaseUrl(true)) . '">' . str_replace("/admin","",Yii::app()->getBaseUrl(true)) . '</a><br />
						<strong>Username</strong> : ' . $valueRow['A'] . '<br />
						<strong>Password</strong> : ' . $passwordGen . '<br /><br />

						ยินดีตอนรับเข้าสู่ระบบ Brother E-Traning<br /><br />

						ทีมงาน SET

						';
						$subject = 'ยินดีต้อนรับเข้าสู่ระบบ SET E-Training';
						$to['email'] = $valueRow['K'];
						$to['firstname'] = $valueRow['F'];
						$to['lastname'] = $valueRow['G'];

						Helpers::lib()->SendMail($to,$subject,$message);*/

						
					} else {

						/*if(!isset($orgchart->id) && $valueRow['E'] != ''){
							//$modelUser->student_board = 0;
							$modelUser->clearErrors('department_id');
							$modelUser->addError('department_id','ไม่มีแผนกนี้');
						}*/

						$HisImportErrorArr[] = $HisImportArr[$key];

						$msgAllArr = array();
						$attrAllArr = array();
						foreach($modelUser->getErrors() as $field => $msgArr){
							$attrAllArr[] = $field;
							$msgAllArr[] = $msgArr[0];
						}

						$HisImportErrorMessageArr[$key] = implode(", ",$msgAllArr);
						$HisImportAttrErrorArr[] = $attrAllArr;

//						unset($HisImportArr[$key]);
						$HisImportArr = $sheet_array;
						$Insert_success[$key] = "สร้างชื่อผู้ใช้ไม่สำเร็จ";
					}
//				}
				}
			}
			unlink($fullPath);
		}

		if(Yii::app()->user->id){
			Helpers::lib()->getControllerActionId();
		}
		$this->render('excel',array('HisImportArr'=>$HisImportArr,'HisImportUserPassArr'=>$HisImportUserPassArr,'HisImportErrorArr'=>$HisImportErrorArr,'HisImportAttrErrorArr'=>$HisImportAttrErrorArr,'HisImportErrorMessageArr'=>$HisImportErrorMessageArr,'Insert_success'=>$Insert_success));
	}

	public function actionExcel()
    {
        $model=new User('import');
        $HisImportArr = array();
		$HisImportErrorArr = array();
		$HisImportAttrErrorArr = array();
		$HisImportErrorMessageArr = array();
		$HisImportUserPassArr = array();
		$data = array();
        // if(isset($_FILES['excel_import_student']))
        $model->excel_file = CUploadedFile::getInstance($model,'excel_file');

        if(!empty($model->excel_file))
        {      
            $phpExcelPath = Yii::getPathOfAlias('ext.phpexcel.Classes');
            include($phpExcelPath . DIRECTORY_SEPARATOR . 'PHPExcel.php');

            $model->excel_file = CUploadedFile::getInstance($model,'excel_file');
			// $model->excel_file =  $_FILES['excel_import_student'];
           
            //if ($model->excel_file && $model->validate()) {
                // $webroot = YiiBase::getPathOfAlias('webroot');
                $webroot = Yii::app()->basePath."/../..";
                // $filename = $webroot.'/uploads/' . $model->excel_file->name . '.' . $model->excel_file->extensionName;
                $filename = $webroot.'/uploads/' . $model->excel_file->name;
                $model->excel_file->saveAs($filename);

				$sheet_array = Yii::app()->yexcel->readActiveSheet($filename);
                $inputFileName = $filename;
                $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
                $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                $objReader->setReadDataOnly(true);
                $objPHPExcel = $objReader->load($inputFileName);
				$objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
				$highestRow = $objWorksheet->getHighestRow();
				$highestColumn = $objWorksheet->getHighestColumn();

				$headingsArray = $objWorksheet->rangeToArray('A1:'.$highestColumn.'1',null, true, true, true);
				$headingsArray = $headingsArray[1];

				$r = -1;
				$namedDataArray = array();
				for ($row = 2; $row <= $highestRow; ++$row) {
					$dataRow = $objWorksheet->rangeToArray('A'.$row.':'.$highestColumn.$row,null, true, true, true);
					if ((isset($dataRow[$row]['A'])) && ($dataRow[$row]['A'] > '')) {
						++$r;
						foreach($headingsArray as $columnKey => $columnHeading) {
							$namedDataArray[$r][$columnHeading] = $dataRow[$row][$columnKey];
						}
					}
				}

				$index = 0;
				
				foreach($namedDataArray as $key => $result){
	
					$model = new User;
					$profile=new Profile;
					$model->email = $result[" email"];
					$model->username = $result["username(รหัสพนักงาน)"];
				    $model->identification = $result["รหัสบัตรประชาชน (13หลัก)"];

					$model->type_register = 2;
					$model->superuser = 0;
					$model->repass_status = 0;
					
					$model->create_at = date('Y-m-d H:i:s');
					$model->status = 1;
					// $model->department_id = $result["รหัสแผนก"];
					// $model->position_id = $result["ตำแหน่ง"];
					// $model->branch_id = $result["Lavel"];

					$genpass = $this->RandomPassword();
					$model->verifyPassword = $genpass;
					$model->password = UserModule::encrypting($genpass);
					$model->activkey=Yii::app()->controller->module->encrypting(microtime().$model->password);
				
					if ($model->validate()) {
						$model->save();
						//$data[$key]['msg'] = 'pass';
						
						 $modelProfile = new Profile;
						 $modelProfile->user_id = $model->id;
						 $modelProfile->type_user = 3;
						 $modelProfile->title_id = $result["คำนำหน้าชื่อ "];
						 $modelProfile->firstname = $result["ชื่อ"];
						 $modelProfile->lastname = $result["นามสกุล"];
						 $modelProfile->firstname_en = $result["ชื่อ-สกุล(EN)"];
						 $modelProfile->lastname_en = $result["นามสกุล(EN)"];
				

						if($modelProfile->validate()){
							$modelProfile->save();
							$data[$key]['msg'] = 'pass';
							
							$Insert_success[$key] = "สร้างชื่อผู้ใช้เรียบร้อย";

							$message = '
							<strong>สวัสดี คุณ' . $modelProfile->firstname . ' ' . $modelProfile->lastname . '</strong><br /><br />

							<h4>ระบบได้ทำการอนุมัติสมาชิกเข้าใช้งาน e-Learning Thoresen เรียบร้อยแล้ว โดยมี ชื่อผู้ใช้งานและรหัสผ่านดังนี้ </h4>
	    					<h4>- User : '. $model->username.'</h4>
							<h4>- Password : '.$genpass.'</h4>

							โปรดคลิกลิงค์ต่อไปนี้ เพื่อดำเนินการเข้าสู่ระบบ<br />
							<a href="' . str_replace("/admin","",Yii::app()->getBaseUrl(true)) . '">' . str_replace("/admin","",Yii::app()->getBaseUrl(true)) . '</a><br />
							<strong>Email</strong> : ' . $model->email . '<br />

							ยินดีต้อนรับเข้าสู่ระบบ e-Learning Thoresen<br /><br />

							';
							$subject = 'ยินดีต้อนรับเข้าสู่ระบบ e-Learning Thoresen';
							$to['email'] = $model->email;
							$to['firstname'] = $modelProfile->firstname;
							$to['lastname'] = $modelProfile->lastname;

							$mail = Helpers::lib()->SendMail($to,$subject,$message);
						} else {

							$HisImportErrorArr[] = $HisImportArr[$key];
							$msgAllArr = array();

							$attrAllArr = array();
							foreach($modelProfile->getErrors() as $field => $msgArr){
								$attrAllArr[] = $field;
								$msgAllArr[] = $msgArr[0];
							}

							$HisImportErrorMessageArr[$key] = implode(", ",$msgAllArr);
							$data[$key]['msg'] = implode(", ",$msgAllArr);
							$HisImportAttrErrorArr[] = $attrAllArr;
							$HisImportArr = $sheet_array;
							$deldata = User::model()->findbyPk($model->id);
							$deldata->delete();
							$Insert_success[$key] = "สร้างชื่อผู้ใช้ไม่สำเร็จ";
						}
					}else{
						$msgAllArr = array();
						$attrAllArr = array();
						foreach($model->getErrors() as $field => $msgArr){
								$attrAllArr[] = $field;
								$msgAllArr[] = $msgArr[0];
						}
						$data[$key]['msg'] = implode(", ",$msgAllArr);
						// var_dump($model->getErrors());
						// exit();
					}

				} //end loop add user
                //if($model->save())
                // $this->redirect(array('import','id'=>$id));
                 $this->render('excel',array('model'=>$model,'HisImportArr'=>$HisImportArr,'HisImportUserPassArr'=>$HisImportUserPassArr,'HisImportErrorArr'=>$HisImportErrorArr,'HisImportAttrErrorArr'=>$HisImportAttrErrorArr,'HisImportErrorMessageArr'=>$HisImportErrorMessageArr,'Insert_success'=>$Insert_success,'data' => $data));
                 exit();
            //}
        }

        // $this->render('excel',array('model'=>$model));
        $this->render('excel',array('model'=>$model,'HisImportArr'=>$HisImportArr,'HisImportUserPassArr'=>$HisImportUserPassArr,'HisImportErrorArr'=>$HisImportErrorArr,'HisImportAttrErrorArr'=>$HisImportAttrErrorArr,'HisImportErrorMessageArr'=>$HisImportErrorMessageArr,'Insert_success'=>$Insert_success));
        // $this->render('import',array(
        //     'model'=>$model,
        // ));
    }

	/**
	 * Displays a particular model.
	 */
	public function actionView()
	{
		$model = $this->loadModel();
		$profile=$model->profile;
		$this->render('view',array(
			'model'=>$model,
			'profile'=>$profile
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		Yii::app()->language ="th";
		$model=new User;
		$profile=new Profile;
		$this->performAjaxValidation(array($model,$profile));
		if(isset($_POST['ajax']) && $_POST['ajax']==='registration-form')
		{
			echo UActiveForm::validate(array($model,$profile));
			Yii::app()->end();
		}
		if(isset($_POST['User']))
		{

            $model->type_register = 1;
            // if ($_POST['User']['username'] == null) {
             	$model->username = $_POST['Profile']['staff_id'];
            // }else{
            // 	$model->username = $_POST['User']['username'];
            // }
			
			$model->create_at = date('Y-m-d H:i:s');
			$model->superuser = 0;

				$model->email = $_POST['User']['email'];
				$model->password = $_POST['Profile']['staff_id'];
				$genpass = $_POST['Profile']['staff_id'];
    			 // $model->password = $genpass;
    		  //    $model->verifyPassword = $genpass;

				$model->verifyPassword = $model->password;
				$model->confirmpass = $model->password;
				$model->online_status = 0;
				$model->del_status = 0;
				$model->repass_status= 0;
				$model->status = 1;
			    $model->scenario = 'general';
			
			$model->activkey=$this->encrypting(microtime().$model->password);
			$profile->attributes=$_POST['Profile'];
			$profile->user_id=0;
			if($model->validate() && $profile->validate()) {

				$model->password=$this->encrypting($_POST['Profile']['staff_id']);
				$model->verifyPassword=$this->encrypting($_POST['Profile']['staff_id']);
			
				if($model->save()) {
					
					$profile->user_id=$model->id;
					$profile->save();
					
				}

				$to = array(
					'email'=>$model->email,
					'firstname'=>$profile->firstname,
					'lastname'=>$profile->lastname,
				);
				$firstname = $profile->firstname;
				$lastname = $profile->lastname;
				$username = $model->username;


				$message = $this->renderPartial('Form_mail',array('email'=>$model->email,'genpass'=>$genpass,'username'=>$username,'firstname'=>$firstname,'lastname'=>$lastname),true);
				$mail = Helpers::lib()->SendMail($to,'สมัครสมาชิกสำเร็จ',$message);

				$this->redirect(array('index'));

			} else {
				$profile->validate();
			}
		
		}

		$this->render('create',array(
			'model'=>$model,
			'profile'=>$profile,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionUpdate($id)
	{
		// $model=$this->loadModel();
		// $profile=$model->profile;
		// $model->verifyPassword = $model->password;
		// //$this->performAjaxValidation(array($model,$profile));
		// // if(isset($_POST['ajax']) && $_POST['ajax']==='registration-form')
		// // {
		// // 	echo UActiveForm::validate(array($model,$profile));
		// // 	Yii::app()->end();
		// // }
		// if(isset($_GET['User']))
		// {		

		// 	$model->username = $_GET['User']['username'];

		// 	$model->status = $_GET['User']['status'];
		
		// 			$model->email = $_GET['User']['email'];

		// 			$model->confirmpass = $model->password;

		// 			if($_GET['User']['newpassword'] != null ){
		// 			$model->password=Yii::app()->controller->module->encrypting($_GET['User']['newpassword']);
		// 			// $model->verifyPassword=UserModule::encrypting($model->password);
		// 			$model->confirmpass=UserModule::encrypting($_GET['User']['confirmpass']);
		// 			}
		// 			$model->scenario = 'general';
		// 		// }

		// 	$profile->attributes=$_GET['Profile'];


		// 	if($model->validate()&&$profile->validate()) {
				

		// 		$model->save();
		// 		$profile->save();

		// 		// if(Yii::app()->user->id){
		// 		// 	Helpers::lib()->getControllerActionId($model->id);
		// 		// }
		// 		$this->redirect(array('index','id'=>$model->id));
		// 	} 

		// }

		// $this->render('update',array(
		// 	'model'=>$model,
		// 	'profile'=>$profile,
		// ));

		if($id){
    		$model = User::model()->notsafe()->findbyPk($_GET['id']);
    		$profile=$model->profile;
    		$model->verifyPassword = $model->password;
    		$this->performAjaxValidation(array($model,$profile));
    		if(isset($_POST['ajax']) && $_POST['ajax']==='registration-form')
    		{
    			echo UActiveForm::validate(array($model,$profile));
    			Yii::app()->end();
    		}

    		if(isset($_GET['User']))
    		{		
    			// $model->username = $_GET['User']['username'];
	    		// if ($_GET['User']['username'] == null) {
	             	$model->username = $_GET['Profile']['staff_id'];
	      //       }else{
	      //       	$model->username = $_GET['User']['username'];
	      //       }
	            $model->email = $_GET['User']['email']; //**

	            //$model->identification = $_GET['Profile']['identification'];


	            if(!empty($_GET['Profile']['staff_id'])){
	            	$model->password = $_GET['Profile']['staff_id'];
	            	$model->verifyPassword = $_GET['Profile']['staff_id'];
	            }
	            $profile->attributes=$_GET['Profile'];

	            if($model->validate()) {
	            	if(!empty($_GET['User']['newpassword'])){
	            		$model->password=UserModule::encrypting($model->password);
	            		$model->verifyPassword=UserModule::encrypting($model->verifyPassword);
	            		$genpass = $_GET['Profile']['staff_id'];

	            		$to = array(
						'email'=>$model->email,
						'firstname'=>$profile->firstname,
						'lastname'=>$profile->lastname,
						);
						$firstname = $profile->firstname;
						$lastname = $profile->lastname;
						$username = $model->username;


						$message = $this->renderPartial('Form_mail',array('email'=>$model->email,'genpass'=>$genpass,'username'=>$username,'firstname'=>$firstname,'lastname'=>$lastname),true);
						$mail = Helpers::lib()->SendMail($to,'สมัครสมาชิกสำเร็จ',$message);

	            	}

	            	if(!$model->save()){
	            		echo 'Model not save';
	            		exit();
	            	}
	            	if(!$profile->save(false)){
	            		echo 'profile not save';
	            		exit();
	            	}

	            	$this->redirect(array('index'));
	            }

	        }

	        $this->render('update',array(
	        	'model'=>$model,
	        	'profile'=>$profile,
	        ));



	    }
	}

	public function actionPrintpdf(){

		$user_id =$_GET['id'];
		if ($user_id != '') {
			$profiles = Profile::model()->find(array(
				'condition' => 'user_id=:user_id ',
				'params' => array('user_id' => $user_id)
			));
			$user = User::model()->find(array(
				'condition' => 'id=:id',
				'params' => array('id' => $user_id)
			));
        $path_img = Yii::app()->baseUrl. '/images/head_print.png';
     
		// $padding_left = 12.7;
		// $padding_right = 12.7;
		// $padding_top = 10;
		// $padding_bottom = 20;

		 require_once __DIR__ . '/../../../vendors/mpdf7/autoload.php';
		 $mPDF = new \Mpdf\Mpdf();
		//$mPDF = Yii::app()->ePdf->mpdf('th', 'A4', '0', 'garuda', $padding_left, $padding_right, $padding_top, $padding_bottom);
		$mPDF->useDictionaryLBR = false;
		$mPDF->setDisplayMode('fullpage');
		$mPDF->autoLangToFont = true;
        $mPDF->autoPageBreak = true;
		$mPDF->SetTitle("ใบสมัครสมาชิก");
		$texttt= '
         <style>
         body { font-family: "garuda"; }
         </style>
         ';
        $mPDF->WriteHTML($texttt);
		$mPDF->WriteHTML(mb_convert_encoding($this->renderPartial('printpdf', array('profiles'=>$profiles,'user'=>$user), true), 'UTF-8', 'UTF-8'));
		$mPDF->Output('ใบสมัครสมาชิก.pdf', 'I');

		}
	}
	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 */
	public function actionDelete()
	{
		// if(Yii::app()->request->isPostRequest)
		// 	{
			// we only allow deletion via POST request
				$model = $this->loadModel();
				$model->del_status = '1';
				$model->save(false);
				// if(Yii::app()->user->id){
				// 	Helpers::lib()->getControllerActionId();
				// }
			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
				if(!isset($_POST['ajax']))
					$this->redirect(array('index'));
			// }
			// else
			// 	throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
		}


		public function actionCheckMail(){

		    $criteria= new CDbCriteria;
		    $criteria->condition='email=:email and del_status=:del_status';
		    $criteria->params=array(':email'=>$_POST['text_mail'],':del_status'=>0);
		    $model = User::model()->findAll($criteria);
		    if ($model != null) {
		    	$response = [ 
						'success' => true,
						'message' => 'อีเมลนี้มีผู้ใช้งานแล้ว'
						];
				echo json_encode($response); 
		    }else{ 
		    	$response = [ 
						'success' => false,
						'message' => ''
						];
				echo json_encode($response); 
		    }

		}

		public function actionRunStaff(){
			if (!empty($_POST)) {
				$criteria= new CDbCriteria;
			    if ($_POST['id'] < 10) {
			       $numMin = "0".$_POST['id']."01";
			       $numMax = "0".$_POST['id']."99";
			       $criteria->addBetweenCondition('staff_id', $numMin, $numMax, 'AND');
			    }else{
			       $numMin = $_POST['id']."01";
			       $numMax = $_POST['id']."99";
			       $criteria->addBetweenCondition('staff_id', $numMin, $numMax, 'AND');
			    }
			    $criteria->order ='staff_id DESC';
			    $Profile = Profile::model()->find($criteria);
		
			    if ($Profile) {
			    	if ($_POST['id'] < 10) {
			    		$response	= $Profile->staff_id + 1;
			    		$response	= "0".$response;
				        echo ($response);
			    	}else{
			    		$response	= $Profile->staff_id + 1;
				        echo ($response);
			    	}
				    
			    }else{
			    	if ($_POST['id'] < 10) {
			    		$response = "0".$_POST['id']."01";
			    		echo ($response);
			    	}else{
			    		$response = $_POST['id']."01";
			    		echo ($response);
			    	}
			    	
			    }
			}
		}

	public function actionResign(){ // รายชื่อ user ออก
		$model = new User();
		$model->unsetAttributes();
		if(isset($_GET['User'])){
			$model->attributes = $_GET['User'];
			$model->fullname = $_GET['User']['fullname'];
			$model->username = $_GET['User']['username'];

		}

		$this->render('resign', array('model'=>$model));
	}
     
     public function actionResignUserDel(){ // user ลาออก

		// var_dump($_POST["user_id"]); exit();

		if(isset($_POST["user_id"]) && $_POST["user_id"] != "" ){
			//if(isset($_POST["user_id"]) && $_POST["user_id"] != "" && isset($_POST["date_laid_off"]) && $_POST["date_laid_off"] != "" ){
			$user = User::model()->findbyPk($_POST["user_id"]);
			if($user){
				$date = $_POST["date_laid_off"];
				$ex_date = explode("-", $date);
				$date = $ex_date[2]."-".$ex_date[1]."-".$ex_date[0];
				$date_add_one = date('Y-m-d', strtotime("+1 day", strtotime($date)));

				$user->del_status = 1;
				
				if($user->save(false)){
					$LogResign = new LogResign;
					$LogResign->user_id = $user->id;
					$LogResign->date_laid_off = $date;
					$LogResign->date_laid_off_plusone = $date_add_one;
					$LogResign->fullname = $user->profile->firstname." ".$user->profile->lastname;
					$LogResign->fullname_en = $user->profile->firstname_en." ".$user->profile->lastname_en;
					$LogResign->staff_id = $user->profile->staff_id;
					// $LogResign->identification = $user->profile->identification;
					// $LogResign->hire_date = $user->profile->hire_date;
					$LogResign->action_status = 1;
					$LogResign->save();
					

						// if(Yii::app()->user->id){
						// 	Helpers::lib()->getControllerActionId($user->id);
						// }


					echo "success";
				}
			}
		}
	}

	public function actionResignUserAdd(){ // user ลาออก

		// var_dump($_POST["user_id"]); exit();

		if(isset($_POST["user_id"]) && $_POST["user_id"] != ""){
			$user = User::model()->findbyPk($_POST["user_id"]);
			if($user){
				$user->del_status = 0;

				if($user->save(false)){
					$LogResign = new LogResign;
					$LogResign->user_id = $user->id;
					$LogResign->fullname = $user->profile->firstname." ".$user->profile->lastname;
					$LogResign->fullname_en = $user->profile->firstname_en." ".$user->profile->lastname_en;
					$LogResign->staff_id = $user->profile->staff_id;
					// $LogResign->identification = $user->profile->identification;
					// $LogResign->hire_date = $user->profile->hire_date;
					$LogResign->action_status = 2;
					$LogResign->save();


					// if(Yii::app()->user->id){
					// 	Helpers::lib()->getControllerActionId($user->id);
					// }
					echo "success";
				}
			}
		}
	}
		


	/**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
	protected function performAjaxValidation($validate)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='user-form')
		{
			echo CActiveForm::validate($validate);
			Yii::app()->end();
		}
	}


	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 */
	public function loadModel()
	{
		if($this->_model===null)
		{
			if(isset($_GET['id']))
				$this->_model=User::model()->notsafe()->findbyPk($_GET['id']);
			if($this->_model===null)
				throw new CHttpException(404,'The requested page does not exist.');
		}
		return $this->_model;
	}

}
