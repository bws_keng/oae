 
<div class="container-fluid">

<!-- start row -->
<div class="row">
    <div class="col-lg-12 set-height set-padding">
        <div id="section1">
            <div class="card">
                <div class="card-header">
                    <h5 class="mb-1">หมวดที่ <?php echo $title["question_group_number"]; ?> จัดการหมวดหมู่</h5>
                </div>
            </div>

            <div class="card table-card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-4 mb-3">
                            <div class="form-group">
                                <label for="">ปี พ.ศ. แบบสอบถาม</label>
                                <select class="form-control digits" id="search_year">
                                    <option selected disabled></option>
                                    <?php
                                        foreach ($years as $key => $value) {
                                            echo '<option>'.$value['year'].'</option>';
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-4 mb-3">
                            <button class="btn btn-lg btn-search text-white" type="submit" onclick="SearchData();"><i class="icon-search"></i>&nbsp;ค้นหา</button>
                        </div>

                    </div>
                </div>
                <div class="card-body">

                </div>

            </div>

            <!-- <div class="row justify-content-end">
                <div class="col-md-3">
                    <a href="<?php echo Yii::app()->baseurl.'/admin/survey/'.$_GET['id'].'/group/create';?>">
                        <div class="card o-hidden create-main">
                            <div class="b-r-4 card-body">
                                <div class="media static-top-widget">
                                    <div class="align-self-center text-center"><i data-feather="plus"></i></div>
                                    <div class="media-body">
                                        <h6 class="mb-0 counter">เพิ่มข้อมูลหมวดหมู่</h6><i class="icon-bg" data-feather="database"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div> -->

            <div class="card table-card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-main" id="">
                            <!-- <table class="table table-striped table-main" id="basic-2"> -->
                            <thead>
                                <tr>
                                    <th width="10%">ลำดับ</th>
                                    <th width="40%">ปี พ.ศ. แบบสอบถาม</th>
                                    <th>จัดฟอร์มคำถาม</th>
                                    <!-- <th>จัดฟอร์มคำตอบ</th> -->
                                    <th width="25%">จัดการ</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                                // print_r($survey);exit();
                                foreach ($survey as $key => $value) {
                                    echo '
                                    <tr>
                                        <td>'.($key + 1).'</td>
                                        <td>'.($value['year']+543).' - '.$value['name'].'</td>
                                        <td>
                                            <a href="'.Yii::app()->baseurl.'/admin/survey/'.$_GET['id'].'/group/'.$value['id'].'/form">
                                                <button class="btn btn-md btn-search text-white mt-0" type="submit">จัดฟอร์ม</button>
                                            </a>
                                        </td>';
                                        // <td>
                                        //     <a href="index.php?r=admin/page&page=admin-1-7">
                                        //         <button class="btn btn-md btn-search text-white mt-0" type="submit">จัดฟอร์ม</button>
                                        //     </a>
                                        // </td>
                                    // echo '<td>
                                    //         <a type="button" class="btn btn-icon text-success" href="'.Yii::app()->baseurl.'/admin/survey/'.$_GET['id'].'/group/'.$value['id'].'/edit">
                                    //             <i data-feather="edit"></i>
                                    //         </a>
                                    //         <button type="button" class="btn btn-icon text-danger" onclick="sendDelete('.$value['id'].', \''.$value['name'].'\')"><i data-feather="trash"></i></button>
                                    //     </td>
                                    // </tr>
                                    // ';
                                    echo '<td>
                                            <a type="button" class="btn btn-icon text-success" href="'.Yii::app()->baseurl.'/admin/survey/'.$_GET['id'].'/group/'.$value['id'].'/edit">
                                                <i data-feather="edit"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    ';
                                }
                            ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="table-responsive-search" style="display: none;"></div>
                </div>

            </div>

        </div>

    </div>
</div>
<!-- end row -->
<div class="container position-fixed fixed-top pt-5" id="deleteForm" style="display:none;">
        <div class="card col-12 p-0">
            <div class="card-header">
                <h5>ลบข้อมูล</h5>
            </div>
            <div class="card-body">
                <h5 class="card-title">เมื่อข้อมูลถูกลบจะไม่สามารถย้อนคืนได้</h5>
                <div class="card-text">
                    <form method="POST" action="<?php echo Yii::app()->baseurl; ?>/admin/survey/<?php echo $_GET['id']; ?>/group/delete">
                        <label for="deleteKey">รหัสอ้างอิง</label>
                        <input id="deleteKey" name="key" class="form-control" value="" readonly/>
                        <label for="deleteValue">ข้อมูล</label>
                        <input id="deleteValue" name="value" class="form-control" value="" readonly/>
                        <button class="btn btn-danger btn-lg mt-2">ลบข้อมูล</button>
                        <button type="button" class="btn btn-light btn-lg mt-2" onclick='$("#deleteForm").hide();'>ยกเลิก</button>
                    </form>
                </div>
            </div>
        </div>

    </div>
</div>
<script type="text/javascript">
    function SearchData(){
     var id = <?php echo $_GET['id'] ?>;
     var search_year = $("#search_year").val();

     if (id != "" && search_year != "") {

         $.ajax({
            url: "<?= $this->createUrl('/admin/SurveyGroupSearch'); ?>", 
            type: "GET",
            data:  {
                id:id,
              search_year:search_year,
          },
          success: function(data){

            if (data) {
                $('.table-responsive').remove();
                $('.table-responsive-search').show();
                $('.table-responsive-search').html(data);
            }
       }                           
   });
     }else{
    
      alert("ป้อนข้อมูลให้ถูกต้องแล้วกดค้นหา");
      location.reload();
  }

}
</script>
