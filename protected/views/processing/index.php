
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
<style type="text/css">
  .dropbtn {
    background-color: #8DBF42;
    color: white;
    padding: 16px;
    font-size: 16px;
    border: none;
  }

  .dropdown {
    position: relative;
    display: inline-block;
  }

  .dropdown-content {
    display: none;
    position: absolute;
    background-color: #f1f1f1;
    min-width: 160px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
  }

  .dropdown-content a {
    color: black;
    padding: 12px 16px;
    text-decoration: none;
    display: block;
  }

  .dropdown-content a:hover {background-color: #ddd;}

  .dropdown:hover .dropdown-content {display: block;}

  .dropdown:hover .dropbtn {background-color: #3e8e41;}

</style> 
<!DOCTYPE html>
<html lang="en">
<body data-spy="scroll" data-target="#myScrollspy">
  <!-- start row -->
  <div class="row">
    <div class="col-lg-12 set-height set-padding">
      <div id="section1">
        <div class="card">
          <div class="card-header">
            <h5 class="mb-1">ค่าตัวคูณขยาย</h5>
          </div>
        </div>
        <div class="card table-card">
          <!--  <form method="GET" accept-charset="UTF-8" id="form_search" action="<?php echo $this->createUrl('/Processing/index'); ?>"> -->
            <div class="card-header">
              <div class="row">
                <div class="col-md-3 mb-3">
                  <div class="form-group">
                    <label for="">ปีเพาะปลูก<font color="red">*</font></label>
                    <?php
                    $Survey = Survey::model()->findAll();
                    ?>
                    <select class="form-control digits" id="search_Survey" name="search[Survey]">
                      <option selected disabled>เลือกปี พ.ศ.</option>
                      <?php
                      foreach ($Survey as $key => $value) {
                        $year = $value->year + 543;
                       ?>
                       <option value="<?= $value->id;  ?>" <?php if(isset($_GET["search"]["Survey"]) && $_GET["search"]["Survey"] == $value->id){ echo "selected"; } ?> ><?= $year;  ?>/63</option>
                       <?php
                     }
                     ?>
                   </select>
                 </div>
               </div>

               <div class="col-md-3 mb-3">
                <div class="form-group">
                  <label for="">พวกที่<font color="red">*</font></label>
                  <?php
                  $criteria = new CDbCriteria();
                  $criteria->compare('active',1);
                  $criteria->order = "those_id ASC";
                  $Those = Those::model()->findAll($criteria);
                  ?>
                  <select class="form-control digits" id="search_Those" name="search[Those]">
                    <option selected disabled>เลือกพวกที่</option>
                    <?php
                    foreach ($Those as $key => $value) { ?>
                     <option value="<?= $value->those_id;  ?>"<?php if(isset($_GET["search"]["Those"]) && $_GET["search"]["Those"] == $value->those_id){ echo "selected"; } ?> ><?= $value->those_code;  ?> -> <?= $value->those_name;  ?></option>
                     <?php
                   }
                   ?>
                 </select>
               </div>
             </div>
             <div class="col-md-3 mb-3">
              <button class="btn btn-lg btn-search text-white search-data" type="submit" onclick="SearchData();"><i class="icon-search"></i>&nbsp;ค้นหา</button>
            </div>
            <div class="col-md-3">
              <div class="form-group m-t-20 text-right">
                <a href="<?= $this->createUrl('/Processing/export'); ?>" class="btn btn-lg btn-export text-white pull-right " type="submit"><i class="fa fa-download"></i>&nbsp;&nbsp;Export คูณขยาย</a>
              </div>
            </div>

          </div>
        </div>
        <!--  </form> -->
      </div>

      <div class="row justify-content-end">
        <div class="col-md-3">
          <a href="#" class="create-processing">
            <div class="card o-hidden create-main">
              <div class="b-r-4 card-body">
                <div class="media static-top-widget">
                  <div class="align-self-center text-center"><i data-feather="plus"></i></div>
                  <div class="media-body">
                    <h6 class="mb-0 counter">เพิ่มข้อมูลค่าคูณขยาย</h6><i class="icon-bg" data-feather="database"></i>
                  </div>
                </div>
              </div>
            </div>
          </a>
        </div>
      </div>
      <div class="row search-results"></div>
    </div>
  </div>
</div>

<style>
  .table-modalmain tr td {
    width: 50%;
  }

  .table-linkmodal {
    cursor: pointer;
  }
</style>

<div class="modal fade" id="exampleModal" aria-labelledby="exampleModalLabel" aria-hidden="true" style="overflow:hidden;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body">
<!--         <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/> -->

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="Save_Data();">บันทึก</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิด</button>
      </div>
    </div>
  </div>
</div>

<div class="row" id="SubProcessing"></div>

</body>

</html>

<script type="text/javascript">


  function Save_Data(){

    var id = $("#total-data").attr('model-id');
    var year = $("#year-data").val();
    var group = $("#group-data").val();
    var province = $("#province-data").val();

    if (group != null && year != null && province != null) {

      $.ajax({
        url: "<?= $this->createUrl('/Processing/SaveData'); ?>", 
        type: "POST",
        data:  {
          id:id,
          year:year,
          group:group,
          province:province,
          total:$("#total-data").val(),
          random:$("#random-data").val(),
          survey:$("#survey-data").val(),
        },
        success: function(data){
          if(data){
          var obj = JSON.parse(data);
          if (obj['success'] === true) {
            $("#SubProcessing").empty();
            if (obj['action'] === 'add') {
             SearchFormcreate(year,group);
            }else if(obj['action'] === 'edit'){
             SearchFormedit(year,group);
            }else{
             SearchData();
            }
             
          }else{
            alert(obj['message']);
          }
            
        }
          // if (data == 1) {
          //    SearchData();
          //    $("#SubProcessing").empty();
          // }else{
          //   alert("SAVE DATA ERROR");
          // }
        },
        error: function (xhr, status) {
            alert("SAVE DATA ERROR");
        }                           
      });
    }else{
      alert("กรุณากรอกข้อมูลให้ครบ");
    }
  }

    $(".create-processing").click(function(){
        var create = 'add'; 
        $.ajax({
            url: "<?= $this->createUrl('/Processing/LoadData'); ?>", 
            type: "POST",
            data:  {
                action:create
            },
            success: function(data){
               $('#exampleModal').modal('show');
               $('#exampleModal .modal-body').html(data);
            }  
        });          
    });

    function SearchData(){
      
       var Survey = $("#search_Survey").val();
       var Those = $("#search_Those").val();
       if (Survey != null && Those != null) {
       $.ajax({
        url: "<?= $this->createUrl('/Processing/Search'); ?>", 
        type: "GET",
        data:  {
          Survey:Survey,
          Those:Those,
        },
        success: function(data){

         $('.search-results').html(data);
       }                           
     });
     }else{
      alert("ป้อนข้อมูลให้ถุกต้องแล้วกดค้นหา");
     }
    }

    function SearchFormcreate(Survey,Those){
  
       $.ajax({
        url: "<?= $this->createUrl('/Processing/Search'); ?>", 
        type: "GET",
        data:  {
          Survey:Survey,
          Those:Those,
        },
        success: function(data){

         $('.search-results').html(data);
       }                           
     });
    }

    function SearchFormedit(Survey,Those){
  
       $.ajax({
        url: "<?= $this->createUrl('/Processing/Search'); ?>", 
        type: "GET",
        data:  {
          Survey:Survey,
          Those:Those,
        },
        success: function(data){

         $('.search-results').html(data);
       }                           
     });
    }

     $("#search_Those").select2( {
     placeholder: "เลือกพวกที่",
     allowClear: true
     });
  </script>
