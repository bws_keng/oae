<?php
$this->breadcrumbs=array(
  // UserModule::t('Users')=>array('/user'),
  UserModule::t('Manage'),
);
$formNameModel = 'User';
$title = "จัดการเจ้าหน้าที่สำนักงานเขต";

Yii::app()->clientScript->registerScript('search', "
  $('#SearchFormAjax').submit(function(){
      $.fn.yiiGridView.update('$formNameModel-grid', {
          data: $(this).serialize()
      });
      return false;
  });
");

Yii::app()->clientScript->registerScript('updateGridView', <<<EOD
  $.updateGridView = function(gridID, name, value) {
      $("#"+gridID+" input[name*="+name+"], #"+gridID+" select[name*="+name+"]").val(value);
      $.fn.yiiGridView.update(gridID, {data: $.param(
          $("#"+gridID+" input, #"+gridID+" .filters select")
      )});
  }
  $.appendFilter = function(name, varName) {
      var val = eval("$."+varName);
      $("#$formNameModel-grid").append('<input type="hidden" name="'+name+'" value="">');
  }
  $.appendFilter("User[news_per_page]", "news_per_page");
EOD
, CClientScript::POS_READY);

?>
<div class="container-fluid">
  <!-- start row -->
  <div class="row">
    <div class="col-lg-12 set-height set-padding">
      <div id="section1">
        <div class="card">
          <div class="card-header">
            <h5 class="mb-1">
              <?php
                echo $title;
              ?>
            </h5>
          </div>
        </div>
        <div class="row justify-content-end">
          <div class="col-md-3">
            <a href="<?php echo Yii::app()->createUrl('user/admin/create'); ?>">
              <div class="card o-hidden create-main">
                <div class="b-r-4 card-body">
                  <div class="media static-top-widget">
                    <div class="align-self-center text-center"><i data-feather="plus"></i></div>
                    <div class="media-body">
                      <h6 class="mb-0 counter">เพิ่มเจ้าหน้าที่สำนักเขต</h6><i class="icon-bg" data-feather="database"></i>
                    </div>
                  </div>
                </div>
              </div>
            </a>
          </div>
        </div>
        <div class="card table-card">
        <!--   <div class="card-header">
            <div class="row">
              <div class="col-md-3 mb-3">
                <label for="">ผืนที่</label>
                <input class="form-control" id="" maxlength="1" type="text" placeholder="" required=""
                  data-original-title="" title="">
              </div>
              <div class="col-md-3 mb-3">
                <div class="form-group">
                  <label for="">จังหวัด</label>
                  <select class="form-control digits" id="">
                    <option selected disabled>เลือกจังหวัด</option>
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                  </select>
                </div>
              </div>
              <div class="col-md-3 mb-3">
                <div class="form-group">
                  <label for="">ปี พ.ศ.</label>
                  <select class="form-control digits" id="">
                    <option selected disabled>เลือกปี พ.ศ.</option>
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                  </select>
                </div>
              </div>
              <div class="col-md-3 mb-3">
                <button class="btn btn-lg btn-search text-white" type="submit"><i
                    class="icon-search"></i>&nbsp;ค้นหา</button>
              </div>
            </div>
          </div> -->
          <div class="card-body">
            <div class="table-responsive">
              <?php $this->widget('AGridView', array(

                  'id'=>$formNameModel.'-grid',
                  'dataProvider'=>$model->search(),
                  //'filter'=>$model,
                  'selectableRows' => 2,
                  //'rowCssClassExpression'=>'"items[]_{$data->id}"',
                  'htmlOptions' => array(
                    'style'=> "margin-top: -1px;width:100%;",
                  ),
                  'afterAjaxUpdate'=>'function(id, data){
                    $.appendFilter("User[news_per_page]");
                    InitialSortTable(); 
                    }',

                  'columns'=>array(
                    array(
                      'header'=>'No.',
                      'value'=>'$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
                      'filterHtmlOptions'=>array('style'=>'width:30px;'), 
                      'htmlOptions'=>array('style'=>'text-align: center;hight:5%;'),
                    ),
      
                    array(
                      'header' => 'ชื่อ - นามสกุล',
                      'type'=>'html',
                      'value'=>function($data){
                          return $data->profiles->firstname . ' ' . $data->profiles->lastname;

                      }
                    ),

                    array(
                      'header' => 'รหัสพนักงาน',
                      'name'=>'username',
                      'type'=>'html',
                      'value'=>function($data){
                        return $data->profiles->staff_id;
                      }
                    ),
                    
                    //   array(
                    //   'header' => 'เลขประจำตัวบัตรประชาชน',
                    //   'name'=>'identification',
                    //   'type'=>'html',
                    //   'value'=>function($data){
                    //     return $data->identification;
                    //   },
                    //   'filterHtmlOptions'=>array('style'=>'width:30px'),
                    // ),
                    array(
                      'name'=>'email',
                      'type'=>'raw',
                      'value'=>'CHtml::link(UHtml::markSearch($data,"email"), "mailto:".$data->email)',
                      'filterHtmlOptions'=>array('style'=>'width:30px'),
      
                    ),
                    // array(
                    //   'name'=>'create_at',
                    //   'type'=>'html',
                    //   'filter' => false,
                    //   'value'=>function($data){
                    //     return Helpers::changeFormatDate($data->create_at,'datetime');
                    //   },
                    //   'filterHtmlOptions'=>array('style'=>'width:30px'),
                    // ),
                    // array(
                    //   'name'=>'lastvisit_at',
                    //   'type'=>'html',
                    //   'filter' => false,
                    //   'value'=>function($data){
                    //     return Helpers::changeFormatDate($data->lastvisit_at,'datetime');
                    //   },
                    //   'filterHtmlOptions'=>array('style'=>'width:30px'),
                    // ),
                    // array(
                    //   'name'=>'status',
                    //   'type'=>'raw',
                    //   'value'=>'User::itemAlias("UserStatus",$data->status)',
                    //   'filter' => User::itemAlias("UserStatus"),
                    //   'filterHtmlOptions'=>array('style'=>'width:30px'),
                    //   //'htmlOptions'=>array('style'=>'text-align: center;width:100%;'),
                    // ),
                    // array(
                    //   'name'=>'online_status',
                    //   'type'=>'raw',
                    //   'value'=>'User::chk_online($data->id,$data->lastactivity,$data->online_status)',
                    //   'filter' => User::itemAlias("Online"),
                    //   'filterHtmlOptions'=>array('style'=>'width:30px'),
                    //   //'htmlOptions'=>array('style'=>'text-align: center;width:100%;'),
                    // ),
                    
                   array(
                    'class' => 'zii.widgets.grid.CButtonColumn',
                    'htmlOptions' => array('style' => 'white-space: nowrap'),
                    'afterDelete' => 'function(link,success,data) { if (success && data) alert(data); }',
                    'template' => '{view} {update} {btn_delete}',
                    'buttons' => array(
                        'view' => array(

                            'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => 'View'),
                            'label' => '<button type="button" class="btn btn-icon text-primary"><i data-feather="eye"></i></button>',
                            'imageUrl' => false,
                        ),
                        'update' => array(
                            'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => 'Update'),
                            'label' => '<button type="button" class="btn btn-icon text-success"><i data-feather="edit"></i></button>',
                            'imageUrl' => false,
                            // 'url'=> function($data) {
                            //     return Yii::app()->controller->createUrl('adminUser/update', ['id' => $data->id]);
                            // }
                        ),
                        'btn_delete' => array(
                            'options' => array(
                                'rel' => 'tooltip', 
                                'data-toggle' => 'tooltip', 
                                'title' => 'Delete',
                                'class' => 'btn_del'
                                ), 
                                'url'=> function($data) {
                                return Yii::app()->controller->createUrl('//user/admin/delete', ['id' => $data->id]);
                                },
                            'label' => '<button type="button" class="btn btn-icon text-danger"><i data-feather="trash"></i></button>',
                            'imageUrl' => false,
                            //'Url' => false,
                            )
                        )
                    ),
                  ),
));


?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- end row -->
</div>