<?php
header("Content-Type: application/vnd.ms-excel");
header('Content-Disposition: attachment; filename="MyXls.xls"');
$strExcelFileName = "รายงานทัศนคติฯ.xls";
header("Content-Type: application/x-msexcel; name=\"" . $strExcelFileName . "\"");
header("Content-Disposition: inline; filename=\"" . $strExcelFileName . "\"");
header('Content-Type: text/plain; charset=UTF-8');
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Pragma:no-cache");

?>
<?php

if (isset($_GET["date"])) {

    $criteria = new CDbCriteria();
    // $criteria->compare('active',1);
    $criteria->order = "those_id ASC";
    $Those = Those::model()->findAll($criteria);
    $array_thoseByVillage = array();
    foreach ($Those as $key => $value) {
        $array_thoseByVillage[$value->those_id] = array();
    }

    foreach ($array_thoseByVillage as $keyThose => $valueThose) {
        $criteria = new CDbCriteria();
        $criteria->compare('t.group',$keyThose);
        $criteria->compare('active',1);
        $Processing = Processing::model()->findAll($criteria);
        if (!empty($Processing)) {
            foreach ($Processing as $keyProcessing => $valueProcessing) {
                $array_thoseByVillage[$keyThose][$valueProcessing->province_table->province_code] = array();

                $criteria = new CDbCriteria();
                $criteria->compare('processing_id',$valueProcessing->id);
                $criteria->compare('active',1);
                $SubProcessing = SubProcessing::model()->findAll($criteria);
                $array_thoseByVillage[$keyThose][$valueProcessing->province_table->province_code] = array();
                foreach ($SubProcessing as $keySubProcessing => $valueSubProcessing) {
                    if (!isset($array_thoseByVillage[$keyThose][$valueProcessing->province_table->province_code][$valueSubProcessing->district_table->district_code])) {
                        $array_thoseByVillage[$keyThose][$valueProcessing->province_table->province_code][$valueSubProcessing->district_table->district_code] = array();
                    }
                    if (!isset($array_thoseByVillage[$keyThose][$valueProcessing->province_table->province_code][$valueSubProcessing->district_table->district_code][$valueSubProcessing->subdistrict_table->subdistrict_code])) {
                        $array_thoseByVillage[$keyThose][$valueProcessing->province_table->province_code][$valueSubProcessing->district_table->district_code][$valueSubProcessing->subdistrict_table->subdistrict_code] = array();
                    }
                    if (!isset($array_thoseByVillage[$keyThose][$valueProcessing->province_table->province_code][$valueSubProcessing->district_table->district_code][$valueSubProcessing->subdistrict_table->subdistrict_code][$valueSubProcessing->village_table->village_code])) {
                        $array_thoseByVillage[$keyThose][$valueProcessing->province_table->province_code][$valueSubProcessing->district_table->district_code][$valueSubProcessing->subdistrict_table->subdistrict_code][$valueSubProcessing->village_table->village_code] = array();
                    }
                    $array_thoseByVillage[$keyThose][$valueProcessing->province_table->province_code][$valueSubProcessing->district_table->district_code][$valueSubProcessing->subdistrict_table->subdistrict_code][$valueSubProcessing->village_table->village_code]["N"] = $valueProcessing->total;
                    $array_thoseByVillage[$keyThose][$valueProcessing->province_table->province_code][$valueSubProcessing->district_table->district_code][$valueSubProcessing->subdistrict_table->subdistrict_code][$valueSubProcessing->village_table->village_code]["n"] = $valueProcessing->survey;
                    $array_thoseByVillage[$keyThose][$valueProcessing->province_table->province_code][$valueSubProcessing->district_table->district_code][$valueSubProcessing->subdistrict_table->subdistrict_code][$valueSubProcessing->village_table->village_code]["M"] = $valueSubProcessing->total;
                    $array_thoseByVillage[$keyThose][$valueProcessing->province_table->province_code][$valueSubProcessing->district_table->district_code][$valueSubProcessing->subdistrict_table->subdistrict_code][$valueSubProcessing->village_table->village_code]["m"] = $valueSubProcessing->survey;
                    
                }
            }
        }
    }

       // หาค่าคูณขยายระดับจังหวัด START
    $array_processingByProvince = array();
    $array_checkProvinceThose = array();
    foreach ($array_thoseByVillage as $keythose => $valuethose) {
        foreach ($valuethose as $keyvaluethose => $valuevaluethose) {
            foreach ($valuevaluethose as $keyvaluevaluethose => $valuevaluevaluethose) {
                foreach ($valuevaluevaluethose as $keyvaluevaluevaluethose => $valuevaluevaluevaluethose) {
                    foreach ($valuevaluevaluevaluethose as $keyvaluevaluevaluevaluethose => $valuevaluevaluevaluevaluethose) {
                        if (!isset($array_processingByProvince[$keyvaluethose])) {
                            $array_processingByProvince[$keyvaluethose] = array();
                        }
                        if (!isset($array_checkProvinceThose[$keythose][$keyvaluethose])) {
                            $array_checkProvinceThose[$keythose] = array();
                            $array_checkProvinceThose[$keythose][$keyvaluethose] = array();
                            if (!isset($array_processingByProvince[$keyvaluethose]["N"])) {
                                $array_processingByProvince[$keyvaluethose]["N"] = $valuevaluevaluevaluevaluethose["N"];
                            }else{
                                $array_processingByProvince[$keyvaluethose]["N"] += $valuevaluevaluevaluevaluethose["N"];
                            }
                            if (!isset($array_processingByProvince[$keyvaluethose]["n"])) {
                                $array_processingByProvince[$keyvaluethose]["n"] = $valuevaluevaluevaluevaluethose["n"];
                            }else{
                                $array_processingByProvince[$keyvaluethose]["n"] += $valuevaluevaluevaluevaluethose["n"];
                            }
                        }

                        if (!isset($array_processingByProvince[$keyvaluethose]["M"])) {
                            $array_processingByProvince[$keyvaluethose]["M"] = $valuevaluevaluevaluevaluethose["M"];
                        }else{
                            $array_processingByProvince[$keyvaluethose]["M"] += $valuevaluevaluevaluevaluethose["M"];
                        }
                        if (!isset($array_processingByProvince[$keyvaluethose]["m"])) {
                            $array_processingByProvince[$keyvaluethose]["m"] = $valuevaluevaluevaluevaluethose["m"];
                        }else{
                            $array_processingByProvince[$keyvaluethose]["m"] += $valuevaluevaluevaluevaluethose["m"];
                        }

                    }
                }
            }
        }
    }
    // หาค่าคูณขยายระดับจังหวัด END
//     echo("<pre>");
// var_dump($array_processingByProvince);exit();
//เริ่มคำนวณค่าคูณขยายระดับหมู่บ้าน
  $criteria = new CDbCriteria();
$criteria->compare('question_group_id',12);
$criteria->order = "sortorder ASC";
$Question = Question::model()->findAll($criteria);
foreach ($Question as $key => $value) {
    $Choice[$value->question_id] = $value->question;

    $criteria=new CDbCriteria;
    $criteria->compare('question_id',$value->question_id);
    $criteria->order = "sortorder ASC";
    $QuestionSub = QuestionSub::model()->findAll($criteria);
    $Array_ans[$value->question_id] = array();
    foreach ($QuestionSub as $keyQuestionSub => $valueQuestionSub) {
        $Array_ans[$value->question_id][$valueQuestionSub->id] = $valueQuestionSub->question_detail;
    }
}

$criteria = new CDbCriteria;
$criteria->addInCondition('sector_id',$_GET["sector"]);
$MasterSectorData = MasterSector::model()->findAll($criteria);
$array_id_county = array();
foreach ($MasterSectorData as $keyMasterSectorData => $valueMasterSectorData) {
    $MasterCountyData = MasterCounty::model()->findAll(array(
        'condition' => 'sector_id=:sector_id',
        'params' => array(':sector_id' => $valueMasterSectorData->sector_id)
    ));
    foreach ($MasterCountyData as $keyMasterCountyData => $valueMasterCountyData) {
        $array_id_county[] = $valueMasterCountyData->county_id;
    }
}
$criteria = new CDbCriteria;
$criteria->addInCondition('county_id',$array_id_county);
$criteria->order = "province_code ASC";
$ProvinceData = MtProvince::model()->findAll($criteria);
$Province_code_array = array();
foreach ($ProvinceData as $key => $value) {
    $Province_code_array[] = $value->province_code;
}
$criteria = new CDbCriteria;
$criteria->addInCondition('province',$Province_code_array);
$criteria->order = "questionnaire_id ASC";
$QuestionnaireData = Questionnaire::model()->findAll($criteria);

foreach ($QuestionnaireData as $keyQuestionnaireData => $valueQuestionnaireData) {
    foreach ($Choice as $keyChoice => $valueChoice) {
        foreach ($Array_ans[$keyChoice] as $keyArray_ans => $valueArray_ans) {
            $criteria = new CDbCriteria;
            $criteria->compare('questionnaire_id',$valueQuestionnaireData->questionnaire_id);
            $criteria->compare('question_sub_id',$keyArray_ans);
            // $criteria->order = "sortorder ASC";
            $SurveySubValueData = SurveySubValue::model()->findAll($criteria);
            $SurveyvalueDataCheckTotal=0;
                        foreach ($SurveySubValueData as $keySurveyvalueDataCheck => $valueSurveyvalueDataCheck) {
                            $SurveyvalueDataCheckTotal +=$valueSurveyvalueDataCheck->value;
                        }
                        if(!isset($array_province[$valueQuestionnaireData->province][$keyChoice][$keyArray_ans])){
                            if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                $array_province[$valueQuestionnaireData->province][$keyChoice][$keyArray_ans] = $SurveyvalueDataCheckTotal*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]);
                            }
                        }else{
                            if (isset($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village])) {
                                $array_province[$valueQuestionnaireData->province][$keyChoice][$keyArray_ans] = $array_province[$valueQuestionnaireData->province][$keyChoice][$keyArray_ans] + ($SurveyvalueDataCheckTotal*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["N"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["n"])*($array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["M"]/$array_thoseByVillage[$valueQuestionnaireData->group][$valueQuestionnaireData->province][$valueQuestionnaireData->district][$valueQuestionnaireData->subdistrict][$valueQuestionnaireData->village]["m"]));
                            }
                        }
        }
    }
}
} 
?>
<style>
table, td, th {
  border: 1px solid black;
}
</style>
<table class="table table-striped table-main" id="">
                        <thead>
                           <tr class="report-th" style="background-color: #95c0c0;">
                            <th width="100%">รายการ</th>
                            <?php $Master_province_array = array(); ?>
                            <?php

                            if (isset($_GET["sector"])) {
                                $criteria = new CDbCriteria;
                                $criteria->addInCondition('sector_id',$_GET["sector"]);
                                $MasterSectorData = MasterSector::model()->findAll($criteria);
                            }else{
                                $MasterSectorData = array();
                            }
                            foreach ($MasterSectorData as $keyMasterSectorData => $valueMasterSectorData) {
                                ?> 
                                <th><?=$valueMasterSectorData->sector_name?></th>
                                <?php
                                $MasterCountyData = MasterCounty::model()->findAll(array(
                                    'condition' => 'sector_id=:sector_id',
                                    'params' => array(':sector_id' => $valueMasterSectorData->sector_id)
                                ));
                                $array_id_county = array();
                                foreach ($MasterCountyData as $keyMasterCountyData => $valueMasterCountyData) {
                                    $array_id_county[] = $valueMasterCountyData->county_id;
                                }
                                $criteria = new CDbCriteria;
                                $criteria->compare('active', 1);
                                $criteria->addInCondition('county_id',$array_id_county);
                                $criteria->order = 'province_code ASC';
                                $MtProvinceDataSort = MtProvince::model()->findAll($criteria);
                                foreach ($MtProvinceDataSort as $keyMtProvinceDataSort => $valueMtProvinceDataSort) {
                                    $Master_province_array[$valueMasterSectorData->sector_id][] = $valueMtProvinceDataSort->id;
                                    ?>
                                    <th ><?=$valueMtProvinceDataSort->province_name_th?></th>
                                    <?php 
                                }
                            }

                            ?>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        if (!empty($Choice)) {
                            foreach ($Choice as $keyChoice => $valueChoice) { ?>
                                <tr class="report-title" style="background-color: #e1f6f6;">

                                    <td colspan="<?=$keyMtProvinceDataSort+3?>"><?=$valueChoice?></td>

                                    <?php 
                                   // foreach ($Array_ans[$keyChoice] as $keyArray_ans => $valueArray_ans) { ?>
                                      <!-- <tr class="report-ans">   -->
                                       <!-- <td class="text-left">"<?=$valueArray_ans?>"</td> -->

                                       <?php foreach ($Master_province_array as $keyMaster_province_array => $valueMaster_province_array) {
                                        $total_result = 0;
                                        foreach ($valueMaster_province_array as $keytotal_result => $valuetotal_result) {
                                            $MtProvinceData = MtProvince::model()->find(array(
                                                'condition' => 'id=:id',
                                                'params' => array(':id' => $valuetotal_result)
                                            ));
                                            if (isset($array_province[$MtProvinceData->province_code][$keyChoice])) {
                                                foreach ($array_province[$MtProvinceData->province_code][$keyChoice] as $key => $value) {

                                                    $total_result+=($value*$array_processingByProvince[$MtProvinceData->province_code]["n"]/$array_processingByProvince[$MtProvinceData->province_code]["N"]/$array_processingByProvince[$MtProvinceData->province_code]["M"]); ?>
                                                <?php   }
                                            }
                                        } ?>
                                    <?php } 
                                      // var_dump($total_result);
                                    ?>

                               

                                    <?php foreach ($Master_province_array as $keyMaster_province_array => $valueMaster_province_array) {
                                        $total_result = 0;
                                        foreach ($valueMaster_province_array as $keytotal_result => $valuetotal_result) {
                                            $MtProvinceData = MtProvince::model()->find(array(
                                                'condition' => 'id=:id',
                                                'params' => array(':id' => $valuetotal_result)
                                            ));
                                            if (isset($array_province[$MtProvinceData->province_code][$keyChoice])) {
                                                foreach ($array_province[$MtProvinceData->province_code][$keyChoice] as $key => $value) {

                                                    $total_result+=($value*$array_processingByProvince[$MtProvinceData->province_code]["n"]/$array_processingByProvince[$MtProvinceData->province_code]["N"]/$array_processingByProvince[$MtProvinceData->province_code]["M"]); ?>
                                                <?php   }
                                            }
                                            ?>
                                        <?php } } ?>
                                  <!--   </tr> -->
                                <?php// } ?> 
                            </tr>
                            <?php  if (count($Array_ans[$keyChoice]) > 1) {
                                foreach ($Array_ans[$keyChoice] as $keyArray_ans => $valueArray_ans) { ?>
                                    <tr class="report-ans">
                                        <td class="text-left"><?=$valueArray_ans?></td>
                                        <?php foreach ($Master_province_array as $keyMaster_province_array => $valueMaster_province_array) {
                                            $total_result_detail = 0;
                                            foreach ($valueMaster_province_array as $keytotal_result => $valuetotal_result) {
                                                $MtProvinceData = MtProvince::model()->find(array(
                                                    'condition' => 'id=:id',
                                                    'params' => array(':id' => $valuetotal_result)
                                                ));
                                                if (isset($array_province[$MtProvinceData->province_code][$keyChoice][$keyArray_ans])) {
                                                  
                                                        $total_result_detail+=($array_province[$MtProvinceData->province_code][$keyChoice][$keyArray_ans]*$array_processingByProvince[$MtProvinceData->province_code]["n"]/$array_processingByProvince[$MtProvinceData->province_code]["N"]/$array_processingByProvince[$MtProvinceData->province_code]["M"]); ?>
                                                    <?php  
                                                }
                                            } 

                                            ?><td><?=sprintf("%.2f",$total_result_detail/count($MtProvinceDataSort))?></td> 
                                        <?php } 
                                            foreach ($Master_province_array as $keyMaster_province_array => $valueMaster_province_array) {
                                            // $total_result_detail = 0;
                                            foreach ($valueMaster_province_array as $keytotal_result => $valuetotal_result) {
                                                $MtProvinceData = MtProvince::model()->find(array(
                                                    'condition' => 'id=:id',
                                                    'params' => array(':id' => $valuetotal_result)
                                                ));
                                                if (isset($array_province[$MtProvinceData->province_code][$keyChoice][$keyArray_ans])) { 
                                                    ?>
                                                    
                                                      <td><?=sprintf("%.2f",$array_province[$MtProvinceData->province_code][$keyChoice][$keyArray_ans]*$array_processingByProvince[$MtProvinceData->province_code]["n"]/$array_processingByProvince[$MtProvinceData->province_code]["N"]/$array_processingByProvince[$MtProvinceData->province_code]["M"])?></td> 
                                               <?php }else{ ?>
                                                        <td>0</td> 
                                               <?php }
                                            }

                                        }
                                        ?>
                                    </tr>
                                <?php }
                            }
                        }
                    }    
                    ?>
                </tbody>
        </table>