<form class="container-fluid" method="POST" action="<?php echo Yii::app()->baseurl.'/admin/survey/'.$_GET['id'].'/question'; ?>">
    <script type="text/javascript">

        function selectMasterData() {
            const selectBox = document.getElementById("selectType");
            const selectedValue = selectBox.options[selectBox.selectedIndex].value;
            const targetBlock = document.getElementById("selectDataBlock");
            const targetForm = document.getElementById("selectData");
            if(selectedValue == 'master'){
                targetBlock.style.display = "flex";
                
            }else{
                targetBlock.style.display = "none";
            }
            const targetOptionsBlock = document.getElementById("selectOptions");
            const targetOptionsSelectBlock = document.getElementById("selectOptionsSum");
            const targetOptionsSumnum = document.getElementById("selectOptionsSumnum");
            const targetOptionsCalFuncBlock = document.getElementById("selectOptionsCalFunc");
            const targetOptionsFuncBlock = document.getElementById("selectOptionsFunc");
            const targetOptionsQuesnum = document.getElementById("selectOptionsQuesnum");
            if([
                'assign',
                'sum-input',
                'ques-ans'
                ]
                .includes(selectedValue)
                ){
                targetOptionsBlock.style.display = "flex";
        }else{
            targetOptionsBlock.style.display = "none";
        }

        if (['output'].includes(selectedValue)) {
        targetOptionsSelectBlock.style.display = "flex";
        }else{
            targetOptionsSelectBlock.style.display = "none";
        }

        if (['function'].includes(selectedValue)) {
            targetOptionsCalFuncBlock.style.display = "flex";
        }else{
            targetOptionsCalFuncBlock.style.display = "none";
        }

        if (['sum-output'].includes(selectedValue)) {
            targetOptionsFuncBlock.style.display = "flex";
        }else{
            targetOptionsFuncBlock.style.display = "none";
        }

        if (['sum-output-number'].includes(selectedValue)) {
            targetOptionsSumnum.style.display = "flex";
        }else{
            targetOptionsSumnum.style.display = "none";
        }

        if([
                'parent-ques-num',
                'parent-ques-value'
                ]
                .includes(selectedValue)
                ) {
            targetOptionsQuesnum.style.display = "flex";
        }else{
            targetOptionsQuesnum.style.display = "none";
        }

    }

    </script>
    <!-- start row -->
    <div class="row">
        <div class="col-lg-12 set-height set-padding">
            <div id="section1">
                <div class="card">
                    <div class="card-header">
                        <h5 class="mb-1">เพิ่มคำถาม</h5>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <div>
                            <input type="hidden" name="question_group" value="<?php echo $_GET['id']; ?>">
                            <div class="row">
                                <div class="col-md-8 mb-3">
                                    <label>ชื่อคำถาม</label>
                                    <div class="row col-12">
                                        <input class="form-control col-9" type="text" name="question">
                                        <button type="button" class="btn col-1" onclick="$(`input[name='question']`).val(() => { return $(`input[name='question']`).val() + '<b></b>'})"><B>B</B></button>
                                        <button type="button" class="btn col-1" onclick="$(`input[name='question']`).val(() => { return $(`input[name='question']`).val() + '<i></i>'})"><i>i</i></button>
                                        <button type="button" class="btn col-1" onclick="$(`input[name='question']`).val(() => { return $(`input[name='question']`).val() + '<red></red>'})"><red>A</red></button>
                                        <button type="button" class="btn col-1" onclick="$(`input[name='question']`).val(() => { return $(`input[name='question']`).val() + '<u></u>'})"><u>U</u></button>
                                    </div>
                                </div>
                                <div class="col-md-4 mb-3">
                                    <label>เลขที่คำถาม</label>
                                    <input class="form-control" type="number" name="question_number">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <label>ประเภทคำถาม</label>
                                    <select class="form-control" name="type" id="selectType" onchange="selectMasterData();">
                                        <option value="text">ตัวอักษร</option>
                                        <option value="number">ตัวเลข</option>
                                        <option value="float">ทศนิยม</option>
                                        <option value="master" >ข้อมูลอ้างอิง</option>
                                        <option value="note">หมายเหตุ</option>
                                        <option value="line">เส้นกั้น</option>
                                        <option value="bottom-line">ขีดเส้นใต้</option>
                                        <option value="header">หัวข้อย่อย</option>
                                        <option value="assign">กำหนดค่าเข้าตัวแปร</option>
                                        <option value="output">แสดงผลคำนวน</option>
                                        <option value="function">คำนวนและแสดงผลฟังก์ชั่น</option>
                                        <option value="sum-input">รับผลรวม</option>
                                        <option value="sum-output">แสดงผลรวมฟังก์ชั่น</option>
                                        <option value="sum-output-number">แสดงผลรวมตัวเลข</option>
                                        <option value="bullet">หัวข้อที่มีสัญลักษณ์</option>
                                        <option value="ques-ans">คำถาม-คำตอบ</option>
                                        <option value="ques-num-value">คำถาม (จำนวนชิ้น/มูลค่า)</option>
                                        <option value="parent-ques-num">จำนวนชิ้น</option>
                                        <option value="parent-ques-value">มูลค่า</option>
                                        <option value="input-product">ผลผลิตและหน่วย</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row" id="selectDataBlock" style="display:none;">
                                <div class="col-md-6 mb-3">
                                    <label>ข้อมูลที่ต้องการอ้างอิง</label>
                                    <select class="form-control" name="master" id="selectData">
                                        <option hidden value="">ยังไม่เลือก</option>
                                        <?php
                                         $name_th = "";
                                            foreach($masterlist as $row){
                                                switch ($row['name']) {
                                                    case 'county':
                                                        $name_th = 'เขต';
                                                        break;
                                                    case 'example':
                                                        $name_th = 'ตัวอย่าง';
                                                        break;
                                                    case 'group':
                                                        $name_th = 'พวกที่';
                                                        break;
                                                    case 'province':
                                                        $name_th = 'จังหวัด';
                                                        break;
                                                    case 'district':
                                                        $name_th = 'อำเภอ';
                                                        break;
                                                    case 'subdistrict':
                                                        $name_th = 'ตำบล';
                                                        break;
                                                    case 'village':
                                                        $name_th = 'หมู่บ้าน';
                                                        break;
                                                    case 'plant':
                                                        $name_th = 'พืชไร่และเลี้ยงสัตว์';
                                                        break;
                                                    default:
                                                        # code...
                                                        break;
                                                }
                                                echo '<option value="'.$row['name'].'">'.$name_th.'</option>';
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row" id="selectOptions" style="display:none;">
                                <div class="col-md-6 mb-3">
                                    <label>ข้อมูลเพิ่มเติม เช่น ตัวแปร หรือ หัวข้อรอง</label>
                                    <input class="form-control" name="options">
                                </div>
                            </div>

                            <div class="row" id="selectOptionsCalFunc" style="display:none;">
                            <div class="col-md-6 mb-3">
                                <label>ตัวแปรที่ใช้ในการคำนวณ</label>
                                <?php $options = FormModel::questionList($_GET['id']); ?>
                                <select class="form-control" name="options">
                                    <?php foreach ($options as $key => $value) {
                                    if (in_array($value['type'], ["number","float"])) { ?>
                                        <option value="<?=$value["question_id"]?>"><?=$value["question"]?></option>
                                     <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                            <div class="row" id="selectOptionsSum" style="display:none;">
                                <div class="col-md-6 mb-3">
                                    <label>ตัวแปรที่ใช้ในการคำนวณ</label>
                                    <?php $options = FormModel::questionList($_GET['id']); ?>
                                    <select class="form-control" multiple name="optionsselect[]">
                                        <?php foreach ($options as $key => $value) {
                                            if (in_array($value['type'], ["number","float"])) { ?>
                                                <option value="<?=$value["question_id"]?>"><?=$value["question"]?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>


                            <div class="row" id="selectOptionsFunc" style="display:none;">
                                <div class="col-md-6 mb-3">
                                    <label>ตัวแปรที่ใช้ในการคำนวณ</label>
                                    <?php $options = FormModel::questionList($_GET['id']); ?>
                                    <select class="form-control" multiple name="optionsselect[]">
                                        <?php foreach ($options as $key => $value) {
                                            if (in_array($value['type'], ["function"])) { ?>
                                                <option value="<?=$value["question_id"]?>"><?=$value["question"]?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="row" id="selectOptionsQuesnum" style="display:none;">
                            <div class="col-md-6 mb-3">
                                <label>ตัวแปรที่ใช้ในการคำนวณ</label>
                                <?php $options = FormModel::questionList($_GET['id']); ?>
                                <select class="form-control" name="options">
                                    <?php foreach ($options as $key => $value) {
                                    if (in_array($value['type'], ["ques-num-value","parent-ques-num"])) { ?>
                                        <option value="<?=$value["question_id"]?>"><?=$value["question"]?></option>
                                     <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div class="row" id="selectOptionsSumnum" style="display:none;">
                            <div class="col-md-6 mb-3">
                                <label>ตัวแปรที่ใช้ในการคำนวณ</label>
                                <?php $options = FormModel::questionList($_GET['id']); ?>
                                <select class="form-control" multiple name="optionsselect[]">
                                    <?php foreach ($options as $key => $value) {
                                    if (in_array($value['type'], ["ques-ans","number","float"])) { ?>
                                        <option value="<?=$value["question_id"]?>"><?=$value["question"]?></option>
                                     <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                            <!-- <div class="row">
                                <div class="col-md-6 mb-3">
                                    <label>รหัสคำตอบที่ต้องกรอก</label>
                                    <input class="form-control" type="text" name="answer">
                                </div>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="row text-center group-submit">
                <input type="submit" class="btn btn-primary btn-lg" value="บันทึกข้อมูล" />
                <button type="button" class="btn btn-outline-secondary btn-lg" onclick="window.history.go(-1); return false;">ยกเลิก</button>
            </div>
        </div>
    </div>
    <!-- end row -->
</form>