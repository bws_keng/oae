<?php

class FormulasController extends Controller
{
	public $layout = 'admin';

    public function init()
    {
        if(!Yii::app()->user->id ){
            $this->redirect(Yii::app()->baseurl.'/user/login?admin=1');
        }else{
            $user = User::model()->findByPk(Yii::app()->user->id);
            if($user->superuser != 1){
                $this->redirect(Yii::app()->baseurl);
            }
        }
    }

	public function actionIndex()
	{
		$criteria = new CDbCriteria;
		$criteria->addCondition('question_group_number != 11');
		$criteria->order = 'question_group_id ASC';
		$data = QuestionGroup::model()->findAll($criteria);
		$this->render('index', Array(
			'data' => $data,
		));
	}

	public function actionEdit($id)
	{
		$criteria = new CDbCriteria;
		$criteria->compare('question_group_id', $id);
		$criteria->order = 'formulas_id ASC';
		$data = QuestionFormulas::model()->findAll($criteria);
		$formtext = QuestionGroup::model()->findByPk($id);
		if(isset($_POST['QuestionFormulas']))
    	{
			foreach ($_POST['QuestionFormulas'] as $key => $value) {
				$criteria = new CDbCriteria;
				$criteria->compare('question_group_id', $_POST["QuestionFormulasGroup"]);
				$criteria->compare('formulas_id', $key);
				$Dataupdate = QuestionFormulas::model()->find($criteria);
				if (!empty($Dataupdate)) {
					$Dataupdate->question_id = $value;
					$Dataupdate->save();
					$this->redirect(array('index'));
				}
			}
		}
		if (empty($data)) {
			$this->redirect(array('index'));
		}
		$this->render('edit', Array(
			'data' => $data,
			'formtext' => $formtext,
		));
	}

}