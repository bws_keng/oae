<form action="<?php echo Yii::app()->baseUrl . "/survey/" . $_GET['id'] .'/'. $_GET['survey_id'].'/edit'; ?>" method="POST">
    <div class="container-fluid">
        <!-- start row -->
        <div class="row">
            <div class="col-lg-12 set-height set-padding">
                <div id="section1">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="mb-1">
                                <?php echo $title; ?>
                            </h5>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">

                            <form>
                                <div class="row">
                                    <div class="col-md-2 mb-3">
                                        <label for="">จังหวัด</label>
                                        <input class="form-control" name="maindata-province" type="text" placeholder=""
                                            required="" title="" value="<?php echo $data['questionaire']['province']; ?>" readonly>
                                    </div>
                                    <div class="col-md-2 mb-3">
                                        <label for="">อำเภอ</label>
                                        <input class="form-control" name="maindata-district" type="text" placeholder=""
                                            required="" title="" value="<?php echo $data['questionaire']['district']; ?>" readonly>
                                    </div>
                                    <div class="col-md-2 mb-3">
                                        <label for="">ตำบล</label>
                                        <div class="input-group">
                                            <input class="form-control" name="maindata-sub-district" type="text"
                                                placeholder="" aria-describedby="" required="" title="" value="<?php echo $data['questionaire']['subdistrict']; ?>" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-2 mb-3">
                                        <label for="">หมู่ที่</label>
                                        <div class="input-group">
                                            <input class="form-control" name="maindata-village-no" type="text"
                                                placeholder="" aria-describedby="" required="" title="" value="<?php echo $data['questionaire']['village']; ?>" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-2 mb-3">
                                        <label for="">พวกที่</label>
                                        <input class="form-control" name="maindata-group" type="text" placeholder=""
                                            required="" title="" value="<?php echo $data['questionaire']['group']; ?>" readonly>
                                    </div>
                                    <div class="col-md-2 mb-3">
                                        <label for="">ตัวอย่างที่</label>
                                        <input class="form-control" name="maindata-sample" type="text" placeholder=""
                                            required="" title="" value="<?php echo $data['questionaire']['sample']; ?>" readonly>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>

                <div id="section2">
                    <?php 
                        if(isset($answer)){
                            ?>
                            <h5 class="mb-4"> &nbsp;
                                <button class="btn btn-lg btn-secondary btn-note float-right mb-2" type="button" data-toggle="modal" data-target=".bd-example-modal-lg"><i class="fa fa-list"></i> &nbsp;รายการ</button>
                                <div class="modal fade bd-example-modal-lg modal-main" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h6 class="modal-title" id="myLargeModalLabel">รายการ</h6>
                                                <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="row">
                                                    <?php
                                                    foreach ($answer as $key => $value) {
                                                        ?>
                                                            <div class="col-md-6">
                                                                <div class="card">
                                                                    <div class="card-header">
                                                                        <label for="">(<?php echo $value['readable_id']; ?>) <?php echo $value['title']; ?></label>
                                                                    </div>
                                                                    <div class="card-body">
                                                                        <div class="row">
                                                                            <?php
                                                                                $answer_bullet = json_decode($value['description_json']);
                                                                                foreach ($answer_bullet as $item_key => $item) {
                                                                                    echo '
                                                                                        <div class="col-md-12">
                                                                                            <small for="">'.$item.'</small>
                                                                                        </div>
                                                                                    ';
                                                                                }
                                                                            ?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        <?php
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </h5>
                            <?php
                        }
                    ?>
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <?php
                                $items = FormModel::generate($_GET['id']);
                                // foreach ($items as $item){
                                //     switch ($item['type']) {
                                //         case 'master':
                                //             echo
                                //             '<div class="row col-md-'.$item['size'].' mb-3">
                                //                 <div class="col-md-4 mb-3">
                                //                     <label for="">'.$item['question'].'</label>
                                //                     <input class="form-control " id="master-1" type="text" name="question-'.$item['question_id'].'"  data-master="'.$item['masterdata'].'" '.($item['isRequired']? 'required': '').' value="'.$data['value'][$item['question_id']].'">
                                //                 </div>
                        
                                //                 <div class="col-md-8 mb-3">
                                //                     <label for="">ข้อมูลที่อ่านได้</label>
                                //                     <input class="form-control" id="master-1-out" type="text" disabled="disabled">
                                //                 </div>
                                                
                                //                 <div class="col-12" role="alert" style="display:none;" id="master-1-alert">
                                //                     <p class="text-danger">ข้อมูลผิดพลาด</p>
                                //                 </div>
                                //             </div>
                                //             ';
                                //             break;
                                //         default:
                                //             echo
                                //             '<div class="col-md-'.$item['size'].' mb-3">
                                //                 <label for="">'.$item['question'].'</label>
                                //                 <input class="form-control" name="question-'.$item['question_id'].'" type="text" placeholder="" required="" data-original-title="" title="" '.($item['isRequired']? 'required': '').' value="'.$data['value'][$item['question_id']].'">
                                //             </div>';
                                //             break;
                                //     }
                                // }
                                foreach ($items as $item){
                                    $options = $item['options'];
                                    switch ($item['type']) {
                                        case 'master':
                                            echo '<div class="row m-0 col-md-'.$item['size'].' mb-3">';
                        
                                                if($options != null){
                                                    echo '<label class="col-md-6 pl-0" for=""> '.$item['question'];
                                                }else{
                                                    echo '<label class="col-md-12 p-0" for=""> '.$item['question'];
                                                }
                                                if($item['readable_id'] != '' && $item['readable_id'] != NULL){
                                                    echo ' <red>('.$item['readable_id'].')</red>';
                                                }
                                                echo $item['isRequired'] ? " <span class='required'>*</span>" : "";
                                                echo '</label>';
                                                
                                                if($options != null){
                                                    echo '<label class="col-md-6 pr-0" for=""> '. $options .'</label>';
                                                }
                                                if (isset($data['value'][$item['question_id']])) {
                                                   $question_id = $data['value'][$item['question_id']];
                                                }else{
                                                   $question_id = ""; 
                                                }
    
                                                echo '<div class="col-md-6 mb-3 pl-0">
                                                    <input class="form-control data-master" id="master-'.$item['question_id'].'" type="text" name="question-'.$item['question_id'].'"  data-master="'.$item['masterdata'].'" '.($item['isRequired']? 'required': '').'  value="'.$question_id.'">
                                                </div>
                                                <div class="col-md-6 mb-3 pr-0">
                                                    <input class="form-control" id="master-'.$item['question_id'].'-out" type="text" disabled="disabled">
                                                </div>
                                                
                                                <div class="col-12" role="alert" style="display:none;" id="master-'.$item['question_id'].'-alert">
                                                    <p class="text-danger">ข้อมูลผิดพลาด</p>
                                                </div>
                                            </div>
                                            ';
                                            break;
                                        case 'note':
                                            echo
                                            '<div class="col-12">
                                                <hr>
                                                <h6><small class="text-danger">หมายเหตุ : '.$item['question'].'</small></h6>
                                            </div>
                                            ';
                                            break;
                                        case 'header':
                                            echo '
                                            <div class="col-12">
                                                <hr>
                                                <h6 class="text-head mb-3"><i class="icon-pencil-alt"></i> '.$item['question'].'</h6>
                                            </div>
                                            ';
                                            break;
                                        case 'ques-num-value':
                                            echo
                                            '<div class="col-md-'.$item['size'].' mb-3">
                                                <label for=""> '.$item['question'];
                                                if($item['readable_id'] != '' && $item['readable_id'] != NULL){
                                                    echo ' <red>('.$item['readable_id'].')</red>';
                                                }
                                                echo $item['isRequired'] ? " <span class='required'>*</span>" : "";
                                                echo '</label>
                                                <input class="form-control" name="question-'.$item['question_id'].'" type="int" placeholder=""  data-original-title="" title="" '.($item['isRequired']? 'required': '').'>
                                            </div>';
                                            break;
                                        case 'line':
                                            echo '
                                            <div class="col-12">
                                                <hr />
                                            </div>
                                            ';
                                            break;
                                        case 'function':
                                            echo '<div class="row m-0 col-md-'.$item['size'].' mb-3">';
                                                echo '<label class="col-md-12 p-0" for=""> '.$item['question'];
                                                echo $item['isRequired'] ? " <span class='required'>*</span>" : "";
                                                echo '</label>';
                                                if (isset($data['value'][$item['question_id']])) {
                                                   $question_id = $data['value'][$item['question_id']];
                                                }else{
                                                   $question_id = ""; 
                                                }
                                                echo '<div class="col-md-6 mb-3 pl-0">
                                                    <input class="form-control " id="calculate-input-'.$item['question_id'].'" type="float" name="question-'.$item['question_id'].'" data-function=\''.$item['options'].'\' '.($item['isRequired']? 'required': '').'  value="'.$question_id.'">
                                                </div>
                                                <div class="col-md-6 mb-3 pl-0">
                                                    <input class="form-control " id="calculate-output-'.$item['question_id'].'" type="float" name="question-'.$item['question_id'].'" '.($item['isRequired']? 'required': '').' value="" disabled>
                                                </div>
                                            </div>
                                            ';
                                            break;
                                        case 'assign':
                                            echo '<div class="row m-0 col-md-'.$item['size'].' mb-3">';
                                                echo '<label class="col-md-12 p-0" for=""> '.$item['question'] .'</label>';
                                                if (isset($data['value'][$item['question_id']])) {
                                                   $question_id = $data['value'][$item['question_id']];
                                                }else{
                                                   $question_id = ""; 
                                                }
                                                echo '<div class="col-md-12 mb-3 pl-0">
                                                    <input class="form-control " id="assign-'.$item['question_id'].'" type="text" name="question-'.$item['question_id'].'" data-assign="'.$item['options'].'" '.($item['isRequired']? 'required': '').'  value="'.$question_id.'">
                                                </div>
                                            </div>
                                            ';
                                            break;
                                        case 'output':
                                            echo '<div class="row m-0 col-md-'.$item['size'].' mb-3">';
                                                echo '<label class="col-md-12 p-0" for=""> '.$item['question'] .'</label>';
                                                if (isset($data['value'][$item['question_id']])) {
                                                   $question_id = $data['value'][$item['question_id']];
                                                }else{
                                                   $question_id = ""; 
                                                }
                                                echo '<div class="col-md-12 mb-3 pl-0">
                                                    <input class="form-control " id="output-'.$item['question_id'].'" type="text" name="question-'.$item['question_id'].'" value="'.$question_id.'" data-function=\''.$item['options'].'\' '.($item['isRequired']? 'required': '').' readonly value=0>
                                                </div>
                                            </div>
                                            ';
                                            $array_options = json_decode($item["options"]);
                                            echo '<script type="text/javascript">';
                                            
                                            foreach ($array_options as $key => $value) {
                                                echo '
                                                $( "#question-sum-'.$value.'" ).keyup(function() {';
                                                echo "
                                                var sum".$item['question_id']." = 0
                                                ";
                                                foreach ($array_options as $keysub => $valuesub) {
                                                    echo '
                                                    if(($( "#question-sum-'.$valuesub.'" ).val()).replaceAll(",", "") > 0){
                                                        if(sum'.$item['question_id'].' == 0){';
                                                            echo 'sum'.$item['question_id'].' = parseFloat(($( "#question-sum-'.$valuesub.'" ).val()).replaceAll(",", ""));';
                                                        echo '}else{';
                                                            echo 'sum'.$item['question_id'].' = sum'.$item['question_id'].' * parseFloat(($( "#question-sum-'.$valuesub.'" ).val()).replaceAll(",", ""));';
                                                        echo '}';
                                                    echo "}
                                                    ";
                                                }

                                                echo '
                                                  $("#output-'.$item['question_id'].'").val(parseFloat(sum'.$item['question_id'].').toLocaleString());
                                                  ';
                                                echo '});';
                                            }

                                            foreach ($array_options as $key => $value) {
                                                echo '
                                                function Changequestionsum'.$value.'() {';
                                                echo "
                                                var sum".$item['question_id']." = 0
                                                ";
                                                foreach ($array_options as $keysub => $valuesub) {
                                                    echo '
                                                    if(($( "#question-sum-'.$valuesub.'" ).val()).replaceAll(",", "") > 0){
                                                        if(sum'.$item['question_id'].' == 0){';
                                                            echo 'sum'.$item['question_id'].' = parseFloat(($( "#question-sum-'.$valuesub.'" ).val()).replaceAll(",", ""));';
                                                        echo '}else{';
                                                            echo 'sum'.$item['question_id'].' = sum'.$item['question_id'].' * parseFloat(($( "#question-sum-'.$valuesub.'" ).val()).replaceAll(",", ""));';
                                                        echo '}';
                                                    echo "}
                                                    ";
                                                }
                                                echo '
                                                  $("#output-'.$item['question_id'].'").val(parseFloat(sum'.$item['question_id'].').toLocaleString());
                                                  ';
                                                echo '};';
                                            }
                                            echo "
                                            $( document ).ready(function() {";
                                            foreach ($array_options as $key => $value) {
                                                echo "
                                                Changequestionsum".$value."();";
                                            }
                                            echo "
                                        });";
                                            echo "</script>";
                                            break;
                                        case 'number':
                                            echo
                                            '<div class="col-md-'.$item['size'].' mb-3">
                                                <label for=""> '.$item['question'];
                                                if($item['readable_id'] != '' && $item['readable_id'] != NULL){
                                                    echo ' <red>('.$item['readable_id'].')</red>';
                                                }
                                                if (isset($data['value'][$item['question_id']])) {
                                                   $question_id = $data['value'][$item['question_id']];
                                                }else{
                                                   $question_id = ""; 
                                                }
                                                echo $item['isRequired'] ? " <span class='required'>*</span>" : "";
                                                echo '</label>
                                                <input class="form-control" name="question-'.$item['question_id'].'" id="question-sum-'.$item['question_id'].'" type="float" placeholder=""  data-original-title="" title="" '.($item['isRequired']? 'required': '').' value="'.$question_id.'">
                                            </div>';
                                            break;
                                        case 'float':
                                            echo
                                            '<div class="col-md-'.$item['size'].' mb-3">
                                            <label for=""> '.$item['question'];
                                            if($item['readable_id'] != '' && $item['readable_id'] != NULL){
                                                echo ' <red>('.$item['readable_id'].')</red>';
                                            }
                                            echo $item['isRequired'] ? " <span class='required'>*</span>" : "";
                                            if (isset($data['value'][$item['question_id']])) {
                                                $question_id = $data['value'][$item['question_id']];
                                            }else{
                                                $question_id = ""; 
                                            }
                                            echo '</label>
                                            <input class="form-control" name="question-'.$item['question_id'].'" id="question-sum-'.$item['question_id'].'" type="'.$item['type'].'" placeholder="" value="'.$question_id.'"  data-original-title="" title="" '.($item['isRequired']? 'required': '').'>
                                            </div>';
                                            break;
                                        case 'sum-input':
                                            echo '<div class="row m-0 col-md-'.$item['size'].' mb-3">';
                                                echo '<label class="col-md-12 p-0" for=""> '.$item['question'] .'</label>';
                                                if (isset($data['value'][$item['question_id']])) {
                                                    $question_id = $data['value'][$item['question_id']];
                                                }else{
                                                    $question_id = ""; 
                                                }
                                                echo '<div class="col-md-12 mb-3 pl-0">
                                                    <input class="form-control " id="sum-input-'.$item['question_id'].'" type="text" name="sum-input-'.$item['question_id'].'" data-variable=\''.$item['options'].'\' '.($item['isRequired']? 'required': '').'  value="'.$question_id.'">
                                                </div>
                                            </div>
                                            ';
                                            break;
                                        case 'sum-output':
                                            $data_question_id =  isset($data['value'][$item['question_id']]) ? $data['value'][$item['question_id']] : 0;
                                            echo '<div class="row m-0 col-md-'.$item['size'].' mb-3">';
                                                echo '<label class="col-md-12 p-0" for=""> '.$item['question'] .'</label>';
                                                echo '<div class="col-md-12 mb-3 pl-0">
                                                    <input class="form-control " id="sum-output-'.$item['question_id'].'" type="text" name="sum-output-'.$item['question_id'].'" value="'.$data_question_id.'" data-variable=\''.$item['options'].'\' '.($item['isRequired']? 'required': '').' readonly>
                                                </div>
                                            </div>
                                            ';
                                            $array_options = json_decode($item["options"]);
                                            echo '<script type="text/javascript">';
                                            foreach ($array_options as $key => $value) {
                                                echo '
                                                $( "#calculate-input-'.$value.'" ).change(function() {';
                                                echo "
                                                var sum".$item['question_id']." = 0
                                                ";
                                                foreach ($array_options as $keysub => $valuesub) {
                                                    echo '
                                                    if(($( "#calculate-output-'.$valuesub.'" ).val()).replaceAll(",", "") > 0){
                                                        ';
                                                    echo 'sum'.$item['question_id'].' += parseFloat(($( "#calculate-output-'.$valuesub.'" ).val()).replaceAll(",", ""));';
                                                    echo "}
                                                    ";
                                                }
                                                echo '
                                                $("#sum-output-'.$item['question_id'].'").val(parseFloat(sum'.$item['question_id'].').toLocaleString());
                                                ';
                                                echo '});';
                                            }

                                            foreach ($array_options as $key => $value) {
                                                echo '
                                               function Changecalculateinput'.$value.'() {';
                                                echo "
                                                var sum".$item['question_id']." = 0
                                                ";
                                                foreach ($array_options as $keysub => $valuesub) {
                                                    echo '
                                                    if(($( "#calculate-output-'.$valuesub.'" ).val()).replaceAll(",", "") > 0){
                                                        ';
                                                    echo 'sum'.$item['question_id'].' += parseFloat(($( "#calculate-output-'.$valuesub.'" ).val()).replaceAll(",", ""));';
                                                    echo "}
                                                    ";
                                                }
                                                echo '
                                                  $("#sum-output-'.$item['question_id'].'").val(parseFloat(sum'.$item['question_id'].').toLocaleString());
                                                  ';
                                                echo '};';
                                            }
                                            echo "
                                            $( document ).ready(function() {";
                                            foreach ($array_options as $key => $value) {
                                                echo "
                                                Changecalculateinput".$value."();";
                                            }
                                            echo "
                                        });";
                                            echo "</script>";
                                            
                                            break;
                                        case 'ques-ans':
                                            echo '<div class="row m-0 col-md-'.$item['size'].' mb-3">';
                                                echo '<label class="col-md-6 p-0" for=""> '.$item['question'] ;
                                                if($item['readable_id'] != '' && $item['readable_id'] != NULL){
                                                    echo ' <red>('.$item['readable_id'].')</red>';
                                                }
                                                echo '</label>';;
                                                echo '<label class="col-md-6 p-0" for=""> '.$item['options'] .'</label>';
                                                echo '<div class="col-md-6 mb-3 pl-0">
                                                </div>';
                                                echo '<div class="col-md-6 mb-3 pl-0">
                                                    <input class="form-control" name="question-'.$item['question_id'].'" id="question-sum-'.$item['question_id'].'" type="float" placeholder="" value="'.$data['value'][$item['question_id']].'"  data-original-title="" title="" '.($item['isRequired']? 'required': '').'>
                                                </div>';
                                            echo '</div>
                                            ';
                                            break;
                                        case 'input-product':
                                            $inputquestion = $data['value'][$item['question_id']];
                                            echo '<div class="row m-0 col-md-'.$item['size'].' mb-3">';
                                                echo '<label class="col-md-12 p-0" for=""> '.$item['question'] ;
                                                if($item['readable_id'] != '' && $item['readable_id'] != NULL){
                                                    echo ' <red>('.$item['readable_id'].')</red>';
                                                }
                                                echo $item['isRequired'] ? " <span class='required'>*</span>" : "";
                                                echo '</label>';;
                                                echo '<div class="col-md-12 mb-3 pl-0">
                                                    <input class="form-control" name="question-'.$item['question_id'].'" id="question-sum-'.$item['question_id'].'" type="float" placeholder="" value='.$inputquestion.'  data-original-title="" title="" '.($item['isRequired']? 'required': '').'>
                                                </div>';
                                                // echo '<div class="col-md-5 mb-3 pl-0">
                                                //     <input class="form-control" name="inputquestionparent-'.$item['question_id'].'" id="question-sum-parent-'.$item['question_id'].'" type="text" placeholder="" value='.$inputquestionparent.'  data-original-title="" title="" '.($item['isRequired']? 'required': '').'>
                                                // </div>';
                                            echo '</div>
                                            ';
                                            break;
                                        case 'sum-output-number':
                                            echo '<div class="row m-0 col-md-'.$item['size'].' mb-3">';
                                                echo '<label class="col-md-6 p-0" for=""> '.$item['question'] .'</label>';
                                                if (isset($data['value'][$item['question_id']])) {
                                                   $question_id = $data['value'][$item['question_id']];
                                                }else{
                                                   $question_id = ""; 
                                                }
                                                echo '<div class="col-md-6 mb-3 pl-0">
                                                    <input class="form-control " id="sum-output-number-'.$item['question_id'].'" type="text" name="sum-output-number-'.$item['question_id'].'" value="'.$question_id.'" data-variable=\''.$item['options'].'\' '.($item['isRequired']? 'required': '').' readonly>
                                                </div>
                                            </div>
                                            ';
                                            $array_options = json_decode($item["options"]);
                                            echo '<script type="text/javascript">';
                                            if ($array_options != null) {
                                                foreach ($array_options as $key => $value) {
                                                    echo '
                                                    $( "#question-sum-'.$value.'" ).keyup(function() {';
                                                    echo "
                                                    var sum".$item['question_id']." = 0
                                                    ";
                                                    foreach ($array_options as $keysub => $valuesub) {
                                                        echo '
                                                        if($( "#question-sum-'.$valuesub.'" ).val() > 0){
                                                            ';
                                                            echo 'sum'.$item['question_id'].' += parseFloat($( "#question-sum-'.$valuesub.'" ).val());';
                                                            echo "}
                                                            ";
                                                        }
                                                        echo '
                                                        $("#sum-output-number-'.$item['question_id'].'").val(sum'.$item['question_id'].');
                                                        ';
                                                        echo '});';
                                                    }
                                                }

                                            echo "</script>";
                                            break;
                                        case 'text':
                                            echo
                                            '<div class="col-md-'.$item['size'].' mb-3">
                                                <label for=""> '.$item['question'];
                                                if($item['readable_id'] != '' && $item['readable_id'] != NULL){
                                                    echo ' <red>('.$item['readable_id'].')</red>';
                                                }
                                            //     echo '</label>
                                            //     <div class="row col-12">
                                            //         <input class="form-control col-10" name="question-'.$item['question_id'].'" type="'.$item['type'].'" placeholder=""  data-original-title="" title="" '.($item['isRequired']? 'required': '').' value="'.$data['value'][$item['question_id']].'">
                                            //         <div class="row col-2">
                                            //             <button type="button" class="btn col-4" onclick="$(`input[name=\'question-'.$item['question_id'].'\']`).val(() => { return $(`input[name=\'question-'.$item['question_id'].'\']`).val() + \'<b></b>\'})"><B>B</B></button>
                                            //             <button type="button" class="btn col-4" onclick="$(`input[name=\'question-'.$item['question_id'].'\']`).val(() => { return $(`input[name=\'question-'.$item['question_id'].'\']`).val() + \'<i></i>\'})"><i>i</i></button>
                                            //             <button type="button" class="btn col-4" onclick="$(`input[name=\'question-'.$item['question_id'].'\']`).val(() => { return $(`input[name=\'question-'.$item['question_id'].'\']`).val() + \'<red></red>\'})"><red>A</red></button>
                                            //         </div>
                                            //     </div>
                                            // </div>';
                                                if (isset($data['value'][$item['question_id']])) {
                                                   $question_id = $data['value'][$item['question_id']];
                                                }else{
                                                   $question_id = ""; 
                                                }
                                                echo '</label>
                                                <div class="row col-12">
                                                    <input class="form-control col-10" name="question-'.$item['question_id'].'" type="'.$item['type'].'" placeholder=""  data-original-title="" title="" '.($item['isRequired']? 'required': '').' value="'.$question_id.'">
                                                </div>
                                            </div>';
                                            break;
                                        case 'parent-ques-num':
                                            echo
                                            '<div style="margin-left: 30px;" class="col-md-'.$item['size'].' mb-3">
                                                <label for=""> '.$item['question'];
                                                if($item['readable_id'] != '' && $item['readable_id'] != NULL){
                                                    echo ' <red>('.$item['readable_id'].')</red>';
                                                }
                                                if (isset($data['value'][$item['question_id']])) {
                                                   $question_id = $data['value'][$item['question_id']];
                                                }else{
                                                   $question_id = ""; 
                                                }
                                                echo '</label>
                                                <div class="row col-12">
                                                    <input class="form-control" name="question-'.$item['question_id'].'" type="number" placeholder="" value="'.$question_id.'"  data-original-title="" title="" '.($item['isRequired']? 'required': '').' >
                                                </div>
                                            </div>';
                                            break;
                                        case 'parent-ques-value':
                                            echo
                                            '<div class="col-md-'.$item['size'].' mb-3">
                                                <label for=""> '.$item['question'];
                                                if($item['readable_id'] != '' && $item['readable_id'] != NULL){
                                                    echo ' <red>('.$item['readable_id'].')</red>';
                                                }
                                                if (isset($data['value'][$item['question_id']])) {
                                                   $question_id = $data['value'][$item['question_id']];
                                                }else{
                                                   $question_id = ""; 
                                                }
                                                echo '</label>
                                                <div class="row col-12">
                                                    <input class="form-control" name="question-'.$item['question_id'].'" type="float" placeholder="" value="'.$question_id.'"  data-original-title="" title="" '.($item['isRequired']? 'required': '').' >
                                                </div>
                                            </div>';
                                            break;
                                        default:
                                              $data_question_id =  isset($data['value'][$item['question_id']]) ? $data['value'][$item['question_id']] : NULL;
                                            echo
                                            '<div class="col-md-'.$item['size'].' mb-3">
                                                <label for=""> '.$item['question'];
                                                if($item['readable_id'] != '' && $item['readable_id'] != NULL){
                                                    echo ' <red>('.$item['readable_id'].')</red>';
                                                }
                                                 echo '</label>
                                                 <input class="form-control" name="question-'.$item['question_id'].'" type="'.$item['type'].'" placeholder=""  data-original-title="" title="" '.($item['isRequired']? 'required': '').'  value="'.$data_question_id.'">
                                             </div>';
                                            break;
                                    }
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row text-center group-submit">
                    <input type="submit" type="button" class="btn btn-primary btn-lg" value="บันทึกข้อมูล" />
                    <button type="button" class="btn btn-outline-secondary btn-lg" onclick="window.history.go(-1); return false;">ยกเลิก</button>
                </div>
            </div>
        </div>
        <!-- end row -->

    </div>
</form>
<script type="text/javascript">
    $( document ).ready(function() {
        Search_Plant();
    });

    function Search_Plant(){
        var Plant = $(".data-master").attr('data-master');
        if (Plant == 'plant') {
            var id = $(".data-master").attr('id');
            var code = $(".data-master").val();
            if (code != null) {
                $.ajax({
                type: "POST",
                url: "<?=$this->createUrl("/survey/SearchPlant");?>",
                data: { code:code },
                success: function(data){
      
                      if (data != null) {
                         $("#"+id+"-out").val(data);
                      }
                    },
                });
            }
        }
    }
</script>