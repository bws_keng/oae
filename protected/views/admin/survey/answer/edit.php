<form class="container-fluid" method="POST"
    action="<?php echo Yii::app()->baseurl.'/admin/survey/'.$_GET['id'].'/answer/'.$_GET['answer_id'].'/edit'; ?>">
    <div class="container-fluid">

        <!-- start row -->
        <div class="row">
            <div class="col-lg-12 set-height set-padding">
                <div id="section1">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="mb-1">แก้ไชคำตอบ</h5>
                            <?php
                                // print_r($data[0]);
                            ?>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <form>
                                <div class="row">
                                    <div class="col-md-12 mb-3">
                                        <label for="">คำถาม</label>
                                        <select class="form-control digits" id="" name="question" required>
                                            <?php
                                            foreach($survey as $key => $row){
                                                echo '<option value="'.$row['question_id'].'" '.($row['question_id'] == $data[0]['question_id']? ' selected': '').'>'.$row['question'].'</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 mb-3">
                                        <!-- &nbsp &nbsp &nbsp <input class="form-check-input" type="checkbox" value=""> -->
                                        <label for="">หัวข้อคำตอบ</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 mb-3">
                                        <div class="row col-12">
                                            <input class="form-control col-8" type="text" value="<?php echo $data[0]['title']; ?>" name="title">
                                            <button type="button" class="btn col-1" onclick="$(`input[name='subtitle']`).val(() => { return $(`input[name='subtitle']`).val() + '<b></b>'})"><B>B</B></button>
                                            <button type="button" class="btn col-1" onclick="$(`input[name='subtitle']`).val(() => { return $(`input[name='subtitle']`).val() + '<i></i>'})"><i>i</i></button>
                                            <button type="button" class="btn col-1" onclick="$(`input[name='subtitle']`).val(() => { return $(`input[name='subtitle']`).val() + '<red></red>'})"><red>A</red></button>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 mb-3" id="answer">
                                        <label for="">คำตอบ</label>
                                        <?php
                                        foreach (json_decode($data[0]['description_json']) as $key => $value) {
                                            echo '<input class="form-control" type="text" name="description[]" value="'.$value.'">';
                                        }
                                        ?>
                                        
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 mb-3"><br>
                                            <label for=""></label>
                                            <button type="button" class="btn" onclick='insertInput("answer", "description");'>
                                                <i data-feather="plus" style="color:#298FDE;cursor:pointer;"
                                                    onclick=""></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="row text-center group-submit">
                    <input type="submit" class="btn btn-primary btn-lg" value="บันทึกข้อมูล" />
                    <button type="button" class="btn btn-outline-secondary btn-lg"
                        onclick="window.history.go(-1); return false;">ยกเลิก</button>
                </div>
            </div>
        </div>
        <!-- end row -->

    </div>

</form>