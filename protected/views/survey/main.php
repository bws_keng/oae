<div class="container-fluid">
    <form action="<?php echo Yii::app()->baseUrl . "/survey/" . $_GET['id']; ?>" method="POST">
    <!-- start row -->
        <div class="row">
            <div class="col-lg-12 set-height set-padding">
                <div id="section1">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="mb-1">แบบสอบถามภาวะเศรษฐกิจสังคมครัวเรือนและแรงงานเกษตร </h5>
                            <p class="mb-0">ระหว่างวันที่ 1 พฤษภาคม 2562 - 30 เมษายน 2563</p>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <h4 class="mb-4 text-main"><i class="icofont icofont-ui-user"></i> เจ้าหน้าที่สำรวจ</h4>
                            <hr>
                            <div>
                                <div class="row">
                                    <div class="col-md-4 mb-3">
                                        <label for="">ชื่อ</label>
                                        <input class="form-control" id="" type="text" placeholder="" required=""
                                            data-original-title="" title="">
                                    </div>
                                    <div class="col-md-4 mb-3">
                                        <label for="">นามสกุล</label>
                                        <input class="form-control" id="" type="text" placeholder="" required=""
                                            data-original-title="" title="">
                                    </div>
                                    <div class="col-md-4 mb-3">
                                        <label for="">รหัส</label>
                                        <div class="input-group">
                                            <input class="form-control" id="" type="text" placeholder=""
                                                aria-describedby="inputGroupPrepend2" required=""
                                                data-original-title="" title="">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 mb-3">
                                        <label for="">วัน/เดือน/ปี</label>
                                        <input class="form-control digits" type="date" value="">
                                        <!-- <div class="date-picker">
                                   <label for="validationDefault03">วัน/เดือน/ปี</label>
                                   <div class="input-group">
                                       <input class="datepicker-here form-control digits" type="text" data-language="th">
                                   </div>
                                   </div>
                               </div> -->
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div id="section2">
                    <div class="card">
                        <div class="card-body">
                            <div>
                                <div class="row acc-1">
                                    <div class="col-md-8 mb-3">
                                        <label for=""><strong class="text-main">1.</strong> สำนักงานเศรษฐกิจการเกษตรที่
                                        </label>
                                        <input class="form-control" id="" type="text" disabled="disabled"
                                            placeholder="" required="" data-original-title="" title="">
                                    </div>
                                    <div class="col-md-4 mb-3">
                                        <label for="">สศท. <small class="text-danger">(เลข 2 หลัก)</small></label>
                                        <input class="form-control " id="" maxlength="2" type="text" placeholder=""
                                            required="" data-original-title="" title="">
                                    </div>
                                </div>

                                <div class="row acc-1">
                                    <div class="col-md-8 mb-3">
                                        <label for=""><strong class="text-main">2.</strong> จังหวัด </label>
                                        <input class="form-control" id="" type="text" disabled="disabled"
                                            placeholder="" required="" data-original-title="" title="">
                                    </div>
                                    <div class="col-md-4 mb-3">
                                        <label for="">จังหวัด <small class="text-danger">(เลข 3 หลัก)</small></label>
                                        <input class="form-control " id="" maxlength="2" type="text" placeholder=""
                                            required="" data-original-title="" title="">
                                    </div>
                                </div>

                                <div class="row acc-1">
                                    <div class="col-md-8 mb-3">
                                        <label for=""><strong class="text-main">3.</strong> อำเภอ </label>
                                        <input class="form-control " id="" type="text" disabled="disabled"
                                            placeholder="" required="" data-original-title="" title="">
                                    </div>
                                    <div class="col-md-4 mb-3">
                                        <label for="">อำเภอ <small class="text-danger">(เลข 2 หลัก)</small></label>
                                        <input class="form-control" id="" maxlength="2" type="text" placeholder=""
                                            required="" data-original-title="" title="">
                                    </div>
                                </div>

                                <div class="row acc-1">
                                    <div class="col-md-8 mb-3">
                                        <label for=""><strong class="text-main">4.</strong> ตำบล </label>
                                        <input class="form-control" id="" type="text" disabled="disabled"
                                            placeholder="" required="" data-original-title="" title="">
                                    </div>
                                    <div class="col-md-4 mb-3">
                                        <label for="">ตำบล <small class="text-danger">(เลข 2 หลัก)</small></label>
                                        <input class="form-control" id="" maxlength="2" type="text" placeholder=""
                                            required="" data-original-title="" title="">
                                    </div>
                                </div>

                                <div class="row acc-1">
                                    <div class="col-md-4 mb-3">
                                        <label for=""><strong class="text-main">5.</strong> หมู่บ้าน </label>
                                        <input class="form-control" id="" type="text" disabled="disabled"
                                            placeholder="" required="" data-original-title="" title="">
                                    </div>
                                    <div class="col-md-4 mb-3">
                                        <label for=""> บ้านเลขที่ </label>
                                        <input class="form-control" id="" type="text" disabled="disabled"
                                            placeholder="" required="" data-original-title="" title="">
                                    </div>
                                    <div class="col-md-4 mb-3">
                                        <label for="">หมู่ที่ <small class="text-danger">(เลข 2 หลัก)</small></label>
                                        <input class="form-control text-center" maxlength="2" id="" type="text"
                                            placeholder="" required="" data-original-title="" title="">
                                    </div>
                                </div>

                                <div class="row acc-1">
                                    <div class="col-md-8 mb-3">
                                        <label for=""><strong class="text-main">6.</strong> พวกที่ </label>
                                        <input class="form-control" id="" type="text" disabled="disabled"
                                            placeholder="" required="" data-original-title="" title="">
                                    </div>
                                    <div class="col-md-4 mb-3">
                                        <label for="">พวกที่ <small class="text-danger">(เลข 1 หลัก)</small></label>
                                        <input class="form-control" id="" maxlength="1" type="text" placeholder=""
                                            required="" data-original-title="" title="">
                                    </div>
                                </div>

                                <div class="row acc-1">
                                    <div class="col-md-12">
                                        <label><strong class="text-main">7.</strong> ชื่อผู้ขึ้นทะเบียนเกษตรกร </label>
                                    </div>
                                    <div class="col-md-4 mb-3">
                                        <label for="">ชื่อ</label>
                                        <input class="form-control" id="" type="text" placeholder="" required=""
                                            data-original-title="" title="">
                                    </div>
                                    <div class="col-md-4 mb-3">
                                        <label for="">นามสกุล </label>
                                        <input class="form-control" id="" type="text" placeholder="" required=""
                                            data-original-title="" title="">
                                    </div>

                                    <div class="col-md-4 mb-3">
                                        <label for="">ตัวอย่างที่ <small class="text-danger">(เลข 1
                                                หลัก)</small></label>
                                        <input class="form-control" id="" maxlength="1" type="text" placeholder=""
                                            required="" data-original-title="" title="">
                                    </div>

                                    <div class="col-md-8 mb-3">
                                        <label for=""> เลขบัตรประจำตัวประชาชน </label>
                                        <input class="form-control" id="" type="text" placeholder="" required=""
                                            data-original-title="" title="">
                                    </div>

                                    <div class="col-md-4 mb-3">
                                        <label for="">ผู้ขึ้นทะเบียนเกษตร <small class="text-danger">มี=1/
                                                ไม่มี=2</small></label>
                                        <input class="form-control" id="" maxlength="1" type="text" placeholder=""
                                            required="" data-original-title="" title="">
                                    </div>
                                </div>

                                <div class="row acc-1">
                                    <div class="col-md-12">
                                        <label><strong class="text-main">8.</strong> ชื่อผู้ให้ข้อมูล </label>
                                    </div>
                                    <div class="col-md-4 ">
                                        <label for="">ชื่อ</label>
                                        <input class="form-control" id="" type="text" placeholder="" required=""
                                            data-original-title="" title="">
                                    </div>
                                    <div class="col-md-4 ">
                                        <label for="">นามสกุล </label>
                                        <input class="form-control" id="" type="text" placeholder="" required=""
                                            data-original-title="" title="">
                                    </div>
                                    <div class="col-md-4 ">
                                        <div class="form-group">
                                            <label for="">ความสัมพันธ์กับหัวหน้าครัวเรือน</label>
                                            <select class="form-control digits" id="">
                                                <option selected disabled>เลือกความสัมพันธ์</option>
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                                <option>4</option>
                                                <option>5</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-8 mb-3">
                                        <label for=""> เลขบัตรประจำตัวประชาชน </label>
                                        <input class="form-control" id="" type="text" placeholder="" required=""
                                            data-original-title="" title="">
                                    </div>

                                    <div class="col-md-4 mb-3">
                                        <label for="">เบอร์ติดต่อ </label>
                                        <input class="form-control" id="" maxlength="10" type="text" placeholder=""
                                            required="" data-original-title="" title="">
                                    </div>
                                </div>

                                <div class="form-group row acc-1">
                                    <label class="col-md-8 col-form-label"><strong class="text-main">9.</strong>
                                        ครัวเรือนเกษตรมีที่ดินทำการเกษตรอย่างน้อย 1 แปลง อยู่ในเขตชลประทาน <small
                                            class="text-danger">(มี=1 / ไม่มี=2)</small></label>
                                    <div class="col-md-2">
                                        <input class="form-control text-center" maxlength="1" type="text"
                                            data-original-title="" title="">
                                    </div>
                                </div>

                                <div class="row acc-1">
                                    <div class="col-md-12 mb-0">
                                        <label for=""><strong class="text-main">10.</strong>
                                            การมีส่วนร่วมในโครงการหรือนโยบายของภาครัฐ ให้ตอบทุกข้อ (มีส่วนร่วม = 1 /
                                            ไม่มีส่วนร่วม = 2) </label>
                                    </div>

                                    <div class="row acc-2">
                                        <label class="col-md-8 col-form-label">
                                            <p class="mb-0">1) ศพก. </p>
                                            <small
                                                class="text-danger">ศูนย์เรียนรู้การเพิ่มประสิทธิภาพการผลิตสินค้าเกษตร</small>
                                        </label>
                                        <div class="col-md-2">
                                            <input class="form-control text-center mt-3" maxlength="1" type="text"
                                                data-original-title="" title="">
                                        </div>

                                        <label class="col-md-8 col-form-label">
                                            <p class="mb-0">2) แปลงใหญ่</p>
                                            <small
                                                class="text-danger">การรวมกลุ่มกันเพื่อลดต้นทุนและเพิ่มผลผลิตทางการเกษตร</small>
                                        </label>
                                        <div class="col-md-2">
                                            <input class="form-control text-center mt-3" maxlength="1" type="text"
                                                data-original-title="" title="">
                                        </div>

                                        <label class="col-md-8 col-form-label">
                                            <p class="mb-0">3) การบริหารจัดการน้ำ</p>
                                            <small class="text-danger">โครงการชลประทานในพื้นที่
                                                ทั้งขนาดใหญ่/กลาง/เล็ก/รวมถึงแหล่งน้ำชุมชน แหล่งน้ำในไร่นา
                                                (บ่อจิ๋ว)</small>
                                        </label>
                                        <div class="col-md-2">
                                            <input class="form-control text-center mt-3" maxlength="1" type="text"
                                                data-original-title="" title="">
                                        </div>

                                        <label class="col-md-8 col-form-label">
                                            <p class="mb-0">4) แผนการผลิตข้าวครบวงจร</p>
                                            <small
                                                class="text-danger">การส่งเสริมและสนับสนุนการปลูกข้าวแก่เกษตรกรและวางแผนการตลาดข้าวแบบครบวงจร</small>
                                        </label>
                                        <div class="col-md-2">
                                            <input class="form-control text-center mt-3" maxlength="1" type="text"
                                                data-original-title="" title="">
                                        </div>

                                        <label class="col-md-8 col-form-label">
                                            <p class="mb-0">5) Zoning by Agri-Map</p>
                                            <small
                                                class="text-danger">การสนับสนุนและส่งเสริมให้เกษตรกรปรับเปลี่ยนการผลิตในพื้นที่ไม่เหมาะสมเป็นการผลิตที่เหมาะสม</small>
                                        </label>
                                        <div class="col-md-2">
                                            <input class="form-control text-center mt-3" maxlength="1" type="text"
                                                data-original-title="" title="">
                                        </div>

                                        <label class="col-md-8 col-form-label">
                                            <p class="mb-0">6) ธนาคารสินค้าเกษตร</p>
                                            <small
                                                class="text-danger">การสนับสนุนช่วยเหลือให้เกษตรกรเข้าถึงและใช้ประโยชน์จากปัจจัยการผลิตเช่น
                                                โค ปุ๋ย เมล็ดพันธุ์</small>
                                        </label>
                                        <div class="col-md-2">
                                            <input class="form-control text-center mt-3" maxlength="1" type="text"
                                                data-original-title="" title="">
                                        </div>

                                        <label class="col-md-8 col-form-label">
                                            <p class="mb-0">7) มาตรฐานสินค้าเกษตร GAP/เกษตรอินทรีย์</p>
                                            <small
                                                class="text-danger">การเพิ่มประสิทธิภาพการผลิตในพื้นที่เกษตรแปลงใหญ่ให้เป็นไปตามมาตรฐาน/การสนับสนุน
                                                ส่งเสริม และพัฒนาเกษตรกร ตามแนวทางเกษตรอินทรีย์</small>
                                        </label>
                                        <div class="col-md-2">
                                            <input class="form-control  text-center mt-3" maxlength="1" type="text"
                                                data-original-title="" title="">
                                        </div>

                                        <label class="col-md-8 col-form-label">
                                            <p class="mb-0">8) โครงการส่งเสริมเกษตรทฤษฎีใหม่</p>
                                            <small
                                                class="text-danger">การสนับสนุนให้เกษตรกรมีความรู้ความเข้าในการฏิิบัติตามระบบเกษตรทฤษฎีใหม่
                                                และสามารถนำไปปรับใช้เพื่อการประกอบอาชีพอย่างมั่นคงและยั่งยืน</small>
                                        </label>
                                        <div class="col-md-2">
                                            <input class="form-control text-center mt-3" maxlength="1" type="text"
                                                data-original-title="" title="">
                                        </div>
                                    </div>
                                </div>

                                <div class="row acc-1">
                                    <label class="col-sm-3 col-form-label"><i class="icon-map-alt"></i> แนบไฟล์แผนที่
                                        :</label>
                                    <div class="col-sm-4">
                                        <input class="form-control" type="file">
                                    </div>
                                </div>




                            </div>
                        </div>
                    </div>
                </div>

                <div class="row text-center group-submit">
                    <input type="submit" type="button" class="btn btn-primary btn-lg" value="บันทึกข้อมูล" />
                    <button type="button" class="btn btn-outline-secondary btn-lg">ยกเลิก</button>
                </div>
            </div>
        </div>
        <!-- end row -->
    </form>
</div>