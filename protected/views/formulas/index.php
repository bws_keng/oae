<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
 
<div class="container-fluid">

<!-- start row -->
<div class="row">
	<div class="col-lg-12 set-height set-padding">
		<div id="section1">
			<div class="card">
				<div class="card-header">
					<h5 class="mb-1">จัดการ Report</h5>
				</div>
			</div>

			<div class="card table-card">
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-striped table-main" id="">
							<!-- <table class="table table-striped table-main" id="basic-2"> -->
							<thead>
								<tr>
									<th width="20%">หมวดที่</th>
									<th width="60%">ชือหมวด</th>
									<th width="20%">จัดการข้อมูล</th>
								</tr>
							</thead>
							<tbody>
								<?php

								$dataProvider=new CArrayDataProvider($data, array(
									'pagination'=>array(
										'pageSize'=>25
									),
								));


							// ----------------- query ------------------
                            if (!empty($data)){
                            	foreach($dataProvider->getData() as $key => $row){ 


                            		echo '<tr>
                            		<td>'.$row->question_group_number.'</td>
                            		<td>'.$row->question_group_name.'</td>
                            		<td>
                            		<a type="button" class="btn btn-icon text-success" href="'.Yii::app()->baseurl.'/Formulas/edit/'.$row->question_group_id.'"><i data-feather="edit"></i></a>
                            		</td>
                            		</tr>';
                            	}

                            }else{
                            	echo '<tr><td colspan="5">ไม่พบข้อมูล</td></tr>';
                            } ?>
							
							</tbody>
							<!-- ----------------- query ------------------  -->
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- end row -->
<div class="container position-fixed fixed-top pt-5" id="deleteForm" style="display:none;">
	<div class="card col-12 p-0">
		<div class="card-header">
			<h5>ลบข้อมูล</h5>
		</div>
<div class="card-body">
	<h5 class="card-title">เมื่อข้อมูลถูกลบจะไม่สามารถย้อนคืนได้</h5>
		<div class="card-text">
			<form method="POST" action="<?php echo Yii::app()->baseurl; ?>/questionnaire/delete">
				<label for="deleteKey">รหัสอ้างอิง</label>
				<input id="deleteKey" name="key" class="form-control" value="" readonly/>
				<label for="deleteValue">ข้อมูล</label>
				<input id="deleteValue" name="value" class="form-control" value="" readonly/>
				<button class="btn btn-danger btn-lg mt-2">ลบข้อมูล</button>
				<button type="button" class="btn btn-light btn-lg mt-2" onclick='$("#deleteForm").hide();'>ยกเลิก</button>
			</form>
		</div>
	</div>
</div>

</div>
</div>

<script type="text/javascript">
	function confirmDelete(id)
	{
		swal({
			title: "ต้องการลบหรือไม่",
			icon: "warning",
			buttons: true,
			dangerMode: true,
		})
		.then((willDelete) => {
			if (willDelete) {
				$.ajax({
					url: "<?php echo $this->createUrl('/MtProvince/delete/'); ?>",
					type: "GET",
					data: {id: id},
					dataType: "html",
					success: function () {
						swal({
							title: "ลบข้อมูลสำเร็จ", 
							icon: "success", 
						});
						location.reload(true);
						window.location.href = "/oae/index.php/mtProvince/index";
					}
				});
			} else {
				swal({
					title: "ยกเลิกการลบข้อมูล",
  					icon: "error",
				});
			}
		});
	}
</script>


