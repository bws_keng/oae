<form class="container-fluid" method="POST" action="<?php echo Yii::app()->baseurl.'/admin/survey/'.$_GET['id'].'/group'; ?>">
    <div class="container-fluid">

        <!-- start row -->
        <div class="row">
            <div class="col-lg-12 set-height set-padding">
                <div id="section1">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="mb-1">เพิ่มข้อมูลหมวดหมู่</h5>
                        </div>
                    </div>

                    <div class="card table-card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <div class="form-group">
                                        <label for="">ปี พ.ศ. แบบสอบถาม</label>
                                        <select class="form-control digits " id="" name="survey" required>
                                            <option selected disabled></option>
                                            <?php
                                            $survey = QuestionnaireModel::surveyList();
                                        foreach($survey as $key => $row){
                                            echo '<option value="'.$row['id'].'">'.$row['year'].' - '.$row['name'].'</option>';
                                        }
                                        ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-12 mb-3">
                                    <label for="">ฟอร์มคำถามที่ใช้ในหมวดหมู่</label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-6 m-t-15 mb-0">
                                    <div class="checkbox checkbox-solid-white">
                                        <input id="all" type="checkbox" >
                                        <label for="all">ทั้งหมด</label>
                                    </div>
                                </div>
                                <div class="form-group col-md-6 m-t-15 mb-0">
                                    <div class="checkbox checkbox-solid-white">
                                        <input id="all-isrequired" type="checkbox" >
                                        <label for="all-isrequired">บังคับทั้งหมด</label>
                                    </div>
                                </div>
                            </div>
                            <?php
                        foreach($questions as $key => $row){
                            echo '<div class="row">
                                <div class="form-group col-md-6 m-t-15 mb-0">
                                    <div class="checkbox checkbox-solid-white">
                                        <input id="solid-'.$key.'" type="checkbox" name="question-'.$row['question_id'].'" value="false">
                                        <label for="solid-'.$key.'">'.$row['question'].'</label>
                                    </div>
                                </div>
                                <div class="form-group col-md-6 m-t-15 mb-0">
                                    <div class="checkbox checkbox-solid-white">
                                        <input id="solid-'.$key.'-isrequired" type="checkbox">
                                        <label for="solid-'.$key.'-isrequired">บังคับ</label>
                                    </div>
                                </div>
                            </div>
                        ';
                        }
                    ?>


                        </div>


                    </div>

                </div>

            </div>
        </div>
        <!-- end row -->
        <div class="row text-center group-submit">
            <input type="submit" class="btn btn-primary btn-lg" value="บันทึกข้อมูล">
            <button type="button" class="btn btn-outline-secondary btn-lg" onclick="window.history.go(-1); return false;">ยกเลิก</button>
        </div>
    </div>
</form>