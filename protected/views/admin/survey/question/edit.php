<form class="container-fluid" method="POST" action="<?php echo Yii::app()->baseurl.'/admin/survey/'.$_GET['id'].'/question/'.$_GET['question_id'].'/edit'; ?>">


    <!-- start row -->
    <div class="row">
        <div class="col-lg-12 set-height set-padding">
            <div id="section1">
                <div class="card">
                    <div class="card-header">
                        <h5 class="mb-1">แก้ไขคำถาม</h5>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <div>
                            <input type="hidden" name="question_group" value="<?php echo $_GET['id']; ?>">
                            <div class="row">
                                <div class="col-md-8 mb-3">
                                    <label>ชื่อคำถาม</label>
                                    <div class="row col-12">
                                        <input class="form-control col-9" type="text" name="question" value="<?php echo $data[0]['question']; ?>">
                                        <button type="button" class="btn col-1" onclick="$(`input[name='question']`).val(() => { return $(`input[name='question']`).val() + '<b></b>'})"><B>B</B></button>
                                        <button type="button" class="btn col-1" onclick="$(`input[name='question']`).val(() => { return $(`input[name='question']`).val() + '<i></i>'})"><i>i</i></button>
                                        <button type="button" class="btn col-1" onclick="$(`input[name='question']`).val(() => { return $(`input[name='question']`).val() + '<red></red>'})"><red>A</red></button>
                                        <button type="button" class="btn col-1" onclick="$(`input[name='question']`).val(() => { return $(`input[name='question']`).val() + '<u></u>'})"><u>U</u></button>
                                    </div>
                                </div>
                                <div class="col-md-4 mb-3">
                                    <label>เลขที่คำถาม</label>
                                    <input class="form-control" type="number" name="question_number" value="<?php echo $data[0]['readable_id']; ?>">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <label>ประเภทคำถาม</label>
                                    <select class="form-control" name="type" id="selectType" onchange="selectMasterData();">
                                        <?php
                                        switch ($data[0]['type']) {
                                            case 'text':
                                            echo '<option value="text" hidden>ตัวอักษร</option>';
                                            break;
                                            case 'number':
                                            echo '<option value="number" hidden>ตัวเลข</option>';
                                            break;
                                            case 'float':
                                            echo '<option value="float" hidden>ทศนิยม</option>';
                                            break;
                                            case 'master':
                                            echo '<option value="master" hidden>ข้อมูลอ้างอิง</option>';
                                            break;
                                            case 'note':
                                            echo '<option value="note" hidden>หมายเหตุ</option>';
                                            break;
                                            case 'header':
                                            echo '<option value="header" hidden>หัวข้อย่อย</option>';
                                            break;
                                            case 'line':
                                            echo '<option value="line" hidden>เส้นกั้น</option>';
                                            break;
                                            case 'assign':
                                            echo '<option value="assign" hidden>กำหนดค่าเข้าตัวแปร</option>';
                                            break;
                                            case 'output':
                                            echo '<option value="output" hidden>แสดงผลคำนวน</option>';
                                            break;
                                            case 'function':
                                            echo '<option value="function" hidden>คำนวนและแสดงผลฟังก์ชั่น</option>';
                                            break;
                                            case 'sum-input':
                                            echo '<option value="sum-input" hidden>รับผลรวม</option>';
                                            break;
                                            case 'sum-output':
                                            echo '<option value="sum-output" hidden>แสดงผลรวมฟังก์ชั่น</option>';
                                            break;
                                            case 'bottom-line':
                                            echo '<option value="bottom-line" hidden>ขีดเส้นใต้</option>';
                                            break;
                                            case 'bullet':
                                            echo '<option value="bullet" hidden>หัวข้อที่มีสัญลักษณ์</option>';
                                            break;
                                            case 'ques-ans':
                                            echo '<option value="ques-ans" hidden>คำถาม-คำตอบ</option>';
                                            break;
                                            case 'ques-num-value':
                                            echo '<option value="ques-num-value" hidden>คำถาม-คำตอบ</option>';
                                            break;
                                            case 'parent-ques-num':
                                            echo '<option value="parent-ques-num" hidden>จำนวนชิ้น</option>';
                                            break;
                                            case 'parent-ques-value':
                                            echo '<option value="parent-ques-value" hidden>มูลค่า</option>';
                                            break;
                                            default:
                                            echo '<option value="'.$data[0]['type'].'" hidden>'.$data[0]['type'].'</option>';
                                            break;
                                        }
                                        ?>
                                        <option <?=$data[0]['type'] =="text" ? "selected" : "" ?> value="text">ตัวอักษร</option>
                                        <option <?=$data[0]['type'] =="number" ? "selected" : "" ?> value="number">ตัวเลข</option>
                                        <option <?=$data[0]['type'] =="float" ? "selected" : "" ?> value="float">ทศนิยม</option>
                                        <option <?=$data[0]['type'] =="master" ? "selected" : "" ?> value="master" >ข้อมูลอ้างอิง</option>
                                        <option <?=$data[0]['type'] =="note" ? "selected" : "" ?> value="note">หมายเหตุ</option>
                                        <option <?=$data[0]['type'] =="line" ? "selected" : "" ?> value="line">เส้นกั้น</option>
                                        <option <?=$data[0]['type'] =="bottom-line" ? "selected" : "" ?> value="bottom-line">ขีดเส้นใต้</option>
                                        <option <?=$data[0]['type'] =="header" ? "selected" : "" ?> value="header">หัวข้อย่อย</option>
                                        <option <?=$data[0]['type'] =="assign" ? "selected" : "" ?> value="assign">กำหนดค่าเข้าตัวแปร</option>
                                        <option <?=$data[0]['type'] =="output" ? "selected" : "" ?> value="output">แสดงผลคำนวน</option>
                                        <option <?=$data[0]['type'] =="function" ? "selected" : "" ?> value="function">คำนวนและแสดงผลฟังก์ชั่น</option>
                                        <option <?=$data[0]['type'] =="sum-input" ? "selected" : "" ?> value="sum-input">รับผลรวม</option>
                                        <option <?=$data[0]['type'] =="sum-output" ? "selected" : "" ?> value="sum-output">แสดงผลรวมฟังก์ชั่น</option>
                                        <option <?=$data[0]['type'] =="sum-output-number" ? "selected" : "" ?> value="sum-output-number">แสดงผลรวมตัวเลข</option>
                                        <option <?=$data[0]['type'] =="bullet" ? "selected" : "" ?> value="bullet">หัวข้อที่มีสัญลักษณ์</option>
                                        <option <?=$data[0]['type'] =="ques-ans" ? "selected" : "" ?> value="ques-ans">คำถาม-คำตอบ</option>
                                        <option <?=$data[0]['type'] =="ques-num-value" ? "selected" : "" ?> value="ques-num-value">คำถาม (จำนวนชิ้น/มูลค่า)</option>
                                        <option <?=$data[0]['type'] =="parent-ques-num" ? "selected" : "" ?> value="parent-ques-num">จำนวนชิ้น</option>
                                        <option <?=$data[0]['type'] =="parent-ques-value" ? "selected" : "" ?> value="parent-ques-value">มูลค่า</option>
                                        <option <?=$data[0]['type'] =="input-product" ? "selected" : "" ?> value="input-product">ผลผลิตและหน่วย</option>

                                    </select>
                                </div>
                            </div>
                            <!-- <div class="row" id="selectDataBlock" style="display:<?php echo ($data[0]['masterdata'] == null? 'none' : $data[0]['masterdata']); ?>;">
                                <div class="col-md-6 mb-3">
                                    <label>ข้อมูลที่ต้องการอ้างอิง</label>
                                    <select class="form-control" name="master" id="selectData">
                                        <?php 
                                        $name_th = "";
                                        if($data[0]['masterdata'] != null){
                                            switch ($data[0]['masterdata']) {
                                                    case 'county':
                                                        $name_th = 'เขต';
                                                        break;
                                                    case 'example':
                                                        $name_th = 'ตัวอย่าง';
                                                        break;
                                                    case 'group':
                                                        $name_th = 'พวกที่';
                                                        break;
                                                    case 'province':
                                                        $name_th = 'จังหวัด';
                                                        break;
                                                    case 'district':
                                                        $name_th = 'อำเภอ';
                                                        break;
                                                    case 'subdistrict':
                                                        $name_th = 'ตำบล';
                                                        break;
                                                    case 'village':
                                                        $name_th = 'หมู่บ้าน';
                                                        break;
                                                    case 'plant':
                                                        $name_th = 'พืชไร่และเลี้ยงสัตว์';
                                                        break;
                                                    default:
                                                        # code...
                                                        break;
                                                }
                                            echo '<option hidden value="'.$data[0]['masterdata'].'">'.$name_th.'</option>';
                                        }else{
                                            echo '<option hidden value="">ยังไม่เลือก</option>';
                                        }
                                        ?>
                                        
                                        <?php
                                            $name_th = "";
                                            foreach($masterlist as $row){
                                                switch ($row['name']) {
                                                    case 'county':
                                                        $name_th = 'เขต';
                                                        break;
                                                    case 'example':
                                                        $name_th = 'ตัวอย่าง';
                                                        break;
                                                    case 'group':
                                                        $name_th = 'พวกที่';
                                                        break;
                                                    case 'province':
                                                        $name_th = 'จังหวัด';
                                                        break;
                                                    case 'district':
                                                        $name_th = 'อำเภอ';
                                                        break;
                                                    case 'subdistrict':
                                                        $name_th = 'ตำบล';
                                                        break;
                                                    case 'village':
                                                        $name_th = 'หมู่บ้าน';
                                                        break;
                                                    case 'plant':
                                                        $name_th = 'พืชไร่และเลี้ยงสัตว์';
                                                        break;
                                                    default:
                                                        # code...
                                                        break;
                                                }
                                                echo '<option value="'.$row['name'].'">'.$name_th.'</option>';
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                             <div class="row">
                                <div class="col-md-6 mb-3">
                                    <label>รหัสคำตอบที่ต้องกรอก</label>
                                    <input class="form-control" type="text" name="answer">
                                </div>
                            </div> 
                        </div> -->
                        <div class="row" id="selectDataBlock" style="display:none;">
                            <div class="col-md-6 mb-3">
                                <label>ข้อมูลที่ต้องการอ้างอิง</label>
                                <select class="form-control" name="master" id="selectData">
                                    <option hidden value="">ยังไม่เลือก</option>
                                    <?php
                                    $name_th = "";
                                    foreach($masterlist as $row){
                                        switch ($row['name']) {
                                            case 'county':
                                            $name_th = 'เขต';
                                            break;
                                            case 'example':
                                            $name_th = 'ตัวอย่าง';
                                            break;
                                            case 'group':
                                            $name_th = 'พวกที่';
                                            break;
                                            case 'province':
                                            $name_th = 'จังหวัด';
                                            break;
                                            case 'district':
                                            $name_th = 'อำเภอ';
                                            break;
                                            case 'subdistrict':
                                            $name_th = 'ตำบล';
                                            break;
                                            case 'village':
                                            $name_th = 'หมู่บ้าน';
                                            break;
                                            case 'plant':
                                            $name_th = 'พืชไร่และเลี้ยงสัตว์';
                                            break;
                                            default:
                                                        # code...
                                            break;
                                        }
                                        echo '<option value="'.$row['name'].'">'.$name_th.'</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="row" id="selectOptions" style="display:none;">
                            <div class="col-md-6 mb-3">
                                <label>ข้อมูลเพิ่มเติม เช่น ตัวแปร หรือ หัวข้อรอง</label>
                                <input class="form-control" value="<?=$data[0]["options"]?>" name="options">
                            </div>
                        </div>

                        <div class="row" id="selectOptionsCalFunc" style="display:none;">
                            <div class="col-md-6 mb-3">
                                <label>ตัวแปรที่ใช้ในการคำนวณ</label>
                                <?php $options = FormModel::questionList($data[0]["question_group_id"]); ?>
                                <?php $array_options_selected = ($data[0]["options"]); ?>
                                <?php if (isset($array_options_selected) && $array_options_selected != "") { ?>
                                <select class="form-control" name="options">
                                    <?php foreach ($options as $key => $value) {
                                    if (in_array($value['type'], ["number","float"])) { ?>
                                        <option <?php if($data[0]["type"]=="function" && $value["question_id"] == $array_options_selected){ echo 'selected="selected"'; }?> value="<?=$value["question_id"]?>"><?=$value["question"]?></option>
                                     <?php } ?>
                                    <?php } ?>
                                </select>
                                <?php }else{ ?>
                                    <select class="form-control" name="options">
                                        <?php foreach ($options as $key => $value) {
                                            if (in_array($value['type'], ["number","float"])) { ?>
                                                <option value="<?=$value["question_id"]?>"><?=$value["question"]?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    </select>
                                <?php } ?>
                            </div>
                        </div>

                        <div class="row" id="selectOptionsSum" style="display:none;">
                            <div class="col-md-6 mb-3">
                                <label>ตัวแปรที่ใช้ในการคำนวณ</label>
                                <?php $options = FormModel::questionList($data[0]["question_group_id"]); ?>
                                <?php $array_options_selected = json_decode($data[0]["options"]); ?>
                                <?php if (isset($array_options_selected) && $array_options_selected != "") { ?>
                                <select class="form-control" multiple name="optionsselect[]">
                                    <?php foreach ($options as $key => $value) {
                                    if (in_array($value['type'], ["number","float"])) { ?>
                                        <option <?php if($data[0]["type"]=="output" && in_array($value["question_id"], $array_options_selected) ){ echo 'selected="selected"'; }?> value="<?=$value["question_id"]?>"><?=$value["question"]?></option>
                                     <?php } ?>
                                    <?php } ?>
                                </select>
                                <?php }else{ ?>
                                    <select class="form-control" multiple name="optionsselect[]">
                                        <?php foreach ($options as $key => $value) {
                                            if (in_array($value['type'], ["number","float"])) { ?>
                                                <option value="<?=$value["question_id"]?>"><?=$value["question"]?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    </select>
                                <?php } ?>
                            </div>
                        </div>

                        <div class="row" id="selectOptionsFunc" style="display:none;">
                            <div class="col-md-6 mb-3">
                                <label>ตัวแปรที่ใช้ในการคำนวณ</label>
                                <?php $options = FormModel::questionList($data[0]["question_group_id"]); ?>
                                <?php $array_options_selected = json_decode($data[0]["options"]); ?>
                                <?php if (isset($array_options_selected) && $array_options_selected != "") { ?>
                                <select class="form-control" multiple name="optionsselect[]">
                                    <?php foreach ($options as $key => $value) {
                                    if (in_array($value['type'], ["function"])) { ?>
                                        <option <?php if($data[0]["type"]=="sum-output" && in_array($value["question_id"], $array_options_selected) ){ echo 'selected="selected"'; }?> value="<?=$value["question_id"]?>"><?=$value["question"]?></option>
                                     <?php } ?>
                                    <?php } ?>
                                </select>
                                <?php }else{ ?>
                                    <select class="form-control" multiple name="optionsselect[]">
                                        <?php foreach ($options as $key => $value) {
                                            if (in_array($value['type'], ["function"])) { ?>
                                                <option value="<?=$value["question_id"]?>"><?=$value["question"]?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    </select>
                                <?php } ?>
                            </div>
                        </div>

                        <div class="row" id="selectOptionsQuesnum" style="display:none;">
                            <div class="col-md-6 mb-3">
                                <label>ตัวแปรที่ใช้ในการคำนวณ</label>
                                <?php $options = FormModel::questionList($data[0]["question_group_id"]); ?>
                                <?php $array_options_selected = $data[0]["options"]; ?>
                                <?php if (isset($array_options_selected) && $array_options_selected != "") { ?>
                                <select class="form-control" name="options">
                                    <?php foreach ($options as $key => $value) {
                                    if (in_array($value['type'], ["ques-num-value","parent-ques-num"])) { ?>
                                        <option <?php if($value["question_id"]==$array_options_selected){ echo 'selected="selected"'; }?> value="<?=$value["question_id"]?>"><?=$value["question"]?></option>
                                     <?php } ?>
                                    <?php } ?>
                                </select>
                                <?php }else{ ?>
                                    <select class="form-control" name="options">
                                        <?php foreach ($options as $key => $value) {
                                            if (in_array($value['type'], ["ques-num-value","parent-ques-num"])) { ?>
                                                <option value="<?=$value["question_id"]?>"><?=$value["question"]?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    </select>
                                <?php } ?>
                            </div>
                        </div>

                        <div class="row" id="selectOptionsSumnum" style="display:none;">
                            <div class="col-md-6 mb-3">
                                <label>ตัวแปรที่ใช้ในการคำนวณ</label>
                                <?php $options = FormModel::questionList($data[0]["question_group_id"]); ?>
                                <?php $array_options_selected = json_decode($data[0]["options"]); ?>
                                <?php if (isset($array_options_selected) && $array_options_selected != "") { ?>
                                <select class="form-control" multiple name="optionsselect[]">
                                    <?php foreach ($options as $key => $value) {
                                    if (in_array($value['type'], ["ques-ans","number","float"])) { ?>
                                        <option <?php if($data[0]["type"]=="sum-output-number" && in_array($value["question_id"], $array_options_selected) ){ echo 'selected="selected"'; }?> value="<?=$value["question_id"]?>"><?=$value["question"]?></option>
                                     <?php } ?>
                                    <?php } ?>
                                </select>
                                <?php }else{ ?>
                                    <select class="form-control" multiple name="optionsselect[]">
                                        <?php foreach ($options as $key => $value) {
                                            if (in_array($value['type'], ["ques-ans","number","float"])) { ?>
                                                <option value="<?=$value["question_id"]?>"><?=$value["question"]?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    </select>
                                <?php } ?>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
            <div class="row text-center group-submit">
                <input type="submit" class="btn btn-primary btn-lg" value="บันทึกข้อมูล" />
                <button type="button" class="btn btn-outline-secondary btn-lg" onclick="window.history.go(-1); return false;">ยกเลิก</button>
            </div>
        </div>
    </div>
    <!-- end row -->
</form>

<script type="text/javascript">
    $( document ).ready(function() {
        selectMasterData();
    });
    function selectMasterData() {
        const selectBox = document.getElementById("selectType");
        const selectedValue = selectBox.options[selectBox.selectedIndex].value;
        const targetBlock = document.getElementById("selectDataBlock");
        const targetForm = document.getElementById("selectData");
        if(selectedValue == 'master'){
            targetBlock.style.display = "flex";

        }else{
            targetBlock.style.display = "none";
        }
        const targetOptionsBlock = document.getElementById("selectOptions");
        const targetOptionsSelectBlock = document.getElementById("selectOptionsSum");
        const targetOptionsSumnum = document.getElementById("selectOptionsSumnum");
        const targetOptionsCalFuncBlock = document.getElementById("selectOptionsCalFunc");
        const targetOptionsFuncBlock = document.getElementById("selectOptionsFunc");
        const targetOptionsQuesnum = document.getElementById("selectOptionsQuesnum");

        if([
            'assign',
            'sum-input',
            'ques-ans'
            ]
            .includes(selectedValue)
            ){
            targetOptionsBlock.style.display = "flex";
    }else{
        targetOptionsBlock.style.display = "none";
    }

    if (['output'].includes(selectedValue)) {
        targetOptionsSelectBlock.style.display = "flex";
    }else{
        targetOptionsSelectBlock.style.display = "none";
    }

    if (['function'].includes(selectedValue)) {
        targetOptionsCalFuncBlock.style.display = "flex";
    }else{
        targetOptionsCalFuncBlock.style.display = "none";
    }

    if (['sum-output'].includes(selectedValue)) {
        targetOptionsFuncBlock.style.display = "flex";
    }else{
        targetOptionsFuncBlock.style.display = "none";
    }

    if (['sum-output-number'].includes(selectedValue)) {
        targetOptionsSumnum.style.display = "flex";
    }else{
        targetOptionsSumnum.style.display = "none";
    }

    if(['parent-ques-num','parent-ques-value'].includes(selectedValue)) {
        targetOptionsQuesnum.style.display = "flex";
    }else{
        targetOptionsQuesnum.style.display = "none";
    }


}

</script>