<?php

/**
 * This is the model class for table "question_formulas".
 *
 * The followings are the available columns in table 'question_formulas':
 * @property integer $formulas_id
 * @property string $formulas_name
 * @property integer $question_id
 * @property integer $question_group_id
 */
class QuestionFormulas extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'oae-survey.question_formulas';

	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('question_id, question_group_id', 'numerical', 'integerOnly'=>true),
			array('formulas_name', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('formulas_id, formulas_name, question_id, question_group_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'formulas_id' => 'Formulas',
			'formulas_name' => 'Formulas Name',
			'question_id' => 'Question',
			'question_group_id' => 'Question Group',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('formulas_id',$this->formulas_id);
		$criteria->compare('formulas_name',$this->formulas_name,true);
		$criteria->compare('question_id',$this->question_id);
		$criteria->compare('question_group_id',$this->question_group_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return QuestionFormulas the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
