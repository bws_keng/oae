<?php
$this->breadcrumbs=array(
  // UserModule::t('Users')=>array('/user'),
  UserModule::t('Manage'),
);
$formNameModel = 'User';
$title = "จัดการเจ้าหน้าที่สำนักงานเขต";

Yii::app()->clientScript->registerScript('search', "
  $('#SearchFormAjax').submit(function(){
      $.fn.yiiGridView.update('$formNameModel-grid', {
          data: $(this).serialize()
      });
      return false;
  });
");

Yii::app()->clientScript->registerScript('updateGridView', <<<EOD
  $.updateGridView = function(gridID, name, value) {
      $("#"+gridID+" input[name*="+name+"], #"+gridID+" select[name*="+name+"]").val(value);
      $.fn.yiiGridView.update(gridID, {data: $.param(
          $("#"+gridID+" input, #"+gridID+" .filters select")
      )});
  }
  $.appendFilter = function(name, varName) {
      var val = eval("$."+varName);
      $("#$formNameModel-grid").append('<input type="hidden" name="'+name+'" value="">');
  }
  $.appendFilter("User[news_per_page]", "news_per_page");
EOD
, CClientScript::POS_READY);

?>
 <script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/theme/assets/js/bootstrap-daterangepicker/jquery.datetimepicker.full.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl; ?>/theme/assets/js/bootstrap-daterangepicker/jquery.datetimepicker.css"> 
<div class="container-fluid">
  <!-- start row -->
  <div class="row">
    <div class="col-lg-12 set-height set-padding">
      <div id="section1">
        <div class="card">
          <div class="card-header">
            <h5 class="mb-1">
              <?php
                echo $title;
              ?>
            </h5>
          </div>
        </div>
        <div class="row justify-content-end">
          <div class="col-md-3">
            <a href="<?php echo Yii::app()->createUrl('authorities/create'); ?>">
              <div class="card o-hidden create-main">
                <div class="b-r-4 card-body">
                  <div class="media static-top-widget">
                    <div class="align-self-center text-center"><i data-feather="plus"></i></div>
                    <div class="media-body">
                      <h6 class="mb-0 counter">เพิ่มเจ้าหน้าที่สำนักเขต</h6><i class="icon-bg" data-feather="database"></i>
                    </div>
                  </div>
                </div>
              </div>
            </a>
          </div>
        </div>
        <div class="card table-card">
        <!--   <div class="card-header">
            <div class="row">
              <div class="col-md-3 mb-3">
                <label for="">ผืนที่</label>
                <input class="form-control" id="" maxlength="1" type="text" placeholder="" required=""
                  data-original-title="" title="">
              </div>
              <div class="col-md-3 mb-3">
                <div class="form-group">
                  <label for="">จังหวัด</label>
                  <select class="form-control digits" id="">
                    <option selected disabled>เลือกจังหวัด</option>
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                  </select>
                </div>
              </div>
              <div class="col-md-3 mb-3">
                <div class="form-group">
                  <label for="">ปี พ.ศ.</label>
                  <select class="form-control digits" id="">
                    <option selected disabled>เลือกปี พ.ศ.</option>
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                  </select>
                </div>
              </div>
              <div class="col-md-3 mb-3">
                <button class="btn btn-lg btn-search text-white" type="submit"><i
                    class="icon-search"></i>&nbsp;ค้นหา</button>
              </div>
            </div>
          </div> -->
          <div class="card-body">
            <div class="table-responsive">
              <?php $this->widget('AGridView', array(

                  'id'=>$formNameModel.'-grid',
                  'dataProvider'=>$model->search(),
                  //'filter'=>$model,
                  'selectableRows' => 2,
                  //'rowCssClassExpression'=>'"items[]_{$data->id}"',
                  'htmlOptions' => array(
                    'style'=> "margin-top: -1px;width:100%;",
                  ),
                  'afterAjaxUpdate'=>'function(id, data){
                    $.appendFilter("User[news_per_page]");
                    InitialSortTable(); 
                    }',

                  'columns'=>array(
                    array(
                      'header'=>'No.',
                      'value'=>'$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
                      'filterHtmlOptions'=>array('style'=>'width:30px;'), 
                      'htmlOptions'=>array('style'=>'text-align: center;hight:5%;'),
                    ),
      
                    array(
                      'header' => 'ชื่อ - นามสกุล',
                      'type'=>'html',
                      'value'=>function($data){
                          return $data->profiles->firstname . ' ' . $data->profiles->lastname;

                      }
                    ),

                    array(
                      'header' => 'รหัสพนักงาน',
                      // 'name'=>'username',
                      'type'=>'html',
                      'value'=>function($data){
                        return $data->profiles->staff_id;
                      },
                      'htmlOptions'=>array('style'=>'color: #2B4497;'),
                    ),
                    
                    //   array(
                    //   'header' => 'เลขประจำตัวบัตรประชาชน',
                    //   'name'=>'identification',
                    //   'type'=>'html',
                    //   'value'=>function($data){
                    //     return $data->identification;
                    //   },
                    //   'filterHtmlOptions'=>array('style'=>'width:30px'),
                    // ),
                    array(
                      'header'=>'อีเมล',
                      'type'=>'raw',
                      'value'=>'CHtml::link(UHtml::markSearch($data,"email"), "mailto:".$data->email)',
                      'filterHtmlOptions'=>array('style'=>'width:30px'),
      
                    ),
                    // array(
                    //   'name'=>'create_at',
                    //   'type'=>'html',
                    //   'filter' => false,
                    //   'value'=>function($data){
                    //     return Helpers::changeFormatDate($data->create_at,'datetime');
                    //   },
                    //   'filterHtmlOptions'=>array('style'=>'width:30px'),
                    // ),
                    // array(
                    //   'name'=>'lastvisit_at',
                    //   'type'=>'html',
                    //   'filter' => false,
                    //   'value'=>function($data){
                    //     return Helpers::changeFormatDate($data->lastvisit_at,'datetime');
                    //   },
                    //   'filterHtmlOptions'=>array('style'=>'width:30px'),
                    // ),
                    // array(
                    //   'name'=>'status',
                    //   'type'=>'raw',
                    //   'value'=>'User::itemAlias("UserStatus",$data->status)',
                    //   'filter' => User::itemAlias("UserStatus"),
                    //   'filterHtmlOptions'=>array('style'=>'width:30px'),
                    //   //'htmlOptions'=>array('style'=>'text-align: center;width:100%;'),
                    // ),
                    // array(
                    //   'name'=>'online_status',
                    //   'type'=>'raw',
                    //   'value'=>'User::chk_online($data->id,$data->lastactivity,$data->online_status)',
                    //   'filter' => User::itemAlias("Online"),
                    //   'filterHtmlOptions'=>array('style'=>'width:30px'),
                    //   //'htmlOptions'=>array('style'=>'text-align: center;width:100%;'),
                    // ),
                    
                   array(
                    'header' => 'จัดการ',
                    'class' => 'zii.widgets.grid.CButtonColumn',
                    'htmlOptions' => array('style' => 'white-space: nowrap'),
                    'afterDelete' => 'function(link,success,data) { if (success && data) alert(data); }',
                    'template' => '{view} {update} {btn_delete}',
                    'buttons' => array(
                        'view' => array(

                            'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => 'View'),
                            'label' => '<button type="button" class="btn btn-icon text-primary"><i data-feather="eye"></i></button>',
                            'imageUrl' => false,
                        ),
                        'update' => array(
                            'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => 'Update'),
                            'label' => '<button type="button" class="btn btn-icon text-success"><i data-feather="edit"></i></button>',
                            'imageUrl' => false,
                            // 'url'=> function($data) {
                            //     return Yii::app()->controller->createUrl('adminUser/update', ['id' => $data->id]);
                            // }
                        ),
                        'btn_delete' => array(
                          'click' => 'function() {if(!confirm("คุณต้องการลบช้อมูลใช่หรือไม่?")) {return false;}}',
                            'options' => array(
                                'rel' => 'tooltip', 
                                'data-toggle' => 'tooltip', 
                                'title' => 'Delete',
                                'class' => 'btn_del'
                                ), 

                               'url'=> function($data) {
                                    return Yii::app()->controller->createUrl('//authorities/delete', ['id' => $data->id]);

                                },
                            'label' => '<button type="button" class="btn btn-icon text-danger"><i data-feather="trash"></i></button>',
                           'imageUrl' => false,
                            //'Url' => false,
                            )
                        )
                    ),
                   array(
                  'header'=>'ลาออก',
                  'type'=>'raw',
                  'htmlOptions' => array(
                    'style' => 'width:100px; text-align:center;',
                  ),
                  'value'=>function($data){

                    $text = '
                    <button class="btn btn-danger" title="ลาออก" onclick="resignuser('.$data->id.')" style="background: #b60000;">ลาออก</button>
                    ';


                    return $text;
                  },


                ),
                  ),
));


?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- end row -->
</div>


  <div class="modal" tabindex="-1" role="dialog" id="modal_date_laid_off">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">วันที่ลาออก</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <label>วันที่ลาออก <font color="red">*</font></label>
          <input class="form-control default_datetimepicker" type="text" name="date_laid_off" id="date_laid_off" autocomplete="off">
        </div>
        <div class="modal-footer">
          <button id="btn_validate_confirm" type="button" class="btn btn-primary">ยืนยัน</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">ยกเลิก</button>
        </div>
      </div>
    </div>
  </div>

  
<script type="text/javascript">

  $('.default_datetimepicker').datetimepicker({
        format:'d-m-Y',
        step:5,
        timepickerScrollbar:false,
        timepicker:false
    });

  function resignuser(user_id){
      $("#btn_validate_confirm").attr("onclick","validate_resignuser("+user_id+");");
      $("#modal_date_laid_off").modal('show');

    }

    function validate_resignuser(user_id){
      $("#modal_date_laid_off").modal('hide');

      if($("#date_laid_off").val() != ""){
        confirm_resignuser(user_id);
      }else{
        alert("กรุณากรอกวันที่ลาออก !");
        $("#btn_validate_confirm").attr("onclick","");
        $("#date_laid_off").val("");
        resignuser(user_id);
      }

    }





    function confirm_resignuser(user_id){
      // console.log("888");

      // if(confirm("แน่ใจว่าจะให้บุคคลนี้ลาออกใช่ไหม")){

        if(user_id != ""){
          $.ajax({
            type: 'POST',
            url: "<?=Yii::app()->createUrl('authorities/ResignUserDel');?>",
            data:{ 
              user_id:user_id, 
              date_laid_off:$("#date_laid_off").val()
            },
            success: function(data) {
              // if(data == "success"){

              // }else{
              //   alert("ทำรายการไม่สำเร็จ กรุณาทำรายการใหม่");
              // }
               location.reload();
            }
          });
        }

        $("#date_laid_off").val("");


      // }else{
      //  console.log("no resign");
      // }
    }
</script>