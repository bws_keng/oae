<table class="table table-striped table-main" id="">
                                 <!-- <table class="table table-striped table-main" id="basic-2"> -->
                                 <thead>
                                     <tr>
                                         <th width="5%">ลำดับ</th>
                                         <th width="25%">ปี พ.ศ. แบบสอบถาม</th>
                                         <th>คำตอบ</th>
                                         <th width="25%">จัดการ</th>
                                     </tr>
                                 </thead>
                                 <tbody>
                                     <?php
                                     $i = 1;
                                    foreach($answer as $key=>$value){
                                        // print_r($value);exit();
                                        echo '<tr>
                                            <td>'.$i++.'</td>
                                            <td>';
                                        foreach($value['years'] as $year){
                                             $years = $year + 543;
                                            echo $years.' ';
                                        }
                                        
                                        echo '</td>
                                            <td>'.$value['title'].'</td>
                                            <td>
                                                <a type="button" class="btn btn-icon text-success" href="'.Yii::app()->baseurl.'/admin/survey/'.$_GET['id'].'/answer/'.$key.'/edit"><i data-feather="edit"></i></a>
                                                <button type="button" class="btn btn-icon text-danger" onclick="sendDelete('.$value['id'].', \''.$value['title'].'\')"><i data-feather="trash"></i></button>
                                            </td>
                                        </tr>';
                                    }
                                ?>
                                 </tbody>
                             </table>
<script type="text/javascript">
     $(document).ready(function(){
         feather.replace();
     });
 </script>