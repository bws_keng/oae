
<!DOCTYPE html>
<html lang="en">
<body data-spy="scroll" data-target="#myScrollspy">
  <!-- start row -->
  <div class="row">
    <div class="col-lg-12 set-height set-padding">
      <div id="section1">
        <div class="card">
          <div class="card-header">
            <h5 class="mb-1">ค่าตัวคูณขยาย</h5>
        </div>
    </div>
    <div class="card table-card">
        <div class="card-header">
          <div class="row">
            <form enctype="multipart/form-data" id="sutdent-form" method="post"
            action="<?php echo Yii::app()->baseurl.'/processing/InsertExcel'; ?>">
            <div class="span4">
                <?php $form = $this->beginWidget('AActiveForm', array(
                    'id'=>'news-form',
                    'enableClientValidation'=>true,
                    'clientOptions'=>array(
                        'validateOnSubmit'=>true
                    ),
                    'errorMessageCssClass' => 'label label-important',
                    'htmlOptions' => array('enctype' => 'multipart/form-data')
                )); ?>
                <h4>นำเข้าไฟล์ ค่าตัวคูณขยาย <label>(ไฟล์ excel เท่านั้น)</label></h4>
                
                <?php echo $form->fileField($model,'excel_file',array('class'=>'form-control','style'=>'width: 250px;')); ?>

                <?php $this->endWidget(); ?>
                <div class="form-actions m-t-20">

                    <button type="submit" class="btn btn-primary glyphicons circle_ok p-15"><i></i>นำเข้าไฟล์
                        excel
                    </button>
                    
                </div>
            </div>
        </form>
        <script type="text/javascript">
            $('#sutdent-form').submit(function () {
              
                if ($('#User_excel_file').val() == '') {
                    alert('กรุณาเลือกไฟล์ Excel');
                    return false;
                }
                return true;
            });
        </script>
    </div>
</div>
</div>
</div>
</div>
</div>


</body>

</html>
