<?php
/**
 * คลาสสำหรับการสร้างหน้าแบบสอบถาม
 */
class QuestionnaireController extends Controller
{


    public function init()
    {   
        if(!Yii::app()->user->id ){
            $this->redirect(Yii::app()->baseurl.'/user/login');
        }else{
            $criteria=new CDbCriteria;
            $criteria->compare('id',Yii::app()->user->id);
            $user = User::model()->find($criteria);
            if ($user->superuser == 1) {
                 $this->layout = "admin";
            }else{

            }
        }
    }

    private $UploadPath = 'uploads/';

    public function actionList()
    {   
        if (Yii::app()->user->id) {
            $User = User::model()->findByPk(Yii::app()->user->id);
            $profile = $User->profile;
            // if ($User->superuser != 1) {
            //     $criteria=new CDbCriteria;
            //     $criteria->compare('office_id',$profile->county_id);
            //     $Questionnaire = Questionnaire::model()->findAll($criteria);
            // }else{
            //     $Questionnaire = Questionnaire::model()->findAll();
            // }
                $Questionnaire = Questionnaire::model()->findAll();

        }
        if (!empty($Questionnaire)) { 

                $year_array = array();
                $Province_array = array();
                foreach ($Questionnaire as $key => $value) {
                 $year_array[] = $value->survey_id;
                 $Province_array[] = $value->province;
             }
             $criteria=new CDbCriteria;
             $criteria->addIncondition('id',$year_array);
             $Survey = Survey::model()->findAll($criteria);

             $criteria=new CDbCriteria;
             $criteria->compare('active',1);
             $criteria->addIncondition('province_code',$Province_array);
             $criteria->order = "province_name_th ASC";
             $MtProvince = MtProvince::model()->findAll($criteria); 

             $Province_array = array();
             foreach ($MtProvince as $key => $value) {
              $Province_array[] = $value->id;
            }

                  $criteria=new CDbCriteria;
                  $criteria->compare('active',1);
                  $criteria->addIncondition('province_id',$Province_array);
                  $MtDistrict = MtDistrict::model()->findAll($criteria);

                  $district_array = array();
                  foreach ($MtDistrict as $key => $value) {
                   $district_array[] = $value->id;
                } 

               $criteria=new CDbCriteria;
               $criteria->compare('active',1);
               $criteria->addIncondition('district_id',$district_array);
               $MtSubDistrict = MtSubDistrict::model()->findAll($criteria);

               $subdistrict_array = array();
               foreach ($MtSubDistrict as $key => $value) {
                $subdistrict_array[] = $value->id;
            }

            $criteria=new CDbCriteria;
            $criteria->compare('active',1);
            $criteria->addIncondition('subdistrict_id',$subdistrict_array);
            $masterVillage = masterVillage::model()->findAll($criteria);

            $this->render('list', Array(
                        // 'years' => QuestionnaireModel::getYears(),
                'Survey'=>$Survey,
                'MtProvince'=>$MtProvince,
                'MtDistrict'=>$MtDistrict,
                'MtSubDistrict'=>$MtSubDistrict,
                'masterVillage'=>$masterVillage,
                'data' => QuestionnaireModel::list(),
            ));
        }
        // $this->render('list', Array(
        //     'data' => QuestionnaireModel::list(),
        // ));
    }

    public function actionSearchData()
     {
        if (isset($_GET)) {
          
            // $year = isset($_GET['search']['year']) ? $_GET['search']['year'] : null;
            // $province = isset($_GET['search']['Province']) ? $_GET['search']['Province'] : null;
            // $district = isset($_GET['search']['District']) ? $_GET['search']['District'] : null;
            // $subdistrict = isset($_GET['search']['subdistrict']) ? $_GET['search']['subdistrict'] : null;
            // $village = isset($_GET['search']['village']) ? $_GET['search']['village'] : null;
            $year = isset($_GET['year']) ? $_GET['year'] : null;
            $province = isset($_GET['province']) ? $_GET['province'] : null;
            $district = isset($_GET['district']) ? $_GET['district'] : null;
            $subdistrict = isset($_GET['subdistrict']) ? $_GET['subdistrict'] : null;
            $village = isset($_GET['village']) ? $_GET['village'] : null;

            // $Survey = Survey::model()->findAll();

            // $criteria=new CDbCriteria;
            // $criteria->compare('active',1);
            // $criteria->order = "province_name_th ASC";
            // $MtProvince = MtProvince::model()->findAll($criteria);

            // $criteria=new CDbCriteria;
            // if(isset($_GET["search"]["Province"])){

            //     $criterianew=new CDbCriteria;
            //     $criterianew->compare('active',1);
            //     $criterianew->compare('province_code',$_GET["search"]["Province"]);
            //     $MtProvince_search = MtProvince::model()->find($criterianew); 

            //     $criteria->compare('province_id',$MtProvince_search->id);
            // }
                                                  
            // $criteria->compare('active',1);
            // $MtDistrict = MtDistrict::model()->findAll($criteria); 

            // $criteria=new CDbCriteria;
            // $criteria->compare('active',1);
            // if (isset($_GET["search"]["District"])) {

            //     $criterianew=new CDbCriteria;
            //     $criterianew->compare('active',1);
            //     $criterianew->compare('province_id',$MtProvince_search->id);
            //     $criterianew->compare('district_code',$_GET["search"]["District"]);
            //     $MtDistrict_search = MtDistrict::model()->find($criterianew); 

            //     $criteria->compare('district_id',$MtDistrict_search->id);
            // }

            // $MtSubDistrict = MtSubDistrict::model()->findAll($criteria);

            // $criteria=new CDbCriteria;
            // $criteria->compare('active',1);
            // if (isset($_GET["search"]["subdistrict"])) {

            //     $criterianew=new CDbCriteria;
            //     $criterianew->compare('active',1);
            //     $criterianew->compare('district_id',$MtDistrict_search->id);
            //     $criterianew->compare('subdistrict_code',$_GET["search"]["subdistrict"]);
            //     $MtSubDistrict_search = MtSubDistrict::model()->find($criterianew); 

            //     $criteria->compare('subdistrict_id',$MtSubDistrict_search->id);
            // }

            // $masterVillage = masterVillage::model()->findAll($criteria); 
            $data = QuestionnaireModel::listSearch($year,$province,$district,$subdistrict,$village);

            // $this->render('list', Array(
            //     // 'years' => QuestionnaireModel::getYears(),
            //     'Survey'=>$Survey,
            //     'MtProvince'=>$MtProvince,
            //     'MtDistrict'=>$MtDistrict,
            //     'MtSubDistrict'=>$MtSubDistrict,
            //     'masterVillage'=>$masterVillage,
            //     'data' => $data,
            // ));
            $this->renderPartial('search', Array(
                'data' => $data,
            ));
        }
    } 

    public function actionLoadmap()
    {
        if (isset($_GET['id'])) {
            $zip_susess = $_SERVER['DOCUMENT_ROOT'].Yii::app()->baseurl.'/uploads/'.$_GET['id'].'.jpg';

            if(file_exists($zip_susess)) {  
                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                Header ("Content-disposition: attachment; filename=".basename($zip_susess));
                header('Content-Transfer-Encoding: binary');
                header('Expires: 0');
                header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
                header('Pragma: public');
                ob_clean();
                flush();             

                 readfile($zip_susess);
                die();
             } else {// echo("ไม่พบข้อมูล");
                http_response_code(404);
                die();
            }
        }
    }   
    
    public function actionCreateForm()
	{
        $form = QuestionnaireModel::form();
		$this->render('create', Array(
            'question' => $form,
            'start' => $form[0]['start'],
            'end' => $form[0]['end'],
        ));
    }

    public function actionCreate(){
        if(isset($_POST)){
            $data = $_POST;
            $questionnaire = QuestionnaireModel::create($data);
            $target_dir = $this->UploadPath;
            $target_file = $target_dir . basename($_FILES["mapfile"]["name"]);
            $uploadOk = 1;
            $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
            if (file_exists($target_file)) {
                echo "Sorry, file already exists.";
                $uploadOk = 0;
            }
            
            // Check file size
            // if ($_FILES["mapfile"]["size"] > 500000) {
            //     echo "Sorry, your file is too large.";
            //     $uploadOk = 0;
            // }
            
            // Allow certain file formats
            if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
            && $imageFileType != "gif" ) {
                echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                $uploadOk = 0;
            }
            
            // Check if $uploadOk is set to 0 by an error
            if ($uploadOk == 0) {
                echo "Sorry, your file was not uploaded.";
            // if everything is ok, try to upload file
            } else {
                if (move_uploaded_file($_FILES["mapfile"]["tmp_name"], $target_dir.$questionnaire['questionnaire']['questionnaire_id'].'.'.$imageFileType)) {
                    echo "The file ". htmlspecialchars( basename( $_FILES["mapfile"]["name"])). " has been uploaded.";
                } else {
                    echo "Sorry, there was an error uploading your file.";
                }
            }
              
            
        }
        $this->redirect(Yii::app()->baseurl.'/questionnaire');
        
    }

    function actionDelete(){
        if(QuestionnaireModel::delete($_POST['key'])){
            $files = glob($this->UploadPath.$_POST['key'].'.*');
            foreach ($files as $file) {
                unlink($file);
            }
        }
        $this->redirect(Yii::app()->baseurl.'/questionnaire');
    }

    public function actionEdit()
	{
        $form = QuestionnaireModel::form();
		$this->render('edit', Array(
            'question' => $form,
            'start' => $form[0]['start'],
            'end' => $form[0]['end'],
            'data' => QuestionnaireModel::get($_GET['id'])
        ));
    }

    public function actionShow()
    {
        $form = QuestionnaireModel::form();

        $this->render('show', Array(
            'question' => $form,
            'start' => $form[0]['start'],
            'end' => $form[0]['end'],
            'data' => QuestionnaireModel::get($_GET['id'])
        ));
    }

    public function actionUpdate()
    {
        $questionnaire = QuestionnaireModel::update($_GET['id'], $_POST);
        if (isset($_FILES["mapfile"])) {
            $target_dir = $this->UploadPath;
            $target_file = $target_dir . basename($_FILES["mapfile"]["name"]);
            $uploadOk = 1;
            $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
            $array_type = array("jpg","png","jpeg","gif");
            foreach ($array_type as $key => $value) {
                if (file_exists($target_dir.$_GET['id'].'.'.$value)) {
                unlink($target_dir.$_GET['id'].'.'.$value); // delete file
                // echo "Sorry, file already exists.";
                // $uploadOk = 0;
                }
            }
            
            // Check file size
            // if ($_FILES["mapfile"]["size"] > 500000) {
            //     echo "Sorry, your file is too large.";
            //     $uploadOk = 0;
            // }


            // Allow certain file formats
            if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
            && $imageFileType != "gif" ) {
                echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                $uploadOk = 0;
            }
            // Check if $uploadOk is set to 0 by an error
            if ($uploadOk == 0) {
                echo "Sorry, your file was not uploaded.";
            // if everything is ok, try to upload file
            } else {
                if (move_uploaded_file($_FILES["mapfile"]["tmp_name"], $target_dir.$_GET['id'].'.'.$imageFileType)) {
                    echo "The file ". htmlspecialchars( basename( $_FILES["mapfile"]["name"])). " has been uploaded.";
                } else {
                    echo "Sorry, there was an error uploading your file.";
                }
            }
        }

        // $this->redirect(Yii::app()->baseurl.'/questionnaire/'.$_GET['id'].'/edit');
        $this->redirect(Yii::app()->baseurl.'/questionnaire/');
    }

     public function actionExcel()
    {
        $model = new User('import');
        $HisImportArr = array();
        $HisImportErrorArr = array();
        $HisImportAttrErrorArr = array();
        $HisImportErrorMessageArr = array();
        $HisImportUserPassArr = array();
        $data = array();

        $model->excel_file = CUploadedFile::getInstance($model,'excel_file');
        
            $phpExcelPath = Yii::getPathOfAlias('ext.phpexcel.Classes');
            include($phpExcelPath . DIRECTORY_SEPARATOR . 'PHPExcel.php');

            $model->excel_file = CUploadedFile::getInstance($model,'excel_file');

            $webroot = Yii::app()->basePath."/../..";

            $filename = $webroot.'/uploads/' . $model->excel_file->name;
            $model->excel_file->saveAs($filename);

            $sheet_array = Yii::app()->yexcel->readActiveSheet($filename);
            $inputFileName = $filename;
            $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objReader->setReadDataOnly(true);
            $objPHPExcel = $objReader->load($inputFileName);
            $objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
            $highestRow = $objWorksheet->getHighestRow();
            $highestColumn = $objWorksheet->getHighestColumn();


            // var_dump("<pre>");
            for ($row = 2; $row <= $highestRow; ++$row) {
                $dataRow = $objWorksheet->rangeToArray('A'.$row.':'.$highestColumn.$row,null, true, true, true);
                $data_row = $dataRow[$row];
                // var_dump($data_row["D"]);

                $no_user = $data_row["A"];

                $staff_id = $data_row["B"];
                $status = $data_row["C"];
            }
                

            $this->render('excel', array(
                'model'=>$model,
                'division'=>$division,
                'department'=>$department,
                'position'=>$position,
                'workLocation'=>$workLocation,
                'MtProvince'=>$MtProvince,
                'MtDistrict'=>$MtDistrict,
                'MtSubdistrict'=>$MtSubdistrict,
                'Orgchart'=>$Orgchart,
                'arr_user'=>$arr_user,
                'arr_reason'=>$arr_reason,
                'arr_user_pass'=>$arr_user_pass,
            ));
        }
   
    public function actionCheckDistrict()
    {
        $criteria=new CDbCriteria;
        $criteria->compare('province_id',$_POST['id']);
        $criteria->compare('active',1);
        $District = District::model()->findAll($criteria);

    }
    
     public function actionCheckProvince()
    {
        $retuen = true;
        if (isset($_POST)) {
            $criteria=new CDbCriteria;
            $criteria->compare('province_code',$_POST['id']);
            $criteria->compare('county_id',$_POST['county']);
            $criteria->compare('active',1);
            $MtProvince = MtProvince::model()->find($criteria); 
            if ($MtProvince) {
                $retuen = true;
            }else{
                $retuen = false;
            }
            echo $retuen;
        }
    }
    
    public function actionEditCounty()
    {
        $criteria=new CDbCriteria;
        $criteria->compare('county_id',$_POST['id']);
        $criteria->compare('active',1);
        $MasterCounty = MasterCounty::model()->find($criteria);

        if (isset($MasterCounty)) {
            echo $MasterCounty->county_name;
        }else{
            echo "ไม่พบข้อมูล";
        }
        
    }

    public function actionEditProvince()
    {
        $criteria=new CDbCriteria;
        $criteria->compare('province_code',$_POST['id']);
        $criteria->compare('active',1);
        $Province = Province::model()->find($criteria);
        
        if (isset($Province)) {
            echo $Province->province_name_th;
        }else{
            echo "ไม่พบข้อมูล";
        }
        
    }

    public function actionEditDistrict()
    {
        $criteria=new CDbCriteria;
        $criteria->compare('district_code',$_POST['id']);
        $criteria->compare('active',1);
        $District = District::model()->find($criteria);
        if (isset($District)) {
            echo $District->district_name_th;
        }else{
            echo "ไม่พบข้อมูล";
        }
        
    }

    public function actionEditSubDistrict()
    {
        $criteria=new CDbCriteria;
        $criteria->compare('subdistrict_code',$_POST['id']);
        $criteria->compare('active',1);
        $SubDistrict = SubDistrict::model()->find($criteria);
        if (isset($SubDistrict)) {
            echo $SubDistrict->subdistrict_name_th;
        }else{
            echo "ไม่พบข้อมูล";
        }
        
    }

     public function actionEditVillage()
    {
        $criteria=new CDbCriteria;
        $criteria->compare('village_code',$_POST['id']);
        $criteria->compare('active',1);
        $Village = Village::model()->find($criteria);
        if (isset($Village)) {
            echo $Village->village_name;
        }else{
            echo "ไม่พบข้อมูล";
        }
        
    }
    public function actionSearchDistrict()
    {
        if ($_POST['id'] != "") {
            $criteria=new CDbCriteria;
            $criteria->compare('province_id',$_POST['id']);
            $criteria->compare('active',1);
            $model = District::model()->findAll($criteria);

            if (!empty($model)) {

                $data = '<option value ="">เลือกอำเภอ</option>';
                foreach ($model as $key => $value) {
                    $data .= '<option value = "'.$value->district_code.'"'.'data-id="'.$value->id.'">'.$value->district_code." -> ".$value->district_name_th.'</option>';
                }
                echo ($data);
            }else{
                $data = '<option value ="">ไม่พบข้อมูล</option>';
                echo ($data);
            }
        }        

    }

    public function actionSearchSubDistrict()
    {
        if ($_POST['id'] != "") {
            $criteria=new CDbCriteria;
            $criteria->compare('district_id',$_POST['id']);
            $criteria->compare('active',1);
            $model = SubDistrict::model()->findAll($criteria);

            if (!empty($model)) {

                $data = '<option value ="">เลือกตำบล</option>';
                foreach ($model as $key => $value) {
                    $data .= '<option value = "'.$value->subdistrict_code.'"'.'data-id="'.$value->id.'">'.$value->subdistrict_code." -> ".$value->subdistrict_name_th.'</option>';
                }
                echo ($data);
            }else{
                $data = '<option value ="">ไม่พบข้อมูล</option>';
                echo ($data);
            }
        }        

    }

    public function actionSearchVillage()
    {
        if ($_POST['id'] != "") {
            $criteria=new CDbCriteria;
            $criteria->compare('subdistrict_id',$_POST['id']);
            $criteria->compare('active',1);
            $model = masterVillage::model()->findAll($criteria);

            if (!empty($model)) {

                $data = '<option value ="">เลือกหมู่ที่</option>';
                foreach ($model as $key => $value) {
                    $data .= '<option value = "'.$value->village_code.'"'.'data-id="'.$value->village_id.'">'.$value->village_code." -> ".$value->village_name.'</option>';
                }
                echo ($data);
            }else{
                $data = '<option value ="">ไม่พบข้อมูล</option>';
                echo ($data);
            }
        }       
    }

}