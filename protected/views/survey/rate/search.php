<table class="table table-striped table-main" id="">
                <!-- <table class="table table-striped table-main" id="basic-2"> -->
                <thead>
                  <tr>
                    <th width="5%">ลำดับ</th>
                    <th width="10%">ปีเพาะปลูก</th>
                    <th width="25%">ชื่อผู้ขึ้นทะเบียนเกษตร</th>
                    <th width="25%">นามสกุลผู้ขึ้นทะเบียนเกษตร</th>
                    <th width="10%">วันที่</th>
                    <th width="25%">จัดการข้อมูล</th>
                  </tr>
                </thead>
                <tbody>
                  
                <?php
                $no = 1;
                  foreach ($data as $key => $value) {
                    $survey_times = explode("-", $value->survey_time);
                    $survey_0 = $survey_times[0] + 543;
                    $survey_times = $survey_times[2]."-".$survey_times[1]."-".$survey_0;
                   ?><tr>
                      <td><?= $no++;?></td>
                      <td><?= ($value->survey->year+543);?>/63</td>
                      <td><?= $value->farmer_name;?></td>
                      <td><?= $value->farmer_lastname;?></td>
                      <td><?= $survey_times;?></td>
                      <td>
                        <button type="button" class="btn btn-icon text-primary"><i data-feather="eye"></i></button>
                        <a type="button" class="btn btn-icon text-success" href="<?= Yii::app()->baseurl.'/survey/'.$_GET['id'].'/'.$value->questionnaire_id.'/rate/edit' ?>"><i data-feather="edit"></i></a>
                        <a type="button" class="btn btn-icon text-danger" href="<?= Yii::app()->baseurl.'/survey/'.$_GET['id'].'/'.$value->questionnaire_id.'/rate/delete' ?>"><i data-feather="trash"></i></a>
                      </td>
                    </tr>
                  <?php  }
                    ?>

                </tbody>
              </table>
              <script type="text/javascript">
   $(document).ready(function(){
     feather.replace();

    });
   </script> 