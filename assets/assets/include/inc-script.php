 <!-- latest jquery-->
 <!-- Bootstrap js-->
 <script src="theme/assets/js/bootstrap/popper.min.js"></script>
 <script src="theme/assets/js/bootstrap/bootstrap.js"></script>
 <!-- feather icon js-->
 <script src="theme/assets/js/icons/feather-icon/feather.min.js"></script>
 <script src="theme/assets/js/icons/feather-icon/feather-icon.js"></script>
 <!-- Sidebar jquery-->
 <script src="theme/assets/js/sidebar-menu.js"></script>
 <script src="theme/assets/js/config.js"></script>
 <!-- Plugins JS start-->
 <script src="theme/assets/js/jquery.ui.min.js"></script>
 <script src="theme/assets/js/dragable/sortable.js"></script>
 <script src="theme/assets/js/dragable/sortable-custom.js"></script>
 <script src="theme/assets/js/datatable/datatables/jquery.dataTables.min.js"></script>
 <script src="theme/assets/js/datatable/datatables/datatable.custom.js"></script>
 <script src="theme/assets/js/jsgrid/jsgrid.min.js"></script>
 <script src="theme/assets/js/jsgrid/griddata.js"></script>
 <script src="theme/assets/js/jsgrid/jsgrid.js"></script>
 <script src="theme/assets/js/chart/chartist/chartist.js"></script>
 <script src="theme/assets/js/chart/chartist/chartist-plugin-tooltip.js"></script>
 <script src="theme/assets/js/chart/knob/knob.min.js"></script>
 <script src="theme/assets/js/chart/knob/knob-chart.js"></script>
 <script src="theme/assets/js/chart/apex-chart/apex-chart.js"></script>
 <script src="theme/assets/js/chart/apex-chart/stock-prices.js"></script>
 <script src="theme/assets/js/notify/bootstrap-notify.min.js"></script>
 <script src="theme/assets/js/dashboard/default.js"></script>
 <script src="theme/assets/js/notify/index.js"></script>
 <script src="theme/assets/js/datepicker/date-picker/datepicker.js"></script>
 <script src="theme/assets/js/datepicker/date-picker/datepicker.en.js"></script>
 <script src="theme/assets/js/datepicker/date-picker/datepicker.th.js"></script>
 <script src="theme/assets/js/datepicker/date-picker/datepicker.custom.js"></script>
 <script src="theme/assets/js/typeahead/handlebars.js"></script>
 <script src="theme/assets/js/typeahead/typeahead.bundle.js"></script>
 <script src="theme/assets/js/typeahead/typeahead.custom.js"></script>
 <script src="theme/assets/js/typeahead-search/handlebars.js"></script>
 <script src="theme/assets/js/typeahead-search/typeahead-custom.js"></script>
 <script src="theme/assets/js/tooltip-init.js"></script>
 <!-- Plugins JS Ends-->
 <!-- Theme js-->
 <script src="theme/assets/js/script.js"></script>
 <!-- <script src="theme/assets/js/theme-customizer/customizer.js"></script> -->
  <script type="text/javascript">
          (function($) {
              $("video").load();
              $("video").prop('autoplay', true);
              $("video").prop('muted', true);
              $("video").prop('controls', false);
          })(jQuery);
</script>