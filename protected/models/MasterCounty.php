<?php

/**
 * This is the model class for table "master_county".
 *
 * The followings are the available columns in table 'master_county':
 * @property integer $county_id
 * @property string $county_name
 * @property integer $sector_id
 * @property integer $created_by
 * @property string $created_at
 * @property integer $updated_by
 * @property string $updated_at
 * @property integer $active
 * @property string $county_code
 */
class MasterCounty extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'db_oae.master_county';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('sector_id, created_by, updated_by, active', 'numerical', 'integerOnly'=>true),
			array('county_name, county_code', 'length', 'max'=>255),
			array('created_at, updated_at', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('county_id, county_name, sector_id, created_by, created_at, updated_by, updated_at, active, county_code', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'county_id' => 'County',
			'county_name' => 'ชื่อเขต',
			'sector_id' => 'ภาค',
			'created_by' => 'Created By',
			'created_at' => 'Created At',
			'updated_by' => 'Updated By',
			'updated_at' => 'Updated At',
			'active' => 'Active',
			'county_code' => 'รหัสเขต',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('county_id',$this->county_id);
		$criteria->compare('county_name',$this->county_name,true);
		$criteria->compare('sector_id',$this->sector_id);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('updated_by',$this->updated_by);
		$criteria->compare('updated_at',$this->updated_at,true);
		$criteria->compare('active',$this->active);
		$criteria->compare('county_code',$this->county_code,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function beforeSave() 
    {
		if(null !== Yii::app()->user && isset(Yii::app()->user->id))
		{
			$id = Yii::app()->user->id;
		}
		else
		{
			$id = 1;
		}

		if($this->isNewRecord)
		{
			$this->created_by = $id;
			$this->created_at = date("Y-m-d H:i:s");
			$this->updated_by = $id;
			$this->updated_at = date("Y-m-d H:i:s");
		}
		else
		{
			$this->updated_by = $id;
			$this->updated_at = date("Y-m-d H:i:s");
		}

        return parent::beforeSave();
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MasterCounty the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
     
    public function getMasterCountyList()
    {
    	$model = MasterCounty::model()->findAll(array("condition"=>"active =  1","order"=>"county_id"));
    	$list = CHtml::listData($model,'county_id','county_name');
		return $list;
    }
}
