/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : PostgreSQL
 Source Server Version : 120003
 Source Host           : localhost:5432
 Source Catalog        : db_oae
 Source Schema         : oae-survey

 Target Server Type    : PostgreSQL
 Target Server Version : 120003
 File Encoding         : 65001

 Date: 03/01/2021 23:28:56
*/


-- ----------------------------
-- Sequence structure for generic_data_data_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "oae-survey"."generic_data_data_id_seq";
CREATE SEQUENCE "oae-survey"."generic_data_data_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for question_group_question_group_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "oae-survey"."question_group_question_group_id_seq";
CREATE SEQUENCE "oae-survey"."question_group_question_group_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for question_question_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "oae-survey"."question_question_id_seq";
CREATE SEQUENCE "oae-survey"."question_question_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for questionnaire_question_question_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "oae-survey"."questionnaire_question_question_id_seq";
CREATE SEQUENCE "oae-survey"."questionnaire_question_question_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for questionnaire_questionnaire_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "oae-survey"."questionnaire_questionnaire_id_seq";
CREATE SEQUENCE "oae-survey"."questionnaire_questionnaire_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for survey_group_survey_group_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "oae-survey"."survey_group_survey_group_seq";
CREATE SEQUENCE "oae-survey"."survey_group_survey_group_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for survey_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "oae-survey"."survey_id_seq";
CREATE SEQUENCE "oae-survey"."survey_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for survey_value_value_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "oae-survey"."survey_value_value_id_seq";
CREATE SEQUENCE "oae-survey"."survey_value_value_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Table structure for generic_data
-- ----------------------------
DROP TABLE IF EXISTS "oae-survey"."generic_data";
CREATE TABLE "oae-survey"."generic_data" (
  "data_id" int4 NOT NULL DEFAULT nextval('"oae-survey".generic_data_data_id_seq'::regclass),
  "key" varchar(255) COLLATE "pg_catalog"."default",
  "value" text COLLATE "pg_catalog"."default",
  "group" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of generic_data
-- ----------------------------

-- ----------------------------
-- Table structure for masterdata
-- ----------------------------
DROP TABLE IF EXISTS "oae-survey"."masterdata";
CREATE TABLE "oae-survey"."masterdata" (
  "name" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "target_table" varchar(255) COLLATE "pg_catalog"."default",
  "key" varchar(255) COLLATE "pg_catalog"."default",
  "target_field" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of masterdata
-- ----------------------------

-- ----------------------------
-- Table structure for question
-- ----------------------------
DROP TABLE IF EXISTS "oae-survey"."question";
CREATE TABLE "oae-survey"."question" (
  "question_id" int4 NOT NULL DEFAULT nextval('"oae-survey".question_question_id_seq'::regclass),
  "question_group_id" int4 NOT NULL,
  "question" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "type" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "masterdata" varchar(255) COLLATE "pg_catalog"."default"
)
;
COMMENT ON COLUMN "oae-survey"."question"."question_id" IS 'รหัสคำถาม';
COMMENT ON COLUMN "oae-survey"."question"."question_group_id" IS 'หมวดหมู่';
COMMENT ON COLUMN "oae-survey"."question"."question" IS 'คำถาม';
COMMENT ON COLUMN "oae-survey"."question"."type" IS 'หมวดหมู่';

-- ----------------------------
-- Records of question
-- ----------------------------
INSERT INTO "oae-survey"."question" VALUES (4, 1, 'ผืนที่ (1)', 'text', '');
INSERT INTO "oae-survey"."question" VALUES (5, 1, 'แปลงที่ (2)', 'text', '');
INSERT INTO "oae-survey"."question" VALUES (6, 1, 'เนื้อที่ถือครอง (3)', 'text', '');
INSERT INTO "oae-survey"."question" VALUES (7, 1, 'การถือครองที่ดิน (4)', 'text', '');
INSERT INTO "oae-survey"."question" VALUES (8, 1, 'การจำนอง/การขายฝาก (5)', 'text', '');
INSERT INTO "oae-survey"."question" VALUES (9, 1, 'การใช้ประโยชน์ที่ดิน (6)', 'text', '');
INSERT INTO "oae-survey"."question" VALUES (10, 1, 'มูลค่าที่ดินที่มีกรรมสิทธิ์ครอบครอง/เข้าทำประโยชน์ (ปลายปีเพาะปลูก) (7)', 'text', '');
INSERT INTO "oae-survey"."question" VALUES (11, 1, 'การชลประทาน (8)', 'text', '');
INSERT INTO "oae-survey"."question" VALUES (12, 1, 'แหล่งน้ำหลักที่ใช้ (9)', 'text', '');
INSERT INTO "oae-survey"."question" VALUES (13, 1, 'รหัสพืชที่ปลูก/ปศุสัตว์/ประมง (10)', 'master', 'plant_animal');
INSERT INTO "oae-survey"."question" VALUES (15, 2, 'ชื่อพืช (2)', 'text', NULL);
INSERT INTO "oae-survey"."question" VALUES (16, 2, 'การปลูกและเนื้อที่', 'header', NULL);
INSERT INTO "oae-survey"."question" VALUES (17, 2, 'ปลูกกี่ครั้งต่อปี (3)', 'number', NULL);
INSERT INTO "oae-survey"."question" VALUES (18, 2, 'เนื้อที่ปลูก/ยืนต้น รวมทั้งปี (4)', 'number', NULL);
INSERT INTO "oae-survey"."question" VALUES (19, 2, 'เนื้อที่เก็บเกี่ยว/ให้ผลรวมทั้งปี (5)', 'number', NULL);
INSERT INTO "oae-survey"."question" VALUES (20, 2, 'ปริมาณผลผลิตพืชที่ได้มาระหว่างปี', 'header', NULL);
INSERT INTO "oae-survey"."question" VALUES (21, 2, 'คงเหลือต้นปี (กก.)(6)', 'number', NULL);
INSERT INTO "oae-survey"."question" VALUES (22, 2, 'ที่ผลิตได้เอง (กก.)(7)', 'number', NULL);
INSERT INTO "oae-survey"."question" VALUES (23, 2, 'ที่ได้รับมาฟรี (กก.)(8)', 'number', NULL);
INSERT INTO "oae-survey"."question" VALUES (24, 2, 'การขายผลผลิตพืช', 'header', NULL);
INSERT INTO "oae-survey"."question" VALUES (25, 2, 'ปริมาณการขายทั้งปี (กก.)(9)', 'number', NULL);
INSERT INTO "oae-survey"."question" VALUES (26, 2, 'ลักษณะการขายส่วนใหญ่ (1-8)(10)', 'text', NULL);
INSERT INTO "oae-survey"."question" VALUES (27, 2, 'ราคาที่ขายได้ (บาท/กก.)(11)', 'number', NULL);
INSERT INTO "oae-survey"."question" VALUES (28, 2, 'ปริมาณการกระจายผลผลิตพืชระหว่างปี', 'header', NULL);
INSERT INTO "oae-survey"."question" VALUES (29, 2, 'จ่ายเป็นค่าเช่า (กก.)(12)', 'number', NULL);
INSERT INTO "oae-survey"."question" VALUES (30, 2, 'จ่ายเพื่อใช้หนี้ (กก.)(13)', 'number', NULL);
INSERT INTO "oae-survey"."question" VALUES (31, 2, 'ใช้เลี้ยงสัตว์ (กก.)(14)', 'number', NULL);
INSERT INTO "oae-survey"."question" VALUES (32, 2, 'ใช้บริโภคในครัวเรือน (กก.)(15)', 'number', NULL);
INSERT INTO "oae-survey"."question" VALUES (33, 2, 'ใช้เพื่อแปรรูป (กก.)(16)', 'number', NULL);
INSERT INTO "oae-survey"."question" VALUES (34, 2, 'ให้ฟรีผู้อื่น/สูญหาย (กก.)(17)', 'number', NULL);
INSERT INTO "oae-survey"."question" VALUES (35, 2, 'ใช้ทำพันธ์ (กก.)(18)', 'number', NULL);
INSERT INTO "oae-survey"."question" VALUES (36, 2, 'คงเหลือปลายปี (กก.)(19)', 'number', NULL);
INSERT INTO "oae-survey"."question" VALUES (37, 2, 'ผลรวมราคาที่ขายได้ทั้งหมด (บาท)', 'number/sum', NULL);
INSERT INTO "oae-survey"."question" VALUES (38, 3, 'รหัสสินค้า (1)', 'master', 'plant');
INSERT INTO "oae-survey"."question" VALUES (39, 3, 'ชื่อพืช (2)', 'text', NULL);
INSERT INTO "oae-survey"."question" VALUES (40, 3, 'การปลูกและเนื้อที่', 'header', NULL);
INSERT INTO "oae-survey"."question" VALUES (41, 3, 'ปลูกกี่ครั้งต่อปี (3)', 'number', NULL);
INSERT INTO "oae-survey"."question" VALUES (42, 3, 'เนื้อที่ปลูก/ยืนต้น รวมทั้งปี (4)', 'number', NULL);
INSERT INTO "oae-survey"."question" VALUES (43, 3, 'เนื้อที่เก็บเกี่ยว/ให้ผลรวมทั้งปี (5)', 'number', NULL);
INSERT INTO "oae-survey"."question" VALUES (44, 3, ' ปริมาณผลผลิตพืชที่ได้มาระหว่างปี', 'header', NULL);
INSERT INTO "oae-survey"."question" VALUES (45, 3, 'คงเหลือต้นปี (บาท)(6)', 'number', NULL);
INSERT INTO "oae-survey"."question" VALUES (46, 3, 'ที่ผลิตได้เอง (บาท)(7)', 'number', NULL);
INSERT INTO "oae-survey"."question" VALUES (47, 3, 'ที่ได้รับมาฟรี (บาท)(8)', 'number', NULL);
INSERT INTO "oae-survey"."question" VALUES (48, 3, 'การขายผลผลิตพืช', 'header', NULL);
INSERT INTO "oae-survey"."question" VALUES (49, 3, 'ปริมาณการขายทั้งปี (บาท)(9)', 'number', NULL);
INSERT INTO "oae-survey"."question" VALUES (50, 3, 'ลักษณะการขายส่วนใหญ่ (1-8)(10)', 'number', NULL);
INSERT INTO "oae-survey"."question" VALUES (51, 3, 'ราคาที่ขายได้ (บาท/กก.)(11)', 'number', NULL);
INSERT INTO "oae-survey"."question" VALUES (52, 3, 'ปริมาณการกระจายผลผลิตพืชระหว่างปี', 'header', NULL);
INSERT INTO "oae-survey"."question" VALUES (53, 3, 'จ่ายเป็นค่าเช่า (บาท)(12)', 'number', NULL);
INSERT INTO "oae-survey"."question" VALUES (54, 3, 'จ่ายเพื่อใช้หนี้ (บาท)(13)', 'number', NULL);
INSERT INTO "oae-survey"."question" VALUES (55, 3, 'ใช้เลี้ยงสัตว์ (บาท)(14)', 'number', NULL);
INSERT INTO "oae-survey"."question" VALUES (56, 3, 'ใช้บริโภคในครัวเรือน (บาท)(15)', 'number', NULL);
INSERT INTO "oae-survey"."question" VALUES (57, 3, 'ใช้เพื่อแปรรูป (บาท)(16)', 'number', NULL);
INSERT INTO "oae-survey"."question" VALUES (58, 3, 'ให้ฟรีผู้อื่น/สูญหาย(บาท)(17)', 'number', NULL);
INSERT INTO "oae-survey"."question" VALUES (59, 3, 'ใช้ทำพันธุ์ (บาท)(18)', 'number', NULL);
INSERT INTO "oae-survey"."question" VALUES (60, 3, 'คงเหลือปลายปี (บาท)(19)', 'number', NULL);
INSERT INTO "oae-survey"."question" VALUES (14, 2, 'รหัสสินค้า (1)', 'master', 'plant');

-- ----------------------------
-- Table structure for question_group
-- ----------------------------
DROP TABLE IF EXISTS "oae-survey"."question_group";
CREATE TABLE "oae-survey"."question_group" (
  "question_group_id" int4 NOT NULL DEFAULT nextval('"oae-survey".question_group_question_group_id_seq'::regclass),
  "question_group_name" varchar(255) COLLATE "pg_catalog"."default",
  "question_group_number" float4
)
;
COMMENT ON COLUMN "oae-survey"."question_group"."question_group_id" IS 'หมวดหมู่';
COMMENT ON COLUMN "oae-survey"."question_group"."question_group_name" IS 'ชื่อหมวด';
COMMENT ON COLUMN "oae-survey"."question_group"."question_group_number" IS 'เลขหมวด';

-- ----------------------------
-- Records of question_group
-- ----------------------------
INSERT INTO "oae-survey"."question_group" VALUES (1, 'การถือครองและการใช้ประโยชน์ที่ดิน (ปี พ.ศ.2562 - พ.ศ.2563)', 1);
INSERT INTO "oae-survey"."question_group" VALUES (2, 'การปลูกพืชและการกระจายผลผลิตพืชในหน่วยปริมาณ ตอบเป็น “กิโลกรัม”', 2.1);
INSERT INTO "oae-survey"."question_group" VALUES (3, 'การปลูกพืชและการกระจายผลผลิตพืชในหน่วยปริมาณ ตอบเป็น “บาท”', 2.2);
INSERT INTO "oae-survey"."question_group" VALUES (4, 'การเลี้ยงสัตว์และการกระจายผลผลิตทางสัตว์ในหน่วยมูลค่า ตอบเป็น “บาท”', 3);
INSERT INTO "oae-survey"."question_group" VALUES (5, 'รายจ่ายเงินสดเกษตรทางพืช', 4);
INSERT INTO "oae-survey"."question_group" VALUES (6, 'รายจ่ายเงินสดเกษตรทางสัตว์', 5);
INSERT INTO "oae-survey"."question_group" VALUES (7, 'รายได้เงินสดจากการเกษตรอื่นๆ และนอกการเกษตร', 6);
INSERT INTO "oae-survey"."question_group" VALUES (8, 'รายจ่ายเงินสดเกษตรในฟาร์มและรายจ่ายเพื่อการอุปโภคบริโภค', 7);
INSERT INTO "oae-survey"."question_group" VALUES (9, 'ภาวะหนี้สิน สินเชื่อ และการชำระคืนของครัวเรือนเกษตร', 8);
INSERT INTO "oae-survey"."question_group" VALUES (10, 'ทรัพย์สินของครัวเรือนเกษตร', 9);
INSERT INTO "oae-survey"."question_group" VALUES (11, 'ข้อมูลส่วนบุคคล การเคลื่อนย้าย และการประกอบอาชีพของสมาชิกในครัวเรือนเกษตร', 10);
INSERT INTO "oae-survey"."question_group" VALUES (12, 'แบบสอบถามทัศนคติ ความคิดเห็น ความรู้สึก และการประเมินเหตุการณ์ในอดีต ปัจจุบัน และ อนาคต', 11);

-- ----------------------------
-- Table structure for questionnaire
-- ----------------------------
DROP TABLE IF EXISTS "oae-survey"."questionnaire";
CREATE TABLE "oae-survey"."questionnaire" (
  "questionnaire_id" int4 NOT NULL DEFAULT nextval('"oae-survey".questionnaire_questionnaire_id_seq'::regclass),
  "survey_id" int4 NOT NULL,
  "province" int4,
  "district" int4,
  "subdistrict" int4,
  "village" int4,
  "group" int4,
  "sample" int4,
  "farmer_name" varchar(255) COLLATE "pg_catalog"."default",
  "farmer_lastname" varchar(255) COLLATE "pg_catalog"."default",
  "farmer_id" varchar(13) COLLATE "pg_catalog"."default",
  "farmer_registered" bit(1),
  "informant_name" varchar(255) COLLATE "pg_catalog"."default",
  "informant_lastname" varchar(255) COLLATE "pg_catalog"."default",
  "informant_relation" varchar(255) COLLATE "pg_catalog"."default",
  "informant_id" varchar(13) COLLATE "pg_catalog"."default",
  "informant_tel" varchar(255) COLLATE "pg_catalog"."default",
  "inIrrigatedArea" bit(1),
  "staff_id" varchar(255) COLLATE "pg_catalog"."default",
  "survey_time" date,
  "addressNumber" varchar COLLATE "pg_catalog"."default",
  "staff_name" varchar(255) COLLATE "pg_catalog"."default",
  "staff_lname" varchar(255) COLLATE "pg_catalog"."default",
  "office_id" varchar COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of questionnaire
-- ----------------------------
INSERT INTO "oae-survey"."questionnaire" VALUES (4, 1, 1, 1, 1, 1, 1, 1, '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '0001-01-01', '1', '1', '1', '1');
INSERT INTO "oae-survey"."questionnaire" VALUES (5, 1, 120, 12, 12, 12, 1, 1, 'สุชาติ', 'นามสกุลสุชาติ', '1234567890123', '1', 'สมศรี', 'นามสกุลสุชาติ', '1', '1234567890123', '0811234561', '1', '11021547', '2019-10-10', '1234', 'สมชาย', 'นามสกุล', '10');
INSERT INTO "oae-survey"."questionnaire" VALUES (6, 1, 120, 12, 12, 12, 1, 1, 'สุชาติ', 'นามสกุลสุชาติ', '1234567890123', '1', 'สมศรี', 'นามสกุลสุชาติ', '1', '1234567890123', '0811234561', '1', '11021547', '2019-10-10', '1234', 'สมชาย', 'นามสกุล', '10');
INSERT INTO "oae-survey"."questionnaire" VALUES (7, 1, 120, 12, 12, 12, 1, 1, 'สุชาติ', 'นามสกุลสุชาติ', '1234567890123', '1', 'สมศรี', 'นามสกุลสุชาติ', '1', '1234567890123', '0811234561', '1', '11021547', '2019-10-10', '1234', 'สมชาย', 'นามสกุล', '10');
INSERT INTO "oae-survey"."questionnaire" VALUES (8, 1, 120, 12, 12, 12, 1, 1, 'สุชาติ', 'นามสกุลสุชาติ', '1234567890123', '1', 'สมศรี', 'นามสกุลสุชาติ', '1', '1234567890123', '0811234561', '1', '11021547', '2019-10-10', '1234', 'สมชาย', 'นามสกุล', '10');

-- ----------------------------
-- Table structure for questionnaire_map
-- ----------------------------
DROP TABLE IF EXISTS "oae-survey"."questionnaire_map";
CREATE TABLE "oae-survey"."questionnaire_map" (
  "survey" int4 NOT NULL,
  "question" int4 NOT NULL
)
;

-- ----------------------------
-- Records of questionnaire_map
-- ----------------------------
INSERT INTO "oae-survey"."questionnaire_map" VALUES (1, 1);
INSERT INTO "oae-survey"."questionnaire_map" VALUES (1, 2);
INSERT INTO "oae-survey"."questionnaire_map" VALUES (1, 3);
INSERT INTO "oae-survey"."questionnaire_map" VALUES (1, 4);
INSERT INTO "oae-survey"."questionnaire_map" VALUES (1, 5);
INSERT INTO "oae-survey"."questionnaire_map" VALUES (1, 6);
INSERT INTO "oae-survey"."questionnaire_map" VALUES (1, 7);
INSERT INTO "oae-survey"."questionnaire_map" VALUES (1, 8);

-- ----------------------------
-- Table structure for questionnaire_question
-- ----------------------------
DROP TABLE IF EXISTS "oae-survey"."questionnaire_question";
CREATE TABLE "oae-survey"."questionnaire_question" (
  "question_id" int4 NOT NULL DEFAULT nextval('"oae-survey".questionnaire_question_question_id_seq'::regclass),
  "title" varchar(255) COLLATE "pg_catalog"."default",
  "subtitle" text COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of questionnaire_question
-- ----------------------------
INSERT INTO "oae-survey"."questionnaire_question" VALUES (1, 'ศพก.', 'ศูนย์เรียนรู้การเพิ่มประสิทธิภาพการผลิตสินค้าเกษตร');
INSERT INTO "oae-survey"."questionnaire_question" VALUES (2, 'แปลงใหญ่', 'การรวมกลุ่มกันเพื่อลดต้นทุนและเพิ่มผลผลิตทางการเกษตร');
INSERT INTO "oae-survey"."questionnaire_question" VALUES (3, 'การบริหารจัดการน้ำ', 'โครงการชลประทานในพื้นที่ ทั้งขนาดใหญ่/กลาง/เล็ก/รวมถึงแหล่งน้ำชุมชน แหล่งน้ำในไร่นา (บ่อจิ๋ว)');
INSERT INTO "oae-survey"."questionnaire_question" VALUES (4, 'แผนการผลิตข้าวครบวงจร', 'การส่งเสริมและสนับสนุนการปลูกข้าวแก่เกษตรกรและวางแผนการตลาดข้าวแบบครบวงจร');
INSERT INTO "oae-survey"."questionnaire_question" VALUES (5, 'Zoning by Agri-Map', 'การสนับสนุนและส่งเสริมให้เกษตรกรปรับเปลี่ยนการผลิตในพื้นที่ไม่เหมาะสมเป็นการผลิตที่เหมาะสม');
INSERT INTO "oae-survey"."questionnaire_question" VALUES (6, 'ธนาคารสินค้าเกษตร', 'การสนับสนุนช่วยเหลือให้เกษตรกรเข้าถึงและใช้ประโยชน์จากปัจจัยการผลิตเช่น โค ปุ๋ย เมล็ดพันธุ์');
INSERT INTO "oae-survey"."questionnaire_question" VALUES (7, 'มาตรฐานสินค้าเกษตร GAP/เกษตรอินทรีย์', 'การเพิ่มประสิทธิภาพการผลิตในพื้นที่เกษตรแปลงใหญ่ให้เป็นไปตามมาตรฐาน/การสนับสนุน ส่งเสริม และพัฒนาเกษตรกร ตามแนวทางเกษตรอินทรีย์');
INSERT INTO "oae-survey"."questionnaire_question" VALUES (8, 'โครงการส่งเสริมเกษตรทฤษฎีใหม่', 'การสนับสนุนให้เกษตรกรมีความรู้ความเข้าในการฏิิบัติตามระบบเกษตรทฤษฎีใหม่ และสามารถนำไปปรับใช้เพื่อการประกอบอาชีพอย่างมั่นคงและยั่งยืน');

-- ----------------------------
-- Table structure for questionniare_question_value
-- ----------------------------
DROP TABLE IF EXISTS "oae-survey"."questionniare_question_value";
CREATE TABLE "oae-survey"."questionniare_question_value" (
  "questionnaire_id" int4 NOT NULL,
  "questionnaire_question_id" int4 NOT NULL,
  "value" text COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of questionniare_question_value
-- ----------------------------
INSERT INTO "oae-survey"."questionniare_question_value" VALUES (4, 1, '1');
INSERT INTO "oae-survey"."questionniare_question_value" VALUES (4, 2, '1');
INSERT INTO "oae-survey"."questionniare_question_value" VALUES (4, 3, '1');
INSERT INTO "oae-survey"."questionniare_question_value" VALUES (4, 4, '1');
INSERT INTO "oae-survey"."questionniare_question_value" VALUES (4, 5, '1');
INSERT INTO "oae-survey"."questionniare_question_value" VALUES (4, 6, '1');
INSERT INTO "oae-survey"."questionniare_question_value" VALUES (4, 7, '1');
INSERT INTO "oae-survey"."questionniare_question_value" VALUES (4, 8, '1');
INSERT INTO "oae-survey"."questionniare_question_value" VALUES (5, 1, '1');
INSERT INTO "oae-survey"."questionniare_question_value" VALUES (5, 2, '1');
INSERT INTO "oae-survey"."questionniare_question_value" VALUES (5, 3, '1');
INSERT INTO "oae-survey"."questionniare_question_value" VALUES (5, 4, '1');
INSERT INTO "oae-survey"."questionniare_question_value" VALUES (5, 5, '1');
INSERT INTO "oae-survey"."questionniare_question_value" VALUES (5, 6, '1');
INSERT INTO "oae-survey"."questionniare_question_value" VALUES (5, 7, '1');
INSERT INTO "oae-survey"."questionniare_question_value" VALUES (5, 8, '1');
INSERT INTO "oae-survey"."questionniare_question_value" VALUES (6, 1, '1');
INSERT INTO "oae-survey"."questionniare_question_value" VALUES (6, 2, '1');
INSERT INTO "oae-survey"."questionniare_question_value" VALUES (6, 3, '1');
INSERT INTO "oae-survey"."questionniare_question_value" VALUES (6, 4, '1');
INSERT INTO "oae-survey"."questionniare_question_value" VALUES (6, 5, '1');
INSERT INTO "oae-survey"."questionniare_question_value" VALUES (6, 6, '1');
INSERT INTO "oae-survey"."questionniare_question_value" VALUES (6, 7, '1');
INSERT INTO "oae-survey"."questionniare_question_value" VALUES (6, 8, '1');
INSERT INTO "oae-survey"."questionniare_question_value" VALUES (7, 1, '1');
INSERT INTO "oae-survey"."questionniare_question_value" VALUES (7, 2, '1');
INSERT INTO "oae-survey"."questionniare_question_value" VALUES (7, 3, '1');
INSERT INTO "oae-survey"."questionniare_question_value" VALUES (7, 4, '1');
INSERT INTO "oae-survey"."questionniare_question_value" VALUES (7, 5, '1');
INSERT INTO "oae-survey"."questionniare_question_value" VALUES (7, 6, '1');
INSERT INTO "oae-survey"."questionniare_question_value" VALUES (7, 7, '1');
INSERT INTO "oae-survey"."questionniare_question_value" VALUES (7, 8, '1');
INSERT INTO "oae-survey"."questionniare_question_value" VALUES (8, 1, '1');
INSERT INTO "oae-survey"."questionniare_question_value" VALUES (8, 2, '1');
INSERT INTO "oae-survey"."questionniare_question_value" VALUES (8, 3, '1');
INSERT INTO "oae-survey"."questionniare_question_value" VALUES (8, 4, '1');
INSERT INTO "oae-survey"."questionniare_question_value" VALUES (8, 5, '1');
INSERT INTO "oae-survey"."questionniare_question_value" VALUES (8, 6, '1');
INSERT INTO "oae-survey"."questionniare_question_value" VALUES (8, 7, '1');
INSERT INTO "oae-survey"."questionniare_question_value" VALUES (8, 8, '1');

-- ----------------------------
-- Table structure for survey
-- ----------------------------
DROP TABLE IF EXISTS "oae-survey"."survey";
CREATE TABLE "oae-survey"."survey" (
  "id" int4 NOT NULL DEFAULT nextval('"oae-survey".survey_id_seq'::regclass),
  "year" int4,
  "start" date,
  "end" date
)
;
COMMENT ON COLUMN "oae-survey"."survey"."id" IS 'รหัสแบบสอบถาม';
COMMENT ON COLUMN "oae-survey"."survey"."year" IS 'ปีที่ใช้งานแบบสอบถาม';
COMMENT ON COLUMN "oae-survey"."survey"."start" IS 'วันที่เริ่ม';
COMMENT ON COLUMN "oae-survey"."survey"."end" IS 'สิ้นสุดการสอบถาม';

-- ----------------------------
-- Records of survey
-- ----------------------------
INSERT INTO "oae-survey"."survey" VALUES (1, 2563, '2020-01-01', '2020-12-31');

-- ----------------------------
-- Table structure for survey_group
-- ----------------------------
DROP TABLE IF EXISTS "oae-survey"."survey_group";
CREATE TABLE "oae-survey"."survey_group" (
  "survey_group" int4 NOT NULL DEFAULT nextval('"oae-survey".survey_group_survey_group_seq'::regclass),
  "timestamp" timestamptz(6) DEFAULT now()
)
;

-- ----------------------------
-- Records of survey_group
-- ----------------------------
INSERT INTO "oae-survey"."survey_group" VALUES (1, '2021-01-03 22:32:01.965171+07');
INSERT INTO "oae-survey"."survey_group" VALUES (2, '2021-01-03 22:32:26+07');
INSERT INTO "oae-survey"."survey_group" VALUES (3, '2021-01-03 22:32:38+07');
INSERT INTO "oae-survey"."survey_group" VALUES (4, '2021-01-03 22:35:45.701099+07');
INSERT INTO "oae-survey"."survey_group" VALUES (5, '2021-01-03 22:36:01.906084+07');
INSERT INTO "oae-survey"."survey_group" VALUES (6, '2021-01-03 23:22:00.571452+07');

-- ----------------------------
-- Table structure for survey_map
-- ----------------------------
DROP TABLE IF EXISTS "oae-survey"."survey_map";
CREATE TABLE "oae-survey"."survey_map" (
  "survey" int4 NOT NULL,
  "question" int4 NOT NULL,
  "size" int4 NOT NULL DEFAULT 12,
  "order" int4 NOT NULL DEFAULT 0
)
;

-- ----------------------------
-- Records of survey_map
-- ----------------------------
INSERT INTO "oae-survey"."survey_map" VALUES (1, 14, 12, 0);
INSERT INTO "oae-survey"."survey_map" VALUES (1, 15, 12, 0);
INSERT INTO "oae-survey"."survey_map" VALUES (1, 16, 12, 0);
INSERT INTO "oae-survey"."survey_map" VALUES (1, 17, 12, 0);
INSERT INTO "oae-survey"."survey_map" VALUES (1, 18, 12, 0);
INSERT INTO "oae-survey"."survey_map" VALUES (1, 19, 12, 0);
INSERT INTO "oae-survey"."survey_map" VALUES (1, 20, 12, 0);
INSERT INTO "oae-survey"."survey_map" VALUES (1, 21, 12, 0);
INSERT INTO "oae-survey"."survey_map" VALUES (1, 22, 12, 0);
INSERT INTO "oae-survey"."survey_map" VALUES (1, 23, 12, 0);
INSERT INTO "oae-survey"."survey_map" VALUES (1, 24, 12, 0);
INSERT INTO "oae-survey"."survey_map" VALUES (1, 25, 12, 0);
INSERT INTO "oae-survey"."survey_map" VALUES (1, 26, 12, 0);
INSERT INTO "oae-survey"."survey_map" VALUES (1, 27, 12, 0);
INSERT INTO "oae-survey"."survey_map" VALUES (1, 28, 12, 0);
INSERT INTO "oae-survey"."survey_map" VALUES (1, 29, 12, 0);
INSERT INTO "oae-survey"."survey_map" VALUES (1, 30, 12, 0);
INSERT INTO "oae-survey"."survey_map" VALUES (1, 31, 12, 0);
INSERT INTO "oae-survey"."survey_map" VALUES (1, 32, 12, 0);
INSERT INTO "oae-survey"."survey_map" VALUES (1, 33, 12, 0);
INSERT INTO "oae-survey"."survey_map" VALUES (1, 34, 12, 0);
INSERT INTO "oae-survey"."survey_map" VALUES (1, 35, 12, 0);
INSERT INTO "oae-survey"."survey_map" VALUES (1, 36, 12, 0);
INSERT INTO "oae-survey"."survey_map" VALUES (1, 37, 12, 0);
INSERT INTO "oae-survey"."survey_map" VALUES (1, 38, 12, 0);
INSERT INTO "oae-survey"."survey_map" VALUES (1, 39, 12, 0);
INSERT INTO "oae-survey"."survey_map" VALUES (1, 40, 12, 0);
INSERT INTO "oae-survey"."survey_map" VALUES (1, 41, 12, 0);
INSERT INTO "oae-survey"."survey_map" VALUES (1, 42, 12, 0);
INSERT INTO "oae-survey"."survey_map" VALUES (1, 43, 12, 0);
INSERT INTO "oae-survey"."survey_map" VALUES (1, 44, 12, 0);
INSERT INTO "oae-survey"."survey_map" VALUES (1, 45, 12, 0);
INSERT INTO "oae-survey"."survey_map" VALUES (1, 46, 12, 0);
INSERT INTO "oae-survey"."survey_map" VALUES (1, 47, 12, 0);
INSERT INTO "oae-survey"."survey_map" VALUES (1, 48, 12, 0);
INSERT INTO "oae-survey"."survey_map" VALUES (1, 49, 12, 0);
INSERT INTO "oae-survey"."survey_map" VALUES (1, 50, 12, 0);
INSERT INTO "oae-survey"."survey_map" VALUES (1, 51, 12, 0);
INSERT INTO "oae-survey"."survey_map" VALUES (1, 52, 12, 0);
INSERT INTO "oae-survey"."survey_map" VALUES (1, 53, 12, 0);
INSERT INTO "oae-survey"."survey_map" VALUES (1, 54, 12, 0);
INSERT INTO "oae-survey"."survey_map" VALUES (1, 55, 12, 0);
INSERT INTO "oae-survey"."survey_map" VALUES (1, 56, 12, 0);
INSERT INTO "oae-survey"."survey_map" VALUES (1, 57, 12, 0);
INSERT INTO "oae-survey"."survey_map" VALUES (1, 58, 12, 0);
INSERT INTO "oae-survey"."survey_map" VALUES (1, 59, 12, 0);
INSERT INTO "oae-survey"."survey_map" VALUES (1, 60, 12, 0);
INSERT INTO "oae-survey"."survey_map" VALUES (1, 13, 12, 13);
INSERT INTO "oae-survey"."survey_map" VALUES (1, 4, 4, 4);
INSERT INTO "oae-survey"."survey_map" VALUES (1, 5, 4, 5);
INSERT INTO "oae-survey"."survey_map" VALUES (1, 6, 4, 6);
INSERT INTO "oae-survey"."survey_map" VALUES (1, 7, 4, 7);
INSERT INTO "oae-survey"."survey_map" VALUES (1, 8, 4, 8);
INSERT INTO "oae-survey"."survey_map" VALUES (1, 9, 4, 9);
INSERT INTO "oae-survey"."survey_map" VALUES (1, 10, 4, 10);
INSERT INTO "oae-survey"."survey_map" VALUES (1, 11, 4, 11);
INSERT INTO "oae-survey"."survey_map" VALUES (1, 12, 4, 12);

-- ----------------------------
-- Table structure for survey_value
-- ----------------------------
DROP TABLE IF EXISTS "oae-survey"."survey_value";
CREATE TABLE "oae-survey"."survey_value" (
  "questionnaire_id" int4 NOT NULL,
  "question_id" int4 NOT NULL,
  "value" text COLLATE "pg_catalog"."default",
  "value_id" int4 NOT NULL DEFAULT nextval('"oae-survey".survey_value_value_id_seq'::regclass),
  "survey_group" int4 NOT NULL
)
;

-- ----------------------------
-- Records of survey_value
-- ----------------------------
INSERT INTO "oae-survey"."survey_value" VALUES (4, 4, '1', 33, 5);
INSERT INTO "oae-survey"."survey_value" VALUES (4, 5, '1', 34, 5);
INSERT INTO "oae-survey"."survey_value" VALUES (4, 6, '1', 35, 5);
INSERT INTO "oae-survey"."survey_value" VALUES (4, 7, '1', 36, 5);
INSERT INTO "oae-survey"."survey_value" VALUES (4, 8, '1111', 37, 5);
INSERT INTO "oae-survey"."survey_value" VALUES (4, 9, '1', 38, 5);
INSERT INTO "oae-survey"."survey_value" VALUES (4, 10, '1', 39, 5);
INSERT INTO "oae-survey"."survey_value" VALUES (4, 11, '1', 40, 5);
INSERT INTO "oae-survey"."survey_value" VALUES (4, 12, '1', 41, 5);
INSERT INTO "oae-survey"."survey_value" VALUES (4, 13, '123', 42, 5);
INSERT INTO "oae-survey"."survey_value" VALUES (4, 4, '1', 43, 6);
INSERT INTO "oae-survey"."survey_value" VALUES (4, 5, '1', 44, 6);
INSERT INTO "oae-survey"."survey_value" VALUES (4, 6, '1', 45, 6);
INSERT INTO "oae-survey"."survey_value" VALUES (4, 7, '1', 46, 6);
INSERT INTO "oae-survey"."survey_value" VALUES (4, 8, '1', 47, 6);
INSERT INTO "oae-survey"."survey_value" VALUES (4, 9, '1', 48, 6);
INSERT INTO "oae-survey"."survey_value" VALUES (4, 10, '1', 49, 6);
INSERT INTO "oae-survey"."survey_value" VALUES (4, 11, '1', 50, 6);
INSERT INTO "oae-survey"."survey_value" VALUES (4, 12, '1', 51, 6);
INSERT INTO "oae-survey"."survey_value" VALUES (4, 13, '121', 52, 6);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "oae-survey"."generic_data_data_id_seq"
OWNED BY "oae-survey"."generic_data"."data_id";
SELECT setval('"oae-survey"."generic_data_data_id_seq"', 2, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "oae-survey"."question_group_question_group_id_seq"
OWNED BY "oae-survey"."question_group"."question_group_id";
SELECT setval('"oae-survey"."question_group_question_group_id_seq"', 13, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "oae-survey"."question_question_id_seq"
OWNED BY "oae-survey"."question"."question_id";
SELECT setval('"oae-survey"."question_question_id_seq"', 9, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "oae-survey"."questionnaire_question_question_id_seq"
OWNED BY "oae-survey"."questionnaire_question"."question_id";
SELECT setval('"oae-survey"."questionnaire_question_question_id_seq"', 2, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "oae-survey"."questionnaire_questionnaire_id_seq"
OWNED BY "oae-survey"."questionnaire"."questionnaire_id";
SELECT setval('"oae-survey"."questionnaire_questionnaire_id_seq"', 9, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "oae-survey"."survey_group_survey_group_seq"
OWNED BY "oae-survey"."survey_group"."survey_group";
SELECT setval('"oae-survey"."survey_group_survey_group_seq"', 7, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "oae-survey"."survey_id_seq"
OWNED BY "oae-survey"."survey"."id";
SELECT setval('"oae-survey"."survey_id_seq"', 2, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "oae-survey"."survey_value_value_id_seq"
OWNED BY "oae-survey"."survey_value"."value_id";
SELECT setval('"oae-survey"."survey_value_value_id_seq"', 53, true);

-- ----------------------------
-- Indexes structure for table generic_data
-- ----------------------------
CREATE INDEX "group" ON "oae-survey"."generic_data" USING btree (
  "group" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table generic_data
-- ----------------------------
ALTER TABLE "oae-survey"."generic_data" ADD CONSTRAINT "generic_data_pkey" PRIMARY KEY ("data_id");

-- ----------------------------
-- Primary Key structure for table masterdata
-- ----------------------------
ALTER TABLE "oae-survey"."masterdata" ADD CONSTRAINT "masterdata_pkey" PRIMARY KEY ("name");

-- ----------------------------
-- Primary Key structure for table question
-- ----------------------------
ALTER TABLE "oae-survey"."question" ADD CONSTRAINT "_copy_4" PRIMARY KEY ("question_id");

-- ----------------------------
-- Primary Key structure for table question_group
-- ----------------------------
ALTER TABLE "oae-survey"."question_group" ADD CONSTRAINT "question_group_pkey" PRIMARY KEY ("question_group_id");

-- ----------------------------
-- Primary Key structure for table questionnaire
-- ----------------------------
ALTER TABLE "oae-survey"."questionnaire" ADD CONSTRAINT "_copy_3" PRIMARY KEY ("questionnaire_id");

-- ----------------------------
-- Primary Key structure for table questionnaire_map
-- ----------------------------
ALTER TABLE "oae-survey"."questionnaire_map" ADD CONSTRAINT "_copy_1" PRIMARY KEY ("survey", "question");

-- ----------------------------
-- Primary Key structure for table questionnaire_question
-- ----------------------------
ALTER TABLE "oae-survey"."questionnaire_question" ADD CONSTRAINT "_copy_2" PRIMARY KEY ("question_id");

-- ----------------------------
-- Primary Key structure for table questionniare_question_value
-- ----------------------------
ALTER TABLE "oae-survey"."questionniare_question_value" ADD CONSTRAINT "questionniare_question_value_pkey" PRIMARY KEY ("questionnaire_id", "questionnaire_question_id");

-- ----------------------------
-- Primary Key structure for table survey
-- ----------------------------
ALTER TABLE "oae-survey"."survey" ADD CONSTRAINT "_copy_6" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table survey_group
-- ----------------------------
ALTER TABLE "oae-survey"."survey_group" ADD CONSTRAINT "survey_group_pkey" PRIMARY KEY ("survey_group");

-- ----------------------------
-- Primary Key structure for table survey_map
-- ----------------------------
ALTER TABLE "oae-survey"."survey_map" ADD CONSTRAINT "_copy_5" PRIMARY KEY ("survey", "question");

-- ----------------------------
-- Primary Key structure for table survey_value
-- ----------------------------
ALTER TABLE "oae-survey"."survey_value" ADD CONSTRAINT "_copy_8" PRIMARY KEY ("questionnaire_id", "question_id", "value_id");

-- ----------------------------
-- Foreign Keys structure for table question
-- ----------------------------
ALTER TABLE "oae-survey"."question" ADD CONSTRAINT "group" FOREIGN KEY ("question_group_id") REFERENCES "oae-survey"."question_group" ("question_group_id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Keys structure for table questionnaire
-- ----------------------------
ALTER TABLE "oae-survey"."questionnaire" ADD CONSTRAINT "survey" FOREIGN KEY ("survey_id") REFERENCES "oae-survey"."survey" ("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Keys structure for table questionnaire_map
-- ----------------------------
ALTER TABLE "oae-survey"."questionnaire_map" ADD CONSTRAINT "member_of_1" FOREIGN KEY ("survey") REFERENCES "oae-survey"."survey" ("id") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "oae-survey"."questionnaire_map" ADD CONSTRAINT "questionnaire_question" FOREIGN KEY ("question") REFERENCES "oae-survey"."questionnaire_question" ("question_id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Keys structure for table questionniare_question_value
-- ----------------------------
ALTER TABLE "oae-survey"."questionniare_question_value" ADD CONSTRAINT "questionnaire_2" FOREIGN KEY ("questionnaire_id") REFERENCES "oae-survey"."questionnaire" ("questionnaire_id") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "oae-survey"."questionniare_question_value" ADD CONSTRAINT "questionnaire_question" FOREIGN KEY ("questionnaire_question_id") REFERENCES "oae-survey"."questionnaire_question" ("question_id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Keys structure for table survey_map
-- ----------------------------
ALTER TABLE "oae-survey"."survey_map" ADD CONSTRAINT "member_of" FOREIGN KEY ("survey") REFERENCES "oae-survey"."survey" ("id") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "oae-survey"."survey_map" ADD CONSTRAINT "question" FOREIGN KEY ("question") REFERENCES "oae-survey"."question" ("question_id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Keys structure for table survey_value
-- ----------------------------
ALTER TABLE "oae-survey"."survey_value" ADD CONSTRAINT "question_1" FOREIGN KEY ("question_id") REFERENCES "oae-survey"."question" ("question_id") ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE "oae-survey"."survey_value" ADD CONSTRAINT "questionnaire_1" FOREIGN KEY ("questionnaire_id") REFERENCES "oae-survey"."questionnaire" ("questionnaire_id") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "oae-survey"."survey_value" ADD CONSTRAINT "survey_group" FOREIGN KEY ("survey_group") REFERENCES "oae-survey"."survey_group" ("survey_group") ON DELETE CASCADE ON UPDATE CASCADE;
