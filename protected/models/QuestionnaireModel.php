<?php
class QuestionnaireModel
{

    public function create($data){
        if ($data) {
            if (!isset($data['parent_farmer'])) {
               $parent_farmer = 'n';
            }else{
               $parent_farmer = 'y';
            }
            if (!isset($data['informant_relation'])) {
               $informant_relation = null;
            }else{
               $informant_relation = $data['informant_relation'];
            }
        }
        $data["survey_time"] = date("Y-m-d", strtotime($data["survey_time"]));



            $query = 'SELECT * FROM "'.schema_name.'"."questionnaire" WHERE "staff_id" = \''.$data['staff_id'].'\' AND "province"=\''.$data['province'].'\' AND "district" = \''.$data['district'].'\' ';
         
       


        $sql = 'INSERT INTO "'.schema_name.'"."questionnaire" (
            "survey_id",
            "province",
            "district",
            "subdistrict",
            "village",
            "addressNumber",
            "group",
            "sample",
            "farmer_name",
            "farmer_lastname",
            "farmer_id",
            "farmer_registered",
            "informant_name",
            "informant_lastname",
            "informant_relation",
            "informant_id",
            "informant_tel",
            "inIrrigatedArea",
            "staff_id",
            "survey_time",
            "staff_name",
            "staff_lname",
            "office_id",
            "parent_farmer" 
        )
        VALUES
            (
                ( SELECT survey."id" FROM "'.schema_name.'".survey ORDER BY survey."id" DESC LIMIT 1 OFFSET 0 ),
                \''.$data['province'].'\',
                \''.$data['district'].'\',
                \''.$data['subdistrict'].'\',
                \''.$data['village'].'\',
                \''.$data['addressNumber'].'\',
                \''.$data['group'].'\',
                \''.$data['sample'].'\',
                \''.$data['farmer_name'].'\',
                \''.$data['farmer_lname'].'\',
                \''.$data['farmer_id'].'\',
                \''.$data['farmer_registered'].'\',
                \''.$data['informant_name'].'\',
                \''.$data['informant_lname'].'\',
                \''.$informant_relation.'\',
                \''.$data['informant_id'].'\',
                \''.$data['informant_tel'].'\',
                \''.$data['inIrrigatedArea'].'\',
                \''.$data['staff_id'].'\',
                \''.$data['survey_time'].'\',
                \''.$data['staff_name'].'\',
                \''.$data['staff_lname'].'\',
                \''.$data['office'].'\',
                \''.$parent_farmer.'\' 
            ) RETURNING *';
    
        $questionaire = QuestionnaireModel::query($sql);
        $questions = Array();
        foreach($data as $key => $value){
            $exp_key = explode('-', $key);
            if($exp_key[0] == 'question'){
                array_push($questions, QuestionnaireModel::query('INSERT INTO "'.schema_name.'"."questionniare_question_value"("questionnaire_question_id", "value", "question_id") VALUES ('.$exp_key[1].', \''.$value.'\','.$questionaire[0]['questionnaire_id'].') RETURNING *'));
            }
        }//exit();
        return Array("questionnaire" => $questionaire[0],"questions" => $questions);
    }

    public static function delete($key){
        try {
            $sql = 'DELETE FROM "'.schema_name.'"."questionniare_question_value" WHERE "question_id" = '.intval($key);
            QuestionnaireModel::query($sql); 

            $sql = 'DELETE FROM "'.schema_name.'"."questionnaire" WHERE "questionnaire_id" = '.intval($key);
            QuestionnaireModel::query($sql);
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public function update($data){
        

        if ($data) {
            if (!isset($data['parent_farmer'])) {
               $parent_farmer = 'n';
            }else{
               $parent_farmer = 'y';
            }
            if (!isset($data['informant_relation'])) {
               $informant_relation = null;
            }else{
               $informant_relation = $data['informant_relation'];
            }
        }
        $sql = 'UPDATE "'.schema_name.'"."questionnaire" SET 
        "province" = \''.$data['province'].'\',
        "district" = \''.$data['district'].'\',
        "subdistrict" = \''.$data['subdistrict'].'\',
        "village" = \''.$data['village'].'\',
        "addressNumber" = \''.$data['addressNumber'].'\',
        "group" = \''.$data['group'].'\',
        "sample" = \''.$data['sample'].'\',
        "farmer_name" = \''.$data['farmer_name'].'\',
        "farmer_lastname" = \''.$data['farmer_lname'].'\',
        "farmer_id" = \''.$data['farmer_id'].'\',
        "farmer_registered" = \''.$data['farmer_registered'].'\',
        "informant_name" = \''.$data['informant_name'].'\',
        "informant_lastname" = \''.$data['informant_lname'].'\',
        "informant_relation" = \''.$informant_relation.'\',
        "informant_id" = \''.$data['informant_id'].'\',
        "informant_tel" = \''.$data['informant_tel'].'\',
        "inIrrigatedArea" = \''.$data['inIrrigatedArea'].'\',
        "staff_id" = \''.$data['staff_id'].'\',
        "survey_time" = \''.$data['survey_time'].'\',
        "staff_name" = \''.$data['staff_name'].'\',
        "staff_lname" = \''.$data['staff_lname'].'\',
        "office_id" = '.$data['office'].',
        "parent_farmer" = \''.$parent_farmer.'\' 
        WHERE "questionnaire_id" = '.$data['id'] .' RETURNING *';
        // echo $sql; exit9)
        QuestionnaireModel::query($sql);
        $questions = Array();
        foreach($data as $refkey => $value){
            $exp_key = explode('-', $refkey);
            if($exp_key[0] == 'question'){ 
                $sql = 'UPDATE "'.schema_name.'"."questionniare_question_value" SET "value" = \''.$value.'\' WHERE "question_id" = '.intval($data['id']).' AND "questionnaire_question_id" = '.intval($exp_key[1]).'';
                // echo $sql; 
                QuestionnaireModel::query($sql);
            }
        }
        // exit();

    }

    public static function get($key){
        $sql = 'SELECT * FROM "'.schema_name.'"."questionnaire" WHERE "questionnaire_id" = \''.intval($key).'\'';
        $questions = 'SELECT * FROM "'.schema_name.'"."questionniare_question_value" WHERE "question_id" = \''.intval($key).'\'';
        $survey_value = Array();
        foreach (QuestionnaireModel::query($questions) as $key => $value) {
            $survey_value[$value['questionnaire_question_id']] = $value['value'];

        }

        return Array('questionaire'=>QuestionnaireModel::query($sql)[0], 'value'=>$survey_value);
    }

    public static function getYears(){
        $sql ='SELECT DISTINCT survey."year" FROM "'.schema_name.'".survey';
        $out = QuestionnaireModel::query($sql);
        foreach ($out as $key => $value) {
            $out[$key]['year'] = $value['year']+543;
        }

        return $out;
    }

    public static function list($year=null){
        $sql = 
        // '
        //     SELECT 
        //         survey."year", 
        //         questionnaire.*,
        //         mt_province."province_name_th",
        //         master_those."those_name"
        //     FROM
        //         "'.schema_name.'".survey
        //         INNER JOIN
        //         "'.schema_name.'".questionnaire
        //         ON 
        //             survey."id" = questionnaire.survey_id
        //         INNER JOIN
        //         "db_oae".mt_province
        //         ON 
        //             questionnaire."province" = mt_province."province_code"
        //         INNER JOIN
        //         "db_oae".master_those
        //         ON 
        //             questionnaire."group" = master_those.those_id
        //             '
        'SELECT
"oae-survey".survey."year",
"oae-survey".questionnaire.questionnaire_id,
"oae-survey".questionnaire.survey_id,
"oae-survey".questionnaire.province,
"oae-survey".questionnaire.district,
"oae-survey".questionnaire.subdistrict,
"oae-survey".questionnaire.village,
"oae-survey".questionnaire."group",
"oae-survey".questionnaire.sample,
"oae-survey".questionnaire.survey_time,
db_oae.mt_province.province_name_th,
db_oae.mt_province.province_code,
"oae-survey".questionnaire.staff_name,
"oae-survey".questionnaire.staff_lname,
db_oae.mt_district.district_code,
db_oae.mt_district.province_id,
db_oae.mt_district.district_name_th,
db_oae.mt_district."id",
db_oae.mt_province."id",
db_oae.mt_subdistrict.subdistrict_code,
db_oae.mt_subdistrict.district_id,
db_oae.master_village.village_code,
db_oae.master_village.village_name,
db_oae.master_village.subdistrict_id,
db_oae.mt_subdistrict.subdistrict_name_th,
db_oae.mt_subdistrict."id"
FROM
"oae-survey".survey
INNER JOIN "oae-survey".questionnaire ON "oae-survey".survey."id" = "oae-survey".questionnaire.survey_id
INNER JOIN db_oae.mt_province ON "oae-survey".questionnaire.province = db_oae.mt_province.province_code
INNER JOIN db_oae.mt_district ON "oae-survey".questionnaire.district = db_oae.mt_district.district_code AND db_oae.mt_province."id" = db_oae.mt_district.province_id
INNER JOIN db_oae.mt_subdistrict ON "oae-survey".questionnaire.subdistrict = db_oae.mt_subdistrict.subdistrict_code AND db_oae.mt_subdistrict.district_id = db_oae.mt_district."id"
INNER JOIN db_oae.master_village ON "oae-survey".questionnaire.village = db_oae.master_village.village_code AND db_oae.mt_subdistrict."id" = db_oae.master_village.subdistrict_id';

        if($year != null){
            $sql = $sql .' WHERE
            survey."year" = '.$year;
        }

        $User = User::model()->findByPk(Yii::app()->user->id);
        $profile = $User->profile;
        if($year != null){
            if ($User->superuser != 1) {
                $sql = $sql .' AND
                questionnaire."office_id" = \''.$profile->county_id.'\'';
            }
        }else{
           if ($User->superuser != 1) {
                $sql = $sql .' WHERE
                questionnaire."office_id" = \''.$profile->county_id.'\'';
            } 
        }
        $sql = $sql . ' ORDER BY "oae-survey".questionnaire.questionnaire_id DESC LIMIT 100';
      // var_dump($sql);exit();
        return QuestionnaireModel::query($sql);
    }

    public static function listSearch($year,$Province,$District,$subdistrict,$village){
        $sql = 'SELECT
"oae-survey".survey."year",
"oae-survey".questionnaire.questionnaire_id,
"oae-survey".questionnaire.survey_id,
"oae-survey".questionnaire.province,
"oae-survey".questionnaire.district,
"oae-survey".questionnaire.subdistrict,
"oae-survey".questionnaire.village,
"oae-survey".questionnaire."group",
"oae-survey".questionnaire.sample,
"oae-survey".questionnaire.survey_time,
db_oae.mt_province.province_name_th,
db_oae.mt_province.province_code,
"oae-survey".questionnaire.staff_name,
"oae-survey".questionnaire.staff_lname,
db_oae.mt_district.district_code,
db_oae.mt_district.province_id,
db_oae.mt_district.district_name_th,
db_oae.mt_district."id",
db_oae.mt_province."id",
db_oae.mt_subdistrict.subdistrict_code,
db_oae.mt_subdistrict.district_id,
db_oae.master_village.village_code,
db_oae.master_village.village_name,
db_oae.master_village.subdistrict_id,
db_oae.mt_subdistrict.subdistrict_name_th,
db_oae.mt_subdistrict."id"
FROM
"oae-survey".survey
INNER JOIN "oae-survey".questionnaire ON "oae-survey".survey."id" = "oae-survey".questionnaire.survey_id
INNER JOIN db_oae.mt_province ON "oae-survey".questionnaire.province = db_oae.mt_province.province_code
INNER JOIN db_oae.mt_district ON "oae-survey".questionnaire.district = db_oae.mt_district.district_code AND db_oae.mt_province."id" = db_oae.mt_district.province_id
INNER JOIN db_oae.mt_subdistrict ON "oae-survey".questionnaire.subdistrict = db_oae.mt_subdistrict.subdistrict_code AND db_oae.mt_subdistrict.district_id = db_oae.mt_district."id"
INNER JOIN db_oae.master_village ON "oae-survey".questionnaire.village = db_oae.master_village.village_code AND db_oae.mt_subdistrict."id" = db_oae.master_village.subdistrict_id';
        if($year != null){
            $sql = $sql .' WHERE
            questionnaire."survey_id" = \''.$year.'\'';
        }
        if ($Province != null) {
            $sql = $sql .' AND
            questionnaire."province" = \''.$Province.'\'';
        }
        if ($District != null) {
            $sql = $sql .' AND
            questionnaire."district" = \''.$District.'\'';
        }
        if ($subdistrict != null) {
            $sql = $sql .' AND
            questionnaire."subdistrict" = \''.$subdistrict.'\'';
        }
        if ($village != null) {
            $sql = $sql .' AND
            questionnaire."village" = \''.$village.'\'';
        }
        $User = User::model()->findByPk(Yii::app()->user->id);
        $profile = $User->profile;
        if ($User->superuser != 1) {
            $sql = $sql .' AND
            questionnaire."office_id" = \''.$profile->county_id.'\'';
        }
        $sql = $sql . ' ORDER BY "oae-survey".questionnaire.questionnaire_id DESC LIMIT 100';
  
        return QuestionnaireModel::query($sql);
    }

    public static function form(){
        $sql = 'SELECT
                survey."year", 
                survey."start", 
                survey."end", 
                questionnaire_map."isRequired",
                questionnaire_map."order",
                questionnaire_question.title, 
                questionnaire_question.subtitle, 
                questionnaire_question.question_id
            FROM
                "'.schema_name.'".survey
                INNER JOIN
                "'.schema_name.'".questionnaire_map
                ON 
                    survey."id" = questionnaire_map.survey
                INNER JOIN
                "'.schema_name.'".questionnaire_question
                ON 
                    questionnaire_map.question = questionnaire_question.question_id
            WHERE
                survey."id" = (
                    SELECT
                        survey."id"
                    FROM
                        "'.schema_name.'".survey
                    ORDER BY
                        survey."id" DESC
                    LIMIT 1
                    OFFSET 0)
            ORDER BY "order"';
        return QuestionnaireModel::query($sql);
    }

    public static function getQuestion($key = null){
        $sql = 'SELECT * FROM "'.schema_name.'".questionnaire_question';
        if($key != null && isset($key)){
            $sql = $sql.' WHERE question_id = '.intval($key);
        }
        return QuestionnaireModel::query($sql);
    }

    public static function getSearchQuestion($catgory_question,$question){
        $sql = 'SELECT * FROM "'.schema_name.'".questionnaire_question';
        if($catgory_question != null && isset($catgory_question)){
            $sql = $sql.' WHERE question_id = '.intval($catgory_question);
        }
        if($question != null && isset($question)){
            if($catgory_question != null && isset($catgory_question)){
                $sql = $sql.' AND title = \''.$question.'\'';
            }else{
                $sql = $sql.' WHERE title LIKE \''.$question.'\'';
            }
        }
       // var_dump($sql);exit();
        return QuestionnaireModel::query($sql);
    }

    public static function query($sql){
        $command = Yii::app()->db->createCommand($sql);
        return $command->queryAll();
    }

    public static function questionCreate($data){
        $sql = 'INSERT INTO "'.schema_name.'"."questionnaire_question"("question_id", "title", "subtitle") VALUES ('.$data['question_id'].', \''.$data['title'].'\', \''.$data['subtitle'].'\') RETURNING *';
        return QuestionnaireModel::query($sql);
    }

    public static function questionDelete($key){
        $sql = 'DELETE FROM "'.schema_name.'"."questionnaire_map" WHERE "question" = '.intval($key);
        QuestionnaireModel::query($sql);
        $sql = 'DELETE FROM "'.schema_name.'"."questionniare_question_value" WHERE "questionnaire_question_id" = '.intval($key);
        QuestionnaireModel::query($sql);
        $sql = 'DELETE FROM "'.schema_name.'"."questionnaire_question" WHERE "question_id" = '.intval($key);
        QuestionnaireModel::query($sql);
        return true;
    }

    public static function questionUpdate($key, $data){
        $sql = 'UPDATE "'.schema_name.'"."questionnaire_question" SET "title" = \''.$data["title"].'\', "subtitle" = \''.$data["subtitle"].'\' WHERE "question_id" = '.intval($key);
        QuestionnaireModel::query($sql);
    }

    public static function questionLastId(){
        $sql = 'SELECT questionnaire_question.question_id FROM "'.schema_name.'".questionnaire_question ORDER BY questionnaire_question.question_id DESC LIMIT 1 OFFSET 0';
        return QuestionnaireModel::query($sql)[0]['question_id'];
    }

    public static function getSurvey($key){
        $sql = 'SELECT * FROM "'.schema_name.'"."survey"';
        if($key != null && isset($key)){
            $sql = $sql.' WHERE id = '.intval($key);
            $questions = 'SELECT * FROM "'.schema_name.'"."questionnaire_map" WHERE "survey" = \''.$key.'\'';
            return Array('survey'=>QuestionnaireModel::query($sql), 'questions'=> QuestionnaireModel::query($questions));
        }else{
            return QuestionnaireModel::query($sql);
        }
    }

    public static function surveyList(){
        $sql = 'SELECT * FROM "'.schema_name.'"."survey" ORDER BY "end" DESC';
        return QuestionnaireModel::query($sql);
    }

    public static function surveySearchList($year_start,$year_end,$date_start,$date_end){
        $sql = 'SELECT * FROM "'.schema_name.'"."survey"';
        if ($year_start != null && $year_end != null) {
          $sql = $sql.' WHERE year BETWEEN '.intval($year_start-543).' AND '.intval($year_end-543);
        }
        if ($date_start != null && $date_end != null) {
             $start_explode = explode("-", $date_start);
             $end_explode = explode("-", $date_end);

             $start = ($start_explode[2]-543).'-'.$start_explode[1].'-'.$start_explode[0];
             $end   = ($end_explode[2]-543).'-'.$end_explode[1].'-'.$end_explode[0];

             $sql = $sql.' WHERE "start" BETWEEN \''.$start.'\' AND \''.$end.'\'';
        }
        $sql = $sql.' ORDER BY "end" DESC';
        //var_dump($sql);exit();
        return QuestionnaireModel::query($sql);
    }

    public static function getSurveyGroup($key, $group){
        $sql = 'SELECT
            survey_map.*,
            survey."name",
            question.question_group_id
        FROM
            "'.schema_name.'".survey_map
            INNER JOIN
            "'.schema_name.'".survey
            ON 
                survey_map.survey = survey."id"
            INNER JOIN
            "'.schema_name.'".question
            ON 
                survey_map.question = question.question_id
        WHERE
            survey_map.survey = '.$key.' AND
            question.question_group_id = '.$group.'';
        $data = QuestionnaireModel::query($sql);
        if (empty($data)) {
            $sql_survey = 'SELECT * FROM "'.schema_name.'"."survey" WHERE survey.id ='.$key.' ';
            $data_survey =  QuestionnaireModel::query($sql_survey);
            array_push($data, [
                'survey' => $data_survey[0]["id"],
                'name' => $data_survey[0]["name"],
            ]);
        }
        return $data;
    }

    public static function surveyListWithoutGroup($id){
        $sql = 'SELECT
            * 
        FROM
            "'.schema_name.'"."survey" 
        WHERE
            NOT EXISTS ( SELECT DISTINCT survey FROM "'.schema_name.'".survey_map INNER JOIN "'.schema_name.'".question ON survey_map.question = question.question_id WHERE question_group_id = '.$id.' )';
        return QuestionnaireModel::query($sql);
    }

    public static function surveyListWithGroup($id){
        $sql = 'SELECT
            * 
        FROM
            "'.schema_name.'"."survey" 
        WHERE
            EXISTS ( SELECT DISTINCT survey FROM "'.schema_name.'".survey_map INNER JOIN "'.schema_name.'".question ON survey_map.question = question.question_id WHERE question_group_id = '.$id.' )';
        return QuestionnaireModel::query($sql);
    }

    public static function surveyListWithGroupSearch($id,$search_year)
    {
        $sql = 'SELECT
            * 
        FROM
            "'.schema_name.'"."survey" 
        WHERE
            EXISTS ( SELECT DISTINCT survey FROM "'.schema_name.'".survey_map INNER JOIN "'.schema_name.'".question ON survey_map.question = question.question_id WHERE question_group_id = '.$id.' )';
        if ($search_year != null) {
            $sql = $sql.' AND "oae-survey".survey."year" ='.($search_year-543);
        }    
    
        return QuestionnaireModel::query($sql);
    }

    public static function surveyInsert($data){
        $year =  (substr($data['date-start'], -4)-543);
        $start_explode = explode("-", $data['date-start']);
        $end_explode = explode("-", $data['date-end']);
     
        $start = ($start_explode[2]-543).'-'.$start_explode[1].'-'.$start_explode[0];
        $end   = ($end_explode[2]-543).'-'.$end_explode[1].'-'.$end_explode[0];
        $sql = 'INSERT INTO "'.schema_name.'"."survey"("year", "start", "end", "name") VALUES ('.$year.', \''.$start.'\', \''.$end.'\', \''.$data['survey-name'].'\') RETURNING *';
        // print_r($sql);exit;
        $survey = QuestionnaireModel::query($sql)[0];
        if ($data["clone-survey"] == "none") {
            $mapper = [];
            foreach ($data as $key => $value) {
                $exp_key = explode("-",$key);
                if($exp_key[0] == 'question'){
                    if($value == 'true'){
                        array_push($mapper,'('.$survey['id'].', '.$exp_key[1].', true)');
                    }else{
                        array_push($mapper,'('.$survey['id'].', '.$exp_key[1].', false)');
                    }
                }
            }
            $sql = 'INSERT INTO "'.schema_name.'"."questionnaire_map"("survey", "question", "isRequired") VALUES ';
            for ($i=0; $i < count($mapper); $i++) { 
                if($i == count($mapper)-1){
                    $sql = $sql . $mapper[$i];
                }else{
                    $sql = $sql . $mapper[$i] .',';
                }
            }
            $sql = $sql . ' RETURNING *';
            $map = QuestionnaireModel::query($sql)[0];
        }else{
            $sqlclone = 'SELECT "'.schema_name.'".questionnaire_map.question, "'.schema_name.'".questionnaire_map."isRequired", "'.schema_name.'".questionnaire_map."order" FROM "'.schema_name.'".questionnaire_map WHERE "'.schema_name.'".questionnaire_map.survey = '.$data["clone-survey"].'';
            $mapper = [];
            $survey_clone = QuestionnaireModel::query($sqlclone);
            foreach ($survey_clone as $key => $value) {
                if($value["isRequired"] == 'true'){
                    array_push($mapper,'('.$survey['id'].', '.$value["question"].', true)');
                }else{
                    array_push($mapper,'('.$survey['id'].', '.$value["question"].', false)');
                }
            }
            $sql = 'INSERT INTO "'.schema_name.'"."questionnaire_map"("survey", "question", "isRequired") VALUES ';
            for ($i=0; $i < count($mapper); $i++) { 
                if($i == count($mapper)-1){
                    $sql = $sql . $mapper[$i];
                }else{
                    $sql = $sql . $mapper[$i] .',';
                }
            }
            $sql = $sql . ' RETURNING *';
            $map = QuestionnaireModel::query($sql)[0];

            $sqlclone = 'SELECT "'.schema_name.'".survey_map.question, "'.schema_name.'".survey_map."size", "'.schema_name.'".survey_map."order", "'.schema_name.'".survey_map."isRequired" FROM "'.schema_name.'".survey_map WHERE "'.schema_name.'".survey_map.survey = '.$data["clone-survey"].'';
            $survey_map_clone = QuestionnaireModel::query($sqlclone);
            $mapper = [];
            foreach ($survey_map_clone as $keysurvey => $valuesurvey) {
                if($valuesurvey["isRequired"] == 'true'){
                    array_push($mapper,'('.$survey['id'].', '.$valuesurvey["question"].', '.$valuesurvey["size"].', '.$valuesurvey["order"].', true)');
                }else{
                    array_push($mapper,'('.$survey['id'].', '.$valuesurvey["question"].', '.$valuesurvey["size"].', '.$valuesurvey["order"].', false)');
                }
            }
            $sql = 'INSERT INTO "'.schema_name.'"."survey_map"("survey", "question", "size", "order", "isRequired") VALUES ';
            for ($i=0; $i < count($mapper); $i++) { 
                if($i == count($mapper)-1){
                    $sql = $sql . $mapper[$i];
                }else{
                    $sql = $sql . $mapper[$i] .',';
                }
            }
            $sql = $sql . ' RETURNING *';
            $map = QuestionnaireModel::query($sql)[0];
        }
        return $survey;
    }

    public static function surveyDelete($key){
        $sql = 'DELETE FROM "'.schema_name.'"."questionnaire" WHERE "survey_id" = '.intval($key);
        QuestionnaireModel::query($sql);
        $sql = 'DELETE FROM "'.schema_name.'"."survey" WHERE "id" = '.intval($key);
        QuestionnaireModel::query($sql);
    }

    public static function surveyUpdate($key, $data){
        // It needs to delete because of data collision.
        QuestionnaireModel::query('DELETE FROM "'.schema_name.'"."questionnaire_map" WHERE "survey" = '.intval($key));
        // exit();
        $year =  (substr($data['date-start'], -4)-543);
        // $start = substr($data['date-start'], -4);
        // $end   = substr($data['date-end'], -4);
        $start_explode = explode("-", $data['date-start']);
        $end_explode = explode("-", $data['date-end']);
     
        $start = ($start_explode[2]-543).'-'.$start_explode[1].'-'.$start_explode[0];
        $end   = ($end_explode[2]-543).'-'.$end_explode[1].'-'.$end_explode[0];

        $sql = 'UPDATE "'.schema_name.'"."survey" SET "year" = \''.$year.'\', "start" = \''.$start.'\', "end" = \''.$end.'\', "name" = \''.$data['survey-name'].'\' WHERE "id" = '.$key.' RETURNING *';
        // $sql = 'INSERT INTO "'.schema_name.'"."survey"("year", "start", "end", "name") VALUES ('.substr($data['date-start'], 0,4).', \''.$start.'\', \''.$end.'\', \''.$data['survey-name'].'\') RETURNING *';
        // print_r($sql);exit;
        $survey = QuestionnaireModel::query($sql)[0];
        // print_r($survey);
        $mapper = [];
        foreach ($data as $datakey => $value) {
            $exp_key = explode("-",$datakey);
            if($exp_key[0] == 'question'){
                if($value == 'true'){
                    array_push($mapper,'('.$survey['id'].', '.$exp_key[1].', true)');
                }else{
                    array_push($mapper,'('.$survey['id'].', '.$exp_key[1].', false)');
                }
            }
        }
        $sql = 'INSERT INTO "'.schema_name.'"."questionnaire_map"("survey", "question", "isRequired") VALUES ';
        for ($i=0; $i < count($mapper); $i++) { 
            if($i == count($mapper)-1){
                $sql = $sql . $mapper[$i];
            }else{
                $sql = $sql . $mapper[$i] .',';
            }
        }
        $sql = $sql . ' RETURNING *';
        // print_r($sql);exit();
        $map = QuestionnaireModel::query($sql)[0];
        return $survey;
    }

    public static function surveyQuestionGroupMap($survey_id, $data){
        $mapper = [];
        // foreach ($data as $key => $value) {
        //     $exp_key = explode("-",$key);
        //     if($exp_key[0] == 'question'){
        //         array_push($mapper,'('.$survey_id.', '.$exp_key[1].', '.(filter_var($value, FILTER_VALIDATE_BOOLEAN) ? 'TRUE': 'FALSE').')');
        //     }
        // }
        $array_id = "";
        foreach ($data as $key => $value) {
            $exp_key = explode("-",$key);
            if ($exp_key[0] == "question") {
                $checkQuestionGroup = $exp_key[1];
                $array_id= $array_id.$exp_key[1].",";
            }
            if ($exp_key[0] == "group_id") {
                $group_id = $value;
            }
        }

        if (isset($checkQuestionGroup)) {
           $sql = 'SELECT * FROM "'.schema_name.'"."question" WHERE question_id = '.$checkQuestionGroup.'';
           $checkQuestion = QuestionnaireModel::query($sql);
           $QuestionGroupID = $checkQuestion[0]["question_group_id"];
           $sql = 'SELECT question_id FROM "'.schema_name.'"."question" WHERE question_group_id = '.$QuestionGroupID.'';
           $QuestionGroupID = QuestionnaireModel::query($sql);
           $array_have_question = "";
           foreach ($QuestionGroupID as $key => $value) {
            $array_have_question= $array_have_question.$value['question_id'].",";
        }
        $array_have_question = substr($array_have_question, 0, -1);
        $dataHave = array();
        if ($array_id != "") {
            $array_id = substr($array_id, 0, -1);
            $sql = 'SELECT * FROM "'.schema_name.'"."survey_map" WHERE survey = '.$survey_id.' AND question IN ('.$array_id.')';
            $dataHave = QuestionnaireModel::query($sql);
            $sql = 'SELECT * FROM "'.schema_name.'"."survey_map" WHERE survey = '.$survey_id.' AND question NOT IN ('.$array_id.') AND question IN ('.$array_have_question.')';
            $dataDelete = QuestionnaireModel::query($sql);
            if (!empty($dataDelete)) {
                foreach ($dataDelete as $key => $value) {
                    $sql = 'DELETE
                    FROM
                    "'.schema_name.'".survey_map
                    WHERE
                    survey_map.survey = '.$value["survey"].' AND
                    survey_map.question = '.$value["question"].'';
                    QuestionnaireModel::query($sql); 
                }

            }
        }
        $count = count($dataHave);
        foreach ($data as $key => $value) {
            $exp_key = explode("-",$key);
            if($exp_key[0] == 'question'){
                $sql = 'SELECT * FROM "'.schema_name.'"."survey_map" WHERE survey = '.$survey_id.' AND question ='.$exp_key[1].'';
                $dataQuery = QuestionnaireModel::query($sql);
                if (empty($dataQuery)) {
                    array_push($mapper,'('.$survey_id.', '.$exp_key[1].', '.(filter_var($value, FILTER_VALIDATE_BOOLEAN) ? 'TRUE': 'FALSE').', '.$count.')');
                    $count++;
                }else{
                    $sqlUpdate = 'UPDATE "'.schema_name.'"."survey_map" SET "isRequired" = '.(filter_var($value, FILTER_VALIDATE_BOOLEAN) ? 'TRUE': 'FALSE').' WHERE "survey" = '.intval($survey_id).' AND "question" = '.intval($exp_key[1]).' RETURNING *';
                    QuestionnaireModel::query($sqlUpdate);
                }

            }
        }
        if (!empty($mapper)) {
            $sql = 'INSERT INTO "'.schema_name.'"."survey_map"("survey", "question", "isRequired","order") VALUES ';
            for ($i=0; $i < count($mapper); $i++) { 
                if($i == count($mapper)-1){
                    $sql = $sql . $mapper[$i];
                }else{
                    $sql = $sql . $mapper[$i] .',';
                }
            }
            $sql = $sql . ' RETURNING *';
            QuestionnaireModel::query($sql);
        }
    }else{
        $sql = 'DELETE
        FROM
        "'.schema_name.'".survey_map
        WHERE
        survey_map.survey = '.$survey_id.'
        ';
        QuestionnaireModel::query($sql); 
    }
       
        return true;
    }
    public function surveyQuestionGroupUnmap($group, $key)
    {
        $sql = 'DELETE
        FROM
            "'.schema_name.'".survey_map
            USING
            "'.schema_name.'".question
        WHERE
            survey_map.question = question.question_id AND
            survey_map.survey = '.intval($key).' AND
            question.question_group_id = '.intval($group);
        return QuestionnaireModel::query($sql);  
    }

    public static function answerInsert($question_id, $title, $description_json){
        $sql = 'INSERT INTO "'.schema_name.'"."answer"("question_id", "title", "description_json") VALUES ('.$question_id.', \''.$title.'\', \''.json_encode($description_json).'\') RETURNING *';
        return QuestionnaireModel::query($sql);
    } 
    public static function answerDelete($key){
        $sql = 'DELETE FROM "'.schema_name.'"."answer" WHERE "answer_id" = '.intval($key);
        return QuestionnaireModel::query($sql);
    } 
    public static function answerUpdate($key,$question_id, $title, $description_json){
        $sql = 'UPDATE "'.schema_name.'"."answer" SET "question_id" = '.$question_id.', "title" = \''.$title.'\', "description_json" = \''.json_encode($description_json).'\' WHERE "answer_id" = '.intval($key).' RETURNING *';
        return QuestionnaireModel::query($sql);
    } 

    public static function answerList($id){
        $sql = '
        SELECT
            survey."year", 
            answer.*,
            question.question
        FROM
            "'.schema_name.'".answer
            INNER JOIN
            "'.schema_name.'".question
            ON 
                answer.question_id = question.question_id
            INNER JOIN
            "'.schema_name.'".survey_map
            ON 
                question.question_id = survey_map.question
            INNER JOIN
            "'.schema_name.'".survey
            ON 
                survey_map.survey = survey."id"
        WHERE
            question.question_group_id = '.$id.'
        ';
       // var_dump($sql);;exit();
        $data = QuestionnaireModel::query($sql);
        $out = Array();
        foreach($data as $key => $value){
            if(!isset($out[$value['answer_id']])){
                $out[$value['answer_id']] = Array(
                    "id"=> $value['answer_id'],
                    "title"=> $value['title'],
                    "description"=> json_decode($value['description_json']),
                    "question"=> $value['question'],
                    "years" => Array());
            }
            array_push($out[$value['answer_id']]['years'], $value['year']);
            // print_r($value);
        }
        return $out;
    }

    public static function answerListSearch($id,$question,$year){
        $sql = '
        SELECT
            survey."year", 
            answer.*,
            question.question
        FROM
            "'.schema_name.'".answer
            INNER JOIN
            "'.schema_name.'".question
            ON 
                answer.question_id = question.question_id
            INNER JOIN
            "'.schema_name.'".survey_map
            ON 
                question.question_id = survey_map.question
            INNER JOIN
            "'.schema_name.'".survey
            ON 
                survey_map.survey = survey."id"
        WHERE
            question.question_group_id = '.$id.'
        ';
        if ($question != null) {
           $sql = $sql .' AND "oae-survey".answer.answer_id = '.$question;
        }
        if ($year != null) {
             $sql = $sql .' AND "oae-survey".survey."year" = '.($year-543);
        }
        // var_dump($sql);;exit();
        $data = QuestionnaireModel::query($sql);
        $out = Array();
        foreach($data as $key => $value){
            if(!isset($out[$value['answer_id']])){
                $out[$value['answer_id']] = Array(
                    "id"=> $value['answer_id'],
                    "title"=> $value['title'],
                    "description"=> json_decode($value['description_json']),
                    "question"=> $value['question'],
                    "years" => Array());
            }
            array_push($out[$value['answer_id']]['years'], $value['year']);
            // print_r($value);
        }
        return $out;
    }

    public function orderMap($survey, $data)
    {
        foreach ($data as $key => $value) {
            $sql = 'UPDATE "'.schema_name.'"."questionnaire_map" SET "order" = '.$key.' WHERE "survey" = '.intval($survey).' AND "question" = '.$value.';';
            QuestionnaireModel::query($sql);
        }
    }
}