<?php

Class Helpers
{
    public $resourceData;

    public static function lib()
    {
        return new Helpers();
    }

    public function getStatePermission($user){
        $dataPermissionGroup = array("1","7","15");
        $state = false;
        foreach ($dataPermissionGroup as $key => $value) {
            if(in_array($value,json_decode($user->group))){
                $state = true;
                break;
            }
        }
        return $state;
    }

    public function changeFormatDateNew($date,$type=null)
    {
        if($type=='date' && $date != ''){
            $date = explode('-', $date);
            $day = $date[0];
            $month = $date[1];
            $year = $date[2]+543;
            if($year == '543' && $month == '00' && $day == '00'){
                return 'ยังไม่เข้าสู่ระบบ';
            }
            switch ($month) {
                case '01':
                    $month = 'มกราคม';
                    break;
                case '02':
                    $month = 'กุมภาพันธ์';
                    break;
                case '03':
                    $month = 'มีนาคม';
                    break;
                case '04':
                    $month = 'เมษายน';
                    break;
                case '05':
                    $month = 'พฤษภาคม';
                    break;
                case '06':
                    $month = 'มิถุนายน';
                    break;
                case '07':
                    $month = 'กรกฎาคม';
                    break;
                case '08':
                    $month = 'สิงหาคม';
                    break;
                case '09':
                    $month = 'กันยายน';
                    break;
                case '10':
                    $month = 'ตุลาคม';
                    break;
                case '11':
                    $month = 'พฤศจิกายน';
                    break;
                case '12':
                    $month = 'ธันวาคม';
                    break;
                default:
                    $month = 'error';
                    break;
            }
            return $day.' '.$month.' '.$year;
        } else if($date != '') {
            if($date == '0000-00-00'){
            return '-';
            }else{
            $date = explode('-', $date);
            $day = $date[0];
            $month = $date[1];
            $year = $date[2]+543;
            switch ($month) {
                case '01':
                    $month = 'มกราคม';
                    break;
                case '02':
                    $month = 'กุมภาพันธ์';
                    break;
                case '03':
                    $month = 'มีนาคม';
                    break;
                case '04':
                    $month = 'เมษายน';
                    break;
                case '05':
                    $month = 'พฤษภาคม';
                    break;
                case '06':
                    $month = 'มิถุนายน';
                    break;
                case '07':
                    $month = 'กรกฎาคม';
                    break;
                case '08':
                    $month = 'สิงหาคม';
                    break;
                case '09':
                    $month = 'กันยายน';
                    break;
                case '10':
                    $month = 'ตุลาคม';
                    break;
                case '11':
                    $month = 'พฤศจิกายน';
                    break;
                case '12':
                    $month = 'ธันวาคม';
                    break;
                default:
                    $month = 'error';
                    break;
            }
            return $day.' '.$month.' '.$year;
         }
        }
        return $date;
    }

    public function changeFormatDate($date,$type=null)
    {
        if($type=='datetime' && $date != ''){
            $date = explode('-', $date);
            $year = $date[0]+543;
            $month = $date[1];
            $day = $date[2];
            $day = explode(' ', $day);
            $days = $day[0];
            $time = explode(':', $day[1]);
            $hour = $time[0];
            $minute = $time[1];
            if($year == '543' && $month == '00' && $days == '00'){
                return 'ยังไม่เข้าสู่ระบบ';
            }
            switch ($month) {
                case '01':
                    $month = 'มกราคม';
                    break;
                case '02':
                    $month = 'กุมภาพันธ์';
                    break;
                case '03':
                    $month = 'มีนาคม';
                    break;
                case '04':
                    $month = 'เมษายน';
                    break;
                case '05':
                    $month = 'พฤษภาคม';
                    break;
                case '06':
                    $month = 'มิถุนายน';
                    break;
                case '07':
                    $month = 'กรกฎาคม';
                    break;
                case '08':
                    $month = 'สิงหาคม';
                    break;
                case '09':
                    $month = 'กันยายน';
                    break;
                case '10':
                    $month = 'ตุลาคม';
                    break;
                case '11':
                    $month = 'พฤศจิกายน';
                    break;
                case '12':
                    $month = 'ธันวาคม';
                    break;
                default:
                    $month = 'error';
                    break;
            }
            return $days.' '.$month.' '.$year. ' '.$hour.':'.$minute.' น.';
        } else if($date != '') {
            $date = explode('-', $date);
            $year = $date[0]+543;
            $month = $date[1];
            $day = $date[2];
            $day = explode(' ', $day);
            $day = $day[0];
            switch ($month) {
                case '01':
                    $month = 'มกราคม';
                    break;
                case '02':
                    $month = 'กุมภาพันธ์';
                    break;
                case '03':
                    $month = 'มีนาคม';
                    break;
                case '04':
                    $month = 'เมษายน';
                    break;
                case '05':
                    $month = 'พฤษภาคม';
                    break;
                case '06':
                    $month = 'มิถุนายน';
                    break;
                case '07':
                    $month = 'กรกฎาคม';
                    break;
                case '08':
                    $month = 'สิงหาคม';
                    break;
                case '09':
                    $month = 'กันยายน';
                    break;
                case '10':
                    $month = 'ตุลาคม';
                    break;
                case '11':
                    $month = 'พฤศจิกายน';
                    break;
                case '12':
                    $month = 'ธันวาคม';
                    break;
                default:
                    $month = 'error';
                    break;
            }
            return $day.' '.$month.' '.$year;
        }
        return $date;
    }

    
    public function PeriodDate($dateStart,$full){
        $date = explode('-', $dateStart);
        $year = $date[0];
        $month = $date[1];
        $day = $date[2];
        $day = explode(' ', $day);
        $days = $day[0];
        switch ($month) {
                case '01':
                    $month = 'Jan';
                    break;
                case '02':
                    $month = 'Feb';
                    break;
                case '03':
                    $month = 'Mar';
                    break;
                case '04':
                    $month = 'Apr';
                    break;
                case '05':
                    $month = 'May';
                    break;
                case '06':
                    $month = 'Jun';
                    break;
                case '07':
                    $month = 'Jul';
                    break;
                case '08':
                    $month = 'Aug';
                    break;
                case '09':
                    $month = 'Sep';
                    break;
                case '10':
                    $month = 'Oct';
                    break;
                case '11':
                    $month = 'Nov';
                    break;
                case '12':
                    $month = 'Dec';
                    break;
                default:
                    $month = 'error';
                    break;
            }
            if($full){
                return $strDate = $days." ".$month." ".$year;
            }else{
                return $strDate = $days." ".$month;
            }
            

    }

    public function getControllerActionId($parameter = null)
        {   

            if(!empty(Yii::app()->controller->action->id)){
                $model = new LogUsers;

               $profile = profile::model()->findbyPk(Yii::app()->session['ID']);
               if($profile->type_user==1){
                        $status = 'Admin';
               }else{
                            $status = 'user';
               }
                // var_dump($profile);exit();
               // var_dump(Yii::app()->controller->module->id);exit();
                $model->controller = Yii::app()->controller->id;
                $model->action = Yii::app()->controller->action->id;
                if(!empty($_GET['id'])){
                    $model->parameter = $_GET['id'];

                }
                if($parameter != null){
                    $model->parameter = $parameter;
                }
                // if(Yii::app()->controller->module->id){
                //     $model->module = Yii::app()->controller->module->id;
                // }
                $model->module = $status;
                $model->user_id = Yii::app()->session['ID'];
                $model->create_date = date('Y-m-d H:i:s');
                $model->save();
            }
        }

    public function SendMail($to, $subject, $message, $fromText = 'Oae')
{

    require dirname(__FILE__)."/../extensions/mailer/phpmailer/src/Exception.php";
    require dirname(__FILE__)."/../extensions/mailer/phpmailer/src/PHPMailer.php";
    require dirname(__FILE__)."/../extensions/mailer/phpmailer/src/SMTP.php";


    $adminEmail = 'OaeTest2021@gmail.com';
    $adminEmailPass = 'Admin@123#';

        $mail =  new PHPMailer(true);
        $mail->SMTPOptions = array(
            'ssl' => array(
            'verify_peer' => false,
            'verify_peer_name' => false,
            'allow_self_signed' => true
            )
        );
        
        $mail->ClearAddresses();
        $mail->CharSet = 'utf-8';
        $mail->IsSMTP();
        //$mail->Host = 'smtp.office365.com'; // gmail server
        $mail->Host = 'smtp.gmail.com';
        $mail->Port = '587'; // port number
        $mail->SMTPSecure = "tls";
        $mail->SMTPKeepAlive = true;
        $mail->Mailer = "smtp";
        $mail->SMTPAuth = true;
        $mail->SMTPDebug = false;
        $mail->Username = $adminEmail;
        $mail->Password = $adminEmailPass;
        $mail->SetFrom($adminEmail, $fromText);
        $mail->AddAddress($to['email'], 'คุณ' . $to['firstname'] . ' ' . $to['lastname']);
        $mail->Subject = $subject;
        $mail->Body = $message;
        $mail->IsHTML(true);

       // $mail->SMTPSecure = 'tls';


        return $mail->Send();
    }

    public function listcounty($model,$id)
	{
        // var_dump($id);exit();
		$list = CHtml::listData(MasterCounty::model()->findAll(array(
            "condition"=>'active = 1','order'=>'county_id')),'county_id', 'county_name');
            // var_dump($list);exit();
		return CHtml::activeDropDownList($model,'county_id',$list , array(
			'class'=>'form-control'
		));
	}

    public function listdistrict($model,$id)
	{
        // var_dump($id);exit();
		$list = CHtml::listData(MtDistrict::model()->findAll(array(
            "condition"=>'active = 1','order'=>'id')),'id', 'district_name_th');
            // var_dump($list);exit();
		return CHtml::activeDropDownList($model,'district_id',$list , array(
			'class'=>'form-control'
		));
	}

    public function listcountyWithSelect($model,$id)
    {
        $list = CHtml::listData(MasterCounty::model()->findAll(array(
            "condition"=>'active = 1','order'=>'county_id')),'county_id', 'county_name');
            // var_dump($list);exit();
		return CHtml::activeDropDownList($model,'county_id',$list , array(
			'class'=>'form-control',
            'empty' => '---- เลือกเขต ----'
		));
    }

    public function listdistrictWithSelect($model,$id)
    {
        $list = CHtml::listData(MtDistrict::model()->findAll(array(
            "condition"=>'active = 1','order'=>'id')),'id', 'district_name_th');
            // var_dump($list);exit();
		return CHtml::activeDropDownList($model,'id',$list , array(
			'class'=>'form-control',
            'empty' => '---- เลือกอำเภอ ----'
		));
    }

    public function listprovinceWithSelect($model,$id)
    {
        $list = CHtml::listData(MtProvince::model()->findAll(array(
            "condition"=>'active = 1','order'=>'id')),'id','province_name_th');
        return CHtml::activeDropDownList($model,'province_id',$list, array(
            'class'=>'form-control',
            'empty'=> '----เลือกจังหวัด----'
        ));
    }

    public function listprovince($model,$id)
    {
        $list = CHtml::listData(MtProvince::model()->findAll(array(
            "condition"=>'active = 1','order'=>'id')),'id','province_name_th');
            return CHtml::activeDropDownList($model,'province_id',$list, array(
                'class'=>'form-control'
            ));
    }

    public function listsubdistrictWithSelect($model,$id)
    {
        $list = CHtml::listData(MtSubDistrict::model()->findAll(array(
            "condition"=>'active = 1','order'=>'id')),'id','subdistrict_name_th');
            return CHtml::activeDropDownList($model,'subdistrict_id',$list, array(
                'class'=>'form-control',
                'empty'=> '----เลือกตำบล----'
            ));
    }

    public function listsubdistrict($model,$id)
    {
        $list = CHtml::listData(MtSubDistrict::model()->findAll(array(
            "condition"=>'active = 1','order'=>'id')),'id','subdistrict_name_th');
            return CHtml::activeDropDownList($model,'subdistrict_id',$list, array(
                'class'=>'form-control'
            ));
    }
}
