<?php

class AdminUserController extends Controller
{
    public $layout = 'admin';
    
	public function init()
	{
        if(!Yii::app()->user->id ){
            $this->redirect(Yii::app()->baseurl.'/user/login?admin=1');
        }else{
            $user = User::model()->findByPk(Yii::app()->user->id);
            if($user->superuser != 1){
                $this->redirect(Yii::app()->baseurl);

            }
        }
	}
	
	// private $_model;
	public function filters()
	{
		return array(
            'accessControl', // perform access control for CRUD operations
            // 'rights',
        );
	}

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    // public function accessRules()
    // {
    //     return array(
    //     	array('allow',  // allow all users to perform 'index' and 'view' actions
    //         	'actions' => array('index', 'view'),
    //         	'users' => array('*'),
    //         	),
    //         array('allow',
    //             // กำหนดสิทธิ์เข้าใช้งาน actionIndex
    //             'actions' => AccessControl::check_action(),
    //             // ได้เฉพาะ group 1 เท่านั่น
    //             'expression' => 'AccessControl::check_access()',
    //             ),
    //         array('deny',  // deny all users
    //             'users' => array('*'),
    //             ),
    //         );
    // }

    public function actionIndex()
    {
    	$model=new UsersAdmin('search');
        $model->unsetAttributes();  // clear any default values
        $model->superuser = 1;
        if(isset($_GET['UsersAdmin'])){

        	$model->name_search=$_GET['UsersAdmin']['name_search'];

        }
        $this->render('index',array(
        	'model'=>$model,
        ));
    }
    public function actionCreate(){

    	$model=new UserAdmin;
    	$profile=new Profile;
    	$this->performAjaxValidation(array($model,$profile));
    	if(isset($_POST['ajax']) && $_POST['ajax']==='registration-form')
    	{
    		echo UActiveForm::validate(array($model,$profile));
    		Yii::app()->end();
    	}

    	if(isset($_POST['UserAdmin']))
    	{		
    		$PGoup = $_POST['PGoup'];           
    		$PGoup = json_encode($PGoup);

    		$model->username = $_POST['UserAdmin']['username'];
    		$model->email = $_POST['UserAdmin']['email'];
    		$model->group = $PGoup;
    		$model->password = $_POST['UserAdmin']['password'];
    		$model->verifyPassword = $_POST['UserAdmin']['verifyPassword'];
    		$model->create_at = date("Y-m-d H:i:s");
    		$model->online_status = 0;
    		$model->repass_status = 0;
    		$model->del_status = 0;
    		$model->type_register = 1;
            $model->status = 1;
            $model->superuser = 1;

    		$model->activkey=UserModule::encrypting(microtime().$model->password);
    		$profile->attributes=$_POST['Profile'];
    		$profile->user_id=0;

    		if($model->validate()) {	
    			$model->password=UserModule::encrypting($model->password);
    			$model->verifyPassword=UserModule::encrypting($model->verifyPassword);

    			if($model->save()) {


    				$profile->user_id=$model->id;
    				$profile->save(false);
    			}
    			$this->redirect(array('index','id'=>$model->id));
    		} else {
    			$profile->validate();
    		}
    	}

    	$this->render('create',array(
    		'model'=>$model,
    		'profile'=>$profile,
    	));

    }
    public function actionView($id){		
    	if($id){
			// $model = Profile::model()->find(array('condition' => 'user_id = '.$id));
    		$model = User::model()->findByPk($id);
            $profile=$model->profile;
    		$this->render('view' ,array(
    			'model'=>$model,
                'profile'=>$profile
    		));
    	}
    }
    public function actionUpdate($id){

    	if($id){
    		$model = User::model()->notsafe()->findbyPk($_GET['id']);
    		$profile=$model->profile;
    		$model->verifyPassword = $model->password;
    		$this->performAjaxValidation(array($model,$profile));
    		if(isset($_POST['ajax']) && $_POST['ajax']==='registration-form')
    		{
    			echo UActiveForm::validate(array($model,$profile));
    			Yii::app()->end();
    		}

    		if(isset($_POST['User']))
    		{

    			$PGoup = $_POST['PGoup'];           
    			$PGoup = json_encode($PGoup);
    			$model->group = $PGoup;

    			$model->username = $_POST['User']['username'];
	            $model->email = $_POST['User']['email']; //**

	            // $model->identification = $_POST['Profile']['identification'];

	            $model->superuser = 1;

	            if(!empty($_POST['User']['newpassword'])){
	            	$model->password = $_POST['User']['newpassword'];
	            	$model->verifyPassword = $_POST['User']['confirmpass'];
	            }
	            $profile->attributes=$_POST['Profile'];

	            if($model->validate()) {
	            	if(!empty($_POST['User']['newpassword'])){
	            		$model->password=UserModule::encrypting($model->password);
	            		$model->verifyPassword=UserModule::encrypting($model->verifyPassword);
	            	}

	            	if(!$model->save()){
	            		echo 'Model not save';
	            		exit();
	            	}
	            	if(!$profile->save(false)){
	            		echo 'profile not save';
	            		exit();
	            	}

	            	$this->redirect(array('index','id'=>$model->id));
	            }

	        }

	        $this->render('update',array(
	        	'model'=>$model,
	        	'profile'=>$profile,
	        ));



	    }
	}
	public function actionDelete($id){

		$model = UsersAdmin::model()->findByPk($id);
		$model->status = '0';
		$model->del_status = '1';
		$model->update();

		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
	}
	
	public function loadModel()
	{
		if($this->_model===null)
		{
			if(isset($_GET['id']))
				$this->_model=User::model()->notsafe()->findbyPk($_GET['id']);
			if($this->_model===null)
				throw new CHttpException(404,'The requested page does not exist.');
		}
		return $this->_model;
	}

	protected function performAjaxValidation($validate)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='user-form')
		{
			echo CActiveForm::validate($validate);
			Yii::app()->end();
		}
	}
}