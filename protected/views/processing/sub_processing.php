
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
<link rel="stylesheet" href="http://cdn.datatables.net/1.10.2/css/jquery.dataTables.min.css"></style>
<script type="text/javascript" src="http://cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js"></script>
<style type="text/css">
  .dropbtn {
    background-color: #8DBF42;
    color: white;
    padding: 16px;
    font-size: 16px;
    border: none;
  }

  .dropdown {
    position: relative;
    display: inline-block;
  }

  .dropdown-content {
    display: none;
    position: absolute;
    background-color: #f1f1f1;
    min-width: 160px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
  }

  .dropdown-content a {
    color: black;
    padding: 12px 16px;
    text-decoration: none;
    display: block;
  }

  .dropdown-content a:hover {background-color: #ddd;}

  .dropdown:hover .dropdown-content {display: block;}

  .dropdown:hover .dropbtn {background-color: #3e8e41;}

</style> 
<div class="col-lg-12 set-height set-padding">
    <div id="section3">
        <div class="card table-card">

            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped table-main table-linkmodal table-hover" id="myTable2">
                        <thead>
                            <tr>
                                <?php
                          
                                $criteria = new CDbCriteria();
                                $criteria->compare('id',$_POST['id_data']);
                                $criteria->compare('active',1);
                                $Processing_sub = Processing::model()->find($criteria);

                                $criteria = new CDbCriteria();
                                $criteria->compare('id',$Processing_sub->province);
                                $criteria->compare('active',1);
                                $Province_sub = Province::model()->find($criteria);
                                ?>
                                <th colspan="8" class="text-left"><?php echo $Province_sub->province_name_th; ?></th>
                                <th colspan="2" style="background: #298FDE !important;"> <a type="button" href="javascript:void(0)" class="btn btn-icon text-white create-subprocessing"><i data-feather="printer" style="width: 15px;height: 15px;"></i> เพิ่มข้อมูล</a></th>
                            </tr>
                            <tr>
                                <!-- <th>
                                    <input class="" id="solid3" type="checkbox" checked="" data-bs-original-title="" title="">
                                </th> -->
                                <th>รหัสอำเภอ</th>
                                <th>อำเภอ</th>
                                <th>รหัสตำบล</th>
                                <th>ตำบล</th>
                                <th>รหัสหมู่บ้าน</th>
                                <th>หมู่บ้าน</th>
                                <th>ครัวเรือนเกษตรทั้งหมด</th>
                                <th>ครัวเรือนเกษตรตามพวก</th>
                                <th>สำรวจ</th>
                                <th>จัดการ</th>
                            </tr>
                        </thead>
                        <tbody>
                            <!-- <tr data-toggle="modal" data-target="#exampleModal2"> -->
                                <!-- <td> <input class="" id="solid3" type="checkbox" checked="" data-bs-original-title="" title=""></td> -->
                                <?php
                                if (!empty($model_SubProcessing)) {
                                 foreach ($model_SubProcessing as $key => $value) {

                                    $criteria = new CDbCriteria();
                                    $criteria->compare('id',$value->district);
                                    $criteria->compare('active',1);
                                    $District = District::model()->find($criteria);

                                    $criteria = new CDbCriteria();
                                    $criteria->compare('id',$value->subdistrict);
                                    $criteria->compare('active',1);
                                    $SubDistrict = SubDistrict::model()->find($criteria);

                                    $criteria = new CDbCriteria();
                                    $criteria->compare('village_id',$value->village);
                                    $criteria->compare('active',1);
                                    $Village = Village::model()->find($criteria);
                    
                                    ?>
                                    <tr> 
                                        <td><?php echo $District->district_code; ?></td>
                                        <td><?php echo $District->district_name_th; ?></td>
                                        <td><?php echo $SubDistrict->subdistrict_code; ?></td>
                                        <td><?php echo $SubDistrict->subdistrict_name_th; ?></td>
                                        <td><?php echo $value->village != "" ? $Village->village_code : ""; ?></td>
                                        <td><?php echo $value->village != "" ? $Village->village_name : ""; ?></td>
                                        <td><?php echo $value->total; ?></td>
                                        <td><?php echo $value->random; ?></td>
                                        <td><?php echo $value->survey; ?></td>
                                        <td>
                                        <a href="javascript:void(0)" class="edit-subprocessing" data-id="<?php echo $value->id; ?>"><img src="<?php echo Yii::app()->baseUrl;?>/theme/assets/images/icon/edit.jpg"></a>
                                        <a href="javascript:void(0)" class="delete-subprocessing" data-id="<?php echo $value->id; ?>"><img src="<?php echo Yii::app()->baseUrl;?>/theme/assets/images/icon/delete.jpg"></a>
                                        </td>  
                                    </tr>
                                    <?php
                                }
                            }else{ ?>
                                <tr>
                                    <td colspan="10" align="text-center">ไม่พบข้อมูล</td>  
                                </tr>
                           <?php }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="exampleModal2" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="Save_SubData();">บันทึก</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิด</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
    $('#myTable2').dataTable({
        responsive: true,
        "pageLength": 10,
        "searching": false,
        "info": false,
        "lengthChange": false

    });

});


    $(".create-subprocessing").click(function(){
        var create = 'add'; 
        var id_prcessing = <?php echo $_POST['id_data'] ?>;
        $.ajax({
            url: "<?= $this->createUrl('/Processing/LoadSubData'); ?>", 
            type: "POST",
            data:  {
                id_prcessing:id_prcessing,
                action:create
            },
            success: function(data){
               $('#exampleModal2').modal('show');
               $('#exampleModal2 .modal-body').html(data);

            }  
        });          
    });

    $(".edit-subprocessing").click(function(){
        var id = $(this).attr("data-id");

        if (id != "") {
            $.ajax({
                url: "<?= $this->createUrl('/Processing/updateSubProcessing'); ?>", 
                type: "POST",
                data:  {
                    id_subProcessing:id,
            },
            success: function(data){

              $('#exampleModal2').modal('show');
              $('#exampleModal2 .modal-body').html(data);
          }                           
      });
        }   
    });

    $(".delete-subprocessing").click(function(){
        var id = $(this).attr("data-id");

        if (id != "") {
            $.ajax({
                url: "<?= $this->createUrl('/Processing/DeleteSubProcessing'); ?>", 
                type: "POST",
                data:  {
                    id_Processing:id,
            },
            success: function(data){
             
            if(data == 1){
                SearchData();
                SearchSub();
            }else{
                alert("DELETE DATA ERROR");
            }
          }                           
      });
        }
    });

    function Save_SubData(){
       var id = $("#total_sub-data").attr('model-id');
       var districts = $("#District_selected").val();
       var subdistricts = $("#SubDistrict_selected").val();
       var villages = $("#Village_selected").val();
       if (districts != null && subdistricts != null && villages != null) {
            $.ajax({
                url: "<?= $this->createUrl('/Processing/SaveSubData'); ?>", 
                type: "POST",
                data:  {
                    id:id,
                    processing_id:<?php echo $Processing_sub->id ?>,
                    district:$("#District_selected").val(),
                    subdistrict:$("#SubDistrict_selected").val(),
                    village:$("#Village_selected").val(),
                    total:$("#total_sub-data").val(),
                    random:$("#random_sub-data").val(),
                    survey:$("#survey_sub-data").val(),
                },
                success: function(data){

                    if (data == true) {
                        SearchData();
                        SearchSub();
                    }else{
                        alert("SAVE DATA ERROR");
                    }
                }                           
            });
        }else{
            alert("กรุณากรอกข้อมูลให้ครบ");
        }
    }

    function SearchSub(){
        var id = <?php echo $_POST['id_data'] ?>;

        if (id != "") {
            $.ajax({
                url: "<?= $this->createUrl('/Processing/SubProcessing'); ?>", 
                type: "POST",
                data:  {
                    id_data:id,
                },
                success: function(data){

                    $('#SubProcessing').html(data);
                }                           
            });
        }
    }


</script>