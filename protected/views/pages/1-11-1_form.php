 
 
                <div class="container-fluid">

                  
                    <!-- start row -->
                    <div class="row">
                        <div class="col-lg-12 set-height set-padding">
                            <div id="section1">
                                <div class="card">
                                    <div class="card-header">
                                    <h5 class="mb-1">หมวดที่ 11 แบบสอบถามทัศนคติ ความคิดเห็น ความรู้สึก และการประเมิน<span class="line-danger">เหตุการณ์ในอดีต</span> ปัจจุบัน และ <span class="line-danger">อนาคต</span> <span>ปี 2563</span></h5>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-body">

                                        <form>
                                            <div class="row">
                                                <div class="col-md-2 mb-3">
                                                    <label for="">จังหวัด</label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-2 mb-3">
                                                    <label for="">อำเภอ</label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-2 mb-3">
                                                    <label for="">ตำบล</label>
                                                    <div class="input-group">
                                                        <input class="form-control" id="" type="text" placeholder="" aria-describedby="" required="" data-original-title="" title="">
                                                    </div>
                                                </div>
                                                <div class="col-md-2 mb-3">
                                                    <label for="">หมู่ที่</label>
                                                    <div class="input-group">
                                                        <input class="form-control" id="" type="text" placeholder="" aria-describedby="" required="" data-original-title="" title="">
                                                    </div>
                                                </div>
                                                <div class="col-md-2 mb-3">
                                                    <label for="">พวกที่</label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                                <div class="col-md-2 mb-3">
                                                    <label for="">ตัวอย่างที่</label>
                                                    <input class="form-control" id="" type="text" placeholder="" required="" data-original-title="" title="">
                                                </div>
                                            </div>
                                        </form>

                                    </div>
                                </div>
                            </div>

                            <div id="section2">

                     

                                <div class="card">
                                    <div class="card-body">
                                        <form>
                                            <div class="row">
                                                    <label for="" >โปรดใส่เลขรหัสในช่อง</label>   <div style="border: 2px solid #DDD;  padding: 15px; margin: 5px; border-radius: 3px; padding-bottom: 0px;"></div> <label for="" >แสดงระดับความพึงพอใจดังนี้</label> 
                                            </div>
											<div class="row">
                                                <label for="">1 = น้อยที่สุด  &nbsp 2 = น้อย   &nbsp 3 = ปานกลาง   &nbsp  4 = มาก    &nbsp  5 = มากที่สุด</label>
                                            </div>
                                            <hr>
                                            <div class="row">
                                                <h6 class="text-primary">1. ความภูมิใจและความพอใจในการเป็นเกษตรกร</h6>
                                            </div>

                                           
                                               
                                            <div class="row acc">
                                                    <label class="col-md-10 col-form-label">
                                                        <p class="mb-0">1.1 ท่านรู้สึกชอบและมีความสุขกับการประกอบอาชีพเกษตรกรรมที่ทำอยู่ในปัจจุบันระดับใด</p>
                                                    </label>
                                                    <div class="col-md-2">
                                                        <input class="form-control text-center mt-3" maxlength="1" type="text" data-original-title="" title="">
                                                    </div>

                                                    <label class="col-md-10 col-form-label">
                                                        <p class="mb-0">1.2 ท่านภูมิใจและพอใจในการเป็นเกษตรกรเพียงใด </p>
                                                    </label>
                                                    <div class="col-md-2">
                                                        <input class="form-control text-center mt-3" maxlength="1" type="text" data-original-title="" title="">
                                                    </div>

                                                    <label class="col-md-10 col-form-label">
                                                        <p class="mb-0">1.3 ท่านคิดว่าตัวท่านประสบความสำเร็จในการประกอบอาชีพเกษตรกรรมระดับใด </p>
                                                    </label>
                                                    <div class="col-md-2">
                                                        <input class="form-control text-center mt-3" maxlength="1" type="text" data-original-title="" title="">
                                                    </div>

                                                    <label class="col-md-10 col-form-label">
                                                        <p class="mb-0">1.4 ท่านคิดว่าตัวท่านได้รับการยอมรับจากสังคมหรือจากผู้ประกอบอาชีพสาขาอื่นๆในชุมชนเพียงใด </p>
                                                    </label>
                                                    <div class="col-md-2">
                                                        <input class="form-control text-center mt-3" maxlength="1" type="text" data-original-title="" title="">
                                                    </div>

                                            
                                           </div>
												
                                              <hr>
											  <div class="row">
                                                <h6 class="text-primary">2. ความคิดเห็นในการประกอบอาชีพเกษตรกรรมและการให้ลูกหลานสืบต่ออาชีพเกษตรกรรม</h6>
                                            </div>
                                            <div class="row acc">
                                                    <label class="col-md-10 col-form-label">
                                                        <p class="mb-0">2.1 ท่านคิดว่าการประกอบอาชีพเกษตรกรรมมีความมั่นคงระดับใด</p>
                                                    </label>
                                                    <div class="col-md-2">
                                                        <input class="form-control text-center mt-3" maxlength="1" type="text" data-original-title="" title="">
                                                    </div>

                                                    <label class="col-md-10 col-form-label">
                                                        <p class="mb-0">2.2 ท่านคิดจะขยายหรือเพิ่มการผลิตในไร่นา/เลี้ยงสัตว์หรือไม่ </p><small class="text-danger">(1.คิด  2.ไม่คิด  3.ไม่แน่ใจ)</small>
                                                    </label>
                                                    <div class="col-md-2">
                                                        <input class="form-control text-center mt-3" maxlength="1" type="text" data-original-title="" title="">
                                                    </div>

                                                    <label class="col-md-10 col-form-label">
                                                        <p class="mb-0">2.3 ท่านมีความคิดให้ลูกหลานสืบทอดอาชีพเกษตรกรรมต่อไปหรือไม่ </p><small class="text-danger">(1.คิด  2.ไม่คิด  3.ไม่แน่ใจ)</small>
                                                    </label>
                                                    <div class="col-md-2">
                                                        <input class="form-control text-center mt-3" maxlength="1" type="text" data-original-title="" title="">
                                                    </div>

                                                    <label class="col-md-10 col-form-label">
                                                        <p class="mb-0">2.4 ท่านวางแผนให้ลูกหลานสืบทอดอาชีพเกษตรกรรมที่ท่านทำอยู่ต่อไปหรือไม่ </p><small class="text-danger">(1.วางแผน  2.ไม่วางแผน)</small>
                                                    </label>
                                                    <div class="col-md-2">
                                                        <input class="form-control text-center mt-3" maxlength="1" type="text" data-original-title="" title="">
                                                    </div>

                                            
                                           </div>
												
												<hr>
											  <div class="row">
                                                <h6 class="text-primary">3. สภาพยุวเกษตรกร</h6>
                                            </div>
                                            <div class="row acc">
                                                    <label class="col-md-10 col-form-label">
                                                        <p class="mb-0">3.1 ท่านคิดว่าลูกหลานของท่านพอใจ/ยินดีจะประกอบอาชีพเกษตรกรรมต่อไปหรือไม่ </p><small class="text-danger">(1.ยินดี  2.ไม่ยินดี  3.ไม่แน่ใจ)</small>
                                                    </label>
                                                    <div class="col-md-2">
                                                        <input class="form-control text-center mt-3" maxlength="1" type="text" data-original-title="" title="">
                                                    </div>

                                                    <label class="col-md-10 col-form-label">
                                                        <p class="mb-0">3.2 ท่านคิดว่าในอนาคตลูกหลานของท่านสนใจจะเรียนต่อด้านเกษตรหรือไม่</p><small class="text-danger">(1.เรียน  2.ไม่เรียน)</small>
                                                    </label>
                                                    <div class="col-md-2">
                                                        <input class="form-control text-center mt-3" maxlength="1" type="text" data-original-title="" title="">
                                                    </div>

                                                    <label class="col-md-10 col-form-label">
                                                        <p class="mb-0">3.3 ปัจจุบันมีลูกหลานของท่านมาช่วยงานในฟาร์มหรือไม่ </p><small class="text-danger">(1.มี 2.ไมมี)</small>
                                                    </label>
                                                    <div class="col-md-2">
                                                        <input class="form-control text-center mt-3" maxlength="1" type="text" data-original-title="" title="">
                                                    </div>

                                                   
                                            
                                           </div>
												
												<hr>
											  <div class="row">
                                                <h6 class="text-primary">4. ความมั่นใจในสุขภาพและความสุขในครอบครัว</h6>
                                            </div>
                                            <div class="row acc">
                                                    <label class="col-md-10 col-form-label">
                                                        <p class="mb-0">4.1 สมาชิกในครอบครัวของท่านมีความรัก ความผูกพัน สัมพันธภาพที่ดีต่อกัน และอยู่ร่วมกันได้อย่างมีความสุขเป็นครอบครัวที่อบอุ่นระดับใด </p>
                                                    </label>
                                                    <div class="col-md-2">
                                                        <input class="form-control text-center mt-3" maxlength="1" type="text" data-original-title="" title="">
                                                    </div>

                                                    <label class="col-md-10 col-form-label">
                                                        <p class="mb-0">4.2 หากท่านหรือสมาชิกในครอบครัวเกิดเจ็บป่วย จะได้รับการรักษาพยาบาลในระดับใด</p>
                                                    </label>
                                                    <div class="col-md-2">
                                                        <input class="form-control text-center mt-3" maxlength="1" type="text" data-original-title="" title="">
                                                    </div>
                                                   
                                            
                                           </div>
                                        </form>

                                    </div>
                                </div>
                            </div>

                            <div class="row text-center group-submit">
                                <button type="button" class="btn btn-primary btn-lg">บันทึกข้อมูล</button>
                                <button type="button" class="btn btn-outline-secondary btn-lg">ยกเลิก</button>
                            </div>
                        </div>
                    </div>
                    <!-- end row -->

                </div>


 