<?php

class MtProvinceController extends Controller
{
	public $layout='admin';
	public function actionIndex()
	{
		$model = new MtProvince;
		$criteria = new CDbCriteria;
		$criteria->compare('active', 1);
		$criteria->order = 'province_name_th ASC';
		$data = MtProvince::model()->findAll($criteria);
		$this->render('index', Array(
			'data' => $data,
			'model' => $model,
		));
	}
	
	public function actionCreate() 
	{
		$model=new MtProvince;
		if(isset($_POST['MtProvince']))
    	{
			$model->attributes = $_POST['MtProvince'];
			$model->active = 1;
			if($model->validate()){
				if($model->save()){
					$this->redirect(array('view', 'id' => $model->id));
				}
			}
		}
		$this->render('create', Array(
			'model' => $model
		));
	}

	public function actionView($id)
    {
    	$this->render('view',array(
    		'model'=>$this->loadModel($id),
    	));
    }
	
	public function actionEdit($id)
	{
		$model=$this->loadModel($id);
		
		if(isset($_POST['MtProvince']))
    	{
			$model->attributes = $_POST['MtProvince'];
			if($model->validate()){
				if($model->save()){
					$this->redirect(array('view', 'id' => $model->id));
				}
			}
		}

		$this->render('update', Array(
			'model' => $model
		));
	}

	public function actionDelete($id)
	{
		$model = $this->loadModel($id);
		$model->active = 0;

		if($model->save())
		{
			$this->redirect(array('index'));
		}
	}

	public function loadModel($id){
		$model = MtProvince::model()->findByPk($id);
		return $model;
	}


	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}
?>