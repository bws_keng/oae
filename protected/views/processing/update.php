
<div class="row">
  <div class="table-responsive">
    <table class="table table-borderless table-modalmain" id="">
      <tbody>
        <tr>
          <td class="text-right">ปีเพาะปลูก :</td>
          <td><span class="text-main">
            <select class="form-control digits" id="year-data" name="year" disabled="true">
              <option selected disabled>เลือกปี พ.ศ.</option>
              <?php
              foreach ($Survey as $key => $value) {
                $year = $value->year + 543;
               ?>
               <option value="<?= $value->id;  ?>" <?php if(isset($model->year) && $model->year == $value->id){ echo "selected"; } ?> ><?= $year;  ?>/63</option>
               <?php
             }
             ?>
           </select></span></td>
         </tr>
         <tr>
          <td class="text-right">พวกที่ :</td>
          <td><span class="text-main"><select class="form-control digits" id="group-data" name="group" disabled="true">
            <option selected disabled>เลือกพวกที่</option>
            <?php
            foreach ($Those as $key => $value) { ?>
             <option value="<?= $value->those_id;  ?>" <?php if(isset($model->group) && $model->group == $value->those_id){ echo "selected"; } ?> ><?= $value->those_code;  ?> -> <?= $value->those_name;  ?></option>
             <?php
           }
           ?>
         </select></span></td>
       </tr>
       <tr>
        <td class="text-right">จังหวัด :</td>
        <td><span class="text-main"><?php
        $criteria = new CDbCriteria();
        $criteria->compare('active',1);
        $criteria->order = "province_name_th ASC";
        $Province = Province::model()->findAll($criteria);
        ?>
        <select class="form-control digits" id="province-data" name="province" disabled="true">
          <option selected disabled>เลือกจังหวัด</option>
          <?php
          foreach ($Province as $key => $value) { ?>
           <option value="<?= $value->id;  ?>" <?php if(isset($model->province) && $model->province == $value->id){ echo "selected"; } ?> ><?= $value->province_code; ?> -> <?= $value->province_name_th;  ?></option>
           <?php
         }
         ?>
       </select></td>
     </tr>
   </tbody>
 </table>
</div>
</div>
<hr>
<div class="row">
  <div class="table-responsive">
    <table class="table table-borderless table-modalmain" id="">
      <tbody>
        <tr>
          <td class="text-right">จำนวนหมู่ทั้งหมด :</td>
          <td><span class="text-main"><input class="form-control" type="text" name="total" id="total-data" value="<?= $model->total;?>" model-id="<?php echo $model->id; ?>"></span></td>
        </tr>
        <tr>
          <td class="text-right">จำนวนหมู่ที่สุ่ม :</td>
          <td><span class="text-main"><input class="form-control" type="text" name="Random" id="random-data" value="<?= $model->random;?>"></span></td>
        </tr>
        <tr>
          <td class="text-right">จำนวนหมู่ที่สำรวจ :</td>
          <td><span class="text-main"><input class="form-control" type="text" name="survey" id="survey-data" value="<?= $model->survey;?>" disabled="true" ></span></td>
        </tr>
      </tbody>
    </table>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function() {
    $("#random-data").keyup(function(){
      $("#survey-data").val($(this).val());
    });
  });
</script>