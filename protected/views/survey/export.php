
<!DOCTYPE html>
<html lang="en">
<body data-spy="scroll" data-target="#myScrollspy">
  <!-- start row -->
  <div class="row">
    <div class="col-lg-12 set-height set-padding">
      <div id="section1">
        <div class="card">
          <div class="card-header">
            <h5 class="mb-1">นำออกข้อมูลดิบ : หมวดที่ <?=$group["question_group_number"]?> - <?=$group["question_group_name"]?></h5>
          </div>
        </div>
        <div class="card table-card">
          <div class="card-header">
            <div class="row">
              <form enctype="multipart/form-data" id="sutdent-form" method="post"
              action="<?php echo Yii::app()->baseurl.'/survey/exportExcel'; ?>">
              <div class="col-md-12 mb-12">
                <div class="form-group">
                  <label for="">จังหวัด<font color="red">*</font></label>
                  <select class="form-control digits" id="export_Province" name="export[Province]">
                    <option selected value="all" >ทุกจังหวัด</option>
                    <?php
                    if (!empty($MtProvince)) {                                                    
                      foreach ($MtProvince as $key => $value) { ?>
                       <option value="<?= $value->province_code;  ?>" data-id="<?= $value->id; ?>"<?php if(isset($_GET["export"]["Province"]) && $_GET["export"]["Province"] == $value->province_code){ echo "selected"; } ?> ><?= $value->province_code;  ?> -> <?= $value->province_name_th;  ?></option>
                       <?php
                     }
                   }
                   ?>
                 </select>
               </div>
               <input type="hidden" name="export[id]" value="<?=$group["question_group_id"]?>">
               <div class="form-actions m-t-20">
                <button type="submit" class="btn btn-primary glyphicons circle_ok p-15"><i></i>นำออก
                  excel
                </button>

              </div>
            </div>

          </form>
        </div>
        <!-- <hr>
        <div class="row">
          <div class="col-md-12">
            <h3>การมีส่วนร่วมในโครงการหรือนโยบายของภาครัฐ</h3>
            <div class="table-responsive">
              <table class="table table-striped table-main" id="">
                <thead>
                  <tr class="report-th">
                    <th width="100">ลำดับ</th>
                    <th width="100">รหัสคำถาม</th>
                    <th width="100">คำถาม</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($QuestionnaireQuestion as $key => $value) {?>
                    <tr class="report-ans">
                      <td width="5%"><?=$key+1?></td>
                      <td width="20%">QuestionnaireQuestion-<?=$value->question_id?></td>
                      <td><h5><?=$value->title?></h5>(<?=$value->subtitle?>)</td>
                    </tr>
                  <?php } ?>

                </tbody>
              </table>
            </div>
          </div>
        </div> -->
        <hr>
        <div class="row">
          <div class="col-md-12">
            <h3>คำถามตามหมวด</h3>
            <div class="table-responsive">
              <table class="table table-striped table-main" id="">
                <thead>
                  <tr class="report-th">
                    <?php if ($group["question_group_id"] == 7 || $group["question_group_id"] == 8 || $group["question_group_id"] == 10) { ?>
                      <th width="100">CODE-<?=$group["question_group_id"]-1?></th>
                    <?php }else{ ?>
                      <th width="100">รหัสคำถาม</th>
                    <?php } ?>
                    <th width="100">คำถาม</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($QuestionMap as $key => $value) {?>
                    <tr class="report-ans">
                        <?php if ($group["question_group_id"] == 7 || $group["question_group_id"] == 8 || $group["question_group_id"] == 10) { ?>
                          <td width="20%"><?=$value->question_table['readable_id']?></td>
                          <td><?=$value->question_table->question?>
                          <?php 
                          if($value->question_table['readable_id'] != '' && $value->question_table['readable_id'] != NULL){
                            echo ' <red>('.$value->question_table['readable_id'].')</red>';
                          }
                          ?>
                        </td>
                      <?php }else{ ?>
                        <td width="20%">Question-<?=$value->question_table->question_id?></td>
                        <td><?=$value->question_table->question?>
                        <?php 
                        if($value->question_table['readable_id'] != '' && $value->question_table['readable_id'] != NULL){
                          echo ' <red>('.$value->question_table['readable_id'].')</red>';
                        }
                        ?>
                      </td>
                    <?php } ?>


                </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>
</div>


</body>

</html>

<script type="text/javascript">
  $( document ).ready(function() {
    <?php if (isset($_GET["search"]) && $_GET["search"] == "false") {?>
      alert("ไม่พบข้อมูล");
    <?php } ?>
  });
</script>