<form action="<?php echo Yii::app()->baseUrl . "/questionnaire/".$_GET['id'].'/edit'; ?>" method="POST" enctype="multipart/form-data">

    <div class="container-fluid">


        <!-- start row -->
        <div class="row">
            <div class="col-lg-12 set-height set-padding">
                <div id="section1">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="mb-1">แบบสอบถามภาวะเศรษฐกิจสังคมครัวเรือนและแรงงานเกษตร </h5>
                            <p class="mb-0">ระหว่างวันที่ <?php echo $start; ?> ถึง <?php echo $end; ?></p>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <h4 class="mb-4 text-main"><i class="icofont icofont-ui-user"></i> เจ้าหน้าที่สำรวจ</h4>
                            <hr>
                            <div>
                                <div class="row">
                                    <div class="col-md-4 mb-3">
                                        <label for="">ชื่อ</label>
                                        <input class="form-control" id="" type="text" name="staff_name" value="<?php echo $data['questionaire']['staff_name']; ?>">
                                    </div>
                                    <div class="col-md-4 mb-3">
                                        <label for="">นามสกุล</label>
                                        <input class="form-control" id="" type="text" name="staff_lname" value="<?php echo $data['questionaire']['staff_lname']; ?>">
                                    </div>
                                    <div class="col-md-4 mb-3">
                                        <label for="">รหัส</label>
                                        <div class="input-group">
                                            <input class="form-control" name="staff_id" value="<?php echo $data['questionaire']['staff_id']; ?>" id="" type="text" placeholder="" aria-describedby="inputGroupPrepend2" required="" data-original-title="" title="" name="staff_id" value="<?php echo $data['questionaire']['staff_id']; ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 mb-3">
                                        <label for="">วัน/เดือน/ปี</label>
                                        <input class="form-control digits" type="date" name="survey_time" value="<?php echo $data['questionaire']['survey_time']; ?>">
                                                    <!-- <div class="date-picker">
                                                        <label for="validationDefault03">วัน/เดือน/ปี</label>
                                                        <div class="input-group">
                                                            <input class="datepicker-here form-control digits" type="text" data-language="th">
                                                        </div>
                                                        </div>
                                                    </div> -->
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div id="section2">
                                <div class="card">
                                    <div class="card-body">
                                        <div>
                                            <div class="row acc-1">
                                                <div class="col-md-8 mb-3">
                                                    <label for=""><strong class="text-main">1.</strong> สำนักงานเศรษฐกิจการเกษตรที่ </label>
                                                    <input class="form-control" id="county-1-out" type="text" disabled="disabled">
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">สศท. <small class="text-danger">(เลข 2 หลัก)</small></label>
                                                     <?php
                                                        $office_id = '';
                                                        if ($data['questionaire']['office_id'] < 10) {
                                                            for ($i=0; $i <= $data['questionaire']['office_id'] ; $i++) { 
                                                                $office_id = '0'.$data['questionaire']['office_id'];
                                                            }
                                                        }else{
                                                            $office_id = $data['questionaire']['office_id'];
                                                        }
                                                    ?>
                                                    <input class="form-control " id="county-1" maxlength="2" type="text" name="office"  value="<?php echo $office_id ; ?>" data-master="county">
                                                </div>
                                                
                                                <div class="col-12" role="alert" style="display:none;" id="county-1-alert">
                                                    <p class="text-danger">ข้อมูลผิดพลาด</p>
                                                </div>
                                            </div>

                                            <div class="row acc-1">
                                                <div class="col-md-8 mb-3">
                                                    <label for=""><strong class="text-main">2.</strong> จังหวัด </label>
                                                    <input class="form-control" id="province-out" type="text" disabled="disabled">
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">จังหวัด <small class="text-danger">(เลข 3 หลัก)</small></label>
                                                    <input class="form-control " id="province" maxlength="3" type="text" name="province" value="<?php echo $data['questionaire']['province']; ?>" data-master="province">
                                                </div>
                                                
                                                <div class="col-12" role="alert" style="display:none;" id="province-alert">
                                                    <p class="text-danger">ข้อมูลผิดพลาด</p>
                                                </div>
                                            </div>

                                            <div class="row acc-1">
                                                <div class="col-md-8 mb-3">
                                                    <label for=""><strong class="text-main">3.</strong> อำเภอ </label>
                                                    <input class="form-control " id="district-out" type="text" disabled="disabled">
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">อำเภอ <small class="text-danger">(เลข 2 หลัก)</small></label>
                                                    <input class="form-control" id="district" maxlength="2" type="text" name="district" value="<?php echo $data['questionaire']['district']; ?>" data-master="district">
                                                </div>
                                                
                                                <div class="col-12" role="alert" style="display:none;" id="district-alert">
                                                    <p class="text-danger">ข้อมูลผิดพลาด</p>
                                                </div>
                                            </div>

                                            <div class="row acc-1">
                                                <div class="col-md-8 mb-3">
                                                    <label for=""><strong class="text-main">4.</strong> ตำบล </label>
                                                    <input class="form-control" id="subdistrict-out" type="text" disabled="disabled">
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">ตำบล <small class="text-danger">(เลข 2 หลัก)</small></label>
                                                    <input class="form-control" id="subdistrict" maxlength="2" type="text" name="subdistrict" value="<?php echo $data['questionaire']['subdistrict']; ?>" data-master="subdistrict">
                                                </div>
                                                
                                                <div class="col-12" role="alert" style="display:none;" id="subdistrict-alert">
                                                    <p class="text-danger">ข้อมูลผิดพลาด</p>
                                                </div>
                                            </div>

                                            <div class="row acc-1">
                                                <div class="col-md-4 mb-3">
                                                    <label for=""><strong class="text-main">5.</strong> หมู่บ้าน </label>
                                                    <input class="form-control" id="village-out" type="text" disabled="disabled">
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">หมู่ที่ <small class="text-danger">(เลข 2 หลัก)</small></label>
                                                    <input class="form-control text-center" maxlength="2" id="village" type="text" name="village" value="<?php echo $data['questionaire']['village']; ?>" data-master="village">
                                                </div>
                                                
                                                <div class="col-md-4 mb-3">
                                                    <label for=""> บ้านเลขที่ </label>
                                                    <input class="form-control"  type="text" name="addressNumber" value="<?php echo $data['questionaire']['addressNumber']; ?>">
                                                </div>
                                                <div class="col-12" role="alert" style="display:none;" id="village-alert">
                                                    <p class="text-danger">ข้อมูลผิดพลาด</p>
                                                </div>
                                                
                                            </div>

                                            <div class="row acc-1">
                                                <div class="col-md-8 mb-3">
                                                    <label for=""><strong class="text-main">6.</strong> พวกที่ </label>
                                                    <input class="form-control" id="master-6-out" type="text" disabled="disabled">
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">พวกที่ <small class="text-danger">(เลข 1 หลัก)</small></label>
                                                    <input class="form-control" id="master-6" maxlength="1" type="text" name="group"  value="<?php echo $data['questionaire']['group']; ?>" data-master="group">
                                                </div>
                                                <div class="col-12" role="alert" style="display:none;" id="master-6-alert">
                                                    <p class="text-danger">ข้อมูลผิดพลาด</p>
                                                </div>
                                            </div>

                                            <div class="row acc-1">
                                                <div class="col-md-12">
                                                    <label><strong class="text-main">7.</strong> ชื่อผู้ขึ้นทะเบียนเกษตรกร </label>
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">ชื่อ</label>
                                                    <input class="form-control" id="" type="text" name="farmer_name" value="<?php echo $data['questionaire']['farmer_name']; ?>">
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <label for="">นามสกุล </label>
                                                    <input class="form-control" id="" type="text" name="farmer_lname" value="<?php echo $data['questionaire']['farmer_lastname']; ?>">
                                                </div>

                                                <div class="col-md-4 mb-3">
                                                    <label for="">ตัวอย่างที่ <small class="text-danger">(เลข 1 หลัก)</small></label>
                                                    <input class="form-control" id="" maxlength="1" type="text" name="sample" value="<?php echo $data['questionaire']['sample']; ?>">
                                                </div>

                                                <div class="col-md-8 mb-3">
                                                    <label for=""> เลขบัตรประจำตัวประชาชน </label>
                                                    <input class="form-control" id="" type="text" name="farmer_id" value="<?php echo $data['questionaire']['farmer_id']; ?>">
                                                </div>

                                                <div class="col-md-4 mb-3">
                                                    <label for="">ผู้ขึ้นทะเบียนเกษตร <small class="text-danger">มี=1/ ไม่มี=2</small></label>
                                                    <input class="form-control" id="" maxlength="1" type="text" name="farmer_registered" value="<?php echo $data['questionaire']['farmer_registered']; ?>">
                                                </div>
                                            </div>

                                            <div class="row acc-1">
                                                <div class="col-md-12">
                                                    <label><strong class="text-main">8.</strong> ชื่อผู้ให้ข้อมูล </label>
                                                </div>
                                                <div class="col-md-4 ">
                                                    <label for="">ชื่อ</label>
                                                    <input class="form-control" id="" type="text" name="informant_name" value="<?php echo $data['questionaire']['informant_name']; ?>">
                                                </div>
                                                <div class="col-md-4 ">
                                                    <label for="">นามสกุล </label>
                                                    <input class="form-control" id="" type="text" name="informant_lname" value="<?php echo $data['questionaire']['informant_lastname']; ?>">
                                                </div>
                                                <div class="col-md-4 ">
                                                    <div class="form-group">
                                                        <label for="">ความสัมพันธ์กับหัวหน้าครัวเรือน</label>
                                                        <select class="form-control digits" name="informant_relation" value="<?php echo $data['questionaire']['informant_relation']; ?>">
                                                            <option selected hidden value="<?php echo $data['questionaire']['informant_relation']; ?>"><?php echo $data['questionaire']['informant_relation']; ?></option>
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-8 mb-3">
                                                    <label for=""> เลขบัตรประจำตัวประชาชน </label>
                                                    <input class="form-control" id="" type="text" name="informant_id" value="<?php echo $data['questionaire']['informant_id']; ?>">
                                                </div>

                                                <div class="col-md-4 mb-3">
                                                    <label for="">เบอร์ติดต่อ </label>
                                                    <input class="form-control" id="" maxlength="10" type="text" name="informant_tel" value="<?php echo $data['questionaire']['informant_tel']; ?>">
                                                </div>
                                            </div>

                                            <div class="form-group row acc-1">
                                                <label class="col-md-8 col-form-label"><strong class="text-main">9.</strong> ครัวเรือนเกษตรมีที่ดินทำการเกษตรอย่างน้อย 1 แปลง อยู่ในเขตชลประทาน <small class="text-danger">(มี=1 / ไม่มี=2)</small></label>
                                                <div class="col-md-2">
                                                    <input class="form-control text-center" maxlength="1" type="text" data-original-title="" title="" name="inIrrigatedArea" value="<?php echo $data['questionaire']['inIrrigatedArea']; ?>">
                                                </div>
                                            </div>

                                            <div class="row acc-1">
                                                <div class="col-md-12 mb-0">
                                                    <label for=""><strong class="text-main">10.</strong> การมีส่วนร่วมในโครงการหรือนโยบายของภาครัฐ ให้ตอบทุกข้อ (มีส่วนร่วม = 1 / ไม่มีส่วนร่วม = 2) </label>
                                                </div>

                                                <div class="row acc-2">
                                                    <?php
                                                    for ($i=0; $i < count($question); $i++) { 

                                                        echo
                                                        '<label class="col-md-8 col-form-label">
                                                        <p class="mb-0">'.($i+1).') '.$question[$i]['title'].' </p>
                                                        <small class="text-danger">'.$question[$i]['subtitle'].'</small>
                                                        </label>
                                                        <div class="col-md-2">
                                                        <input class="form-control text-center mt-3" maxlength="1" type="text" data-original-title="" title="" name="question-'.$question[$i]['question_id'].'"';
                                                        if($question[$i]['isRequired']){
                                                            echo ' required';
                                                        }
                                                        if(array_key_exists($question[$i]['question_id'], $data['value'])){
                                                            echo ' value="'.$data['value'][$question[$i]['question_id']].'"';
                                                        }
                                                        echo '>
                                                        </div>';
                                                    }
                                                    ?>
                                                </div>
                                            </div>

                                            <!-- <div class="row acc-1">
                                                <label class="col-sm-3 col-form-label"><i class="icon-map-alt"></i> แนบไฟล์แผนที่ :</label>
                                                <div class="col-sm-4">
                                                    <input class="form-control" type="file" name="mapfile">
                                                </div>
                                            </div> -->




                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row text-center group-submit">
                               <input type="submit" type="button" class="btn btn-primary btn-lg" value="บันทึกข้อมูล" />
                               <a href="javascript:void(0)" class="btn btn-outline-secondary btn-lg" onclick="window.history.go(-1); return false;">ย้อนกลับ</a>
                           </div>
                       </div>
                   </div>
                   <!-- end row -->

               </div>


           </form>
           <script type="text/javascript">
            $( document ).ready(function() {
                $(':input').attr('disabled','disabled');
                $(':submit').attr('style','display:none;');
                onchangeoffice();
                onchangePuak();
                // setTimeout(function(){ onchangeProvince(); }, 100);
                // setTimeout(function(){ onchangeDistrict(); }, 400);
                // setTimeout(function(){ onchangeSubdistrict(); }, 700);
                // setTimeout(function(){ onchangeVillage(); }, 1000);
                // setTimeout(function(){ onchangePuak(); }, 1000);
            });

            $( "#master-1" ).change(function() {
                $("#province").attr("data-id", "");
                $("#province").val("");
                $("#province-out").val("");
                $("#province-alert").val("");
                $("#province-alert").hide();

                $("#district").attr("data-id", "");
                $("#district").val("");
                $("#district-out").val("");
                $("#district-alert").val("");
                $("#district-alert").hide();

                $("#subdistrict").attr("data-id", "");
                $("#subdistrict").val("");
                $("#subdistrict-out").val("");
                $("#subdistrict-alert").val("");
                $("#subdistrict-alert").hide();

                $("#village").attr("data-id", "");
                $("#village").val("");
                $("#village-out").val("");
                $("#village-alert").val("");
                $("#village-alert").hide();
            });
            $( "#province" ).change(function() {
                var province_code = $("#province").val();
                var county_code = $("#county-1").val();
                if (province_code !="") {
                    if (county_code != undefined && county_code != "") {
                        $.ajax({
                            type: "POST",
                            url: "<?=$this->createUrl("/ajax/checkprovince");?>",
                            data: { province_code:province_code,county_code:county_code },
                            success: function(data){
                                dataarray = JSON.parse(data);
                                if(dataarray['error'] == "have-data"){
                                    $("#province-out").val(dataarray["name"]);
                                    $("#province").attr("data-id", dataarray['id']);
                                    $("#province-alert").val("");
                                    $("#province-alert").hide();
                                }else{
                                    $("#province-out").val("");
                                    $("#province").attr("data-id", "");
                                    $("#province-alert").val("ไม่พบข้อมูล");
                                    $("#province-alert").show();
                                }
                            },
                        });
                    }else{
                        alert("กรุณากรอก สศท.");
                        $("#province").val("");
                    }

                }else{
                    $("#province").attr("data-id", "");
                    $("#province-out").val("");
                    $("#province-alert").val("");
                    $("#province-alert").hide();

                    $("#district").attr("data-id", "");
                    $("#district").val("");
                    $("#district-out").val("");
                    $("#district-alert").val("");
                    $("#district-alert").hide();

                    $("#subdistrict").attr("data-id", "");
                    $("#subdistrict").val("");
                    $("#subdistrict-out").val("");
                    $("#subdistrict-alert").val("");
                    $("#subdistrict-alert").hide();

                    $("#village").attr("data-id", "");
                    $("#village").val("");
                    $("#village-out").val("");
                    $("#village-alert").val("");
                    $("#village-alert").hide();


                }

            });

            $( "#district" ).change(function() {
                var district_code = $("#district").val();
                var province_id = $("#province").attr("data-id");
                if (district_code != "") {
                    if (province_id != undefined && province_id != "") {
                        $.ajax({
                            type: "POST",
                            url: "<?=$this->createUrl("/ajax/checkdistrict");?>",
                            data: { district_code:district_code,province_id:province_id },
                            success: function(data){
                                dataarray = JSON.parse(data);
                                if(dataarray['error'] == "have-data"){
                                    $("#district-out").val(dataarray["name"]);
                                    $("#district").attr("data-id", dataarray['id']);
                                    $("#district-alert").val("");
                                    $("#district-alert").hide();
                                }else{
                                    $("#district-out").val("");
                                    $("#district").attr("data-id", "");
                                    $("#district-alert").val("ไม่พบข้อมูล");
                                    $("#district-alert").show();
                                }
                            },
                        });
                    }else{
                        alert("กรุณากรอกจังหวัด");
                        $("#district").val("");
                    }
                }else{
                    $("#district").attr("data-id", "");
                    $("#district-out").val("");
                    $("#district-alert").val("");
                    $("#district-alert").hide();

                    $("#subdistrict").attr("data-id", "");
                    $("#subdistrict").val("");
                    $("#subdistrict-out").val("");
                    $("#subdistrict-alert").val("");
                    $("#subdistrict-alert").hide();

                    $("#village").attr("data-id", "");
                    $("#village").val("");
                    $("#village-out").val("");
                    $("#village-alert").val("");
                    $("#village-alert").hide();
                }

            });

            $( "#subdistrict" ).change(function() {
                var subdistrict_code = $("#subdistrict").val();
                var district_id = $("#district").attr("data-id");
                if (subdistrict_code != "") {
                    if (district_id != undefined && district_id != "") {
                        $.ajax({
                            type: "POST",
                            url: "<?=$this->createUrl("/ajax/checksubdistrict");?>",
                            data: { subdistrict_code:subdistrict_code,district_id:district_id },
                            success: function(data){
                                dataarray = JSON.parse(data);
                                if(dataarray['error'] == "have-data"){
                                    $("#subdistrict-out").val(dataarray["name"]);
                                    $("#subdistrict").attr("data-id", dataarray['id']);
                                    $("#subdistrict-alert").val("");
                                    $("#subdistrict-alert").hide();
                                }else{
                                    $("#subdistrict-out").val("");
                                    $("#subdistrict").attr("data-id", "");
                                    $("#subdistrict-alert").val("ไม่พบข้อมูล");
                                    $("#subdistrict-alert").show();
                                }
                            },
                        });
                    }else{
                        alert("กรุณากรอกอำเภอ");
                        $("#subdistrict").val("");
                    }
                }else{
                    $("#subdistrict").attr("data-id", "");
                    $("#subdistrict-out").val("");
                    $("#subdistrict-alert").val("");
                    $("#subdistrict-alert").hide();

                    $("#village").attr("data-id", "");
                    $("#village").val("");
                    $("#village-out").val("");
                    $("#village-alert").val("");
                    $("#village-alert").hide();
                }


            });

            $( "#village" ).change(function() {
                var village_code = $("#village").val();
                var subdistrict_id = $("#subdistrict").attr("data-id");
                if (village_code != "") {
                    if (subdistrict_id != undefined && subdistrict_id != "") {
                        $.ajax({
                            type: "POST",
                            url: "<?=$this->createUrl("/ajax/checkvillage");?>",
                            data: { village_code:village_code,subdistrict_id:subdistrict_id },
                            success: function(data){
                                dataarray = JSON.parse(data);
                                if(dataarray['error'] == "have-data"){
                                    $("#village-out").val(dataarray["name"]);
                                    $("#village").attr("data-id", dataarray['id']);
                                    $("#village-alert").val("");
                                    $("#village-alert").hide();
                                }else{
                                    $("#village-out").val("");
                                    $("#village").attr("data-id", "");
                                    $("#village-alert").val("ไม่พบข้อมูล");
                                    $("#village-alert").show();
                                }
                            },
                        });
                    }else{
                        alert("กรุณากรอกตำบล");
                        $("#village").val("");
                    }
                }else{
                    $("#village").attr("data-id", "");
                    $("#village-out").val("");
                    $("#village-alert").val("");
                    $("#village-alert").hide();
                }
            });

            function onchangeoffice() {
                var county_code = $("#county-1").val();
                if (county_code != undefined && county_code != "") {
                    $.ajax({
                        type: "POST",
                        url: "<?=$this->createUrl("/ajax/checkoffice");?>",
                        data: { county_code:county_code },
                        success: function(data){
                            dataarray = JSON.parse(data);
                            if(dataarray['error'] == "have-data"){
                                $("#county-1-out").val(dataarray["name"]);
                                $("#county").attr("data-id", dataarray['id']);
                                $("#county-alert").val("");
                                $("#county-alert").hide();

                                onchangeProvince();
                            }
                        },
                    });
                }
            }

            function onchangeProvince() {
               var province_code = $("#province").val();
               var county_code = $("#county-1").val();
   
               if (province_code !="") {
                if (county_code != undefined && county_code != "") {
                    $.ajax({
                        type: "POST",
                        url: "<?=$this->createUrl("/ajax/checkprovince");?>",
                        data: { province_code:province_code,county_code:county_code },
                        success: function(data){
                            console.log(data);
                            dataarray = JSON.parse(data);
                            if(dataarray['error'] == "have-data"){
                                $("#province-out").val(dataarray["name"]);
                                $("#province").attr("data-id", dataarray['id']);
                                $("#province-alert").val("");
                                $("#province-alert").hide();

                                onchangeDistrict();
                            }
                        },
                    });
                }

            }
        }

        function onchangeDistrict() {
            var district_code = $("#district").val();
            var province_id = $("#province").attr("data-id");
            if (district_code != "") {
                if (province_id != undefined && province_id != "") {
                    $.ajax({
                        type: "POST",
                        url: "<?=$this->createUrl("/ajax/checkdistrict");?>",
                        data: { district_code:district_code,province_id:province_id },
                        success: function(data){
                            dataarray = JSON.parse(data);
                            if(dataarray['error'] == "have-data"){
                                $("#district-out").val(dataarray["name"]);
                                $("#district").attr("data-id", dataarray['id']);
                                $("#district-alert").val("");
                                $("#district-alert").hide();

                                onchangeSubdistrict();
                            }
                        },
                    });
                }
            }

        }

        function onchangeSubdistrict() {
            var subdistrict_code = $("#subdistrict").val();
            var district_id = $("#district").attr("data-id");
            if (subdistrict_code != "") {
                if (district_id != undefined && district_id != "") {
                    $.ajax({
                        type: "POST",
                        url: "<?=$this->createUrl("/ajax/checksubdistrict");?>",
                        data: { subdistrict_code:subdistrict_code,district_id:district_id },
                        success: function(data){
                            dataarray = JSON.parse(data);
                            if(dataarray['error'] == "have-data"){
                                $("#subdistrict-out").val(dataarray["name"]);
                                $("#subdistrict").attr("data-id", dataarray['id']);
                                $("#subdistrict-alert").val("");
                                $("#subdistrict-alert").hide();

                                onchangeVillage();
                            }
                        },
                    });
                }
            }

        }

        function onchangeVillage() {
            var village_code = $("#village").val();
            var subdistrict_id = $("#subdistrict").attr("data-id");
            if (village_code != "") {
                if (subdistrict_id != undefined && subdistrict_id != "") {
                    $.ajax({
                        type: "POST",
                        url: "<?=$this->createUrl("/ajax/checkvillage");?>",
                        data: { village_code:village_code,subdistrict_id:subdistrict_id },
                        success: function(data){
                            dataarray = JSON.parse(data);
                            if(dataarray['error'] == "have-data"){
                                $("#village-out").val(dataarray["name"]);
                                $("#village").attr("data-id", dataarray['id']);
                                $("#village-alert").val("");
                                $("#village-alert").hide();
                            }
                        },
                    });
                }
            }
        }

        function onchangePuak() {
            var puak_code = $("#master-6").val();
            if (puak_code != "") {
                $.ajax({
                    type: "POST",
                    url: "<?=$this->createUrl("/ajax/checkpuak");?>",
                    data: { puak_code:puak_code },
                    success: function(data){
                        dataarray = JSON.parse(data);
                        if(dataarray['error'] == "have-data"){
                            $("#master-6-out").val(dataarray["name"]);
                            $("#master-6-alert").val("");
                            $("#master-6-alert").hide();
                        }
                    },
                });
            }
        }
    </script>