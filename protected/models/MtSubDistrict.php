<?php

/**
 * This is the model class for table "master_county".
 *
 * The followings are the available columns in table 'master_county':
 * @property integer $county_id
 * @property string $county_name
 * @property integer $sector_id
 * @property integer $created_by
 * @property string $created_at
 * @property integer $updated_by
 * @property string $updated_at
 * @property integer $active
 * @property string $county_code
 */
class MtSubDistrict extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'db_oae.mt_subdistrict';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, created_by, updated_by, active, district_id', 'numerical', 'integerOnly'=>true),
			array('subdistrict_code, subdistrict_name_th', 'length', 'max'=>255),
			array('created_at, updated_at', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('district_id, subdistrict_code, subdistrict_name_th, id, created_by, created_at, updated_by, updated_at, active', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'SubDistrict ID',
			'subdistrict_code' => 'CODE ตำบล',
			'subdistrict_name_th' => 'ชื่อตำบล',
            'district_id' => 'District ID',
            'district_name_th' => 'ชื่ออำเภอ',
			'created_by' => 'Created By',
			'created_at' => 'Created At',
			'updated_by' => 'Updated By',
			'updated_at' => 'Updated At',
			'active' => 'Active',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('subdistrict_code',$this->subdistrict_code,true);
		$criteria->compare('subdistrict_name_th',$this->subdistrict_name_th);
        $criteria->compare('district_id',$this->district_id);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('updated_by',$this->updated_by);
		$criteria->compare('updated_at',$this->updated_at,true);
		$criteria->compare('active',$this->active,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function beforeSave() 
    {
		if(null !== Yii::app()->user && isset(Yii::app()->user->id))
		{
			$id = Yii::app()->user->id;
		}
		else
		{
			$id = 1;
		}

		if($this->isNewRecord)
		{
			$this->created_by = $id;
			$this->created_at = date("Y-m-d H:i:s");
			$this->updated_by = $id;
			$this->updated_at = date("Y-m-d H:i:s");
		}
		else
		{
			$this->updated_by = $id;
			$this->updated_at = date("Y-m-d H:i:s");
		}

        return parent::beforeSave();
    }
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MasterCounty the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

}
