<?php
$formNameModel = 'LogAdmin';
$titleName = 'Log การใช้งานผู้ดูแลระบบ';

Yii::app()->clientScript->registerScript('updateGridView', <<<EOD
	$.updateGridView = function(gridID, name, value) {
	    $("#"+gridID+" input[name*="+name+"], #"+gridID+" select[name*="+name+"]").val(value);
	    $.fn.yiiGridView.update(gridID, {data: $.param(
	        $("#"+gridID+" input, #"+gridID+" .filters select")
	    )});
	}
	$.appendFilter = function(name, varName) {
	    var val = eval("$."+varName);
	    $("#$formNameModel-grid").append('<input type="hidden" name="'+name+'" value="">');
	}
	$.appendFilter("LogAdmin[news_per_page]", "news_per_page");
EOD
    , CClientScript::POS_READY);
?>
    <!-- <div class="separator bottom form-inline small">
    <span class="pull-right">
        <label class="strong">แสดงแถว:</label>
        <?php echo $this->listPageShow($formNameModel);?>
    </span>
    </div> -->
    <div class="innerLR">
<div class="widget" style="margin-top: -1px;">
        <div class="widget-head">
            <h4 class="heading glyphicons show_thumbnails_with_lines"><i></i> <?php echo $titleName;?></h4>
        </div>
        <div class="widget-body">
            <div class="separator bottom form-inline small">
                <span class="pull-right">
                    <label class="strong">แสดงแถว:</label>
                    <?php echo $this->listPageShow($formNameModel);?>
                </span>
            </div>
            <div class="clear-div"></div>
            <div class="overflow-table">
                <?php $this->widget('AGridView', array(
                    'id'=>$formNameModel.'-grid',
                    'dataProvider'=>$model->search(),
                    //'filter'=>$model,
                    'selectableRows' => 2,
                    'rowCssClassExpression'=>'"items[]_{$data->id}"',
                    'htmlOptions' => array(
                        'style'=> "margin-top: -1px;",
                    ),
                    'afterAjaxUpdate'=>'function(id, data){
                        $.appendFilter("LogAdmin[news_per_page]");
                        InitialSortTable();
                        jQuery("#course_date").datepicker({
                            "dateFormat": "dd/mm/yy",
                            "showAnim" : "slideDown",
                            "showOtherMonths": true,
                            "selectOtherMonths": true,
                            "yearRange" : "-5+10",
                            "changeMonth": true,
                            "changeYear": true,
                            "dayNamesMin" : ["อา.","จ.","อ.","พ.","พฤ.","ศ.","ส."],
                            "monthNamesShort" : ["ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.",
                                "ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค."],
                       })
                    }',
                    'columns'=>array(
                        array(
            'header' => 'ลำดับ',
            // 'name' => 'cert_id',
            'sortable' => false,
            'htmlOptions' => array(
                'width' => '40px',
                'text-align' => 'center',
            ),
            'value'=>'$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
        ),
        array(
            'header' => 'ชื่อ - นามสกุล',
            'name'=>'search_name',
            'type'=>'raw',
            'value'=>function($data){
                return $data->member->pro_fname . ' ' . $data->member->pro_lname;
            }
        ),
        array(
            'header' => 'ฟังก์ชั่นการใช้งาน',
            'name'=>'controller',
            'type'=>'raw',
            'filter' => false,
            'value'=>function($data){
                if($data->module){
                    $text = Helpers::lib()->changeNameFunction($data->module);
                } else {
                    $text = Helpers::lib()->changeNameFunction($data->controller);
                }
                return $text;
            }
        ),
        array(
            'header' => 'รายละเอียด',
            'name'=>'action',
            'type'=>'raw',
            'filter' => false,
            'value'=>function($data){
                $link = '';
                $action = $data->action;
                $text = Helpers::lib()->changeNameFunction($data->action);
                if($data->module){
                    $link .= $data->module.'/'.$data->controller.'/'.$data->action;
                } else {
                    $link .= $data->controller.'/'.$data->action;
                }
                if($data->parameter){
                    $have_idcard = 2;

                    if($link == 'user/admin/update'){
                        $link .= '/id';
                    }elseif($link == 'profile/remove_file'){
                        $link .= '/../update';
                    }elseif($link == 'profile/up_file_att'){
                        $link .= '/../update';
                    }elseif($link == 'course/updatestatus'){
                        $link .= '/../update';
                    }elseif($link == 'profile/remove_file_ques'){
                        $link .= '/../update';
                    }elseif($link == 'profile/up_file_ques'){
                        $link .= '/../update';
                    }elseif($link == 'course/delete_teacher'){
                        $link .= '/../add_questionnaire';
                    }elseif($link == 'course/edit_teacher'){
                        $link .= '/../add_questionnaire';
                    }elseif($link == 'profile/delete'){
                        $link = $data->id_card;
                        $have_idcard = 1;
                    }

                    if($have_idcard == 2){
                        $link .= '/'. $data->parameter;
                    }

                }

                $link = Helpers::lib()->changeLink($link);

                if($text == "ajax_sort_table"){
                    return '<p>Move Table</p>';
                }elseif($text == "cancelIssueAll"){
                    return '<p>ยกเลิกการสมัครหลักสูตร</p>';
                }elseif($have_idcard == 1){
                    return '<p>'.$data->id_card.'</p>';
                }else{
                    return '<a target="_blank" href="'.Yii::app()->createUrl($link).'">'.$text.'</a>';
                }
            }
        ),
        array(
            'header' => 'วันและเวลา',
            'name'=>'create_date',
            'type'=>'raw',
            'filter' => false,
            'value'=>function($data){
                return $data->create_date;
            }
        ),
                    ),
                )); ?>
            </div>
        </div>
    </div>
</div>
