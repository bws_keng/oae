<?php
class ApiController extends Controller
{

    private $format = 'json';
    /**
     * @return array action filters
     */
    public function filters()
    {
            return array();
    }

    // Actions
    public function actionList()
    {
        // $test = Yii::app()->db->createCommand("SELECT * FROM db_oae.mt_province;")->queryAll();
        // $command = Yii::app()->db->createCommand();
        // $province = $command->select('*')->from('db_oae.mt_province')->queryAll();
        switch ($_GET['model']) {    
            case 'survey':
                $data = QuestionnaireModel::list();
                break;
            default:
                $data = (new MasterData($_GET['model']))->data();
                break;
        }
        $this->_sendResponse($status=200, $body=$data);
    }
    public function actionView()
    {
        switch ($_GET['model']) {
            case 'survey':
                $data = QuestionnaireModel::list($_GET['id']);
                break;
            default:
                $data = (new MasterData($_GET['model'], $where="mt_province.province_code = '".$_GET['id']."'"))->data();
                break;
        }
        $this->_sendResponse($status=200, $body=$data);
    }
    public function actionCreate()
    {
    }
    public function actionUpdate()
    {
    }
    public function actionDelete()
    {
    }

    private function _sendResponse($status = 200, $body = [], $content_type = 'application/json')
    {
        // and the content type
        header('Content-type: ' . $content_type);
        echo json_encode($body);
        Yii::app()->end();
    }

    private function _getStatusCodeMessage($status)
    {
        // these could be stored in a .ini file and loaded
        // via parse_ini_file()... however, this will suffice
        // for an example
        $codes = Array(
            200 => 'OK',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
        );
        return (isset($codes[$status])) ? $codes[$status] : '';
    }
    private function _checkAuth()
    {
        // Check if we have the USERNAME and PASSWORD HTTP headers set?
        if(!(isset($_SERVER['HTTP_X_USERNAME']) and isset($_SERVER['HTTP_X_PASSWORD']))) {
            // Error: Unauthorized
            $this->_sendResponse(401);
        }
        $username = $_SERVER['HTTP_X_USERNAME'];
        $password = $_SERVER['HTTP_X_PASSWORD'];
        // Find the user
        $user=User::model()->find('LOWER(username)=?',array(strtolower($username)));
        if($user===null) {
            // Error: Unauthorized
            $this->_sendResponse(401, 'Error: User Name is invalid');
        } else if(!$user->validatePassword($password)) {
            // Error: Unauthorized
            $this->_sendResponse(401, 'Error: User Password is invalid');
        }
    }
}
