<div class="container-fluid">
    <div id="section">
        <div class="card">
            <div class="card-header">
                <h5 class="mb-1">ตาราง การถือครองและการใช้ประโยชน์ที่ดินครัวเรือนเกษตร (ณ. xx เดือน คิวรี่นะ) <span class="pull-right">หมวด 1</span></h5>
            </div>
        </div>

        <div class="card table-card">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="">ปี พ.ศ.</label>
                            <select class="form-control digits" id="">
                                <option selected="" disabled="">เลือกปี พ.ศ.</option>
                                <option value="">2562</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <button class="btn btn-lg btn-search text-white" type="submit"><i class="icon-search"></i>&nbsp;ค้นหา</button>
                    </div>
                    <div class="col-md-7">
                        <div class="form-group m-t-20 text-right">
                            <button class="btn btn-lg btn-excel text-white pull-right " type="submit"><i class="fa fa-file-excel-o"></i>&nbsp;&nbsp;Export Excel</button>
                        </div>
                    </div>
                </div>


            </div>
            <div class="card-body ">
                <div class="table-responsive">
                    <table class="table table-striped table-main" id="">
                        <thead>
                            <tr class="report-th">
                                <th width="50%">รายการ</th>
                                <th width="100">ประเทศ</th>
                                <th width="100">ภาคเหนือ</th>
                                <th width="100">ภาคตะวันออกเฉียงเหนือ</th>
                                <th width="100">ภาคกลาง</th>
                                <th width="100">ภาคใต้</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="report-title">
                                <td colspan="6">เนื้อที่รวมทั้งหมดของครัวเรือน (ไร่/ครัวเรือน)</td>
                            </tr>
                            <tr class="report-ans">
                                <td class="text-left">"ที่เป็นของตนเอง/ที่มีกรรมสิทธิ์ครอบครองเป็นเจ้าของ"</td>
                                <td>1</td>
                                <td>1</td>
                                <td>1</td>
                                <td>1</td>
                                <td>1</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!--  card close -->

    </div>
</div>