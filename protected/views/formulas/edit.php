<?php $form=$this->beginWidget('AActiveForm', array(
 'id'=>'masterCounty-form',
 'enableClientValidation'=>true,
 'clientOptions'=>array(
  'validateOnSubmit'=>true
),
 'errorMessageCssClass' => 'label label-important',
 'htmlOptions' => array('enctype' => 'multipart/form-data')
)); ?>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12 set-height set-padding">
            <div id="section1">
                <div class="card">
                    <div class="card-header">
                        <h5 class="mb-1"><?=$formtext->question_group_name?> </h5>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <div>
                            <?php foreach ($data as $key => $value) { ?>
                                <div class="row">
                                    <div class="col-md-12 mb-3">
                                        <label><?=$value->formulas_name?></label>
                                        <?php 
                                        $criteria = new CDbCriteria;
                                        $criteria->compare('question_group_id', $value->question_group_id);
                                        $criteria->order = 'question_id ASC';
                                        $Datalist = Question::model()->findAll($criteria);
                                        ?>
                                        <select class="form-control" name="QuestionFormulas[<?=$value->formulas_id?>]">
                                            <option disabled>กรุณาเลือกคำถาม</option>
                                            <?php foreach ($Datalist as $keylist => $valuelist) { ?>
                                                <option <?=$value->question_id==$valuelist->question_id ? "selected":""?> value="<?=$valuelist->question_id?>"><?=$valuelist->question?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            <?php } ?>
                            <input type="hidden" name="QuestionFormulasGroup" value="<?=$value->question_group_id?>">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row text-center group-submit">
        <?php echo CHtml::tag('button',array('class' => 'btn btn-primary btn-lg'),'<i></i>บันทึกข้อมูล');?>
        <button type="button" id="button-cancel" class="btn btn-outline-secondary btn-lg" onclick="window.history.go(-1); return false;">ยกเลิก</button>
    </div>

    <?php $this->endWidget(); ?>
</div>

