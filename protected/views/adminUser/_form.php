<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

<style type="text/css">
.ui-datepicker select.ui-datepicker-month, .ui-datepicker select.ui-datepicker-year{
    color: black;
}
input.form-control{
    height: 40px;
}
.wizard-header{margin-bottom: 2em;}
.form-control{height: 40px;}
label{font-weight: bold;}
.card{padding: 1em;background-color: rgba(255, 255, 255, 0.5);}
.wizard-card .picture{width: 200px;height: 200px;border-radius: 0;}
.wizard-card.ct-wizard-orange .picture:hover {
    border-color: #26A69A;
}
</style>
<?php 
 require dirname(__FILE__)."/../../extensions/booster/widgets/TbSelect2.php";
 require dirname(__FILE__)."/../../extensions/booster/components/Booster.php";
date_default_timezone_set("Asia/Bangkok");
?>

<div class="container-fluid">
        <!-- start row -->
        <div class="row">
            <div class="col-lg-12 set-height set-padding">
                <div id="section1">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="mb-1">
                                <?php 
                                 $FormText = 'จัดการผู้ดูแลระบบ';

                                echo $FormText;
                                 ?>
                            </h5>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">

                            
                                <?php $form = $this->beginWidget('UActiveForm', array(
                                    'id'=>'registration-form',
                                   // 'enableAjaxValidation' => true,
                                   // 'enableClientValidation' => true,
                                    'clientOptions'=>array(
                                        'validateOnSubmit'=>true,
                                    ),
                                    'htmlOptions'=>array('enctype'=>'multipart/form-data'),
                                )); 
                                ?>
                                <?php echo $form->errorSummary(array($model, $profile)); ?>
                                <div class="row">
                                    <div class="col-md-6 mb-3">
                                            <div class="form-group">
                                            <label>กลุ่มผู้ใช้</label>
                                            <br>
                                            <?php
                                            $UPGroup =  PGroup::model()->findAll(array('condition' => 'id != 1'));
                                            $UPGrouplist = CHtml::listData($UPGroup,'id','group_name');
                                            $data_selected = [];
                                            if(!$model->isNewRecord){
                                                $UGroups = json_decode($model->group);
                                                if ($UGroups != null) {
                                                    foreach ($UGroups as $key => $uGroup) {
                                                        $data_selected[$uGroup]=array('selected' => 'selected');
                                                    }
                                                }
                                            }

                                            foreach ($UPGroup as $Group) {
                                                $UGroup[$Group->id] = $Group->group_name;
                                            }


                                            ?>

                                            <?php  
                                            $this->widget('booster.widgets.TbSelect2',
                                             array(
                                                 'name' => 'PGoup[]',
                                                 'data' => $UGroup,
                                                 'options' => array(
                                                     'placeholder' => 'type clever, or is, or just type!',
                                                     'width' => '100%',
                                                 ),
                                                 'htmlOptions' => array(
                                                     'multiple' => 'multiple',
                                                     'options'=> $data_selected,
                                                 ),
                                             )
                                         );
                                         ?>   

                                     </div>     
                                    </div> 
                                    <div class="col-md-6 mb-3">
                                        <div class="form-group">
                                        <label><?php //echo $form->labelEx($model, 'email'); ?>E-mail</label>
                                        <?php echo $form->textField($model, 'email', array('class' => 'form-control', 'placeholder' => 'Email')); ?>
                                        <?php echo $form->error($model, 'email'); ?>
                                    </div>
                                    </div>
                                    <div class="col-md-6 mb-3">
                                            <div class="form-group">
                                            <label><?php echo $form->labelEx($model, 'username'); ?></label>
                                            <?php echo $form->textField($model, 'username', array('class' => 'form-control', 'placeholder' => 'ชื่อผู้ใช้ ')); ?>
                                            <?php echo $form->error($model, 'username'); ?>
                                        </div>
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        <div class="row">
                                            <?php if($model->isNewRecord) { ?>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label><?php echo $form->labelEx($model, 'password'); ?></label>
                                                        <?php echo $form->passwordField($model, 'password', array('class' => 'form-control', 'placeholder' => 'รหัสผ่าน (ควรเป็น (A-z0-9) และมากกว่า 4 ตัวอักษร)')); ?>
                                                        <?php echo $form->error($model, 'password'); ?>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label><?php echo $form->labelEx($model, 'verifyPassword'); ?></label>
                                                        <?php echo $form->passwordField($model, 'verifyPassword', array('class' => 'form-control', 'placeholder' => 'ยืนยันรหัสผ่าน')); ?>
                                                        <?php echo $form->error($model, 'verifyPassword'); ?>
                                                    </div>
                                                </div>
                                            <?php } else { ?>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label><?php echo $form->labelEx($model, 'newpassword'); ?></label>
                                                        <?php echo $form->passwordField($model, 'newpassword', array('class' => 'form-control', 'placeholder' => 'รหัสผ่าน (ควรเป็น (A-z0-9) และมากกว่า 4 ตัวอักษร)')); ?>
                                                        <?php echo $form->error($model, 'newpassword'); ?>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label><?php echo $form->labelEx($model, 'confirmpass'); ?></label>
                                                        <?php echo $form->passwordField($model, 'confirmpass', array('class' => 'form-control', 'placeholder' => 'ยืนยันรหัสผ่าน')); ?>
                                                        <?php echo $form->error($model, 'confirmpass'); ?>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="col-md-6 mb-3">
                                            <div class="form-group">
                                            <label><?php echo $form->labelEx($profile, 'title_id'); ?></label>
                                            <?php echo $form->dropDownList($profile, 'title_id', ProfilesTitle::getTitleList(), array('empty' => '---เลือกคำนำหน้าชื่อ---', 'class' => 'form-control', 'style' => 'width:100%')); ?>
                                            <?php echo $form->error($profile, 'title_id'); ?>
                                        </div>
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label><?php echo $form->labelEx($profile, 'firstname'); ?></label>
                                                    <?php echo $form->textField($profile, 'firstname', array('class' => 'form-control', 'placeholder' => 'ชื่อจริง')); ?>
                                                    <?php echo $form->error($profile, 'firstname'); ?>
                                                </div>
                                            </div>                                     
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label><?php echo $form->labelEx($profile, 'lastname'); ?></label>
                                                <?php echo $form->textField($profile, 'lastname', array('class' => 'form-control', 'placeholder' => 'นามสกุล')); ?>
                                                <?php echo $form->error($profile, 'lastname'); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                </div>
                                    <?php
                                        $my_group = '';
                                        if(!Yii::app()->user->isGuest){
                                            $my_group = json_decode($model->group);
                                        }
                                    ?>

                        </div>
                    </div>
                </div>

                <style type="text/css">
                    .form-control{
                        font-size: 1.5rem !important;
                    }
                </style>

           <!--      <div id="section2">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                              <?php //$this->widget("CustomFormBuilder");?>
                            </div>
                        </div>
                    </div>
                </div >-->

                <div class="row text-center group-submit">
                    <!-- <input type="submit" type="button" class="btn btn-primary btn-lg" value="บันทึกข้อมูล" />
                    <button type="button" class="btn btn-outline-secondary btn-lg">ยกเลิก</button> -->
                    <?php echo CHtml::submitButton($model->isNewRecord ? 'บันทึกข้อมูล' : 'บันทึกข้อมูล', array('class' => 'btn btn-primary btn-lgy',)); ?>
                    <button type="button" class="btn btn-outline-secondary btn-lgy" onclick="window.history.go(-1); return false;">ยกเลิก</button>
                    <?php //echo CHtml::submitButton($model->isNewRecord ? 'บันทึกข้อมูล' : 'บันทึกข้อมูล', array('class' => 'btn btn-primary btn-lgy',)); ?>
                </div>
            </div>
        </div>
        <?php $this->endWidget(); ?>
        <!-- end row -->

    </div>
