<?php
/**
 * คลาสสำหรับการสร้างหน้า ใช้ชั่วคราวกรณีที่ต้องการจัดการหน้าแทนการใช้ php แบบเดิม
 */
class PagesController extends Controller
{

    public function actionIndex()
	{
		$this->render('index');
    }
    
    /**
     * ฟังค์ชั่นสำหรับแสดงหน้าเว็บไซต์ตามไฟล์ที่อยู่ใน `/views/pages/`
     * โดยนำค่าจาก GET ที่มี key ว่า `page` มาแสดง
     *  */ 
    public function actionPage()
    {
        $this->render($_GET['page']);
    }

     public function actionLogin()
    {
        $this->render('Login');
    }

}