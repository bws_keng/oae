<form class="container-fluid" method="POST" action="<?php echo Yii::app()->baseurl.'/admin/survey/'.$_GET['id'].'/answer'; ?>">
                <div class="container-fluid">

                   
<!-- start row -->
<div class="row">
    <div class="col-lg-12 set-height set-padding">
        <div id="section1">
            <div class="card">
                <div class="card-header">
                    <h5 class="mb-1">เพิ่มคำตอบ</h5>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <form>
                        <div class="row">
                            <div class="col-md-12 mb-3">
                                <label for="">คำถาม</label>
                                <select class="form-control digits" id="" name="question" required>
                                    <option selected disabled></option>
                                    <?php
                                    foreach($survey as $key => $row){
                                        echo '<option value="'.$row['question_id'].'">'.$row['question'].'</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 mb-3"> 
                            <!-- &nbsp &nbsp &nbsp <input class="form-check-input" type="checkbox" value=""> -->
                            <label for="">หัวข้อคำตอบ</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 mb-3">
                                <div class="row col-12">
                                    <input class="form-control col-8 mx-0" type="text" value="" name="title">
                                    <button type="button" class="btn col-1 mx-0" onclick="$(`input[name='title']`).val(() => { return $(`input[name='title']`).val() + '<b></b>'})"><B>B</B></button>
                                    <button type="button" class="btn col-1 mx-0" onclick="$(`input[name='title']`).val(() => { return $(`input[name='title']`).val() + '<i></i>'})"><i>i</i></button>
                                    <button type="button" class="btn col-1 mx-0" onclick="$(`input[name='title']`).val(() => { return $(`input[name='title']`).val() + '<red></red>'})"><red>A</red></button>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 mb-3" id="answer">
                                <label for="">คำตอบ</label>
                                <input class="form-control" type="text" name="description[]">
                            </div>
                            <div class="row">
                            <div class="col-md-6 mb-3"><br>
                                <label for=""></label>
                                <button type="button" class="btn" onclick='insertInput("answer", "description");'>
                                    <i data-feather="plus" style= "color:#298FDE;cursor:pointer;" onclick=""></i>
                                </button>
                            </div> 
                            </div>
                       </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="row text-center group-submit">
            <input type="submit" class="btn btn-primary btn-lg" value="บันทึกข้อมูล" />
            <button type="button" class="btn btn-outline-secondary btn-lg" onclick="window.history.go(-1); return false;">ยกเลิก</button>
        </div>
    </div>
</div>
<!-- end row -->

</div>

</form>