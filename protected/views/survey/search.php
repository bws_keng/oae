
<table class="table table-striped table-main" id="">
  <!-- <table class="table table-striped table-main" id="basic-2"> -->
    <thead>
      <tr>
        <th width="5%">ลำดับ</th>
        <?php
        foreach($data['schema'] as $header){
          echo '<th>'.$header['question'].'</th>';
        }
        ?>
        <th width="25%">จัดการข้อมูล</th>
      </tr>
    </thead>
    <tbody>
      <?php
                  // print_r($data);
      ?>
      <?php
      $no = 1;
      foreach($data['data'] as $key => $body){
        echo '<tr><td>'.$no.'</td>';
        foreach($data['schema'] as $header){
          echo '<td>'.$body[$header['question_id']].'</td>';
        }
        echo '<td>
        <button type="button" class="btn btn-icon text-primary"><i data-feather="eye"></i></button>
        <a type="button" class="btn btn-icon text-success" href="'.Yii::app()->baseurl.'/survey/'.$_GET['id'].'/'.$key.'/edit"><i data-feather="edit"></i></a>
        <button type="button" class="btn btn-icon text-danger" onclick="sendDelete('.$key.', \'';
        foreach($data['schema'] as $header){
          echo $body[$header['question_id']].', ';
        }
        echo'\')"><i data-feather="trash"></i></button>
        </td></tr>';
        $no++;
      }
      ?>
    </tbody>
  </table>
  <script type="text/javascript">
   $(document).ready(function(){
     feather.replace();

    });
   </script>    