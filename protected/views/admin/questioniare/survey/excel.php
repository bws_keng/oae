<?php
$FormText = 'เพิ่ม สมาชิกจาก Excel';
$this->breadcrumbs = array('ระบบรายชื่อสมาชิก' => array('admin'), $FormText,);
?>
<?php 
?>
<link href="<?php echo Yii::app()->getBaseUrl(); ?>/theme/scripts/assets/dropzone/css/dropzone.css" rel="stylesheet">
<script src="<?php echo Yii::app()->getBaseUrl(); ?>/theme/scripts/assets/dropzone/dropzone.min.js"></script>

<div class="innerLR">
    <div class="widget">

        <div class="widget-head">
            <h4 class="heading">นำเข้าจาก Excel</h4>
        </div>

        <div class="widget-body">
            <div class="row-fluid">

                <form enctype="multipart/form-data" id="sutdent-form" method="post"
                      action="<?php echo Yii::app()->baseurl.'/admin/survey/excel'; ?>">
                    <div class="span4">
                        <?php $form = $this->beginWidget('AActiveForm', array(
                            'id'=>'news-form',
                            'enableClientValidation'=>true,
                            'clientOptions'=>array(
                                'validateOnSubmit'=>true
                            ),
                            'errorMessageCssClass' => 'label label-important',
                            'htmlOptions' => array('enctype' => 'multipart/form-data')
                        )); ?>
                        <h4>นำเข้าไฟล์ CSV <label>(ไฟล์ excel เท่านั้น)</label></h4>
                        
                            <?php echo $form->fileField($model,'excel_file',array('class'=>'form-control','style'=>'width: 250px;')); ?>
                            <!-- </span> -->

                        <?php $this->endWidget(); ?>
                        <div class="form-actions m-t-20">

                            <button type="submit" class="btn btn-primary glyphicons circle_ok p-15"><i></i>นำเข้าไฟล์
                                excel
                            </button>
                            
                        </div>
                    </div>
                </form>
                        <br>
                        <br>
                <script type="text/javascript">
                    $('#sutdent-form').submit(function () {
                      
                        if ($('#User_excel_file').val() == '') {
                            alert('กรุณาเลือกไฟล์ Excel');
                            return false;
                        }
                        return true;
                    });
                </script>
              
            </div>
        </div>
    </div>
<br>
<br>
<br>
<?php if(!empty($chk_num_key)){ ?>
    <div class="widget">
        <div class="widget-head">
            <h4 class="heading">รายการที่เพิ่มสำเร็จ</h4>
        </div>

        <div class="widget-body">
            <div class="row-fluid" id='print'>
                <div class="widget-body">
                     <table class="table table-striped table-bordered ">
                        <thead>
                            <tr>
                                <th width="10%">ลำดับ</th>
                                <th>รหัสแบบสอบถาม</th>
                                <th>รหัส</th>
                                <th>สำนักงานเศรษฐกิจการเกษตร</th>
                                <th>สถานะ</th>

                            </tr>
                        </thead>
                        <tbody>
                    <?php
                    $no = 1;
                    foreach ($chk_num_key as $key => $value) {
                        ?>
                        <tr>
                            <td><?php echo $no; $no++; ?></td>
                            <td><?= $chkdatakey[$value]; ?></td>
                            <td><?= $chkdatastaff[$value]; ?></td>
                            <td><?= $chkdataoffice[$value]; ?></td>
                            <td><?= $chktext[$value]; ?></td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

<?php }else{
    ?>
    <div class="widget">
        <div class="widget-head">
            <h4 class="heading"></h4>
        </div>

        <div class="widget-body">
            <div class="row-fluid" id='print'>
                <div class="widget-body">
                     <!-- <h2>ไม่มีข้อมูล</h2> -->
                </div>
            </div>
        </div>
    </div>
    <?php
} ?>
    
</div>


